<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>William A. Weber</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">66</other_info_on_meta>
    <other_info_on_meta type="treatment_page">100</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Nuttall" date="1834" rank="genus">WYETHIA</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>7: 39, plate 5. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus WYETHIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Nathaniel Jarvis Wyeth, 1802–1856, early western American explorer</other_info_on_name>
    <other_info_on_name type="fna_id">135007</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 15–60 (–100) cm (taproots relatively massive; caudices seldom branched).</text>
      <biological_entity id="o23495" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect (or bending erect from bases), branched mostly from bases.</text>
      <biological_entity id="o23496" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from bases" constraintid="o23497" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o23497" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>usually petiolate (basal), sometimes sessile;</text>
      <biological_entity id="o23498" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (usually pinnately nerved, basal larger than cauline) ± deltate, elliptic-ovate, lanceolate, lance-elliptic, lance-linear, or oblong-ovate, bases truncate to cuneate, margins usually entire, rarely dentate to serrate (sometimes ciliate), faces glabrous or hairy (sometimes glanddotted or finely stipitate-glandular).</text>
      <biological_entity id="o23499" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic-ovate" value_original="elliptic-ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s5" value="lance-elliptic" value_original="lance-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lance-linear" value_original="lance-linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong-ovate" value_original="oblong-ovate" />
      </biological_entity>
      <biological_entity id="o23500" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
      <biological_entity id="o23501" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s5" value="dentate to serrate" value_original="dentate to serrate" />
      </biological_entity>
      <biological_entity id="o23502" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or 2–5 (–8+) in ± corymbiform to racemiform arrays.</text>
      <biological_entity id="o23503" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="8" upper_restricted="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o23504" from="2" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
      <biological_entity id="o23504" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="less corymbiform" is_modifier="true" name="architecture" src="d0_s6" to="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric to campanulate or turbinate, 12–60+ mm diam.</text>
      <biological_entity id="o23505" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s7" to="campanulate or turbinate" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s7" to="60" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 12–36 (–48) in 2–3+ series (subequal to unequal, outer sometimes foliaceous, much larger than inner).</text>
      <biological_entity id="o23506" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="36" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="48" />
        <character char_type="range_value" constraint="in series" constraintid="o23507" from="12" name="quantity" src="d0_s8" to="36" />
      </biological_entity>
      <biological_entity id="o23507" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to convex, paleate (paleae conduplicate, at least bases, papery).</text>
      <biological_entity id="o23508" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 5–25+, pistillate, fertile;</text>
      <biological_entity id="o23509" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="25" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas mostly yellow (cream to white in W. helianthoides).</text>
      <biological_entity id="o23510" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 35–150+, bisexual, fertile;</text>
      <biological_entity id="o23511" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="35" name="quantity" src="d0_s12" to="150" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes much shorter than cylindric throats;</text>
      <biological_entity id="o23512" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o23513" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than cylindric throats" constraintid="o23514" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o23514" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lobes 5, ± deltate to lanceolate (style-branches stigmatic in 2 barely distinct lines, appendages ± filiform).</text>
      <biological_entity id="o23515" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character char_type="range_value" from="less deltate" name="shape" src="d0_s14" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae ± prismatic, weakly 3–4-angled (faces glabrous or hairy);</text>
      <biological_entity id="o23516" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s15" value="3-4-angled" value_original="3-4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0, or persistent, coroniform (usually lacerate) or of 1–4+ ovate to subulate, erose to lacerate (often basally connate) scales.</text>
      <biological_entity id="o23518" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s16" to="4" upper_restricted="false" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s16" to="subulate" />
        <character is_modifier="true" name="architecture_or_relief" src="d0_s16" value="erose" value_original="erose" />
        <character is_modifier="true" name="shape" src="d0_s16" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>x = 19.</text>
      <biological_entity id="o23517" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character constraint="of scales" constraintid="o23518" is_modifier="false" name="shape" src="d0_s16" value="coroniform" value_original="coroniform" />
        <character is_modifier="false" name="shape" src="d0_s16" value="of 1-4+ ovate to subulate , erose to lacerate scales" />
      </biological_entity>
      <biological_entity constraint="x" id="o23519" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>275.</number>
  <other_name type="common_name">Mules-ears</other_name>
  <discussion>Species 8 (8 in the flora).</discussion>
  <discussion>Some species formerly included in Wyethia are here treated in Agnorhiza and Scabrethia. Balsamorhiza is closely related to Wyethia.</discussion>
  <references>
    <reference>Weber, W. A. 1946. A taxonomic and cytological study of the genus Wyethia, family Compositae, with notes on the related genus Balsamorhiza. Amer. Midl. Naturalist 35: 400–452.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres 35–60+ mm diam.; outer phyllaries (30–)40–80+ mm (± foliaceous, much surpassing discs)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres 10–30(–40) mm diam.; outer phyllaries 15–30(–40) mm (not foliaceous, seldom surpassing discs)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades: faces glabrous or finely stipitate-glandular, sometimes sparsely pilosulous as well (usually shining); cypselae 10–13 mm</description>
      <determination>1 Wyethia glabra</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades: faces densely tomentose to tomentulose (usually gland-dotted as well), glabrescent; cypselae 12–15 mm</description>
      <determination>2 Wyethia helenioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades: faces tomentose to tomentulose (at least when young, glabrescent, usuallygland-dotted)</description>
      <determination>3 Wyethia mollis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades: faces glabrous, glabrate, hirsute, hirsutulous, hirtellous, pilose, pilosulous, scabrellous, or strigillose (not tomentose to tomentulose, often gland-dotted or stipitate-glandular)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades: faces glabrous or glabrate (gland-dotted); phyllary margins not ciliate; cypselae glabrous</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades: faces usually hirsute, hirsutulous, hirtellous, pilose, pilosulous, scabrellous, or strigillose (sometimes vernicose, rarely finely stipitate-glandular or gland-dotted), sometimes glabrate; phyllary margins ± ciliate; cypselae strigillose or glabrous</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Basal leaves: blades lance-elliptic or oblong-lanceolate to lanceolate; rays 8–21(–25), laminae 25–60 mm; cypselae 8–9 mm</description>
      <determination>4 Wyethia amplexicaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Basal leaves: blades narrowly oblong-lanceolate to lance-linear; rays 5–8+, lami-nae 18–30 mm; cypselae 6–8 mm</description>
      <determination>5 Wyethia longicaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Basal leaves: blades lanceolate to lance-linear; cypselae 7–8 mm, ± strigillose</description>
      <determination>6 Wyethia angustifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Basal leaves: blades elliptic-ovate, lanceolate, or lance-elliptic; cypselae 9–11 mm, strigillose or glabrous</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Phyllaries 36–48; rays 13–25, laminae (cream to white) 25–45 mm; cypselae strigillose distally and on margins</description>
      <determination>7 Wyethia helianthoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Phyllaries 16–34; rays 11–12, laminae (yellow) 25–50 mm; cypselaeglabrous</description>
      <determination>8 Wyethia arizonica</determination>
    </key_statement>
  </key>
</bio:treatment>