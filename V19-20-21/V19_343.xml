<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="treatment_page">257</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1824" rank="genus">MYCELIS</taxon_name>
    <place_of_publication>
      <publication_title>in F. Cuvier, Dict. Sci. Nat. ed.</publication_title>
      <place_in_publication>2, 33: 483. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus MYCELIS</taxon_hierarchy>
    <other_info_on_name type="etymology">No etymology in protologue; no readily discernible meaning from Greek or Latin roots</other_info_on_name>
    <other_info_on_name type="fna_id">121375</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, (10–) 40–90+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted.</text>
      <biological_entity id="o2485" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1, usually erect, branched distally, glabrous.</text>
      <biological_entity id="o2487" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline (mostly cauline at flowering);</text>
      <biological_entity id="o2488" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal ± petiolate, distal ± sessile;</text>
      <biological_entity constraint="proximal" id="o2489" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2490" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades oblanceolate to spatulate (bases often clasping), margins pinnately lobed (lyrate to runcinate, terminal lobes ± deltate) and ± sharply dentate (faces glabrous).</text>
      <biological_entity id="o2491" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="spatulate" />
      </biological_entity>
      <biological_entity id="o2492" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="more or less sharply" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads in paniculiform to thyrsiform arrays.</text>
      <biological_entity id="o2493" name="head" name_original="heads" src="d0_s6" type="structure" />
      <biological_entity id="o2494" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="paniculiform" is_modifier="true" name="architecture" src="d0_s6" to="thyrsiform" />
      </biological_entity>
      <relation from="o2493" id="r245" name="in" negation="false" src="d0_s6" to="o2494" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncles not inflated distally, sometimes bracteolate.</text>
      <biological_entity id="o2495" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not; distally" name="shape" src="d0_s7" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s7" value="bracteolate" value_original="bracteolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi of 2–4 (often spreading to patent), ± deltate to lanceolate bractlets in 1 series.</text>
      <biological_entity id="o2496" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character char_type="range_value" from="less deltate" modifier="of 2-4" name="shape" src="d0_s8" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o2497" name="bractlet" name_original="bractlets" src="d0_s8" type="structure" />
      <biological_entity id="o2498" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <relation from="o2497" id="r246" name="in" negation="false" src="d0_s8" to="o2498" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres narrowly cylindric, 1–2+ mm diam.</text>
      <biological_entity id="o2499" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s9" to="2" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries (4–) 5 in 1 (–2) series (reflexed in fruit), linear, equal, margins little, if at all, scarious, apices rounded.</text>
      <biological_entity id="o2500" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character constraint="in series" constraintid="o2501" name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" notes="" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o2501" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s10" to="2" />
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o2502" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="at all" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o2503" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles flat to convex, weakly pitted, glabrous, epaleate.</text>
      <biological_entity id="o2504" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s11" to="convex" />
        <character is_modifier="false" modifier="weakly" name="relief" src="d0_s11" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Florets 5;</text>
      <biological_entity id="o2505" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow.</text>
      <biological_entity id="o2506" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae: bodies blackish to reddish, compressed, obovoid to lanceoloid, beaks whitish, stout, ribs 5–7 on each face, faces scabrellous;</text>
      <biological_entity id="o2507" name="cypsela" name_original="cypselae" src="d0_s14" type="structure" />
      <biological_entity id="o2508" name="body" name_original="bodies" src="d0_s14" type="structure">
        <character char_type="range_value" from="blackish" name="coloration" src="d0_s14" to="reddish" />
        <character is_modifier="false" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s14" to="lanceoloid" />
      </biological_entity>
      <biological_entity id="o2509" name="beak" name_original="beaks" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s14" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o2510" name="rib" name_original="ribs" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="on face" constraintid="o2511" from="5" name="quantity" src="d0_s14" to="7" />
      </biological_entity>
      <biological_entity id="o2511" name="face" name_original="face" src="d0_s14" type="structure" />
      <biological_entity id="o2512" name="face" name_original="faces" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent (borne on discs at tips of beaks), white;</text>
      <biological_entity id="o2513" name="cypsela" name_original="cypselae" src="d0_s15" type="structure" />
      <biological_entity id="o2514" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>outer of 12–20+, minute setae, inner of 60–80+, white, subequal, barbellulate bristles in 1–2+ series.</text>
      <biological_entity id="o2515" name="cypsela" name_original="cypselae" src="d0_s16" type="structure" />
      <biological_entity constraint="outer" id="o2516" name="cypsela" name_original="cypselae" src="d0_s16" type="structure" />
      <biological_entity id="o2517" name="seta" name_original="setae" src="d0_s16" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s16" to="20" upper_restricted="false" />
        <character is_modifier="true" name="size" src="d0_s16" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o2519" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="60" is_modifier="true" name="quantity" src="d0_s16" to="80" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="true" name="size" src="d0_s16" value="subequal" value_original="subequal" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <biological_entity id="o2520" name="series" name_original="series" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s16" to="2" upper_restricted="false" />
      </biological_entity>
      <relation from="o2516" id="r247" name="consist_of" negation="false" src="d0_s16" to="o2517" />
      <relation from="o2518" id="r248" name="consist_of" negation="false" src="d0_s16" to="o2519" />
      <relation from="o2519" id="r249" name="in" negation="false" src="d0_s16" to="o2520" />
    </statement>
    <statement id="d0_s17">
      <text>x = 9.</text>
      <biological_entity constraint="inner" id="o2518" name="cypsela" name_original="cypselae" src="d0_s16" type="structure" />
      <biological_entity constraint="x" id="o2521" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>43.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>