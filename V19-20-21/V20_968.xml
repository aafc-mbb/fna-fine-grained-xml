<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="treatment_page">421</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="genus">pyrrocoma</taxon_name>
    <taxon_name authority="(D. D. Keck) Kartesz &amp; Gandhi" date="1991" rank="species">linearis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>71: 60. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus pyrrocoma;species linearis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067426</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Hooker) Torrey &amp; A. Gray" date="unknown" rank="species">uniflorus</taxon_name>
    <taxon_name authority="D. D. Keck" date="unknown" rank="subspecies">linearis</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>4: 103. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Haplopappus;species uniflorus;subspecies linearis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–20 cm.</text>
      <biological_entity id="o21866" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–4, ascending, reddish, slender, lightly tomentose, glabrescent, eglandular.</text>
      <biological_entity id="o21867" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="4" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="lightly" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal sessile to winged-petiolate, blades linear (grasslike), 40–120 × 2–5 mm, margins usually entire, rarely with a few small teeth;</text>
      <biological_entity id="o21868" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o21869" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s2" to="winged-petiolate" />
      </biological_entity>
      <biological_entity id="o21870" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s2" to="120" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21871" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o21872" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
      <relation from="o21871" id="r2020" modifier="rarely" name="with" negation="false" src="d0_s2" to="o21872" />
    </statement>
    <statement id="d0_s3">
      <text>cauline sessile, blades linear, 10–20 × 1–3 mm;</text>
      <biological_entity id="o21873" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o21874" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o21875" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>faces sericeous to sparsely shaggy-tomentose, eglandular.</text>
      <biological_entity id="o21876" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o21877" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="sericeous" name="pubescence" src="d0_s4" to="sparsely shaggy-tomentose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads usually borne singly, terminal.</text>
      <biological_entity id="o21878" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 4–8 cm.</text>
      <biological_entity id="o21879" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s6" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric, 7–10 × 13–18 mm (bases white-tomentose).</text>
      <biological_entity id="o21880" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s7" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 2 series, loosely appressed, linear to linear-lanceolate, 8–9 mm, equal, bases chartaceous, margins entire, ciliate, apices green, acute, faces sparsely villous.</text>
      <biological_entity id="o21881" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" notes="" src="d0_s8" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="linear-lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s8" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o21882" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o21883" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o21884" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o21885" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o21886" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
      <relation from="o21881" id="r2021" name="in" negation="false" src="d0_s8" to="o21882" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 25–50;</text>
      <biological_entity id="o21887" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s9" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 6–10 mm.</text>
      <biological_entity id="o21888" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 30–60;</text>
      <biological_entity id="o21889" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s11" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 5–7 mm.</text>
      <biological_entity id="o21890" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae subcylindric, 2–4 mm, faces sericeous;</text>
      <biological_entity id="o21891" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="subcylindric" value_original="subcylindric" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21892" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi tawny, 3–4 mm.</text>
      <biological_entity id="o21893" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="tawny" value_original="tawny" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshy grassy areas, vernal stream banks, swales, meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshy grassy areas" />
        <character name="habitat" value="vernal stream banks" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Marsh goldenweed</other_name>
  <discussion>Pyrrocoma linearis is easily distinguished by its linear, entire, grasslike leaves, and terminal, single heads. This species is similar to P. uniflora and was formerly included there as a subspecies. Pyrrocoma uniflora differs in having lanceolate or oblanceolate leaves with dentate margins.</discussion>
  
</bio:treatment>