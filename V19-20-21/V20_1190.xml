<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">477</other_info_on_meta>
    <other_info_on_meta type="treatment_page">534</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="(Rydberg) G. L. Nesom" date="1995" rank="section">occidentales</taxon_name>
    <taxon_name authority="(Fernald) G. L. Nesom" date="1995" rank="species">hendersonii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 283. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section occidentales;species hendersonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067654</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="species">hendersonii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>22: 273. 1895 (as hendersoni)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species hendersonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="species">foliaceus</taxon_name>
    <taxon_name authority="(A. Gray) Cronquist" date="unknown" rank="subspecies">lyallii</taxon_name>
    <taxon_hierarchy>genus Aster;species foliaceus;subspecies lyallii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 70–150 cm, colonial;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-rhizomatous.</text>
      <biological_entity id="o22152" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="70" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="long-rhizomatous" value_original="long-rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5+, ascending to erect, glabrous or puberulent, especially distally.</text>
      <biological_entity id="o22153" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves thin, margins entire, apices acute, faces glabrous or scabridulous;</text>
      <biological_entity id="o22154" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o22155" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o22156" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o22157" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal persistent or withering by flowering, petiolate, blades oblanceolate, 50–150 × 10–20 mm, bases attenuate or cuneate;</text>
      <biological_entity constraint="basal" id="o22158" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character constraint="by blades, bases" constraintid="o22159, o22160" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o22159" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o22160" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline sessile, blades elliptic to oblanceolate, 80–200 × 10–25 mm, bases cuneate, auriculate;</text>
      <biological_entity constraint="proximal cauline" id="o22161" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o22162" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="oblanceolate" />
        <character char_type="range_value" from="80" from_unit="mm" name="length" src="d0_s5" to="200" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22163" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, 40–80 × 7–20 mm, reduced distally.</text>
      <biological_entity constraint="distal" id="o22164" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s6" to="80" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads in paniculiform arrays, branches numerous, 10–45 cm.</text>
      <biological_entity id="o22165" name="head" name_original="heads" src="d0_s7" type="structure" />
      <biological_entity id="o22166" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o22167" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="numerous" value_original="numerous" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s7" to="45" to_unit="cm" />
      </biological_entity>
      <relation from="o22165" id="r2046" name="in" negation="false" src="d0_s7" to="o22166" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles sparsely to densely hairy, bracts 3–10, acute, lanceolate to linear, bases auriculate.</text>
      <biological_entity id="o22168" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o22169" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="10" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="linear" />
      </biological_entity>
      <biological_entity id="o22170" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate, 5–10 mm.</text>
      <biological_entity id="o22171" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 4–5 series, narrowly oblong or linear (outer) to linear (inner), unequal, bases scarious, margins entire, sometimes proximally ciliate, green zones linear, apices acute, faces glabrous.</text>
      <biological_entity id="o22172" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s10" value="oblong" value_original="oblong" />
        <character name="shape" src="d0_s10" value="linear to linear" value_original="linear to linear" />
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o22173" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <biological_entity id="o22174" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o22175" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes proximally" name="architecture_or_pubescence_or_shape" src="d0_s10" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o22176" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o22177" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o22178" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o22172" id="r2047" name="in" negation="false" src="d0_s10" to="o22173" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 25–45;</text>
      <biological_entity id="o22179" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s11" to="45" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas violet, laminae 9–15 × 1–2.5 mm.</text>
      <biological_entity id="o22180" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="violet" value_original="violet" />
      </biological_entity>
      <biological_entity id="o22181" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s12" to="15" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets (50–) 70–80;</text>
      <biological_entity id="o22182" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="50" name="atypical_quantity" src="d0_s13" to="70" to_inclusive="false" />
        <character char_type="range_value" from="70" name="quantity" src="d0_s13" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, 5–6 mm, lobes triangular, 0.5–0.8 mm.</text>
      <biological_entity id="o22183" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22184" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae brown, cylindric to obovoid, not compressed, 3–3.5 mm, 3–4-nerved, faces hairy;</text>
      <biological_entity id="o22185" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s15" to="obovoid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-4-nerved" value_original="3-4-nerved" />
      </biological_entity>
      <biological_entity id="o22186" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi white, 4.5–5 mm. 2n = 16, 32.</text>
      <biological_entity id="o22187" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22188" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
        <character name="quantity" src="d0_s16" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, stream banks, forest openings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="forest openings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–1500+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500+" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>69.</number>
  <other_name type="common_name">Henderson’s aster</other_name>
  <discussion>Symphyotrichum hendersonii is concentrated in north-central Idaho and adjacent Montana and Washington, with possibly outlying populations in the Siskiyou Mountains of southwestern Oregon and adjacent California. It is in some respects intermediate between S. spathulatum and S. cusickii; forms transitional to both of these species occur.</discussion>
  
</bio:treatment>