<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">279</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="treatment_page">282</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">deinandra</taxon_name>
    <taxon_name authority="(de Candolle) Greene" date="1897" rank="species">fasciculata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Francisc.</publication_title>
      <place_in_publication>4: 424. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus deinandra;species fasciculata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066466</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hartmannia</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">fasciculata</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 693. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hartmannia;species fasciculata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="Bentham" date="unknown" rank="species">ramosissima</taxon_name>
    <taxon_hierarchy>genus Hemizonia;species ramosissima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 4–100 cm.</text>
      <biological_entity id="o4436" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± solid.</text>
      <biological_entity id="o4437" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal blades toothed, faces hirsute.</text>
      <biological_entity id="o4438" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o4439" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o4440" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually in glomerules or pairs, sometimes well separated, in racemiform or paniculiform arrays.</text>
      <biological_entity id="o4441" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="in glomerules or pairs; sometimes well" name="arrangement" notes="" src="d0_s3" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o4442" name="glomerule" name_original="glomerules" src="d0_s3" type="structure" />
      <biological_entity id="o4443" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o4441" id="r351" name="in" negation="false" src="d0_s3" to="o4442" />
      <relation from="o4441" id="r352" name="in" negation="false" src="d0_s3" to="o4443" />
    </statement>
    <statement id="d0_s4">
      <text>Bracts subtending heads usually overlapping at least proximal 1/2 of each involucre.</text>
      <biological_entity id="o4444" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" modifier="at-least" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character constraint="of involucre" constraintid="o4446" name="quantity" src="d0_s4" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o4445" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o4446" name="involucre" name_original="involucre" src="d0_s4" type="structure" />
      <relation from="o4444" id="r353" name="subtending" negation="false" src="d0_s4" to="o4445" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries sessile-glandular near margins, sometimes with nonglandular, non-pustule-based hairs as well.</text>
      <biological_entity id="o4447" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character constraint="near margins" constraintid="o4448" is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
      <biological_entity id="o4448" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o4449" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="non-pustule-based" value_original="non-pustule-based" />
      </biological_entity>
      <relation from="o4447" id="r354" modifier="sometimes; well" name="with" negation="false" src="d0_s5" to="o4449" />
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series.</text>
      <biological_entity id="o4450" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity id="o4451" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o4450" id="r355" name="in" negation="false" src="d0_s6" to="o4451" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 5;</text>
      <biological_entity id="o4452" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae deep yellow, 6–14 mm.</text>
      <biological_entity id="o4453" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 6, all or mostly functionally staminate;</text>
      <biological_entity id="o4454" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="6" value_original="6" />
        <character is_modifier="false" modifier="mostly functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers reddish to dark purple.</text>
      <biological_entity id="o4455" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s10" to="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pappi of 5–12 lanceolate to oblong or linear, entire or fringed scales 1–1.5 mm. 2n = 24.</text>
      <biological_entity id="o4456" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" modifier="of" name="quantity" src="d0_s11" to="12" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="oblong or linear entire or fringed" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="oblong or linear entire or fringed" />
      </biological_entity>
      <biological_entity id="o4457" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4458" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, openings in chaparral, coastal scrub, and woodlands, vernal pool beds, disturbed sites (e.g., burns), often in sandy or clayey soils, sometimes serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="openings" constraint="in chaparral , coastal scrub" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="vernal pool beds" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="burns" />
        <character name="habitat" value="sandy" modifier=") often" />
        <character name="habitat" value="clayey soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Deinandra fasciculata occurs in southwestern California, especially on immediate coast, and on southern Central Coast and in the western Outer South Coast Ranges.</discussion>
  
</bio:treatment>