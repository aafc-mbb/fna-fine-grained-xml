<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">470</other_info_on_meta>
    <other_info_on_meta type="mention_page">483</other_info_on_meta>
    <other_info_on_meta type="treatment_page">485</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="(Rafinesque) G. L. Nesom" date="1995" rank="subgenus">virgulus</taxon_name>
    <taxon_name authority="(Lindley) Brouillet &amp; S. Selliah" date="2005" rank="species">pygmaeum</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 1635. 2005</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus virgulus;species pygmaeum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067679</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="species">pygmaeus</taxon_name>
    <place_of_publication>
      <publication_title>in W. J. Hooker, Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 6. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species pygmaeus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">sibiricus</taxon_name>
    <taxon_name authority="(Lindley) Á. Löve &amp; D. Löve" date="unknown" rank="subspecies">pygmaeus</taxon_name>
    <taxon_hierarchy>genus Aster;species sibiricus;subspecies pygmaeus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Lindley) G. L. Nesom" date="unknown" rank="species">sibiricus</taxon_name>
    <taxon_name authority="(Lindley) Cody" date="unknown" rank="variety">pygmaeus</taxon_name>
    <taxon_hierarchy>genus Aster;species sibiricus;variety pygmaeus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eurybia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pygmaea</taxon_name>
    <taxon_hierarchy>genus Eurybia;species pygmaea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials 1.5–15 cm, cespitose;</text>
    </statement>
    <statement id="d0_s1">
      <text>with short, branched caudices, long-rhizomatous (both wiry).</text>
      <biological_entity id="o25081" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o25082" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="long-rhizomatous" value_original="long-rhizomatous" />
      </biological_entity>
      <relation from="o25081" id="r2313" name="with" negation="false" src="d0_s1" to="o25082" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10+, decumbent to ascending (purple), sparsely or densely villous to woolly distally.</text>
      <biological_entity id="o25083" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="ascending" />
        <character char_type="range_value" from="villous" modifier="sparsely; densely; distally" name="pubescence" src="d0_s2" to="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves firm, margins usually entire, sometimes remotely pauci-serrulate, sparsely villoso-ciliate, apices obtuse to acute, sometimes mucronate;</text>
      <biological_entity id="o25084" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o25085" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes remotely" name="architecture_or_shape" src="d0_s3" value="pauci-serrulate" value_original="pauci-serrulate" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s3" value="villoso-ciliate" value_original="villoso-ciliate" />
      </biological_entity>
      <biological_entity id="o25086" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal often withering by flowering, petiolate (petioles widely winged, sheathing), blades spatulate, 5–19 × 2–4 mm, bases attenuate, apices rounded, faces glabrous or sparsely villous proximally;</text>
      <biological_entity constraint="basal" id="o25087" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by blades, bases, apices, faces" constraintid="o25088, o25089, o25090, o25091" is_modifier="false" modifier="often" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o25088" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="19" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o25089" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="19" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o25090" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="19" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o25091" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="19" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal sessile, blades lanceolate to oblong-lanceolate or (sometimes) spatulate, 30–50 × 3–10 mm, bases ± clasping, apices obtuse to acute, faces glabrous or sparsely villous;</text>
      <biological_entity constraint="proximal" id="o25092" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o25093" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblong-lanceolate or spatulate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s5" to="50" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25094" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o25095" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity id="o25096" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades lanceolate to oblong, 13–19 × 2–4.5 mm, bases clasping to cuneate, apices acute to obtuse, faces sparsely woolly, sometimes sparsely stipitate-glandular.</text>
      <biological_entity constraint="distal" id="o25097" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o25098" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s6" to="oblong" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s6" to="19" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25099" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o25100" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
      <biological_entity id="o25101" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="woolly" value_original="woolly" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads borne singly.</text>
      <biological_entity id="o25102" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles densely villous to lanate distally, bracts 0.</text>
      <biological_entity id="o25103" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="densely villous" modifier="distally" name="pubescence" src="d0_s8" to="lanate" />
      </biological_entity>
      <biological_entity id="o25104" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres hemispherico-campanulate, 9–12.5 mm.</text>
      <biological_entity id="o25105" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="hemispherico-campanulate" value_original="hemispherico-campanulate" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="12.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–4 series (dark purple), lance-oblong or oblong (outer) to linear-lanceolate or sometimes linear (inner), subequal, outer ± herbaceous, bases not indurate, margins herbaceous (outer) to narrowly scarious and erose proximally (inner), strongly purple, villoso-ciliate in green portion, green zones (inner) 1/2–2/3 of distal portions, apices acute to acuminate, inner sometimes apiculate, appressed to loose and squarrose (particularly outer), faces woolly to densely villous, sparsely to moderately stipitate-glandular.</text>
      <biological_entity id="o25106" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblong" name="shape" notes="" src="d0_s10" to="linear-lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o25107" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity constraint="outer" id="o25108" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="growth_form_or_texture" src="d0_s10" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o25109" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o25110" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s10" to="narrowly scarious" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_relief" src="d0_s10" value="erose" value_original="erose" />
        <character is_modifier="false" modifier="strongly" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character constraint="in portion" constraintid="o25111" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s10" value="villoso-ciliate" value_original="villoso-ciliate" />
      </biological_entity>
      <biological_entity id="o25111" name="portion" name_original="portion" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o25112" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" constraint="of distal portions" constraintid="o25113" from="1/2" name="quantity" src="d0_s10" to="2/3" />
      </biological_entity>
      <biological_entity constraint="distal" id="o25113" name="portion" name_original="portions" src="d0_s10" type="structure" />
      <biological_entity id="o25114" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o25115" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s10" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s10" value="loose" value_original="loose" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="squarrose" value_original="squarrose" />
      </biological_entity>
      <biological_entity id="o25116" name="face" name_original="faces" src="d0_s10" type="structure">
        <character char_type="range_value" from="villous" modifier="densely" name="pubescence" src="d0_s10" to="sparsely moderately stipitate-glandular" />
        <character char_type="range_value" from="villous" name="pubescence" src="d0_s10" to="sparsely moderately stipitate-glandular" />
      </biological_entity>
      <relation from="o25106" id="r2314" name="in" negation="false" src="d0_s10" to="o25107" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 16–28;</text>
      <biological_entity id="o25117" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s11" to="28" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas purple to violet, laminae 12–18 × 2–3.2 mm.</text>
      <biological_entity id="o25118" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="purple" name="coloration" src="d0_s12" to="violet" />
      </biological_entity>
      <biological_entity id="o25119" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s12" to="18" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 53–55;</text>
      <biological_entity id="o25120" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="53" name="quantity" src="d0_s13" to="55" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow, 5.6–6.5 mm, throats funnelform, lobes triangular, 0.5–0.8 mm (red or white clavate-hairy).</text>
      <biological_entity id="o25121" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5.6" from_unit="mm" name="some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25122" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o25123" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae fusiform to cylindro-obconic, ± compressed, [size unknown], 4–7-nerved (faint), faces ± densely strigillose;</text>
      <biological_entity id="o25124" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s15" to="cylindro-obconic" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="4-7-nerved" value_original="4-7-nerved" />
      </biological_entity>
      <biological_entity id="o25125" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi whitish to yellowish, 5–7.2 mm.</text>
      <biological_entity id="o25126" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="whitish" name="coloration" src="d0_s16" to="yellowish" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="7.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, active, moist sand dunes, sandy or silty stream banks and terraces, usually cyclically disturbed gravelly tundra and tundra slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist sand dunes" modifier="open active" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="silty stream" />
        <character name="habitat" value="banks" />
        <character name="habitat" value="terraces" />
        <character name="habitat" value="gravelly tundra" modifier="usually cyclically disturbed" />
        <character name="habitat" value="tundra slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Nunavut; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Pygmy aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Symphyotrichum pygmaeum has long been included within or associated with Eurybia sibirica. Though similar in appearance, the two species can be distinguished by the glands present on S. pygmaeum on the distal leaves and phyllaries.</discussion>
  
</bio:treatment>