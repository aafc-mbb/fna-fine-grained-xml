<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="treatment_page">241</other_info_on_meta>
    <other_info_on_meta type="illustration_page">243</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">thymophylla</taxon_name>
    <taxon_name authority="(de Candolle) Small" date="1903" rank="species">pentachaeta</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1295, 1341. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus thymophylla;species pentachaeta</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250007123</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hymenatherum</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">pentachaetum</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 642. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hymenatherum;species pentachaetum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dyssodia</taxon_name>
    <taxon_name authority="(de Candolle) B. L. Robinson" date="unknown" rank="species">pentachaeta</taxon_name>
    <taxon_hierarchy>genus Dyssodia;species pentachaeta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, ± grayish to green, to 15 (–25) cm, usually puberulent to canescent, sometimes glabrescent or glabrous.</text>
      <biological_entity id="o12932" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="less grayish" name="coloration" src="d0_s0" to="green" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character char_type="range_value" from="usually puberulent" name="pubescence" src="d0_s0" to="canescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o12933" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="less grayish" name="coloration" src="d0_s0" to="green" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character char_type="range_value" from="usually puberulent" name="pubescence" src="d0_s0" to="canescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or spreading.</text>
      <biological_entity id="o12934" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly opposite;</text>
      <biological_entity id="o12935" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades mostly pinnately lobed, 6–28+ mm overall, lobes 3–11 linear to filiform (usually stiff, setiform).</text>
      <biological_entity id="o12936" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly pinnately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="28" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12937" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="11" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 20–100 mm, puberulent or glabrous.</text>
      <biological_entity id="o12938" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="100" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyculi 0, or of 1–5 deltate bractlets, lengths less than 1/2 phyllaries.</text>
      <biological_entity id="o12939" name="calyculus" name_original="calyculi" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12940" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="5" />
        <character is_modifier="true" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="1/2" />
      </biological_entity>
      <biological_entity id="o12941" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure" />
      <relation from="o12939" id="r884" name="consist_of" negation="false" src="d0_s5" to="o12940" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres obconic to campanulate or hemispheric, 4–6 mm.</text>
      <biological_entity id="o12942" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="obconic" name="shape" src="d0_s6" to="campanulate or hemispheric" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 12–21, margins of outer distinct 1/5 to nearly all their lengths, abaxial faces puberulent or glabrous.</text>
      <biological_entity id="o12943" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s7" to="21" />
      </biological_entity>
      <biological_entity id="o12944" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
        <character name="quantity" src="d0_s7" value="1/5" value_original="1/5" />
        <character char_type="range_value" from="1/5" name="character" src="d0_s7" to="lengths" />
      </biological_entity>
      <biological_entity constraint="outer" id="o12945" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o12946" name="face" name_original="faces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o12944" id="r885" name="part_of" negation="false" src="d0_s7" to="o12945" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets (8–) 12–21;</text>
      <biological_entity id="o12947" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s8" to="12" to_inclusive="false" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow to orange-yellow, laminae 2–6 (–8) × 1–3 mm.</text>
      <biological_entity id="o12948" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s9" to="orange-yellow" />
      </biological_entity>
      <biological_entity id="o12949" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 16–40 or 50–80;</text>
      <biological_entity id="o12950" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s10" to="40" />
        <character char_type="range_value" from="50" name="quantity" src="d0_s10" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 2–4 mm (tending to zygomorphy in peripheral florets in some plants).</text>
      <biological_entity id="o12951" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 2–3 mm;</text>
      <biological_entity id="o12952" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 10 erose and/or aristate scales mostly 1–3 mm.</text>
      <biological_entity id="o12953" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o12954" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="10" value_original="10" />
        <character is_modifier="true" name="architecture_or_relief" src="d0_s13" value="erose" value_original="erose" />
        <character is_modifier="true" name="shape" src="d0_s13" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o12953" id="r886" name="consist_of" negation="false" src="d0_s13" to="o12954" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., N.Mex., Nev., Tex., Utah; Mexico; introduced(?) in South America (Argentina).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="(?) in South America (Argentina)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres cylindric, 2–3.5 mm diam.; disc florets 16–40</description>
      <determination>5d Thymophylla pentachaeta var. hartwegii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres obconic, campanulate, or hemispheric, mostly 4–5 mm diam.; disc florets 50–80</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peduncles 20–50 mm; margins of outer phyllaries distinct almost to bases</description>
      <determination>5a Thymophylla pentachaeta var. belenidium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Peduncles (40–)50–100 mm; margins of outer phyllaries distinct less ca. 1/2 or ca. 1/3 their lengths</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf lobes mostly 9–11; phyllaries usually glabrous or glabrescent, rarely hairy</description>
      <determination>5b Thymophylla pentachaeta var. pentachaeta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf lobes mostly 5–7(–10); phyllaries densely puberulent</description>
      <determination>5c Thymophylla pentachaeta var. puberula</determination>
    </key_statement>
  </key>
</bio:treatment>