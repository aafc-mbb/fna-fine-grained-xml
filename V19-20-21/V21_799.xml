<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">321</other_info_on_meta>
    <other_info_on_meta type="treatment_page">322</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">perityle</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">coronopifolia</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 82. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section perityle;species coronopifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067317</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Laphamia</taxon_name>
    <taxon_name authority="(A. Gray) Hemsley" date="unknown" rank="species">coronopifolia</taxon_name>
    <taxon_hierarchy>genus Laphamia;species coronopifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 6–36 cm (in rock crevices, stems relatively many, erect or pendulous, very leafy);</text>
    </statement>
    <statement id="d0_s1">
      <text>sparsely to densely grayish hairy.</text>
      <biological_entity id="o1051" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="36" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely to densely" name="coloration" src="d0_s1" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 2–8 (–12) mm;</text>
      <biological_entity id="o1052" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o1053" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades pedately 3-lobed (lobes spatulate or linear), or 2–3-pinnatifid (lobes linear-filiform), 4–30 × 4–20 mm, ultimate margins entire.</text>
      <biological_entity id="o1054" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1055" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="pedately" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="2-3-pinnatifid" value_original="2-3-pinnatifid" />
        <character is_modifier="false" modifier="pedately" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="2-3-pinnatifid" value_original="2-3-pinnatifid" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o1056" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (2–5) in corymbiform arrays, 5–6.5 × 5–6 mm.</text>
      <biological_entity id="o1057" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o1058" from="2" name="atypical_quantity" src="d0_s4" to="5" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" notes="" src="d0_s4" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1058" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 7–15 mm.</text>
      <biological_entity id="o1059" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate.</text>
      <biological_entity id="o1060" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 2–16, linear-lanceolate to narrow-ovate, 3.5–5 × 0.5–1.5 mm.</text>
      <biological_entity id="o1061" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="16" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s7" to="narrow-ovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 8–12;</text>
      <biological_entity id="o1062" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, laminae broadly oblong or oblongelliptic to subspatulate, 3–7 × 2–3 mm.</text>
      <biological_entity id="o1063" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o1064" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblongelliptic" name="shape" src="d0_s9" to="subspatulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 30–40;</text>
      <biological_entity id="o1065" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s10" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, often purple-tinged, tubes 0.8–1 mm, throats tubular, tubular-funnelform, or tubular-campanulate, 1–1.3 mm, lobes 0.3–0.4 mm.</text>
      <biological_entity id="o1066" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
      <biological_entity id="o1067" name="tube" name_original="tubes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1068" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular-funnelform" value_original="tubular-funnelform" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular-campanulate" value_original="tubular-campanulate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular-funnelform" value_original="tubular-funnelform" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tubular-campanulate" value_original="tubular-campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1069" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae linear-oblong to narrowly oblanceolate, 1.8–2.5 mm, margins usually prominently calloused, sometimes thin, usually ciliate;</text>
      <biological_entity id="o1070" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s12" to="narrowly oblanceolate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1071" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually prominently" name="texture" src="d0_s12" value="calloused" value_original="calloused" />
        <character is_modifier="false" modifier="sometimes" name="width" src="d0_s12" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 2 (–3+) barbellulate bristles 1.5–2.5 mm plus crowns of hyaline, laciniate scales.</text>
      <biological_entity id="o1072" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o1073" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s13" to="3" upper_restricted="false" />
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="barbellulate" value_original="barbellulate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1075" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
        <character is_modifier="true" name="shape" src="d0_s13" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <relation from="o1072" id="r68" name="consist_of" negation="false" src="d0_s13" to="o1073" />
      <relation from="o1074" id="r69" name="part_of" negation="false" src="d0_s13" to="o1075" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 34.</text>
      <biological_entity id="o1074" name="crown" name_original="crowns" src="d0_s13" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity constraint="2n" id="o1076" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock and cliff faces</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" />
        <character name="habitat" value="cliff" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Chihuahua).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Crow-foot rock daisy</other_name>
  <discussion>Perityle coronopifolia is widespread in south-central and southeastern Arizona, and southwestern and south-central New Mexico. The combination of white rays, often pinnatifid leaves, and perennial habit distinguish it.</discussion>
  
</bio:treatment>