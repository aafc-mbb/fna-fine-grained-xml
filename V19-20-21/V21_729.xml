<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">298</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="B. G. Baldwin" date="1999" rank="genus">harmonia</taxon_name>
    <taxon_name authority="B. G. Baldwin" date="2002" rank="species">guggolziorum</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>48: 293, figs. 1, 2. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus harmonia;species guggolziorum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066833</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–30 cm;</text>
      <biological_entity id="o20208" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal unbranched portions of primary-stems usually longer than branches supporting heads.</text>
      <biological_entity constraint="primary-stem" id="o20209" name="portion" name_original="portions" src="d0_s1" type="structure" constraint_original="primary-stem proximal; primary-stem">
        <character is_modifier="true" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character constraint="than branches" constraintid="o20211" is_modifier="false" name="length_or_size" src="d0_s1" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity id="o20210" name="primary-stem" name_original="primary-stems" src="d0_s1" type="structure" />
      <biological_entity id="o20211" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o20212" name="head" name_original="heads" src="d0_s1" type="structure" />
      <relation from="o20209" id="r1384" name="part_of" negation="false" src="d0_s1" to="o20210" />
      <relation from="o20211" id="r1385" name="supporting" negation="false" src="d0_s1" to="o20212" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly on primary-stems (distal leaves of primary-stems not congested) and immediately proximal to branches supporting heads.</text>
      <biological_entity id="o20213" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="to branches" constraintid="o20215" is_modifier="false" modifier="immediately" name="position" notes="" src="d0_s2" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o20214" name="primary-stem" name_original="primary-stems" src="d0_s2" type="structure" />
      <biological_entity id="o20215" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity id="o20216" name="head" name_original="heads" src="d0_s2" type="structure" />
      <relation from="o20213" id="r1386" name="on" negation="false" src="d0_s2" to="o20214" />
      <relation from="o20215" id="r1387" name="supporting" negation="false" src="d0_s2" to="o20216" />
    </statement>
    <statement id="d0_s3">
      <text>Heads usually erect in bud and fruit.</text>
      <biological_entity id="o20217" name="head" name_original="heads" src="d0_s3" type="structure">
        <character constraint="in fruit" constraintid="o20219" is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o20218" name="bud" name_original="bud" src="d0_s3" type="structure" />
      <biological_entity id="o20219" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 3–6, hirsute and/or hirtellous near folded edges.</text>
      <biological_entity id="o20220" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="6" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
        <character constraint="near edges" constraintid="o20221" is_modifier="false" name="pubescence" src="d0_s4" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity id="o20221" name="edge" name_original="edges" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s4" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 3–6;</text>
      <biological_entity id="o20222" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corolla laminae 4–5 mm.</text>
      <biological_entity constraint="corolla" id="o20223" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 8–13, some or all bisexual, fertile.</text>
      <biological_entity id="o20224" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="13" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray cypselae not gibbous, beakless;</text>
      <biological_entity constraint="ray" id="o20225" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="gibbous" value_original="gibbous" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="beakless" value_original="beakless" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi to 0.5 mm.</text>
      <biological_entity id="o20226" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc cypselae 3–3.5 mm;</text>
      <biological_entity constraint="disc" id="o20227" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi of 9–11 lanceolate to linear, fimbriate scales 0.6–0.8 mm. 2n = 18.</text>
      <biological_entity id="o20228" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="9" modifier="of" name="quantity" src="d0_s11" to="11" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="linear" />
      </biological_entity>
      <biological_entity id="o20229" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="fimbriate" value_original="fimbriate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20230" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Harmonia guggolziorum occurs in the southern Inner North Coast Ranges. Molecular phylogenetic analyses have indicated a sister-group relationship between H. guggolziorum and the other serpentine-endemic species of Harmonia (H. doris-nilesiae, H. hallii, and H. stebbinsii; B. G. Baldwin 2001).</discussion>
  
</bio:treatment>