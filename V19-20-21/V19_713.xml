<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">432</other_info_on_meta>
    <other_info_on_meta type="treatment_page">434</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Weddell" date="1856" rank="genus">gamochaeta</taxon_name>
    <taxon_name authority="(Kunth) Cabrera" date="1961" rank="species">sphacelata</taxon_name>
    <place_of_publication>
      <publication_title>Bol. Soc. Argent. Bot.</publication_title>
      <place_in_publication>9: 380. 1961</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus gamochaeta;species sphacelata</taxon_hierarchy>
    <other_info_on_name type="fna_id">242427689</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">sphacelatum</taxon_name>
    <place_of_publication>
      <publication_title>in A. von Humboldt et al., Nov. Gen. Sp.</publication_title>
      <place_in_publication>4(fol.): 67. 1818;  4(qto.): 86. 1820 (as sphacilatum)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Gnaphalium;species sphacelatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–35 (–50) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually taprooted, rarely fibrous-rooted.</text>
      <biological_entity id="o8516" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to ascending, densely gray-white pannose.</text>
      <biological_entity id="o8517" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" modifier="densely" name="coloration" src="d0_s2" value="gray-white" value_original="gray-white" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pannose" value_original="pannose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, basal usually withering before flowering;</text>
      <biological_entity id="o8518" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character constraint="before flowering" is_modifier="false" modifier="usually" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear to narrowly oblanceolate (often folded along midveins), 1–4 cm × 1–3 (–5) mm (distally becoming ± patent, arcuate bracts surpassing the heads), faces concolor or weakly bicolor, pannose-tomentose (± equally grayish to whitish, basal-cells of hairs on adaxial faces persistent, expanded, glassy).</text>
      <biological_entity id="o8519" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8520" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="concolor" value_original="concolor" />
        <character is_modifier="false" modifier="weakly" name="coloration" src="d0_s4" value="bicolor" value_original="bicolor" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pannose-tomentose" value_original="pannose-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in terminal glomerules 1 cm or in interrupted, spiciform arrays 2–14 cm × 10–12 (–14) mm (pressed; glomerules sometimes axillary).</text>
      <biological_entity id="o8521" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="14" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o8522" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character constraint="in interrupted , spiciform arrays" name="some_measurement" src="d0_s5" unit="cm" value="1" value_original="1" />
        <character is_modifier="false" name="some_measurement" src="d0_s5" value="in interrupted , spiciform arrays" />
      </biological_entity>
      <relation from="o8521" id="r783" name="in" negation="false" src="d0_s5" to="o8522" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres cylindro-campanulate, 3.5–4 (–5) mm, sparsely arachnose.</text>
      <biological_entity id="o8523" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="arachnose" value_original="arachnose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in (3–) 4–5 series, outer triangular, lengths 1/3–1/2 inner, apices (brown) acute-acuminate (usually inrolled and spreading or recurved), inner triangular-lanceolate, laminae purplish (in bud) to whitish or silvery (in fruit), apices (usually striate) acute (not apiculate).</text>
      <biological_entity id="o8524" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity id="o8525" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s7" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity constraint="outer" id="o8526" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s7" to="1/2" />
      </biological_entity>
      <biological_entity constraint="inner" id="o8527" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure" />
      <biological_entity id="o8528" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="lengths" src="d0_s7" value="acute-acuminate" value_original="acute-acuminate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o8529" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="triangular-lanceolate" value_original="triangular-lanceolate" />
      </biological_entity>
      <biological_entity id="o8530" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="purplish" name="coloration" src="d0_s7" to="whitish or silvery" />
      </biological_entity>
      <biological_entity id="o8531" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o8524" id="r784" name="in" negation="false" src="d0_s7" to="o8525" />
    </statement>
    <statement id="d0_s8">
      <text>Florets: bisexual 3–5;</text>
      <biological_entity id="o8532" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>all corollas usually purplish distally.</text>
      <biological_entity id="o8533" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o8534" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually; distally" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae (tan) 0.5–0.6 mm.</text>
      <biological_entity id="o8535" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, pine-oak woodlands, dry and wet sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="pine-oak woodlands" />
        <character name="habitat" value="dry" />
        <character name="habitat" value="wet sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico; South America (scattered, fide A. L. Cabrera 1977+, part 10).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America (scattered)" establishment_means="native" />
        <character name="distribution" value="South America (fide A. L. Cabrera 1977+)" establishment_means="native" />
        <character name="distribution" value="South America (part 10)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="past_name">sphacilata</other_name>
  <other_name type="common_name">Owl’s crown</other_name>
  <discussion>Gamochaeta sphacelata is recognized by its linear to narrowly oblanceolate, concolor leaves, interrupted, spiciform arrays of heads in compact axillary glomerules or on lateral branches, glomerules subtended by ± patent, arcuate bracts, dark brown involucres, and acute-acuminate, recurving tips of outer and mid phyllaries. Roots are typically lignescent taproots. As in G. purpurea, the basal cells of each hair on adaxial leaf faces are persistent, expanded, and glassy.</discussion>
  
</bio:treatment>