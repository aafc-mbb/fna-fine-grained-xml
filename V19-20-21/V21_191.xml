<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">81</other_info_on_meta>
    <other_info_on_meta type="treatment_page">82</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">silphium</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">asteriscus</taxon_name>
    <taxon_name authority="(Greene) J. A. Clevinger" date="2004" rank="variety">simpsonii</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>14: 275. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus silphium;species asteriscus;variety simpsonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068739</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silphium</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">simpsonii</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>4: 44. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Silphium;species simpsonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 25–110 cm.</text>
      <biological_entity id="o21839" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal persistent;</text>
      <biological_entity id="o21840" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o21841" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline opposite or alternate;</text>
      <biological_entity id="o21842" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o21843" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>faces hirsute, hispid, or scabrous.</text>
      <biological_entity id="o21844" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o21845" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Paleae puberulent and stipitate-glandular.</text>
      <biological_entity id="o21846" name="palea" name_original="paleae" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 10–15 (–20).</text>
      <biological_entity id="o21847" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="20" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s5" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 85–110.</text>
      <biological_entity id="o21848" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="85" name="quantity" src="d0_s6" to="110" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, meadows, open fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="open fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., Miss., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12c.</number>
  
</bio:treatment>