<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">194</other_info_on_meta>
    <other_info_on_meta type="treatment_page">198</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1834" rank="genus">townsendia</taxon_name>
    <taxon_name authority="Beaman" date="1957" rank="species">annua</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>183: 132, plate 22, fig. 4. 1957</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus townsendia;species annua</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067757</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 3–12 (–16+) cm.</text>
      <biological_entity id="o3642" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="16" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="12" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to erect;</text>
      <biological_entity id="o3643" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes (1–) 3–12 (–18+) mm, strigose.</text>
      <biological_entity id="o3644" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="18" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly cauline, blades spatulate to narrowly oblanceolate, 8–12 (–25+) × 1–3 (–5+) mm, not fleshy, faces ± strigillose.</text>
      <biological_entity id="o3645" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3646" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o3647" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="narrowly oblanceolate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="25" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="5" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o3648" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads at tips of stems.</text>
      <biological_entity id="o3649" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o3650" name="tip" name_original="tips" src="d0_s4" type="structure" />
      <biological_entity id="o3651" name="stem" name_original="stems" src="d0_s4" type="structure" />
      <relation from="o3649" id="r333" name="at" negation="false" src="d0_s4" to="o3650" />
      <relation from="o3650" id="r334" name="part_of" negation="false" src="d0_s4" to="o3651" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± hemispheric, 8–12 (–16) mm diam.</text>
      <biological_entity id="o3652" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s5" to="16" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s5" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 26–30+ in 3+ series, the longer ± lanceovate to oblanceolate, 5–6+ mm (l/w = 2.5–5), apices acute, abaxial faces strigillose to glabrate.</text>
      <biological_entity id="o3653" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o3654" from="26" name="quantity" src="d0_s6" to="30" upper_restricted="false" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="longer" value_original="longer" />
        <character char_type="range_value" from="less lanceovate" name="shape" src="d0_s6" to="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o3654" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o3655" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3656" name="face" name_original="faces" src="d0_s6" type="structure">
        <character char_type="range_value" from="strigillose" name="pubescence" src="d0_s6" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 12–30+;</text>
      <biological_entity id="o3657" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s7" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas white or pinkish to lavender adaxially, laminae 5–8+ mm, glabrous abaxially.</text>
      <biological_entity id="o3658" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="pinkish" modifier="adaxially" name="coloration" src="d0_s8" to="lavender" />
      </biological_entity>
      <biological_entity id="o3659" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 60–120+;</text>
      <biological_entity id="o3660" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s9" to="120" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 2.5–3.5+ mm.</text>
      <biological_entity id="o3661" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2–3 mm, faces hairy, hair tips glochidiform;</text>
      <biological_entity id="o3662" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3663" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="hair" id="o3664" name="tip" name_original="tips" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="glochidiform" value_original="glochidiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi persistent;</text>
    </statement>
    <statement id="d0_s13">
      <text>on ray cypselae ± 10 lanceolate scales 0.2–0.5 (–1) mm;</text>
      <biological_entity id="o3665" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>on disc cypselae 20+ subulate scales 1–2.5 (–4) mm.</text>
      <biological_entity id="o3666" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s14" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity id="o3667" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3668" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soils, alkaline flats, red clays</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soils" />
        <character name="habitat" value="alkaline flats" />
        <character name="habitat" value="red clays" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Tex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>The type of Townsendia annua may prove to be conspecific with that of T. strigosa.</discussion>
  
</bio:treatment>