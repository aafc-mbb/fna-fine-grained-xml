<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Theodore M. Barkley†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">540</other_info_on_meta>
    <other_info_on_meta type="mention_page">542</other_info_on_meta>
    <other_info_on_meta type="treatment_page">613</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Cassini" date="1816" rank="genus">LIGULARIA</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Sci. Soc. Philom. Paris</publication_title>
      <place_in_publication>1816: 198. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus LIGULARIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin, ligula, little tongue, and -aria, pertaining to or possession of; alluding to corollas of radiate heads</other_info_on_name>
    <other_info_on_name type="fna_id">118542</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 15–150+ cm (glabrous or scattered-hairy, especially distally [arachno-tomentose]; roots fibrous).</text>
      <biological_entity id="o104" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect.</text>
      <biological_entity id="o105" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (petiole bases dilated, ± sheathing stems);</text>
      <biological_entity id="o106" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (basal and proximal largest, cauline smaller distally) palmately [palmati-pinnately] nerved, orbiculate to reniform [elliptic, lanceolate, oblanceolate, ovate], margins dentate [denticulate, serrate, dissected], faces glabrous or sparsely pilosulous (mostly on nerves) [glaucous; arachno-tomentose].</text>
      <biological_entity id="o107" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s5" value="nerved" value_original="nerved" />
        <character char_type="range_value" from="orbiculate" name="shape" src="d0_s5" to="reniform" />
      </biological_entity>
      <biological_entity id="o108" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o109" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pilosulous" value_original="pilosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate [discoid], in corymbiform [racemiform or spiciform] arrays.</text>
      <biological_entity id="o110" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o111" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o110" id="r10" name="in" negation="false" src="d0_s6" to="o111" />
    </statement>
    <statement id="d0_s7">
      <text>Calyculi 0 [1–2+ bractlets].</text>
      <biological_entity id="o112" name="calyculus" name_original="calyculi" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres cylindric to campanulate, [3–] 16–28 mm diam.</text>
      <biological_entity id="o113" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s8" to="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s8" to="16" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="diameter" src="d0_s8" to="28" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries persistent, [5–] 8–13+ in 1–2 series, erect, distinct (margins interlocking) [connate at bases], mostly oblong or lanceolate to linear, subequal, margins usually ± scarious (tips greenish or reddish, not blackened).</text>
      <biological_entity id="o114" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s9" to="8" to_inclusive="false" />
        <character char_type="range_value" constraint="in series" constraintid="o115" from="8" name="quantity" src="d0_s9" to="13" upper_restricted="false" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="linear" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o115" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o116" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat to convex, smooth, epaleate.</text>
      <biological_entity id="o117" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s10" to="convex" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets [0 or 1–7] 8–14+, pistillate, fertile;</text>
      <biological_entity id="o118" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s11" to="14" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas (laminae prominent, showy) orange to orange-yellow or brick-red [yellow].</text>
      <biological_entity id="o119" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="orange" name="coloration" src="d0_s12" to="orange-yellow or brick-red" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets [5–] 12–100+, bisexual, fertile;</text>
      <biological_entity id="o120" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s13" to="12" to_inclusive="false" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s13" to="100" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas orange-yellow to orange, becoming brownish [yellow], tubes longer than cylindric throats, lobes 5, recurved, lance-linear;</text>
      <biological_entity id="o121" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="orange-yellow" name="coloration" src="d0_s14" to="orange" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s14" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o122" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than cylindric throats" constraintid="o123" is_modifier="false" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o123" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o124" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="lance-linear" value_original="lance-linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style-branches: stigmatic areas continuous, apices truncate or rounded-truncate.</text>
      <biological_entity id="o125" name="style-branch" name_original="style-branches" src="d0_s15" type="structure" />
      <biological_entity constraint="stigmatic" id="o126" name="area" name_original="areas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o127" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded-truncate" value_original="rounded-truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae (stramineous to brownish) ± ellipsoid [cylindric or fusiform], 5 [–10] -ribbed or nerved, glabrous;</text>
      <biological_entity id="o128" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="5[-10]-ribbed" value_original="5[-10]-ribbed" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi persistent (fragile), of 40–100+, reddish [sordid, brownish, purplish], barbellate to barbellulate bristles ([shorter than] longer than cypselae).</text>
      <biological_entity id="o130" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="40" is_modifier="true" name="quantity" src="d0_s17" to="100" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="barbellate" is_modifier="true" name="architecture" src="d0_s17" to="barbellulate" />
      </biological_entity>
      <relation from="o129" id="r11" name="consist_of" negation="false" src="d0_s17" to="o130" />
    </statement>
    <statement id="d0_s18">
      <text>x = 30.</text>
      <biological_entity id="o129" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" name="duration" src="d0_s17" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o131" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; temperate Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="temperate Eurasia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>225.</number>
  <other_name type="common_name">Ligularia</other_name>
  <discussion>Species 125+ (1 in the flora).</discussion>
  
</bio:treatment>