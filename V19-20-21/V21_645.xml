<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="treatment_page">264</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott ex de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">layia</taxon_name>
    <taxon_name authority="(de Candolle) Hooker &amp; Arnott" date="1839" rank="species">heterotricha</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Beechey Voy.,</publication_title>
      <place_in_publication>358. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus layia;species heterotricha</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067064</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Madaroglossa</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">heterotricha</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 694. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Madaroglossa;species heterotricha;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 13–90 cm (self-incompat-ible);</text>
    </statement>
    <statement id="d0_s1">
      <text>glandular, strongly apple or banana-scented.</text>
      <biological_entity id="o9798" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="strongly" name="odor" src="d0_s1" value="apple" value_original="apple" />
        <character is_modifier="false" name="odor" src="d0_s1" value="banana-scented" value_original="banana-scented" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems not purple-streaked.</text>
      <biological_entity id="o9799" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s2" value="purple-streaked" value_original="purple-streaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaf-blades elliptic to ovate, 10–120 mm, margins (basal leaves) entire or shallowly toothed.</text>
      <biological_entity id="o9800" name="blade-leaf" name_original="leaf-blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="distance" src="d0_s3" to="120" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9801" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres hemispheric, 7–12 × 6–13+ mm.</text>
      <biological_entity id="o9802" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="13" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 7–13, apices usually shorter than folded bases.</text>
      <biological_entity id="o9803" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s5" to="13" />
      </biological_entity>
      <biological_entity id="o9804" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character constraint="than folded bases" constraintid="o9805" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="usually shorter" value_original="usually shorter" />
      </biological_entity>
      <biological_entity id="o9805" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s5" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series between ray and disc-florets.</text>
      <biological_entity id="o9806" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity constraint="between ray and disc-floret" constraintid="o9808-o9809" id="o9807" name="series" name_original="series" src="d0_s6" type="structure" constraint_original="between  ray and  disc-floret, ">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o9808" name="ray" name_original="ray" src="d0_s6" type="structure" />
      <biological_entity id="o9809" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure" />
      <relation from="o9806" id="r677" name="in" negation="false" src="d0_s6" to="o9807" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 7–13;</text>
      <biological_entity id="o9810" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s7" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae white to cream, 5–24 mm.</text>
      <biological_entity id="o9811" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="cream" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 40–90+;</text>
      <biological_entity id="o9812" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s9" to="90" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 4–7 mm;</text>
      <biological_entity id="o9813" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers yellow to brownish.</text>
      <biological_entity id="o9814" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray cypselae usually glabrous, sometimes sparsely hairy.</text>
      <biological_entity constraint="ray" id="o9815" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc pappi 0, or (readily falling as units) of 14–20 white, ± equal bristles or setiform scales 3–6 mm, each proximally plumose, not adaxially woolly.</text>
      <biological_entity id="o9817" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="14" is_modifier="true" name="quantity" src="d0_s13" to="20" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s13" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o9818" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="setiform" value_original="setiform" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o9816" id="r678" name="consist_of" negation="false" src="d0_s13" to="o9817" />
      <relation from="o9816" id="r679" name="consist_of" negation="false" src="d0_s13" to="o9818" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 16.</text>
      <biological_entity constraint="disc" id="o9816" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="proximally" name="shape" notes="" src="d0_s13" value="plumose" value_original="plumose" />
        <character is_modifier="false" modifier="not adaxially" name="pubescence" src="d0_s13" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9819" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, meadows, openings in woodlands, on clayey or sandy, sometimes ± alkaline soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="openings" constraint="in woodlands" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="alkaline soils" modifier="sandy sometimes" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Layia heterotricha occurs in the South Coast Ranges, western Transverse Ranges, and Tehachapi Range. Molecular phylogenetic data have indicated that L. heterotricha is sister to all other members of Layia (B. G. Baldwin 1996). Weak, ± sterile artificial hybrids have been produced with other species of Layia (no natural hybrids have been reported; J. Clausen 1951).</discussion>
  
</bio:treatment>