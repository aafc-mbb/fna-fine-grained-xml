<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="treatment_page">213</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">bidens</taxon_name>
    <taxon_name authority="(Michaux) Britton" date="1893" rank="species">trichosperma</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>20: 281. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus bidens;species trichosperma</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066236</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coreopsis</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">trichosperma</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 139. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Coreopsis;species trichosperma;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bidens</taxon_name>
    <taxon_name authority="(Linnaeus) Britton" date="unknown" rank="species">coronata</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">brachyodonta</taxon_name>
    <taxon_hierarchy>genus Bidens;species coronata;variety brachyodonta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bidens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">coronata</taxon_name>
    <taxon_name authority="(A. Gray) Sherff" date="unknown" rank="variety">tenuiloba</taxon_name>
    <taxon_hierarchy>genus Bidens;species coronata;variety tenuiloba;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bidens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">coronata</taxon_name>
    <taxon_name authority="(Michaux) Fernald" date="unknown" rank="variety">trichosperma</taxon_name>
    <taxon_hierarchy>genus Bidens;species coronata;variety trichosperma;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals (biennials), (15–) 30–50 (–150+) cm.</text>
      <biological_entity id="o13133" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petioles 5–20+ mm;</text>
      <biological_entity id="o13134" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o13135" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s1" to="20" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades ± deltate to ovate overall, 40–75 (–150) × 10–45 (–130) mm, usually laciniately 1–2-pinnatisect, ultimate lobes (3–) 5–7+, oblanceolate or lanceolate to linear, (5–) 10–30 (–80+) × (1–) 4–8 (–12+) mm, bases cuneate, ultimate margins incised, dentate, serrate, or entire, little, if at all, ciliate, apices acute to attenuate, faces glabrous or ± hirtellous to strigillose.</text>
      <biological_entity id="o13136" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o13137" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="less deltate" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="150" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s2" to="75" to_unit="mm" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="130" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="45" to_unit="mm" />
        <character is_modifier="false" modifier="usually laciniately" name="architecture_or_shape" src="d0_s2" value="1-2-pinnatisect" value_original="1-2-pinnatisect" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o13138" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s2" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s2" to="7" upper_restricted="false" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s2" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="80" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s2" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="12" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13139" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o13140" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="incised" value_original="incised" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13141" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" modifier="at all , ciliate" name="shape" src="d0_s2" to="attenuate" />
      </biological_entity>
      <biological_entity id="o13142" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="more or less hirtellous" name="pubescence" src="d0_s2" to="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually in open, ± corymbiform arrays.</text>
      <biological_entity id="o13143" name="head" name_original="heads" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 20–150 mm.</text>
      <biological_entity id="o13144" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s4" to="150" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Calyculi of (6–) 8 (–11), ascending to spreading, spatulate to linear, sometimes ± foliaceous bractlets or bracts 3–10 (–18) mm, margins sometimes ciliate, abaxial faces glabrous.</text>
      <biological_entity id="o13145" name="calyculus" name_original="calyculi" src="d0_s5" type="structure" />
      <biological_entity id="o13146" name="bractlet" name_original="bractlets" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="atypical_quantity" src="d0_s5" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s5" to="11" />
        <character is_modifier="true" name="quantity" src="d0_s5" value="8" value_original="8" />
        <character char_type="range_value" from="ascending" is_modifier="true" name="orientation" src="d0_s5" to="spreading" />
        <character char_type="range_value" from="spatulate" is_modifier="true" name="shape" src="d0_s5" to="linear" />
        <character is_modifier="true" modifier="sometimes more or less" name="architecture" src="d0_s5" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o13147" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="18" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13148" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13149" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o13145" id="r897" name="consist_of" negation="false" src="d0_s5" to="o13146" />
      <relation from="o13145" id="r898" name="consist_of" negation="false" src="d0_s5" to="o13147" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres hemispheric or broader, 4–6 (–8) × 6–12 mm.</text>
      <biological_entity id="o13150" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s6" value="broader" value_original="broader" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 6–8 (–10), oblong, 3–8 mm.</text>
      <biological_entity id="o13151" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="10" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s7" to="8" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets (7–) 8–9;</text>
      <biological_entity id="o13152" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s8" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminae golden yellow, 10–30 mm.</text>
      <biological_entity id="o13153" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="golden yellow" value_original="golden yellow" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 40–60 (–80+);</text>
      <biological_entity id="o13154" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="80" upper_restricted="false" />
        <character char_type="range_value" from="40" name="quantity" src="d0_s10" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, 3–5 mm.</text>
      <biological_entity id="o13155" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae blackish or brown, flattened, narrowly cuneate, outer 3–6 mm, inner 5–9 mm (lengths mostly 2.5–4 times widths), margins antrorsely barbed or ciliate, apices ± truncate, faces obscurely 1-nerved, sometimes tuberculate, glabrous or sparsely hispidulous;</text>
      <biological_entity id="o13156" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity constraint="outer" id="o13157" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o13158" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13159" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="antrorsely" name="architecture" src="d0_s12" value="barbed" value_original="barbed" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o13160" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o13161" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s12" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" modifier="sometimes" name="relief" src="d0_s12" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 2 erect, ± patently barbed awns or scales (0.4–) 1–2.5 (–4) mm.</text>
      <biological_entity id="o13163" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="true" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="true" modifier="more or less patently" name="architecture_or_shape" src="d0_s13" value="barbed" value_original="barbed" />
      </biological_entity>
      <biological_entity id="o13164" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
      <relation from="o13162" id="r899" name="consist_of" negation="false" src="d0_s13" to="o13163" />
      <relation from="o13162" id="r900" name="consist_of" negation="false" src="d0_s13" to="o13164" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 24.</text>
      <biological_entity id="o13162" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o13165" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshes, estuaries</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshes" />
        <character name="habitat" value="estuaries" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Iowa, Ky., Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., S.Dak., Tenn., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13.</number>
  <discussion>Plants here called Bidens trichosperma have long been known as B. coronata (Linnaeus) Britton (or Britton ex Sherff). Alas, Britton’s B. coronata (1913) is a later homonym of B. coronata Fischer ex Colla (1834) and cannot be used.</discussion>
  
</bio:treatment>