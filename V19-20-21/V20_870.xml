<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="mention_page">382</other_info_on_meta>
    <other_info_on_meta type="treatment_page">381</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="(Cassini) Cassini in F. Cuvier" date="1820" rank="genus">eurybia</taxon_name>
    <taxon_name authority="(Chapman) G. L. Nesom" date="1995" rank="species">spinulosa</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 262. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus eurybia;species spinulosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066762</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Chapman" date="unknown" rank="species">spinulosus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. South. U.S.,</publication_title>
      <place_in_publication>199. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species spinulosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heleastrum</taxon_name>
    <taxon_name authority="(Chapman) Greene" date="unknown" rank="species">spinulosum</taxon_name>
    <taxon_hierarchy>genus Heleastrum;species spinulosum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–70 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>solitary or clumped, eglandular;</text>
      <biological_entity id="o15172" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="clumped" value_original="clumped" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizomes short and stout or elongate and wiry, or caudices.</text>
      <biological_entity id="o15173" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="wiry" value_original="wiry" />
      </biological_entity>
      <biological_entity id="o15174" name="caudex" name_original="caudices" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems 1 (–3+), erect, simple, ± villous to glabrescent.</text>
      <biological_entity id="o15175" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="3" upper_restricted="false" />
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="less villous" name="pubescence" src="d0_s3" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves strongly basal and cauline, linear, firm, ± fleshy, margins indurate, ± revolute, entire to spinose-serrate, smooth to remotely scabridulous or ciliate, spines indurate, finely parallel-veined with evident midribs, apices acute, revolute-indurate, faces glabrescent (minute hairs bulbous at base, threadlike distally);</text>
      <biological_entity id="o15176" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="strongly" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="texture" src="d0_s4" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s4" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o15177" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="indurate" value_original="indurate" />
        <character is_modifier="false" modifier="more or less" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s4" value="entire to spinose-serrate" value_original="entire to spinose-serrate" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s4" to="remotely scabridulous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15178" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="indurate" value_original="indurate" />
        <character constraint="with midribs" constraintid="o15179" is_modifier="false" modifier="finely" name="architecture" src="d0_s4" value="parallel-veined" value_original="parallel-veined" />
      </biological_entity>
      <biological_entity id="o15179" name="midrib" name_original="midribs" src="d0_s4" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s4" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o15180" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="texture" src="d0_s4" value="revolute-indurate" value_original="revolute-indurate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal and proximal cauline persistent, sessile or petiolate (narrowing between bases and blades), blades lance-linear to linear, 100–300 × (1–) 2–5 mm, bases ± marcescent, sheathing, ciliate;</text>
      <biological_entity id="o15181" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o15182" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o15183" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lance-linear" name="arrangement_or_course_or_shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s5" to="300" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s5" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15184" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="condition" src="d0_s5" value="marcescent" value_original="marcescent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="sheathing" value_original="sheathing" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline sessile, blades linear to lance-linear, 20–95 × 3–5 mm, progressively reduced distally, bases rounded to auriculate-clasping, adaxial faces sparsely villous in distal, the distal subtending heads boatshaped.</text>
      <biological_entity constraint="cauline" id="o15185" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o15186" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="arrangement_or_course_or_shape" src="d0_s6" to="lance-linear" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s6" to="95" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o15187" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s6" value="auriculate-clasping" value_original="auriculate-clasping" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15188" name="face" name_original="faces" src="d0_s6" type="structure">
        <character constraint="in distal blades" constraintid="o15189" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
        <character is_modifier="false" name="position_or_shape" notes="" src="d0_s6" value="distal" value_original="distal" />
        <character is_modifier="false" name="shape" src="d0_s6" value="boat-shaped" value_original="boat-shaped" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15189" name="blade" name_original="blades" src="d0_s6" type="structure" />
      <biological_entity id="o15190" name="head" name_original="heads" src="d0_s6" type="structure" />
      <relation from="o15188" id="r1386" name="subtending" negation="false" src="d0_s6" to="o15190" />
    </statement>
    <statement id="d0_s7">
      <text>Heads 3–16+ in spiciform to narrow, racemiform arrays.</text>
      <biological_entity id="o15191" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in spiciform to narrow , racemiform arrays" from="3" name="quantity" src="d0_s7" to="16" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 0 (usually) or ascending, 1–17+ mm, sparsely villosulous;</text>
      <biological_entity id="o15192" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="17" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="villosulous" value_original="villosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 0–2, ascending, lanceolate, bases not indurate, rounded (boatshaped), margins ciliate, faces glabrous.</text>
      <biological_entity id="o15193" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="2" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o15194" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s9" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o15195" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15196" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres campanulate, 6.5–9.7 mm, shorter than pappi.</text>
      <biological_entity id="o15197" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s10" to="9.7" to_unit="mm" />
        <character constraint="than pappi" constraintid="o15198" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o15198" name="pappus" name_original="pappi" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries 20–40 in 4–5 series, green, often ± involute in distal 1/2–2/3 (outer) to 1/3 (inner), densely nerved (nerves not thickened), lanceolate, unequal, coriaceous, bases indurate, rounded (outer), margins entire, indurate (outer) or scarious and often purplish (inner), sparsely ciliate, apices acute to acuminate, indurate, apiculate, adaxial faces glabrous or sparsely villosulous.</text>
      <biological_entity id="o15199" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o15200" from="20" name="quantity" src="d0_s11" to="40" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s11" value="green" value_original="green" />
        <character constraint="in distal 1/2-2/3-1/3" constraintid="o15201" is_modifier="false" modifier="often more or less" name="shape_or_vernation" src="d0_s11" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="densely" name="architecture" notes="" src="d0_s11" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="texture" src="d0_s11" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o15200" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15201" name="1/2-2/3-1/3" name_original="1/2-2/3-1/3" src="d0_s11" type="structure" />
      <biological_entity id="o15202" name="base" name_original="bases" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o15203" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="texture" src="d0_s11" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15204" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
        <character is_modifier="false" name="texture" src="d0_s11" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15205" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="villosulous" value_original="villosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 8–17;</text>
      <biological_entity id="o15206" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s12" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>laminae pale-purple to purplish white, 10–16 (–20) × 1–1.8 mm.</text>
      <biological_entity id="o15207" name="lamina" name_original="laminae" src="d0_s13" type="structure">
        <character char_type="range_value" from="pale-purple" name="coloration" src="d0_s13" to="purplish white" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s13" to="16" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Disc-florets 18–30;</text>
      <biological_entity id="o15208" name="disc-floret" name_original="disc-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s14" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas yellow, 5.5–7.6 mm, barely ampliate, tubes much shorter than tubular-funnelform throats (1–2 mm), lobes erect, lanceolate, 0.65–1 mm.</text>
      <biological_entity id="o15209" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s15" to="7.6" to_unit="mm" />
        <character is_modifier="false" modifier="barely" name="size" src="d0_s15" value="ampliate" value_original="ampliate" />
      </biological_entity>
      <biological_entity id="o15210" name="tube" name_original="tubes" src="d0_s15" type="structure">
        <character constraint="than tubular-funnelform throats" constraintid="o15211" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o15211" name="throat" name_original="throats" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="tubular-funnelform" value_original="tubular-funnelform" />
      </biological_entity>
      <biological_entity id="o15212" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.65" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae brown to gray-brown, fusiform, ± compressed, 2–2.5 mm, ribs 7–10, faces ± strigillose;</text>
      <biological_entity id="o15213" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s16" to="gray-brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s16" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15214" name="rib" name_original="ribs" src="d0_s16" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s16" to="10" />
      </biological_entity>
      <biological_entity id="o15215" name="face" name_original="faces" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s16" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of burnt-orange (coarse, sometimes apically clavellate) bristles 6–7.5 mm, as long as or slightly longer than disc corollas.</text>
      <biological_entity id="o15216" name="pappus" name_original="pappi" src="d0_s17" type="structure" constraint="bristle" constraint_original="bristle; bristle">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s17" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15217" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s17" value="burnt-orange" value_original="burnt-orange" />
      </biological_entity>
      <biological_entity constraint="disc" id="o15219" name="corolla" name_original="corollas" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s17" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="disc" id="o15218" name="corolla" name_original="corollas" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s17" value="longer" value_original="longer" />
      </biological_entity>
      <relation from="o15216" id="r1387" name="part_of" negation="false" src="d0_s17" to="o15217" />
      <relation from="o15216" id="r1388" name="as long as" negation="false" src="d0_s17" to="o15219" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to dry, acid sandy peats, savannas in long-leaf pinelands, fire-maintained</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist to dry" />
        <character name="habitat" value="moist to acid sandy peats" />
        <character name="habitat" value="savannas" constraint="in long-leaf pinelands , fire-maintained" />
        <character name="habitat" value="long-leaf pinelands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Apalachicola aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eurybia spinulosa is known only from the Apalachicola River drainage of the Florida panhandle; it is of conservation concern in Florida and is a facultative wetland indicator. Much of its habitat has now been lost to development (R. Kral 1983, vol. 2). Kral published a map of the species.</discussion>
  
</bio:treatment>