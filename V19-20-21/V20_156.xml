<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">82</other_info_on_meta>
    <other_info_on_meta type="treatment_page">83</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">ionactis</taxon_name>
    <taxon_name authority="(Linnaeus) Greene" date="1897" rank="species">linariifolia</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 245. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ionactis;species linariifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067004</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">linariifolius</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 874. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species linariifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">linariifolius</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">victorinii</taxon_name>
    <taxon_hierarchy>genus Aster;species linariifolius;variety victorinii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–50 (–70) cm (commonly cespitose; rhizomes compact, crownlike, woody, fibrous-rooted).</text>
      <biological_entity id="o20666" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally herbaceous or slightly woody, eglandular.</text>
      <biological_entity id="o20667" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves separated by evident internodes;</text>
      <biological_entity id="o20668" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="by internodes" constraintid="o20669" is_modifier="false" name="arrangement" src="d0_s2" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o20669" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s2" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades uniform, linear to narrowly oblong or oblanceolate, 12–40 mm, margins green, faces glabrous, eglandular.</text>
      <biological_entity id="o20670" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="variability" src="d0_s3" value="uniform" value_original="uniform" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s3" to="narrowly oblong or oblanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s3" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20671" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o20672" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads usually in loose, corymbiform arrays, sometimes borne singly.</text>
      <biological_entity id="o20673" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement" notes="" src="d0_s4" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o20674" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o20673" id="r1917" name="in" negation="false" src="d0_s4" to="o20674" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres 6–9 mm.</text>
      <biological_entity id="o20675" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets bisexual, fertile;</text>
      <biological_entity id="o20676" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s6" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s6" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas (4.5–) 5–7 mm.</text>
      <biological_entity id="o20677" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae (2.5–) 3.5–4 mm, eglandular.</text>
    </statement>
    <statement id="d0_s9">
      <text>2n = 18.</text>
      <biological_entity id="o20678" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20679" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Sep" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy habitats, often seeps or other moist sites, commonly in longleaf pine communities along Gulf Coast, or inland sites of rocky hills, ridges, bluffs, sometimes in clay, in oak pine woods, sandy cracks and ledges of acid rocks in stream falls or rapids, open jackpine stands on sand</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy habitats" />
        <character name="habitat" value="other moist sites" modifier="often seeps or" />
        <character name="habitat" value="longleaf pine communities" modifier="commonly in" constraint="along gulf coast" />
        <character name="habitat" value="gulf coast" />
        <character name="habitat" value="sites" modifier="inland" constraint="of rocky hills" />
        <character name="habitat" value="rocky hills" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="clay" modifier="sometimes in" />
        <character name="habitat" value="oak pine woods" modifier="in" />
        <character name="habitat" value="sandy cracks" constraint="of acid rocks" />
        <character name="habitat" value="ledges" constraint="of acid rocks" />
        <character name="habitat" value="acid rocks" />
        <character name="habitat" value="rapids" modifier="falls" />
        <character name="habitat" value="stream" />
        <character name="habitat" value="open jackpine" />
        <character name="habitat" value="sand" modifier="stands on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>5–800(–900) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="5" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="900" to_unit="m" from="5" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Que.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Flax-leaf ankle-aster</other_name>
  <other_name type="common_name">flaxleaf whitetop or aster</other_name>
  <other_name type="common_name">aster à feuilles de linaires</other_name>
  <discussion>Ionactis linariifolia was noted by M. L. Fernald (1950) to occur in “s. Minn.”; G. B. Ownbey and T. Morley (1991) did not include it for Minnesota.</discussion>
  
</bio:treatment>