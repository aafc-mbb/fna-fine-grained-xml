<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">272</other_info_on_meta>
    <other_info_on_meta type="treatment_page">309</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1990" rank="species">heliographis</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>69: 230. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species heliographis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066609</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 1.5–4.5 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudex branches relatively thick, short, retaining old leaf-bases.</text>
      <biological_entity id="o7089" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s0" to="4.5" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o7090" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o7091" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
      </biological_entity>
      <relation from="o7090" id="r641" name="retaining" negation="false" src="d0_s1" to="o7091" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, usually sparsely strigose, eglandular.</text>
      <biological_entity id="o7092" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o7093" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades linear to barely linear-oblanceolate, 10–20 (–40) × 0.5–0.9 mm, cauline slightly reduced distally, margins entire, faces sparsely strigose or glabrous, eglandular.</text>
      <biological_entity constraint="basal" id="o7094" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="barely linear-oblanceolate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o7095" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="slightly; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o7096" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7097" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 (–3).</text>
      <biological_entity id="o7098" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="3" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 3–4 × 5–6 mm.</text>
      <biological_entity id="o7099" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 3–4 series (purplish), sparsely strigose, little, if at all, minutely glandular.</text>
      <biological_entity id="o7100" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s7" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="at all; minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o7101" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o7100" id="r642" name="in" negation="false" src="d0_s7" to="o7101" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 20–23;</text>
      <biological_entity id="o7102" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="23" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas laminae white, drying white to lavender, 5–8 mm, laminae reflexing.</text>
      <biological_entity constraint="corollas" id="o7103" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="drying" value_original="drying" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="lavender" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7104" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3–3.5 mm.</text>
      <biological_entity constraint="disc" id="o7105" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae ca. 1.2 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o7106" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="1.2" value_original="1.2" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o7107" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 16–21 bristles.</text>
      <biological_entity id="o7108" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity id="o7109" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o7110" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="16" is_modifier="true" name="quantity" src="d0_s12" to="21" />
      </biological_entity>
      <relation from="o7108" id="r643" name="outer of" negation="false" src="d0_s12" to="o7109" />
      <relation from="o7108" id="r644" name="inner of" negation="false" src="d0_s12" to="o7110" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Boulder and cliff crevices, oak, pine-fir, mixed conifer</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="boulder" />
        <character name="habitat" value="cliff crevices" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pine-fir" />
        <character name="habitat" value="mixed conifer" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2500–3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="2500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>90.</number>
  <other_name type="common_name">Heliograph Peak fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>