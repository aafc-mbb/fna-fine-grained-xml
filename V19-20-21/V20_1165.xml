<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">476</other_info_on_meta>
    <other_info_on_meta type="mention_page">501</other_info_on_meta>
    <other_info_on_meta type="mention_page">525</other_info_on_meta>
    <other_info_on_meta type="treatment_page">524</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">symphyotrichum</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) G. L. Nesom" date="1995" rank="species">elliottii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 280. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section symphyotrichum;species elliottii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067641</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">elliottii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 140. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species elliottii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 60–160 (–200) cm, colonial;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-rhizomatous.</text>
      <biological_entity id="o1124" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="160" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="160" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="long-rhizomatous" value_original="long-rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1, erect (straight, stout, sometimes purplish-pink), mostly glabrous, hirsutulous in arrays, mainly in lines.</text>
      <biological_entity id="o1125" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="in arrays" constraintid="o1126" is_modifier="false" name="pubescence" src="d0_s2" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
      <biological_entity id="o1126" name="array" name_original="arrays" src="d0_s2" type="structure" />
      <biological_entity id="o1127" name="line" name_original="lines" src="d0_s2" type="structure" />
      <relation from="o1125" id="r101" modifier="mainly" name="in" negation="false" src="d0_s2" to="o1127" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves firm, margins serrate to serrulate, scabridulous, apices acute to shortly acuminate, apiculate to mucronate, abaxial faces glabrous, adaxial scabridulous;</text>
      <biological_entity id="o1128" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o1129" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="serrate" name="architecture_or_shape" src="d0_s3" to="serrulate" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o1130" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="shortly acuminate apiculate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="shortly acuminate apiculate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1131" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1132" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal withering by flowering, long-petiolate (petioles narrowly winged, sheathing), blades elliptic, 50–250 × 10–50 mm, bases attenuate;</text>
      <biological_entity constraint="basal" id="o1133" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by blades, bases" constraintid="o1134, o1135" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o1134" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="long-petiolate" value_original="long-petiolate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="250" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="50" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o1135" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="long-petiolate" value_original="long-petiolate" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s4" to="250" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="50" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline withering by flowering, long-petiolate to subpetiolate (petioles winged, bases expanded, sheathing), blades elliptic to lanceolate or oblanceolate, gradually reduced distally, 70–110 × 15–35 mm, bases attenuate;</text>
      <biological_entity constraint="proximal cauline" id="o1136" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="by blades, bases" constraintid="o1137, o1138" is_modifier="false" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o1137" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="long-petiolate" is_modifier="true" name="architecture" src="d0_s5" to="subpetiolate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="lanceolate or oblanceolate" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="70" from_unit="mm" name="length" src="d0_s5" to="110" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s5" to="35" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o1138" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="long-petiolate" is_modifier="true" name="architecture" src="d0_s5" to="subpetiolate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="lanceolate or oblanceolate" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="70" from_unit="mm" name="length" src="d0_s5" to="110" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s5" to="35" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal subpetiolate or sessile, (petioles broadly winged, bases sheathing to clasping), blades elliptic, 10–70 × 1–30 mm, progressively reduced distally, more strongly so on branches, bases attenuate to cuneate, clasping, margins serrulate or entire.</text>
      <biological_entity constraint="distal" id="o1139" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="subpetiolate" value_original="subpetiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o1140" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s6" to="70" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="progressively; distally" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o1141" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <biological_entity id="o1142" name="base" name_original="bases" src="d0_s6" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s6" to="cuneate" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s6" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o1143" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o1140" id="r102" modifier="strongly" name="on" negation="false" src="d0_s6" to="o1141" />
    </statement>
    <statement id="d0_s7">
      <text>Heads in paniculiform arrays, branches strongly ascending, leafy.</text>
      <biological_entity id="o1144" name="head" name_original="heads" src="d0_s7" type="structure" />
      <biological_entity id="o1145" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o1146" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="leafy" value_original="leafy" />
      </biological_entity>
      <relation from="o1144" id="r103" name="in" negation="false" src="d0_s7" to="o1145" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 0.5–2.5 cm, to erect, hirsute, bracts 3–5, linear, grading into phyllaries.</text>
      <biological_entity id="o1147" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o1148" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="5" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o1149" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o1148" id="r104" name="into" negation="false" src="d0_s8" to="o1149" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres narrowly campanulate, 8–11 mm.</text>
      <biological_entity id="o1150" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 5–6 series, linear-lanceolate or oblanceolate to linear, slightly unequal, bases indurate 1/3–1/2, margins narrowly scarious, hyaline, sparsely and remotely ciliolate, green zones lanceolate to linear, outer distally foliaceous, sometimes constricted in middle, apices spreading to squarrose, long-acuminate, apiculate, faces glabrous.</text>
      <biological_entity id="o1151" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" notes="" src="d0_s10" to="linear" />
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o1152" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
      <biological_entity id="o1153" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
        <character char_type="range_value" from="1/3" name="quantity" src="d0_s10" to="1/2" />
      </biological_entity>
      <biological_entity id="o1154" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="sparsely; remotely" name="pubescence" src="d0_s10" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o1155" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="linear" />
      </biological_entity>
      <biological_entity constraint="outer" id="o1156" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s10" value="foliaceous" value_original="foliaceous" />
        <character constraint="in middle phyllaries" constraintid="o1157" is_modifier="false" modifier="sometimes" name="size" src="d0_s10" value="constricted" value_original="constricted" />
      </biological_entity>
      <biological_entity constraint="middle" id="o1157" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure" />
      <biological_entity id="o1158" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s10" to="squarrose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="long-acuminate" value_original="long-acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="apiculate" value_original="apiculate" />
      </biological_entity>
      <biological_entity id="o1159" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o1151" id="r105" name="in" negation="false" src="d0_s10" to="o1152" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets (25–) 30–46+;</text>
      <biological_entity id="o1160" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" name="atypical_quantity" src="d0_s11" to="30" to_inclusive="false" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s11" to="46" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas pink or sometimes lavender, laminae 7–14 × 0.8–1.6 mm.</text>
      <biological_entity id="o1161" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="sometimes" name="coloration_or_odor" src="d0_s12" value="lavender" value_original="lavender" />
      </biological_entity>
      <biological_entity id="o1162" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s12" to="14" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 25–35+;</text>
      <biological_entity id="o1163" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s13" to="35" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas pale-yellow becoming pink and later brown, 6–6.2 mm, tubes slightly shorter than narrowly funnelform limbs, lobes narrowly triangular to lanceolate, 0.8–1 mm.</text>
      <biological_entity id="o1164" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s14" value="pale-yellow becoming pink" value_original="pale-yellow becoming pink" />
        <character is_modifier="false" modifier="later" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="6.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1165" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than narrowly funnelform limbs" constraintid="o1166" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o1166" name="limb" name_original="limbs" src="d0_s14" type="structure" />
      <biological_entity id="o1167" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="narrowly triangular" name="shape" src="d0_s14" to="lanceolate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae tan to pale-brown, oblanceoloid, compressed, 2–2.7 mm, 3–4-nerved, faces glabrous or sparsely hairy;</text>
      <biological_entity id="o1168" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s15" to="pale-brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblanceoloid" value_original="oblanceoloid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-4-nerved" value_original="3-4-nerved" />
      </biological_entity>
      <biological_entity id="o1169" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi sordid or whitish, 5.5–5.8 mm. 2n = 16.</text>
      <biological_entity id="o1170" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="sordid" value_original="sordid" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1171" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist, low sites, swamps, bogs, marshes, brackish marshes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" />
        <character name="habitat" value="low sites" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="marshes" modifier="brackish" />
        <character name="habitat" value="brackish" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>55.</number>
  <other_name type="common_name">Elliott’s aster</other_name>
  <discussion>Symphyotrichum elliottii grows on the Atlantic coastal plain. It is of conservation concern in some states.</discussion>
  
</bio:treatment>