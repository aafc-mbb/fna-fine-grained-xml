<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">394</other_info_on_meta>
    <other_info_on_meta type="treatment_page">400</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Gaertner" date="1791" rank="genus">antennaria</taxon_name>
    <taxon_name authority="(Hooker) Greene" date="1897" rank="species">pulcherrima</taxon_name>
    <taxon_name authority="(Fernald &amp; Wiegand) R. J. Bayer" date="2004" rank="subspecies">eucosma</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 768. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus antennaria;species pulcherrima;subspecies eucosma</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068034</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antennaria</taxon_name>
    <taxon_name authority="Fernald &amp; Wiegand" date="unknown" rank="species">eucosma</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>13: 23. 1911</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Antennaria;species eucosma;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antennaria</taxon_name>
    <taxon_name authority="(Wahlenberg) Hooker" date="unknown" rank="species">carpatica</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="variety">humilis</taxon_name>
    <taxon_hierarchy>genus Antennaria;species carpatica;variety humilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 8–25 cm.</text>
      <biological_entity id="o1082" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves lanceolate to oblanceolate, 50–170 × 5–18 mm, faces silvery sericeous.</text>
      <biological_entity constraint="basal" id="o1083" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s1" to="oblanceolate" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s1" to="170" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s1" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1084" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="silvery" value_original="silvery" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves 7–30 mm, mostly not flagged (sometimes flagged near heads).</text>
      <biological_entity constraint="cauline" id="o1085" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s2" to="30" to_unit="mm" />
        <character is_modifier="false" modifier="mostly not" name="architecture" src="d0_s2" value="flagged" value_original="flagged" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 3–15.</text>
      <biological_entity id="o1086" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres: staminate 5.5–7 mm;</text>
      <biological_entity id="o1087" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pistillate 7–10 mm.</text>
      <biological_entity id="o1088" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries distally light-brown or castaneous.</text>
      <biological_entity id="o1089" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s6" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="castaneous" value_original="castaneous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Corollas: staminate 3–4 mm;</text>
      <biological_entity id="o1090" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pistillate 3–4.3 mm.</text>
      <biological_entity id="o1091" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 1.2–1.5 mm;</text>
      <biological_entity id="o1092" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi: staminate 4–6 mm;</text>
      <biological_entity id="o1093" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillate 5–7 mm. 2n = 56.</text>
      <biological_entity id="o1094" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1095" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="56" value_original="56" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone barrens, serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nfld. and Labr. (Nfld.), Que. (Anticosti Island).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" value="Que. (Anticosti Island)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10b.</number>
  <other_name type="common_name">Elegant pussytoes</other_name>
  <discussion>Subspecies eucosma is known from limestone barrens in western Newfoundland and Anticosti Island (K. M. Urbanska 1983). It is morphologically similar to subsp. pulcherrima; the two are separated mainly by the presence of prominent flags on cauline leaves in subsp. pulcherrima and their absence in subsp. eucosma.</discussion>
  
</bio:treatment>