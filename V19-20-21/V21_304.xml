<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">123</other_info_on_meta>
    <other_info_on_meta type="treatment_page">125</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Rohr" date="1792" rank="genus">melanthera</taxon_name>
    <taxon_name authority="Small" date="1903" rank="species">parvifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1251, 1340. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus melanthera;species parvifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067176</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Melanthera</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">radiata</taxon_name>
    <taxon_hierarchy>genus Melanthera;species radiata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–60 cm.</text>
      <biological_entity id="o24209" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sprawling to weakly erect, strigose to hirsute.</text>
      <biological_entity id="o24210" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="sprawling" name="orientation" src="d0_s1" to="weakly erect" />
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s1" to="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades ovate or 3-lobed, 1.5–4 × 1–1.5 cm, bases broadly ± cuneate, hastate, margins coarsely serrate (sometimes undulate), faces strigose to hispid.</text>
      <biological_entity id="o24211" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24212" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly more or less" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="hastate" value_original="hastate" />
      </biological_entity>
      <biological_entity id="o24213" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o24214" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="strigose" name="pubescence" src="d0_s2" to="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads borne singly.</text>
      <biological_entity id="o24215" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles 2.5–10 cm, hispid to strigose.</text>
      <biological_entity id="o24216" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="hispid" name="pubescence" src="d0_s4" to="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres 10–14 mm diam.</text>
      <biological_entity id="o24217" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s5" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries lanceolate, 5–7 × 1.5–3 mm.</text>
      <biological_entity id="o24218" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Paleae 6–7 × 2 mm, apical mucros 1–1.5 mm, usually recurved.</text>
      <biological_entity id="o24219" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="apical" id="o24220" name="mucro" name_original="mucros" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s7" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 25–40;</text>
      <biological_entity id="o24221" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s8" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 5 mm;</text>
      <biological_entity id="o24222" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anther sacs 1.5 mm.</text>
      <biological_entity constraint="anther" id="o24223" name="sac" name_original="sacs" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2.5–3 × 1.5–2 mm. 2n = 30.</text>
      <biological_entity id="o24224" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24225" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Apr (perhaps year-round).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="perhaps" to="Apr" from="Feb" />
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Old coral reefs or porous oölitic rocks in open pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="old coral reefs" />
        <character name="habitat" value="porous oölitic rocks" />
        <character name="habitat" value="open pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Melanthera parvifolia is known only from southern Florida, including Big Pine Key.</discussion>
  
</bio:treatment>