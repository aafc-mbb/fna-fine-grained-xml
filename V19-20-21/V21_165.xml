<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="treatment_page">75</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">chrysogonum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">virginianum</taxon_name>
    <taxon_name authority="(Alexander ex Small) H. E. Ahles" date="1964" rank="variety">australe</taxon_name>
    <place_of_publication>
      <publication_title>J. Elisha Mitchell Sci. Soc.</publication_title>
      <place_in_publication>80: 173. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus chrysogonum;species virginianum;variety australe</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068147</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysogonum</taxon_name>
    <taxon_name authority="Alexander ex Small" date="unknown" rank="species">australe</taxon_name>
    <place_of_publication>
      <publication_title>Man. S.E. Fl.,</publication_title>
      <place_in_publication>1415, 1509. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysogonum;species australe;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants stoloniferous (colonial, mat-forming; longest stolon internodes 12–60 cm).</text>
      <biological_entity id="o14139" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (flowering): leafless, 2–10 cm.</text>
      <biological_entity id="o14140" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="leafless" value_original="leafless" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades elliptic-ovate, bases gradually cuneate, faces villoso-hirsute (hairs spreading, 1–3 mm).</text>
      <biological_entity id="o14141" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic-ovate" value_original="elliptic-ovate" />
      </biological_entity>
      <biological_entity id="o14142" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o14143" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villoso-hirsute" value_original="villoso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 2–10 cm.</text>
      <biological_entity id="o14144" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Outer phyllaries 7–10 × 2.5–4 mm.</text>
      <biological_entity constraint="outer" id="o14145" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Mar–Apr(–May).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="mid Mar" />
        <character name="flowering time" char_type="atypical_range" to="May" from="mid Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pine-oak, longleaf pine, beech-oak-magnolia woods, ravine slopes, limestone outcrops, flood plains and terraces, sand or sandy loam</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="beech-oak-magnolia woods" modifier="pine-oak longleaf pine" />
        <character name="habitat" value="ravine slopes" />
        <character name="habitat" value="limestone outcrops" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="terraces" />
        <character name="habitat" value="sand" />
        <character name="habitat" value="sandy loam" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Fla., Ga., La., Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1c.</number>
  
</bio:treatment>