<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">411</other_info_on_meta>
    <other_info_on_meta type="treatment_page">413</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chaenactis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">chaenactis</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">glabriuscula</taxon_name>
    <taxon_name authority="(Greene) H. M. Hall" date="1907" rank="variety">orcuttiana</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>3: 192. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus chaenactis;section chaenactis;species glabriuscula;variety orcuttiana</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068143</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chaenactis</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">tenuifolia</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="variety">orcuttiana</taxon_name>
    <place_of_publication>
      <publication_title>W. Amer. Sci.</publication_title>
      <place_in_publication>3: 157. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chaenactis;species tenuifolia;variety orcuttiana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–30 cm;</text>
      <biological_entity id="o7213" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal indument grayish, sparsely arachnoid.</text>
      <biological_entity constraint="proximal" id="o7214" name="indument" name_original="indument" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="grayish" value_original="grayish" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems mostly 3–7 (–12), ± horizontal;</text>
      <biological_entity id="o7215" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="12" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="7" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s2" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches proximal and distal.</text>
      <biological_entity id="o7216" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal (withering) and cauline, mostly 2–5 cm;</text>
      <biological_entity id="o7217" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="2" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>largest blades ± plane to 3-dimensional, succulent, mostly 2-pinnately lobed;</text>
      <biological_entity constraint="largest" id="o7218" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="plane" value_original="plane" />
        <character is_modifier="false" name="texture" src="d0_s5" value="succulent" value_original="succulent" />
        <character is_modifier="false" modifier="mostly 2-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>primary lobes 2–7 pairs, ± congested, ultimate lobes ± plane to involute.</text>
      <biological_entity constraint="primary" id="o7219" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="7" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_arrangement" src="d0_s6" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o7220" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character char_type="range_value" from="less plane" name="shape" src="d0_s6" to="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads mostly 2–5 per stem.</text>
      <biological_entity id="o7221" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per stem" constraintid="o7222" from="2" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <biological_entity id="o7222" name="stem" name_original="stem" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles 1–4 cm.</text>
      <biological_entity id="o7223" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres ± hemispheric.</text>
      <biological_entity id="o7224" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries: longest 4.5–6.5 (–9) × 1–2 mm;</text>
      <biological_entity id="o7225" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="length" src="d0_s10" value="longest" value_original="longest" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s10" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>outer predominantly stipitate-glandular and ± villous in fruit.</text>
      <biological_entity id="o7226" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure" />
      <biological_entity constraint="outer" id="o7227" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="predominantly" name="pubescence" src="d0_s11" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character constraint="in fruit" constraintid="o7228" is_modifier="false" modifier="more or less" name="pubescence" src="d0_s11" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o7228" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Florets: inner corollas 4.5–5.5 mm.</text>
      <biological_entity id="o7229" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <biological_entity constraint="inner" id="o7230" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s12" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 4–5 mm;</text>
      <biological_entity id="o7231" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 4 scales in 1 series, longest scales mostly 1–2.5 mm, lengths 0.2–0.5 times corollas.</text>
      <biological_entity id="o7232" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o7233" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o7234" name="series" name_original="series" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o7236" name="corolla" name_original="corollas" src="d0_s14" type="structure" />
      <relation from="o7232" id="r518" name="consist_of" negation="false" src="d0_s14" to="o7233" />
      <relation from="o7233" id="r519" name="in" negation="false" src="d0_s14" to="o7234" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 12.</text>
      <biological_entity constraint="longest" id="o7235" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character constraint="corolla" constraintid="o7236" is_modifier="false" name="length" src="d0_s14" value="0.2-0.5 times corollas" value_original="0.2-0.5 times corollas" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7237" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="late Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal dunes and bluffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal dunes" />
        <character name="habitat" value="bluffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15e.</number>
  <other_name type="common_name">Orcutt’s pincushion</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety orcuttiana is known along the coast from near Santa Barbara to northwestern Baja California. It appears to intergrade with var. glabriuscula shortly inland.</discussion>
  
</bio:treatment>