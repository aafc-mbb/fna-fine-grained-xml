<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">479</other_info_on_meta>
    <other_info_on_meta type="treatment_page">480</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="(Nuttall) Semple in J. C. Semple et al." date="2002" rank="subgenus">astropolium</taxon_name>
    <taxon_name authority="(Linnaeus) G. L. Nesom" date="1995" rank="species">tenuifolium</taxon_name>
    <taxon_name authority="(R. W. Long) S. D. Sundberg" date="2004" rank="variety">aphyllum</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 905. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus astropolium;species tenuifolium;variety aphyllum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068866</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">tenuifolius</taxon_name>
    <taxon_name authority="R. W. Long" date="unknown" rank="variety">aphyllus</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>72: 40. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species tenuifolius;variety aphyllus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Britton" date="unknown" rank="species">bracei</taxon_name>
    <taxon_hierarchy>genus Aster;species bracei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Symphyotrichum</taxon_name>
    <taxon_name authority="(Britton) G. L. Nesom" date="unknown" rank="species">bracei</taxon_name>
    <taxon_hierarchy>genus Symphyotrichum;species bracei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (20–) 30–80 (–90) cm, clustered;</text>
    </statement>
    <statement id="d0_s1">
      <text>short-rhizomatous.</text>
      <biological_entity id="o16575" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="short-rhizomatous" value_original="short-rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5+.</text>
      <biological_entity id="o16576" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Mid leaves (1–) 1.5–2.7 mm wide.</text>
      <biological_entity constraint="mid" id="o16577" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 4.1–5 (–5.3) mm.</text>
      <biological_entity id="o16578" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="5.3" to_unit="mm" />
        <character char_type="range_value" from="4.1" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries lanceolate to subulate.</text>
      <biological_entity id="o16579" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 10–16.</text>
      <biological_entity id="o16580" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets (10–) 13–23;</text>
      <biological_entity id="o16581" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s7" to="13" to_inclusive="false" />
        <character char_type="range_value" from="13" name="quantity" src="d0_s7" to="23" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas 3.4–4.2 (–4.6) mm.</text>
      <biological_entity id="o16582" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="4.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s8" to="4.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 1.5–2 (–2.5) mm;</text>
      <biological_entity id="o16583" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi 3–4.4 mm. 2n = 10.</text>
      <biological_entity id="o16584" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16585" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Dec(–Feb).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Aug" />
        <character name="flowering time" char_type="atypical_range" to="Feb" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal salt marshes, brackish marshes, low pine woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal salt marshes" />
        <character name="habitat" value="brackish marshes" />
        <character name="habitat" value="low pine woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies (Bahamas, Cuba).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies (Bahamas)" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2b.</number>
  <other_name type="common_name">Brace’s aster</other_name>
  
</bio:treatment>