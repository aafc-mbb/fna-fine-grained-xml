<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James D. Morefield</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">28</other_info_on_meta>
    <other_info_on_meta type="mention_page">385</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">444</other_info_on_meta>
    <other_info_on_meta type="mention_page">448</other_info_on_meta>
    <other_info_on_meta type="mention_page">461</other_info_on_meta>
    <other_info_on_meta type="treatment_page">447</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linny Heagy</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Loefling in C. Linnaeus" date="unknown" rank="genus">FILAGO</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 927, 1199. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 397. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus FILAGO</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin filum, thread, and - ago, possessing or resembling, alluding to abundant cottony indument</other_info_on_name>
    <other_info_on_name type="fna_id">112780</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (1–) 5–40 cm.</text>
      <biological_entity id="o10161" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems [0] 1, ± erect, or 2–7 [–10+], ± ascending [prostrate].</text>
      <biological_entity id="o10162" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="atypical_quantity" src="d0_s1" value="0" value_original="0" />
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="10" upper_restricted="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s1" to="7" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline [basal];</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
      <biological_entity id="o10163" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades lanceolate to oblanceolate [spatulate or ± round].</text>
      <biological_entity id="o10164" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in (dense, spheric [hemispheric]) glomerules of [2–] 8–35+ in ± dichasiiform arrays [borne singly].</text>
      <biological_entity id="o10165" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o10166" name="glomerule" name_original="glomerules" src="d0_s5" type="structure" />
      <biological_entity id="o10167" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s5" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s5" to="35" upper_restricted="false" />
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s5" value="dichasiiform" value_original="dichasiiform" />
      </biological_entity>
      <relation from="o10165" id="r945" name="in" negation="false" src="d0_s5" to="o10166" />
      <relation from="o10166" id="r946" name="part_of" negation="false" src="d0_s5" to="o10167" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres 0 or inconspicuous.</text>
      <biological_entity id="o10168" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s6" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries usually 0, rarely 1–4, unequal (similar to paleae).</text>
      <biological_entity id="o10169" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" modifier="rarely" name="quantity" src="d0_s7" to="4" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles cylindric to clavate (heights [2–] 5–15 times diams.), glabrous.</text>
      <biological_entity id="o10170" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s8" to="clavate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pistillate paleae (except usually innermost) ± persistent [falling], ± erect to ascending;</text>
      <biological_entity id="o10171" name="palea" name_original="paleae" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="more or less" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="less erect" name="orientation" src="d0_s9" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>bodies with 5+ nerves (nerves ± parallel, obscure), lanceolate to ovate, open to ± folded (each at most enfolding, not enclosing a floret);</text>
      <biological_entity id="o10172" name="body" name_original="bodies" src="d0_s10" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s10" to="ovate" />
        <character char_type="range_value" from="open" name="architecture" src="d0_s10" to="more or less folded" />
      </biological_entity>
      <biological_entity id="o10173" name="nerve" name_original="nerves" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" upper_restricted="false" />
      </biological_entity>
      <relation from="o10172" id="r947" name="with" negation="false" src="d0_s10" to="o10173" />
    </statement>
    <statement id="d0_s11">
      <text>wings erect to recurved (apices acuminate to aristate).</text>
      <biological_entity id="o10174" name="wing" name_original="wings" src="d0_s11" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s11" to="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Innermost paleae usually all pistillate, in some species bisexual and pistillate, persistent or tardily falling, usually 5, erect to ascending [spreading] (scarcely enlarged) in fruit, shorter than other pistillate paleae;</text>
      <biological_entity constraint="innermost" id="o10175" name="palea" name_original="paleae" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="tardily" name="life_cycle" src="d0_s12" value="falling" value_original="falling" />
        <character modifier="usually" name="quantity" src="d0_s12" value="5" value_original="5" />
        <character char_type="range_value" constraint="in fruit" constraintid="o10177" from="erect" name="orientation" src="d0_s12" to="ascending" />
        <character constraint="than other pistillate paleae" constraintid="o10178" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o10176" name="species" name_original="species" src="d0_s12" type="taxon_name">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o10177" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
      <biological_entity id="o10178" name="palea" name_original="paleae" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o10175" id="r948" name="in" negation="false" src="d0_s12" to="o10176" />
    </statement>
    <statement id="d0_s13">
      <text>bodies lanceolate to ovate.</text>
      <biological_entity id="o10179" name="body" name_original="bodies" src="d0_s13" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s13" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate florets [12–] 27–40+.</text>
      <biological_entity id="o10180" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="12" name="atypical_quantity" src="d0_s14" to="27" to_inclusive="false" />
        <character char_type="range_value" from="27" name="quantity" src="d0_s14" to="40" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Functionally staminate florets 0.</text>
      <biological_entity id="o10181" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Bisexual florets (1–) 2–9 (–11);</text>
      <biological_entity id="o10182" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s16" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s16" to="2" to_inclusive="false" />
        <character char_type="range_value" from="9" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="11" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s16" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>corolla lobes 4, ± equal.</text>
      <biological_entity constraint="corolla" id="o10183" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="4" value_original="4" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s17" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Cypselae brown, ± monomorphic: terete to ± compressed, cylindric to ± obovoid, usually straight, not gibbous, faces papillate to muricate [glabrous, smooth], dull;</text>
      <biological_entity id="o10184" name="cypsela" name_original="cypselae" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s18" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s18" to="more or less compressed cylindric" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s18" to="more or less compressed cylindric" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s18" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s18" value="gibbous" value_original="gibbous" />
      </biological_entity>
      <biological_entity id="o10185" name="face" name_original="faces" src="d0_s18" type="structure">
        <character char_type="range_value" from="papillate" name="relief" src="d0_s18" to="muricate" />
        <character is_modifier="false" name="reflectance" src="d0_s18" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>corolla scars apical [subapical];</text>
      <biological_entity id="o10186" name="cypsela" name_original="cypselae" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s19" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o10187" name="scar" name_original="scars" src="d0_s19" type="structure">
        <character is_modifier="false" name="position" src="d0_s19" value="apical" value_original="apical" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>pappi: outer pistillate 0, inner pistillate and bisexual of [3–] 13–21 bristles (visible in heads).</text>
      <biological_entity id="o10188" name="pappus" name_original="pappi" src="d0_s20" type="structure" />
      <biological_entity constraint="outer" id="o10189" name="pappus" name_original="pappi" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
        <character name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o10191" name="bristle" name_original="bristles" src="d0_s20" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s20" to="13" to_inclusive="false" />
        <character char_type="range_value" from="13" is_modifier="true" name="quantity" src="d0_s20" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>x = 14.</text>
      <biological_entity constraint="inner" id="o10190" name="pappus" name_original="pappi" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
        <character constraint="of bristles" constraintid="o10191" is_modifier="false" name="reproduction" src="d0_s20" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity constraint="x" id="o10192" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, w Asia, n Africa, Atlantic Islands, introduced in North America, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="w Asia" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands" establishment_means="introduced" />
        <character name="distribution" value="in North America" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>98.</number>
  <other_name type="common_name">Herba impia</other_name>
  <other_name type="common_name">cotonnière</other_name>
  <other_name type="common_name">cottonrose</other_name>
  <discussion>Species 12(–23) (2 in the flora).</discussion>
  <discussion>See discussion of Filagininae following the tribal description (p. 385).</discussion>
  <discussion>The name Filago has been used also for the genus now usually recognized as Evax Gaertner. Here Filago, in the narrow sense, contains twelve Old World species. Six species long included in Filago in North America are here separated as Logfia.</discussion>
  <discussion>Filago species grow in open, dry or somewhat moist habitats of arid, semiarid, Mediterranean, and humid-temperate to subtropical climates. The species in the flora grow in disturbed habitats; neither appears to be aggressively weedy.</discussion>
  <discussion>Filago appears to be sister to or derived from Logfia and is probably ancestral to Evacopsis and Evax (J. D. Morefield 1992). Filago is most easily recognized by outer epappose florets subtended by open or ± folded, persistent, acuminate to aristate paleae, and prominent pappi on inner pistillate and bisexual florets.</discussion>
  <references>
    <reference>Wagenitz, G. 1969. Abgrenzung und Gliederung der Gattung Filago L. s.l. (Compositae–Inuleae). Willdenowia 5: 395–444.</reference>
    <reference>Wagenitz, G. 1976. Two species of the “Filago germanica” group (Compositae–Inuleae) in the United States. Sida 6: 221–223.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Largest leaves oblong to lanceolate, widest in proximal 2/3; heads in glomerules of (15–)20–35+, narrowly ± ampulliform, largest 1.5–2 mm diam.; pistillate paleae spirally ranked, longest 3.5–4.2 mm, innermost surrounding 14–25+ florets (10–20+ pistillate); longest dis- tal capitular leaves 0.8–1.1 times head heights, acute</description>
      <determination>1 Filago vulgaris</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Largest leaves oblanceolate to narrowly spatulate, widest in distal 1/3; heads in glomerules of mostly (8–)12–16(–20), ± bipyramidal, largest 2.5–4 mm diam.; pistillate paleae vertically ranked, longest 4.5–6 mm, innermost surrounding 8–13 florets (2–7 pistillate); longest dis- tal capitular leaves 1.3–2 times head heights, obtuse</description>
      <determination>2 Filago pyramidata</determination>
    </key_statement>
  </key>
</bio:treatment>