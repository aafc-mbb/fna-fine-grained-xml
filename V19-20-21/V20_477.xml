<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="treatment_page">216</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="(Nuttall) Elliott" date="1823" rank="genus">chrysopsis</taxon_name>
    <taxon_name authority="Delaney &amp; Wunderlin" date="2002" rank="species">highlandsensis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Explor.</publication_title>
      <place_in_publication>2: 2, figs. 1–3, 5, 7, 9, 11. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chrysopsis;species highlandsensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066342</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials (weak or monocarpic), (30–) 60–110 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted (new rosettes from bases of previous season’s stems or as side branches of primary unbolted rosettes).</text>
      <biological_entity id="o9104" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="110" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–8 (–20), usually ascending to erect, moderately spreading near base, rarely decumbent, frequently branched (densely leafy), sparsely lanate, stipitate-glandular distally.</text>
      <biological_entity id="o9105" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="20" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="8" />
        <character char_type="range_value" from="usually ascending" name="orientation" src="d0_s2" to="erect" />
        <character constraint="near base" constraintid="o9106" is_modifier="false" modifier="moderately" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="rarely" name="growth_form_or_orientation" notes="" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="frequently" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="lanate" value_original="lanate" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o9106" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal blades usually spatulate to oblanceolate, rarely obovate, (35–) 55–120 (–150) × 15–20 (–30) mm, bases narrowly attenuate-cuneate, margins entire or apically serrate-denticulate, faces loosely long-lanate, obscurely stipitate-glandular, viscid;</text>
      <biological_entity id="o9107" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o9108" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually spatulate" name="shape" src="d0_s3" to="oblanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="35" from_unit="mm" name="atypical_length" src="d0_s3" to="55" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="150" to_unit="mm" />
        <character char_type="range_value" from="55" from_unit="mm" name="length" src="d0_s3" to="120" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9109" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="attenuate-cuneate" value_original="attenuate-cuneate" />
      </biological_entity>
      <biological_entity id="o9110" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s3" value="serrate-denticulate" value_original="serrate-denticulate" />
      </biological_entity>
      <biological_entity id="o9111" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s3" value="long-lanate" value_original="long-lanate" />
        <character is_modifier="false" modifier="obscurely" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="coating" src="d0_s3" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline sessile, blades usually oblong to oblongelliptic, sometimes ovate to lanceolate, bases usually slightly auriculate-clasping, truncate to rounded, margins undulate, some cilia 2–3 mm, faces moderately woolly-lanulate, stipitate-glandular;</text>
      <biological_entity id="o9112" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o9113" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o9114" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually oblong" name="shape" src="d0_s4" to="oblongelliptic" />
        <character char_type="range_value" from="ovate" modifier="sometimes" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o9115" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually slightly" name="architecture_or_fixation" src="d0_s4" value="auriculate-clasping" value_original="auriculate-clasping" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="rounded" />
      </biological_entity>
      <biological_entity id="o9116" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o9117" name="cilium" name_original="cilia" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9118" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s4" value="woolly-lanulate" value_original="woolly-lanulate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>mid to distal leaves ascending, often appressed, apices usually obtuse, sometimes subacute, mucronate or mucronulate, faces stipitate-glandular, distalmost glabrate to sparsely woolly-pilose, densely stipitae-glandular.</text>
      <biological_entity id="o9119" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o9120" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="often" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o9121" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s5" value="subacute" value_original="subacute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
      <biological_entity id="o9122" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o9123" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s5" to="sparsely woolly-pilose" />
        <character is_modifier="false" modifier="densely" name="architecture_or_function_or_pubescence" src="d0_s5" value="stipitae-glandular" value_original="stipitae-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 10–50, in compact corymbiform to paniculiform arrays (height usually less than 1/5 plants, branches slender, ascending, stipitate-glandular, sweet camphor smelling).</text>
      <biological_entity id="o9124" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s6" to="50" />
      </biological_entity>
      <biological_entity id="o9125" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="compact" value_original="compact" />
        <character char_type="range_value" from="corymbiform" is_modifier="true" name="architecture" src="d0_s6" to="paniculiform" />
      </biological_entity>
      <relation from="o9124" id="r839" name="in" negation="false" src="d0_s6" to="o9125" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres (yellow-green in bud) cylindro-campanulate, 6–8.5 mm.</text>
      <biological_entity id="o9126" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 5–6 series, erect, linear-lanceolate, unequal, 0.7–1.1 mm wide, apices usually acute-acuminate, sometimes acute-aristate, sometimes inner obtuse to rounded, densely stipitate-glandular, viscid.</text>
      <biological_entity id="o9127" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s8" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9128" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity id="o9129" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s8" value="acute-acuminate" value_original="acute-acuminate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="acute-aristate" value_original="acute-aristate" />
      </biological_entity>
      <biological_entity id="o9130" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s8" value="inner" value_original="inner" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="rounded" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="coating" src="d0_s8" value="viscid" value_original="viscid" />
      </biological_entity>
      <relation from="o9127" id="r840" name="in" negation="false" src="d0_s8" to="o9128" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 16–22;</text>
      <biological_entity id="o9131" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s9" to="22" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae 8–9.5 × 1–2 mm.</text>
      <biological_entity id="o9132" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 20–40;</text>
      <biological_entity id="o9133" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s11" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 4.5–5.5 mm, lobes 0.6–1 mm.</text>
      <biological_entity id="o9134" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s12" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9135" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 2–2.7 mm, without ridges, shallowly ribbed or smooth, moderately long-strigose;</text>
      <biological_entity id="o9136" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.7" to_unit="mm" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" notes="" src="d0_s13" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s13" value="long-strigose" value_original="long-strigose" />
      </biological_entity>
      <biological_entity id="o9137" name="ridge" name_original="ridges" src="d0_s13" type="structure" />
      <relation from="o9136" id="r841" name="without" negation="false" src="d0_s13" to="o9137" />
    </statement>
    <statement id="d0_s14">
      <text>pappi in 3–4 series, outer of linear scales 0.5–1.5 mm, inner of 30–35 bristles 5–6 mm, inner moderately clavate.</text>
      <biological_entity id="o9138" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o9139" name="series" name_original="series" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s14" to="4" />
      </biological_entity>
      <biological_entity constraint="outer" id="o9140" name="series" name_original="series" src="d0_s14" type="structure" />
      <biological_entity id="o9141" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o9142" name="series" name_original="series" src="d0_s14" type="structure" />
      <biological_entity id="o9143" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s14" to="35" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o9138" id="r842" name="in" negation="false" src="d0_s14" to="o9139" />
      <relation from="o9140" id="r843" name="consist_of" negation="false" src="d0_s14" to="o9141" />
      <relation from="o9142" id="r844" name="consist_of" negation="false" src="d0_s14" to="o9143" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 10.</text>
      <biological_entity constraint="inner" id="o9144" name="series" name_original="series" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="moderately" name="shape" src="d0_s14" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9145" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Oct–mid Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Dec" from="late Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sand pine scrub, scrubby flatwoods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="scrub" modifier="pine" />
        <character name="habitat" value="scrubby flatwoods" />
        <character name="habitat" value="sand" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Highlands goldenaster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Chrysopsis highlandsensis is known from Highlands, southern Polk, and northern Glades counties.</discussion>
  
</bio:treatment>