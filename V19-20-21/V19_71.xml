<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="treatment_page">118</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(Linnaeus) Michaux" date="1803" rank="species">virginianum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 90. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species virginianum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066405</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carduus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">virginianus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 824. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Carduus;species virginianus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carduus</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">revolutus</taxon_name>
    <taxon_hierarchy>genus Carduus;species revolutus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cirsium</taxon_name>
    <taxon_name authority="(Small) Petrak" date="unknown" rank="species">revolutum</taxon_name>
    <taxon_hierarchy>genus Cirsium;species revolutum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials, 60–150 (–200) cm;</text>
      <biological_entity id="o3297" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="150" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="150" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>crown sprouts from cluster of fibrous-roots, these often tuberous-thickened.</text>
      <biological_entity constraint="fibrou-root" id="o3299" name="sprout" name_original="sprouts" src="d0_s1" type="structure" constraint_original="fibrou-root crown; fibrou-root">
        <character is_modifier="false" modifier="often" name="size_or_width" src="d0_s1" value="tuberous-thickened" value_original="tuberous-thickened" />
      </biological_entity>
      <biological_entity id="o3300" name="fibrou-root" name_original="fibrous-roots" src="d0_s1" type="structure" />
      <relation from="o3299" id="r328" modifier="from cluster" name="part_of" negation="false" src="d0_s1" to="o3300" />
    </statement>
    <statement id="d0_s2">
      <text>Stems usually single, erect, thinly appressed-tomentose, ± glabrate in age;</text>
      <biological_entity id="o3301" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s2" value="appressed-tomentose" value_original="appressed-tomentose" />
        <character constraint="in age" constraintid="o3302" is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o3302" name="age" name_original="age" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>branches 0–few in distal 1/3, ascending.</text>
      <biological_entity id="o3303" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="in distal 1/3" constraintid="o3304" from="0" is_modifier="false" name="quantity" src="d0_s3" to="few" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3304" name="1/3" name_original="1/3" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves very numerous, firm-textured, blades 3–15 cm, thick, ± rigid, linear or linear-elliptic, 0.5–2 cm wide and spinulose, or narrowly ovate, 2–4 cm wide, deeply lobed, lobes remote, spreading, separated by broad sinuses, few toothed or lobed, margins often revolute, main spines slender, 3–5 (–9) mm, abaxial faces white-tomentose, adaxial green, glabrous or thinly tomentose;</text>
      <biological_entity id="o3305" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="very" name="quantity" src="d0_s4" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="texture" src="d0_s4" value="firm-textured" value_original="firm-textured" />
      </biological_entity>
      <biological_entity id="o3306" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="linear-elliptic" value_original="linear-elliptic" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o3307" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_density" src="d0_s4" value="remote" value_original="remote" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character constraint="by sinuses" constraintid="o3308" is_modifier="false" name="arrangement" src="d0_s4" value="separated" value_original="separated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="few" value_original="few" />
        <character is_modifier="false" name="shape" src="d0_s4" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o3308" name="sinuse" name_original="sinuses" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o3309" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
      <biological_entity constraint="main" id="o3310" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o3311" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o3312" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal usually absent at flowering, winged-petiolate;</text>
      <biological_entity constraint="basal" id="o3313" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal cauline usually absent at flowering, well separated, winged-petiolate;</text>
    </statement>
    <statement id="d0_s7">
      <text>middle and distal numerous (30–70+), sessile, well distributed, gradually reduced distally, bases tapered, not decurrent;</text>
      <biological_entity constraint="proximal cauline" id="o3314" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="usually" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s6" value="separated" value_original="separated" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="winged-petiolate" value_original="winged-petiolate" />
        <character is_modifier="false" name="position" src="d0_s7" value="middle" value_original="middle" />
      </biological_entity>
      <biological_entity id="o3315" name="whole-organism" name_original="" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s7" value="distal" value_original="distal" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="numerous" value_original="numerous" />
        <character char_type="range_value" from="30" name="atypical_quantity" src="d0_s7" to="70" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s7" value="distributed" value_original="distributed" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>distal linear, entire or few lobed, ca. 1 cm.</text>
      <biological_entity id="o3316" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="position_or_shape" src="d0_s8" value="distal" value_original="distal" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s8" value="few" value_original="few" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
        <character name="some_measurement" src="d0_s8" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Heads 1–10+ in open, corymbiform or paniculiform arrays.</text>
      <biological_entity id="o3317" name="head" name_original="heads" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o3318" from="1" name="quantity" src="d0_s9" to="10" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o3318" name="array" name_original="arrays" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Peduncles 10–15 cm (not overtopped by distal leaves).</text>
      <biological_entity id="o3319" name="peduncle" name_original="peduncles" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s10" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Involucres ovoid to cylindric or narrowly campanulate, 1.7–2.4 × 1–2 cm, glabrous or outer phyllaries very thinly tomentose.</text>
      <biological_entity id="o3320" name="involucre" name_original="involucres" src="d0_s11" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s11" to="cylindric or narrowly campanulate" />
        <character char_type="range_value" from="1.7" from_unit="cm" name="length" src="d0_s11" to="2.4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s11" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="outer" id="o3321" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="very thinly" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Phyllaries in 8–13 series, strongly imbricate, light green to brownish with dark apices, ovate (outer) to narrowly linear-elliptic (inner), abaxial faces with evident, narrow glutinous ridge;</text>
      <biological_entity id="o3322" name="phyllary" name_original="phyllaries" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" notes="" src="d0_s12" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" constraint="with apices" constraintid="o3324" from="light green" name="coloration" src="d0_s12" to="brownish" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s12" to="narrowly linear-elliptic" />
      </biological_entity>
      <biological_entity id="o3323" name="series" name_original="series" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s12" to="13" />
      </biological_entity>
      <biological_entity id="o3324" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s12" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o3326" name="ridge" name_original="ridge" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="evident" value_original="evident" />
        <character is_modifier="true" name="size_or_width" src="d0_s12" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coating" src="d0_s12" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o3322" id="r329" name="in" negation="false" src="d0_s12" to="o3323" />
      <relation from="o3325" id="r330" name="with" negation="false" src="d0_s12" to="o3326" />
    </statement>
    <statement id="d0_s13">
      <text>outer and middle appressed, bodies entire, apices erect or spreading, muticous to short-spinose, spines ascending to spreading, weak, 1–2 mm;</text>
      <biological_entity constraint="abaxial" id="o3325" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity id="o3327" name="whole-organism" name_original="" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="middle" value_original="middle" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s13" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o3328" name="body" name_original="bodies" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3329" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="muticous" name="shape" src="d0_s13" to="short-spinose" />
      </biological_entity>
      <biological_entity id="o3330" name="spine" name_original="spines" src="d0_s13" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s13" to="spreading" />
        <character is_modifier="false" name="fragility" src="d0_s13" value="weak" value_original="weak" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>apices of inner all straight and entire or innermost ± flexuous, erose.</text>
      <biological_entity id="o3331" name="apex" name_original="apices" src="d0_s14" type="structure" constraint="straight; straight">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="false" name="position" src="d0_s14" value="innermost" value_original="innermost" />
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s14" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s14" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o3332" name="straight" name_original="straight" src="d0_s14" type="structure">
        <character is_modifier="true" name="position" src="d0_s14" value="inner" value_original="inner" />
      </biological_entity>
      <relation from="o3331" id="r331" name="part_of" negation="false" src="d0_s14" to="o3332" />
    </statement>
    <statement id="d0_s15">
      <text>Corollas purple, 21–26 mm, tubes 8.5–11 mm, throats 6–8 mm (noticeably wider than tubes), lobes 4–8 mm;</text>
      <biological_entity id="o3333" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s15" value="purple" value_original="purple" />
        <character char_type="range_value" from="21" from_unit="mm" name="some_measurement" src="d0_s15" to="26" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3334" name="tube" name_original="tubes" src="d0_s15" type="structure">
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s15" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3335" name="throat" name_original="throats" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s15" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3336" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>style tips 3.5–5 mm.</text>
      <biological_entity constraint="style" id="o3337" name="tip" name_original="tips" src="d0_s16" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Cypselae dark-brown, 4–5 mm, apical collars yellowish, 0.5–1;</text>
      <biological_entity id="o3338" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s17" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o3339" name="collar" name_original="collars" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="0.5" name="quantity" src="d0_s17" to="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi 17–20 mm. 2n = 28.</text>
      <biological_entity id="o3340" name="pappus" name_original="pappi" src="d0_s18" type="structure">
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s18" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3341" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall (Aug–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist savannas, pine barrens, coastal plain bogs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist savannas" />
        <character name="habitat" value="pine barrens" />
        <character name="habitat" value="coastal plain bogs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–150 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="150" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del., Fla., Ga., N.J., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Virginia thistle</other_name>
  <discussion>Cirsium virginianum occurs on the Atlantic coastal plain from Delaware to Florida.</discussion>
  
</bio:treatment>