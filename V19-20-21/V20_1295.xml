<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">571</other_info_on_meta>
    <other_info_on_meta type="treatment_page">582</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">packera</taxon_name>
    <taxon_name authority="(Greenman) J. F. Bain" date="1999" rank="species">contermina</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 457. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus packera;species contermina</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067238</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Greenman" date="unknown" rank="species">conterminus</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>3: 101. 1916</place_in_publication>
      <other_info_on_pub>based on S. lyallii Klatt, Ann. K. K. Naturhist. Hofmus. 9: 365. 1894 not Hooker f. 1852</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species conterminus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 4–10+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous and/or fibrous-rooted (mat-forming, bases ascending to erect, coarse).</text>
      <biological_entity id="o19424" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1 or 2–3, clustered, bases floccose-tomentose, leaf-axils tomentose, glabrous elsewhere.</text>
      <biological_entity id="o19425" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s2" to="3" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o19426" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="floccose-tomentose" value_original="floccose-tomentose" />
      </biological_entity>
      <biological_entity id="o19427" name="leaf-axil" name_original="leaf-axils" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="elsewhere" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (thick, fleshy) petiolate;</text>
      <biological_entity constraint="basal" id="o19428" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades usually ovate, oblong, or spatulate, sometimes sublyrate, 20–50+ × 20–40+ mm, bases tapering (to winged petioles) or abruptly contracted to subcordate (petioles narrow), margins crenate, coarsely serrate, or subentire.</text>
      <biological_entity id="o19429" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="sublyrate" value_original="sublyrate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="50" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o19430" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="false" modifier="abruptly" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o19431" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" value_original="subentire" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subentire" value_original="subentire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves (often cyanic) gradually reduced (sessile, not clasping; lanceolate to linear, usually irregularly and shallowly lobed, rarely entire).</text>
      <biological_entity constraint="cauline" id="o19432" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–2 (–5+).</text>
      <biological_entity id="o19433" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="5" upper_restricted="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles conspicuously bracteate, glabrous.</text>
      <biological_entity id="o19434" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi conspicuous (tips of bractlets often purple).</text>
      <biological_entity id="o19435" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 21, deep red or green (tips reddish), 8–12+ mm, white-tomentose proximally.</text>
      <biological_entity id="o19436" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="21" value_original="21" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s9" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 10–12;</text>
      <biological_entity id="o19437" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla laminae 8–14+ mm.</text>
      <biological_entity constraint="corolla" id="o19438" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="14" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 55–75+;</text>
      <biological_entity id="o19439" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="55" name="quantity" src="d0_s12" to="75" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corolla-tubes 2.5–3.5 mm, limbs 3–4 mm.</text>
      <biological_entity id="o19440" name="corolla-tube" name_original="corolla-tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="distance" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19441" name="limb" name_original="limbs" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1–1.25 mm, glabrous;</text>
      <biological_entity id="o19442" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 4–7 mm. 2n = 160+.</text>
      <biological_entity id="o19443" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19444" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character char_type="range_value" from="160" name="quantity" src="d0_s15" upper_restricted="false" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early Jul–late Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late Aug" from="early Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Subalpine or alpine, open areas, rocky slopes or ravines, moist tundra or snowbeds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" modifier="subalpine or alpine" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="moist tundra" />
        <character name="habitat" value="snowbeds" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Mont., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion>Packera contermina grows in rocky areas and produces relatively short rhizomes and abundant thin fibrous roots. In mesic sites, the rhizomes are more robust and the fibrous roots are fewer. This taxon has been treated as part of P. cymbalaria or P. subnuda. Morphologic and cytologic data lend support to its recognition at species rank.</discussion>
  
</bio:treatment>