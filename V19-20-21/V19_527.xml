<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">341</other_info_on_meta>
    <other_info_on_meta type="treatment_page">342</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="D. Don" date="1832" rank="genus">microseris</taxon_name>
    <taxon_name authority="(Hooker) Schultz-Bipontinus" date="1866" rank="species">laciniata</taxon_name>
    <taxon_name authority="K. L. Chambers" date="2004" rank="subspecies">siskiyouensis</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 195, figs. 1, 2A, C. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus microseris;species laciniata;subspecies siskiyouensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068599</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually branched.</text>
      <biological_entity id="o20678" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves linear to narrowly lanceolate, entire or pinnately lobed.</text>
      <biological_entity id="o20679" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s1" to="narrowly lanceolate entire or pinnately lobed" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s1" to="narrowly lanceolate entire or pinnately lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Outer phyllaries not spotted, linear to ovate-deltate, smallest 1–1.5 mm wide, apices acute to acuminate, abaxial faces usually scurfy-puberulent.</text>
      <biological_entity constraint="outer" id="o20680" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s2" value="spotted" value_original="spotted" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="ovate-deltate" />
        <character is_modifier="false" name="size" src="d0_s2" value="smallest" value_original="smallest" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20681" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20682" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="scurfy-puberulent" value_original="scurfy-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pappi of 9–24, white, glabrous, aristate scales 0.5–2 mm, aristae barbellate.</text>
      <biological_entity id="o20683" name="pappus" name_original="pappi" src="d0_s3" type="structure" />
      <biological_entity id="o20684" name="scale" name_original="scales" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s3" to="24" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character is_modifier="true" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="true" name="shape" src="d0_s3" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o20683" id="r1867" name="consist_of" negation="false" src="d0_s3" to="o20684" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 18.</text>
      <biological_entity id="o20685" name="arista" name_original="aristae" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20686" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May-–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Loams or gravelly or rocky soils, rarely serpentine-derived, hillsides and valley flats, open grassy sites and woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="loams" />
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="rocky soils" />
        <character name="habitat" value="serpentine-derived" modifier="rarely" />
        <character name="habitat" value="hillsides" />
        <character name="habitat" value="valley flats" />
        <character name="habitat" value="open grassy sites" />
        <character name="habitat" value="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3c.</number>
  <other_name type="common_name">Siskiyou silverpuffs</other_name>
  <discussion>Subspecies siskiyouensis is known only from the Klamath Mountains, principally in Del Norte and Siskiyou counties, California, and Curry, Josephine, and Jackson counties, Oregon. It is found in open, rocky sites as well as woods, mostly at middle elevations. In the Illinois River Valley, Oregon, where it occurs together with Microseris howellii, the two are ecologically distinct. Microseris howellii is always found on rocky, open serpentine substrates; subsp. siskiyouensis is in mixed evergreen woodlands on better developed loam soils (K. L. Chambers 2004b).</discussion>
  
</bio:treatment>