<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="treatment_page">190</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">coreopsis</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray in A. Gray et al." date="1884" rank="section">silphidium</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">latifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 137. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus coreopsis;section silphidium;species latifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066426</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 90–150 cm.</text>
      <biological_entity id="o20250" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="90" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades ovate, 6–15 (–20+) × 2–10+ cm.</text>
      <biological_entity id="o20251" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s1" to="20" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s1" to="10" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Peduncles 1–5 (–10) cm.</text>
      <biological_entity id="o20252" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Calyculi of 3–8+ linear bractlets 5–8 mm.</text>
      <biological_entity id="o20253" name="calyculus" name_original="calyculi" src="d0_s3" type="structure" />
      <biological_entity id="o20254" name="bractlet" name_original="bractlets" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="8" upper_restricted="false" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o20253" id="r1390" name="consist_of" negation="false" src="d0_s3" to="o20254" />
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 5, oblong, 8–12 mm.</text>
      <biological_entity id="o20255" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray laminae 10–18 mm.</text>
      <biological_entity constraint="ray" id="o20256" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc corollas 7–8 mm.</text>
      <biological_entity constraint="disc" id="o20257" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 7–8 mm. 2n = 26.</text>
      <biological_entity id="o20258" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20259" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded slopes in woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded slopes" constraint="in woods" />
        <character name="habitat" value="woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., N.C., S.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion>Coreopsis latifolia is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>