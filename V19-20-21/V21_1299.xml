<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">460</other_info_on_meta>
    <other_info_on_meta type="treatment_page">510</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="R. M. King &amp; H. Robinson" date="1966" rank="genus">PLEUROCORONIS</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>12: 468, fig. pp. 472, 473. 1966</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus PLEUROCORONIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pleura, side, and korone, crown, alluding to squamellae that appear to form a crown subtending bristles</other_info_on_name>
    <other_info_on_name type="fna_id">126005</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, (10–) 30–100 cm.</text>
      <biological_entity id="o9080" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, usually much branched.</text>
      <biological_entity id="o9082" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>mostly opposite (distal often alternate);</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o9083" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-nerved or 3-nerved, ovate, deltate, or rhombic to lanceolate, margins entire or toothed [lobed, 2-pinnatifid], faces usually stipitate-glandular.</text>
      <biological_entity id="o9084" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="rhombic" name="shape" src="d0_s5" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o9085" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o9086" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, borne singly or in loose, corymbiform arrays.</text>
      <biological_entity id="o9087" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o9088" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o9087" id="r628" name="in" negation="false" src="d0_s6" to="o9088" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± obconic, 4–6 mm diam.</text>
      <biological_entity id="o9089" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 30–35 in 3–4+ series, (herbaceous to chartaceous) 1-nerved or 3-nerved, ovate to lanceolate, unequal.</text>
      <biological_entity id="o9090" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o9091" from="30" name="quantity" src="d0_s8" to="35" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o9091" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 25–30;</text>
      <biological_entity id="o9092" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s9" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white, throats narrowly cylindric (lengths 4–6 times diams.);</text>
      <biological_entity id="o9093" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o9094" name="throat" name_original="throats" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles: bases not enlarged, glabrous, branches ± clavate.</text>
      <biological_entity id="o9095" name="style" name_original="styles" src="d0_s11" type="structure" />
      <biological_entity id="o9096" name="base" name_original="bases" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s11" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9097" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Receptacles flat or convex, epaleate.</text>
      <biological_entity id="o9098" name="receptacle" name_original="receptacles" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s12" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae prismatic, 4–5-ribbed, scabrellous;</text>
      <biological_entity id="o9099" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="4-5-ribbed" value_original="4-5-ribbed" />
        <character is_modifier="false" name="relief" src="d0_s13" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi persistent, of 3–6 (–12) subulate-aristate scales plus 3–6 (–12) coarse, barbellate bristles in 1 series.</text>
      <biological_entity id="o9100" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="relief" src="d0_s14" value="coarse" value_original="coarse" />
      </biological_entity>
      <biological_entity id="o9101" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s14" to="12" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s14" to="6" />
        <character is_modifier="true" name="shape" src="d0_s14" value="subulate-aristate" value_original="subulate-aristate" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="12" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="6" />
      </biological_entity>
      <biological_entity id="o9103" name="series" name_original="series" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <relation from="o9100" id="r629" name="consist_of" negation="false" src="d0_s14" to="o9101" />
      <relation from="o9102" id="r630" name="in" negation="false" src="d0_s14" to="o9103" />
    </statement>
    <statement id="d0_s15">
      <text>x = 9.</text>
      <biological_entity id="o9102" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity constraint="x" id="o9104" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w United States, nw Mexico</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w United States" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>408.</number>
  <discussion>Species 3 (1 in the flora).</discussion>
  <references>
    <reference>King, R. M. 1967. Studies in the Eupatorieae, (Compositae). I–III. Rhodora 69: 35–47.</reference>
  </references>
  
</bio:treatment>