<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">239</other_info_on_meta>
    <other_info_on_meta type="treatment_page">238</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(Nuttall) Shinners" date="1951" rank="species">sessiliflora</taxon_name>
    <taxon_name authority="(Greene) Semple" date="1993" rank="subspecies">fastigiata</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>73: 451. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species sessiliflora;subspecies fastigiata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068485</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">fastigiata</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 296. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysopsis;species fastigiata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="(Pursh) Nuttall ex de Candolle" date="unknown" rank="species">villosa</taxon_name>
    <taxon_name authority="(Greene) H. M. Hall" date="unknown" rank="variety">fastigiata</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;species villosa;variety fastigiata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heterotheca</taxon_name>
    <taxon_name authority="(Greene) V. L. Harms" date="unknown" rank="species">fastigiata</taxon_name>
    <taxon_hierarchy>genus Heterotheca;species fastigiata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heterotheca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sessiliflora</taxon_name>
    <taxon_name authority="Semple" date="unknown" rank="variety">sanjacintensis</taxon_name>
    <taxon_hierarchy>genus Heterotheca;species sessiliflora;variety sanjacintensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1–20, ascending to erect, (24–) 37–85 (–108) cm, densely strigose, becoming moderately hispido-strigose and densely stipitate-glandular distally.</text>
      <biological_entity id="o21032" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s0" to="20" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character char_type="range_value" from="24" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="37" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="85" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="108" to_unit="cm" />
        <character char_type="range_value" from="37" from_unit="cm" name="some_measurement" src="d0_s0" to="85" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="becoming moderately" name="pubescence" src="d0_s0" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves sessile, light green or silvery-whitish;</text>
      <biological_entity constraint="cauline" id="o21033" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="light green" value_original="light green" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="silvery-whitish" value_original="silvery-whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal oblanceolate 12–38 × (3–) 4.7–7 (–8.5) mm, margins entire, undulate, faces moderately to densely hispido-strigose, very sparsely to moderately stipitate-glandular;</text>
      <biological_entity constraint="proximal" id="o21034" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s2" to="38" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s2" to="4.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="4.7" from_unit="mm" name="width" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21035" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o21036" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s2" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="very sparsely; sparsely to moderately" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal lanceolate, 10–20 × 2–6 mm, reduced distally, stiff, bases rounded, margins usually strongly undulate, often more stipitate-glandular and less ciliate than faces, sparsely to extremely densely short-strigose, sparsely to densely glandular.</text>
      <biological_entity constraint="distal" id="o21037" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o21038" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o21039" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually strongly" name="shape" src="d0_s3" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character constraint="than faces" constraintid="o21040" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="more stipitate-glandular and less ciliate" />
        <character is_modifier="false" modifier="often; sparsely to extremely densely" name="pubescence" src="d0_s3" value="short-strigose" value_original="short-strigose" />
        <character is_modifier="false" modifier="sparsely to densely" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o21040" name="face" name_original="faces" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Peduncle bracts 3–5, leaflike, greatly reduced, none enlarged and subtending heads.</text>
      <biological_entity constraint="peduncle" id="o21041" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="5" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" modifier="greatly" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="none" name="size" src="d0_s4" value="enlarged" value_original="enlarged" />
      </biological_entity>
      <biological_entity id="o21042" name="head" name_original="heads" src="d0_s4" type="structure" />
      <relation from="o21041" id="r1949" name="subtending" negation="false" src="d0_s4" to="o21042" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres 7–10 (–12) mm.</text>
      <biological_entity id="o21043" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 4–14;</text>
      <biological_entity id="o21044" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="14" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 3.5–5.9 (–9) mm.</text>
      <biological_entity id="o21045" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="5.9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="9" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets: corolla lobes sparsely pilose, hairs 0.25–0.6 mm.</text>
      <biological_entity id="o21046" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure" />
      <biological_entity constraint="corolla" id="o21047" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o21048" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.25" from_unit="mm" name="some_measurement" src="d0_s8" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypsela faces sparsely strigose.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 18.</text>
      <biological_entity constraint="cypsela" id="o21049" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21050" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct(–Dec) (rarely spring).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="atypical_range" modifier="rarely" to="Oct" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Dec" from="Jul" />
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arroyos, embankments, in oak scrub and pine forests, rarely adventive in desert arroyos below mountains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="arroyos" />
        <character name="habitat" value="embankments" constraint="in oak scrub" />
        <character name="habitat" value="oak scrub" />
        <character name="habitat" value="pine forests" />
        <character name="habitat" value="desert arroyos" modifier="rarely adventive in" constraint="below mountains" />
        <character name="habitat" value="mountains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(100–)300–1800(–2200) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="300" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4b.</number>
  <discussion>Subspecies fastigiata is known from the San Gabriel, San Bernardino, and San Jacinto mountains, and from Mount Palomar. It is distinguished by its undulate leaf margins, heads not subtended by large peduncle bracts, and usually low number of ray florets. Two varieties have been recognized on the basis of the density of leaf hairs and glands. In var. fastigiata, the leaves appear silvery white because of the densely short-strigoso-canescent indument of tightly appressed, short hairs obscuring underlying glands; it occurs in the San Gabriel, San Bernardino, and Santa Monica mountains. In var. sanjacintensis, the leaves are sparsely to moderately hispido-strigose and are more densely stipitate-glandular, giving them a light green appearance; it is known from the San Jacinto Mountains and Mount Palomar, and is of conservation concern. Hybridization with subsp. echioides occurs at lower elevations throughout the range.</discussion>
  
</bio:treatment>