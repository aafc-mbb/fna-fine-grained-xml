<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">514</other_info_on_meta>
    <other_info_on_meta type="mention_page">515</other_info_on_meta>
    <other_info_on_meta type="mention_page">519</other_info_on_meta>
    <other_info_on_meta type="treatment_page">520</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">symphyotrichum</taxon_name>
    <taxon_name authority="(Willdenow) G. L. Nesom" date="1995" rank="species">lanceolatum</taxon_name>
    <taxon_name authority="(Wiegand) G. L. Nesom" date="1995" rank="variety">interior</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 284. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section symphyotrichum;species lanceolatum;variety interior</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068843</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Wiegand" date="unknown" rank="species">interior</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>35: 35. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species interior;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">lanceolatus</taxon_name>
    <taxon_name authority="(Wiegand) A. G. Jones" date="unknown" rank="subspecies">interior</taxon_name>
    <taxon_hierarchy>genus Aster;species lanceolatus;subspecies interior;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">simplex</taxon_name>
    <taxon_name authority="(Wiegand) Cronquist" date="unknown" rank="variety">interior</taxon_name>
    <taxon_hierarchy>genus Aster;species simplex;variety interior;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually slender, sometimes stout, glabrous or hairy in lines.</text>
      <biological_entity id="o6070" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="sometimes" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character constraint="in lines" constraintid="o6071" is_modifier="false" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o6071" name="line" name_original="lines" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades linear-lanceolate to lanceolate, distal cauline margins entire.</text>
      <biological_entity id="o6072" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s1" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="distal cauline" id="o6073" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads congested on lateral branches of arrays, often developing on short peduncles along most of the branch, usually not subtended by large foliaceous bracts.</text>
      <biological_entity id="o6074" name="head" name_original="heads" src="d0_s2" type="structure">
        <character constraint="on lateral branches" constraintid="o6075" is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="congested" value_original="congested" />
        <character constraint="on peduncles" constraintid="o6077" is_modifier="false" modifier="often" name="development" notes="" src="d0_s2" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o6075" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity id="o6076" name="array" name_original="arrays" src="d0_s2" type="structure" />
      <biological_entity id="o6077" name="peduncle" name_original="peduncles" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o6078" name="branch" name_original="branch" src="d0_s2" type="structure" />
      <biological_entity id="o6079" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="large" value_original="large" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <relation from="o6075" id="r555" name="part_of" negation="false" src="d0_s2" to="o6076" />
      <relation from="o6077" id="r556" modifier="along" name="part_of" negation="false" src="d0_s2" to="o6078" />
      <relation from="o6074" id="r557" modifier="usually not" name="subtended by" negation="true" src="d0_s2" to="o6079" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres 3–4 mm.</text>
      <biological_entity id="o6080" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries: outer 1.8–3.4 × ca. 0.4, inner 3.5–3.7 × 0.4–0.5 mm, outer 1/3–2/3 length of inner.</text>
      <biological_entity id="o6081" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure" />
      <biological_entity constraint="outer" id="o6082" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.8" name="quantity" src="d0_s4" to="3.4" />
        <character name="quantity" src="d0_s4" value="0.4" value_original="0.4" />
      </biological_entity>
      <biological_entity constraint="inner" id="o6083" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s4" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s4" value="outer" value_original="outer" />
        <character char_type="range_value" from="1/3 length of inner phyllaries" name="length" src="d0_s4" to="2/3 length of inner phyllaries" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 22–24;</text>
      <biological_entity id="o6084" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="22" name="quantity" src="d0_s5" to="24" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corollas white, laminae 3.9–5.8 mm.</text>
      <biological_entity id="o6085" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o6086" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.9" from_unit="mm" name="some_measurement" src="d0_s6" to="5.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 20–23;</text>
      <biological_entity id="o6087" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s7" to="23" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas 2.8–3.1 mm, limbs 1.6–1.8 mm, lobes 0.5–0.7 mm.</text>
      <biological_entity id="o6088" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s8" to="3.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6089" name="limb" name_original="limbs" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s8" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6090" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 0.6–0.8 mm;</text>
      <biological_entity id="o6091" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi 2.9–3.3 mm. 2n = 48, 64.</text>
      <biological_entity id="o6092" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s10" to="3.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6093" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="48" value_original="48" />
        <character name="quantity" src="d0_s10" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Bottomlands, along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="bottomlands" constraint="along streams" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ark., Ill., Ind., Iowa, Kans., Ky., Mich., Mo., Nebr., N.Y., Ohio, Okla., Pa., Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>50c.</number>
  
</bio:treatment>