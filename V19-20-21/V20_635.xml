<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="treatment_page">298</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="1865" rank="species">bloomeri</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 540. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species bloomeri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066563</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 4–15 (–20) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices branched.</text>
      <biological_entity id="o12321" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o12322" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems (usually white-indurate at bases) erect to slightly basally ascending, glabrous or densely white-strigose, eglandular.</text>
      <biological_entity id="o12323" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="slightly basally ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="white-strigose" value_original="white-strigose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal (persistent);</text>
      <biological_entity id="o12324" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o12325" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear, 20–70 (–90) × 1–2 (–3) mm, cauline reduced (on proximal 1/8–1/2 of stems, bases white-indurate, usually thickened) margins entire, faces glabrous or strigose, eglandular.</text>
      <biological_entity id="o12326" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o12327" name="blade" name_original="blades" src="d0_s4" type="structure" />
      <biological_entity id="o12328" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o12329" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1.</text>
      <biological_entity id="o12330" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–10 × 7–10 mm.</text>
      <biological_entity id="o12331" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series, strigose to villoso-hirsute or villous, minutely glandular to eglandular.</text>
      <biological_entity id="o12332" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="strigose" name="pubescence" notes="" src="d0_s7" to="villoso-hirsute or villous" />
        <character char_type="range_value" from="minutely glandular" name="architecture" src="d0_s7" to="eglandular" />
      </biological_entity>
      <biological_entity id="o12333" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o12332" id="r1126" name="in" negation="false" src="d0_s7" to="o12333" />
    </statement>
    <statement id="d0_s8">
      <text>Ray (pistillate) florets 0.</text>
      <biological_entity constraint="ray" id="o12334" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas 4.5–7 mm.</text>
      <biological_entity constraint="disc" id="o12335" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae (2.5–) 3–4 mm, 2-nerved, glabrous proximally, faces sparsely sericeous on distal 1/3;</text>
      <biological_entity id="o12336" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-nerved" value_original="2-nerved" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12337" name="face" name_original="faces" src="d0_s10" type="structure">
        <character constraint="on distal 1/3" constraintid="o12338" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12338" name="1/3" name_original="1/3" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>pappi: outer of setae, inner of 25–40 bristles.</text>
      <biological_entity id="o12339" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o12340" name="seta" name_original="setae" src="d0_s11" type="structure" />
      <biological_entity id="o12341" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s11" to="40" />
      </biological_entity>
      <relation from="o12339" id="r1127" name="outer of" negation="false" src="d0_s11" to="o12340" />
      <relation from="o12339" id="r1128" name="inner of" negation="false" src="d0_s11" to="o12341" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54.</number>
  <other_name type="common_name">Bloomer’s fleabane</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems and leaves strigose to puberulo-hirsutulous; phyllaries densely strigose to hirsute or hirsuto-villous, minutely glandular</description>
      <determination>54a Erigeron bloomeri var. bloomeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems and leaves glabrous or sparsely strigose; phyllaries glabrous, usually sparsely minutely glandular, sometimes eglandular</description>
      <determination>54b Erigeron bloomeri var. nudatus</determination>
    </key_statement>
  </key>
</bio:treatment>