<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">300</other_info_on_meta>
    <other_info_on_meta type="illustration_page">301</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Zinn" date="1757" rank="genus">helminthotheca</taxon_name>
    <taxon_name authority="(Linnaeus) Holub" date="1973" rank="species">echioides</taxon_name>
    <place_of_publication>
      <publication_title>Folia Geobot. Phytotax.</publication_title>
      <place_in_publication>8: 176. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus helminthotheca;species echioides</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250006892</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Picris</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">echioides</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 792. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Picris;species echioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades 50–150 (–250) × 10–50 (–80+) mm.</text>
      <biological_entity id="o9805" name="leaf-blade" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="150" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s0" to="250" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s0" to="150" to_unit="mm" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s0" to="80" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s0" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Peduncles 1–5+ cm, bristly hispid.</text>
      <biological_entity id="o9806" name="peduncle" name_original="peduncles" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="5" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="bristly" value_original="bristly" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Calyculi: bracts 9–15+ × 3–5+ mm, margins and tips bristly-ciliate.</text>
      <biological_entity id="o9807" name="calyculus" name_original="calyculi" src="d0_s2" type="structure" />
      <biological_entity id="o9808" name="bract" name_original="bracts" src="d0_s2" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s2" to="15" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9809" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s2" value="bristly-ciliate" value_original="bristly-ciliate" />
      </biological_entity>
      <biological_entity id="o9810" name="tip" name_original="tips" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s2" value="bristly-ciliate" value_original="bristly-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries often sigmoid in fruit, abaxial faces and tips bristly.</text>
      <biological_entity id="o9811" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character constraint="in tips" constraintid="o9815" is_modifier="false" modifier="often" name="course_or_shape" src="d0_s3" value="sigmoid" value_original="sigmoid" />
      </biological_entity>
      <biological_entity id="o9812" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
      <biological_entity id="o9813" name="face" name_original="faces" src="d0_s3" type="structure" />
      <biological_entity constraint="abaxial" id="o9814" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
      <biological_entity id="o9815" name="tip" name_original="tips" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae: bodies 2.5–3 mm, beaks 2.5–5 mm;</text>
      <biological_entity id="o9816" name="cypsela" name_original="cypselae" src="d0_s4" type="structure" />
      <biological_entity id="o9817" name="body" name_original="bodies" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9818" name="beak" name_original="beaks" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pappi 4–7 mm. 2n = 10.</text>
      <biological_entity id="o9819" name="cypsela" name_original="cypselae" src="d0_s5" type="structure" />
      <biological_entity id="o9820" name="pappus" name_original="pappi" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9821" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50(–500) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., N.B., Ont., Sask.; Ariz., Calif., Conn., D.C., Iowa, Maine, Md., Mass., Mo., Mont., N.J., N.Y., N.Dak., Ohio, Oreg., Pa., Vt., Va.; Europe; widely introduced elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="widely  elsewhere" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Reports for British Columbia and Nova Scotia need confirmation.</discussion>
  
</bio:treatment>