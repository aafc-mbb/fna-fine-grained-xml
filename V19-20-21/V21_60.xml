<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">33</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">melampodiinae</taxon_name>
    <taxon_name authority="Mackenzie ex Small" date="1933" rank="genus">SMALLANTHUS</taxon_name>
    <place_of_publication>
      <publication_title>Man. S.E. Fl.,</publication_title>
      <place_in_publication>1406, 1509. 1933</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe melampodiinae;genus SMALLANTHUS</taxon_hierarchy>
    <other_info_on_name type="etymology">For John Kunkel Small, 1869–1938, American botanist</other_info_on_name>
    <other_info_on_name type="fna_id">130559</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials [annuals, shrubs], 100–300 [1200+] cm.</text>
      <biological_entity id="o5938" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1200" from_unit="cm" name="average_some_measurement" src="d0_s0" to="100" to_inclusive="false" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect.</text>
      <biological_entity id="o5939" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (petioles usually winged) [sessile];</text>
      <biological_entity id="o5940" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades mostly deltate to ovate, usually ± palmately lobed, ultimate margins dentate to denticulate, faces hirtellous, pilosulous, or puberulent, glanddotted (at least abaxially).</text>
      <biological_entity id="o5941" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="mostly deltate" name="shape" src="d0_s5" to="ovate" />
        <character is_modifier="false" modifier="usually more or less palmately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o5942" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s5" to="denticulate" />
      </biological_entity>
      <biological_entity id="o5943" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilosulous" value_original="pilosulous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilosulous" value_original="pilosulous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or (2–5) in crowded, corymbiform arrays.</text>
      <biological_entity id="o5944" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o5945" from="2" name="atypical_quantity" src="d0_s6" to="5" />
      </biological_entity>
      <biological_entity id="o5945" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="crowded" value_original="crowded" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric, 8–15 mm diam.</text>
      <biological_entity id="o5946" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 12–13 [–25+] in 2 series (outer 4–6 ovate to lanceolate [orbiculate], herbaceous, inner as many as rays, more membranous to scarious, narrower and shorter).</text>
      <biological_entity id="o5947" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="25" upper_restricted="false" />
        <character char_type="range_value" constraint="in series" constraintid="o5948" from="12" name="quantity" src="d0_s8" to="13" />
      </biological_entity>
      <biological_entity id="o5948" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to convex, paleate (paleae obovate to spatulate, scarious).</text>
      <biological_entity id="o5949" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 7–13 [–25+], pistillate, fertile;</text>
      <biological_entity id="o5950" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="25" upper_restricted="false" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s10" to="13" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow [white or orange] (tubes hairy, laminae linear to elliptic [ovate]).</text>
      <biological_entity id="o5951" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets [20–] 40–80 [–150+], functionally staminate;</text>
      <biological_entity id="o5952" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" name="atypical_quantity" src="d0_s12" to="40" to_inclusive="false" />
        <character char_type="range_value" from="80" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="150" upper_restricted="false" />
        <character char_type="range_value" from="40" name="quantity" src="d0_s12" to="80" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow [orange], tubes shorter than abruptly campanulate [funnelform] throats, lobes 5, deltate.</text>
      <biological_entity id="o5953" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o5954" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than abruptly campanulate throats" constraintid="o5955" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o5955" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="abruptly" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o5956" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae (obliquely inserted on receptacles, each shed separate from subtending phyllary) obovoid [quadrangular], somewhat compressed, finely 30–40-ribbed or striate (not narrowed at bases, not apically beaked);</text>
      <biological_entity id="o5957" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s14" value="30-40-ribbed" value_original="30-40-ribbed" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s14" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 0 (cypselae sometimes hairy at apices).</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 16.</text>
      <biological_entity id="o5958" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o5959" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>United States, Mexico, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>255.</number>
  <discussion>Species ca. 20 (1 in the flora).</discussion>
  <references>
    <reference>Robinson, H. 1978. Studies in the Heliantheae (Asteraceae). XII. Re-establishment of the genus Smallanthus. Phytologia 39: 47–53.</reference>
    <reference>Turner, B. L. 1988. A new species of, and observations on, the genus Smallanthus (Asteraceae–Heliantheae). Phytologia 64: 405–409.</reference>
  </references>
  
</bio:treatment>