<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">435</other_info_on_meta>
    <other_info_on_meta type="mention_page">437</other_info_on_meta>
    <other_info_on_meta type="treatment_page">442</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Cassini in F. Cuvier" date="1828" rank="genus">hymenoxys</taxon_name>
    <taxon_name authority="(Rydberg) Cockerell" date="1904" rank="species">helenioides</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>31: 481. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus hymenoxys;species helenioides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066989</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Picradenia</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">helenioides</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>28: 21. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Picradenia;species helenioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–50 cm (polycarpic; usually with moderately branched, woody caudices).</text>
      <biological_entity id="o8122" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5 (–10+), green throughout or lightly purple-red-tinted proximally, branched distally, ± hairy.</text>
      <biological_entity id="o8123" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="10" upper_restricted="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
        <character is_modifier="false" modifier="throughout; throughout; lightly proximally; proximally" name="coloration" src="d0_s1" value="green throughout or lightly purple-red-tinted" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades simple or lobed (lobes 3), glabrous or sparsely hairy, glanddotted (basal leaf-bases sparsely, if at all, long-villous-woolly);</text>
      <biological_entity id="o8124" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o8125" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>mid leaves usually lobed (lobes 3, terminal lobes 2–5.5 mm wide), sometimes simple.</text>
      <biological_entity id="o8126" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="mid" id="o8127" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 5–50+ per plant, in paniculiform to corymbiform arrays.</text>
      <biological_entity id="o8128" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per plant" constraintid="o8129" from="5" name="quantity" src="d0_s4" to="50" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o8129" name="plant" name_original="plant" src="d0_s4" type="structure" />
      <biological_entity id="o8130" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character char_type="range_value" from="paniculiform" is_modifier="true" name="architecture" src="d0_s4" to="corymbiform" />
      </biological_entity>
      <relation from="o8128" id="r573" name="in" negation="false" src="d0_s4" to="o8130" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles (2.5–) 4–7.5 cm, ± hairy.</text>
      <biological_entity id="o8131" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s5" to="7.5" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres subhemispheric to hemispheric, 9–12 × 12–18 mm.</text>
      <biological_entity id="o8132" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="subhemispheric" name="shape" src="d0_s6" to="hemispheric" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s6" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2 series, unequal;</text>
      <biological_entity id="o8133" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o8134" name="series" name_original="series" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o8133" id="r574" name="in" negation="false" src="d0_s7" to="o8134" />
    </statement>
    <statement id="d0_s8">
      <text>outer 10–15, basally connate 1/4–1/3 their lengths (weakly keeled), lanceolate, 7.5–11 mm, apices acuminate;</text>
      <biological_entity constraint="outer" id="o8135" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="15" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s8" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/4" name="lengths" src="d0_s8" to="1/3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>inner 13 (–17), obovate, 5–8 mm, apices acuminate to mucronate.</text>
      <biological_entity id="o8136" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="position" src="d0_s9" value="inner" value_original="inner" />
      </biological_entity>
      <biological_entity id="o8137" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="17" />
        <character name="quantity" src="d0_s9" value="13" value_original="13" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8138" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s9" to="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 10–16;</text>
      <biological_entity id="o8139" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow to yellow-orange, 17–31 × 5–11 mm.</text>
      <biological_entity id="o8140" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="yellow-orange" />
        <character char_type="range_value" from="17" from_unit="mm" name="length" src="d0_s11" to="31" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 50–150+;</text>
      <biological_entity id="o8141" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s12" to="150" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas 3.5–5.5 mm.</text>
      <biological_entity id="o8142" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae narrowly obpyramidal, 2.5–3.5 mm;</text>
      <biological_entity id="o8143" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="obpyramidal" value_original="obpyramidal" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi of 5–7 obovate to lanceolate, aristate scales 2.5–4 mm. 2n = 30.</text>
      <biological_entity id="o8144" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" modifier="of" name="quantity" src="d0_s15" to="7" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s15" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o8145" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8146" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, open areas, edges of forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="open areas" />
        <character name="habitat" value="edges" constraint="of forests" />
        <character name="habitat" value="forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Intermountain rubberweed or bitterweed</other_name>
  
</bio:treatment>