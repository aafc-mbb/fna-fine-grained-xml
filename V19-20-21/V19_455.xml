<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="treatment_page">307</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">scorzonera</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">hispanica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 791. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus scorzonera;species hispanica</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250067468</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials.</text>
      <biological_entity id="o17923" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 120–400 × (1–) 3–6 mm, margins entire (flat or undulate).</text>
      <biological_entity id="o17924" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="120" from_unit="mm" name="length" src="d0_s1" to="400" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s1" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s1" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17925" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Involucres 20–30 × 8–12+ mm.</text>
      <biological_entity id="o17926" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries ovatelanceolate to lanceolate, glabrous.</text>
      <biological_entity id="o17927" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s3" to="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae 10–15 (–20) mm;</text>
      <biological_entity id="o17928" name="cypsela" name_original="cypselae" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pappi 9–15 (–20) mm.</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = 14.</text>
      <biological_entity id="o17929" name="pappus" name_original="pappi" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17930" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Black or Spanish salsify</other_name>
  <discussion>Scorzonera hispanica sometimes is used culinarily.</discussion>
  
</bio:treatment>