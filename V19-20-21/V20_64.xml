<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">46</other_info_on_meta>
    <other_info_on_meta type="treatment_page">47</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">pentachaeta</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="1873" rank="species">exilis</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 633. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus pentachaeta;species exilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067302</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aphantochaeta</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">exilis</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>4(5): 100, plate 11[as 10]A. 1857</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aphantochaeta;species exilis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chaetopappa</taxon_name>
    <taxon_name authority="(A. Gray) D. D. Keck" date="unknown" rank="species">exilis</taxon_name>
    <taxon_hierarchy>genus Chaetopappa;species exilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–6 cm.</text>
      <biological_entity id="o11477" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="6" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple or branched, rarely with many branches from bases.</text>
      <biological_entity id="o11478" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o11479" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="many" value_original="many" />
      </biological_entity>
      <biological_entity id="o11480" name="base" name_original="bases" src="d0_s1" type="structure" />
      <relation from="o11478" id="r1050" modifier="rarely" name="with" negation="false" src="d0_s1" to="o11479" />
      <relation from="o11479" id="r1051" name="from" negation="false" src="d0_s1" to="o11480" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades linear to filiform, 12–32 × 0.5–1 mm.</text>
      <biological_entity id="o11481" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="filiform" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s2" to="32" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres turbinate to campanulate.</text>
      <biological_entity id="o11482" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s3" to="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries in 2 (–3) series, elliptic to obovate, glabrous or sparsely and minutely glandular.</text>
      <biological_entity id="o11483" name="phyllarie" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s4" to="obovate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s4" value="sparsely" value_original="sparsely" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o11484" name="series" name_original="series" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s4" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <relation from="o11483" id="r1052" name="in" negation="false" src="d0_s4" to="o11484" />
    </statement>
    <statement id="d0_s5">
      <text>Ray (or pistillate) florets 0 or 1–5;</text>
      <biological_entity constraint="ray" id="o11485" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corollas white, laminae 0.8–2 mm, or 0, or apiculum 0.1–0.5 (–1) mm.</text>
      <biological_entity id="o11486" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o11487" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11488" name="apiculum" name_original="apiculum" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 3–6 (–15);</text>
      <biological_entity id="o11489" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="15" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellowish to reddish, club-shaped, widened through limbs, abruptly contracted at lobes, lobes 5, (lobes and sometimes distalmost portion of limbs) yellow or purplish red at maturity, style-branches (0.7–) 1–2 mm, stigmatic portions 0.4–0.5 mm.</text>
      <biological_entity id="o11490" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s8" to="reddish" />
        <character is_modifier="false" name="shape" src="d0_s8" value="club--shaped" value_original="club--shaped" />
        <character constraint="through limbs" constraintid="o11491" is_modifier="false" name="width" src="d0_s8" value="widened" value_original="widened" />
        <character constraint="at lobes" constraintid="o11492" is_modifier="false" modifier="abruptly" name="condition_or_size" notes="" src="d0_s8" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o11491" name="limb" name_original="limbs" src="d0_s8" type="structure" />
      <biological_entity id="o11492" name="lobe" name_original="lobes" src="d0_s8" type="structure" />
      <biological_entity id="o11493" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s8" value="purplish red" value_original="purplish red" />
      </biological_entity>
      <biological_entity id="o11494" name="branch-style" name_original="style-branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="atypical_distance" src="d0_s8" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="stigmatic" id="o11495" name="portion" name_original="portions" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pappi 0, or of (3–) 5 bristles, not dilated at bases.</text>
      <biological_entity id="o11497" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s9" to="5" to_inclusive="false" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o11498" name="base" name_original="bases" src="d0_s9" type="structure" />
      <relation from="o11496" id="r1053" name="consist_of" negation="false" src="d0_s9" to="o11497" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 18.</text>
      <biological_entity id="o11496" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character constraint="at bases" constraintid="o11498" is_modifier="false" modifier="not" name="shape" notes="" src="d0_s9" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11499" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Meager pentachaeta</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Plants of Pentachaeta exilis with greatly reduced or completely absent pappi apparently are scattered over the range of the species. This feature is relatively constant within a population.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ray (pistillate) corollas elaminate or with apiculum 0.1–0.5(–1) mm; disc florets 3–6(–15), corolla lobes red-purple, style branches (0.7–)1–2 mm, stigmatic portions 0.4–0.5 mm</description>
      <determination>2a Pentachaeta exilis subsp. exilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ray (pistillate) corollas laminate, laminae 0.8–2 mm; disc florets (7–)15–34, corolla lobes yellow, style branches 2–3 mm, stigmatic portions 0.6–1mm</description>
      <determination>2b Pentachaeta exilis subsp. aeolica</determination>
    </key_statement>
  </key>
</bio:treatment>