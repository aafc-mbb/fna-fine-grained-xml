<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="treatment_page">236</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">tagetes</taxon_name>
    <taxon_name authority="A. Gray" date="1883" rank="species">lemmonii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 40. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus tagetes;species lemmonii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067698</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, subshrubs, or shrubs 10–50 (–100+) cm.</text>
      <biological_entity id="o12502" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o12503" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades 50–70 (–120+) mm overall, lobes or leaflets (3–) 5–7+, lance-elliptic to lanceolate, 15–30 (–45) × 3–7 (–10+) mm.</text>
      <biological_entity id="o12505" name="blade-leaf" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="70" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s1" to="120" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="50" from_unit="mm" name="distance" src="d0_s1" to="70" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12506" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character char_type="range_value" from="lance-elliptic" name="shape" src="d0_s1" to="lanceolate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="45" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s1" to="30" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="10" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s1" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12507" name="leaflet" name_original="leaflets" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s1" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s1" to="7" upper_restricted="false" />
        <character char_type="range_value" from="lance-elliptic" name="shape" src="d0_s1" to="lanceolate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s1" to="45" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s1" to="30" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s1" to="10" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s1" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads in ± corymbiform arrays.</text>
      <biological_entity id="o12508" name="head" name_original="heads" src="d0_s2" type="structure" />
      <biological_entity id="o12509" name="array" name_original="arrays" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s2" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o12508" id="r858" name="in" negation="false" src="d0_s2" to="o12509" />
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 10–50 mm.</text>
      <biological_entity id="o12510" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="50" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 8–10 × 5–7 mm.</text>
      <biological_entity id="o12511" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets (3–) 5–8+;</text>
      <biological_entity id="o12512" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s5" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s5" to="8" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>laminae yellow, ± oblong, 9–15+ mm.</text>
      <biological_entity id="o12513" name="lamina" name_original="laminae" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 12–30+;</text>
      <biological_entity id="o12514" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s7" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas 6–7 mm.</text>
      <biological_entity id="o12515" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 5–6 mm;</text>
      <biological_entity id="o12516" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 0–5 lanceolate to subulate-aristate scales 2.5–3+ mm plus 5+ lanceolate to oblong, ± erose scales 0.5–1 mm, distinct or connate, linear-oblong, ± erose scales 2–6+ mm.</text>
      <biological_entity id="o12517" name="pappus" name_original="pappi" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" upper_restricted="false" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="oblong" />
      </biological_entity>
      <biological_entity id="o12518" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s10" to="5" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s10" to="subulate-aristate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12519" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture_or_relief" src="d0_s10" value="erose" value_original="erose" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-oblong" value_original="linear-oblong" />
      </biological_entity>
      <relation from="o12517" id="r859" name="consist_of" negation="false" src="d0_s10" to="o12518" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 24.</text>
      <biological_entity id="o12520" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture_or_relief" src="d0_s10" value="erose" value_original="erose" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12521" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Aug–)Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Sep" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist sites, grasslands, scrublands, woodlands, cliffs, streamsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist sites" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="scrublands" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="past_name">lemmoni</other_name>
  
</bio:treatment>