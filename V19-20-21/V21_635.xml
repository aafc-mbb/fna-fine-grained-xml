<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Bruce G. Baldwin,John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">255</other_info_on_meta>
    <other_info_on_meta type="treatment_page">258</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Schauer" date="1837" rank="genus">ACHYRACHAENA</taxon_name>
    <place_of_publication>
      <publication_title>Index Seminum (Bratislave)</publication_title>
      <place_in_publication>1837: [3]. 1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus ACHYRACHAENA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek achyron, scale, and Latin achaenium, fruit, alluding to cypselae</other_info_on_name>
    <other_info_on_name type="fna_id">100226</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 4–62 cm.</text>
      <biological_entity id="o16077" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="62" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched distally.</text>
      <biological_entity id="o16078" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o16079" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16080" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal opposite, distal alternate;</text>
      <biological_entity constraint="proximal" id="o16081" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>± sessile;</text>
      <biological_entity constraint="distal" id="o16082" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades linear, margins entire or toothed, faces hirsute or villous and (distal leaves) sparsely glandular-pubescent.</text>
      <biological_entity id="o16083" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o16084" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o16085" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in loose, ± corymbiform arrays.</text>
      <biological_entity id="o16086" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character constraint="in loose , more or less corymbiform arrays" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , more or less corymbiform arrays" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncular bracts: pit-glands, tack-glands, and/or spines 0 at tips.</text>
      <biological_entity constraint="peduncular" id="o16087" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o16088" name="pit-gland" name_original="pit-glands" src="d0_s7" type="structure" />
      <biological_entity id="o16089" name="tack-gland" name_original="tack-glands" src="d0_s7" type="structure" />
      <biological_entity id="o16090" name="spine" name_original="spines" src="d0_s7" type="structure">
        <character constraint="at tips" constraintid="o16091" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o16091" name="tip" name_original="tips" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres ± campanulate to cylindric or ellipsoid, 3–12+ mm diam.</text>
      <biological_entity id="o16092" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="less campanulate" name="shape" src="d0_s8" to="cylindric or ellipsoid" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s8" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 3–8 in 1 series (± lance-linear, herbaceous, each fully enveloping a ray ovary, abaxially hirsute or villous and sparsely glandular-pubescent or eglandular).</text>
      <biological_entity id="o16093" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o16094" from="3" name="quantity" src="d0_s9" to="8" />
      </biological_entity>
      <biological_entity id="o16094" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat to convex (setulose), paleate (paleae falling, in 1 series between rays and disc, distinct).</text>
      <biological_entity id="o16095" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s10" to="convex" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 3–8, pistillate, fertile;</text>
      <biological_entity id="o16096" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow, turning reddish (lobes ± parallel).</text>
      <biological_entity id="o16097" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 4–35, bisexual and fertile;</text>
      <biological_entity id="o16098" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="35" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow to reddish, tubes ± equaling narrowly funnelform throats, lobes 5, lance-deltate (anthers ± dark purple; styles glabrous proximal to branches).</text>
      <biological_entity id="o16099" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s14" to="reddish" />
      </biological_entity>
      <biological_entity id="o16100" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less; narrowly" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o16101" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o16102" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lance-deltate" value_original="lance-deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Ray cypselae (black) obcompressed, clavate (10-ribbed, straight, basal attachments centered, apices beakless, faces glabrous or ± scabrous);</text>
      <biological_entity constraint="ray" id="o16103" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" name="shape" src="d0_s15" value="clavate" value_original="clavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0.</text>
      <biological_entity id="o16104" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Disc cypselae (brown to black) similar to rays (± scabrous);</text>
      <biological_entity constraint="disc" id="o16105" name="cypsela" name_original="cypselae" src="d0_s17" type="structure" />
      <biological_entity id="o16106" name="ray" name_original="rays" src="d0_s17" type="structure" />
      <relation from="o16105" id="r1114" name="to" negation="false" src="d0_s17" to="o16106" />
    </statement>
    <statement id="d0_s18">
      <text>pappi of 10 white, oblong scales (apices obtuse).</text>
      <biological_entity id="o16108" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s18" value="10" value_original="10" />
        <character is_modifier="true" name="coloration" src="d0_s18" value="white" value_original="white" />
        <character is_modifier="true" name="shape" src="d0_s18" value="oblong" value_original="oblong" />
      </biological_entity>
      <relation from="o16107" id="r1115" name="consist_of" negation="false" src="d0_s18" to="o16108" />
    </statement>
    <statement id="d0_s19">
      <text>x = 8.</text>
      <biological_entity id="o16107" name="pappus" name_original="pappi" src="d0_s18" type="structure" />
      <biological_entity constraint="x" id="o16109" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America, nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>332.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>