<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">34</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">baccharis</taxon_name>
    <taxon_name authority="R. M. Beauchamp" date="1980" rank="species">vanessae</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>46: 216, figs. 2, 3. 1980</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus baccharis;species vanessae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066193</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 50–200 cm (sprawling, densely stemmed from crowns, broomlike).</text>
      <biological_entity id="o7898" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, slender, rounded, smooth, glabrous or stipitate-glandular proximal to heads.</text>
      <biological_entity id="o7899" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s1" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character constraint="to heads" constraintid="o7900" is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o7900" name="head" name_original="heads" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves often withering and sparse by flowering;</text>
    </statement>
    <statement id="d0_s3">
      <text>sessile;</text>
      <biological_entity id="o7901" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="life_cycle" src="d0_s2" value="withering" value_original="withering" />
        <character constraint="by flowering" is_modifier="false" name="count_or_density" src="d0_s2" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (1-nerved) filiform to linear-oblanceolate, 10–30 × 1–3 mm (slightly fleshy), bases narrowed, margins entire (revolute), apices acute (mucronate), faces glabrous, gland-dotted.</text>
      <biological_entity id="o7902" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="filiform" name="shape" src="d0_s4" to="linear-oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7903" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o7904" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7905" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o7906" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads borne singly or in (pedunculate clusters) in loose paniculiform or racemiform arrays.</text>
      <biological_entity id="o7907" name="head" name_original="heads" src="d0_s5" type="structure">
        <character constraint="in arrays" constraintid="o7908" is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o7908" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="racemiform" value_original="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres funnelform;</text>
    </statement>
    <statement id="d0_s7">
      <text>staminate 3–5 mm, pistillate 3–5 mm.</text>
      <biological_entity id="o7909" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries lanceolate (not keeled), 1–4 mm, margins ciliate, chartaceous, apices acute to acuminate (abaxial faces scurfy-glandular).</text>
      <biological_entity id="o7910" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7911" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o7912" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate florets 15–22;</text>
      <biological_entity id="o7913" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s9" to="22" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 4 mm.</text>
      <biological_entity id="o7914" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate florets ca. 25;</text>
      <biological_entity id="o7915" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character name="quantity" src="d0_s11" value="25" value_original="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 2.5 mm.</text>
      <biological_entity id="o7916" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 2–3 mm, 10-nerved, glabrous or ciliate along nerves;</text>
      <biological_entity id="o7917" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="10-nerved" value_original="10-nerved" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character constraint="along nerves" constraintid="o7918" is_modifier="false" name="pubescence" src="d0_s13" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o7918" name="nerve" name_original="nerves" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>pappi 7–10 mm. 2n = 18.</text>
      <biological_entity id="o7919" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7920" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Oct" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Chaparral, Torrey-pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="torrey-pine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <other_name type="common_name">Encinitas false willow or baccharis</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Baccharis vanessae is highly localized in chaparral remnants in relictual Torrey Pine forests of coastal San Diego County. It is distinguished from other species of Baccharis by its filiform leaves and delicate, ciliate phyllaries that reflex at maturity.</discussion>
  
</bio:treatment>