<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">89</other_info_on_meta>
    <other_info_on_meta type="treatment_page">91</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">gutierrezia</taxon_name>
    <taxon_name authority="(de Candolle) Torrey &amp; A. Gray" date="1842" rank="species">texana</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 194. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus gutierrezia;species texana</taxon_hierarchy>
    <other_info_on_name type="fna_id">242416610</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemiachyris</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">texana</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 314. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemiachyris;species texana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xanthocephalum</taxon_name>
    <taxon_name authority="(de Candolle) Shinners" date="unknown" rank="species">texanum</taxon_name>
    <taxon_hierarchy>genus Xanthocephalum;species texanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–80 (–100) cm.</text>
      <biological_entity id="o21327" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems glabrous.</text>
      <biological_entity id="o21328" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal usually absent at flowering;</text>
      <biological_entity id="o21329" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o21330" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="usually" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline blades 1-nerved or 3-nerved, linear, 1.5–4 mm wide, reduced distally.</text>
      <biological_entity id="o21331" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o21332" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in loose arrays.</text>
      <biological_entity id="o21333" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o21334" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o21333" id="r1967" name="in" negation="false" src="d0_s4" to="o21334" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres campanulate to obconic, 2–4.5 mm diam..</text>
      <biological_entity id="o21335" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s5" to="obconic" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllary apices flat.</text>
      <biological_entity constraint="phyllary" id="o21336" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 5–23;</text>
      <biological_entity id="o21337" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="23" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow, 3–6 mm.</text>
      <biological_entity id="o21338" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 7–48 (bisexual and fertile or functionally staminate).</text>
      <biological_entity id="o21339" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s9" to="48" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 1.3–1.8 mm, faces without oil cavities, loosely strigose (hairs appressed, apices acute, microscopically 2-pronged);</text>
      <biological_entity id="o21340" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21341" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" notes="" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity constraint="oil" id="o21342" name="cavity" name_original="cavities" src="d0_s10" type="structure" />
      <relation from="o21341" id="r1968" name="without" negation="false" src="d0_s10" to="o21342" />
    </statement>
    <statement id="d0_s11">
      <text>pappi of 1 series of spreading-erect, oblong to ovate or obovate scales 0.1–0.5 mm (bases sometimes connate, usually shorter in rays, sometimes 0 in var. texana).</text>
      <biological_entity id="o21343" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o21344" name="series" name_original="series" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21345" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s11" value="spreading-erect" value_original="spreading-erect" />
        <character char_type="range_value" from="oblong" is_modifier="true" name="shape" src="d0_s11" to="ovate or obovate" />
      </biological_entity>
      <relation from="o21343" id="r1969" name="consist_of" negation="false" src="d0_s11" to="o21344" />
      <relation from="o21344" id="r1970" name="part_of" negation="false" src="d0_s11" to="o21345" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Ill., La., Mo., N.Mex., Okla., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Texas snakeweed</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Although the two varieties of Gutierrezia texana are similar, the geographic demarcation between them appears to be sharp, and a detailed investigation of their biologic interaction would be interesting.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres (2–)3.5–4.5 mm diam.; disc florets 10–48; cypselae moderately to densely strigoso-sericeous, hairs usually in dense lines and at least partially obscuring faces; pappi of distinct scales 0.2–0.5 mm, rarely more reduced</description>
      <determination>3a Gutierrezia texana var. glutinosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Involucres 2–3 mm diam.; disc florets 7–13; cypselae sparsely and evenly strigose, hairs not obscuring cypsela faces; pappi of continuous or scaly crowns less than 0.1 mm</description>
      <determination>3b Gutierrezia texana var. texana</determination>
    </key_statement>
  </key>
</bio:treatment>