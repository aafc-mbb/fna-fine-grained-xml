<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">506</other_info_on_meta>
    <other_info_on_meta type="treatment_page">507</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="Besser" date="1829" rank="subgenus">drancunculus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">campestris</taxon_name>
    <taxon_name authority="(Michaux) H. M. Hall &amp; Clements" date="1923" rank="subspecies">caudata</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Carnegie Inst. Wash.</publication_title>
      <place_in_publication>326: 122. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus drancunculus;species campestris;subspecies caudata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068059</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">caudata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 129. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Artemisia;species caudata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">forwoodii</taxon_name>
    <taxon_hierarchy>genus Artemisia;species forwoodii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials, 20–80 (–150) cm.</text>
      <biological_entity id="o10958" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1.</text>
      <biological_entity id="o10959" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal rosettes not persistent (faces green and glabrous or sparsely white-pubescent).</text>
      <biological_entity id="o10960" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o10961" name="rosette" name_original="rosettes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="duration" src="d0_s2" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in arrays 12–30 (–35) × 1–8 (–12) cm.</text>
      <biological_entity id="o10962" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" modifier="in arrays" name="atypical_length" src="d0_s3" to="35" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" modifier="in arrays" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" modifier="in arrays" name="atypical_width" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" modifier="in arrays" name="width" src="d0_s3" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres turbinate, 2–3 × 2–3 mm.</text>
      <biological_entity id="o10963" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early–late summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late summer" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open meadows, usually moist soils, sometimes sandy or rocky habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open meadows" />
        <character name="habitat" value="moist soils" modifier="usually" />
        <character name="habitat" value="sandy" modifier="sometimes" />
        <character name="habitat" value="rocky habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que., Sask.; Ark., Colo., Conn., Fla., Ill., Ind., Iowa, Kans., Maine, Mass., Mich., Minn., Miss., Mo., Mont., Nebr., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tex., Vt., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3b.</number>
  <other_name type="common_name">Armoise caudée</other_name>
  <discussion>A population of Artemisia campestris found in Massachusetts differs from populations of subsp. caudata by its smaller heads and multiple branched stems. That population is typical of subsp. campestris, formerly believed to be restricted to Europe.</discussion>
  
</bio:treatment>