<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Leila M. Shultz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">53</other_info_on_meta>
    <other_info_on_meta type="mention_page">398</other_info_on_meta>
    <other_info_on_meta type="mention_page">486</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="mention_page">498</other_info_on_meta>
    <other_info_on_meta type="mention_page">504</other_info_on_meta>
    <other_info_on_meta type="mention_page">50</other_info_on_meta>
    <other_info_on_meta type="treatment_page">503</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ARTEMISIA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 845. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 367. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus ARTEMISIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek Artemis, goddess of the hunt and namesake of Artemisia, Queen of Anatolia</other_info_on_name>
    <other_info_on_name type="fna_id">102682</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, perennials, subshrubs, or shrubs, 3–350 cm (usually, rarely not, aromatic).</text>
      <biological_entity id="o7040" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o7043" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="350" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–10+, usually erect, usually branched, glabrous or hairy (hairs basi or medifixed).</text>
      <biological_entity id="o7045" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="10" upper_restricted="false" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal or basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o7046" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades filiform, linear, lanceolate, ovate, elliptic, oblong, oblanceolate, obovate, cuneate, flabellate, or spatulate, usually pinnately and/or palmately lobed, sometimes apically ± 3-lobed or toothed, or entire, faces glabrous or hairy (hairs multicelled and filled with aromatic terpenoids and/or 1-celled and hollow, dolabriform, T-shaped).</text>
      <biological_entity id="o7047" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flabellate" value_original="flabellate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flabellate" value_original="flabellate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="usually pinnately; pinnately; palmately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sometimes apically more or less" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes apically more or less" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o7048" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads usually discoid, sometimes disciform (subradiate in A. bigelovii), in relatively broad, paniculiform arrays, or in relatively narrow, racemiform or spiciform arrays.</text>
      <biological_entity id="o7049" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s6" value="disciform" value_original="disciform" />
        <character constraint="in relatively broad , paniculiform arrays , or in" is_modifier="false" modifier="relatively" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="racemiform" value_original="racemiform" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="spiciform" value_original="spiciform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate, globose, ovoid, or turbinate, 1.5–8 mm diam.</text>
      <biological_entity id="o7050" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 2–20+ in 4–7 series, distinct, (usually green to whitish green, rarely stramineous) ovate to lanceolate, unequal, margins and apices (usually green or white, rarely dark-brown or black) ± scarious (abaxial faces glabrous or hairy).</text>
      <biological_entity id="o7051" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o7052" from="2" name="quantity" src="d0_s8" to="20" upper_restricted="false" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s8" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o7052" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
      <biological_entity id="o7053" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o7054" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat, convex, or conic (glabrous or hairy), epaleate (except paleate in A. palmeri).</text>
      <biological_entity id="o7055" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s9" value="conic" value_original="conic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s9" value="conic" value_original="conic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0 (peripheral pistillate florets in disciform heads usually 1–20, their corollas filiform; corollas of 1–3 pistillate florets in heads of A. bigelovii sometimes ± 2-lobed, weakly raylike).</text>
      <biological_entity id="o7056" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 2–20 (–30+), bisexual and fertile, or functionally staminate;</text>
      <biological_entity id="o7057" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="30" upper_restricted="false" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="20" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas (glabrous or ± hirtellous) usually pale-yellow, rarely red, tubes ± cylindric, throats subglobose or funnelform, lobes 5, ± deltate.</text>
      <biological_entity id="o7058" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s12" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o7059" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o7060" name="throat" name_original="throats" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="subglobose" value_original="subglobose" />
        <character name="shape" src="d0_s12" value="funnel" value_original="funnelform" />
      </biological_entity>
      <biological_entity id="o7061" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae (brown) fusiform, ribs 0 (and faces finely striate) or 2–5, faces glabrous or hairy (not villous), often gland-dotted (pericarps sometimes with myxogenic cells, without resin sacs; embryo-sac development monosporic);</text>
      <biological_entity id="o7062" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="fusiform" value_original="fusiform" />
      </biological_entity>
      <biological_entity id="o7063" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="5" />
      </biological_entity>
      <biological_entity id="o7064" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s13" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi usually 0 (coroniform in A. californica and A. papposa, sometimes on outer in A. rothrockii).</text>
    </statement>
    <statement id="d0_s15">
      <text>x = 9.</text>
      <biological_entity id="o7065" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o7066" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly Northern Hemisphere (North America, Eurasia), some in South America and Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly Northern Hemisphere (North America)" establishment_means="native" />
        <character name="distribution" value="Mostly Northern Hemisphere (Eurasia)" establishment_means="native" />
        <character name="distribution" value="some in South America and Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>119.</number>
  <other_name type="common_name">Felon-herb</other_name>
  <other_name type="common_name">mugwort</other_name>
  <other_name type="common_name">sagebrush</other_name>
  <other_name type="common_name">sailor’s-tobacco</other_name>
  <other_name type="common_name">wormwood</other_name>
  <other_name type="common_name">armoise</other_name>
  <other_name type="common_name">herbe Saint-Jean</other_name>
  <discussion>species ca. 350–500 (50 in the flora)</discussion>
  <discussion>As circumscribed here, there are five subgenera in Artemisia; four are represented in the flora area.</discussion>
  <discussion>Etymologies of the common names used for Artemisia species provide glimpses of their uses and demonstrate the rich diversity within the genus. The common name ‘mugwort’ is from the Old English mucgwyrt, mucg meaning ‘midge,’ and refers to the use of Old World herbaceous species in repelling flies and midges. Artemisia was called Motherwort in nineteenth century Maine (as an indication of the high esteem for this otherwise rather pedestrian plant), and in the herbal by R. Banckes (1525): “This herb helpeth a woman to conceyve a chylde, and clenseth the mother, and maketh a woman to have her flowers.” Early settlers in North America brought European plants of A. dracunculus, A. vulgaris, A. absinthium, and A. abrotanum into their herb gardens for seasoning and medicinal uses; they would also have learned about aboriginal uses of Artemisia species native to North America, uses that included fertility rites (sagebrush in western North America) and antihelminthics (wormwoods of grasslands and mountain habitats). Immigrants used A. annua (sweet Annie) in potpourris and later recognized its utility as an anti-malarial drug, a use that was well known in oriental medicine. ‘Bulwand’ is the local name used for herbaceous wormwoods in Scotland, and ‘green-ginger’ and ‘Sailor’s tobacco’ are local names in England (T. Coffey 1993). Use of the names ‘sagewort’ and ‘sagebrush’ in North America arise from the familiar aroma of culinary sage, Salvia officinalis (Lamiaceae). Because true sages (Salvia) and sagewort/sagebrushes (Artemisia) are in separate families, the chemical similarities are an example of convergent evolution. The intense aroma and bitter taste of the plants from terpenoids and sesquiterpene lactones discourages herbivory and undoubtedly has contributed to the remarkable evolutionary success (measured by abundance as well as diversity) of species in this genus.</discussion>
  <discussion>Members of Artemisia are wind-pollinated and their heads and florets are exceptionally small (even for composites) and, consequently, difficult to examine and assess. Nevertheless, the sexual constitution of floral heads is important in recognition of subgenera. Plant habits and ornamentations of receptacles have also figured in arriving at subgeneric circumscriptions; additional characteristics are enumerated in the descriptions.</discussion>
  <discussion>Artemisia has a well-deserved reputation for being taxonomically difficult. The number of subgenera varies from four to five in modern treatments, and the number of taxa recognized at the species or subspecific levels varies between 250 and 500 (K. Bremer and C. J. Humphries 1993; H. M. Hall and F. E. Clements 1923; Y. R. Ling 1982, 1995; P. P. Poljakov 1961; M. Torrell et al. 1999). In this treatment, I recognize four native subgenera; subg. Seriphidium is endemic to Asia. In the flora area, the greatest diversity occurs in subg. Artemisia. Subgenus Absinthium can be segregated on the basis of hairs on the receptacle; it may be not phylogenetically distinct (L. E. Watson et al. 2002; J. Valles and E. D. McArthur 2001). Subgenus Dracunculus is clearly distinguished by molecular differences, and subg. Tridentatae is well defined with the exception of A. pygmaea.</discussion>
  <discussion>This treatment is based on extensive fieldwork, review of recent research, and examination of thousands of specimens; taxonomic circumscriptions remain controversial. Molecular analyses have helped define subgenera but have not clarified relationships between closely related species. The morphologic characters useful in distinguishing species tend to be variable and are often hard to assess (i.e., the sexuality of microscopic florets). Users of the keys will meet with frustrations; descriptions of subgenera and illustrations will help in defining the major groupings of species.</discussion>
  <discussion>The subgenera are arranged in approximate phylogenetic order; species are arranged alphabetically within the subgenera. Molecular studies define subg. Dracunculus as a major clade that is ancestral to the majority of Artemisia. The subgenera Absinthium, Tridentatae, and Artemisia can be classified as clades; they are weakly supported by molecular evidence.</discussion>
  <references>
    <reference>Hall, H. M. and F. E. Clements. 1923. The phylogenetic method in taxonomy: The North America species of Artemisia, Chrysothamnus, and Atriplex. Publ. Carnegie Inst. Wash. 326.</reference>
    <reference>Ling, Y. R. 1995. The New World Artemisia L. In: D. J. N. Hind et al., eds. 1995. Advances in Compositae Systematics. Kew. Pp. 225–281.</reference>
    <reference>Torrell, M., N. Garcia-Jacas, A. Susanna, and J. Valles. 1999. Phylogeny in Artemisia (Asteraceae, Anthemideae) inferred from nuclear ribosomal DNA (ITS) sequences. Taxon 48: 721–736.</reference>
    <reference>Valles, J. and E. D. McArthur. 2001. Artemisia systematics and phylogeny: Cytogenetic and molecular insights. In: E. D. McArthur and D. J. Fairbanks, comps. 2001. Shrubland Ecosystem Genetics and Biodiversity: Proceedings: Provo, UT, June 13–15, 2000. Ogden. Pp. 67–74.</reference>
    <reference>Ling, Y. R. 1982. On the system of the genus Artemisia L. and the relationship with its allies. Bull. Bot. Lab. N. E. Forest. Inst., Harbin 2: 1–60.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs; leaves in lateral fascicles (on vegetative shoots); heads discoid (except in A. bigelovii with, rarely, 1–2 raylike florets): florets bisexual (corollas 5-lobed); receptacles glabrous.</description>
      <determination>119b Artemisia subg. Tridentatae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals, biennials, perennials, or subshrubs (shrubs in A. filifolia, A. californica, and A. nesiotica); leaves not in fascicles; heads usually disciform, rarely discoid; receptacles glabrous or villous</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Disc florets functionally staminate (not setting fruits), corollas subglobose</description>
      <determination>119a Artemisia subg. Drancunculus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Disc florets usually bisexual and fertile (sometimes functionally staminate in A. packardiae in subg. Artemisia), corollas funnelform</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Receptacles villous</description>
      <determination>119c Artemisia subg. Absinthium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Receptacles glabrous (paleate in A. palmeri)</description>
      <determination>119d Artemisia subg. Artemisia</determination>
    </key_statement>
  </key>
</bio:treatment>