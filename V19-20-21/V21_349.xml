<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">142</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="mention_page">153</other_info_on_meta>
    <other_info_on_meta type="treatment_page">150</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helianthus</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">debilis</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 367. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus helianthus;species debilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066877</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 30–200 cm (taprooted).</text>
      <biological_entity id="o10042" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to erect, glabrous, hirsute, or puberulent.</text>
      <biological_entity id="o10044" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o10046" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>mostly alternate;</text>
      <biological_entity id="o10045" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles 1–7 cm;</text>
      <biological_entity id="o10047" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades deltate-ovate, lanceovate, or ovate, 2.5–14 × 1.8–13 cm, bases cordate to truncate or broadly cuneate, margins subentire to serrate, abaxial faces glabrate to hispid, not glanddotted.</text>
      <biological_entity id="o10048" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate-ovate" value_original="deltate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceovate" value_original="lanceovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceovate" value_original="lanceovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s5" to="14" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="cm" name="width" src="d0_s5" to="13" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10049" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s5" to="truncate or broadly cuneate" />
      </biological_entity>
      <biological_entity id="o10050" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s5" to="serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10051" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s5" to="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–3.</text>
      <biological_entity id="o10052" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 9–50 cm.</text>
      <biological_entity id="o10053" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s7" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres hemispheric, 10–22 mm diam.</text>
      <biological_entity id="o10054" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s8" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 20–30, lanceolate, 8–17 × 1–3 mm, apices acute to long-attenuate, abaxial faces glabrous or ± hispid, not glanddotted.</text>
      <biological_entity id="o10055" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="30" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="17" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10056" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="long-attenuate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10057" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Paleae 7.5–8 mm, apices 3-toothed (middle teeth acuminate, usually glabrous or hispid, sometimes ± villous or bearded).</text>
      <biological_entity id="o10058" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character char_type="range_value" from="7.5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10059" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 11–20;</text>
      <biological_entity id="o10060" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s11" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 12–23 mm.</text>
      <biological_entity id="o10061" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s12" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 30+;</text>
      <biological_entity id="o10062" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s13" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 4.5–5 mm, lobes usually reddish, sometimes yellow;</text>
      <biological_entity id="o10063" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10064" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers dark, appendages dark (style-branches usually reddish, rarely yellow).</text>
      <biological_entity id="o10065" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o10066" name="appendage" name_original="appendages" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 2.5–3.2 mm, glabrous or sparsely hairy;</text>
      <biological_entity id="o10067" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of 2 lanceolate or lance-linear scales 1.2–2.5 mm.</text>
      <biological_entity id="o10068" name="pappus" name_original="pappi" src="d0_s17" type="structure" />
      <biological_entity id="o10069" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s17" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s17" value="lance-linear" value_original="lance-linear" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
      </biological_entity>
      <relation from="o10068" id="r695" name="consist_of" negation="false" src="d0_s17" to="o10069" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Conn., Fla., Ga., La., Mass., Md., Mich., Miss., N.C., N.H., N.Y., Pa., R.I., S.C., Tex., Va., Vt., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Subspecies 5 (5 in the flora).</discussion>
  <discussion>C. B. Heiser (1956) placed 8 subspecies in Helianthus debilis; he noted that alternative taxonomic treatments might recognize these in as many as three species, or expand the single species to include H. petiolaris. Later, Heiser et al. (1969) separated three of the subspecies as H. praecox. Isozyme data (R. P. Wain 1982, 1983; L. H. Rieseberg and M. F. Doyle 1989) show that all are closely related. Documented hybridization with H. annuus further complicates the situation. The treatment by Heiser et al. is followed here.</discussion>
  <discussion>Helianthus debilis is adventive beyond the Atlantic and Gulf coasts of the United States.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems decumbent; peduncles 9–20(–22) cm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems erect; peduncles (15–)20–50 cm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems glabrous or puberulent; leaf blades serrulate or shallowly, regularly serrate, abaxial faces sparsely, if at all, gland-dotted</description>
      <determination>7a Helianthus debilis subsp. debilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems hirsute; leaf blades deeply, irregularly serrate, abaxial faces densely gland-dotted</description>
      <determination>7b Helianthus debilis subsp. vestitus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves 8–14 cm; peduncles (relatively slender) 20–40 cm; discs 10–15(–17) mm diam</description>
      <determination>7c Helianthus debilis subsp. silvestris</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves 2.5–9 cm; peduncles (not notably slender) 10–50 cm; discs 14–20 mm diam</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades usually deeply, irregularly serrate; peduncles 10–25(–30) cm; ray lami-nae 12–20(–22) mm</description>
      <determination>7d Helianthus debilis subsp. tardiflorus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades usually shallowly, regularly serrate; peduncles 25–50 cm; ray laminae (15–)20–23 mm</description>
      <determination>7d Helianthus debilis subsp. cucumerifolius</determination>
    </key_statement>
  </key>
</bio:treatment>