<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">5</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">11</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="treatment_page">200</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">Vernonieae</taxon_name>
    <place_of_publication>
      <publication_title>J. Phys. Chim. Hist. Nat. Arts</publication_title>
      <place_in_publication>88: 203. 1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe Vernonieae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20545</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, perennials, or shrubs [trees or lianas] (sap rarely milky).</text>
      <biological_entity id="o137" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o140" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves usually cauline, sometimes basal or basal and cauline;</text>
      <biological_entity id="o142" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="usually" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o143" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>alternate (rarely subopposite distally) [opposite];</text>
    </statement>
    <statement id="d0_s3">
      <text>usually petiolate, sometimes sessile (or petioles winged);</text>
      <biological_entity id="o141" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>margins usually ± dentate, sometimes entire [lobed or dissected].</text>
      <biological_entity id="o144" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads homogamous (discoid, pseudo-radiant or liguliflorous in Stokesia), usually in corymbiform, paniculiform, or scorpioid arrays, sometimes borne singly or in glomerules [aggregated in second-order heads].</text>
      <biological_entity id="o145" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="homogamous" value_original="homogamous" />
      </biological_entity>
      <biological_entity id="o146" name="glomerule" name_original="glomerules" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o145" id="r13" modifier="in corymbiform , paniculiform , or scorpioid arrays; sometimes" name="borne" negation="false" src="d0_s5" to="o146" />
    </statement>
    <statement id="d0_s6">
      <text>Calyculi 0.</text>
      <biological_entity id="o147" name="calyculus" name_original="calyculi" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries usually persistent [readily falling], in 2–8+ series, distinct, unequal, herbaceous to chartaceous, margins and/or apices sometimes scarious.</text>
      <biological_entity id="o148" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s7" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s7" to="chartaceous" />
      </biological_entity>
      <biological_entity id="o149" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="8" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o150" name="margin" name_original="margins" src="d0_s7" type="structure" />
      <biological_entity id="o151" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o148" id="r14" name="in" negation="false" src="d0_s7" to="o149" />
    </statement>
    <statement id="d0_s8">
      <text>Receptacles flat to convex, usually epaleate (often foveolate, sometimes setose).</text>
      <biological_entity id="o152" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s8" to="convex" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 0 (corollas of peripheral florets enlarged, zygomorphic, ± raylike in Stokesia).</text>
      <biological_entity id="o153" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets bisexual, fertile;</text>
      <biological_entity id="o154" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas white, ochroleucous, or pink to cyanic [yellow];</text>
      <biological_entity id="o155" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="cyanic" />
        <character char_type="range_value" from="pink" name="coloration" src="d0_s11" to="cyanic" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anther bases ± sagittate [tailed], apical appendages ovate to lanceolate;</text>
      <biological_entity constraint="anther" id="o156" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s12" value="sagittate" value_original="sagittate" />
      </biological_entity>
      <biological_entity constraint="apical" id="o157" name="appendage" name_original="appendages" src="d0_s12" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles abaxially hirsutulous (at least distally), branches lance-linear to ± lanceolate, adaxially continuously stigmatic from bases nearly to apices, apices acute, appendages essentially none.</text>
      <biological_entity id="o158" name="style" name_original="styles" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s13" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
      <biological_entity id="o159" name="branch" name_original="branches" src="d0_s13" type="structure">
        <character char_type="range_value" from="lance-linear" name="shape" src="d0_s13" to="more or less lanceolate" />
        <character constraint="from bases" constraintid="o160" is_modifier="false" modifier="adaxially continuously; continuously" name="structure_in_adjective_form" src="d0_s13" value="stigmatic" value_original="stigmatic" />
      </biological_entity>
      <biological_entity id="o160" name="base" name_original="bases" src="d0_s13" type="structure" />
      <biological_entity id="o161" name="apex" name_original="apices" src="d0_s13" type="structure" />
      <biological_entity id="o162" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o163" name="appendage" name_original="appendages" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="essentially" name="quantity" src="d0_s13" value="0" value_original="none" />
      </biological_entity>
      <relation from="o160" id="r15" name="to" negation="false" src="d0_s13" to="o161" />
    </statement>
    <statement id="d0_s14">
      <text>Cypselae ± monomorphic within heads, columnar to clavate, fusiform, or prismatic, sometimes compressed, not beaked, bodies smooth, nerved, or ribbed (glabrous or hirsutulous to strigillose, sometimes resin-gland-dotted as well);</text>
      <biological_entity id="o164" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character constraint="within heads" constraintid="o165" is_modifier="false" modifier="more or less" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="columnar" name="shape" notes="" src="d0_s14" to="clavate fusiform or prismatic" />
        <character char_type="range_value" from="columnar" name="shape" src="d0_s14" to="clavate fusiform or prismatic" />
        <character char_type="range_value" from="columnar" name="shape" src="d0_s14" to="clavate fusiform or prismatic" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s14" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o165" name="head" name_original="heads" src="d0_s14" type="structure" />
      <biological_entity id="o166" name="body" name_original="bodies" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="nerved" value_original="nerved" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="ribbed" value_original="ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi usually persistent, usually in 2 series (outer series of shorter, stouter bristles or narrow scales, inner of longer, usually barbellate bristles), sometimes in 1 series (bristles or scales, scales often aristate).</text>
      <biological_entity id="o167" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o168" name="series" name_original="series" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o169" name="series" name_original="series" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <relation from="o167" id="r16" modifier="usually" name="in" negation="false" src="d0_s15" to="o168" />
      <relation from="o167" id="r17" modifier="sometimes" name="in" negation="false" src="d0_s15" to="o169" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly tropics and warm-temperate regions of New World and Old World.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly tropics and warm-temperate regions of New World and Old World" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>187d.</number>
  <discussion>Genera 100–140, species ca. 1300 (6 genera, 25 species in the flora).</discussion>
  <discussion>Most members of Vernonieae are herbs, subshrubs, or shrubs (Vernonia arborea Buchanan-Hamilton of tropical Asia may form trees to 33 m). They are characterized by discoid heads of bisexual florets with purple to pink or white corollas, calcarate anthers, attenuate, abaxially hirsutulous style branches stigmatic ± uniformly (rather than in two lines or bands) nearly to their tips, and pollen grains with regular, polygonal, patterns of ± spiny to smooth ridges. Centers of species concentration for the tribe are found in Africa, Madagascar, South America, and Antilles. In the flora, most species are found in the eastern and southern states of the United States. The plants are often associated with open, prairie or savanna-like areas.</discussion>
  <discussion>Treating clades recognized by J. L. Panero and V. A. Funk (2002) as corresponding to tribes, Vernonieae is sister to Liabeae (none in the flora) and is included with Arctotideae (introduced), Cichorieae, and Gundelieae (none in the flora) within Cichorioideae.</discussion>
  <discussion>Historically, 80% or so of the species in the tribe were included in Vernonia. H. Robinson (1999) has argued for resurrections and recircumscriptions of some old genera and recognition of some “new” genera, resulting in a Vernonia of ca. 20 species.</discussion>
  <discussion>Stokesia laevis and some Vernonia species are grown as ornamentals. Some Vernonia species have been used medicinally in folk remedies and some may be locally troublesome as weeds (e.g., V. baldwinii).</discussion>
  <references>
    <reference>Robinson, H. 1999. Generic and subtribal classification of American Vernonieae. Smithsonian Contr. Bot. 89.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads pseudo-radiant (corollas of peripheral, bisexual florets enlarged, zygomorphic); margins of phyllaries (at least the outer), pectinately spinose-toothed</description>
      <determination>28 Stokesia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Heads ± discoid; margins of phyllaries not pectinately spinose-toothed</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads sessile, borne in congested clusters; florets (1–)4(–5) in each head</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Heads mostly pedunculate, not borne in congested clusters; florets 9–100+ in each head</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Heads (1–)10–40 per cluster, each cluster subtended by (2–)3 ± deltate bracts; pappi of 5(–6) 1-aristate scales (look closely for squamiform, gradually to abruptly tapering base of each arista), no scales tipped with plicate aristae</description>
      <determination>29 Elephantopus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Heads 1–5+ per cluster, each cluster subtended by 1–2 lanceolate to spatulate or linear bracts; pappi of 6–10 ± laciniate to aristate scales, 2(–3+) of aristate scales each with awnlike arista plicate (2-folded) distally</description>
      <determination>30 Pseudelephantopus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Annuals (perhaps persisting); cypselae not ribbed</description>
      <determination>31 Cyanthillium</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Perennials or functionally annuals; cypselae 8–10-ribbed</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Heads each subtended by 3–8+, ± foliaceous bracts; pappi caducous</description>
      <determination>32 Centratherum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Heads not each subtended by foliaceous bracts; pappi persistent</description>
      <determination>33 Vernonia</determination>
    </key_statement>
  </key>
</bio:treatment>