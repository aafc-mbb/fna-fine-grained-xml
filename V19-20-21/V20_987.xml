<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">427</other_info_on_meta>
    <other_info_on_meta type="treatment_page">430</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Willdenow" date="1807" rank="genus">grindelia</taxon_name>
    <taxon_name authority="(Steyermark) G. L. Nesom" date="1992" rank="species">adenodonta</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>73: 327. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus grindelia;species adenodonta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066805</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Grindelia</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">microcephala</taxon_name>
    <taxon_name authority="Steyermark" date="unknown" rank="variety">adenodonta</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>21: 467. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Grindelia;species microcephala;variety adenodonta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 30–130 cm.</text>
      <biological_entity id="o14900" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="130" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, stramineous to purple, proximally sparsely hirtellous to glabrate, distally ± hirtellous to villosulous.</text>
      <biological_entity id="o14901" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s1" to="purple" />
        <character char_type="range_value" from="proximally sparsely hirtellous" name="pubescence" src="d0_s1" to="glabrate distally more or less hirtellous" />
        <character char_type="range_value" from="proximally sparsely hirtellous" name="pubescence" src="d0_s1" to="glabrate distally more or less hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaf-blades ovate or ± triangular to oblong or obovate, 15–60 (–90) mm, lengths 1.5–3 (–4+) times widths, bases ± clasping, margins ± crenate (teeth 8–14 per cm, blunt, resin-tipped), apices obtuse to acute, faces usually hirtellous to scabridulous and glandular (glands usually in pits, sometimes sessile, seldom stipitate), sometimes glabrate.</text>
      <biological_entity constraint="cauline" id="o14902" name="blade-leaf" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="more or less triangular" name="shape" src="d0_s2" to="oblong or obovate" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s2" to="90" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="distance" src="d0_s2" to="60" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="1.5-3(-4+)" value_original="1.5-3(-4+)" />
      </biological_entity>
      <biological_entity id="o14903" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o14904" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o14905" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s2" to="acute" />
      </biological_entity>
      <biological_entity id="o14906" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" name="relief" src="d0_s2" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in open, corymbiform to paniculiform arrays or borne singly.</text>
      <biological_entity id="o14907" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="corymbiform" modifier="in open" name="architecture" src="d0_s3" to="paniculiform" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres ± urceolate to hemispheric, 8–12 × 10–20 mm (usually subtended by leaflike bracts).</text>
      <biological_entity id="o14908" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="less urceolate" name="shape" src="d0_s4" to="hemispheric" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries in 4–6 series, spreading to appressed, lanceolate to linear, apices subulate, hooked to ± recurved or nearly straight, moderately resinous.</text>
      <biological_entity id="o14909" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s5" to="appressed" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="linear" />
      </biological_entity>
      <biological_entity id="o14910" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o14911" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="hooked" value_original="hooked" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s5" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="nearly; nearly" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="moderately" name="coating" src="d0_s5" value="resinous" value_original="resinous" />
      </biological_entity>
      <relation from="o14909" id="r1362" name="in" negation="false" src="d0_s5" to="o14910" />
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 20–27;</text>
      <biological_entity id="o14912" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s6" to="27" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 8–12 mm.</text>
      <biological_entity id="o14913" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae stramineous or brownish, 3–4.5 mm, apices ± coronate to knobby, faces (outer) rugose (not transversely fissured; angles ± ribbed) or (inner) striate;</text>
      <biological_entity id="o14914" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14915" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="less coronate" name="shape" src="d0_s8" to="knobby" />
      </biological_entity>
      <biological_entity id="o14916" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="relief" src="d0_s8" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="relief" src="d0_s8" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi of 2 straight or weakly contorted, smooth (apices dilated), bristles or setiform awns 5–6 mm, equaling or surpassing disc corollas.</text>
      <biological_entity id="o14918" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="true" modifier="weakly" name="arrangement_or_shape" src="d0_s9" value="contorted" value_original="contorted" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s9" value="setiform" value_original="setiform" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14919" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="true" modifier="weakly" name="arrangement_or_shape" src="d0_s9" value="contorted" value_original="contorted" />
        <character is_modifier="true" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s9" value="setiform" value_original="setiform" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="disc" id="o14920" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s9" value="surpassing" value_original="surpassing" />
      </biological_entity>
      <relation from="o14917" id="r1363" name="consist_of" negation="false" src="d0_s9" to="o14918" />
      <relation from="o14917" id="r1364" name="consist_of" negation="false" src="d0_s9" to="o14919" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 12.</text>
      <biological_entity id="o14917" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character is_modifier="false" name="variability" notes="" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14921" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, thickets, along streams</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="streams" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  
</bio:treatment>