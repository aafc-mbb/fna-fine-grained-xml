<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="treatment_page">550</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Spach" date="1841" rank="genus">ageratina</taxon_name>
    <taxon_name authority="(Fernald) R. M. King &amp; H. Robinson" date="1970" rank="species">luciae-brauniae</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>19: 215. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus ageratina;species luciae-brauniae</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066018</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="species">luciae-brauniae</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>44: 463. 1942,</place_in_publication>
      <other_info_on_pub>based on E. deltoides E. L. Braun, Rhodora 42: 50. 1940, not Jacquin 1798</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Eupatorium;species luciae-brauniae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–60 cm.</text>
      <biological_entity id="o2766" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, glabrous.</text>
      <biological_entity id="o2767" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o2768" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 25–70 mm;</text>
      <biological_entity id="o2769" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s3" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades broadly ovate-deltate, 4–8 × 5–9 cm, (thin, delicate) bases truncate to subcordate, margins coarsely dentate, apices acute to acuminate, abaxial faces glabrous or sparsely puberulent.</text>
      <biological_entity id="o2770" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="ovate-deltate" value_original="ovate-deltate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s4" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2771" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o2772" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o2773" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2774" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads clustered.</text>
      <biological_entity id="o2775" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s5" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 1–3 mm, glabrous or sparsely puberulent.</text>
      <biological_entity id="o2776" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 3.5–4 mm.</text>
      <biological_entity id="o2777" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries: apices acuminate, abaxial faces glabrous or sparsely puberulent.</text>
      <biological_entity id="o2778" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure" />
      <biological_entity id="o2779" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2780" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas white, lobes glabrous or sparsely puberulent.</text>
      <biological_entity id="o2781" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o2782" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae sparsely and evenly hirtellous.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 34.</text>
      <biological_entity id="o2783" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s10" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2784" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Under overhanging sandstone (Pottsville formation) cliffs and ledges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="overhanging sandstone" modifier="under" />
        <character name="habitat" value="pottsville formation" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="ledges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ky., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Rockhouse white snakeroot</other_name>
  <discussion>Ageratina luciae-brauniae was treated by A. F. Clewell and J. W. Wooten (1971) as a synonym of A. altissima and regarded by them as “bizarre plants showing extreme signs of etiolation from growing under limestone ledges” (p. 134). B. E. Wofford (1976) observed that greenhouse transplants of both species maintained distinctions that provide rationale for maintaining A. luciae-brauniae at specific rank.</discussion>
  <discussion>Ageratina luciae-brauniae is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  <references>
    <reference>Wofford, B. E. 1976. The taxonomic status of Ageratina luciae-brauniae (Fern.) King &amp; H. Robins. Phytologia 33: 369–371.</reference>
  </references>
  
</bio:treatment>