<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">30</other_info_on_meta>
    <other_info_on_meta type="treatment_page">31</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">baccharis</taxon_name>
    <taxon_name authority="A. Gray" date="1879" rank="species">plummerae</taxon_name>
    <taxon_name authority="Hoover" date="1970" rank="subspecies">glabrata</taxon_name>
    <place_of_publication>
      <publication_title>Vasc. Pl. San Luis Obispo Co.,</publication_title>
      <place_in_publication>302. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus baccharis;species plummerae;subspecies glabrata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068084</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–80 cm.</text>
      <biological_entity id="o14751" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems glabrate, glandular.</text>
      <biological_entity id="o14752" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves linear to narrowly oblanceolate (principal 1-nerved or 3-nerved), 8–35 × 1–2 (–3) mm, margins entire or sharply serrate.</text>
      <biological_entity id="o14753" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="narrowly oblanceolate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="35" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14754" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes near beaches, sea bluffs, serpentine rock outcrops, chaparral, brushy canyons, woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" constraint="near beaches , sea bluffs , serpentine rock outcrops , chaparral , brushy canyons , woodlands" />
        <character name="habitat" value="beaches" />
        <character name="habitat" value="sea bluffs" />
        <character name="habitat" value="serpentine rock outcrops" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="brushy canyons" />
        <character name="habitat" value="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12b.</number>
  <other_name type="common_name">San Simeon or smooth baccharis</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Known only from coastal scrub in the Santa Lucia Range, northwestern San Luis Obispo County, subsp. glabrata is generally reduced in most characters than subsp. plummerae, with smaller heads and fewer florets (R. M. Beauchamp and J. Henrickson 1995).</discussion>
  
</bio:treatment>