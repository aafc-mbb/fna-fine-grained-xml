<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">336</other_info_on_meta>
    <other_info_on_meta type="treatment_page">346</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="unknown" rank="subtribe">baeriinae</taxon_name>
    <taxon_name authority="Cassini" date="1834" rank="genus">lasthenia</taxon_name>
    <taxon_name authority="(H. M. Hall) Ornduff" date="1966" rank="section">platycarpha</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1894" rank="species">platycarpha</taxon_name>
    <place_of_publication>
      <publication_title>Man. Bot. San Francisco,</publication_title>
      <place_in_publication>205. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe baeriinae;genus lasthenia;section platycarpha;species platycarpha</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067058</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Burrielia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">platycarpha</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 97. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Burrielia;species platycarpha;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Baeria</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="unknown" rank="species">platycarpha</taxon_name>
    <taxon_hierarchy>genus Baeria;species platycarpha;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, to 30 cm.</text>
      <biological_entity id="o12858" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched proximally, glabrous or woolly to villous, especially distally.</text>
      <biological_entity id="o12859" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="woolly" name="pubescence" src="d0_s1" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves linear, 10–60 × 1–2+ mm (simple blades or single lobes), margins usually lobed, sometimes entire, faces glabrous or hairy.</text>
      <biological_entity id="o12860" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="60" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12861" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o12862" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Involucres obconic, 6–8 mm.</text>
      <biological_entity id="o12863" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 6–9, elliptic to ovate, glabrous or villous.</text>
      <biological_entity id="o12864" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s4" to="9" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles conic, muricate, glabrous or sparsely hairy.</text>
      <biological_entity id="o12865" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="conic" value_original="conic" />
        <character is_modifier="false" name="relief" src="d0_s5" value="muricate" value_original="muricate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 6–13;</text>
      <biological_entity id="o12866" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s6" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae elliptic, 7–8 mm.</text>
      <biological_entity id="o12867" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Anther appendages deltate.</text>
      <biological_entity constraint="anther" id="o12868" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae black to gray, narrowly clavate, 1.5–3.5 mm, hairy;</text>
      <biological_entity id="o12869" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="black" name="coloration" src="d0_s9" to="gray" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 4–6 white or yellowish, lanceolate to ovate, aristate scales.</text>
      <biological_entity id="o12871" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="6" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s10" to="ovate" />
        <character is_modifier="true" name="shape" src="d0_s10" value="aristate" value_original="aristate" />
      </biological_entity>
      <relation from="o12870" id="r880" name="consist_of" negation="false" src="d0_s10" to="o12871" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 8.</text>
      <biological_entity id="o12870" name="pappus" name_original="pappi" src="d0_s10" type="structure" />
      <biological_entity constraint="2n" id="o12872" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkali flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkali flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Alkali goldfields</other_name>
  <discussion>Lasthenia platycarpha is known only from highly saline soils and is frequently found with species of sects. Hologymne and Ornduffia, particularly L. fremontii. In the northern part of its range, L. platycarpha is more robust and has longer pappus scales and more densely pubescent peduncles. Plants with entire leaves resemble species of sect. Amphiachaenia; L. platycarpha does not have anthochlor pigments that turn red in aqueous alkali.</discussion>
  
</bio:treatment>