<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">351</other_info_on_meta>
    <other_info_on_meta type="mention_page">353</other_info_on_meta>
    <other_info_on_meta type="treatment_page">352</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">aphanostephus</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="species">riddellii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 189. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus aphanostephus;species riddellii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066096</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–40 (–50) cm (caudices usually woody, stems 1–5+);</text>
      <biological_entity id="o29717" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>vestiture sparsely hispidulous to puberulent, stem hairs spreading to deflexed or antrorse, 0.2–0.5 mm.</text>
      <biological_entity id="o29718" name="vestiture" name_original="vestiture" src="d0_s1" type="structure">
        <character char_type="range_value" from="sparsely hispidulous" name="pubescence" src="d0_s1" to="puberulent" />
      </biological_entity>
      <biological_entity constraint="stem" id="o29719" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s1" to="deflexed or antrorse" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s1" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllary apices long-acuminate.</text>
      <biological_entity constraint="phyllary" id="o29720" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ray-florets 30–75.</text>
      <biological_entity id="o29721" name="ray-floret" name_original="ray-florets" src="d0_s3" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s3" to="75" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc-floret corollas: bases indurate, not conspicuously swollen.</text>
      <biological_entity constraint="disc-floret" id="o29722" name="corolla" name_original="corollas" src="d0_s4" type="structure" />
      <biological_entity id="o29723" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="indurate" value_original="indurate" />
        <character is_modifier="false" modifier="not conspicuously" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae: hairs apically glochidiate;</text>
      <biological_entity id="o29724" name="cypsela" name_original="cypselae" src="d0_s5" type="structure" />
      <biological_entity id="o29725" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="apically" name="pubescence" src="d0_s5" value="glochidiate" value_original="glochidiate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pappi coroniform, minutely ciliate or lacerate, 0.1–0.3 mm. 2n = 10, 20.</text>
      <biological_entity id="o29726" name="cypsela" name_original="cypselae" src="d0_s6" type="structure" />
      <biological_entity id="o29727" name="pappus" name_original="pappi" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="coroniform" value_original="coroniform" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s6" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lacerate" value_original="lacerate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29728" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="10" value_original="10" />
        <character name="quantity" src="d0_s6" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Aug(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sites, often calcareous, often with scrubby oaks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sites" />
        <character name="habitat" value="calcareous" modifier="often" />
        <character name="habitat" value="scrubby oaks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Riddell’s lazydaisy</other_name>
  <discussion>Aphanostephus riddellii is the only perennial in the genus. The caudex usually is woody, bearing numerous stems. The distal cauline leaves (upper half of stem) usually are linear, the basal and proximal cauline are conspicuously broader, spatulate, and dentate to pinnatifid. In other species, distal leaves usually are more similar to proximal.</discussion>
  
</bio:treatment>