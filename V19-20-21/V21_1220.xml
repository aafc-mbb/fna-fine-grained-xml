<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">485</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Cavanilles" date="1797" rank="genus">stevia</taxon_name>
    <taxon_name authority="Willdenow" date="1809" rank="species">ovata</taxon_name>
    <taxon_name authority="Grashoff" date="1974" rank="variety">texana</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>26: 367 fig. 11. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus stevia;species ovata;variety texana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068821</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 40–80 cm.</text>
      <biological_entity id="o8264" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly opposite, distal sometimes alternate;</text>
      <biological_entity id="o8265" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8266" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s1" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petioles (0–) 2–10 mm;</text>
      <biological_entity id="o8267" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades (raised venation mostly including only midvein and primary laterals) ovate to trullate, mostly 3–6 cm, margins serrate.</text>
      <biological_entity id="o8268" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="trullate" />
        <character char_type="range_value" from="3" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8269" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in ± congested, compact clusters.</text>
      <biological_entity id="o8270" name="head" name_original="heads" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0 or 1–2 mm, sessile-glandular and finely villous.</text>
      <biological_entity id="o8271" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s5" value="1-2 mm" value_original="1-2 mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 4–5.5 (–6) mm.</text>
      <biological_entity id="o8272" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries sessile-glandular and finely villous, apices rounded to blunt or obtuse.</text>
      <biological_entity id="o8273" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="sessile-glandular" value_original="sessile-glandular" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o8274" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="blunt or obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas white or light pink, lobes sessile-glandular.</text>
      <biological_entity id="o8275" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="light pink" value_original="light pink" />
      </biological_entity>
      <biological_entity id="o8276" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s8" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pappi shorter than corollas.</text>
      <biological_entity id="o8277" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character constraint="than corollas" constraintid="o8278" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o8278" name="corolla" name_original="corollas" src="d0_s9" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4a.</number>
  <other_name type="common_name">Texas candyleaf</other_name>
  <discussion>The type of Stevia rhombifolia Kunth, a name that has been used for plants of var. texana, is referable to var. ovata.</discussion>
  
</bio:treatment>