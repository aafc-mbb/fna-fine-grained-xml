<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="treatment_page">234</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crepis</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">rubra</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 806. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus crepis;species rubra</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250066453</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 4–40 cm (taproots shallow).</text>
      <biological_entity id="o9780" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–8, decumbent to ascending, scapiform, branched proximally, glabrate to tomentulose.</text>
      <biological_entity id="o9781" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="8" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s1" value="scapiform" value_original="scapiform" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s1" to="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>petiolate;</text>
      <biological_entity id="o9782" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (at least basal) oblanceolate or runcinate, 2–15 × 0.5–3 cm, (bases attenuate) margins pinnately lobed to dentate, apices acute, faces hirsute.</text>
      <biological_entity id="o9783" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="runcinate" value_original="runcinate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9784" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="pinnately lobed" name="shape" src="d0_s4" to="dentate" />
      </biological_entity>
      <biological_entity id="o9785" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o9786" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 (–2), usually borne singly (peduncles scapiform).</text>
      <biological_entity id="o9787" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="2" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi of 8–10, ovatelanceolate, glabrous bractlets 4–8 mm.</text>
      <biological_entity id="o9788" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o9789" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s6" to="10" />
        <character is_modifier="true" name="shape" src="d0_s6" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="true" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o9788" id="r906" name="consist_of" negation="false" src="d0_s6" to="o9789" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindro-campanulate, 11–15 × 4–7 mm.</text>
      <biological_entity id="o9790" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 8–14, (dark medially), lanceolate, 10–12 mm, (margins yellowish) apices acute, abaxial faces sparsely to densely stipitate-glandular, adaxial with fine, appressed hairs.</text>
      <biological_entity id="o9791" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s8" to="14" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9792" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9793" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9794" name="face" name_original="faces" src="d0_s8" type="structure" />
      <biological_entity id="o9795" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="fine" value_original="fine" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o9794" id="r907" name="with" negation="false" src="d0_s8" to="o9795" />
    </statement>
    <statement id="d0_s9">
      <text>Florets 40–100;</text>
      <biological_entity id="o9796" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s9" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas pink or white, 16–17 mm.</text>
      <biological_entity id="o9797" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="16" from_unit="mm" name="some_measurement" src="d0_s10" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae dimorphic, dark-brown, fusiform: outer curved, 8–9 mm, coarsely beaked, inner straight, 12–21 mm, finely beaked, ribs 10 (sharply spiculate);</text>
      <biological_entity id="o9798" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s11" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s11" value="fusiform" value_original="fusiform" />
      </biological_entity>
      <biological_entity constraint="outer" id="o9799" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="curved" value_original="curved" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s11" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity constraint="inner" id="o9800" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s11" to="21" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s11" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o9801" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi yellowish white to dusky white (fine), 5–8 mm. 2n = 10.</text>
      <biological_entity id="o9802" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s12" value="fusiform" value_original="fusiform" />
      </biological_entity>
      <biological_entity id="o9803" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="yellowish white" name="coloration" src="d0_s12" to="dusky white" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9804" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky fields, waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky fields" />
        <character name="habitat" value="waste places" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; Eurasia; also introduced widely.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="also  widely" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <other_name type="common_name">Red hawksbeard</other_name>
  <discussion>Native to the Mediterranean region and Asia Minor, Crepis rubra is widely cultivated throughout the world and occasionally escapes. It can be easily recognized by its annual habit, scapiform stems, relatively large, often single heads, and pink or white corollas. Wild plants are shorter than cultivated ones.</discussion>
  
</bio:treatment>