<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">127</other_info_on_meta>
    <other_info_on_meta type="treatment_page">126</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray in A. Gray et al." date="1884" rank="subsection">glomeruliflorae</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="species">curtisii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 200. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection glomeruliflorae;species curtisii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067539</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Aiton" date="unknown" rank="species">ambigua</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Alph. Wood" date="unknown" rank="variety">curtisii</taxon_name>
    <taxon_hierarchy>genus Solidago;species ambigua;variety curtisii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">caesia</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) C. E. S. Taylor &amp; R. J. Taylor" date="unknown" rank="variety">curtisii</taxon_name>
    <taxon_hierarchy>genus Solidago;species caesia;variety curtisii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (20–) 40–90 (–100) cm;</text>
      <biological_entity id="o15559" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices woody.</text>
      <biological_entity id="o15560" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–4, erect, straight, glabrous or moderately hirtello-strigose.</text>
      <biological_entity id="o15561" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="4" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s2" value="hirtello-strigose" value_original="hirtello-strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal withering by flowering;</text>
      <biological_entity id="o15562" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o15563" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="by flowering" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal to mid cauline weakly petiolate or sessile, blades lanceolate to elliptic, (36–) 95–150 (–180) × (10–) 19–43 (–60) mm, margins serrate (with (3–) 8–20 (–36) teeth), faces glabrous or sparsely hairy, more so along nerves;</text>
      <biological_entity id="o15564" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="proximal" name="position" src="d0_s4" to="mid" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o15565" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="elliptic" />
        <character char_type="range_value" from="36" from_unit="mm" name="atypical_length" src="d0_s4" to="95" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="150" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="180" to_unit="mm" />
        <character char_type="range_value" from="95" from_unit="mm" name="length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_width" src="d0_s4" to="19" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="43" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="19" from_unit="mm" name="width" src="d0_s4" to="43" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15566" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character constraint="with teeth" constraintid="o15567" is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o15567" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s4" to="8" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s4" to="36" />
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s4" to="20" />
      </biological_entity>
      <biological_entity id="o15568" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o15569" name="nerve" name_original="nerves" src="d0_s4" type="structure" />
      <relation from="o15568" id="r1422" name="along" negation="false" src="d0_s4" to="o15569" />
    </statement>
    <statement id="d0_s5">
      <text>distal cauline sessile, blades narrowly lanceolate to narrowly elliptic, (5–) 37–90 (–130) × (3.5) 5–18 (–34) mm, margins entire to sparsely serrate (0–9 (–14) teeth), faces glabrous or sparsely hairy, sometimes more pilose along nerves.</text>
      <biological_entity id="o15570" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="distal cauline" id="o15571" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o15572" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s5" to="narrowly elliptic" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s5" to="37" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="130" to_unit="mm" />
        <character char_type="range_value" from="37" from_unit="mm" name="length" src="d0_s5" to="90" to_unit="mm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="34" to_unit="mm" />
        <character name="atypical_width" src="d0_s5" unit="mm" value="3.5" value_original="3.5" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15573" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s5" to="sparsely serrate" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <biological_entity id="o15574" name="tooth" name_original="teeth" src="d0_s5" type="structure" />
      <biological_entity id="o15575" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
        <character constraint="along nerves" constraintid="o15576" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o15576" name="nerve" name_original="nerves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Heads 20–800 in short axillary and terminal racemiform/paniculiform, non-secund arrays (2–) 8.5–38.5 (–65) cm.</text>
      <biological_entity id="o15577" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in axillary" constraintid="o15578" from="20" name="quantity" src="d0_s6" to="800" />
        <character is_modifier="false" name="position_or_structure_subtype" notes="" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="non-secund" value_original="non-secund" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="8.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="38.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="65" to_unit="cm" />
        <character char_type="range_value" from="8.5" from_unit="cm" name="some_measurement" src="d0_s6" to="38.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15578" name="axillary" name_original="axillary" src="d0_s6" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 2–6 mm, moderately to densely finely strigose;</text>
      <biological_entity id="o15579" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to densely finely" name="pubescence" src="d0_s7" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles 0–3, linear-oblong.</text>
      <biological_entity id="o15580" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="linear-oblong" value_original="linear-oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres narrowly campanulate, (5–) 5.6–7 (–8) mm.</text>
      <biological_entity id="o15581" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="5.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="5.6" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–4 series, strongly unequal, outermost 1–1.4 (–2) mm, innermost (2.5–) 3–4 (–4.4) mm, obtuse to acute, 1-nerved.</text>
      <biological_entity id="o15582" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="size" notes="" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o15583" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o15584" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="innermost" id="o15585" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4.4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s10" to="acute" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-nerved" value_original="1-nerved" />
      </biological_entity>
      <relation from="o15582" id="r1423" name="in" negation="false" src="d0_s10" to="o15583" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 2–4 (–6);</text>
      <biological_entity id="o15586" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="6" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s11" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae (2–) 2.5–4 (–4.6) × 1–2 mm.</text>
      <biological_entity id="o15587" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s12" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 3–7 (–9);</text>
      <biological_entity id="o15588" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="9" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas mostly 2–3 mm, lobes 1–1.7 (–2.4) mm.</text>
      <biological_entity id="o15589" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15590" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae 1–2 (–3) mm, sparsely to moderately strigose;</text>
      <biological_entity id="o15591" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi (2–) 2.7–3.6 (–4.5) mm.</text>
      <biological_entity id="o15592" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="2.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s16" to="3.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., Ky., Miss., N.C., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Curtis’ goldenrod</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Solidago curtisii is similar to S. caesia and may be confused with robust, pressed and dried specimens of the latter if they are mounted such that the arching habit cannot be observed.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Proximal midcauline leaf blades broadly lanceolateand sparsely hairy</description>
      <determination>22a Solidago curtisii var. curtisii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Proximal midcauline leaf blades elliptic and moderately hairy 22b. Solidago curtisii var. flaccidifolia</description>
      <next_statement_id>1</next_statement_id>
    </key_statement>
  </key>
</bio:treatment>