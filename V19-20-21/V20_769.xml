<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="treatment_page">336</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Rydberg" date="1900" rank="species">gracilis</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>1: 404. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species gracilis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066606</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="D. C. Eaton" date="unknown" rank="species">ursinus</taxon_name>
    <taxon_name authority="(Rydberg) A. Nelson" date="unknown" rank="variety">gracilis</taxon_name>
    <taxon_hierarchy>genus Erigeron;species ursinus;variety gracilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–20 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, forming diffuse systems of slender, rhizomelike caudex branches.</text>
      <biological_entity id="o10545" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o10546" name="system" name_original="systems" src="d0_s1" type="structure">
        <character is_modifier="true" name="density" src="d0_s1" value="diffuse" value_original="diffuse" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o10547" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="rhizomelike" value_original="rhizomelike" />
      </biological_entity>
      <relation from="o10545" id="r965" name="forming" negation="false" src="d0_s1" to="o10546" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending (bases usually purplish), glabrous or sparsely strigose (or hairs loosely spreading), eglandular.</text>
      <biological_entity id="o10548" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o10549" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal (purplish) and proximal cauline blades oblanceolate to oblong, 20–40 (–90) × 1–3 (–5) mm, cauline reduced distally, margins entire, glabrous or sparsely strigose (or hairs loosely spreading), eglandular.</text>
      <biological_entity constraint="basal and proximal cauline" id="o10550" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="oblong" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o10551" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o10552" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1.</text>
      <biological_entity id="o10553" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–7.5 × 10–15 mm.</text>
      <biological_entity id="o10554" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 (–4) series (margins and tips often purplish, loose, linear-lanceolate, apices spreading), sparsely strigoso-hirsute to strigose (hairs appressed or slightly loose), sometimes minutely glandular.</text>
      <biological_entity id="o10555" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="sparsely strigoso-hirsute" name="pubescence" notes="" src="d0_s7" to="strigose" />
        <character is_modifier="false" modifier="sometimes minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o10556" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o10555" id="r966" name="in" negation="false" src="d0_s7" to="o10556" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 40–100;</text>
      <biological_entity id="o10557" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s8" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas blue to pinkish purple, 7–14 mm, laminae not coiling or reflexing.</text>
      <biological_entity id="o10558" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s9" to="pinkish purple" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10559" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 4.1–5.6 mm.</text>
      <biological_entity constraint="disc" id="o10560" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.1" from_unit="mm" name="some_measurement" src="d0_s10" to="5.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 2 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o10561" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o10562" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 14–21 bristles.</text>
      <biological_entity id="o10564" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o10565" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="14" is_modifier="true" name="quantity" src="d0_s12" to="21" />
      </biological_entity>
      <relation from="o10563" id="r967" name="outer of" negation="false" src="d0_s12" to="o10564" />
      <relation from="o10563" id="r968" name="inner of" negation="false" src="d0_s12" to="o10565" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o10563" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o10566" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist slopes, creek bottoms, sagebrush meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist slopes" />
        <character name="habitat" value="creek bottoms" />
        <character name="habitat" value="meadows" modifier="sagebrush" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>150.</number>
  <other_name type="common_name">Quill fleabane</other_name>
  <discussion>Erigeron gracilis “differs from E. ursinus in the strigose and scarcely glandular pubescence of the involucre, narrower and less herbaceous phyllaries, narrower and on the average longer disc-corollas, simple or nearly simple pappus, narrower ligules, on the average, and ordinarily narrower and slightly hairier leaves. E. gracilis grows at lower elevations than E. ursinus, in a drier habitat, and has a much more restricted range” (A. Cronquist 1947, p. 162). The two species are sympatric in northwestern Wyoming.</discussion>
  
</bio:treatment>