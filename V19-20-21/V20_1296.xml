<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">572</other_info_on_meta>
    <other_info_on_meta type="mention_page">573</other_info_on_meta>
    <other_info_on_meta type="mention_page">585</other_info_on_meta>
    <other_info_on_meta type="treatment_page">583</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">packera</taxon_name>
    <taxon_name authority="(Rydberg) W. A. Weber &amp; Á. Löve" date="1981" rank="species">crocata</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>49: 46. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus packera;species crocata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067239</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">crocatus</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>24: 299. 1897,</place_in_publication>
      <other_info_on_pub>based on S. aureus Linnaeus var. croceus A. Gray, Proc. Acad. Nat. Sci. Philadelphia 15: 68. 1864, not S. croceus de Candolle 1838</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species crocatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">pyrrhochrous</taxon_name>
    <taxon_hierarchy>genus Senecio;species pyrrhochrous;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">tracyi</taxon_name>
    <taxon_hierarchy>genus Senecio;species tracyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–60+ cm;</text>
      <biological_entity id="o6385" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>perennating bases horizontal to ascending (relatively long and stout).</text>
      <biological_entity id="o6386" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="perennating" value_original="perennating" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s1" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1, rarely 2–3, clustered, glabrous.</text>
      <biological_entity id="o6387" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character char_type="range_value" from="2" modifier="rarely" name="quantity" src="d0_s2" to="3" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (and proximal cauline) petiolate (petioles narrow);</text>
      <biological_entity constraint="basal" id="o6388" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades narrowly lanceolate or ovate to oblong-ovate, 20–60+ × 10–40+ mm, bases abruptly contracted to tapering, margins subentire to crenate-dentate.</text>
      <biological_entity id="o6389" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="oblong-ovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6390" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="abruptly" name="condition_or_size" src="d0_s4" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o6391" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s4" to="crenate-dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves gradually reduced (sessile, weakly clasping; lanceolate to oblong or sublyrate, often pinnately lobed).</text>
      <biological_entity constraint="cauline" id="o6392" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 7–15+ in open, corymbiform arrays.</text>
      <biological_entity id="o6393" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o6394" from="7" name="quantity" src="d0_s6" to="15" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o6394" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles ebracteate or bracteate (bractlets inconspicuous), glabrous.</text>
      <biological_entity id="o6395" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi 0 or inconspicuous.</text>
      <biological_entity id="o6396" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 13 or 21, light green (or tips cyanic), 4–8 mm, glabrous.</text>
      <biological_entity id="o6397" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" unit="or" value="13" value_original="13" />
        <character name="quantity" src="d0_s9" unit="or" value="21" value_original="21" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="light green" value_original="light green" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8 or 13;</text>
      <biological_entity id="o6398" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" unit="or" value="8" value_original="8" />
        <character name="quantity" src="d0_s10" unit="or" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla laminae (deep yellow to orange-red) 6–8+ mm.</text>
      <biological_entity constraint="corolla" id="o6399" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 60–80+;</text>
      <biological_entity id="o6400" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s12" to="80" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corolla-tubes 4.5–5.5 mm, limbs 2.5–3.5 mm.</text>
      <biological_entity id="o6401" name="corolla-tube" name_original="corolla-tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="distance" src="d0_s13" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6402" name="limb" name_original="limbs" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1–1.5 mm, glabrous;</text>
      <biological_entity id="o6403" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 3–5 mm. 2n = 46.</text>
      <biological_entity id="o6404" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6405" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early Jul–mid Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Aug" from="early Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet meadows, along trails, rocky outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" constraint="along trails , rocky outcrops" />
        <character name="habitat" value="trails" />
        <character name="habitat" value="rocky outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Nev., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Saffron ragwort</other_name>
  <discussion>Packera crocata is characterized by deep yellow to reddish orange corollas. Some collectors have noted that there may be evidence for hybridization between P. crocata and P. dimorphophylla; the relationship between the two species needs further study.</discussion>
  
</bio:treatment>