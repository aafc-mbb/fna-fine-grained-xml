<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">142</other_info_on_meta>
    <other_info_on_meta type="treatment_page">149</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Cassini ex Dumortier" date="1827" rank="subtribe">helianthinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">helianthus</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="species">argophyllus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 318. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe helianthinae;genus helianthus;species argophyllus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066869</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 100–300 cm.</text>
      <biological_entity id="o6824" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (silvery white) erect, usually tomentose or floccose.</text>
      <biological_entity id="o6825" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o6827" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>mostly alternate;</text>
      <biological_entity id="o6826" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles 2–10 cm;</text>
      <biological_entity id="o6828" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades ovate to lanceovate, 15–25 × 10–20 cm, bases truncate to subcordate, margins entire or serrulate, abaxial faces usually floccose, sericeous, or tomentose, glanddotted.</text>
      <biological_entity id="o6829" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceovate" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s5" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6830" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="subcordate" />
      </biological_entity>
      <biological_entity id="o6831" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6832" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="sericeous" value_original="sericeous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–5.</text>
      <biological_entity id="o6833" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 2–8 cm.</text>
      <biological_entity id="o6834" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres hemispheric, 20–30 mm diam.</text>
      <biological_entity id="o6835" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s8" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 20–35, ovate to lanceovate, 15–18 × (2–) 5–8 mm, apices long-attenuate, abaxial faces usually densely white-villous, glanddotted.</text>
      <biological_entity id="o6836" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="35" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s9" to="18" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s9" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6837" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="long-attenuate" value_original="long-attenuate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6838" name="face" name_original="faces" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s9" value="white-villous" value_original="white-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Paleae 10–11 mm, 3-toothed (apices glabrous or sparsely villous).</text>
      <biological_entity id="o6839" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="3-toothed" value_original="3-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 15–20;</text>
      <biological_entity id="o6840" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s11" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 20–30 mm.</text>
      <biological_entity id="o6841" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s12" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 150+;</text>
      <biological_entity id="o6842" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="150" name="quantity" src="d0_s13" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 6.5–7.5 mm, lobes reddish;</text>
      <biological_entity id="o6843" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s14" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6844" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers dark, appendages dark (style-branches reddish).</text>
      <biological_entity id="o6845" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o6846" name="appendage" name_original="appendages" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark" value_original="dark" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Cypselae 4–6 mm, glabrate;</text>
      <biological_entity id="o6847" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi of 2 aristate scales 2–2.7 mm. 2n = 34.</text>
      <biological_entity id="o6848" name="pappus" name_original="pappi" src="d0_s17" type="structure" />
      <biological_entity id="o6849" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s17" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6850" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="34" value_original="34" />
      </biological_entity>
      <relation from="o6848" id="r496" name="consist_of" negation="false" src="d0_s17" to="o6849" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open areas, sandy soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" />
        <character name="habitat" value="sandy soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–30+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="30+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., N.C., Tex.; introduced in South America (Argentina), Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="in South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Silverleaf sunflower</other_name>
  <discussion>Helianthus argophyllus hybridizes naturally with H. annuus.</discussion>
  
</bio:treatment>