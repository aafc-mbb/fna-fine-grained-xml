<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="treatment_page">408</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">xylorhiza</taxon_name>
    <taxon_name authority="(T. J. Watson) G. L. Nesom" date="2002" rank="species">linearifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>20: 145. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus xylorhiza;species linearifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067838</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xylorhiza</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">glabriuscula</taxon_name>
    <taxon_name authority="T. J. Watson" date="unknown" rank="variety">linearifolia</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>29: 215. 1977</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Xylorhiza;species glabriuscula;variety linearifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(T. J. Watson) Cronquist" date="unknown" rank="species">linearifolia</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species linearifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 15–35 cm.</text>
      <biological_entity id="o24097" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched mostly in proximal 3/4, puberulent to coarsely stipitate-glandular.</text>
      <biological_entity id="o24098" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="in proximal 3/4" constraintid="o24099" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="puberulent" name="pubescence" notes="" src="d0_s1" to="coarsely stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o24099" name="3/4" name_original="3/4" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blades linear-oblong to linear-lanceolate, (2–) 2.5–5 mm wide, bases truncate or rounded-auriculate, margins flat, usually entire, rarely few-toothed, faces puberulent to coarsely stipitate-glandular.</text>
      <biological_entity id="o24100" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24101" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s2" to="linear-lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24102" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded-auriculate" value_original="rounded-auriculate" />
      </biological_entity>
      <biological_entity id="o24103" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="few-toothed" value_original="few-toothed" />
      </biological_entity>
      <biological_entity id="o24104" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s2" to="coarsely stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 3–11 cm.</text>
      <biological_entity id="o24105" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 10–17 × 15–30 mm.</text>
      <biological_entity id="o24106" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="17" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 13–21;</text>
      <biological_entity id="o24107" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s5" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corollas light blue to white.</text>
      <biological_entity id="o24108" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="light blue" name="coloration" src="d0_s6" to="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Style-branch appendages slightly shorter to ± equaling stigmatic lines.</text>
      <biological_entity constraint="stigmatic" id="o24110" name="line" name_original="lines" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s7" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 12, 24.</text>
      <biological_entity constraint="style-branch" id="o24109" name="appendage" name_original="appendages" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24111" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="12" value_original="12" />
        <character name="quantity" src="d0_s8" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May(–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deep canyons, sands, and clays</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deep canyons" />
        <character name="habitat" value="sands" />
        <character name="habitat" value="clays" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Moab woody-aster</other_name>
  <discussion>Xylorhiza linearifolia is known from Garfield, Grand, San Juan, and Wayne counties. Plants with few-toothed leaf margins may be indicative of hybridization with X. tortifolia, which is sympatric with X. linearifolia in the southern part of its range.</discussion>
  
</bio:treatment>