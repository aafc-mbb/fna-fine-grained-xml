<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="treatment_page">58</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(A. Gray) McClatchie" date="1894" rank="species">cuneata</taxon_name>
    <taxon_name authority="(A. Gray) H. M. Hall" date="1907" rank="variety">spathulata</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>3: 52. 1907</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species cuneata;variety spathulata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068272</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bigelowia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">spathulata</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>11: 74. 1876 (as Bigelovia)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Bigelowia;species spathulata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves petiolate;</text>
      <biological_entity id="o1777" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blades spatulate, largest (9–) 12–25 × 4–16 mm, apices obtuse to retuse.</text>
      <biological_entity id="o1778" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="size" src="d0_s1" value="largest" value_original="largest" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_length" src="d0_s1" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s1" to="25" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s1" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1779" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s1" to="retuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads mostly discoid, 8–11 × 5–7 mm.</text>
      <biological_entity id="o1780" name="head" name_original="heads" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s2" value="discoid" value_original="discoid" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s2" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllary midnerves usually evident throughout, subapical resin ducts sometimes present, thickened and expanded apically, subapical herbaceous patches rarely evident.</text>
      <biological_entity constraint="phyllary" id="o1781" name="midnerve" name_original="midnerves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually; throughout" name="prominence" src="d0_s3" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity constraint="resin" id="o1782" name="duct" name_original="ducts" src="d0_s3" type="structure" constraint_original="subapical resin">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="apically" name="size" src="d0_s3" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o1783" name="patch" name_original="patches" src="d0_s3" type="structure">
        <character is_modifier="true" name="growth_form_or_texture" src="d0_s3" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="rarely" name="prominence" src="d0_s3" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc-florets 7–15.2n = 18.</text>
      <biological_entity id="o1784" name="disc-floret" name_original="disc-florets" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s4" to="15" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1785" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky hillsides, canyon walls, rocky outwash slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="canyon walls" />
        <character name="habitat" value="rocky outwash slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., N.Mex.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10c.</number>
  
</bio:treatment>