<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="treatment_page">320</other_info_on_meta>
    <other_info_on_meta type="illustration_page">313</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">malacothrix</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1843" rank="species">sonchoides</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 486. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus malacothrix;species sonchoides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067156</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leptoseris</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">sonchoides</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 439. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Leptoseris;species sonchoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malacothrix</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">runcinata</taxon_name>
    <taxon_hierarchy>genus Malacothrix;species runcinata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, (5–) 10–25 (–50) cm.</text>
      <biological_entity id="o15630" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5, ascending to erect, branched near bases and distally, usually glabrous (sometimes glaucous), rarely stipitate-glandular.</text>
      <biological_entity id="o15631" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character constraint="near bases" constraintid="o15632" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally; usually" name="pubescence" notes="" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o15632" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: proximal narrowly oblong to elliptic, pinnately lobed (lobes 3–8+ pairs, oblong to triangular, ± equal, apices obtuse to acute), ± fleshy, ultimate margins dentate to denticulate, faces glabrous;</text>
      <biological_entity constraint="cauline" id="o15633" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o15634" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s2" to="elliptic" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o15635" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s2" to="denticulate" />
      </biological_entity>
      <biological_entity id="o15636" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal reduced (narrowly triangular to linear, bases ± dilated, ± clasping).</text>
      <biological_entity constraint="cauline" id="o15637" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o15638" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of 8–12+, ovate to lanceolate bractlets, hyaline margins 0.05–0.3 (–0.7) mm wide, usually glabrous (margins sometimes stipitate-glandular).</text>
      <biological_entity id="o15639" name="calyculus" name_original="calyculi" src="d0_s4" type="structure" />
      <biological_entity id="o15640" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s4" to="12" upper_restricted="false" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o15641" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="mm" name="width" src="d0_s4" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s4" to="0.3" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o15639" id="r1426" name="consist_of" negation="false" src="d0_s4" to="o15640" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± campanulate to hemispheric, 7–13 × 4–6 (–12+) mm.</text>
      <biological_entity id="o15642" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="less campanulate" name="shape" src="d0_s5" to="hemispheric" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="13" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="12" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Receptacles bristly.</text>
      <biological_entity id="o15643" name="receptacle" name_original="receptacles" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 75–115;</text>
      <biological_entity id="o15644" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="75" name="quantity" src="d0_s7" to="115" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas lemon yellow, 10–14 (–16) mm;</text>
      <biological_entity id="o15645" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="lemon yellow" value_original="lemon yellow" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="16" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer ligules exserted 6–10 (–13) mm.</text>
      <biological_entity constraint="outer" id="o15646" name="ligule" name_original="ligules" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="13" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae ± cylindro-fusiform to prismatic, 1.8–3 mm, ribs extending to apices, ± equal or 5 more prominent than others;</text>
      <biological_entity id="o15647" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="less cylindro-fusiform" name="shape" src="d0_s10" to="prismatic" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15648" name="rib" name_original="ribs" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character constraint="than others" constraintid="o15650" is_modifier="false" name="prominence" src="d0_s10" value="more or less equal or 5 more prominent" />
      </biological_entity>
      <biological_entity id="o15649" name="apex" name_original="apices" src="d0_s10" type="structure" />
      <biological_entity id="o15650" name="other" name_original="others" src="d0_s10" type="structure" />
      <relation from="o15648" id="r1427" name="extending to" negation="false" src="d0_s10" to="o15649" />
    </statement>
    <statement id="d0_s11">
      <text>pappi persistent, crenate crowns of 15–25+, blunt or rounded teeth.</text>
      <biological_entity id="o15651" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o15652" name="crown" name_original="crowns" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o15653" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s11" to="25" upper_restricted="false" />
        <character is_modifier="true" name="shape" src="d0_s11" value="blunt" value_original="blunt" />
        <character is_modifier="true" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o15652" id="r1428" name="consist_of" negation="false" src="d0_s11" to="o15653" />
    </statement>
    <statement id="d0_s12">
      <text>Pollen 70–100% 3-porate.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 14.</text>
      <biological_entity id="o15654" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="70-100%" name="architecture" src="d0_s12" value="3-porate" value_original="3-porate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15655" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Usually on dunes or in deep, fine sand in arroyos and on plains in Joshua tree woodlands, grasslands, Ephedra-Coleogyne associations</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dunes" modifier="usually on" />
        <character name="habitat" value="deep" modifier="or in" constraint="in arroyos" />
        <character name="habitat" value="fine sand" constraint="in arroyos" />
        <character name="habitat" value="arroyos" />
        <character name="habitat" value="plains" modifier="and on" constraint="in joshua tree woodlands , grasslands , ephedra-coleogyne associations" />
        <character name="habitat" value="joshua tree woodlands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="ephedra-coleogyne associations" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Nev., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <other_name type="common_name">Sow-thistle desertdandelion</other_name>
  <discussion>Malacothrix sonchoides grows in California in the Mojave Desert (Los Angeles, Kern, San Bernardino, and Riverside counties), the Great Basin Desert (Inyo Mountains), and barely enters the northern margins of the Sonoran Desert. It also grows in the Intermountain Region in Arizona, Colorado, Nevada, New Mexico, Utah, and Wyoming.</discussion>
  
</bio:treatment>