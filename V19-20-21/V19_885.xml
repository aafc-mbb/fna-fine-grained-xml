<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">503</other_info_on_meta>
    <other_info_on_meta type="treatment_page">519</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="(Miller) Lessing" date="1832" rank="subgenus">absinthium</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">absinthium</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 848. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus absinthium;species absinthium</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200023158</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 40–60 (–100) cm (mat-forming), aromatic.</text>
      <biological_entity id="o8941" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems gray-green (sometimes woody proximally), densely canescent to glabrescent (hairs appressed).</text>
      <biological_entity id="o8942" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-green" value_original="gray-green" />
        <character char_type="range_value" from="densely canescent" name="pubescence" src="d0_s1" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous, gray-green;</text>
      <biological_entity id="o8943" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-green" value_original="gray-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades broadly ovate, 3–8 × 1–4 cm, mostly pinnately lobed (basal 2–3-pinnatifid, lobes obovate), faces densely canescent.</text>
      <biological_entity id="o8944" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="mostly pinnately" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o8945" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="canescent" value_original="canescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (nodding) in open (diffusely branched), paniculiform arrays 10–20 (–35) × (2–) 10–13 (–15) cm.</text>
      <biological_entity id="o8946" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o8947" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="35" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_width" src="d0_s4" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s4" to="13" to_unit="cm" />
      </biological_entity>
      <relation from="o8946" id="r833" name="in" negation="false" src="d0_s4" to="o8947" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres broadly ovoid, 2–3 × 3–5 mm.</text>
      <biological_entity id="o8948" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries gray-green, densely sericeous.</text>
      <biological_entity id="o8949" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="gray-green" value_original="gray-green" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets: pistillate 9–20;</text>
      <biological_entity id="o8950" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="9" name="quantity" src="d0_s7" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bisexual 30–50;</text>
      <biological_entity id="o8951" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s8" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas 1–2 mm, glandular.</text>
      <biological_entity id="o8952" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o8953" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae (± cylindric, slightly curved, obscurely nerved), ± 0.5 mm, glabrous (shiny).</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 18.</text>
      <biological_entity id="o8954" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character modifier="more or less" name="some_measurement" src="d0_s10" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8955" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Widely cultivated, persisting from plantings, disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="plantings" modifier="widely cultivated persisting from" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask.; Calif., Colo., Conn., Idaho, Ill., Ind., Iowa, Kans., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., R.I., S.C., S.Dak., Tenn., Utah, Vt., Wash., Wis., Wyo.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <other_name type="common_name">Common wormwood</other_name>
  <other_name type="common_name">armoise absinthe</other_name>
  <discussion>Artemisia absinthium provides the flavoring as well as the psychoactive ingredient for absinthe liquor, a beverage that is illegal in some markets. Known as a powerful neurotoxin, absinthe in large quantities is addictive as well as deadly. The species is popular in the horticultural trade. Prized by gardeners for its gracefully scalloped leaves and gray-green foliage, it creates an attractive and winter-hardy flower border.</discussion>
  
</bio:treatment>