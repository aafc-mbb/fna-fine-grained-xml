<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">367</other_info_on_meta>
    <other_info_on_meta type="mention_page">377</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="treatment_page">378</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="(Cassini) Cassini in F. Cuvier" date="1820" rank="genus">eurybia</taxon_name>
    <taxon_name authority="(Alexander) G. L. Nesom" date="1995" rank="species">avita</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 259. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus eurybia;species avita</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="etymology">Alexander’s rock aster</other_info_on_name>
    <other_info_on_name type="fna_id">250066744</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Alexander" date="unknown" rank="species">avitus</taxon_name>
    <place_of_publication>
      <publication_title>Castanea</publication_title>
      <place_in_publication>4: 60. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species avitus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–50 (–80) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>in clumps, eglandular;</text>
      <biological_entity id="o25331" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizomes short, creeping, ± woody in age.</text>
      <biological_entity id="o25332" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="creeping" value_original="creeping" />
        <character constraint="in age" constraintid="o25333" is_modifier="false" modifier="more or less" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o25333" name="age" name_original="age" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems 1–5+, erect to ascending, simple, strict, slender, stiff, proximally glabrous or glabrate, distally hirtello-puberulent.</text>
      <biological_entity id="o25334" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="5" upper_restricted="false" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="course" src="d0_s3" value="strict" value_original="strict" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s3" value="hirtello-puberulent" value_original="hirtello-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline, firm, blades (1-nerved) linear to lance-linear or oblong-linear (grasslike), 25–140+ × 2–5 mm, ± coriaceous, bases sheathing, margins entire to remotely serrulate-spinose, indurate, remotely scabrous, spines thickened, apices acute, revolute, faces glabrous;</text>
      <biological_entity id="o25335" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="texture" src="d0_s4" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o25336" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="arrangement_or_course_or_shape" src="d0_s4" to="lance-linear" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="oblong-linear" value_original="oblong-linear" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="140" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s4" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o25337" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o25338" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s4" to="remotely serrulate-spinose" />
        <character is_modifier="false" name="texture" src="d0_s4" value="indurate" value_original="indurate" />
        <character is_modifier="false" modifier="remotely" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o25339" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o25340" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal and proximal cauline often withering by flowering, bases marcescent, ± long-petiolate;</text>
      <biological_entity id="o25341" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity id="o25342" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character constraint="by flowering" is_modifier="false" modifier="often" name="life_cycle" src="d0_s5" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o25343" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="condition" src="d0_s5" value="marcescent" value_original="marcescent" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s5" value="long-petiolate" value_original="long-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>mid and distal progressively sessile and reduced.</text>
      <biological_entity constraint="mid and distal" id="o25344" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="progressively" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads 3–15+ in narrow, flat-topped corymbiform arrays.</text>
      <biological_entity id="o25345" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o25346" from="3" name="quantity" src="d0_s7" to="15" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o25346" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="shape" src="d0_s7" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles densely hirtellous;</text>
      <biological_entity id="o25347" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bracts 1–2, usually immediately subtending heads.</text>
      <biological_entity id="o25348" name="bract" name_original="bracts" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o25349" name="head" name_original="heads" src="d0_s9" type="structure" />
      <relation from="o25348" id="r2338" name="usually immediately subtending" negation="false" src="d0_s9" to="o25349" />
    </statement>
    <statement id="d0_s10">
      <text>Involucres cylindro-campanulate, 7–9 mm, shorter than pappi.</text>
      <biological_entity id="o25350" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindro-campanulate" value_original="cylindro-campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
        <character constraint="than pappi" constraintid="o25351" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o25351" name="pappus" name_original="pappi" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries 30–55+ in 4–5 series, unequal, chartaceous, bases indurate, margins narrowly hyaline, scabrous, erose, fimbriate, apices appressed or reflexed, acute, sometimes acuminate (some outer), mucronate, marginally thickened, faces glabrous;</text>
      <biological_entity id="o25352" name="phyllarie" name_original="phyllaries" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o25353" from="30" name="quantity" src="d0_s11" to="55" upper_restricted="false" />
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s11" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o25353" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="5" />
      </biological_entity>
      <biological_entity id="o25354" name="base" name_original="bases" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o25355" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s11" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
      <biological_entity id="o25356" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="marginally" name="size_or_width" src="d0_s11" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o25357" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>outer lance-oblong, rounded, apical zones dark green, flat;</text>
      <biological_entity constraint="outer" id="o25358" name="phyllarie" name_original="phyllaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="lance-oblong" value_original="lance-oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="apical" id="o25359" name="zone" name_original="zones" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s12" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>inner linear-oblong, apical zones pale green, restricted to broadly acute tips.</text>
      <biological_entity constraint="inner" id="o25360" name="phyllarie" name_original="phyllaries" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="linear-oblong" value_original="linear-oblong" />
      </biological_entity>
      <biological_entity constraint="apical" id="o25361" name="zone" name_original="zones" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale green" value_original="pale green" />
      </biological_entity>
      <biological_entity id="o25362" name="tip" name_original="tips" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="broadly" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o25361" id="r2339" name="restricted to" negation="false" src="d0_s13" to="o25362" />
    </statement>
    <statement id="d0_s14">
      <text>Ray-florets 8–20;</text>
      <biological_entity id="o25363" name="ray-floret" name_original="ray-florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s14" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>corollas bluish white to lavender or deep violet, 5–10 × 1–1.7 mm.</text>
      <biological_entity id="o25364" name="corolla" name_original="corollas" src="d0_s15" type="structure">
        <character char_type="range_value" from="bluish white" name="coloration" src="d0_s15" to="lavender or deep violet" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s15" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Disc-florets 15–45;</text>
      <biological_entity id="o25365" name="disc-floret" name_original="disc-florets" src="d0_s16" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s16" to="45" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>corollas yellow, 5.5–7 mm, barely ampliate, tubes shorter than narrowly funnelform throats, lobes erect, triangular, 0.6–0.7 mm.</text>
      <biological_entity id="o25366" name="corolla" name_original="corollas" src="d0_s17" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s17" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="barely" name="size" src="d0_s17" value="ampliate" value_original="ampliate" />
      </biological_entity>
      <biological_entity id="o25367" name="tube" name_original="tubes" src="d0_s17" type="structure">
        <character constraint="than narrowly funnelform throats" constraintid="o25368" is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o25368" name="throat" name_original="throats" src="d0_s17" type="structure" />
      <biological_entity id="o25369" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s17" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s17" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Cypselae tan, fusiform, ± compressed, ca. 5 mm, ribs 7–10 (–12) (stramineous, broad), strigillose;</text>
      <biological_entity id="o25370" name="cypsela" name_original="cypselae" src="d0_s18" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s18" value="tan" value_original="tan" />
        <character is_modifier="false" name="shape" src="d0_s18" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s18" value="compressed" value_original="compressed" />
        <character name="some_measurement" src="d0_s18" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o25371" name="rib" name_original="ribs" src="d0_s18" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="12" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s18" to="10" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>pappi of cinnamon to sordid bristles 5.5–6 mm, equaling disc corollas.</text>
      <biological_entity id="o25372" name="pappus" name_original="pappi" src="d0_s19" type="structure" constraint="bristle" constraint_original="bristle; bristle">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s19" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25373" name="bristle" name_original="bristles" src="d0_s19" type="structure">
        <character char_type="range_value" from="cinnamon" is_modifier="true" name="coloration" src="d0_s19" to="sordid" />
      </biological_entity>
      <relation from="o25372" id="r2340" name="part_of" negation="false" src="d0_s19" to="o25373" />
    </statement>
    <statement id="d0_s20">
      <text>2n = 18.</text>
      <biological_entity constraint="disc" id="o25374" name="corolla" name_original="corollas" src="d0_s19" type="structure">
        <character is_modifier="true" name="variability" src="d0_s19" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25375" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow sandy soils around edges of granite flatrock outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow sandy soils" constraint="around edges of granite" />
        <character name="habitat" value="edges" constraint="of granite" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="outcrops" modifier="flatrock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga., N.C., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eurybia avita is known from Stone Mountain (the type location, where it is now extirpated according to R. Kral 1983, vol. 2) and granite flatrocks in Georgia, and from Pickens County in South Carolina, where it is imperiled; it is presumed extirpated from North Carolina (www.natureserve.org). Kral mapped the species; he underlined its similarities to both E. surculosa and E. paludosa and the need for further studies of its relationships.</discussion>
  
</bio:treatment>