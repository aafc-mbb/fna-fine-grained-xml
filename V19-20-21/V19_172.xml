<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Keil</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="treatment_page">165</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="de Candolle" date="1810" rank="genus">SAUSSUREA</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Mus. Natl. Hist. Nat.</publication_title>
      <place_in_publication>16: 156, 196, plates 10–13. 1810</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus SAUSSUREA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Nicolas Théodore (1767–1845) and Horace Bénédict (1740–1799) de Saussure, Swiss naturalists</other_info_on_name>
    <other_info_on_name type="fna_id">129338</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–120+ cm;</text>
      <biological_entity id="o17663" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>herbage tomentose or glabrescent, not spiny.</text>
      <biological_entity id="o17664" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s1" value="spiny" value_original="spiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or ascending, simple or branched.</text>
      <biological_entity id="o17665" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal or cauline (sometimes cauline only), sessile or petiolate;</text>
      <biological_entity id="o17666" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins entire or dentate to pinnately lobed, faces glabrous to densely tomentose, glandular or eglandular.</text>
      <biological_entity constraint="blade" id="o17667" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s4" value="dentate to pinnately lobed" value_original="dentate to pinnately lobed" />
      </biological_entity>
      <biological_entity id="o17668" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="densely tomentose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads discoid, borne singly or in corymbiform arrays.</text>
      <biological_entity id="o17669" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="in corymbiform arrays" value_original="in corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o17670" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o17669" id="r1589" name="in" negation="false" src="d0_s5" to="o17670" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres ovoid to campanulate or ± turbinate.</text>
      <biological_entity id="o17671" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s6" to="campanulate or more or less turbinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries many in 3–5 (–10+) series, subequal to strongly unequal, appressed or not, ovate to lanceolate, margins entire, toothed, or lobed, apices obtuse or acute, appendaged or not, not spine-tipped.</text>
      <biological_entity id="o17672" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character constraint="in series" constraintid="o17673" is_modifier="false" name="quantity" src="d0_s7" value="many" value_original="many" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character name="fixation_or_orientation" src="d0_s7" value="not" value_original="not" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o17673" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="10" upper_restricted="false" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="5" />
        <character char_type="range_value" from="subequal" name="size" notes="" src="d0_s7" to="strongly unequal" />
      </biological_entity>
      <biological_entity id="o17674" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o17675" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="appendaged" value_original="appendaged" />
        <character name="architecture" src="d0_s7" value="not" value_original="not" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s7" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Receptacles flat or convex, epaleate, smooth, usually subulate-scaly, sometimes bristly or naked.</text>
      <biological_entity id="o17676" name="receptacle" name_original="receptacles" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s8" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="epaleate" value_original="epaleate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence" src="d0_s8" value="subulate-scaly" value_original="subulate-scaly" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="bristly" value_original="bristly" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="naked" value_original="naked" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 10–20;</text>
      <biological_entity id="o17677" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white to blue or purple, tubes slender, abruptly expanded to throats, lobes linear;</text>
      <biological_entity id="o17678" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="blue or purple" />
      </biological_entity>
      <biological_entity id="o17679" name="tube" name_original="tubes" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character constraint="to throats" constraintid="o17680" is_modifier="false" modifier="abruptly" name="size" src="d0_s10" value="expanded" value_original="expanded" />
      </biological_entity>
      <biological_entity id="o17680" name="throat" name_original="throats" src="d0_s10" type="structure" />
      <biological_entity id="o17681" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anther bases short-tailed, apical appendages linear, acute;</text>
      <biological_entity constraint="anther" id="o17682" name="base" name_original="bases" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="short-tailed" value_original="short-tailed" />
      </biological_entity>
      <biological_entity constraint="apical" id="o17683" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>style-branches: fused portions with minutely hairy subterminal nodes, distinct portions oblong to linear, short-papillate.</text>
      <biological_entity id="o17684" name="style-branch" name_original="style-branches" src="d0_s12" type="structure" />
      <biological_entity id="o17685" name="portion" name_original="portions" src="d0_s12" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s12" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity constraint="subterminal" id="o17686" name="node" name_original="nodes" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="minutely" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o17687" name="portion" name_original="portions" src="d0_s12" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s12" to="linear" />
        <character is_modifier="false" name="relief" src="d0_s12" value="short-papillate" value_original="short-papillate" />
      </biological_entity>
      <relation from="o17685" id="r1590" name="with" negation="false" src="d0_s12" to="o17686" />
    </statement>
    <statement id="d0_s13">
      <text>Cypselae oblong, ± angled, cylindric or 4–5-angled, ribs (when present) smooth or roughened, apices entire, glabrous or minutely glandular, attachment scars basal;</text>
      <biological_entity id="o17688" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s13" value="4-5-angled" value_original="4-5-angled" />
      </biological_entity>
      <biological_entity id="o17689" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character is_modifier="false" name="relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s13" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o17690" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s13" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="attachment" id="o17691" name="scar" name_original="scars" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi usually of 2 series, outer of readily falling, short bristles, inner persistent or falling as unit, of basally connate, usually longer, plumose bristles.</text>
      <biological_entity id="o17692" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o17693" name="series" name_original="series" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="outer" id="o17694" name="series" name_original="series" src="d0_s14" type="structure" />
      <biological_entity id="o17695" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="readily" name="life_cycle" src="d0_s14" value="falling" value_original="falling" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o17697" name="unit" name_original="unit" src="d0_s14" type="structure" />
      <biological_entity id="o17698" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="basally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
        <character is_modifier="true" modifier="usually" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
        <character is_modifier="true" name="shape" src="d0_s14" value="plumose" value_original="plumose" />
      </biological_entity>
      <relation from="o17692" id="r1591" name="consist_of" negation="false" src="d0_s14" to="o17693" />
      <relation from="o17694" id="r1592" name="consist_of" negation="false" src="d0_s14" to="o17695" />
      <relation from="o17696" id="r1593" name="consist_of" negation="false" src="d0_s14" to="o17698" />
    </statement>
    <statement id="d0_s15">
      <text>x = 13, 14, 16, 17, 18, 19?.</text>
      <biological_entity constraint="inner" id="o17696" name="series" name_original="series" src="d0_s14" type="structure">
        <character is_modifier="false" name="duration" src="d0_s14" value="persistent" value_original="persistent" />
        <character constraint="as unit" constraintid="o17697" is_modifier="false" name="life_cycle" src="d0_s14" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o17699" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="13" value_original="13" />
        <character name="quantity" src="d0_s15" value="14" value_original="14" />
        <character name="quantity" src="d0_s15" value="16" value_original="16" />
        <character name="quantity" src="d0_s15" value="17" value_original="17" />
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
        <character name="quantity" src="d0_s15" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia, 1 in Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="1 in Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15</number>
  <other_name type="common_name">Saw-wort</other_name>
  <discussion>Species 300–400 (6 in the flora).</discussion>
  <discussion>Saussurea is a notoriously difficult, largely Asiatic genus with species boundaries often indistinct.</discussion>
  <references>
    <reference>Lipschitz, S. J. 1979. Rod Saussurea DC. (Asteraceae). Leningrad.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Outer and mid phyllaries with toothed or lobed appendages</description>
      <determination>6 Saussurea amara</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Outer and mid phyllaries entire, without appendages</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Proximal leaves ovate to lanceolate, usually more than 30 mm wide, bases broadly obtuse to truncate or cordate; plants 30–120 cm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Proximal leaves linear to elliptic, 2–25 mm wide, bases acute to acuminate; plants 3–40 cm</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Cauline leaves usually more than 20, finely to ± coarsely serrate or dentate; s Alaska and Yukon to California, Idaho, and Montana</description>
      <determination>1 Saussurea americana</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Cauline leaves usually 15 or fewer, coarsely laciniate-dentate; nw Alaska</description>
      <determination>2 Saussurea triangulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Phyllaries subequal, linear to lanceolate; receptacles naked</description>
      <determination>4 Saussurea nuda</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Phyllaries strongly unequal, the outer ovate to lanceolate, conspicuously shorter than inner; receptacles scaly</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Tips of outer and mid phyllaries acute; Alaska and nw Canada</description>
      <determination>3 Saussurea angustifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Tips of outer and mid phyllaries ± rounded; Rocky Mountains</description>
      <determination>5 Saussurea weberi</determination>
    </key_statement>
  </key>
</bio:treatment>