<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">39</other_info_on_meta>
    <other_info_on_meta type="mention_page">40</other_info_on_meta>
    <other_info_on_meta type="mention_page">41</other_info_on_meta>
    <other_info_on_meta type="treatment_page">42</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">eucephalus</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1896" rank="species">tomentellus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 55. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus eucephalus;species tomentellus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066726</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sericocarpus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">tomentellus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 283. 1889</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sericocarpus;species tomentellus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">brickellioides</taxon_name>
    <taxon_hierarchy>genus Aster;species brickellioides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Greene) Frye &amp; Rigg" date="unknown" rank="species">tomentellus</taxon_name>
    <taxon_hierarchy>genus Aster;species tomentellus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eucephalus</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">bicolor</taxon_name>
    <taxon_hierarchy>genus Eucephalus;species bicolor;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eucephalus</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom" date="unknown" rank="species">brickellioides</taxon_name>
    <taxon_hierarchy>genus Eucephalus;species brickellioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials 40–90 cm (caudices woody).</text>
      <biological_entity id="o28244" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, woolly or cottony.</text>
      <biological_entity id="o28245" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="cottony" value_original="cottony" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: mid and distal blades lanceolate to elliptic, 2.5–6 cm × 7–20 mm, abaxial faces glabrous or glabrate, adaxial ± densely woolly to cottony.</text>
      <biological_entity id="o28246" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="mid and distal" id="o28247" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="elliptic" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28248" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o28249" name="face" name_original="faces" src="d0_s2" type="structure">
        <character char_type="range_value" from="less densely woolly" name="pubescence" src="d0_s2" to="cottony" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 3–40 in racemiform to paniculiform arrays.</text>
      <biological_entity id="o28250" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o28251" from="3" name="quantity" src="d0_s3" to="40" />
      </biological_entity>
      <biological_entity id="o28251" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character char_type="range_value" from="racemiform" is_modifier="true" name="architecture" src="d0_s3" to="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles pubescent.</text>
      <biological_entity id="o28252" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate, 7–10 mm.</text>
      <biological_entity id="o28253" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 4–6 series (often reddish at margins and apices), linear-oblong to ovate (strongly unequal), apices acute, abaxial faces tomentose to stipitate-glandular.</text>
      <biological_entity id="o28254" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear-oblong" name="shape" notes="" src="d0_s6" to="ovate" />
      </biological_entity>
      <biological_entity id="o28255" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <biological_entity id="o28256" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28257" name="face" name_original="faces" src="d0_s6" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s6" to="stipitate-glandular" />
      </biological_entity>
      <relation from="o28254" id="r2618" name="in" negation="false" src="d0_s6" to="o28255" />
    </statement>
    <statement id="d0_s7">
      <text>Rays (0–) 1–3 (–6), violet-purple.</text>
      <biological_entity id="o28258" name="ray" name_original="rays" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s7" to="1" to_inclusive="false" />
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="6" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="3" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s7" value="violet-purple" value_original="violet-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae glabrous or pilose;</text>
      <biological_entity id="o28259" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappus bristles in 2 series, ± barbellate.</text>
      <biological_entity constraint="pappus" id="o28260" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" notes="" src="d0_s9" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o28261" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <relation from="o28260" id="r2619" name="in" negation="false" src="d0_s9" to="o28261" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open oak or coniferous woods, forest openings and rocky cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="oak" modifier="open" />
        <character name="habitat" value="coniferous woods" />
        <character name="habitat" value="forest openings" />
        <character name="habitat" value="rocky cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Brickellbush aster</other_name>
  <discussion>Eucephalus tomentellus grows in the Siskiyou Mountains of southwestern Oregon and northern California. It may intergrade with E. breweri and E. glabratus.</discussion>
  
</bio:treatment>