<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="treatment_page">320</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">malacothrix</taxon_name>
    <taxon_name authority="W. S. Davis &amp; P. H. Raven" date="1962" rank="species">sonorae</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>16: 264, fig. 2d. 1962</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus malacothrix;species sonorae</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067157</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–35 cm.</text>
      <biological_entity id="o11464" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1 (–9), erect, branched from bases and distally, relatively sparsely leafy, glabrous.</text>
      <biological_entity id="o11465" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="9" />
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from bases" constraintid="o11466" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally; relatively sparsely" name="architecture" notes="" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11466" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: proximal narrowly oblanceolate to obovate, usually pinnately lobed (lobes oblong to triangular), not fleshy, ultimate margins ± dentate, faces glabrous;</text>
      <biological_entity constraint="cauline" id="o11467" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o11468" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s2" to="obovate" />
        <character is_modifier="false" modifier="usually pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o11469" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o11470" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal greatly reduced (margins entire or basally dentate, apices acute).</text>
      <biological_entity constraint="cauline" id="o11471" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o11472" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="greatly" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of 5–8+, subulate to lanceolate bractlets, hyaline margins 0.05–0.2 mm.</text>
      <biological_entity id="o11473" name="calyculus" name_original="calyculi" src="d0_s4" type="structure" />
      <biological_entity id="o11474" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="8" upper_restricted="false" />
        <character char_type="range_value" from="subulate" is_modifier="true" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o11475" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s4" to="0.2" to_unit="mm" />
      </biological_entity>
      <relation from="o11473" id="r1062" name="consist_of" negation="false" src="d0_s4" to="o11474" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± campanulate, 6–9 × 4–6.6 mm.</text>
      <biological_entity id="o11476" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="9" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="6.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 12–15+ in 2 (–3) series, lance-oblong to lance-linear, hyaline margins 0.05–0.2 mm wide, faces glabrous.</text>
      <biological_entity id="o11477" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o11478" from="12" name="quantity" src="d0_s6" to="15" upper_restricted="false" />
        <character char_type="range_value" from="lance-oblong" name="shape" notes="" src="d0_s6" to="lance-linear" />
      </biological_entity>
      <biological_entity id="o11478" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o11479" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s6" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11480" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles not bristly.</text>
      <biological_entity id="o11481" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s7" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 30–61;</text>
      <biological_entity id="o11482" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s8" to="61" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white or pale-yellow, 6–10+ mm;</text>
      <biological_entity id="o11483" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer ligules exserted 1–4 mm.</text>
      <biological_entity constraint="outer" id="o11484" name="ligule" name_original="ligules" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae ± cylindro-fusiform, 1.7–2 mm, ribs ending 0.2–0.3 mm short of apices, ± equal (distal 0.2–0.3 mm of cypselae slightly expanded, smooth);</text>
      <biological_entity id="o11485" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="cylindro-fusiform" value_original="cylindro-fusiform" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11486" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character constraint="of apices" constraintid="o11487" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
        <character is_modifier="false" modifier="more or less" name="variability" notes="" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o11487" name="apex" name_original="apices" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>pappi persistent, of 16–18 needlelike teeth plus 2 bristles.</text>
      <biological_entity id="o11488" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o11489" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="16" is_modifier="true" name="quantity" src="d0_s12" to="18" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s12" value="needlelike" value_original="needlelike" />
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o11490" name="bristle" name_original="bristles" src="d0_s12" type="structure" />
      <relation from="o11488" id="r1063" name="consist_of" negation="false" src="d0_s12" to="o11489" />
    </statement>
    <statement id="d0_s13">
      <text>Pollen 70–100% 3-porate.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o11491" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="70-100%" name="architecture" src="d0_s13" value="3-porate" value_original="3-porate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11492" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, open areas among bushes, Larrea-Lycium-Cercidium-Baccharis associations, Quercus, Pinus, Juglans woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open areas" modifier="sandy" constraint="among bushes , larrea-lycium-cercidium-baccharis associations , quercus , pinus , juglans woodlands" />
        <character name="habitat" value="bushes" />
        <character name="habitat" value="larrea-lycium-cercidium-baccharis associations" />
        <character name="habitat" value="juglans woodlands" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Sonoran desertdandelion</other_name>
  <discussion>Malacothrix sonorae is found mainly in the Sonoran Desert (Tucson, Kofa, Pinal, White Tank, Baboquivari, and Waterman mountains).</discussion>
  
</bio:treatment>