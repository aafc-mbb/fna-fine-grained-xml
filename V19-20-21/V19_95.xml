<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">105</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="mention_page">128</other_info_on_meta>
    <other_info_on_meta type="mention_page">130</other_info_on_meta>
    <other_info_on_meta type="mention_page">1</other_info_on_meta>
    <other_info_on_meta type="treatment_page">129</other_info_on_meta>
    <other_info_on_meta type="illustration_page">115</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cirsium</taxon_name>
    <taxon_name authority="(Hooker) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="species">remotifolium</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>6: 655. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus cirsium;species remotifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066397</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Carduus</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">remotifolius</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 302. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Carduus;species remotifolius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–150 cm, monocarpic;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted or polycarpic, perennating by runner roots.</text>
      <biological_entity id="o17343" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monocarpic" value_original="monocarpic" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="runner" id="o17344" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s1" value="polycarpic" value_original="polycarpic" />
        <character is_modifier="false" name="duration" src="d0_s1" value="perennating" value_original="perennating" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1, erect, finely arachnoid-tomentose, sometimes villous with septate trichomes below nodes;</text>
      <biological_entity id="o17345" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
        <character constraint="with trichomes" constraintid="o17346" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o17346" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="septate" value_original="septate" />
      </biological_entity>
      <biological_entity id="o17347" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <relation from="o17346" id="r1557" name="below" negation="false" src="d0_s2" to="o17347" />
    </statement>
    <statement id="d0_s3">
      <text>branches 0–10+, slender, usually arising in distal 1/2, ascending.</text>
      <biological_entity id="o17348" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s3" to="10" upper_restricted="false" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character constraint="in distal 1/2" constraintid="o17349" is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="arising" value_original="arising" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17349" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: blades linear-oblong to oblanceolate or elliptic, 7–30 × 1–15 cm, unlobed and spinulose to dentate or shallowly to deeply pinnatifid, lobes well separated, linear to triangular-ovate, dentate to deeply lobed, main spines 2–5 mm, slender, abaxial faces green to gray, thinly to densely arachnoid-tomentose, sometimes glabrate, sometimes villous with septate trichomes along veins, adaxial green, glabrous;</text>
      <biological_entity id="o17350" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o17351" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s4" to="oblanceolate or elliptic" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="15" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="unlobed" value_original="unlobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spinulose" value_original="spinulose" />
        <character char_type="range_value" from="dentate or" name="shape" src="d0_s4" to="shallowly deeply pinnatifid" />
      </biological_entity>
      <biological_entity id="o17352" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s4" value="separated" value_original="separated" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="triangular-ovate dentate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="triangular-ovate dentate" />
      </biological_entity>
      <biological_entity constraint="main" id="o17353" name="spine" name_original="spines" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17354" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s4" to="gray" />
        <character is_modifier="false" modifier="thinly to densely" name="pubescence" src="d0_s4" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character constraint="with trichomes" constraintid="o17355" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o17355" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="septate" value_original="septate" />
      </biological_entity>
      <biological_entity id="o17356" name="vein" name_original="veins" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o17357" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o17355" id="r1558" name="along" negation="false" src="d0_s4" to="o17356" />
    </statement>
    <statement id="d0_s5">
      <text>basal sometimes present at flowering, sessile or winged-petiolate;</text>
      <biological_entity id="o17358" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o17359" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character constraint="at flowering" is_modifier="false" modifier="sometimes" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="winged-petiolate" value_original="winged-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>principal cauline mostly in proximal 1/2, winged-petiolate or sessile, bases narrowed, sometimes auriculate;</text>
      <biological_entity id="o17360" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="principal cauline" id="o17361" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="winged-petiolate" value_original="winged-petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o17362" name="1/2" name_original="1/2" src="d0_s6" type="structure" />
      <biological_entity id="o17363" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <relation from="o17361" id="r1559" name="in" negation="false" src="d0_s6" to="o17362" />
    </statement>
    <statement id="d0_s7">
      <text>distal well separated, progressively reduced, becoming bractlike, often unlobed or less deeply divided than the proximal, sometimes spinier than proximal, bases often distally expanded and auriculate-clasping.</text>
      <biological_entity id="o17364" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="distal" id="o17365" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s7" value="separated" value_original="separated" />
        <character is_modifier="false" modifier="progressively" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s7" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s7" value="unlobed" value_original="unlobed" />
        <character constraint="than the proximal leaves" constraintid="o17366" is_modifier="false" name="shape" src="d0_s7" value="less deeply divided" value_original="less deeply divided" />
        <character constraint="than proximal leaves" constraintid="o17367" is_modifier="false" name="architecture" src="d0_s7" value="sometimes spinier" value_original="sometimes spinier" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o17366" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="proximal" id="o17367" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o17368" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often distally" name="size" src="d0_s7" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s7" value="auriculate-clasping" value_original="auriculate-clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Heads few–many, borne singly or in openly branched in corymbiform, racemiform, or paniculiform arrays on main-stem and branches, sometimes also in distal axils, not closely subtended by clustered leaf bracts.</text>
      <biological_entity id="o17369" name="head" name_original="heads" src="d0_s8" type="structure">
        <character char_type="range_value" from="few" is_modifier="false" name="quantity" src="d0_s8" to="many" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="singly" value_original="singly" />
        <character constraint="in arrays" constraintid="o17370" is_modifier="false" modifier="openly" name="architecture" src="d0_s8" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o17370" name="array" name_original="arrays" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <biological_entity id="o17371" name="main-stem" name_original="main-stem" src="d0_s8" type="structure" />
      <biological_entity id="o17372" name="branch" name_original="branches" src="d0_s8" type="structure" />
      <biological_entity constraint="distal" id="o17373" name="axil" name_original="axils" src="d0_s8" type="structure" />
      <biological_entity constraint="leaf" id="o17374" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement_or_growth_form" src="d0_s8" value="clustered" value_original="clustered" />
      </biological_entity>
      <relation from="o17370" id="r1560" name="on" negation="false" src="d0_s8" to="o17371" />
      <relation from="o17370" id="r1561" name="on" negation="false" src="d0_s8" to="o17372" />
      <relation from="o17369" id="r1562" modifier="sometimes" name="in" negation="false" src="d0_s8" to="o17373" />
      <relation from="o17369" id="r1563" modifier="not closely; closely" name="subtended by" negation="false" src="d0_s8" to="o17374" />
    </statement>
    <statement id="d0_s9">
      <text>Peduncles (0–) 2–15 cm.</text>
      <biological_entity id="o17375" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres ovoid to hemispheric or campanulate, 1.5–2.5 × 1.5–3.5 cm, glabrous to arachnoid-floccose.</text>
      <biological_entity id="o17376" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s10" to="hemispheric or campanulate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s10" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s10" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s10" to="arachnoid-floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Phyllaries in 6–8 series, subequal to strongly imbricate, green, linear to obovate (outer) to linear (inner), abaxial faces with inconspicuous glutinous ridge;</text>
      <biological_entity id="o17377" name="phyllary" name_original="phyllaries" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s11" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="obovate" />
      </biological_entity>
      <biological_entity id="o17378" name="series" name_original="series" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s11" to="8" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17379" name="face" name_original="faces" src="d0_s11" type="structure" />
      <biological_entity id="o17380" name="ridge" name_original="ridge" src="d0_s11" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s11" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="true" name="coating" src="d0_s11" value="glutinous" value_original="glutinous" />
      </biological_entity>
      <relation from="o17377" id="r1564" name="in" negation="false" src="d0_s11" to="o17378" />
      <relation from="o17379" id="r1565" name="with" negation="false" src="d0_s11" to="o17380" />
    </statement>
    <statement id="d0_s12">
      <text>outer and middle bases appressed, margins entire to spinulose-dentate or broad, scarious, lacerate-dentate, spines absent or ascending to spreading, 1–2 mm;</text>
      <biological_entity constraint="outer and middle" id="o17381" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o17382" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s12" to="spinulose-dentate" />
        <character is_modifier="false" name="width" src="d0_s12" value="broad" value_original="broad" />
        <character is_modifier="false" name="texture" src="d0_s12" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="lacerate-dentate" value_original="lacerate-dentate" />
      </biological_entity>
      <biological_entity id="o17383" name="spine" name_original="spines" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s12" value="ascending to spreading" value_original="ascending to spreading" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>apices of inner sometimes flexuous or reflexed, narrow, flat, entire or expanded, scarious, and lacerate-dentate.</text>
      <biological_entity id="o17384" name="apex" name_original="apices" src="d0_s13" type="structure" constraint="base" constraint_original="base; base">
        <character is_modifier="false" modifier="sometimes" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="size_or_width" src="d0_s13" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="size" src="d0_s13" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="texture" src="d0_s13" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="lacerate-dentate" value_original="lacerate-dentate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o17385" name="base" name_original="bases" src="d0_s13" type="structure" />
      <relation from="o17384" id="r1566" name="part_of" negation="false" src="d0_s13" to="o17385" />
    </statement>
    <statement id="d0_s14">
      <text>Corollas creamy white to purple, 18–28 mm, tubes 7–12 mm, throats 5–12 mm, lobes 3.5–7 mm, style tips 4–6 mm.</text>
      <biological_entity id="o17386" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="creamy white" name="coloration" src="d0_s14" to="purple" />
        <character char_type="range_value" from="18" from_unit="mm" name="some_measurement" src="d0_s14" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17387" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17388" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17389" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="style" id="o17390" name="tip" name_original="tips" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae tan to dark-brown, 4.5–5.5 mm, apical collars differentiated or not;</text>
      <biological_entity id="o17391" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s15" to="dark-brown" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s15" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o17392" name="collar" name_original="collars" src="d0_s15" type="structure">
        <character is_modifier="false" name="variability" src="d0_s15" value="differentiated" value_original="differentiated" />
        <character name="variability" src="d0_s15" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 13–23 mm. 2n = 32.</text>
      <biological_entity id="o17393" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s16" to="23" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17394" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <other_name type="common_name">Remote-leaved or fewleaf thistle</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Cirsium remotifolium occurs from the Coast Ranges and valleys of the Pacific Northwest to the western slopes of the Cascade and Klamath ranges, south in the California North Coast Ranges to the San Francisco Bay region. It is closely related to the C. clavatum complex of the Rocky Mountains region. Both have a similar growth habit and some forms variably express the character of broadly scarious, lacerate-toothed phyllary margins. Gray, in naming Cnicus carlinoides var. americanus, included as syntypes both California and Colorado specimens. F. Petrak (1917) treated both the West Coast plants and those of the Rocky Mountains as Cirsium subsect. Americana, recognizing C. remotifolium with several infraspecific taxa plus two other species, C. callilepis and C. amblylepis from the West Coast, and four additional species from the Rocky Mountains. A. Cronquist (1955) rejected Petrak’s subspecies, treating C. remotifolium in a restricted sense, limited to plants of Washington and Oregon without dilated phyllary tips, and circumscribed C. centaureae broadly to include the Rocky Mountains and West Coast plants with dilated phyllary tips. Because of the frequent presence of dilated phyllary tips in C. remotifolium in the restricted sense, Cronquist acknowledged the likelihood of past introgression with C. centaureae in the broad sense.</discussion>
  <discussion>J. T. Howell (1960b) recognized three species: Cirsium remotifolium, C. acanthodontum, and C. callilepis, the latter with four varieties collectively corresponding to the West Coast representatives of C. centaureae (in the sense of Cronquist). Because of the great similarity of the various West Coast plants and their intergradation, I see no value in recognizing two or more species.</discussion>
  <discussion>The West Coast and Rocky Mountains plants are clearly related, but are separated by the Great Basin region and there is little chance of current genetic interchange. As is often the case with American Cirsium, genetic enrichment from past hybridization with other nearby species within their respective areas has likely been fertile ground for evolutionary diversification. Different species have contributed genes in the Pacific states and in the Rockies. I have chosen to recognize two geographically-based species complexes, each with intergrading races here treated as varieties. I treat the West Coast plants as C. remotifolium and the Rocky Mountains plants as C. clavatum.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllary margins ciliate with tiny spreading to recurved spines</description>
      <determination>32c Cirsium remotifolium var. rivulare</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllary margins unappendaged or dilated, scarious, and ± lacerate-toothed</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries narrowly oblong or linear, often ± subequal, all or most without scarious-dilated margins</description>
      <determination>32a Cirsium remotifolium var. remotifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Phyllaries oblong to obovate, often strongly graduated, most or all with dilated, scarious, erose to lacerate-dentate margins</description>
      <determination>32b Cirsium remotifolium var. odontolepis</determination>
    </key_statement>
  </key>
</bio:treatment>