<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">78</other_info_on_meta>
    <other_info_on_meta type="illustration_page">77</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">mutisieae</taxon_name>
    <taxon_name authority="Hooker" date="1829" rank="genus">adenocaulon</taxon_name>
    <taxon_name authority="Hooker" date="1829" rank="species">bicolor</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Misc.</publication_title>
      <place_in_publication>1: 19, plate 15. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe mutisieae;genus adenocaulon;species bicolor</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">220000203</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial stems usually leafy only near bases, openly branched.</text>
      <biological_entity id="o11507" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character constraint="near bases" constraintid="o11508" is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="leafy" value_original="leafy" />
        <character is_modifier="false" modifier="openly" name="architecture" notes="" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o11508" name="base" name_original="bases" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petioles winged;</text>
      <biological_entity id="o11509" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o11510" name="petiole" name_original="petioles" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 3–nerved, 3–25 cm.</text>
      <biological_entity id="o11511" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o11512" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-nerved" value_original="3-nerved" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries 5–6 (–10), 1–2 mm.</text>
      <biological_entity id="o11513" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="10" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="6" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peripheral florets: corollas soon falling, 0.5–1.2 mm.</text>
      <biological_entity constraint="peripheral" id="o11514" name="floret" name_original="florets" src="d0_s4" type="structure" />
      <biological_entity id="o11515" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="soon" name="life_cycle" src="d0_s4" value="falling" value_original="falling" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inner florets: corollas tardily falling, 1–2.3 mm.</text>
      <biological_entity constraint="inner" id="o11516" name="floret" name_original="florets" src="d0_s5" type="structure" />
      <biological_entity id="o11517" name="corolla" name_original="corollas" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="tardily" name="life_cycle" src="d0_s5" value="falling" value_original="falling" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 5–9 mm. 2n = 46.</text>
      <biological_entity id="o11518" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11519" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woods, forests, usually in shade</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woods" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="shade" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Ont.; Calif., Idaho, Mich., Minn., Mont., Oreg., S.Dak., Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Trail plant</other_name>
  <discussion>Adenocaulon bicolor is a common forest herb from southwestern Canada to central California. It is disjunct in the Black Hills (eastern Wyoming, western South Dakota) and the Great Lakes region (southern Ontario, northern Michigan). Reports of the species from Minnesota and Wisconsin are unverified.</discussion>
  
</bio:treatment>