<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">363</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="1913" rank="genus">herrickia</taxon_name>
    <taxon_name authority="(Nuttall) Brouillet" date="2004" rank="species">glauca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">glauca</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus herrickia;species glauca;variety glauca</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068464</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">glaucodes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">formosus</taxon_name>
    <taxon_hierarchy>genus Aster;species glaucodes;variety formosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eucephalus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">formosus</taxon_name>
    <taxon_hierarchy>genus Eucephalus;species formosus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–70 cm, eglandular.</text>
      <biological_entity id="o14268" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–7+, glabrous proximally, sometimes thinly scabridulous distally.</text>
      <biological_entity id="o14269" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="7" upper_restricted="false" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes thinly; distally" name="relief" src="d0_s1" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades oblong or elliptic-oblong to lanceolate (sometimes narrowly), 40–120 × 5–25 mm, faces glabrous.</text>
      <biological_entity id="o14270" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic-oblong" name="shape" src="d0_s2" to="lanceolate" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s2" to="120" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14271" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 2–116+.</text>
      <biological_entity id="o14272" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="116" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles glabrous or sometimes thinly scabridulous or villosulous;</text>
      <biological_entity id="o14273" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes thinly" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villosulous" value_original="villosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts 0–2, foliaceous, margins villoso-ciliate or glabrous.</text>
      <biological_entity id="o14274" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o14275" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villoso-ciliate" value_original="villoso-ciliate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate, 6–9 mm.</text>
      <biological_entity id="o14276" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 15–35, glabrous.</text>
      <biological_entity id="o14277" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s7" to="35" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets (8–) 10–15 (–19);</text>
      <biological_entity id="o14278" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s8" to="10" to_inclusive="false" />
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="19" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>laminae 8–18 × 0.7–1.3 mm.</text>
      <biological_entity id="o14279" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="18" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s9" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 12–32;</text>
      <biological_entity id="o14280" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s10" to="32" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas 6.8–7.5 mm, lobes 0.7–1.3 mm. 2n = 18.</text>
      <biological_entity id="o14281" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="6.8" from_unit="mm" name="some_measurement" src="d0_s11" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14282" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s11" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14283" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, rocky slopes, often on calcareous substrates, saline seeps at lower elevations, in sagebrush, pinyon-juniper, Douglas fir, lodgepole pine, and hanging gardens communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="calcareous substrates" modifier="often on" />
        <character name="habitat" value="seeps" modifier="saline" constraint="at lower elevations" />
        <character name="habitat" value="lower elevations" modifier="at" />
        <character name="habitat" value="saline" />
        <character name="habitat" value="sagebrush" modifier="in" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="douglas fir" />
        <character name="habitat" value="lodgepole pine" />
        <character name="habitat" value="gardens communities" modifier="and hanging" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Idaho, Mont., N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2a.</number>
  
</bio:treatment>