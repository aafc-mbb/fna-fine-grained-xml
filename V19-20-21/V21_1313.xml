<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">519</other_info_on_meta>
    <other_info_on_meta type="treatment_page">520</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="species">punctata</taxon_name>
    <taxon_name authority="(de Candolle) B. L. Turner in B. L. Turner et al." date="2003" rank="variety">mucronata</taxon_name>
    <place_of_publication>
      <publication_title>in B. L. Turner et al., Atlas Vasc. Pl. Texas,</publication_title>
      <place_in_publication>6. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species punctata;variety mucronata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068566</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">mucronata</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 129. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Liatris;species mucronata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lacinaria</taxon_name>
    <taxon_name authority="Bush" date="unknown" rank="species">leptostachya</taxon_name>
    <taxon_hierarchy>genus Lacinaria;species leptostachya;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lacinaria</taxon_name>
    <taxon_name authority="Bush" date="unknown" rank="species">ruthii</taxon_name>
    <taxon_hierarchy>genus Lacinaria;species ruthii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Liatris</taxon_name>
    <taxon_name authority="(Bush) Gaiser" date="unknown" rank="species">angustifolia</taxon_name>
    <taxon_hierarchy>genus Liatris;species angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Corms globose to depressed-globose.</text>
      <biological_entity id="o25910" name="corm" name_original="corms" src="d0_s0" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s0" to="depressed-globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 70–150 × 1–3 mm.</text>
      <biological_entity id="o25911" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="70" from_unit="mm" name="length" src="d0_s1" to="150" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads in dense, spiciform arrays (closely spaced, stems usually obscured by heads).</text>
      <biological_entity id="o25912" name="head" name_original="heads" src="d0_s2" type="structure" />
      <biological_entity id="o25913" name="array" name_original="arrays" src="d0_s2" type="structure">
        <character is_modifier="true" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="spiciform" value_original="spiciform" />
      </biological_entity>
      <relation from="o25912" id="r1762" name="in" negation="false" src="d0_s2" to="o25913" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres 7–9 (–12) mm.</text>
      <biological_entity id="o25914" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="12" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries in (4–) 5–6 series.</text>
      <biological_entity id="o25915" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure" />
      <biological_entity id="o25916" name="series" name_original="series" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="atypical_quantity" src="d0_s4" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="6" />
      </biological_entity>
      <relation from="o25915" id="r1763" name="in" negation="false" src="d0_s4" to="o25916" />
    </statement>
    <statement id="d0_s5">
      <text>Florets (3–) 4–5 (–6).</text>
      <biological_entity id="o25917" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s5" to="4" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="6" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="5" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="mid Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, mesquite-grasslands, roadsides, fencerows, sands, clay, sandy loams, often rocky</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="mesquite-grasslands" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fencerows" />
        <character name="habitat" value="sands" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="sandy loams" />
        <character name="habitat" value="rocky" modifier="often" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Kans., Mo., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5c.</number>
  <discussion>Varieties mucronata and punctata intergrade where their ranges meet in central Texas (G. L. Nesom and R. J. O’Kennon 2001). Plants currently identified as Liatris punctata or L. mucronata in northern Arkansas, southern Missouri, and adjacent Kansas appear to represent an undescribed race, perhaps most closely related to L. aestivalis.</discussion>
  
</bio:treatment>