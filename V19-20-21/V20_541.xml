<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="mention_page">249</other_info_on_meta>
    <other_info_on_meta type="treatment_page">247</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(A. Gray) Shinners" date="1951" rank="species">stenophylla</taxon_name>
    <taxon_name authority="(Rydberg) Semple" date="1994" rank="variety">angustifolia</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>4: 53. 1994</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species stenophylla;variety angustifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068487</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">angustifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>37: 128. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysopsis;species angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysopsis</taxon_name>
    <taxon_name authority="(Pursh) Nuttall ex de Candolle" date="unknown" rank="species">villosa</taxon_name>
    <taxon_name authority="(Rydberg) Cronquist" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>genus Chrysopsis;species villosa;variety angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Heterotheca</taxon_name>
    <taxon_name authority="(Pursh) Shinners" date="unknown" rank="species">villosa</taxon_name>
    <taxon_name authority="(Rydberg) V. L. Harms" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>genus Heterotheca;species villosa;variety angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Distal mid-stems moderately to densely hispido-strigose, eglandular or sparsely stipitate-glandular.</text>
      <biological_entity constraint="distal" id="o5540" name="mid-stem" name_original="mid-stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s0" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves pale green to gray-green, faces moderately to ± densely hispido-strigose (21–60 hairs/mm²; hairs rarely broad-based, pustulate), eglandular to sparsely glandular;</text>
      <biological_entity id="o5541" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s1" to="gray-green" />
      </biological_entity>
      <biological_entity id="o5542" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="moderately to more or less densely" name="pubescence" src="d0_s1" value="hispido-strigose" value_original="hispido-strigose" />
        <character char_type="range_value" from="eglandular" name="architecture" src="d0_s1" to="sparsely glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>distal cauline linear to narrowly oblanceolate, rarely broadly so, (14–) 18.7 (–30) × (2–) 2.6–4.5 (–6) mm, margins proximally long-hispido-strigose, sometimes cilia numerous along whole length, faces sparsely to moderately hispido-strigose, eglandular.</text>
      <biological_entity constraint="distal cauline" id="o5543" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="narrowly oblanceolate" />
        <character char_type="range_value" from="14" from_unit="mm" name="atypical_length" src="d0_s2" to="18.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="18.7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="30" to_unit="mm" />
        <character name="length" src="d0_s2" unit="mm" value="18.7" value_original="18.7" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s2" to="2.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.6" from_unit="mm" name="width" src="d0_s2" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5544" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="rarely broadly; broadly; proximally" name="pubescence" src="d0_s2" value="long-hispido-strigose" value_original="long-hispido-strigose" />
      </biological_entity>
      <biological_entity id="o5545" name="cilium" name_original="cilia" src="d0_s2" type="structure">
        <character is_modifier="false" name="length" src="d0_s2" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o5546" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s2" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllary faces usually moderately strigose, eglandular.</text>
    </statement>
    <statement id="d0_s4">
      <text>2n = 36.</text>
      <biological_entity constraint="phyllary" id="o5547" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually moderately" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5548" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy and gravelly soils, sandy loam, sandy gypsiferous soil, prairies, hillsides, pastures, open areas by oak wooded ravines, disturbed slopes, grasslands, stabilized dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="sandy loam" />
        <character name="habitat" value="sandy gypsiferous soil" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="hillsides" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="open areas" constraint="by oak" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="ravines" modifier="wooded" />
        <character name="habitat" value="wooded" />
        <character name="habitat" value="disturbed slopes" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="dunes" modifier="stabilized" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., Nebr., N.Mex., Okla., S.Dak., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12b.</number>
  <discussion>Variety angustifolia grows in the Great Plains from central Texas and New Mexico to eastern Colorado and southern South Dakota. Triploid chromosome counts from putative intervarietal hybrids have been reported. Variety angustifolia usually has densely hispid distal mid stems with many long-spreading hairs and densely hispido-strigose, eglandular leaves and phyllaries. The most densely hairy individuals are not always easily distinguished from sympatric forms of Heterotheca canescens. Both diploid and tetraploid individuals of H. canescens usually have more than 100 hairs/mm2 of leaf face versus less than 60 hairs/mm2 in both varieties of H. stenophylla. The difference manifests itself in silvery gray-green versus green (bright to dark) or gray-green leaves, respectively. Stems of H. villosa usually have relatively longer nodes and a less densely leafy appearance. The leaves can be as densely hairy. Variety angustifolia historically has been included in H. villosa.</discussion>
  
</bio:treatment>