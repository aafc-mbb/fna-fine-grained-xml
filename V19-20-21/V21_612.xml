<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Sharon C. Yarborough,A. Michael Powell</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">245</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="mention_page">249</other_info_on_meta>
    <other_info_on_meta type="mention_page">250</other_info_on_meta>
    <other_info_on_meta type="treatment_page">247</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1832" rank="subtribe">flaveriinae</taxon_name>
    <taxon_name authority="Jussieu" date="1789" rank="genus">flaveria</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Pl.,</publication_title>
      <place_in_publication>186. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe flaveriinae;genus FLAVERIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin flavus, yellow</other_info_on_name>
    <other_info_on_name type="fna_id">112857</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, perennials, or subshrubs, to 200+ cm [trees to 400 cm] (usually ± succulent, herbage usually glaucous).</text>
      <biological_entity id="o18628" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o18630" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (often purplish) erect or decumbent, branched distally or ± throughout.</text>
      <biological_entity id="o18631" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="distally; distally; more or less throughout; throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite (decussate);</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile (weakly connate to connate-perfoliate);</text>
      <biological_entity id="o18632" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (often 3-nerved) oblong-ovate to lanceolate or linear, margins entire, serrate, or spinulose-serrate, faces glabrous or short-pubescent.</text>
      <biological_entity id="o18633" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s5" to="lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o18634" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="spinulose-serrate" value_original="spinulose-serrate" />
      </biological_entity>
      <biological_entity id="o18635" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="short-pubescent" value_original="short-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate or discoid, usually in tight or loose aggregations in (often flat-topped) ± corymbiform arrays or glomerules.</text>
      <biological_entity id="o18636" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="discoid" value_original="discoid" />
      </biological_entity>
      <biological_entity id="o18637" name="aggregation" name_original="aggregations" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement_or_density" src="d0_s6" value="tight" value_original="tight" />
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o18638" name="glomerule" name_original="glomerules" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o18636" id="r1269" modifier="usually" name="in" negation="false" src="d0_s6" to="o18637" />
      <relation from="o18637" id="r1270" name="in" negation="false" src="d0_s6" to="o18638" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres oblong, urceolate, cylindric, or turbinate, 0.5–2 mm diam.</text>
      <biological_entity id="o18639" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 2–6 (–9) in ± 1 series (linear, concave, or boatshaped, subequal).</text>
      <biological_entity id="o18640" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="9" />
        <character char_type="range_value" constraint="in series" constraintid="o18641" from="2" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity id="o18641" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex, epaleate (“receptacles” of glomerules sometimes setose).</text>
      <biological_entity id="o18642" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0–1 (–2), pistillate, fertile;</text>
      <biological_entity id="o18643" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="2" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s10" to="1" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow or whitish (laminae inconspicuous).</text>
      <biological_entity id="o18644" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 1–15, bisexual, fertile;</text>
      <biological_entity id="o18645" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="15" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes shorter than to about equaling funnelform to campanulate throats, lobes 5, ± deltate.</text>
      <biological_entity id="o18646" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o18647" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than to about equaling funnelform to campanulate throats" constraintid="o18648" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o18648" name="funnelform" name_original="funnelform" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o18649" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae (black) weakly compressed, narrowly oblanceolate or linear-oblong (usually 10-nerved, glabrous);</text>
      <biological_entity id="o18650" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="linear-oblong" value_original="linear-oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi usually 0, sometimes persistent, of 2–4 hyaline scales, or coroniform (of connate scales).</text>
      <biological_entity id="o18652" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s15" to="4" />
        <character is_modifier="true" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o18651" id="r1271" name="consist_of" negation="false" src="d0_s15" to="o18652" />
    </statement>
    <statement id="d0_s16">
      <text>x = 18.</text>
      <biological_entity id="o18651" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s15" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o18653" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>United States, Mexico, West Indies (Greater Antilles), Central America, South America, Asia (India), Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Greater Antilles)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia (India)" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>327.</number>
  <discussion>Species 21 (8 in the flora).</discussion>
  <discussion>Members of Flaveria are frequently found in alkaline, saline, and gypseous soils, often in disturbed and moist areas. Heads of Flaveria may be either radiate or discoid; when both are present in the same capitulescence, the discoid heads tend to be central and the radiate heads peripheral. Many species of Flaveria have persistent sheathing leaf bases that ring the stems after the leaves have fallen.</discussion>
  <discussion>Flaveria is notable because certain species exhibit C3 photosynthesis, some C3–C4 (intermediate) photosynthesis, and others (F. brownii, F. bidentis, F. campestris, and F. trinervia in the United States) classic C4 photosynthesis.</discussion>
  <references>
    <reference>Powell, A. M. 1978. Systematics of Flaveria (Flaveriinae–Asteraceae). Ann. Missouri Bot. Gard. 65: 590–636.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pappi usually of 2–4 scales or coroniform, rarely 0</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pappi 0</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades to 10–40 mm wide, bases connate-perfoliate; New Mexico, Texas</description>
      <determination>1 Flaveria chlorifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades 2–7 mm wide, bases weakly connate; Arizona</description>
      <determination>8 Flaveria mcdougallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Heads in tight, axillary glomerules ("receptacles" of glomerules setose)</description>
      <determination>7 Flaveria trinervia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Heads usually in corymbiform, paniculiform, or spiciform arrays, seldom in tight, axillary glomerules ("receptacles" of glomerules not setose).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Annuals</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Perennials (annuals)</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Heads in scorpioid cymiform arrays; ray laminae to 1 mm; Alabama, Florida, Georgia</description>
      <determination>5 Flaveria bidentis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Heads in corymbiform arrays; ray laminae 1.5–2.5 mm; sc, sw United States</description>
      <determination>6 Flaveria campestris</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Calyculus bractlets surpassing involucres (sw coast, Florida)</description>
      <determination>3 Flaveria floridana</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Calyculus bractlets shorter than involucres</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Ray laminae oblong-elliptic, 2 mm; disc florets (5–)7–10; Texas</description>
      <determination>2 Flaveria brownii</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Ray laminae oval to obovate-spatulate, 2–3 mm; disc florets (2–)5–7(–8); Florida</description>
      <determination>4 Flaveria linearis</determination>
    </key_statement>
  </key>
</bio:treatment>