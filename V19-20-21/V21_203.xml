<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Keil</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="treatment_page">87</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="A. Gray ex Nuttall" date="1840" rank="genus">ENGELMANNIA</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 343. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus ENGELMANNIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For George Engelmann, 1809–1884, German-American physician and botanist</other_info_on_name>
    <other_info_on_name type="fna_id">111691</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 20–50 (–100) cm (taproots or caudices becoming woody).</text>
      <biological_entity id="o11165" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect (coarsely strigose, hispid, or hirsute), usually branched (at least distally, sometimes branched from bases, aerial stems multiple).</text>
      <biological_entity id="o11166" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (basal and proximal cauline) or sessile (distal);</text>
      <biological_entity id="o11167" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (pinnately nerved) mostly oblong to lanceolate, usually 1 (–2) -pinnately lobed, bases ± cuneate, ultimate margins entire, faces coarsely strigose, hispid, or hirsute.</text>
      <biological_entity id="o11168" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="mostly oblong" name="shape" src="d0_s5" to="lanceolate" />
        <character is_modifier="false" modifier="usually 1(-2)-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o11169" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o11170" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11171" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="coarsely" name="pubescence" src="d0_s5" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, in open, corymbiform arrays.</text>
      <biological_entity id="o11172" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o11173" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o11172" id="r770" name="in" negation="false" src="d0_s6" to="o11173" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric, 6–10 mm diam.</text>
      <biological_entity id="o11174" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent (outer) or falling (inner, with cypselae), mostly 18–24+ in ± 3 series (outer with relatively short, expanded, indurate bases and longer, linear, herbaceous tips, inner broadly ovate, mostly indurate, scarious-margined, herbaceous tips relatively broader and shorter, tending to split along midveins in age).</text>
      <biological_entity id="o11175" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="life_cycle" src="d0_s8" value="falling" value_original="falling" />
        <character char_type="range_value" constraint="in series" constraintid="o11176" from="18" modifier="mostly" name="quantity" src="d0_s8" to="24" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o11176" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat, paleate (paleae linear to narrowly oblong, hirsute-ciliate at tips).</text>
      <biological_entity id="o11177" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8–9 (each subtended by an inner phyllary), pistillate, fertile;</text>
      <biological_entity id="o11178" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s10" to="9" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow (laminae oblongelliptic, entire or minutely 2–3-toothed).</text>
      <biological_entity id="o11179" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 25–50, functionally staminate;</text>
      <biological_entity id="o11180" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s12" to="50" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes shorter than campanulate throats, lobes 5, ± deltate (anthers black, appendages deltate, obtuse; styles not branched).</text>
      <biological_entity id="o11181" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o11182" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than campanulate throats" constraintid="o11183" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o11183" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o11184" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae obcompressed or obflattened, obovate (each falling with subtending phyllary, 2–4, indurate paleae, plus sterile ovaries of 2–4 disc-florets, margins ± ciliate, faces strigose to pilose);</text>
      <biological_entity id="o11185" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obcompressed" value_original="obcompressed" />
        <character name="shape" src="d0_s14" value="obflattened" value_original="obflattened" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent or tardily falling, of 2–4, ciliate scales.</text>
      <biological_entity id="o11187" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s15" to="4" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s15" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o11186" id="r771" name="consist_of" negation="false" src="d0_s15" to="o11187" />
    </statement>
    <statement id="d0_s16">
      <text>x = 9.</text>
      <biological_entity id="o11186" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="tardily" name="life_cycle" src="d0_s15" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o11188" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>271.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>