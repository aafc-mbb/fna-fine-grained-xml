<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">199</other_info_on_meta>
    <other_info_on_meta type="illustration_page">199</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">coreopsidinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">coreocarpus</taxon_name>
    <taxon_name authority="(A. Gray) S. F. Blake" date="1913" rank="species">arizonicus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>49: 344. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe coreopsidinae;genus coreocarpus;species arizonicus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066414</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leptosyne</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">arizonica</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>17: 218. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Leptosyne;species arizonica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials (may flower first-year).</text>
      <biological_entity id="o1778" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves ± scattered over proximal 3/4+ of plant height, internodes mostly 30–75+ mm;</text>
      <biological_entity id="o1779" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character constraint="over plant" constraintid="o1780" is_modifier="false" modifier="more or less" name="arrangement" src="d0_s1" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o1780" name="plant" name_original="plant" src="d0_s1" type="structure">
        <character is_modifier="true" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="3/4" is_modifier="true" name="quantity" src="d0_s1" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o1781" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="height" src="d0_s1" to="75" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>lobes mostly filiform to linear, sometimes lanceolate, (5–) 15–35+ × 0.5–1 (–3+) mm.</text>
      <biological_entity id="o1782" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="mostly filiform" name="shape" src="d0_s2" to="linear" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s2" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="35" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="3" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ray laminae pale-yellow (fading to white), 2–4.5 (–6+) mm.</text>
      <biological_entity constraint="ray" id="o1783" name="lamina" name_original="laminae" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="6" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Disc corollas 3–4.5 mm.</text>
      <biological_entity constraint="disc" id="o1784" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cypselae 2–3 (–5) mm;</text>
      <biological_entity id="o1785" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pappi 0, or 0.5–1 mm. 2n = 24.</text>
      <biological_entity id="o1786" name="pappus" name_original="pappi" src="d0_s6" type="structure">
        <character name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1787" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year round, following rains.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sites, along streams, rocky canyons</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="open" constraint="along streams , rocky canyons" />
        <character name="habitat" value="streams" modifier="along" />
        <character name="habitat" value="rocky canyons" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Chihuahua, Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>E. B. Smith (1989) treated plants of Coreocarpus arizonicus from the flora area and Mexico as var. arizonicus and other plants of the species from Mexico as var. pubescens (B. L. Robinson &amp; Fernald) S. F. Blake.</discussion>
  
</bio:treatment>