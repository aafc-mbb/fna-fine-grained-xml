<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="treatment_page">614</other_info_on_meta>
    <other_info_on_meta type="illustration_page">613</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Cassini" date="1816" rank="genus">ligularia</taxon_name>
    <taxon_name authority="(A. Gray) H. Hara" date="1939" rank="species">dentata</taxon_name>
    <place_of_publication>
      <publication_title>J. Jap. Bot.</publication_title>
      <place_in_publication>15: 318. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus ligularia;species dentata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200024210</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erythrochaete</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">dentata</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>6: 395. 1858</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erythrochaete;species dentata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ligularia</taxon_name>
    <taxon_name authority="Maximowicz" date="unknown" rank="species">clivorum</taxon_name>
    <taxon_hierarchy>genus Ligularia;species clivorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="(Maximowicz) Maximowicz" date="unknown" rank="species">clivorum</taxon_name>
    <taxon_hierarchy>genus Senecio;species clivorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants often purplish-tinged, glabrous or distally unevenly hairy;</text>
      <biological_entity id="o27112" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s0" value="purplish-tinged" value_original="purplish-tinged" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally unevenly" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rootstocks stout, fibrous-rooted.</text>
      <biological_entity id="o27113" name="rootstock" name_original="rootstocks" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal blades 20–45 × 20–40 cm, bases deeply cordate.</text>
      <biological_entity id="o27114" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o27115" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s2" to="45" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="width" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o27116" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="cordate" value_original="cordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 2–9 cm.</text>
      <biological_entity id="o27117" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 9–12 (–20) × 16–28 mm.</text>
      <biological_entity id="o27118" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="16" from_unit="mm" name="width" src="d0_s4" to="28" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries greenish-tipped.</text>
      <biological_entity id="o27119" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="greenish-tipped" value_original="greenish-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray corolla laminae 20–35 (–50) mm.</text>
      <biological_entity constraint="corolla" id="o27120" name="lamina" name_original="laminae" src="d0_s6" type="structure" constraint_original="ray corolla">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s6" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 8–10 mm;</text>
      <biological_entity id="o27121" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi 10–12 mm. 2n = 60.</text>
      <biological_entity id="o27122" name="pappus" name_original="pappi" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27123" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, abandoned plantings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="abandoned plantings" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–50+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50+" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Md.; Asia (China, Japan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Ligularia dentata is commonly cultivated in eastern Canada and the United States; it sometimes persists (as in Maryland).</discussion>
  
</bio:treatment>