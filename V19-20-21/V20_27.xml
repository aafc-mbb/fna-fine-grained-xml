<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">29</other_info_on_meta>
    <other_info_on_meta type="mention_page">33</other_info_on_meta>
    <other_info_on_meta type="treatment_page">32</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">baccharis</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="species">salicina</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 258. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus baccharis;species salicina</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066188</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Baccharis</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">salicifolia</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 337. 1840,</place_in_publication>
      <other_info_on_pub>not (Ruiz &amp; Pavón) Persoon 1807</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Baccharis;species salicifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Baccharis</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">emoryi</taxon_name>
    <taxon_hierarchy>genus Baccharis;species emoryi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 100–300 cm (much branched).</text>
      <biological_entity id="o30245" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending, striate-angled, glabrous, smooth or minutely roughened, resinous.</text>
      <biological_entity id="o30246" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s1" value="striate-angled" value_original="striate-angled" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s1" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s1" value="roughened" value_original="roughened" />
        <character is_modifier="false" name="coating" src="d0_s1" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves present at flowering (numerous and well developed);</text>
    </statement>
    <statement id="d0_s3">
      <text>short-petiolate;</text>
      <biological_entity id="o30247" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (at least broader distinctly 3-nerved) oblong to oblanceolate, 25–70 × 5–10 (–20) mm, bases tapering attenuate, margins usually serrate distally (teeth 1–3, coarse irregular, ca. 5 mm apart), sometimes entire, apices acute or obtuse, faces finely gland-dotted.</text>
      <biological_entity id="o30248" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30249" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o30250" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually; distally" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o30251" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o30252" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="finely" name="coloration" src="d0_s4" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (100–200+, short-pedunculate or sessile) in (large, crowded, leafy) paniculiform arrays.</text>
      <biological_entity id="o30253" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o30254" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o30253" id="r2805" name="in" negation="false" src="d0_s5" to="o30254" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres narrowly obconic to campanulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>staminate 4–7 mm, pistillate 5–9 mm.</text>
      <biological_entity id="o30255" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly obconic" name="shape" src="d0_s6" to="campanulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries lanceolate, 2–6 mm, margins scarious, medians green or reddish, apices greenish or purplish, often erose-ciliate, faces glabrous, gland-dotted, resinous.</text>
      <biological_entity id="o30256" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30257" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o30258" name="median" name_original="medians" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o30259" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="often" name="architecture_or_pubescence_or_shape" src="d0_s8" value="erose-ciliate" value_original="erose-ciliate" />
      </biological_entity>
      <biological_entity id="o30260" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s8" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate florets 20–25;</text>
      <biological_entity id="o30261" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 3–5 mm.</text>
      <biological_entity id="o30262" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate florets 25–30;</text>
      <biological_entity id="o30263" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s11" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 3–4 mm.</text>
      <biological_entity id="o30264" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 1.2–2 mm, irregularly 8–10-nerved, glabrous;</text>
      <biological_entity id="o30265" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s13" value="8-10-nerved" value_original="8-10-nerved" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 8–12 mm (elongating in fruit).</text>
      <biological_entity id="o30266" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream banks, alkaline meadows, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="alkaline meadows" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Kans., Nev., N.Mex., Okla., Tex., Utah; Mexico (Baja California, Chihuahua, Coahuila, Durango, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Willow-baccharis</other_name>
  <other_name type="common_name">Great Plains false willow</other_name>
  <discussion>Baccharis salicina is recognized by its narrow, gland-dotted leaves with 1–3 irregular teeth on the distal half, heads in loose leafy arrays, campanulate involucres, and cypselae with 8–10 ribs.</discussion>
  <discussion>The recognition of Baccharis emoryi as a separate species in other floras has been based on its wider, glabrous, eglandular leaves, more cylindric pistillate involucres, and dense whitish pappi. It was said to occur both west of the Rocky Mountains and in western Texas. In our study, expressions of the characters used to distinguish B. emoryi from other species were found to be inconsistent and inadequate to warrant recognition as a distinct species. There appears to be a complex of up to four species—emoryi, salicina, neglecta and angustifolia—that intergrade from west to east. Characteristics progress from broader leaves and larger heads (emoryi form of salicina) to narrow leaves with small heads (neglecta, angustifolia). The delimitation of taxa within this complex merits further investigation.</discussion>
  
</bio:treatment>