<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">24</other_info_on_meta>
    <other_info_on_meta type="mention_page">25</other_info_on_meta>
    <other_info_on_meta type="mention_page">34</other_info_on_meta>
    <other_info_on_meta type="treatment_page">26</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">baccharis</taxon_name>
    <taxon_name authority="A. Gray in W. H. Emory" date="1859" rank="species">bigelovii</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 84. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus baccharis;species bigelovii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066176</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 30–100 cm (branched from bases).</text>
      <biological_entity id="o23529" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, slender, striate-angled, glabrous, resinous.</text>
      <biological_entity id="o23530" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s1" value="striate-angled" value_original="striate-angled" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s1" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves present at flowering;</text>
    </statement>
    <statement id="d0_s3">
      <text>short-petiolate;</text>
      <biological_entity id="o23531" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-petiolate" value_original="short-petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (1-nerved or obscurely 3-nerved) obovate to oblanceolate, 20–35 × 3–15 mm, distally reduced and narrowed, bases cuneate, margins irregularly incised to coarsely serrate or 2-serrate, faces glabrous, gland-dotted, resinous.</text>
      <biological_entity id="o23532" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s4" to="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s4" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o23533" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o23534" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s4" value="incised" value_original="incised" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="2-serrate" value_original="2-serrate" />
      </biological_entity>
      <biological_entity id="o23535" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s4" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (20–50) in corymbiform arrays.</text>
      <biological_entity id="o23536" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o23537" from="20" name="atypical_quantity" src="d0_s5" to="50" />
      </biological_entity>
      <biological_entity id="o23537" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>staminate 4–5 mm, pistillate 4–5 mm.</text>
      <biological_entity id="o23538" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries lanceolate, 1–4 mm, margins scarious, medians green, apices acute, erose.</text>
      <biological_entity id="o23539" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23540" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o23541" name="median" name_original="medians" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o23542" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s8" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate florets 15–20;</text>
      <biological_entity id="o23543" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s9" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 3.5–4 mm.</text>
      <biological_entity id="o23544" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate florets 25–30;</text>
      <biological_entity id="o23545" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s11" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 2–2.6 mm.</text>
      <biological_entity id="o23546" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 1.5–2.2 mm, 5-nerved, glabrous;</text>
      <biological_entity id="o23547" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="5-nerved" value_original="5-nerved" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 3–4.5 mm.</text>
      <biological_entity id="o23548" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry rocky ground in coniferous forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky ground" modifier="dry" constraint="in coniferous forests" />
        <character name="habitat" value="coniferous forests" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Durango, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Bigelow’s false willow</other_name>
  <discussion>Baccharis bigelovii occurs in the general Chihuahuan Desert region in the Davis Mountains of West Texas, and in the Chiricahua and Huachuca mountains of Arizona. It is recognized by the relatively short stature, obovate, coarsely and irregularly serrate leaves, erose-ciliate phyllaries, and 5-nerved cypselae. It is similar to B. thesioides, which differs mainly by having narrower, more oblong leaves with more evenly serrate margins and spinulose teeth. Further investigation may show these two taxa to be different geographic expressions of a single species centered in Mexico.</discussion>
  
</bio:treatment>