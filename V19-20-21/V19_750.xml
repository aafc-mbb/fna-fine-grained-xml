<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="treatment_page">453</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">stylocline</taxon_name>
    <taxon_name authority="Morefield" date="1992" rank="species">intertexta</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>39: 121, fig. 3. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus stylocline;species intertexta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067618</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–8 (–11) cm.</text>
      <biological_entity id="o6278" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="11" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="8" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves acute, mucronate, longest 6–15 mm;</text>
      <biological_entity id="o6279" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s1" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" name="length" src="d0_s1" value="longest" value_original="longest" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>largest capitular leaves (all) ± elliptic to ± oblanceolate (widest in distal 2/3), 4–11 × 1–2.5 mm (distalmost mainly 0.8–1.2 times head heights).</text>
      <biological_entity constraint="largest capitular" id="o6280" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="less elliptic" name="shape" src="d0_s2" to="more or less oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="11" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in ± paniculiform to cymiform, rarely dichasiform, arrays, ± spheric, largest 5–6 mm, thickly lanuginose.</text>
      <biological_entity id="o6281" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s3" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="size" src="d0_s3" value="largest" value_original="largest" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="thickly" name="pubescence" src="d0_s3" value="lanuginose" value_original="lanuginose" />
      </biological_entity>
      <biological_entity id="o6282" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character char_type="range_value" from="less paniculiform" is_modifier="true" name="architecture" src="d0_s3" to="cymiform" />
        <character is_modifier="true" modifier="rarely" name="arrangement" src="d0_s3" value="dichasiform" value_original="dichasiform" />
      </biological_entity>
      <relation from="o6281" id="r599" name="in" negation="false" src="d0_s3" to="o6282" />
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries 0, vestigial, or falling, ± subulate, mostly 0.1–0.5 mm, unequal.</text>
      <biological_entity id="o6283" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="life_cycle" src="d0_s4" value="falling" value_original="falling" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0.1" from_unit="mm" modifier="mostly" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s4" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles cylindric, 1.4–2.7 mm, heights 4–7 times diams.;</text>
      <biological_entity id="o6284" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s5" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="height" src="d0_s5" value="4-7 times diams" value_original="4-7 times diams" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>scars ± evenly distributed, mamillate.</text>
      <biological_entity id="o6285" name="scar" name_original="scars" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less evenly" name="arrangement" src="d0_s6" value="distributed" value_original="distributed" />
        <character is_modifier="false" name="relief" src="d0_s6" value="mamillate" value_original="mamillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate paleae: longest 3.4–4.2 mm, winged distally;</text>
      <biological_entity id="o6286" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="length" src="d0_s7" value="longest" value_original="longest" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s7" to="4.2" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s7" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>wings elliptic to ovate, widest in distal 1/3 of palea lengths;</text>
      <biological_entity id="o6287" name="palea" name_original="paleae" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6288" name="wing" name_original="wings" src="d0_s8" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s8" to="ovate" />
        <character constraint="in distal 1/3" constraintid="o6289" is_modifier="false" name="width" src="d0_s8" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6289" name="1/3" name_original="1/3" src="d0_s8" type="structure" />
      <biological_entity id="o6290" name="paleum" name_original="palea" src="d0_s8" type="structure" />
      <relation from="o6289" id="r600" name="part_of" negation="false" src="d0_s8" to="o6290" />
    </statement>
    <statement id="d0_s9">
      <text>bodies cartilaginous;</text>
      <biological_entity id="o6291" name="palea" name_original="paleae" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6292" name="body" name_original="bodies" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s9" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outermost paleae ± saccate.</text>
      <biological_entity id="o6293" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o6294" name="palea" name_original="paleae" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Functionally staminate florets 3–6;</text>
      <biological_entity id="o6295" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="functionally" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovaries vestigial, 0–0.3 mm;</text>
      <biological_entity id="o6296" name="ovary" name_original="ovaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s12" value="vestigial" value_original="vestigial" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas 1.1–2.3 mm.</text>
      <biological_entity id="o6297" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s13" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1–1.4 mm, obcompressed;</text>
      <biological_entity id="o6298" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obcompressed" value_original="obcompressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi: staminate rarely 0, usually of 1–4 (–8) smooth to barbellulate bristles 1.1–2 mm.</text>
      <biological_entity id="o6299" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6300" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s15" to="8" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s15" to="4" />
        <character char_type="range_value" from="smooth" is_modifier="true" name="architecture" src="d0_s15" to="barbellulate" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o6299" id="r601" modifier="usually" name="consist_of" negation="false" src="d0_s15" to="o6300" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
        <character name="fruiting time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, stable, often calcareous desert gravels, sands, often with extra moisture (rock bases, shrub drip lines, dry drainages, depressions)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="stable" />
        <character name="habitat" value="calcareous desert gravels" modifier="often" />
        <character name="habitat" value="sands" />
        <character name="habitat" value="extra moisture" />
        <character name="habitat" value="rock bases" />
        <character name="habitat" value="shrub drip lines" />
        <character name="habitat" value="dry drainages" />
        <character name="habitat" value="depressions" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>40–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="40" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Mojave or Morefield neststraw</other_name>
  <discussion>Stylocline intertexta is known from the Mojave and northwestern Sonoran deserts. It combines character states of S. micropoides and S. psilocarphoides, is often sympatric with both, and appears to be stable, uniform, and reproducing independently. Stylocline intertexta shares most character states with S. micropoides. Presence of some subulate to lanceolate capitular leaves in S. micropoides helps distinguish the species in the field.</discussion>
  
</bio:treatment>