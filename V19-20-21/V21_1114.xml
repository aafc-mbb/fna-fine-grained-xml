<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">415</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="treatment_page">444</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="(A. Gray) Cockerell" date="1904" rank="genus">PLATEILEMA</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>31: 462. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus PLATEILEMA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek platys, broad, and eilema, envelope, alluding to broad phyllaries</other_info_on_name>
    <other_info_on_name type="fna_id">125750</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Nuttall" date="unknown" rank="genus">Actinella</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="section">Plateilema</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 31. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Actinella;section Plateilema;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, mostly 5–8+ cm.</text>
      <biological_entity id="o6251" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s0" to="8" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched mostly from bases.</text>
      <biological_entity id="o6252" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character constraint="from bases" constraintid="o6253" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o6253" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o6255" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>obscurely petiolate;</text>
      <biological_entity id="o6254" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades obovate to oblanceolate, pinnately toothed or lobed (lobes deltate to obovate), ultimate margins toothed or entire, faces sparsely hispid (hairs white, coarse) or glabrate, not glanddotted.</text>
      <biological_entity id="o6256" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="oblanceolate pinnately toothed or lobed" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="oblanceolate pinnately toothed or lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o6257" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o6258" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly.</text>
      <biological_entity id="o6259" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres broadly turbinate, mostly 7–9 mm diam.</text>
      <biological_entity id="o6260" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="7" from_unit="mm" modifier="mostly" name="diameter" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 8–12 in 2 series (spreading or erect in fruit, basally connate, gray-green, ± ovate, sparsely hispid).</text>
      <biological_entity id="o6261" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o6262" from="8" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
      <biological_entity id="o6262" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex, ± pitted, epaleate.</text>
      <biological_entity id="o6263" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="more or less" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 8–12, pistillate, fertile;</text>
      <biological_entity id="o6264" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s10" to="12" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellowish with 4–5+ purplish nerves (apices 3–4-lobed).</text>
      <biological_entity id="o6265" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character constraint="with nerves" constraintid="o6266" is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity id="o6266" name="nerve" name_original="nerves" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="5" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 40–60+, bisexual, fertile;</text>
      <biological_entity id="o6267" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s12" to="60" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellowish to purplish, tubes much shorter than ampliate, cylindric throats, lobes 5, ± deltate (± equal, usually hispidulous and/or glandular-puberulent).</text>
      <biological_entity id="o6268" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s13" to="purplish" />
      </biological_entity>
      <biological_entity id="o6269" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than ampliate , cylindric throats" constraintid="o6270" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o6270" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="size" src="d0_s13" value="ampliate" value_original="ampliate" />
        <character is_modifier="true" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o6271" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae obpyramidal, ± 4–5-angled (lengths 1.5–3 times widths), ± hispid on angles;</text>
      <biological_entity id="o6272" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obpyramidal" value_original="obpyramidal" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="4-5-angled" value_original="4-5-angled" />
        <character constraint="on angles" constraintid="o6273" is_modifier="false" modifier="more or less" name="pubescence" src="d0_s14" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o6273" name="angle" name_original="angles" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent, of 4–5 brownish, moderately lacerate to erose or truncate scales.</text>
      <biological_entity id="o6274" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character constraint="to scales" constraintid="o6275" is_modifier="false" modifier="of 4-5 brownish; moderately" name="shape" src="d0_s15" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity id="o6275" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="erose" value_original="erose" />
        <character is_modifier="true" name="architecture" src="d0_s15" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>387.</number>
  <discussion>Species 1.</discussion>
  <references>
    <reference>Turner, B. L. 2000. Plateilema (Asteraceae: Helenieae) a new generic report for the United States. Sida 19: 185–187.</reference>
  </references>
  
</bio:treatment>