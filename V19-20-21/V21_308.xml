<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">66</other_info_on_meta>
    <other_info_on_meta type="treatment_page">126</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="O. Hoffmann" date="1900" rank="genus">SPHAGNETICOLA</taxon_name>
    <place_of_publication>
      <publication_title>Notizbl. Königl. Bot. Gart. Berlin</publication_title>
      <place_in_publication>3: 36. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus SPHAGNETICOLA</taxon_hierarchy>
    <other_info_on_name type="etymology">Evidently from Latin sphagnum, a moss, and cola, dwelling in, perhaps alluding to usually wet habitats</other_info_on_name>
    <other_info_on_name type="fna_id">130944</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, mostly 30–200 cm.</text>
      <biological_entity id="o22138" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" modifier="mostly" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, branched ± throughout (rooting at nodes, usually ± succulent).</text>
      <biological_entity id="o22139" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="more or less throughout; throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile or petiolate;</text>
      <biological_entity id="o22140" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (usually 3-nerved) mostly trullate to lanceolate, often obscurely 3-lobed, bases ± cuneate, margins entire or toothed, faces usually scabrous.</text>
      <biological_entity id="o22141" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="mostly trullate" name="shape" src="d0_s5" to="lanceolate" />
        <character is_modifier="false" modifier="often obscurely" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o22142" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o22143" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o22144" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly.</text>
      <biological_entity id="o22145" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres obconic, 6–12 mm diam.</text>
      <biological_entity id="o22146" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 12–15 in 2–3 series (outer larger, more foliaceous than inner).</text>
      <biological_entity id="o22147" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o22148" from="12" name="quantity" src="d0_s8" to="15" />
      </biological_entity>
      <biological_entity id="o22148" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex to conic, paleate (paleae conduplicate, scarious, acute).</text>
      <biological_entity id="o22149" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s9" to="conic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 4–10+, pistillate, fertile;</text>
      <biological_entity id="o22150" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="10" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow to orange.</text>
      <biological_entity id="o22151" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 30–60+, bisexual, fertile;</text>
      <biological_entity id="o22152" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="30" name="quantity" src="d0_s12" to="60" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow to orange, tubes shorter than campanulate throats, lobes 5, lance-triangular.</text>
      <biological_entity id="o22153" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s13" to="orange" />
      </biological_entity>
      <biological_entity id="o22154" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than campanulate throats" constraintid="o22155" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o22155" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o22156" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lance-triangular" value_original="lance-triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae strongly biconvex to plumply 3–4-angled (epidermes usually thick, corky, smooth or tuberculate, apices rostrate, each bearing central neck or boss, rostra obscure in mature fruits);</text>
      <biological_entity id="o22157" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="strongly biconvex" name="shape" src="d0_s14" to="plumply 3-4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi 0, or persistent, coroniform (erose or fimbrillate to lacerate, 0.1–0.6 mm).</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 15.</text>
      <biological_entity id="o22158" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="coroniform" value_original="coroniform" />
      </biological_entity>
      <biological_entity constraint="x" id="o22159" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mexico, West Indies, Bermuda, Central America, South America, Asia, Indian Ocean Islands, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Bermuda" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Indian Ocean Islands" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>286.</number>
  <discussion>Species 4 (1 in the flora).</discussion>
  
</bio:treatment>