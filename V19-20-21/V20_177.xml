<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">90</other_info_on_meta>
    <other_info_on_meta type="treatment_page">92</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">gutierrezia</taxon_name>
    <taxon_name authority="(de Candolle) A. Gray" date="1849" rank="species">microcephala</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 74. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus gutierrezia;species microcephala</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066825</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brachyris</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">microcephala</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 313. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Brachyris;species microcephala;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gutierrezia</taxon_name>
    <taxon_name authority="(Pursh) Britton &amp; Rusby" date="unknown" rank="species">sarothrae</taxon_name>
    <taxon_name authority="(de Candolle) L. D. Benson" date="unknown" rank="variety">microcephala</taxon_name>
    <taxon_hierarchy>genus Gutierrezia;species sarothrae;variety microcephala;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xanthocephalum</taxon_name>
    <taxon_name authority="(de Candolle) Shinners" date="unknown" rank="species">microcephalum</taxon_name>
    <taxon_hierarchy>genus Xanthocephalum;species microcephalum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs, 20–140 cm.</text>
      <biological_entity id="o24195" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="140" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems glabrous or minutely hispidulous.</text>
      <biological_entity id="o24196" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s1" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal and proximal absent at flowering;</text>
      <biological_entity id="o24197" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline blades 1-nerved, linear or filiform to narrowly oblanceolate or lanceolate, 0.5–2.2 (–4) mm wide, little reduced distally.</text>
      <biological_entity id="o24198" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o24199" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s3" to="narrowly oblanceolate or lanceolate" />
        <character char_type="range_value" from="2.2" from_inclusive="false" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="2.2" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (2–6, sessile to subsessile, in compact glomerules) in flat-topped arrays.</text>
      <biological_entity id="o24200" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o24201" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="flat-topped" value_original="flat-topped" />
      </biological_entity>
      <relation from="o24200" id="r2227" name="in" negation="false" src="d0_s4" to="o24201" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres cylindric, 1–1.5 mm diam.</text>
      <biological_entity id="o24202" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllary apices flat.</text>
      <biological_entity constraint="phyllary" id="o24203" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 1 (–2; each enclosed by conduplicate inner phyllary);</text>
      <biological_entity id="o24204" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow, (1.5–) 2–3.5 mm.</text>
      <biological_entity id="o24205" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 1, sometimes 2 (functionally staminate; corollas broadly obdeltate-funnelform, throats widely flaring, lobes 1/3 corolla lengths, recurved-coiling).</text>
      <biological_entity id="o24206" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character modifier="sometimes" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 1–1.8 (–2.5) mm, faces densely strigoso-sericeous;</text>
      <biological_entity id="o24207" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24208" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="strigoso-sericeous" value_original="strigoso-sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi (rays, readily falling) of 1 series of narrowly lanceolate-oblong scales.</text>
      <biological_entity id="o24210" name="series" name_original="series" src="d0_s11" type="structure" constraint="scale" constraint_original="scale; scale">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o24211" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s11" value="lanceolate-oblong" value_original="lanceolate-oblong" />
      </biological_entity>
      <relation from="o24209" id="r2228" name="consist_of" negation="false" src="d0_s11" to="o24210" />
      <relation from="o24210" id="r2229" name="part_of" negation="false" src="d0_s11" to="o24211" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 8, 16, 24, 32.</text>
      <biological_entity id="o24209" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o24212" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="8" value_original="8" />
        <character name="quantity" src="d0_s12" value="16" value_original="16" />
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
        <character name="quantity" src="d0_s12" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Jul–Dec(–Feb).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Feb" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, chaparral, oak or oak-pine woodlands, usually over gravelly or rocky limestone or gypsum substrates, dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="oak-pine woodlands" />
        <character name="habitat" value="gravelly" />
        <character name="habitat" value="rocky limestone" />
        <character name="habitat" value="gypsum substrates" />
        <character name="habitat" value="dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Nev., N.Mex., Tex., Utah; Mexico (Baja California, Chihuahua, Coahuila, Durango, Nuevo León, San Luis Potosí, Sonora, Tamaulipas, Veracruz, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Durango)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="Mexico (Veracruz)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Small-head snakeweed</other_name>
  <discussion>Gutierrezia microcephala is recognized by its perennial habit and its small, tightly clustered heads, each with 4–8 phyllaries and 1(–2) ray and disc florets. Each ray floret is enclosed by a conduplicate inner phyllary. Forms of G. sarothrae with few florets in each head can be distinguished by their bisexual and fertile disc florets and tubular-funnelform disc corollas.</discussion>
  
</bio:treatment>