<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">40</other_info_on_meta>
    <other_info_on_meta type="mention_page">41</other_info_on_meta>
    <other_info_on_meta type="treatment_page">42</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">eucephalus</taxon_name>
    <taxon_name authority="(B. L. Robinson) Greene" date="1896" rank="species">paucicapitatus</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>3: 56. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus eucephalus;species paucicapitatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066725</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(D. C. Eaton) A. Gray" date="unknown" rank="species">engelmannii</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="variety">paucicapitatus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>26: 176. 1891 (as engelmanni)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species engelmannii;variety paucicapitatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(B. L. Robinson) B. L. Robinson" date="unknown" rank="species">paucicapitatus</taxon_name>
    <taxon_hierarchy>genus Aster;species paucicapitatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials 20–55 cm (caudices woody).</text>
      <biological_entity id="o12841" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="55" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, pilose or glandular-pubescent.</text>
      <biological_entity id="o12842" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: mid and distal blades elliptic to elliptic-oblong, 2–4 cm × 4–13 mm, sparsely scabrous to stipitate-glandular abaxially, moderately stipitate-glandular adaxially.</text>
      <biological_entity id="o12843" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="mid and distal" id="o12844" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="elliptic-oblong" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s2" to="13" to_unit="mm" />
        <character char_type="range_value" from="sparsely scabrous" name="pubescence" src="d0_s2" to="stipitate-glandular abaxially" />
        <character is_modifier="false" modifier="moderately; adaxially" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually 2–4 in racemiform to corymbiform arrays, somtimes borne singly.</text>
      <biological_entity id="o12845" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o12846" from="2" name="quantity" src="d0_s3" to="4" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s3" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o12846" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character char_type="range_value" from="racemiform" is_modifier="true" name="architecture" src="d0_s3" to="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles stipitate-glandular.</text>
      <biological_entity id="o12847" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate-obconic, 7–9 mm.</text>
      <biological_entity id="o12848" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate-obconic" value_original="turbinate-obconic" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 2–3 series (whitish), lance-linear (unequal), apices acute, abaxial faces stipitate-glandular.</text>
      <biological_entity id="o12849" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" notes="" src="d0_s6" value="lance-linear" value_original="lance-linear" />
      </biological_entity>
      <biological_entity id="o12850" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o12851" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12852" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o12849" id="r1169" name="in" negation="false" src="d0_s6" to="o12850" />
    </statement>
    <statement id="d0_s7">
      <text>Rays 7–13 (–21), white.</text>
      <biological_entity id="o12853" name="ray" name_original="rays" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="21" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s7" to="13" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae obconic, pilose;</text>
      <biological_entity id="o12854" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappus bristles in 2 series, ± barbellate.</text>
      <biological_entity constraint="pappus" id="o12855" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" notes="" src="d0_s9" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <biological_entity id="o12856" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <relation from="o12855" id="r1170" name="in" negation="false" src="d0_s9" to="o12856" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open subalpine meadows or scree slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="subalpine meadows" modifier="open" />
        <character name="habitat" value="scree slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–3300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Olympic Mountain aster</other_name>
  <discussion>Eucephalus paucicapitatus is found on Vancouver Island, where it is very uncommon, and the Olympic Peninsula. It is closely related to E. gormanii.</discussion>
  
</bio:treatment>