<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">528</other_info_on_meta>
    <other_info_on_meta type="treatment_page">529</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">symphyotrichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">symphyotrichum</taxon_name>
    <taxon_name authority="(Linnaeus) G. L. Nesom" date="1995" rank="species">novi-belgii</taxon_name>
    <taxon_name authority="(Fernald) Labrecque &amp; Brouillet" date="1997" rank="variety">crenifolium</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>82: 138. 1997</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus symphyotrichum;section symphyotrichum;species novi-belgii;variety crenifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068846</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Lindley" date="unknown" rank="species">foliaceus</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">crenifolius</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>17: 15. 1915</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species foliaceus;variety crenifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Fernald) Cronquist" date="unknown" rank="species">crenifolius</taxon_name>
    <taxon_hierarchy>genus Aster;species crenifolius;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">foliaceus</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">subpetiolatus</taxon_name>
    <taxon_hierarchy>genus Aster;species foliaceus;variety subpetiolatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">novi-belgii</taxon_name>
    <taxon_name authority="(Fernald) Labrecque &amp; Brouillet" date="unknown" rank="variety">crenifolius</taxon_name>
    <taxon_hierarchy>genus Aster;species novi-belgii;variety crenifolius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants robust, compact.</text>
      <biological_entity id="o20707" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s0" value="robust" value_original="robust" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems glabrous (sometimes hirsute in lines distally).</text>
      <biological_entity id="o20708" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades widely ovate to oblong, 4–5 times as long as wide, bases strongly clasping.</text>
      <biological_entity id="o20709" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="widely ovate" name="shape" src="d0_s2" to="oblong" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="4-5" value_original="4-5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>2n = 48.</text>
      <biological_entity id="o20710" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture_or_fixation" src="d0_s2" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20711" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stony or sandy, open upper seashores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stony" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="open upper seashores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Que.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>60b.</number>
  <discussion>Variety crenifolium is known from the Gaspé Peninsula and nearby New Brunswick. Reports from New England states need confirmation, as all of the material from there seen by us is var. novi-belgii or var. elodes.</discussion>
  
</bio:treatment>