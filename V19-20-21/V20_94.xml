<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="treatment_page">60</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom" date="1990" rank="species">greenei</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>68: 153. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species greenei</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066521</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">greenei</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>16: 80. 1880 (as Aplopappus)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Haplopappus;species greenei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">greenei</taxon_name>
    <taxon_name authority="(A. Gray) H. M. Hall" date="unknown" rank="subspecies">mollis</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species greenei;subspecies mollis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–30 cm.</text>
      <biological_entity id="o13234" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, green when young, soon reddish to brownish, branched, glabrous or tomentose, stipitate-glandular.</text>
      <biological_entity id="o13235" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character char_type="range_value" from="reddish" modifier="soon" name="coloration" src="d0_s1" to="brownish" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly erect or ascending;</text>
      <biological_entity id="o13236" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades spatulate (flat), 15–30 × 3–7 mm, midnerves (and sometimes 2 fainter, collateral nerves) evident (slightly raised abaxially), apices obtuse, mucronate, faces glabrous or tomentose and/or stipitate-glandular;</text>
      <biological_entity id="o13237" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13238" name="midnerve" name_original="midnerves" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o13239" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles absent.</text>
      <biological_entity id="o13240" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (12–22) usually in (leafy) congested, cymiform or racemiform arrays, rarely borne singly.</text>
      <biological_entity id="o13241" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o13242" from="12" name="atypical_quantity" src="d0_s5" to="22" />
        <character is_modifier="false" modifier="rarely" name="arrangement" notes="" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o13242" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="congested" value_original="congested" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="cymiform" value_original="cymiform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="racemiform" value_original="racemiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles usually less than 20, rarely to 100 mm (leafy).</text>
      <biological_entity id="o13243" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s6" to="20" />
        <character char_type="range_value" from="0" from_unit="mm" modifier="rarely" name="some_measurement" src="d0_s6" to="100" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres broadly campanulate, 8–12 × 12–15 mm.</text>
      <biological_entity id="o13244" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="12" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 18–28 in 2–3 series, green to tan, lanceolate to elliptic, 10–14 × 1.3–2.7 mm, subequal (outer sometimes slightly longer than inner), outer herbaceous or with herbaceous appendages, inner mostly chartaceous, midnerves (and often 2 collateral nerves) evident, (margins often narrowly membranous, fimbriate or tomentose) apices acute, acuminate to cuspidate, abaxial faces stipitate-glandular.</text>
      <biological_entity id="o13245" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o13246" from="18" name="quantity" src="d0_s8" to="28" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s8" to="tan" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="elliptic" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s8" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o13246" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" />
      </biological_entity>
      <biological_entity constraint="outer" id="o13247" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="with herbaceous appendages" value_original="with herbaceous appendages" />
      </biological_entity>
      <biological_entity id="o13248" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character is_modifier="true" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o13249" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o13250" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o13251" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s8" to="cuspidate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13252" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <relation from="o13247" id="r1201" name="with" negation="false" src="d0_s8" to="o13248" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets (0–) 1–7;</text>
      <biological_entity id="o13253" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s9" to="1" to_inclusive="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s9" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>laminae 7–10 × 1.5–2.8 mm.</text>
      <biological_entity id="o13254" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 7–20;</text>
      <biological_entity id="o13255" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s11" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 8–9.5 mm.</text>
      <biological_entity id="o13256" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="9.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae tan to reddish, ellipsoid, 5–7 mm, glabrous or distally hairy;</text>
      <biological_entity id="o13257" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s13" to="reddish" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi off-white to reddish-brown, 7–9 mm. 2n = 18.</text>
      <biological_entity id="o13258" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="off-white" name="coloration" src="d0_s14" to="reddish-brown" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13259" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky flats and sparsely wooded slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky flats" />
        <character name="habitat" value="wooded slopes" modifier="sparsely" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15.</number>
  <other_name type="common_name">Greene’s goldenbush</other_name>
  <other_name type="common_name">rabbitbrush</other_name>
  <discussion>A tomentose entity that was recognized as Haplopappus greenei subsp. mollis differs also in other ways from typical Ericameria greenei. It may merit recognition at some level. A biosystematic and population-level investigation of this complex is needed to better understand the causes and significance of such variation.</discussion>
  
</bio:treatment>