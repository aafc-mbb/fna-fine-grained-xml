<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="treatment_page">234</other_info_on_meta>
    <other_info_on_meta type="illustration_page">227</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">heterotheca</taxon_name>
    <taxon_name authority="(Lamarck) Britton &amp; Rusby" date="1887" rank="species">subaxillaris</taxon_name>
    <place_of_publication>
      <publication_title>Trans. New York Acad. Sci.</publication_title>
      <place_in_publication>7: 10. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus heterotheca;species subaxillaris</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416651</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Inula</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="species">subaxillaris</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl.</publication_title>
      <place_in_publication>3: 259. 1789</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Inula;species subaxillaris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials, 10–200 cm (aromatic);</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted (rarely perennating in south from proximal stem nodes).</text>
      <biological_entity id="o5266" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–4+, procumbent to erect (sometimes reddish-brown, proximal to distal branches well developed in larger plants), sparsely to densely hispido-strigose, sparsely to densely stipitate-glandular.</text>
      <biological_entity id="o5268" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="4" upper_restricted="false" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="hispido-strigose" modifier="sparsely to densely; densely" name="pubescence" src="d0_s2" to="sparsely densely stipitate-glandular" />
        <character char_type="range_value" from="hispido-strigose" name="pubescence" src="d0_s2" to="sparsely densely stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal rarely persisting to flowering, sometimes present and withered, brown to black;</text>
      <biological_entity id="o5269" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" modifier="rarely" name="duration" src="d0_s3" value="persisting" value_original="persisting" />
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="flowering" value_original="flowering" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="life_cycle" src="d0_s3" value="withered" value_original="withered" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s3" to="black" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal to mid cauline petiolate (petioles 10–40 mm, bases auriculate-clasping), blades ovate to elliptic or lanceolate, 10–70 × 6–55 mm, bases cuneate to attenuate, margins flat or undulate, coarsely serrate or entire, basally long-ciliate, apices acute, faces moderately hispido-scabrous;</text>
      <biological_entity id="o5270" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="basal" name="position" src="d0_s4" to="mid" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o5271" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="elliptic or lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="55" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5272" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
      </biological_entity>
      <biological_entity id="o5273" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="basally" name="architecture_or_pubescence_or_shape" src="d0_s4" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
      <biological_entity id="o5274" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o5275" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence_or_relief" src="d0_s4" value="hispido-scabrous" value_original="hispido-scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>distal sessile, blades ovate to lanceolate, 10–90 × 2–20 mm, reduced distally, bases often becoming cordate distally, subclasping or not clasping, margins entire, faces sparsely to moderately hispido-strigose, sparsely to densely stipitate-glandular.</text>
      <biological_entity id="o5276" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o5277" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o5278" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="90" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o5279" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often becoming; distally" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o5280" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5281" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="hispido-strigose" modifier="moderately" name="pubescence" src="d0_s5" to="sparsely densely stipitate-glandular" />
        <character char_type="range_value" from="hispido-strigose" name="pubescence" src="d0_s5" to="sparsely densely stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 3–180+ in corymbo-paniculiform arrays, sometimes becoming profusely branched (loosely paniculiform or broadly corymbiform in larger plants), branches ascending to spreading.</text>
      <biological_entity id="o5282" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in branches" constraintid="o5283" from="3" name="quantity" src="d0_s6" to="180" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o5283" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbo-paniculiform" value_original="corymbo-paniculiform" />
        <character is_modifier="true" modifier="sometimes becoming profusely" name="architecture" src="d0_s6" value="branched" value_original="branched" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s6" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 2–35 mm, sparsely to densely hispido-strigose, moderately to densely stipitate-glandular;</text>
      <biological_entity id="o5284" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s7" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 0–4, proximal leaflike, ovate to lanceolate, reduced distally and becoming linear, faces sparsely to moderately hispido-strigose, sparsely to densely stipitate-glandular.</text>
      <biological_entity id="o5285" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5286" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="lanceolate" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s8" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="becoming" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o5287" name="face" name_original="faces" src="d0_s8" type="structure">
        <character char_type="range_value" from="hispido-strigose" modifier="moderately" name="pubescence" src="d0_s8" to="sparsely densely stipitate-glandular" />
        <character char_type="range_value" from="hispido-strigose" name="pubescence" src="d0_s8" to="sparsely densely stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres hemispheric to campanulate, 4–8 (–10) mm.</text>
      <biological_entity id="o5288" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character char_type="range_value" from="hemispheric" name="shape" src="d0_s9" to="campanulate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 4–6 series, lanceolate, sometimes strongly unequal, margins scarious and distally strigoso-ciliate, faces sparsely to densely stipitate-glandular, with 0–28 coarse, scabro-strigose hairs distally.</text>
      <biological_entity id="o5289" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="sometimes strongly" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o5290" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="6" />
      </biological_entity>
      <biological_entity id="o5291" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_shape" src="d0_s10" value="strigoso-ciliate" value_original="strigoso-ciliate" />
      </biological_entity>
      <biological_entity id="o5292" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o5293" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s10" to="28" />
        <character is_modifier="true" name="relief" src="d0_s10" value="coarse" value_original="coarse" />
        <character is_modifier="true" name="pubescence" src="d0_s10" value="scabro-strigose" value_original="scabro-strigose" />
      </biological_entity>
      <relation from="o5289" id="r475" name="in" negation="false" src="d0_s10" to="o5290" />
      <relation from="o5292" id="r476" name="with" negation="false" src="d0_s10" to="o5293" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 15–35;</text>
      <biological_entity id="o5294" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s11" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 3–7 (–9) × 1–2 mm.</text>
      <biological_entity id="o5295" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 25–60;</text>
      <biological_entity id="o5296" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s13" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas weakly ampliate, 2–9 mm, proximal throats glabrous to sparsely short-strigose, lobes 0.5–0.7 mm, glabrous.</text>
      <biological_entity id="o5297" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="weakly" name="size" src="d0_s14" value="ampliate" value_original="ampliate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5298" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s14" to="sparsely short-strigose" />
      </biological_entity>
      <biological_entity id="o5299" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae dimorphic, obconic, ribs 2–3, (ray) triangular in cross-section, 1.5–2.5 mm, faces glabrous to slightly strigose, (disc) laterally compressed, 2–4 mm, faces moderately to densely strigose;</text>
      <biological_entity id="o5300" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s15" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obconic" value_original="obconic" />
      </biological_entity>
      <biological_entity id="o5301" name="rib" name_original="ribs" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s15" to="3" />
        <character constraint="in cross-section" constraintid="o5302" is_modifier="false" name="shape" src="d0_s15" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5302" name="cross-section" name_original="cross-section" src="d0_s15" type="structure" />
      <biological_entity id="o5303" name="face" name_original="faces" src="d0_s15" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s15" to="slightly strigose" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5304" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s15" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 0 (ray) or (disc) tan to rust, outer of linear to triangular scales 0.25–0.6 mm, inner of 25–45 bristles 4–9 mm, longest weakly clavate.</text>
      <biological_entity id="o5305" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s16" value="tan to rust" value_original="tan to rust" />
      </biological_entity>
      <biological_entity id="o5306" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s16" to="triangular" />
        <character char_type="range_value" from="0.25" from_unit="mm" name="some_measurement" src="d0_s16" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5308" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s16" to="45" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="9" to_unit="mm" />
      </biological_entity>
      <relation from="o5305" id="r477" name="outer of" negation="false" src="d0_s16" to="o5306" />
      <relation from="o5307" id="r478" name="consist_of" negation="false" src="d0_s16" to="o5308" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 18.</text>
      <biological_entity constraint="inner" id="o5307" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character is_modifier="false" name="length" notes="" src="d0_s16" value="longest" value_original="longest" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s16" value="clavate" value_original="clavate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5309" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Ark., Calif., Colo., Conn., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Md., Miss., Mo., N.C., N.J., N.Mex., N.Y., Nebr., Nev., Okla., Pa., S.C., Tenn., Tex., Utah, Va.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Heterotheca subaxillaris is a very weedy, morphologically variable species that has been treated as three species (B. L. Wagenknecht 1960; V. L. Harms 1965, 1970; J. C. Semple 1996) or as a single polymorphic species (G. L. Nesom 1990e). Harms (1965) acknowledged the difficulty in finding consistent characters to separate plants on a geographic basis, as did Nesom (1990e). Harms (1970) noted that H. psammophila and H. latifolia are probably conspecific. It seems best to treat the complex as a single polymorphic species. In historic times, human-assisted dispersal of possibly once distinct races has likely resulted in blurring of such distinctions with one exception. Plants of the outer coastal plain from Texas to North Carolina usually have a cluster of coarse hairs near the tip of phyllaries, at least in summer and fall-blooming plants.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Outer and mid phyllaries with apical tuft of 6–28 coarse, scabro-strigose hairs; coastal plain, Texas to Delaware</description>
      <determination>1a Heterotheca subaxillaris subsp. subaxillaris</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Outer and mid phyllaries without distinct apical tuft of hairs (sometimes distally with 1–2 coarse, scabro-strigose hairs); throughout s United States</description>
      <determination>1b Heterotheca subaxillaris subsp. latifolia</determination>
    </key_statement>
  </key>
</bio:treatment>