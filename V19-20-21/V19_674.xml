<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">390</other_info_on_meta>
    <other_info_on_meta type="mention_page">392</other_info_on_meta>
    <other_info_on_meta type="mention_page">393</other_info_on_meta>
    <other_info_on_meta type="mention_page">395</other_info_on_meta>
    <other_info_on_meta type="mention_page">413</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="treatment_page">412</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Gaertner" date="1791" rank="genus">antennaria</taxon_name>
    <taxon_name authority="(Trautvetter) E. Ekman" date="1928" rank="species">friesiana</taxon_name>
    <place_of_publication>
      <publication_title>Svensk Bot. Tidskr.</publication_title>
      <place_in_publication>22: 416. 1928</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus antennaria;species friesiana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066074</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Antennaria</taxon_name>
    <taxon_name authority="(Linnaeus) Gaertner" date="unknown" rank="species">alpina</taxon_name>
    <taxon_name authority="Trautvetter" date="unknown" rank="variety">friesiana</taxon_name>
    <place_of_publication>
      <publication_title>Trudy Imp. S.-Petersburgsk. Bot. Sada</publication_title>
      <place_in_publication>6: 24. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Antennaria;species alpina;variety friesiana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Dioecious or gynoecious (staminate plants uncommon or in equal frequencies to pistillates, respectively).</text>
      <biological_entity id="o11070" name="whole-organism" name_original="" src="d0_s0" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="gynoecious" value_original="gynoecious" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Plants 7–15 cm (stems stipitate-glandular, hairs purple).</text>
      <biological_entity id="o11071" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stolons 0.1–4 cm.</text>
      <biological_entity id="o11072" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves 1-nerved, narrowly spatulate to oblanceolate, 11–30 × 2–4 mm, tips mucronate, abaxial faces tomentose, adaxial green-glabrescent to gray-pubescent.</text>
      <biological_entity constraint="basal" id="o11073" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="narrowly spatulate" name="shape" src="d0_s3" to="oblanceolate" />
        <character char_type="range_value" from="11" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11074" name="tip" name_original="tips" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11075" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11076" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="green-glabrescent" name="pubescence" src="d0_s3" to="gray-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves linear, 4–20 mm, flagged.</text>
      <biological_entity constraint="cauline" id="o11077" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="20" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="flagged" value_original="flagged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 2–6 in corymbiform arrays.</text>
      <biological_entity id="o11078" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o11079" from="2" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <biological_entity id="o11079" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres: staminate 4–6.5 mm;</text>
      <biological_entity id="o11080" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pistillate 5.5–8 mm.</text>
      <biological_entity id="o11081" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries distally usually black, light-brown, dark-brown, or olivaceous, sometimes.</text>
      <biological_entity id="o11082" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="distally usually" name="coloration" src="d0_s8" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="olivaceous" value_original="olivaceous" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="olivaceous" value_original="olivaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas: staminate 2.5–3 mm;</text>
      <biological_entity id="o11083" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate 3–4.5 mm.</text>
      <biological_entity id="o11084" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.2–2 mm, glabrous or slightly papillate;</text>
      <biological_entity id="o11085" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s11" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: staminate 3–4 mm;</text>
      <biological_entity id="o11086" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pistillate 3.5–5 mm. 2n = 28, 56, 63, 100+.</text>
      <biological_entity id="o11087" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11088" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
        <character name="quantity" src="d0_s13" value="56" value_original="56" />
        <character name="quantity" src="d0_s13" value="63" value_original="63" />
        <character char_type="range_value" from="100" name="quantity" src="d0_s13" upper_restricted="false" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Nfld. and Labr., Nunavut, Que., Yukon; Alaska; Arctic North America, arctic Siberia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Arctic North America" establishment_means="native" />
        <character name="distribution" value="arctic Siberia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  <other_name type="past_name">frieseana</other_name>
  <other_name type="common_name">Fries’s pussytoes</other_name>
  <other_name type="common_name">antennaire de Fries</other_name>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <discussion>The Antennaria friesiana complex consists of subsp. alaskana, subsp. neoalaskana, and subsp. friesiana, the former two are dioecious (sexual) phases of the latter gynoecious (asexual) form. The sexual populations are known from Alaska and cordilleran areas of northern Yukon and adjacent Northwest Territories (R. J. Bayer 1991). The apomictic phase is almost circumpolar, occurring from the central and eastern Siberian plateau eastward across the North American arctic to Greenland (Bayer). E. Hultén (1968) circumscribed a fourth subspecies, A. friesiana subsp. compacta. After studying its morphology, in the field and herbarium, it is apparent that Hultén’s taxon contains at least three incongruous entities that are probably not at all related to the other two subspecies of A. friesiana. Hultén’s subsp. compacta included A. densifolia, which is recognized as a distinct species, and A. crymophila and A. neoalaskana as taxonomic synonyms. Antennaria compacta in the strict sense and A. crymophila are perhaps hybrid apomicts and are treated here in A. alpina (see Bayer for details). Antennaria neoalaskana is treated here as a subspecies of A. friesiana.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stolons 1–4 cm (usually prostrate, sometimes ascending, usually rooting at tips, plants forming mats); involucres: pistillate 7–8 mm</description>
      <determination>33c Antennaria friesiana subsp. neoalaskana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stolons 0.5–1 cm (erect, usually not rooting at tips, plants not forming mats); involucres: pistillate 5.5–7 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants gynoecious (staminates unknown)</description>
      <determination>33b Antennaria friesiana subsp. friesiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants dioecious (staminates and pistillates in equal frequencies in populations)</description>
      <determination>33a Antennaria friesiana subsp. alaskana</determination>
    </key_statement>
  </key>
</bio:treatment>