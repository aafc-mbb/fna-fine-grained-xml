<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">3</other_info_on_meta>
    <other_info_on_meta type="mention_page">9</other_info_on_meta>
    <other_info_on_meta type="mention_page">46</other_info_on_meta>
    <other_info_on_meta type="treatment_page">48</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="A. Gray" date="1865" rank="genus">RIGIOPAPPUS</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>6: 548. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus RIGIOPAPPUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin rigeo, rigid, and pappus; alluding to stiff pappus scales</other_info_on_name>
    <other_info_on_name type="fna_id">128593</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–30+ cm (taprooted).</text>
      <biological_entity id="o3832" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect, simple or branched (laterals often overtopping main-stems), pilosulous to hirtellous, glabrate.</text>
      <biological_entity id="o3833" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="pilosulous" name="pubescence" src="d0_s1" to="hirtellous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline (at flowering);</text>
      <biological_entity id="o3835" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>± sessile;</text>
      <biological_entity id="o3834" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (1-nerved or 3-nerved) narrowly oblanceolate to lance-linear or linear, margins entire, faces glabrous or sparsely hirsutulous.</text>
      <biological_entity id="o3836" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity id="o3837" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3838" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly.</text>
      <biological_entity id="o3839" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± cylindric to turbinate, 1–3 (–5+) mm diam.</text>
      <biological_entity id="o3840" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="less cylindric" name="shape" src="d0_s7" to="turbinate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s7" to="5" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 11–20+ in 2+ series, 1-nerved (flat) linear to lanceolate or subulate, subequal (distinct), herbaceous, margins scarious, faces hirsutulous (± navicular, each of the inner usually ± enfolding a floret).</text>
      <biological_entity id="o3841" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o3842" from="11" name="quantity" src="d0_s8" to="20" upper_restricted="false" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="lanceolate or subulate" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s8" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o3842" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o3843" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o3844" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles ± flat, obscurely pitted, sparsely paleate (between ray and disc-florets, paleae resembling inner phyllaries).</text>
      <biological_entity id="o3845" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="obscurely" name="relief" src="d0_s9" value="pitted" value_original="pitted" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 3–13+ (usually 3, 5, or 8), pistillate, fertile;</text>
      <biological_entity id="o3846" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s10" to="13" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellowish, often tinged with red or purple (laminae inconspicuous).</text>
      <biological_entity id="o3847" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="tinged with red or tinged with purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 5–70+, bisexual, fertile;</text>
      <biological_entity id="o3848" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s12" to="70" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas pale-yellow, sometimes tipped with purple, tubes shorter than narrowly funnelform throats, lobes 2–4, erect, ± deltate;</text>
      <biological_entity id="o3849" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale-yellow" value_original="pale-yellow" />
        <character constraint="with purple" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s13" value="tipped" value_original="tipped" />
      </biological_entity>
      <biological_entity id="o3850" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than narrowly funnelform throats" constraintid="o3851" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o3851" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o3852" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="4" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branch appendages subulate to filiform.</text>
      <biological_entity constraint="style-branch" id="o3853" name="appendage" name_original="appendages" src="d0_s14" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s14" to="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae ± cylindric to fusiform, little, if at all, compressed, transversely rugulose, all minutely hirsutulous or ray cypselae glabrous or nearly so;</text>
      <biological_entity id="o3854" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="less cylindric" name="shape" src="d0_s15" to="fusiform" />
      </biological_entity>
      <biological_entity constraint="ray" id="o3855" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="true" modifier="transversely" name="relief" src="d0_s15" value="rugulose" value_original="rugulose" />
        <character is_modifier="true" modifier="minutely" name="pubescence" src="d0_s15" value="hirsutulous" value_original="hirsutulous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s15" value="nearly" value_original="nearly" />
      </biological_entity>
      <relation from="o3854" id="r347" name="at" negation="false" src="d0_s15" to="o3855" />
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, usually of 3–5 subulate scales (sometimes much reduced or 0 on ray cypselae).</text>
      <biological_entity id="o3857" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s16" to="5" />
        <character is_modifier="true" name="shape" src="d0_s16" value="subulate" value_original="subulate" />
      </biological_entity>
      <relation from="o3856" id="r348" modifier="usually" name="consist_of" negation="false" src="d0_s16" to="o3857" />
    </statement>
    <statement id="d0_s17">
      <text>x = 9.</text>
      <biological_entity id="o3856" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o3858" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>146.</number>
  <discussion>Species 1.</discussion>
  <references>
    <reference>Ornduff, R. and B. A. Bohm. 1975. Relationships of Tracyina and Rigiopappus (Compositae). Madroño 23: 53–55.</reference>
  </references>
  
</bio:treatment>