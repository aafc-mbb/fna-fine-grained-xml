<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="treatment_page">73</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1759" rank="genus">zinnia</taxon_name>
    <taxon_name authority="Cavanilles" date="1791" rank="species">violacea</taxon_name>
    <place_of_publication>
      <publication_title>Icon.</publication_title>
      <place_in_publication>1: 57, plate 81. 1791</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus zinnia;species violacea</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250067848</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Zinnia</taxon_name>
    <taxon_name authority="Jacquin" date="unknown" rank="species">elegans</taxon_name>
    <taxon_hierarchy>genus Zinnia;species elegans;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, to 100 (–200) cm.</text>
      <biological_entity id="o18897" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems greenish, becoming yellowish to purplish, unbranched or sparingly branched distal to bases, hirsute to strigose or scabrous.</text>
      <biological_entity id="o18898" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="yellowish" modifier="becoming" name="coloration" src="d0_s1" to="purplish" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character constraint="to bases" constraintid="o18899" is_modifier="false" name="position_or_shape" src="d0_s1" value="distal" value_original="distal" />
        <character char_type="range_value" from="hirsute" name="pubescence" notes="" src="d0_s1" to="strigose or scabrous" />
      </biological_entity>
      <biological_entity id="o18899" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades 3–5-nerved, ovate to oblong, mostly 60–100 × 20–60 mm, scabrellous to glabrate.</text>
      <biological_entity id="o18900" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="3-5-nerved" value_original="3-5-nerved" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="oblong" />
        <character char_type="range_value" from="60" from_unit="mm" name="length" src="d0_s2" to="100" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s2" to="60" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="relief" src="d0_s2" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles to 85 mm.</text>
      <biological_entity id="o18901" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="85" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres ± hemispheric or broader, 10–15 × 5–25 mm.</text>
      <biological_entity id="o18902" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s4" value="broader" value_original="broader" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries obovate, becoming scarious, glabrous or sparsely hairy, apices rounded, erose or fimbriate.</text>
      <biological_entity id="o18903" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="becoming" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o18904" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s5" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae red to purple, apices rounded to acute, fimbriate.</text>
      <biological_entity id="o18905" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character char_type="range_value" from="red" name="coloration" src="d0_s6" to="purple" />
      </biological_entity>
      <biological_entity id="o18906" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s6" to="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 8–21 (more in “double” cultivars);</text>
      <biological_entity id="o18907" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas usually red (white, yellow, or purple in cultivars), laminae spatulate to obovate, 10–35 mm.</text>
      <biological_entity id="o18908" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o18909" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 100–150+;</text>
      <biological_entity id="o18910" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s9" to="150" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 7–9 mm, lobes 1–2.5 mm.</text>
      <biological_entity id="o18911" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18912" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 6–10 mm, 3-angled (ray) or ± compressed (disc), not or faintly ribbed, ciliolate;</text>
      <biological_entity id="o18913" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not; faintly" name="architecture_or_shape" src="d0_s11" value="ribbed" value_original="ribbed" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi 0.2n = 24.</text>
      <biological_entity id="o18914" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18915" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500? m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="" from="0" from_unit="" constraint=" ? m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Conn., Fla., Ga., Ky., La., N.C., Ohio, Pa., S.C., Tex.; Mexico; West Indies (Cuba); Central America; South America (Bolivia); also introduced in Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Cuba)" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
        <character name="distribution" value="also  in Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Elegant or garden zinnia</other_name>
  <discussion>Zinnia violacea is perhaps adventive in Costa Rica, Panama, Cuba, Bolivia, China, and Malesia. The most widely cultivated Zinnia, it is reported to have escaped from cultivation and apparently naturalized in ten eastern and southern states but is nowhere common in the flora area. It is not as weedy as Z. peruviana, possibly because it lacks awns and thus is not as easily dispersed by animals.</discussion>
  
</bio:treatment>