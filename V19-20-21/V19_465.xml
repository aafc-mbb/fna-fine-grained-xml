<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="mention_page">312</other_info_on_meta>
    <other_info_on_meta type="mention_page">314</other_info_on_meta>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">malacothrix</taxon_name>
    <taxon_name authority="A. Gray in W. H. Brewer et al." date="1876" rank="species">clevelandii</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>1: 433. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus malacothrix;species clevelandii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067145</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crepis</taxon_name>
    <taxon_name authority="Philippi" date="unknown" rank="species">geisseana</taxon_name>
    <taxon_hierarchy>genus Crepis;species geisseana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Malacothrix</taxon_name>
    <taxon_name authority="Reiche" date="unknown" rank="species">senecioides</taxon_name>
    <taxon_hierarchy>genus Malacothrix;species senecioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 4–36 cm.</text>
      <biological_entity id="o17894" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="36" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–5, erect or ascending, branched mostly distally, glabrous.</text>
      <biological_entity id="o17895" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="5" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="mostly distally; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: proximal oblanceolate to lance-linear, sometimes pinnately lobed, not fleshy, ultimate margins usually dentate, faces glabrous;</text>
      <biological_entity constraint="cauline" id="o17896" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o17897" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="lance-linear" />
        <character is_modifier="false" modifier="sometimes pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o17898" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o17899" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal reduced (margins 2–4-dentate near bases or entire).</text>
      <biological_entity constraint="cauline" id="o17900" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o17901" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of 5–12, lance-deltate to lanceolate bractlets, hyaline margins 0.05–0.2 mm.</text>
      <biological_entity id="o17902" name="calyculus" name_original="calyculi" src="d0_s4" type="structure">
        <character char_type="range_value" from="lance-deltate" modifier="of 5-12" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o17903" name="bractlet" name_original="bractlets" src="d0_s4" type="structure" />
      <biological_entity id="o17904" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s4" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres narrowly campanulate, 4–8+ × 2–4+ mm.</text>
      <biological_entity id="o17905" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="8" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 8–15+ in 2+ series, lance-linear to linear, ± equal, hyaline margins 0.05–0.3 mm wide, abaxial faces glabrous.</text>
      <biological_entity id="o17906" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o17907" from="8" name="quantity" src="d0_s6" to="15" upper_restricted="false" />
        <character char_type="range_value" from="lance-linear" name="arrangement_or_course_or_shape" notes="" src="d0_s6" to="linear" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s6" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o17907" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o17908" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s6" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17909" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles not bristly.</text>
      <biological_entity id="o17910" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s7" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets (19–67);</text>
      <biological_entity id="o17911" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="19" name="atypical_quantity" src="d0_s8" to="67" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas pale-yellow, 4–7.4 mm;</text>
      <biological_entity id="o17912" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer ligules exserted 1–3 mm.</text>
      <biological_entity constraint="outer" id="o17913" name="ligule" name_original="ligules" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae fusiform or prismatic, 1.2–1.8 mm, ribs extending to apices, 5 more prominent than others;</text>
      <biological_entity id="o17914" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="shape" src="d0_s11" value="prismatic" value_original="prismatic" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17915" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character constraint="than others" constraintid="o17917" is_modifier="false" name="prominence" src="d0_s11" value="more prominent" value_original="more prominent" />
      </biological_entity>
      <biological_entity id="o17916" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <biological_entity id="o17917" name="other" name_original="others" src="d0_s11" type="structure" />
      <relation from="o17915" id="r1613" name="extending to" negation="false" src="d0_s11" to="o17916" />
    </statement>
    <statement id="d0_s12">
      <text>persistent pappi of 15–24+, needlelike teeth plus 1 bristle.</text>
      <biological_entity id="o17918" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="true" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o17919" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s12" to="24" upper_restricted="false" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s12" value="needlelike" value_original="needlelike" />
        <character name="quantity" src="d0_s12" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o17920" name="bristle" name_original="bristle" src="d0_s12" type="structure" />
      <relation from="o17918" id="r1614" name="consist_of" negation="false" src="d0_s12" to="o17919" />
    </statement>
    <statement id="d0_s13">
      <text>Pollen 70–100% 3-porate, mean 25 µm. 2n = 14.</text>
      <biological_entity id="o17921" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="70-100%" name="architecture" src="d0_s13" value="3-porate" value_original="3-porate" />
        <character name="some_measurement" src="d0_s13" unit="um" value="25" value_original="25" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17922" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cleared areas (burns, slides), usually chaparral, rarely margins of creosote bush shrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="areas" modifier="cleared" />
        <character name="habitat" value="burns" />
        <character name="habitat" value="margins" modifier="slides" constraint="of creosote bush shrub" />
        <character name="habitat" value="creosote bush shrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California); introduced, South America (Argentina, Chile).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value="South America (Chile)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="past_name">clevelandi</other_name>
  <other_name type="common_name">Cleveland’s desertdandelion</other_name>
  <discussion>Malacothrix clevelandii grows in northwestern California, Sierra Nevada foothills, San Joaquin Valley, central western California, and northern Channel Islands (Santa Rosa Island).</discussion>
  
</bio:treatment>