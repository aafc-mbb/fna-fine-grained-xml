<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">70</other_info_on_meta>
    <other_info_on_meta type="treatment_page">71</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Lamarck" date="1792" rank="genus">sanvitalia</taxon_name>
    <taxon_name authority="Lamarck" date="1792" rank="species">procumbens</taxon_name>
    <place_of_publication>
      <publication_title>J. Hist. Nat.</publication_title>
      <place_in_publication>2: 176, plate 33. 1792</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus sanvitalia;species procumbens</taxon_hierarchy>
    <other_info_on_name type="fna_id">200024396</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems procumbent to erect, 3–15 cm.</text>
      <biological_entity id="o19183" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaf-blades ovate to lance-linear, 10–60 × 4–31 mm.</text>
      <biological_entity id="o19184" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="lance-linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s1" to="60" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s1" to="31" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllaries 13–21, unequal.</text>
      <biological_entity id="o19185" name="phyllary" name_original="phyllaries" src="d0_s2" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s2" to="21" />
        <character is_modifier="false" name="size" src="d0_s2" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ray corollas 2–9 mm.</text>
      <biological_entity constraint="ray" id="o19186" name="corolla" name_original="corollas" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cypselae: rays 2.5–3.5 mm with awns 1–3 mm, clearly 3-faced, adaxial faces often 2–3-nerved;</text>
      <biological_entity id="o19187" name="cypsela" name_original="cypselae" src="d0_s4" type="structure" />
      <biological_entity id="o19188" name="ray" name_original="rays" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="with awns" constraintid="o19189" from="2.5" from_unit="mm" name="some_measurement" src="d0_s4" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="clearly" name="shape" notes="" src="d0_s4" value="3-faced" value_original="3-faced" />
      </biological_entity>
      <biological_entity id="o19189" name="awn" name_original="awns" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o19190" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s4" value="2-3-nerved" value_original="2-3-nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>discs strongly dimorphic within single heads: outer 4-angled, wingless, inner ± flattened, 1–2-winged.</text>
      <biological_entity id="o19192" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="single" value_original="single" />
        <character is_modifier="false" name="position" src="d0_s5" value="outer" value_original="outer" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>2n = 16, 32.</text>
      <biological_entity id="o19191" name="disc" name_original="discs" src="d0_s5" type="structure">
        <character constraint="within heads" constraintid="o19192" is_modifier="false" modifier="strongly" name="growth_form" src="d0_s5" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="wingless" value_original="wingless" />
        <character is_modifier="false" name="position" src="d0_s5" value="inner" value_original="inner" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-2-winged" value_original="1-2-winged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19193" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="16" value_original="16" />
        <character name="quantity" src="d0_s6" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ruderal</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ruderal" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Md., Tex.; Mexico; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Sanvitalia procumbens has been seen only sporadically in the flora; it probably is not a resident.</discussion>
  
</bio:treatment>