<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">471</other_info_on_meta>
    <other_info_on_meta type="mention_page">483</other_info_on_meta>
    <other_info_on_meta type="treatment_page">486</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nees" date="1832" rank="genus">symphyotrichum</taxon_name>
    <taxon_name authority="(Rafinesque) G. L. Nesom" date="1995" rank="subgenus">virgulus</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom" date="1995" rank="species">fendleri</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>77: 282. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus symphyotrichum;subgenus virgulus;species fendleri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067645</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">fendleri</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 66. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species fendleri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Virgulus</taxon_name>
    <taxon_name authority="(A. Gray) Reveal &amp; Keener" date="unknown" rank="species">fendleri</taxon_name>
    <taxon_hierarchy>genus Virgulus;species fendleri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 6–30 cm, cespitose;</text>
    </statement>
    <statement id="d0_s1">
      <text>with thick, woody, branched caudices.</text>
      <biological_entity id="o3203" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o3204" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="true" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o3203" id="r296" name="with" negation="false" src="d0_s1" to="o3204" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10+, decumbent to ascending or erect (grayish brown, slender), sparsely strigoso-hispid, ± scabrous.</text>
      <biological_entity id="o3205" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s2" to="ascending or erect" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="strigoso-hispid" value_original="strigoso-hispid" />
        <character is_modifier="false" modifier="more or less" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves (dark bright green) firm, much reduced distally, margins entire, apices acute, mucronate, faces usually glabrous, sometimes sparsely strigoso-hispid;</text>
      <biological_entity id="o3206" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="much; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o3207" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3208" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o3209" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="pubescence" src="d0_s3" value="strigoso-hispid" value_original="strigoso-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal withering by flowering (new vernal rosettes often present), sessile, blades (1–3-nerved) linear-oblanceolate, 20–40 × 5–30 mm, bases attenuate, margins scabro-ciliate;</text>
      <biological_entity constraint="basal" id="o3210" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by blades, bases, margins" constraintid="o3211, o3212, o3213" is_modifier="false" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o3211" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="scabro-ciliate" value_original="scabro-ciliate" />
      </biological_entity>
      <biological_entity id="o3212" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="scabro-ciliate" value_original="scabro-ciliate" />
      </biological_entity>
      <biological_entity id="o3213" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="30" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="scabro-ciliate" value_original="scabro-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal cauline sometimes persistent, sessile, blades linear to linear-lanceolate, 10–40 × 5–20 mm, bases sometimes subclasping, margins entire, scabrous;</text>
      <biological_entity constraint="proximal cauline" id="o3214" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o3215" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="linear-lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3216" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="subclasping" value_original="subclasping" />
      </biological_entity>
      <biological_entity id="o3217" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>distal sessile, blades linear to linear-lanceolate, 20–40 × 2–3 mm, bases cuneate, margins entire, coarsely cililate-spinulose, apices acute, white-spinulose, faces sometimes stipitate-glandular.</text>
      <biological_entity constraint="distal" id="o3218" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o3219" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="linear-lanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s6" to="40" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3220" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o3221" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s6" value="cililate-spinulose" value_original="cililate-spinulose" />
      </biological_entity>
      <biological_entity id="o3222" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="white-spinulose" value_original="white-spinulose" />
      </biological_entity>
      <biological_entity id="o3223" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads in ± narrowly racemiform to paniculiform arrays, branches sometimes initially patent, then spreading to ascending.</text>
      <biological_entity id="o3224" name="head" name_original="heads" src="d0_s7" type="structure" />
      <biological_entity id="o3225" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character char_type="range_value" from="less narrowly racemiform" is_modifier="true" name="architecture" src="d0_s7" to="paniculiform" />
      </biological_entity>
      <biological_entity id="o3226" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes initially" name="orientation" src="d0_s7" value="patent" value_original="patent" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s7" to="ascending" />
      </biological_entity>
      <relation from="o3224" id="r297" name="in" negation="false" src="d0_s7" to="o3225" />
    </statement>
    <statement id="d0_s8">
      <text>Peduncles sparsely hispido-strigose, ± densely stipitate-glandular, bracts ± ascending, linear to lance-oblong, grading into phyllaries.</text>
      <biological_entity id="o3227" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o3228" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s8" to="lance-oblong" />
      </biological_entity>
      <biological_entity id="o3229" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure" />
      <relation from="o3228" id="r298" name="into" negation="false" src="d0_s8" to="o3229" />
    </statement>
    <statement id="d0_s9">
      <text>Involucres campanulate, 4–7 mm.</text>
      <biological_entity id="o3230" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–4 series, lanceolate, unequal, bases ± indurate, margins narrowly to widely scarious, hyaline except apically, sometimes ciliolate, often stipitate-glandular, green zones lanceolate to diamond-shaped, covering distal portion (outer), apices acuminate, spreading to reflexed, faces glabrous, moderately to densely short-stipitate-glandular.</text>
      <biological_entity id="o3231" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o3232" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <biological_entity id="o3233" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s10" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity id="o3234" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly to widely" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="diamond-shaped" />
      </biological_entity>
      <biological_entity id="o3235" name="zone" name_original="zones" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="apically; sometimes" name="pubescence" src="d0_s10" value="ciliolate" value_original="ciliolate" />
        <character is_modifier="true" modifier="often" name="pubescence" src="d0_s10" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3236" name="portion" name_original="portion" src="d0_s10" type="structure" />
      <biological_entity id="o3237" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s10" to="reflexed" />
      </biological_entity>
      <biological_entity id="o3238" name="face" name_original="faces" src="d0_s10" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s10" to="moderately densely short-stipitate-glandular" />
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s10" to="moderately densely short-stipitate-glandular" />
      </biological_entity>
      <relation from="o3231" id="r299" name="in" negation="false" src="d0_s10" to="o3232" />
      <relation from="o3234" id="r300" name="except" negation="false" src="d0_s10" to="o3235" />
      <relation from="o3234" id="r301" name="covering" negation="false" src="d0_s10" to="o3236" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 10–20;</text>
      <biological_entity id="o3239" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s11" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas light to dark lavender to purple, laminae 5–10 × 1–2 mm.</text>
      <biological_entity id="o3240" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s12" to="dark lavender" />
      </biological_entity>
      <biological_entity id="o3241" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets (7–) 10–30;</text>
      <biological_entity id="o3242" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s13" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s13" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow becoming reddish purple, 3.5–5 mm, throats narrowly funnelform, lobes narrowly triangular, 0.4–0.7 mm.</text>
      <biological_entity id="o3243" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s14" value="yellow becoming reddish purple" value_original="yellow becoming reddish purple" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3244" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o3245" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae brown (nerves stramineous), obovoid, ± compressed, 1.5–2.5 mm, 7–10-nerved, faces moderately strigillose;</text>
      <biological_entity id="o3246" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="7-10-nerved" value_original="7-10-nerved" />
      </biological_entity>
      <biological_entity id="o3247" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi cinnamon to sordid, sometimes purplish-tinged, 4.5–5 mm. 2n = 10.</text>
      <biological_entity id="o3248" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="cinnamon" name="coloration" src="d0_s16" to="sordid" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s16" value="purplish-tinged" value_original="purplish-tinged" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3249" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Aug–)Sep–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Sep" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, sandy, silty, shaly, often rocky soils, eroded limestone or sandstone outcrops, mixed-grass prairies, pastures, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="silty" />
        <character name="habitat" value="shaly" />
        <character name="habitat" value="rocky soils" modifier="often" />
        <character name="habitat" value="limestone" modifier="eroded" />
        <character name="habitat" value="sandstone outcrops" />
        <character name="habitat" value="mixed-grass prairies" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Kans., Nebr., N.Mex., Okla., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Fendler’s aster</other_name>
  <discussion>Symphyotrichum fendleri has been reported from Mexico (Chihuahua) [by C. H. Schultz-Bipontinus (1856) fide G. L. Nesom (pers. comm.)], but its occurrence in Mexico remains to be confirmed.</discussion>
  
</bio:treatment>