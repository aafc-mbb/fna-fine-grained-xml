<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">271</other_info_on_meta>
    <other_info_on_meta type="mention_page">272</other_info_on_meta>
    <other_info_on_meta type="treatment_page">273</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">calycadenia</taxon_name>
    <taxon_name authority="G. D. Carr" date="1975" rank="species">hooveri</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>27: 140, fig. 19. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus calycadenia;species hooveri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066276</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–60 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>self-compatible.</text>
      <biological_entity id="o24093" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="self-compatible" value_original="self-compatible" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems branched (branches relatively many, distal, filiform, flexible, minutely scabrous, glandular).</text>
      <biological_entity id="o24094" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly alternate, 1–6 (–8) cm, ± thinly hispidulous and ± long-hairy (especially proximal margins and adaxial faces).</text>
      <biological_entity id="o24095" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="more or less thinly" name="pubescence" src="d0_s3" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="long-hairy" value_original="long-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or in ± spiciform arrays (1–4 per node).</text>
      <biological_entity id="o24096" name="head" name_original="heads" src="d0_s4" type="structure">
        <character constraint="in more or less spiciform arrays" is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="in more or less spiciform arrays" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncular bracts subclaviform, 1–5 mm (hispidulous, sometimes ± pectinate-fimbriate), apices rounded, tack-glands 1 (terminal).</text>
      <biological_entity constraint="peduncular" id="o24097" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="subclaviform" value_original="subclaviform" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24098" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o24099" name="tack-gland" name_original="tack-glands" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 2.5–3.5 mm, abaxial faces ± hispidulous (hairs scattered, stout), ± shaggy long-hairy distally, especially margins, minutely glandular, tack-glands (0–) 1 (terminal).</text>
      <biological_entity id="o24100" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24101" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s6" value="shaggy" value_original="shaggy" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s6" value="long-hairy" value_original="long-hairy" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" notes="" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o24102" name="margin" name_original="margins" src="d0_s6" type="structure" />
      <biological_entity id="o24103" name="tack-gland" name_original="tack-glands" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s6" to="1" to_inclusive="false" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Paleae 3–5 mm (vestiture similar to phyllaries, tack-glands 0).</text>
      <biological_entity id="o24104" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets (0–) 1 (–2);</text>
      <biological_entity id="o24105" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s8" to="1" to_inclusive="false" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="2" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, tubes ca. 2 mm, laminae 2–3.5 mm (central lobes smaller than laterals, widest at bases, symmetric, sometimes 2-partite, laterals weakly asymmetric, sinuses ca. 2/3 laminae).</text>
      <biological_entity id="o24106" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o24107" name="tube" name_original="tubes" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o24108" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 1–2;</text>
      <biological_entity id="o24109" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas white, 2.5–3.5 mm.</text>
      <biological_entity id="o24110" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray cypselae 1.5–2.5 mm, smooth to rugose, glabrous.</text>
      <biological_entity constraint="ray" id="o24111" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s12" to="rugose" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc cypselae 2–3 mm, ± appressed-hairy;</text>
      <biological_entity constraint="disc" id="o24112" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s13" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi of 6–13 lanceolate-aristate, scales 1.5–2.5 mm. 2n = 14.</text>
      <biological_entity id="o24113" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity id="o24114" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" modifier="of 6-13 lanceolate-aristate" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24115" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, exposed places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="exposed places" modifier="rocky" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Calycadenia hooveri resembles variants of Calycadenia pauciflora; it is more closely related to C. villosa (G. D. Carr 1975b). Calycadenia hooveri is known only from the Sierra Nevada foothills of Calaveras, Mariposa, and Stanislaus counties.</discussion>
  
</bio:treatment>