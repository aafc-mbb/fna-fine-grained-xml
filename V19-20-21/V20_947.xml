<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="treatment_page">415</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1833" rank="genus">pyrrocoma</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="1894" rank="species">apargioides</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>2: 70. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus pyrrocoma;species apargioides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067418</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">apargioides</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 354. 1868 (as Aplopappus)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Haplopappus;species apargioides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pyrrocoma</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">demissa</taxon_name>
    <taxon_hierarchy>genus Pyrrocoma;species demissa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–18 (–30) cm.</text>
      <biological_entity id="o9243" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="18" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1–7, procumbent to decumbent or ascending, red tinged, scapiform, glabrous or sparsely tomentose.</text>
      <biological_entity id="o9244" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s1" to="7" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="red tinged" value_original="red tinged" />
        <character is_modifier="false" name="shape" src="d0_s1" value="scapiform" value_original="scapiform" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal petiolate, blades lanceolate to narrowly oblanceolate, 30–100 × 2–18 mm (leathery), margins usually coarsely dentate to laciniate, rarely entire, ciliate;</text>
      <biological_entity id="o9245" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o9246" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o9247" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s2" to="narrowly oblanceolate" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s2" to="100" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9248" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="usually coarsely dentate" name="shape" src="d0_s2" to="laciniate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>cauline sessile, blades linear-lanceolate (bractlike), 10–20 × 1–2 mm;</text>
      <biological_entity id="o9249" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o9250" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o9251" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>faces glabrous, eglandular.</text>
      <biological_entity id="o9252" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9253" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads usually borne singly, terminal, rarely 1–2 smaller proximally.</text>
      <biological_entity id="o9254" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="singly" value_original="singly" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="1" modifier="rarely" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" modifier="proximally" name="size" src="d0_s5" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 2–3 cm.</text>
      <biological_entity id="o9255" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric, 7–13 × 13–20 mm.</text>
      <biological_entity id="o9256" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="13" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 3–4 series, green, oblanceolate to narrowly oblong, 4–10 mm, unequal, margins white to purplish, entire, ciliate, apices green, broad, acute, faces glabrous.</text>
      <biological_entity id="o9257" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="green" value_original="green" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="narrowly oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o9258" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o9259" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s8" to="purplish" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o9260" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="width" src="d0_s8" value="broad" value_original="broad" />
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o9261" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o9257" id="r857" name="in" negation="false" src="d0_s8" to="o9258" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 11–40;</text>
      <biological_entity id="o9262" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s9" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 7–16 mm.</text>
      <biological_entity id="o9263" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc-florets 45–90;</text>
      <biological_entity id="o9264" name="disc-floret" name_original="disc-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="45" name="quantity" src="d0_s11" to="90" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 5–7 mm.</text>
      <biological_entity id="o9265" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae fusiform, slightly flattened, 5–7 mm, 3-angled, faces striate, glabrous;</text>
      <biological_entity id="o9266" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="3-angled" value_original="3-angled" />
      </biological_entity>
      <biological_entity id="o9267" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s13" value="striate" value_original="striate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi tawny, 5–7.5 mm. 2n = 12.</text>
      <biological_entity id="o9268" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="tawny" value_original="tawny" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9269" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in pine forest, wet meadows, open rocky slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in pine forest" />
        <character name="habitat" value="pine forest" />
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="rocky slopes" modifier="open" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100–3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Alpine-flames</other_name>
  <discussion>Pyrrocoma apargioides is distinguished by its sharply serrate or laciniate leaves, subscapiform stems with bractlike cauline leaves, and heads usually borne singly. It is thought to be closely related to P. racemosa (H. M. Hall 1928; R. A. Mayes 1976).</discussion>
  
</bio:treatment>