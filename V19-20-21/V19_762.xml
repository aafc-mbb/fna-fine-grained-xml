<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="mention_page">459</other_info_on_meta>
    <other_info_on_meta type="treatment_page">458</other_info_on_meta>
    <other_info_on_meta type="illustration_page">455</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">psilocarphus</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray in A. Gray et al." date="1886" rank="species">elatior</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer. ed.</publication_title>
      <place_in_publication>2, 1: 448. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus psilocarphus;species elatior</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067406</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Psilocarphus</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">oregonus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">elatior</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>8: 652. 1873</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Psilocarphus;species oregonus;variety elatior;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants greenish gray to silvery, ± sericeous.</text>
      <biological_entity id="o701" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="greenish gray" name="coloration" src="d0_s0" to="silvery" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="sericeous" value_original="sericeous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1 (–3), ± erect;</text>
      <biological_entity id="o702" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s1" to="3" />
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal internode lengths mostly 0.5–1.5 (–2) times leaf lengths.</text>
      <biological_entity constraint="proximal" id="o703" name="internode" name_original="internode" src="d0_s2" type="structure">
        <character constraint="leaf" constraintid="o704" is_modifier="false" modifier="mostly" name="length" src="d0_s2" value="0.5-1.5(-2) times leaf lengths" value_original="0.5-1.5(-2) times leaf lengths" />
      </biological_entity>
      <biological_entity id="o704" name="leaf" name_original="leaf" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Capitular leaves ± erect, appressed to heads, mostly oblanceolate to nearly linear, widest in distal 1/3, longest mostly 17–35 mm, lengths mostly 4.5–9 times widths, 2.5–5 times head heights.</text>
      <biological_entity constraint="capitular" id="o705" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character constraint="to heads" constraintid="o706" is_modifier="false" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="mostly oblanceolate" name="shape" notes="" src="d0_s3" to="nearly linear" />
        <character constraint="in distal 1/3" constraintid="o707" is_modifier="false" name="width" src="d0_s3" value="widest" value_original="widest" />
        <character is_modifier="false" modifier="mostly" name="length" notes="" src="d0_s3" value="longest" value_original="longest" />
        <character char_type="range_value" from="17" from_unit="mm" name="some_measurement" src="d0_s3" to="35" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="l_w_ratio" src="d0_s3" value="4.5-9" value_original="4.5-9" />
        <character constraint="head" constraintid="o708" is_modifier="false" name="height" src="d0_s3" value="2.5-5 times head heights" value_original="2.5-5 times head heights" />
      </biological_entity>
      <biological_entity id="o706" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o707" name="1/3" name_original="1/3" src="d0_s3" type="structure" />
      <biological_entity id="o708" name="head" name_original="head" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads ± spheric, largest 6–8 mm.</text>
      <biological_entity id="o709" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="spheric" value_original="spheric" />
        <character is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Receptacles unlobed.</text>
      <biological_entity id="o710" name="receptacle" name_original="receptacles" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pistillate paleae collectively ± hidden by indument, longest 2.8–3.8 mm (lengths 1.5–3 times longest diams.; wings supramedian).</text>
      <biological_entity id="o711" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character constraint="by indument" constraintid="o712" is_modifier="false" modifier="collectively more or less" name="prominence" src="d0_s6" value="hidden" value_original="hidden" />
        <character is_modifier="false" name="length" notes="" src="d0_s6" value="longest" value_original="longest" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s6" to="3.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o712" name="indument" name_original="indument" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Staminate corollas 1.3–1.9 mm, lobes 5.</text>
      <biological_entity id="o713" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s7" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o714" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae ± cylindric, terete, 0.9–1.7 mm.</text>
      <biological_entity id="o715" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s8" value="terete" value_original="terete" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s8" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting mid May–mid Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Aug" from="mid May" />
        <character name="fruiting time" char_type="range_value" to="mid Aug" from="mid May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mainly coastal or montane, relatively dry or seasonally flooded, wooded, grassy, or barren slopes, flats, often disturbed sites (roadsides, trails, drainages), rarely near vernal pools</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="barren slopes" modifier="mainly coastal or montane relatively dry or seasonally flooded wooded grassy or" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="disturbed sites" modifier="often" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="trails" />
        <character name="habitat" value="drainages" />
        <character name="habitat" value="vernal pools" modifier=") rarely near" />
        <character name="habitat" value="coastal" modifier="mainly" />
        <character name="habitat" value="wooded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Tall or meadow woollyheads</other_name>
  <discussion>Psilocarphus elatior occurs west of the Cascade Range from California to Vancouver Island, British Columbia, and in scattered areas eastward (northwestern Montana, mountains surrounding the border area common to Oregon, Washington, and Idaho). Reports of P. elatior from Alberta and Saskatchewan were based on relatively erect forms of P. brevissimus var. brevissimus. Psilocarphus elatior has been of conservation concern in Canada (J. M. Illingworth and G. W. Douglas 1994).</discussion>
  <discussion>Where sympatric, Psilocarphus elatior tends to inhabit relatively dry or seasonally flooded sites in more mesic coastal or montane climates and P. brevissimus var. brevissimus occurs mainly in wetter, seasonally inundated sites in semiarid climates. Some specimens appear to be intermediate; further study may show the two taxa to be better treated as varietally distinct. See also under P. brevissimus var. multiflorus.</discussion>
  
</bio:treatment>