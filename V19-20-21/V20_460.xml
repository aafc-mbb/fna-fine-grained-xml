<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">206</other_info_on_meta>
    <other_info_on_meta type="treatment_page">207</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="genus">chaetopappa</taxon_name>
    <taxon_name authority="A. Gray" date="1880" rank="species">parryi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>16: 82. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus chaetopappa;species parryi</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066327</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 10–35 cm, eglandular;</text>
    </statement>
    <statement id="d0_s1">
      <text>fibrous-rooted with slender, branching rhizomes.</text>
      <biological_entity id="o17028" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="eglandular" value_original="eglandular" />
        <character constraint="with rhizomes" constraintid="o17029" is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o17029" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves (basal often persistent at rhizome tips) not densely overlapping;</text>
      <biological_entity id="o17030" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not densely" name="arrangement" src="d0_s2" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades spatulate to spatulate-obovate, 15–30 (–40) × 2–5 mm, gradually or little reduced distally, herbaceous, bases attenuate, not clasping, flat (2-grooved adaxially), faces hispido-pilose to strigose or glabrate.</text>
      <biological_entity id="o17031" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="spatulate-obovate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s3" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o17032" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="not" name="architecture_or_fixation" src="d0_s3" value="clasping" value_original="clasping" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o17033" name="face" name_original="faces" src="d0_s3" type="structure">
        <character char_type="range_value" from="hispido-pilose" name="pubescence" src="d0_s3" to="strigose or glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres cylindro-turbinate, 4–6 × 2–4 mm.</text>
      <biological_entity id="o17034" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="cylindro-turbinate" value_original="cylindro-turbinate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 7–9;</text>
      <biological_entity id="o17035" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>corollas white.</text>
      <biological_entity id="o17036" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Disc-florets 10–15, bisexual.</text>
      <biological_entity id="o17037" name="disc-floret" name_original="disc-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="15" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cypselae 1.9–2 mm, 3–5-nerved, faces glabrate to sparsely strigose;</text>
      <biological_entity id="o17038" name="cypsela" name_original="cypselae" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-5-nerved" value_original="3-5-nerved" />
      </biological_entity>
      <biological_entity id="o17039" name="face" name_original="faces" src="d0_s8" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s8" to="sparsely strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pappi hyaline crowns of basally fused scales 0.3–0.8 mm in 1 series.</text>
      <biological_entity id="o17040" name="pappus" name_original="pappi" src="d0_s9" type="structure" />
      <biological_entity id="o17042" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="basally" name="fusion" src="d0_s9" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o17043" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <relation from="o17041" id="r1579" name="part_of" negation="false" src="d0_s9" to="o17042" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 16.</text>
      <biological_entity id="o17041" name="crown" name_original="crowns" src="d0_s9" type="structure" constraint="scale" constraint_original="scale; scale">
        <character is_modifier="true" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" constraint="in series" constraintid="o17043" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17044" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep(–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
        <character name="flowering time" char_type="atypical_range" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes, talus, crevices, chaparral, oak, oak-juniper, or oak-pine-fir woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="crevices" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="oak-juniper" />
        <character name="habitat" value="oak-pine-fir woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León, San Luis Potosí, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Parry’s lazy daisy</other_name>
  
</bio:treatment>