<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">571</other_info_on_meta>
    <other_info_on_meta type="mention_page">575</other_info_on_meta>
    <other_info_on_meta type="treatment_page">589</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">packera</taxon_name>
    <taxon_name authority="(Greenman) Á. Löve &amp; D. Löve" date="1976" rank="species">hyperborealis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Not.</publication_title>
      <place_in_publication>128: 520. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus packera;species hyperborealis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067252</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Greenman" date="unknown" rank="species">hyperborealis</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>1: 264. 1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species hyperborealis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 6–20+ cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>fibrous-rooted or rhizomatous (rhizomes horizontal to suberect, stout).</text>
      <biological_entity id="o30267" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="20" to_unit="cm" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually 1, sometimes multiple, bases and leaf-axils tomentose, otherwise glabrous.</text>
      <biological_entity id="o30268" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" value="1" value_original="1" />
        <character is_modifier="false" modifier="sometimes" name="quantity" src="d0_s2" value="multiple" value_original="multiple" />
      </biological_entity>
      <biological_entity id="o30269" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o30270" name="leaf-axil" name_original="leaf-axils" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (and proximal cauline) petiolate;</text>
      <biological_entity constraint="basal" id="o30271" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades oblanceolate to obovate or lyrate (pinnately lobed to pinnatifid, terminal lobes larger than laterals), 10–30+ × 10–20+ mm, bases tapering, ultimate margins ± crenate to serrate.</text>
      <biological_entity id="o30272" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate or lyrate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o30273" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o30274" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="less crenate" name="shape" src="d0_s4" to="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves gradually to abruptly reduced (sessile; pinnatisect or entire).</text>
      <biological_entity constraint="cauline" id="o30275" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="gradually to abruptly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–2 (–5+) in cymiform arrays.</text>
      <biological_entity id="o30276" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="5" upper_restricted="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o30277" from="1" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
      <biological_entity id="o30277" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles bracteate (bractlets purple-tinged), glabrous or glabrate.</text>
      <biological_entity id="o30278" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi 0 or inconspicuous (bractlets purple-tinged).</text>
      <biological_entity id="o30279" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 13, green (sometimes purple-tinged distally), 6–8 mm, glabrous.</text>
      <biological_entity id="o30280" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="13" value_original="13" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0 (rarely) or 10–12;</text>
      <biological_entity id="o30281" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corolla laminae 8–10 mm.</text>
      <biological_entity constraint="corolla" id="o30282" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 45–60+;</text>
      <biological_entity id="o30283" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="45" name="quantity" src="d0_s12" to="60" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corolla-tubes 3.5–4 mm, limbs 3–3.5 mm.</text>
      <biological_entity id="o30284" name="corolla-tube" name_original="corolla-tubes" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="distance" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30285" name="limb" name_original="limbs" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae 1.5–2.5 mm, hirtellous on ribs;</text>
      <biological_entity id="o30286" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character constraint="on ribs" constraintid="o30287" is_modifier="false" name="pubescence" src="d0_s14" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity id="o30287" name="rib" name_original="ribs" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>pappi 6–7 mm. 2n = 46.</text>
      <biological_entity id="o30288" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30289" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Jun–early Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Aug" from="mid Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, dry, rocky or sandy areas, sometimes in fertile soils overlaying limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" />
        <character name="habitat" value="dry" />
        <character name="habitat" value="rocky" />
        <character name="habitat" value="sandy areas" />
        <character name="habitat" value="fertile soils" modifier="sometimes" />
        <character name="habitat" value="limestone" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Nunavut, Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <other_name type="common_name">Northern groundsel</other_name>
  <discussion>Packera hyperborealis is known only from lowland tundra from Alaska to the Mackenzie River and as far south as the northern end of the Franklin Mountains. It has also been collected as far north as Banks Island.</discussion>
  
</bio:treatment>