<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="mention_page">327</other_info_on_meta>
    <other_info_on_meta type="mention_page">328</other_info_on_meta>
    <other_info_on_meta type="treatment_page">326</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="(A. Gray) A. M. Powell" date="1968" rank="section">laphamia</taxon_name>
    <taxon_name authority="(A. Gray) J. F. Macbride" date="unknown" rank="species">stansburyi</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>56: 39. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section laphamia;species stansburyi</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067335</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Laphamia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">stansburii</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 101. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Laphamia;species stansburii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 7–45 cm (often dense clumps to 60 cm across);</text>
    </statement>
    <statement id="d0_s1">
      <text>hirtellous.</text>
      <biological_entity id="o22287" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirtellous" value_original="hirtellous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o22288" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirtellous" value_original="hirtellous" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 3–10 mm;</text>
      <biological_entity id="o22289" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o22290" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades subdeltate, suborbiculate, or subovate, 3–14 × 3–15 mm, margins usually 2–5-lobed or serrate, sometimes subentire or 3-lobed.</text>
      <biological_entity id="o22291" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o22292" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subdeltate" value_original="subdeltate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subovate" value_original="subovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subovate" value_original="subovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22293" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="2-5-lobed" value_original="2-5-lobed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or in corymbiform arrays, 7–8 × 5–9 mm.</text>
      <biological_entity id="o22294" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="in corymbiform arrays" value_original="in corymbiform arrays" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" notes="" src="d0_s4" to="8" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22295" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o22294" id="r1533" name="in" negation="false" src="d0_s4" to="o22295" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 5–60 mm.</text>
      <biological_entity id="o22296" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="60" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate.</text>
      <biological_entity id="o22297" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 14–22, lanceolate to broadly oblanceolate, 5–6 × 1–2 mm.</text>
      <biological_entity id="o22298" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s7" to="22" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="broadly oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 6–14;</text>
      <biological_entity id="o22299" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="14" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow, laminae 3–6 × 1.2–3 mm.</text>
      <biological_entity id="o22300" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o22301" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 60–80;</text>
      <biological_entity id="o22302" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s10" to="80" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, tubes 1.2–1.5 mm, throats tubular to subfunnelform, 2.4–3 mm, lobes 0.4–0.6 mm.</text>
      <biological_entity id="o22303" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o22304" name="tube" name_original="tubes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22305" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character char_type="range_value" from="tubular" name="shape" src="d0_s11" to="subfunnelform" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22306" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae narrowly oblanceolate, 2–3.5 mm, margins thin-calloused, short-hairy;</text>
      <biological_entity id="o22307" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22308" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="thin-calloused" value_original="thin-calloused" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of single, ± stout bristles 2.5–4 mm plus crowns of vestigial, hyaline scales.</text>
      <biological_entity id="o22309" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity id="o22310" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="single" value_original="single" />
        <character is_modifier="true" modifier="more or less" name="fragility_or_size" src="d0_s13" value="stout" value_original="stout" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22312" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s13" value="vestigial" value_original="vestigial" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <relation from="o22309" id="r1534" name="consist_of" negation="false" src="d0_s13" to="o22310" />
      <relation from="o22311" id="r1535" name="part_of" negation="false" src="d0_s13" to="o22312" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 34.</text>
      <biological_entity id="o22311" name="crown" name_original="crowns" src="d0_s13" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity constraint="2n" id="o22313" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  <other_name type="common_name">Stansbury’s rock daisy</other_name>
  <discussion>Perityle stansburii with its large habit, relatively broad leaves, radiate heads, bristle pappus, chromosome number, and wide distribution, fills most expectations as the ancestral taxon of the group of related species called the “southwestern alliance.” This natural assemblage of taxa, which is thought to have evolved through geographic displacement and subsequent genetic differentiation, includes P. congesta, P. gracilis, P. intricata, P. inyoensis, P. megalocephala, P. specuicola, P. tenella, and P. villosa. These taxa, which are found mostly to the south and west of P. stansburii, all have rayless heads and may or may not have pappus bristles. In western and northwestern Utah and adjacent Nevada, P. stansburii occurs in crevices of rock exposures.</discussion>
  
</bio:treatment>