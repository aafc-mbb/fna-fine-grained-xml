<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">546</other_info_on_meta>
    <other_info_on_meta type="mention_page">551</other_info_on_meta>
    <other_info_on_meta type="treatment_page">553</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">senecio</taxon_name>
    <taxon_name authority="S. F. Blake" date="1957" rank="species">neowebsteri</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>8: 143. 1957</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus senecio;species neowebsteri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067497</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Senecio</taxon_name>
    <taxon_name authority="Greenman" date="unknown" rank="species">websteri</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>53: 511. 1912,</place_in_publication>
      <other_info_on_pub>not Hooker f. 1846</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Senecio;species websteri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 7–15 (–20+) cm (rhizomes fibrous-rooted).</text>
      <biological_entity id="o11898" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="20" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Herbage (sometimes purplish-tinged) floccose-tomentose, unevenly glabrescent.</text>
      <biological_entity id="o11899" name="herbage" name_original="herbage" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="floccose-tomentose" value_original="floccose-tomentose" />
        <character is_modifier="false" modifier="unevenly" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems single or loosely clustered (erect or arching).</text>
      <biological_entity id="o11900" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="single" value_original="single" />
        <character is_modifier="false" modifier="loosely" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o11902" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiolate (petioles about equaling blades);</text>
      <biological_entity id="o11901" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades lanceolate or oblanceolate to ovate, (2–) 4–8+ × 1.5–3 cm, bases tapered, margins denticulate (distal leaves smaller, lanceolate or linear-lanceolate, bractlike).</text>
      <biological_entity id="o11903" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s5" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="8" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11904" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o11905" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads nodding, 1 (–2).</text>
      <biological_entity id="o11906" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="nodding" value_original="nodding" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="2" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi of 4–8 lanceolate to lance-linear bractlets (lengths mostly less than 1/2 phyllaries).</text>
      <biological_entity id="o11907" name="calyculus" name_original="calyculi" src="d0_s7" type="structure" />
      <biological_entity id="o11908" name="bractlet" name_original="bractlets" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="8" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s7" to="lance-linear" />
      </biological_entity>
      <relation from="o11907" id="r1086" name="consist_of" negation="false" src="d0_s7" to="o11908" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries usually ± 21, sometimes ± 13, (8–) 10–15 mm, tips usually greenish (often sparsely hairy).</text>
      <biological_entity id="o11909" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="21" value_original="21" />
        <character modifier="sometimes more or less; more or less" name="quantity" src="d0_s8" value="13" value_original="13" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11910" name="tip" name_original="tips" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets ± 13;</text>
      <biological_entity id="o11911" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="13" value_original="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla laminae ± 15 mm.</text>
      <biological_entity constraint="corolla" id="o11912" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="15" value_original="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae glabrous.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 40.</text>
      <biological_entity id="o11913" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11914" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–early fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>High talus slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="high talus slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>