<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="treatment_page">232</other_info_on_meta>
    <other_info_on_meta type="illustration_page">227</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="A. Gray in J. C. Frémont" date="1845" rank="genus">nicolletia</taxon_name>
    <taxon_name authority="A. Gray" date="1852" rank="species">edwardsii</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 119. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus nicolletia;species edwardsii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250067203</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 3–25 cm.</text>
      <biological_entity id="o21417" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading.</text>
      <biological_entity id="o21419" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 10–60 mm overall, lobes 3–5, linear to filiform, rachis widths hardly greater than lobe widths.</text>
      <biological_entity id="o21420" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21421" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="5" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="filiform" />
      </biological_entity>
      <biological_entity id="o21422" name="rachis" name_original="rachis" src="d0_s2" type="structure">
        <character constraint="than lobe" constraintid="o21423" is_modifier="false" name="widths" src="d0_s2" value="hardly greater" value_original="hardly greater" />
      </biological_entity>
      <biological_entity id="o21423" name="lobe" name_original="lobe" src="d0_s2" type="structure">
        <character is_modifier="false" name="widths" src="d0_s2" value="widths" value_original="widths" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 5–30 mm.</text>
      <biological_entity id="o21424" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi of 4–6 bractlets 1–2 mm.</text>
      <biological_entity id="o21425" name="calyculus" name_original="calyculi" src="d0_s4" type="structure" />
      <biological_entity id="o21426" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o21425" id="r1476" name="consist_of" negation="false" src="d0_s4" to="o21426" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres turbinate to fusiform, 11–15 mm.</text>
      <biological_entity id="o21427" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="turbinate" name="shape" src="d0_s5" to="fusiform" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 6–8, lanceolate.</text>
      <biological_entity id="o21428" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s6" to="8" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 7–9, laminae 7–16 × 3–6 mm.</text>
      <biological_entity id="o21429" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s7" to="9" />
      </biological_entity>
      <biological_entity id="o21430" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="16" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 15–25 (–50);</text>
      <biological_entity id="o21431" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="50" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s8" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow, 7–8 mm.</text>
      <biological_entity id="o21432" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 5–7 mm;</text>
      <biological_entity id="o21433" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: bristles 3–5 mm, scales 5–7 mm. 2n = 20.</text>
      <biological_entity id="o21434" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o21435" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21436" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21437" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mostly fall, following rains.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="mostly; , following rains" to="fall" from="fall" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, often calcareous or gypseous soils of fans, flats, or playas in desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" constraint="of fans , flats , or playas in desert scrub" />
        <character name="habitat" value="calcareous" modifier="often" constraint="of fans , flats , or playas in desert scrub" />
        <character name="habitat" value="gypseous soils" constraint="of fans , flats , or playas in desert scrub" />
        <character name="habitat" value="fans" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="playas" constraint="in desert scrub" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila, San Luis Potosí, Zacatecas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Zacatecas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  
</bio:treatment>