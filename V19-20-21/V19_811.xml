<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="mention_page">483</other_info_on_meta>
    <other_info_on_meta type="treatment_page">482</other_info_on_meta>
    <other_info_on_meta type="illustration_page">482</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="(Cassini ex Dumortier) Anderberg" date="1989" rank="tribe">plucheeae</taxon_name>
    <taxon_name authority="Cassini" date="1817" rank="genus">pluchea</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1836" rank="species">foetida</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 452. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe plucheeae;genus pluchea;species foetida</taxon_hierarchy>
    <other_info_on_name type="fna_id">242417011</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Baccharis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">foetida</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 861. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Baccharis;species foetida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pluchea</taxon_name>
    <taxon_name authority="Urban" date="unknown" rank="species">eggersii</taxon_name>
    <taxon_hierarchy>genus Pluchea;species eggersii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pluchea</taxon_name>
    <taxon_name authority="(Kearney) Nash" date="unknown" rank="species">foetida</taxon_name>
    <taxon_name authority="Kearney" date="unknown" rank="variety">imbricata</taxon_name>
    <taxon_hierarchy>genus Pluchea;species foetida;variety imbricata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pluchea</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">imbricata</taxon_name>
    <taxon_hierarchy>genus Pluchea;species imbricata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pluchea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tenuifolia</taxon_name>
    <taxon_hierarchy>genus Pluchea;species tenuifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials, 40–100 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>fibrous-rooted, sometimes rhizomatous.</text>
      <biological_entity id="o5684" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems (often dark purplish) arachnose, glandular.</text>
      <biological_entity id="o5686" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="arachnose" value_original="arachnose" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves sessile;</text>
      <biological_entity id="o5687" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (thick, reticulate-veined) oblong to elliptic, lanceovate, or ovate, mostly 3–10 (–13) × 1–4 cm (bases clasping), margins denticulate (apices rounded to acute), faces minutely sessile-glandular.</text>
      <biological_entity id="o5688" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="elliptic lanceovate or ovate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="elliptic lanceovate or ovate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="elliptic lanceovate or ovate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="13" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5689" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o5690" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s4" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in loose to dense, corymbiform arrays.</text>
      <biological_entity id="o5691" name="head" name_original="heads" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres usually cupulate to campanulate, sometimes turbinate-campanulate, 5–10 × 6–9 (–12) mm (bases mostly rounded to impressed, sometimes obtuse).</text>
      <biological_entity id="o5692" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="usually cupulate" name="shape" src="d0_s6" to="campanulate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries usually creamy white, sometimes cream, greenish, pinkish, rose-purplish, purplish, yellowish, or pale-pink, thinly arachnoid-pubescent and sessile-glandular (the outer ovate to ovatelanceolate, lengths mostly 0.2–0.6 times inner).</text>
      <biological_entity id="o5693" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s7" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="rose-purplish" value_original="rose-purplish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale-pink" value_original="pale-pink" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="pale-pink" value_original="pale-pink" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s7" value="arachnoid-pubescent" value_original="arachnoid-pubescent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="sessile-glandular" value_original="sessile-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas creamy white to yellowish or pale-pink.</text>
      <biological_entity id="o5694" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="creamy white" name="coloration" src="d0_s8" to="yellowish or pale-pink" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pappi persistent, bristles distinct.</text>
      <biological_entity id="o5695" name="pappus" name_original="pappi" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o5696" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Late Jul–Oct (year-round in south).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Seasonally wet soil, pond and lake edges, ditches, borrow pits, swampy woods, bogs, other freshwater wetlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet soil" modifier="seasonally" />
        <character name="habitat" value="pond" />
        <character name="habitat" value="lake edges" />
        <character name="habitat" value="pits" modifier="ditches borrow" />
        <character name="habitat" value="swampy woods" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="other freshwater wetlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Fla., Ga., La., Md., Miss., Mo., N.J., N.C., Okla., S.C., Tex., Va.; Mexico; West Indies (Hispaniola).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies (Hispaniola)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Stinking camphorweed</other_name>
  <discussion>Pluchea foetida var. imbricata has not been treated as distinct from typical P. foetida by recent authors (e.g., A. Cronquist 1980; R. K. Godfrey and J. W. Wooten 1981; R. P. Wunderlin et al. 1996). Although plants similar to the type can be found scattered in Florida and Georgia, a populational integrity does not appear to occur, and intermediate forms exist. Nevertheless, field biologists should be aware of the putative distinctions of var. imbricata to make more critical observations regarding its status.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distalmost leaves triangular-ovate; heads in tight, rounded clusters at ends of branches; involucres turbinate-campanulate, 9–10 mm; phyllaries pink- ish to rose-purplish</description>
      <determination>6 Pluchea foetida var. imbricata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Distalmost leaves mostly oblong-elliptic; heads in paniculiform arrays of usually flat-topped cymiform clusters; involucres broadly campanulate, 5–8 mm; phyllaries cream to greenish</description>
      <determination>6 Pluchea foetida var. foetida</determination>
    </key_statement>
  </key>
</bio:treatment>