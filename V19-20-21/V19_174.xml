<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">165</other_info_on_meta>
    <other_info_on_meta type="treatment_page">166</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="de Candolle" date="1810" rank="genus">saussurea</taxon_name>
    <taxon_name authority="Trautvetter &amp; C. A. Meyer in A. T. von Middendorff" date="1856" rank="species">triangulata</taxon_name>
    <place_of_publication>
      <publication_title>in A. T. von Middendorff, Reise Siber.</publication_title>
      <place_in_publication>1(2,3): 58. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus saussurea;species triangulata</taxon_hierarchy>
    <other_info_on_name type="fna_id">242347001</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–70 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>from rhizomes.</text>
      <biological_entity id="o13312" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems leafy, simple or with ascending branches.</text>
      <biological_entity id="o13313" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="with ascending branches" value_original="with ascending branches" />
      </biological_entity>
      <biological_entity id="o13314" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
      <relation from="o13313" id="r1215" name="with" negation="false" src="d0_s2" to="o13314" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, well distributed or distally much smaller, proximal and mid with winged petioles to 15 cm, not decurrent on stems, proximal petioles ciliate with multicellular trichomes, distal petioles loosely tomentose when young, glabrescent;</text>
      <biological_entity id="o13315" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s3" value="distributed" value_original="distributed" />
        <character is_modifier="false" modifier="distally much; distally much" name="size" src="d0_s3" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character constraint="with petioles" constraintid="o13316" is_modifier="false" name="position" src="d0_s3" value="mid" value_original="mid" />
        <character constraint="on stems" constraintid="o13317" is_modifier="false" modifier="not" name="shape" notes="" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <biological_entity id="o13316" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="winged" value_original="winged" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13317" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o13318" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character constraint="with trichomes" constraintid="o13319" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o13319" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <biological_entity constraint="distal" id="o13320" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="when young" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades lanceolate to triangular-ovate, 6–13 cm, bases truncate or subcordate to obtuse or subacute, margins coarsely laciniate-dentate, ciliate with multicellular trichomes, apices acute or acuminate, faces glabrous;</text>
      <biological_entity id="o13321" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="triangular-ovate" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s4" to="13" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13322" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="subcordate" name="shape" src="d0_s4" to="obtuse or subacute" />
      </biological_entity>
      <biological_entity id="o13323" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="laciniate-dentate" value_original="laciniate-dentate" />
        <character constraint="with trichomes" constraintid="o13324" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o13324" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <biological_entity id="o13325" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>mid and distal progressively smaller, becoming sessile, narrower, bases obtuse to acuminate, cauline usually 15 or fewer.</text>
      <biological_entity id="o13326" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="position" src="d0_s5" value="mid" value_original="mid" />
      </biological_entity>
      <biological_entity id="o13327" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character is_modifier="false" modifier="progressively" name="size" src="d0_s5" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="becoming" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="width" src="d0_s5" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o13328" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="acuminate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o13329" name="base" name_original="bases" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or fewer" value="15" value_original="15" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 4–11, in congested corymbiform to capitate terminal arrays (peduncles 0–4 cm).</text>
      <biological_entity id="o13330" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="11" />
        <character constraint="corymbiform to capitate terminal arrays" constraintid="o13331" is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="congested" value_original="congested" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o13331" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform to capitate" value_original="corymbiform to capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 10–12 mm.</text>
      <biological_entity id="o13332" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 4–5 series, unequal, appressed throughout, the outer ovate, inner narrowly lanceolate, abaxially dark purplish to nearly black, with or without central green or tan patch, margins loosely arachnoid-tomentose, apices acute to obtuse or rounded, faces glabrate.</text>
      <biological_entity id="o13333" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s8" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="throughout" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o13334" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity constraint="outer" id="o13335" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o13336" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="abaxially dark purplish" name="coloration" src="d0_s8" to="nearly black" />
        <character constraint="with or without central" is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="tan patch" value_original="tan patch" />
      </biological_entity>
      <biological_entity id="o13337" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s8" value="arachnoid-tomentose" value_original="arachnoid-tomentose" />
      </biological_entity>
      <biological_entity id="o13338" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse or rounded" />
      </biological_entity>
      <biological_entity id="o13339" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <relation from="o13333" id="r1216" name="in" negation="false" src="d0_s8" to="o13334" />
    </statement>
    <statement id="d0_s9">
      <text>Receptacles scaly.</text>
      <biological_entity id="o13340" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s9" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 10–17;</text>
      <biological_entity id="o13341" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas light purple, goblet-shaped, 9–11 mm;</text>
      <biological_entity id="o13342" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="light purple" value_original="light purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="goblet--shaped" value_original="goblet--shaped" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s11" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>tubes 4–5.5 mm, throat 2 mm, lobes 3–3.5 mm;</text>
      <biological_entity id="o13343" name="tube" name_original="tubes" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13344" name="throat" name_original="throat" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o13345" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers dark purple, 4–5 mm;</text>
      <biological_entity id="o13346" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark purple" value_original="dark purple" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style-branches recurved, 1–1.5 mm.</text>
      <biological_entity id="o13347" name="branch-style" name_original="style-branches" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae ca. 3 mm;</text>
      <biological_entity id="o13348" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi brownish, outer bristles very slender, 0.5–4 mm, inner 14–17 bristles stouter, 8–9 mm, all plumose.</text>
      <biological_entity id="o13349" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="outer" id="o13350" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="very" name="size" src="d0_s16" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="inner" id="o13351" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s16" to="17" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>2n = 26 (Russia).</text>
      <biological_entity id="o13352" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s16" value="stouter" value_original="stouter" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s16" to="9" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="plumose" value_original="plumose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13353" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Slopes, ridges, sometimes in shade</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" />
        <character name="habitat" value="ridges" />
        <character name="habitat" value="shade" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; Russian Far East (Chukotka).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Russian Far East (Chukotka)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>In North America Saussurea triangulata is known only from the Waring Mountains of northwestern Alaska, where it was discovered in the late 1990s. I provisionally recognize the Waring Mountains plants as a disjunct population of S. triangulata pending examination of additional material from both Alaskan and Siberian populations.</discussion>
  
</bio:treatment>