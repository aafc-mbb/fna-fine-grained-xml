<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">422</other_info_on_meta>
    <other_info_on_meta type="mention_page">425</other_info_on_meta>
    <other_info_on_meta type="treatment_page">424</other_info_on_meta>
    <other_info_on_meta type="illustration_page">421</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">gaillardiinae</taxon_name>
    <taxon_name authority="Fougeroux" date="1788" rank="genus">gaillardia</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">aristata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 573. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe gaillardiinae;genus gaillardia;species aristata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">200023952</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials (sometimes flowering first-year), 20–80 cm.</text>
      <biological_entity id="o15157" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline or cauline;</text>
      <biological_entity id="o15158" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiolar bases 5–15 cm;</text>
      <biological_entity constraint="petiolar" id="o15159" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblanceolate to lanceolate, 5–15 cm × 5–30 (–40) mm, margins raggedly pinnately lobed to toothed or entire, faces scabrellous and/or sparsely to densely villous (hairs jointed).</text>
      <biological_entity id="o15160" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15161" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="raggedly pinnately lobed" name="shape" src="d0_s3" to="toothed or entire" />
      </biological_entity>
      <biological_entity id="o15162" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Peduncles (5–) 20–35+ cm.</text>
      <biological_entity id="o15163" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s4" to="35" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 24–40+ ovate to lance-attenuate, 10–15+ mm, ciliate with jointed hairs (also strigose and glanddotted).</text>
      <biological_entity id="o15164" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="24" name="quantity" src="d0_s5" to="40" upper_restricted="false" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="lance-attenuate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" upper_restricted="false" />
        <character constraint="with hairs" constraintid="o15165" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15165" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="jointed" value_original="jointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Receptacular setae 2–6 mm.</text>
      <biological_entity constraint="receptacular" id="o15166" name="seta" name_original="setae" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (6–) 12–18+;</text>
      <biological_entity id="o15167" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" name="atypical_quantity" src="d0_s7" to="12" to_inclusive="false" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s7" to="18" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas yellow or yellow/purple, rarely tubular and 5-lobed, usually distally laminate and 3-lobed, 15–35+ mm.</text>
      <biological_entity id="o15168" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="tubular" value_original="tubular" />
        <character is_modifier="false" name="shape" src="d0_s8" value="5-lobed" value_original="5-lobed" />
        <character is_modifier="false" modifier="usually distally" name="architecture" src="d0_s8" value="laminate" value_original="laminate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="3-lobed" value_original="3-lobed" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="35" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 60–120+;</text>
      <biological_entity id="o15169" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s9" to="120" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas usually purple or purple-tipped, sometimes yellow, tubes 0.5–1.5 mm, throats cylindric to urceolate, 4.5–5.5 mm, lobes lanceovate to triangular-attenuate, 1–2 mm, jointed hairs 0.3+ mm.</text>
      <biological_entity id="o15170" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="purple-tipped" value_original="purple-tipped" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o15171" name="tube" name_original="tubes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15172" name="throat" name_original="throats" src="d0_s10" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s10" to="urceolate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15173" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s10" to="triangular-attenuate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15174" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="jointed" value_original="jointed" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae clavate (outer) to obpyramidal (inner), 2.5–6 mm, hairs 1.5–2.5 mm, inserted at bases;</text>
      <biological_entity id="o15175" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="clavate" name="shape" src="d0_s11" to="obpyramidal" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15176" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15177" name="base" name_original="bases" src="d0_s11" type="structure" />
      <relation from="o15176" id="r1041" name="inserted at" negation="false" src="d0_s11" to="o15177" />
    </statement>
    <statement id="d0_s12">
      <text>pappi of 8 ovate to lanceolate, aristate scales 5–6 mm (scarious bases 1.5–3 × 0.4–1.5 mm).</text>
      <biological_entity id="o15179" name="scale" name_original="scales" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="8" value_original="8" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s12" to="lanceolate" />
        <character is_modifier="true" name="shape" src="d0_s12" value="aristate" value_original="aristate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <relation from="o15178" id="r1042" name="consist_of" negation="false" src="d0_s12" to="o15179" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 34, 68.</text>
      <biological_entity id="o15178" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o15180" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="34" value_original="34" />
        <character name="quantity" src="d0_s13" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open places, usually among aspens or pines, or with sagebrush, often dry, sandy benches or bars</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="places" modifier="open" />
        <character name="habitat" value="aspens" modifier="usually among" />
        <character name="habitat" value="pines" />
        <character name="habitat" value="sagebrush" modifier="or with" />
        <character name="habitat" value="dry" modifier="often" />
        <character name="habitat" value="sandy benches" />
        <character name="habitat" value="bars" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Sask., Yukon; Colo., Conn., Idaho, Mass., Minn., Mont., N.H., N.Dak., Oreg., S.Dak., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="past_name">Galardia</other_name>
  
</bio:treatment>