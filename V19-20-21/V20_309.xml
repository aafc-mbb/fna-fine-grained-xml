<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">144</other_info_on_meta>
    <other_info_on_meta type="mention_page">145</other_info_on_meta>
    <other_info_on_meta type="treatment_page">146</other_info_on_meta>
    <other_info_on_meta type="illustration_page">139</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(G. Don) G. L. Nesom" date="1993" rank="subsection">Venosae</taxon_name>
    <taxon_name authority="unknown" date="1830" rank="series">venosae</taxon_name>
    <taxon_name authority="Miller" date="1768" rank="species">rugosa</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8, Solidago no. 25. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection venosae;series venosae;species rugosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417298</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="(Miller) Kuntze" date="unknown" rank="species">rugosus</taxon_name>
    <taxon_hierarchy>genus Aster;species rugosus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–200 cm;</text>
      <biological_entity id="o26621" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes long-creeping, forming clones.</text>
      <biological_entity id="o26622" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="long-creeping" value_original="long-creeping" />
      </biological_entity>
      <biological_entity id="o26623" name="clone" name_original="clones" src="d0_s1" type="structure" />
      <relation from="o26622" id="r2458" name="forming" negation="false" src="d0_s1" to="o26623" />
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–50+, erect, glabrous or densely hispid to strigose.</text>
      <biological_entity id="o26624" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="50" upper_restricted="false" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="densely hispid" name="pubescence" src="d0_s2" to="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal withering by flowering;</text>
      <biological_entity id="o26625" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o26626" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="by flowering" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal cauline usually withering by flowering, sessile, blades elliptic to lanceolate, 68–104 × 20–25 mm, margins sharply serrate, apices acute to attenuate, abaxial faces usually hispido-strigose (at least on main nerves), nerves sometimes prominent, abaxial glabrate;</text>
      <biological_entity id="o26627" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal cauline" id="o26628" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="by blades, margins, apices, faces, nerves" constraintid="o26629, o26630, o26631, o26632, o26633" is_modifier="false" modifier="usually" name="life_cycle" src="d0_s4" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o26629" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="68" from_unit="mm" name="length" src="d0_s4" to="104" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o26630" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="68" from_unit="mm" name="length" src="d0_s4" to="104" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o26631" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="68" from_unit="mm" name="length" src="d0_s4" to="104" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o26632" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="68" from_unit="mm" name="length" src="d0_s4" to="104" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o26633" name="nerve" name_original="nerves" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="68" from_unit="mm" name="length" src="d0_s4" to="104" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="attenuate" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="hispido-strigose" value_original="hispido-strigose" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o26634" name="blade" name_original="blades" src="d0_s4" type="structure" />
      <biological_entity constraint="abaxial" id="o26635" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>mid to distal cauline sessile, blades lanceolate, elliptic or ovate, (15–) 40–75 (–90) × (6–) 12–22 (–32) mm, largest at midstem, somewhat reduced to much reduced distally, margins coarsely to finely serrate, ciliate, indument similar to proximal or denser.</text>
      <biological_entity id="o26636" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o26637" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o26638" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s5" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="75" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="90" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s5" to="75" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_width" src="d0_s5" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="32" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s5" to="22" to_unit="mm" />
        <character constraint="at midstem" constraintid="o26639" is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
        <character is_modifier="false" name="size" src="d0_s5" value="reduced to much" value_original="reduced to much" />
        <character is_modifier="false" modifier="much; distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o26639" name="midstem" name_original="midstem" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size" notes="" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o26640" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="coarsely to finely" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o26641" name="indument" name_original="indument" src="d0_s5" type="structure">
        <character is_modifier="false" name="density" src="d0_s5" value="denser" value_original="denser" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o26642" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o26641" id="r2459" name="to" negation="false" src="d0_s5" to="o26642" />
    </statement>
    <statement id="d0_s6">
      <text>Heads 50–1500, secund, in secund-pyramidal paniculiform arrays 7–36 (–50) × 9–26 cm, compact to lax, branches divergent and recurved, longest 0.8–34 cm, leafy-bracteate.</text>
      <biological_entity id="o26643" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s6" to="1500" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="secund" value_original="secund" />
        <character char_type="range_value" from="36" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="50" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s6" to="36" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="width" src="d0_s6" to="26" to_unit="cm" />
        <character is_modifier="false" name="arrangement_or_architecture" src="d0_s6" value="compact to lax" value_original="compact to lax" />
      </biological_entity>
      <biological_entity id="o26644" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="in secund-pyramidal paniculiform arrays" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="length" src="d0_s6" value="longest" value_original="longest" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s6" to="34" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="leafy-bracteate" value_original="leafy-bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Peduncles 1–1.8 mm, sparsely to densely hispido-strigillose;</text>
      <biological_entity id="o26645" name="peduncle" name_original="peduncles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="hispido-strigillose" value_original="hispido-strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracteoles linear-lanceolate to ovate.</text>
      <biological_entity id="o26646" name="bracteole" name_original="bracteoles" src="d0_s8" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s8" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres narrowly campanulate, (2–) 2.5–3.5 (–4.5) mm.</text>
      <biological_entity id="o26647" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries in 3–4 series, strongly unequal, lanceolate to linear-lanceolate, acute to obtuse.</text>
      <biological_entity id="o26648" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="strongly" name="size" notes="" src="d0_s10" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="linear-lanceolate acute" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="linear-lanceolate acute" />
      </biological_entity>
      <biological_entity id="o26649" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s10" to="4" />
      </biological_entity>
      <relation from="o26648" id="r2460" name="in" negation="false" src="d0_s10" to="o26649" />
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets (4–) 6–8 (–12);</text>
      <biological_entity id="o26650" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s11" to="6" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="12" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s11" to="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae (0.9–) 1–1.6 (–2.3) × 0.4–0.7 mm.</text>
      <biological_entity id="o26651" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="atypical_length" src="d0_s12" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s12" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets (2–) 4–6 (–8);</text>
      <biological_entity id="o26652" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s13" to="4" to_inclusive="false" />
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="8" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s13" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 2–3.5 (–4.5) mm, lobes (0.5–) 0.7–1 (–1.3) mm.</text>
      <biological_entity id="o26653" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26654" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (narrowly obconic) 0.9–1.5 mm, moderately strigillose;</text>
      <biological_entity id="o26655" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="pubescence" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi 1.8–2.5 mm.</text>
      <biological_entity id="o26656" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Ky., La., Maine, Mass., Md., Mich., Miss., Mo., N.C., N.J., N.Y., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Va., Vt., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>52.</number>
  <other_name type="common_name">Rough-stemmed or wrinkle-leaf goldenrod</other_name>
  <other_name type="common_name">verge d’or rugueuse</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Solidago rugosa is highly variable in size, array shape, and hairiness. It is similar to members of the S. canadensis complex; it differs in not having 3-nerved leaves. The species is divided into two subspecies and five varieties that can be difficult to distinguish.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves relatively thin, not very rugose, usually sharply toothed, apices acuminate, glabrous or relatively soft-hairy; ray florets (4–)6–11(–13); northern (subsp. rugosa)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves relatively thick and firm, strongly rugose-nerved, usually blunt-toothed to subentire, apices often acute, relatively short and stiff hairy; ray florets 4–9; mostly se United States (subsp. aspera)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems and leaves hairy; e Canada and ne United States s to Virginia</description>
      <determination>52a Solidago rugosa var. rugosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems and leaves glabrous; coastal cedar bogs and swamps</description>
      <determination>52b Solidago rugosa var. sphagnophila</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Arrays narrow, proximal branches not much exceeding subtending leaves; herbage sparsely hairy; mid to higher elevations in mountains</description>
      <determination>52e Solidago rugosa var. cronquistiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Arrays wide, usually with elongate proximal branches greatly exceeding subtending leaves; herbage moderately to densely hairy; lower elevations in mountains, piedmont, and coastal plain</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Distal cauline leaves lanceolate to elliptic, not much reduced distally; much of range of subspecies</description>
      <determination>52c Solidago rugosa var. aspera</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Distal cauline leaves ovate, much reduced distally; outer coastal plain</description>
      <determination>52d Solidago rugosa var. celtidifolia</determination>
    </key_statement>
  </key>
</bio:treatment>