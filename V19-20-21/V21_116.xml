<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">54</other_info_on_meta>
    <other_info_on_meta type="treatment_page">55</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">rudbeckia</taxon_name>
    <taxon_name authority="Aiton" date="1789" rank="species">fulgida</taxon_name>
    <taxon_name authority="(C. L. Boynton &amp; Beadle) Cronquist" date="1945" rank="variety">umbrosa</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>47: 400. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section rudbeckia;species fulgida;variety umbrosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068690</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rudbeckia</taxon_name>
    <taxon_name authority="C. L. Boynton &amp; Beadle" date="unknown" rank="species">umbrosa</taxon_name>
    <place_of_publication>
      <publication_title>Biltmore Bot. Stud.</publication_title>
      <place_in_publication>1: 16. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Rudbeckia;species umbrosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rudbeckia</taxon_name>
    <taxon_name authority="C. L. Boynton &amp; Beadle" date="unknown" rank="species">chapmanii</taxon_name>
    <taxon_hierarchy>genus Rudbeckia;species chapmanii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems glabrous or sparsely hirsute (hairs antrorse to spreading).</text>
      <biological_entity id="o18218" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: basal blades ovate, 1.5–3.5 cm wide, lengths to 2 times widths, bases broadly rounded to cordate, margins coarsely dentate, faces glabrous or sparsely hairy;</text>
      <biological_entity id="o18219" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="basal" id="o18220" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s1" to="3.5" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s1" value="0-2" value_original="0-2" />
      </biological_entity>
      <biological_entity id="o18221" name="base" name_original="bases" src="d0_s1" type="structure">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s1" to="cordate" />
      </biological_entity>
      <biological_entity id="o18222" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o18223" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline petiolate (proximal) to nearly sessile (distal), ovate to lanceolate, not notably smaller distally, bases rounded to attenuate, margins usually coarsely serrate, sometimes entire, faces sparsely to moderately hairy.</text>
      <biological_entity id="o18224" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o18225" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="petiolate" name="architecture" src="d0_s2" to="nearly sessile" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
        <character is_modifier="false" modifier="not notably; distally" name="size" src="d0_s2" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o18226" name="base" name_original="bases" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="attenuate" />
      </biological_entity>
      <biological_entity id="o18227" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually coarsely" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18228" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Phyllaries reflexed, 1–2.2 × 0.25–0.5 cm, glabrous or sparsely hairy.</text>
      <biological_entity id="o18229" name="phyllary" name_original="phyllaries" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="2.2" to_unit="cm" />
        <character char_type="range_value" from="0.25" from_unit="cm" name="width" src="d0_s3" to="0.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Receptacles 10–15 mm diam.;</text>
      <biological_entity id="o18230" name="receptacle" name_original="receptacles" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>palea margins ciliate, faces glabrous.</text>
      <biological_entity constraint="palea" id="o18231" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o18232" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets 8–12;</text>
      <biological_entity id="o18233" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 10–30 mm. 2n = 76.</text>
      <biological_entity id="o18234" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18235" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="76" value_original="76" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet woodlands, bottoms</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet woodlands" />
        <character name="habitat" value="bottoms" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ga., Ind., Ky., Miss., Mo., N.C., Ohio, S.C., Tenn., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>15g.</number>
  <other_name type="common_name">Shady coneflower</other_name>
  
</bio:treatment>