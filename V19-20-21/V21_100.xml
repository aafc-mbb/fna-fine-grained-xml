<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">49</other_info_on_meta>
    <other_info_on_meta type="treatment_page">50</other_info_on_meta>
    <other_info_on_meta type="illustration_page">43</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">rudbeckiinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rudbeckia</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="1842" rank="section">macrocline</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">laciniata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">laciniata</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe rudbeckiinae;genus rudbeckia;section macrocline;species laciniata;variety laciniata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068701</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: basal 15–40 × 10–25 cm, blades pinnately compound to pinnatifid;</text>
      <biological_entity id="o18137" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o18138" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s0" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18139" name="blade" name_original="blades" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s0" value="compound" value_original="compound" />
        <character is_modifier="false" name="shape" src="d0_s0" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>proximal and mid cauline blades 5–9-lobed (distal cauline 3-lobed or not lobed);</text>
      <biological_entity id="o18140" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal and mid cauline" id="o18141" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="5-9-lobed" value_original="5-9-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>adaxial leaf faces glabrous or sparsely hairy.</text>
      <biological_entity id="o18142" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="leaf" id="o18143" name="face" name_original="faces" src="d0_s2" type="structure" constraint_original="adaxial leaf">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Receptacles globose to ovoid;</text>
      <biological_entity id="o18144" name="receptacle" name_original="receptacles" src="d0_s3" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s3" to="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>paleae 4.4–6.1 mm.</text>
      <biological_entity id="o18145" name="palea" name_original="paleae" src="d0_s4" type="structure">
        <character char_type="range_value" from="4.4" from_unit="mm" name="some_measurement" src="d0_s4" to="6.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ray laminae 20–45 × 7–18 mm.</text>
      <biological_entity constraint="ray" id="o18146" name="lamina" name_original="laminae" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s5" to="45" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Discs 15–20 × 10–20 mm.</text>
      <biological_entity id="o18147" name="disc" name_original="discs" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cypselae 4.2–6 mm;</text>
      <biological_entity id="o18148" name="cypsela" name_original="cypselae" src="d0_s7" type="structure">
        <character char_type="range_value" from="4.2" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pappi to 0.7 mm. 2n = 36, 54.</text>
      <biological_entity id="o18149" name="pappus" name_original="pappi" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18150" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="36" value_original="36" />
        <character name="quantity" src="d0_s8" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <number>7e.</number>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–fall</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet sites, along streams, edges of woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet sites" constraint="along streams , edges of woods" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="edges" constraint="of woods" />
        <character name="habitat" value="woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mich., Minn., Miss., Mo., Nebr., N.H., N.Y., N.C., N.Dak., Ohio, Okla., Pa., S.C., S.Dak., Tenn., Tex., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>