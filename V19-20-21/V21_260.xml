<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">verbesina</taxon_name>
    <taxon_name authority="(Chapman) A. Gray" date="1883" rank="species">heterophylla</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 12. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus verbesina;species heterophylla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067793</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Actinomeris</taxon_name>
    <taxon_name authority="Chapman" date="unknown" rank="species">heterophylla</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>3: 6. 1878</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Actinomeris;species heterophylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Verbesina</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">warei</taxon_name>
    <taxon_hierarchy>genus Verbesina;species warei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 40–50 (–80+) cm (perennating bases ± horizontal rhizomes, internodes winged).</text>
      <biological_entity id="o4825" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves all or mostly opposite (distal sometimes alternate);</text>
      <biological_entity id="o4826" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s1" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades ± ovate to elliptic or lanceolate, 3–7+ × 0.8–2.5+ cm, bases ± cuneate, margins toothed, apices obtuse, faces scabrellous.</text>
      <biological_entity id="o4827" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="less ovate" name="shape" src="d0_s2" to="elliptic or lanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="7" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s2" to="2.5" to_unit="cm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4828" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o4829" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o4830" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o4831" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="relief" src="d0_s2" value="scabrellous" value_original="scabrellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads borne singly or 3–9+ in loose, corymbiform arrays.</text>
      <biological_entity id="o4832" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o4833" from="3" name="quantity" src="d0_s3" to="9" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4833" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s3" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres hemispheric or broader, 12–15+ mm diam.</text>
      <biological_entity id="o4834" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s4" value="broader" value_original="broader" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s4" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 18–20+ in 2–3 series, ± erect, narrowly oblong to lanceolate, 4–6+ mm.</text>
      <biological_entity id="o4835" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o4836" from="18" name="quantity" src="d0_s5" to="20" upper_restricted="false" />
        <character is_modifier="false" modifier="more or less" name="orientation" notes="" src="d0_s5" value="erect" value_original="erect" />
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s5" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="6" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4836" name="series" name_original="series" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets (5–) 8;</text>
      <biological_entity id="o4837" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s6" to="8" to_inclusive="false" />
        <character name="quantity" src="d0_s6" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>laminae 12–15+ mm.</text>
      <biological_entity id="o4838" name="lamina" name_original="laminae" src="d0_s7" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Disc-florets 20–60+;</text>
      <biological_entity id="o4839" name="disc-floret" name_original="disc-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="60" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow.</text>
      <biological_entity id="o4840" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae purplish black, narrowly obovate to ± elliptic, 5 mm, faces glabrous;</text>
      <biological_entity id="o4841" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="purplish black" value_original="purplish black" />
        <character char_type="range_value" from="narrowly obovate" name="shape" src="d0_s10" to="more or less elliptic" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o4842" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi 0–0.3 mm. 2n = 34.</text>
      <biological_entity id="o4843" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4844" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pine barrens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="pine barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20+ m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20+" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  
</bio:treatment>