<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">373</other_info_on_meta>
    <other_info_on_meta type="treatment_page">370</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="D. Don" date="1829" rank="genus">lygodesmia</taxon_name>
    <taxon_name authority="(Nuttall) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="species">aphylla</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>7: 198. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus lygodesmia;species aphylla</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067131</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prenanthes</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">aphylla</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 123. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Prenanthes;species aphylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erythremia</taxon_name>
    <taxon_name authority="(Nuttall) Nuttall" date="unknown" rank="species">aphylla</taxon_name>
    <taxon_hierarchy>genus Erythremia;species aphylla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 30–80 cm solitary;</text>
      <biological_entity id="o20152" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s0" value="solitary" value_original="solitary" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots or rhizomes fleshy;</text>
      <biological_entity id="o20153" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o20154" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudices woody, branched.</text>
      <biological_entity id="o20155" name="caudex" name_original="caudices" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems 1, erect, green, simple or sparsely branched distally, strongly striate.</text>
      <biological_entity id="o20156" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="sparsely; distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="strongly" name="coloration_or_pubescence_or_relief" src="d0_s3" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves (basal in rosettes, sometimes withered at flowering);</text>
      <biological_entity id="o20157" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>proximal blades linear, 100–350 × 2–3 mm, margins entire;</text>
      <biological_entity constraint="proximal" id="o20158" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="100" from_unit="mm" name="length" src="d0_s5" to="350" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20159" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline similar or reduced to scales.</text>
      <biological_entity constraint="cauline" id="o20160" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character constraint="to scales" constraintid="o20161" is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o20161" name="scale" name_original="scales" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Heads (1–5) borne singly or in corymbiform arrays.</text>
      <biological_entity id="o20162" name="head" name_original="heads" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s7" to="5" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="in corymbiform arrays" value_original="in corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o20163" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o20162" id="r1824" name="in" negation="false" src="d0_s7" to="o20163" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres cylindric, 14–22 × 5–6 mm, apices narrow.</text>
      <biological_entity id="o20164" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s8" to="22" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20165" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyculi of 8–14, deltate bractlets 1–5 mm, margins scarious, erose-ciliate (faces tomentulose).</text>
      <biological_entity id="o20166" name="calyculus" name_original="calyculi" src="d0_s9" type="structure" />
      <biological_entity id="o20167" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s9" to="14" />
        <character is_modifier="true" name="shape" src="d0_s9" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20168" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="erose-ciliate" value_original="erose-ciliate" />
      </biological_entity>
      <relation from="o20166" id="r1825" name="consist_of" negation="false" src="d0_s9" to="o20167" />
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries 8, linear, 14–22 mm, margins scarious, apices often purplish, acute, appendaged.</text>
      <biological_entity id="o20169" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="8" value_original="8" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s10" value="linear" value_original="linear" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s10" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20170" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o20171" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="appendaged" value_original="appendaged" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Florets 8–10;</text>
      <biological_entity id="o20172" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas pink to lavender or blue, 30–40 mm, ligules 4–6 mm wide.</text>
      <biological_entity id="o20173" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s12" to="lavender or blue" />
        <character char_type="range_value" from="30" from_unit="mm" name="some_measurement" src="d0_s12" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20174" name="ligule" name_original="ligules" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae (subcylindric) 11–14 mm (faces smooth, abaxial weakly striate, glabrous, adaxial sulcate);</text>
      <biological_entity id="o20175" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s13" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 12–18 mm. 2n = 18.</text>
      <biological_entity id="o20176" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s14" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20177" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry sandy soils, flatwoods, pine barrens, disturbed areas, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry sandy soils" />
        <character name="habitat" value="flatwoods" />
        <character name="habitat" value="pine barrens" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Roserush</other_name>
  <discussion>Lygodesmia aphylla is recognized by its erect, sparingly branched habit, leafless stems, relatively large involucres and florets, apical appendages on phyllaries, and grooved cypselae. The habit is similar to that of L. texana. The basal leaves are frequently absent at flowering and are entire, not pinnately lobed as in L. texana.</discussion>
  
</bio:treatment>