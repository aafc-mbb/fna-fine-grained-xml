<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">321</other_info_on_meta>
    <other_info_on_meta type="treatment_page">323</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">peritylinae</taxon_name>
    <taxon_name authority="Bentham" date="1844" rank="genus">perityle</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">perityle</taxon_name>
    <taxon_name authority="J. M. Coulter" date="1890" rank="species">vaseyi</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>1: 42. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe peritylinae;genus perityle;section perityle;species vaseyi</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067338</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, (10–) 15–75 cm (in soil, tap roots fleshy, stems erect or spreading);</text>
    </statement>
    <statement id="d0_s1">
      <text>glandular-pubescent.</text>
      <biological_entity id="o17318" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o17319" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petioles 10–35 mm;</text>
      <biological_entity id="o17320" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o17321" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s2" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades usually palmately 3-lobed or ± cruciform, 6–35 × 10–50 mm, lobes usually again ternately dissected, cleft, or parted.</text>
      <biological_entity id="o17322" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17323" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually palmately" name="shape" src="d0_s3" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="cruciform" value_original="cruciform" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s3" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17324" name="lobe" name_original="lobes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="again ternately" name="shape" src="d0_s3" value="dissected" value_original="dissected" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="shape" src="d0_s3" value="parted" value_original="parted" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="shape" src="d0_s3" value="parted" value_original="parted" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads borne singly or (2–3) in corymbiform arrays, 8–10 × 10–13 mm.</text>
      <biological_entity id="o17325" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="singly" value_original="singly" />
        <character char_type="range_value" constraint="in arrays" constraintid="o17326" from="2" name="atypical_quantity" src="d0_s4" to="3" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" notes="" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" notes="" src="d0_s4" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17326" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 10–80 mm.</text>
      <biological_entity id="o17327" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="80" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate to hemispheric.</text>
      <biological_entity id="o17328" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s6" to="hemispheric" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries 20–30, oblanceolate or lanceolate to linear-lanceolate, 6–7 × 1–2 mm, apices usually acute, sometimes short-attenuate.</text>
      <biological_entity id="o17329" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s7" to="30" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="linear-lanceolate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17330" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s7" value="short-attenuate" value_original="short-attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 12–18;</text>
      <biological_entity id="o17331" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="18" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow, laminae oblong, 4–10 × 2–4 mm.</text>
      <biological_entity id="o17332" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o17333" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 50–100;</text>
      <biological_entity id="o17334" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s10" to="100" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, tubes 1.2–1.5 mm, throats tubular to broadly tubular, 2.2–3 mm, lobes 0.4–0.6 mm.</text>
      <biological_entity id="o17335" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o17336" name="tube" name_original="tubes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17337" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character char_type="range_value" from="tubular" name="shape" src="d0_s11" to="broadly tubular" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17338" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae linear-elliptic to narrowly oblanceolate, (2–) 3–4 mm, margins usually prominently calloused, sometimes thin, usually densely ciliate, sometimes short-hairy;</text>
      <biological_entity id="o17339" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="linear-elliptic" name="shape" src="d0_s12" to="narrowly oblanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17340" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually prominently" name="texture" src="d0_s12" value="calloused" value_original="calloused" />
        <character is_modifier="false" modifier="sometimes" name="width" src="d0_s12" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="usually densely" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s12" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi 0 or of 1 (–2) retrorsely or erectly barbellate (at least near tips) bristles 1–4 mm plus crowns of hyaline, laciniate scales.</text>
      <biological_entity id="o17341" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="2" />
        <character modifier="retrorsely" name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o17342" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="erectly" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17344" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
        <character is_modifier="true" name="shape" src="d0_s13" value="laciniate" value_original="laciniate" />
      </biological_entity>
      <relation from="o17343" id="r1192" name="part_of" negation="false" src="d0_s13" to="o17344" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 34.</text>
      <biological_entity id="o17343" name="crown" name_original="crowns" src="d0_s13" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity constraint="2n" id="o17345" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year around.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert soils, especially gypsiferous clays</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert soils" />
        <character name="habitat" value="gypsiferous clays" modifier="especially" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Chihuahua, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Vasey’s rock daisy</other_name>
  <discussion>Perityle vaseyi is always found growing in soil, often on roadsides and in road cuts in the southwestern Big Bend area. The more deeply dissected leaves, shorter pappus bristles with barbellate tips, and broader phyllaries help distinguish it from the soil-growing form of P. parryi.</discussion>
  
</bio:treatment>