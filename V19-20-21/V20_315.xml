<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">146</other_info_on_meta>
    <other_info_on_meta type="treatment_page">147</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(G. Don) G. L. Nesom" date="1993" rank="subsection">venosae</taxon_name>
    <taxon_name authority="unknown" date="1830" rank="series">venosae</taxon_name>
    <taxon_name authority="Miller" date="1768" rank="species">rugosa</taxon_name>
    <taxon_name authority="(Aiton) Cronquist" date="1947" rank="subspecies">aspera</taxon_name>
    <taxon_name authority="(Small) Fernald" date="1936" rank="variety">celtidifolia</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>38: 223. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection venosae;series venosae;species rugosa;subspecies rugosa;variety celtidifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068781</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Solidago</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">celtidifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>1198, 1339. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Solidago;species celtidifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbage moderately to densely hairy.</text>
      <biological_entity id="o27572" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Distal cauline leaf-blades ovate, much reduced distally.</text>
      <biological_entity constraint="distal cauline" id="o27573" name="leaf-blade" name_original="leaf-blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="much; distally" name="size" src="d0_s1" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads in wide arrays, usually with elongate proximal branches greatly exceeding subtending leaves.</text>
      <biological_entity id="o27574" name="head" name_original="heads" src="d0_s2" type="structure" />
      <biological_entity id="o27575" name="array" name_original="arrays" src="d0_s2" type="structure">
        <character is_modifier="true" name="width" src="d0_s2" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o27576" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o27577" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o27574" id="r2551" name="in" negation="false" src="d0_s2" to="o27575" />
      <relation from="o27574" id="r2552" modifier="usually" name="with" negation="false" src="d0_s2" to="o27576" />
      <relation from="o27576" id="r2553" modifier="greatly" name="exceeding" negation="false" src="d0_s2" to="o27577" />
    </statement>
    <statement id="d0_s3">
      <text>Rays florets 5–9.2n = 18, 36.</text>
      <biological_entity constraint="rays" id="o27578" name="floret" name_original="florets" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="9" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27579" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="18" value_original="18" />
        <character name="quantity" src="d0_s3" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy and silty soils, fields, woods and wet ground, mostly outer coastal plain</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy" />
        <character name="habitat" value="silty soils" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="woods" />
        <character name="habitat" value="wet ground" />
        <character name="habitat" value="outer coastal plain" modifier="mostly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Fla., Ga., La., Miss., N.C., Okla., S.C., Tex., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>52b.2.</number>
  
</bio:treatment>