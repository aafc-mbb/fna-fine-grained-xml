<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">364</other_info_on_meta>
    <other_info_on_meta type="mention_page">365</other_info_on_meta>
    <other_info_on_meta type="mention_page">382</other_info_on_meta>
    <other_info_on_meta type="mention_page">385</other_info_on_meta>
    <other_info_on_meta type="mention_page">393</other_info_on_meta>
    <other_info_on_meta type="treatment_page">383</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Rydberg in N. L. Britton et al." date="1914" rank="subtribe">chaenactidinae</taxon_name>
    <taxon_name authority="Lagasca" date="1816" rank="genus">BAHIA</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Sp. Pl.,</publication_title>
      <place_in_publication>30. 1816</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe chaenactidinae;genus BAHIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For J. F. Bahí, 1775–1841, professor of botany at Barcelona</other_info_on_name>
    <other_info_on_name type="fna_id">103363</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials, 10–80+ cm.</text>
      <biological_entity id="o14265" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="80" to_unit="cm" upper_restricted="false" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or spreading, branched from bases or throughout.</text>
      <biological_entity id="o14268" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character constraint="from bases" constraintid="o14269" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o14269" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o14271" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>all or mostly opposite or all or mostly alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o14270" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character name="arrangement" src="d0_s3" value="all" value_original="all" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades usually 1–2-ternately lobed (blades or lobes mostly filiform, lanceolate, linar, oblanceolate, oblong, or ovate), ultimate margins toothed or entire, faces sparsely to densely hairy (hairs white, straight, conic or fusiform, 0.1–0.3 or 0.3–0.8 mm), often glanddotted as well.</text>
      <biological_entity id="o14272" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually 1-2-ternately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o14273" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o14274" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in loose, corymbiform arrays.</text>
      <biological_entity id="o14275" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o14276" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o14275" id="r981" name="in" negation="false" src="d0_s6" to="o14276" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± hemispheric or broader, 6–14+ mm diam.</text>
      <biological_entity id="o14277" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="true" name="width" src="d0_s7" value="broader" value_original="broader" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="14" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 8–18+ in ± 2 series (reflexed in fruit, distinct, subequal or outer smaller, mostly lanceolate or oblanceolate, thin-herbaceous, margins membranous, rarely purplish).</text>
      <biological_entity id="o14278" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o14279" from="8" name="quantity" src="d0_s8" to="18" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o14279" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex, smooth or knobby, epaleate.</text>
      <biological_entity id="o14280" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s9" value="knobby" value_original="knobby" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 5–15, pistillate, fertile;</text>
      <biological_entity id="o14281" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="15" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow (sometimes pale).</text>
      <biological_entity id="o14282" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 25–120+, bisexual, fertile;</text>
      <biological_entity id="o14283" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s12" to="120" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow (hairy), tubes shorter than or about equaling cylindric or campanulate to funnelform throats, lobes 5, deltate to lanceovate.</text>
      <biological_entity id="o14284" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o14285" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than or about equaling cylindric or campanulate to funnelform throats" constraintid="o14286" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o14286" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
        <character is_modifier="true" name="shape" src="d0_s13" value="campanulate" value_original="campanulate" />
      </biological_entity>
      <biological_entity id="o14287" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s13" to="lanceovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae obpyramidal, 4-angled, ± hirtellous to ± sericeous;</text>
      <biological_entity id="o14288" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obpyramidal" value_original="obpyramidal" />
        <character is_modifier="false" name="shape" src="d0_s14" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="less hirtellous" name="pubescence" src="d0_s14" to="more or less sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi persistent, of 6–12 distinct, spatulate or oblanceolate to ovate or quadrate, basally and/or medially thickened, distally and/or laterally scarious scales in ± 1 series (apices usually muticous, sometimes some or all ± aristate).</text>
      <biological_entity id="o14289" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="oblanceolate" modifier="of 6-12 distinct" name="shape" src="d0_s15" to="ovate or quadrate" />
        <character is_modifier="false" modifier="basally; medially" name="size_or_width" src="d0_s15" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o14291" name="series" name_original="series" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s15" value="1" value_original="1" />
      </biological_entity>
      <relation from="o14290" id="r982" modifier="distally" name="in" negation="false" src="d0_s15" to="o14291" />
    </statement>
    <statement id="d0_s16">
      <text>x = 12.</text>
      <biological_entity id="o14290" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" modifier="laterally" name="texture" src="d0_s15" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity constraint="x" id="o14292" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>368.</number>
  <discussion>Species ca. 10 (4 in the flora).</discussion>
  <discussion>Bahia dissecta is treated here in Amauriopsis; B. oppositifolia and B. woodhousei are treated in Picradeniopsis; B. nudicaulis is treated in Platyschkuhria. These departures from the treatment of Bahia in a broad sense by W. L. Ellison (1964) are consistent with findings by B. G. Baldwin et al. (2002).</discussion>
  <references>
    <reference>Ellison, W. L. 1964. A systematic study of the genus Bahia (Compositae). Rhodora 66: 67–86; 177–215; 281–311.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves all or mostly opposite (distal sometimes alternate)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves all or mostly alternate (proximal sometimes opposite)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Annuals; leaf lobes filiform to linear, 0.5–1(–2+) mm wide, faces sparsely scabrellous; pappi 0.5–1 mm</description>
      <determination>1 Bahia bigelovii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perennials; leaf lobes lanceolate to oblong, 2–5(–20+) mm wide, faces densely scabrello-canescent; pappi 1–1.5 mm</description>
      <determination>2 Bahia absinthifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf lobes filiform to linear, 0.5–1.5(–3+) mm wide; pappus scales (outer cypselae) ± ovate to quadrate, 0.5–1 mm, apices ± muticous or (innermost cypselae) lanceolate to lance-subulate, 1–2.5 mm, apices ± aristate</description>
      <determination>3 Bahia biternata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf lobes oblong or ovate to oblanceolate, (1–)2–6(–8+) mm wide; pappus scales (all cypselae) spatulate to oblanceolate, 1–1.5 mm, apices ± muticous</description>
      <determination>4 Bahia pedata</determination>
    </key_statement>
  </key>
</bio:treatment>