<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="treatment_page">315</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1838" rank="genus">malacothrix</taxon_name>
    <taxon_name authority="A. Gray in A. Gray et al." date="1886" rank="species">foliosa</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer. ed.</publication_title>
      <place_in_publication>2, 1: 455. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus malacothrix;species foliosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067148</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 4–45 cm.</text>
      <biological_entity id="o7012" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="45" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1, erect, simple or branched proximally and/or distally or 2–10, decumbent to ascending, and ± branched distally, glabrous or sparsely arachnose (sometimes only in leaf-axils).</text>
      <biological_entity id="o7013" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="2" modifier="distally" name="quantity" src="d0_s1" to="10" />
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="more or less; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="arachnose" value_original="arachnose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: proximal oblanceolate to narrowly obovate, usually pinnately lobed (lobes usually unequal, apices acute or obtuse), sometimes ± fleshy, ultimate margins entire or dentate, faces glabrous (proximalmost cauline usually more deeply divided);</text>
      <biological_entity constraint="cauline" id="o7014" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o7015" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="narrowly obovate" />
        <character is_modifier="false" modifier="usually pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sometimes more or less" name="texture" src="d0_s2" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o7016" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o7017" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distal seldom notably reduced (sometimes pinnately lobed).</text>
      <biological_entity constraint="cauline" id="o7018" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o7019" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="seldom notably" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Calyculi 0 (i.e., outer phyllaries intergrading with inner), or of 5–12+, oblong or ovate to lanceolate or linear bractlets, hyaline margins 0.1–0.3 mm wide.</text>
      <biological_entity id="o7020" name="calyculus" name_original="calyculi" src="d0_s4" type="structure">
        <character name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7021" name="bractlet" name_original="bractlets" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s4" to="12" upper_restricted="false" />
        <character char_type="range_value" from="ovate" is_modifier="true" name="shape" src="d0_s4" to="lanceolate or linear" />
      </biological_entity>
      <biological_entity id="o7022" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
      <relation from="o7020" id="r663" name="consist_of" negation="false" src="d0_s4" to="o7021" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± campanulate, 5–12 × 2–7 mm.</text>
      <biological_entity id="o7023" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 12–22 (–40+, without calyculi) in 2–3 (–5+) series, (usually red-tinged) lanceolate to linear, hyaline margins 0.05–0.2 mm wide, faces glabrous.</text>
      <biological_entity id="o7024" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o7025" from="12" name="quantity" src="d0_s6" to="22" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s6" to="linear" />
      </biological_entity>
      <biological_entity id="o7025" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s6" to="5" upper_restricted="false" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o7026" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s6" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s6" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7027" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Receptacles not bristly.</text>
      <biological_entity id="o7028" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s7" value="bristly" value_original="bristly" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets 10–123;</text>
      <biological_entity id="o7029" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s8" to="123" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas light to medium yellow, 5–17 mm;</text>
      <biological_entity id="o7030" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s9" to="medium yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer ligules exserted 1–10 mm.</text>
      <biological_entity constraint="outer" id="o7031" name="ligule" name_original="ligules" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae ± cylindro-fusiform or ± prismatic, 0.9–1.7 mm, ribs extending to apices, ± equal or 5 more prominent than others;</text>
      <biological_entity id="o7032" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="cylindro-fusiform" value_original="cylindro-fusiform" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="prismatic" value_original="prismatic" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s11" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7033" name="rib" name_original="ribs" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character constraint="than others" constraintid="o7035" is_modifier="false" name="prominence" src="d0_s11" value="more or less equal or 5 more prominent" />
      </biological_entity>
      <biological_entity id="o7034" name="apex" name_original="apices" src="d0_s11" type="structure" />
      <biological_entity id="o7035" name="other" name_original="others" src="d0_s11" type="structure" />
      <relation from="o7033" id="r664" name="extending to" negation="false" src="d0_s11" to="o7034" />
    </statement>
    <statement id="d0_s12">
      <text>persistent pappi usually 0, rarely of 1–2 bristles.</text>
      <biological_entity id="o7036" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="true" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7037" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s12" to="2" />
      </biological_entity>
      <relation from="o7036" id="r665" modifier="rarely" name="consist_of" negation="false" src="d0_s12" to="o7037" />
    </statement>
    <statement id="d0_s13">
      <text>Pollen 70–100% 3-porate.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 14.</text>
      <biological_entity id="o7038" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="70-100%" name="architecture" src="d0_s13" value="3-porate" value_original="3-porate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7039" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Channel Islands, Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Channel Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Leafy desertdandelion</other_name>
  <discussion>Subspecies 4 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Outer ligules usually exserted 1–4 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Outer ligules exserted 5–15 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Calyculi 0 or bractlets intergrading with phyllaries; cypselae ± prismatic (5-angled), 1.3–1.6 mm, 5 ribs more prominent than others; Anacapa Island, California</description>
      <determination>6a Malacothrix foliosa subsp. crispifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Calyculi of 8–12+, oblong to linear bractlets; cypselae ± cylindro-fusiform, 0.9–1.5 mm, ribs ± equal; San Nicolas Island, California</description>
      <determination>6b Malacothrix foliosa subsp. polycephala</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems 1, usually erect; distal cauline leaves usually pinnately lobed (near bases, lobes 1–2 pairs, narrow); cypselae ± cylindro-fusiform, 0.9–1.5 mm, ribs ± equal; San Clemente Island, California</description>
      <determination>6c Malacothrix foliosa subsp. foliosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems (1–)3–5(–10), erect or decumbent to ascending; distal cauline leaves usually pinnately lobed (from bases to near apices); cypselae ± prismatic (5-angled) 1.3–1.7 mm, 5 ribs more prominent than others; Santa Barbara Island, California</description>
      <determination>6d Malacothrix foliosa subsp. philbrickii</determination>
    </key_statement>
  </key>
</bio:treatment>