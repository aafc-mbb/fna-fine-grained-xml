<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">314</other_info_on_meta>
    <other_info_on_meta type="treatment_page">315</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="(A. Gray) A. Gray" date="1880" rank="species">inornatus</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1992" rank="variety">keilii</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>72: 191. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species inornatus;variety keilii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068349</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems ascending-erect, 30–60 cm, densely hirsutulous (hairs spreading-deflexed, sometimes ascending-strigose) distally.</text>
      <biological_entity id="o13590" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending-erect" value_original="ascending-erect" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s0" value="hirsutulous" value_original="hirsutulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: proximal and mid margins with stiff, spreading cilia, faces sparsely strigoso-hispid.</text>
      <biological_entity id="o13591" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal and mid" id="o13592" name="margin" name_original="margins" src="d0_s1" type="structure" />
      <biological_entity id="o13593" name="cilium" name_original="cilia" src="d0_s1" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o13594" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="strigoso-hispid" value_original="strigoso-hispid" />
      </biological_entity>
      <relation from="o13592" id="r1235" name="with" negation="false" src="d0_s1" to="o13593" />
    </statement>
    <statement id="d0_s2">
      <text>Involucres 4.5–5.5 mm.</text>
      <biological_entity id="o13595" name="involucre" name_original="involucres" src="d0_s2" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s2" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, grassy slopes and meadows, areas of conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy slopes" modifier="dry" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="areas" constraint="of conifer woodlands" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>101c.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Phyllaries in var. keilii vary from strigose to glabrous and glandular, the glandularity perhaps reflecting hybridization with Erigeron breweri.</discussion>
  
</bio:treatment>