<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">30</other_info_on_meta>
    <other_info_on_meta type="mention_page">487</other_info_on_meta>
    <other_info_on_meta type="treatment_page">551</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Schott" date="1818" rank="genus">HETERANTHEMIS</taxon_name>
    <place_of_publication>
      <publication_title>Isis (Oken)</publication_title>
      <place_in_publication>1818(5): 822. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus HETERANTHEMIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek heteros -, different, and anthemis, a genus name</other_info_on_name>
    <other_info_on_name type="fna_id">115265</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–30 (–100+) cm (plants sticky, viscid).</text>
      <biological_entity id="o12476" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1, erect, usually branched distally, hirtellous to pilosulous (hairs basifixed, some gland-tipped).</text>
      <biological_entity id="o12477" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s1" to="pilosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o12479" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o12478" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades obovate to oblong (bases sometimes ± clasping), often pinnately lobed, ultimate margins usually dentate, rarely entire, faces hirtellous to pilosulous (some hairs gland-tipped).</text>
      <biological_entity id="o12480" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s5" to="oblong" />
        <character is_modifier="false" modifier="often pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o12481" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o12482" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="hirtellous" name="pubescence" src="d0_s5" to="pilosulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly or in 2s or 3s.</text>
      <biological_entity id="o12483" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character constraint="in 2s; in 2s" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in 2s" value_original="in 2s" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres hemispheric or broader, 12–25+ mm diam.</text>
      <biological_entity id="o12484" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s7" value="broader" value_original="broader" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s7" to="25" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, 20–30+ in 2–3+ series, distinct, ovate or obovate to lance-deltate or lanceolate (not keeled abaxially, usually each with central resin canal), unequal, margins and apices scarious (tips of inner usually dilated).</text>
      <biological_entity id="o12485" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o12486" from="20" name="quantity" src="d0_s8" to="30" upper_restricted="false" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s8" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="lance-deltate or lanceolate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o12486" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o12487" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o12488" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex to conic, epaleate.</text>
      <biological_entity id="o12489" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s9" to="conic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 13–21+, pistillate, fertile;</text>
      <biological_entity id="o12490" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s10" to="21" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow, laminae ± linear.</text>
      <biological_entity id="o12491" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o12492" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 40–80 (–100+), bisexual, fertile;</text>
      <biological_entity id="o12493" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="100" upper_restricted="false" />
        <character char_type="range_value" from="40" name="quantity" src="d0_s12" to="80" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas proximally ochroleucous, distally yellow, tubes cylindric (usually gland-dotted), throats funnelform, lobes 5, deltate.</text>
      <biological_entity id="o12494" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s13" value="ochroleucous" value_original="ochroleucous" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o12495" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o12496" name="throat" name_original="throats" src="d0_s13" type="structure" />
      <biological_entity id="o12497" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae dimorphic: outer 3-angled (each angle ± winged, wings ± spine-tipped);</text>
      <biological_entity id="o12498" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s14" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="outer" id="o12499" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="3-angled" value_original="3-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>inner ± compressed (adaxial angles ± winged, wings ± spine-tipped);</text>
      <biological_entity id="o12500" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s15" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="inner" id="o12501" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ribs 0, faces glabrous (peric) arps without myxogenic cells or resin sacs;</text>
      <biological_entity id="o12502" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s16" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o12503" name="rib" name_original="ribs" src="d0_s16" type="structure">
        <character name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o12504" name="face" name_original="faces" src="d0_s16" type="structure">
        <character constraint="without resin sacs" constraintid="o12506" is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="myxogenic" id="o12505" name="cell" name_original="cells" src="d0_s16" type="structure" />
      <biological_entity constraint="resin" id="o12506" name="sac" name_original="sacs" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>embryo-sac development tetrasporic;</text>
      <biological_entity id="o12507" name="cypsela" name_original="cypselae" src="d0_s17" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s17" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o12508" name="embryo-sac" name_original="embryo-sac" src="d0_s17" type="structure">
        <character is_modifier="false" name="development" src="d0_s17" value="tetrasporic" value_original="tetrasporic" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>pappi 0.</text>
      <biological_entity id="o12509" name="cypsela" name_original="cypselae" src="d0_s18" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s18" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>x = 9.</text>
      <biological_entity id="o12510" name="pappus" name_original="pappi" src="d0_s18" type="structure">
        <character name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o12511" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mediterranean Europe, n Africa, widely adventive elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mediterranean Europe" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="widely adventive elsewhere" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>130.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>