<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="treatment_page">458</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">marshalliinae</taxon_name>
    <taxon_name authority="Schreber" date="1791" rank="genus">marshallia</taxon_name>
    <taxon_name authority="Beadle &amp; F. E. Boynton" date="1901" rank="species">ramosa</taxon_name>
    <place_of_publication>
      <publication_title>Biltmore Bot. Stud.</publication_title>
      <place_in_publication>1: 8, plate 2. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe marshalliinae;genus marshallia;species ramosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067166</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 20–60 cm.</text>
      <biological_entity id="o25443" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o25444" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>basal petiolate;</text>
      <biological_entity constraint="basal" id="o25445" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 3-nerved, linear, 6–18 cm × 2–7 mm.</text>
      <biological_entity id="o25446" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s3" to="18" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (2–) 4–10 (–20), 10–25 mm diam.</text>
      <biological_entity id="o25447" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s4" to="4" to_inclusive="false" />
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="20" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="10" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 6–12 cm.</text>
      <biological_entity id="o25448" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s5" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 5–8 × 1.5–2 mm, (margins often winged proximally) apices obtuse to acute, often mucronulate.</text>
      <biological_entity id="o25449" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25450" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="acute" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s6" value="mucronulate" value_original="mucronulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Paleae ± linear, apices obtuse, mucronulate to ± subulate.</text>
      <biological_entity id="o25451" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o25452" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character char_type="range_value" from="mucronulate" name="shape" src="d0_s7" to="more or less subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas usually white, sometimes pale lavender, lobes 4–6 × 1 mm.</text>
      <biological_entity id="o25453" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="pale lavender" value_original="pale lavender" />
      </biological_entity>
      <biological_entity id="o25454" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="6" to_unit="mm" />
        <character name="width" src="d0_s8" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pappi: scales margins entire or denticulate.</text>
      <biological_entity id="o25455" name="pappus" name_original="pappi" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 18.</text>
      <biological_entity constraint="scales" id="o25456" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25457" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandstone outcrops, pine savannas, mixed hardwoods and pines</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandstone outcrops" />
        <character name="habitat" value="pine savannas" />
        <character name="habitat" value="mixed hardwoods" />
        <character name="habitat" value="pines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Marshallia ramosa is known only from sandstone outcrops in southeastern Georgia and in the Florida panhandle. It is associated with pine savannas in Florida and with mixed hardwoods and pines in Georgia.</discussion>
  
</bio:treatment>