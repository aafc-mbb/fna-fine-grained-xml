<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="mention_page">203</other_info_on_meta>
    <other_info_on_meta type="treatment_page">202</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1834" rank="genus">townsendia</taxon_name>
    <taxon_name authority="A. Gray ex Rothrock" date="1879" rank="species">rothrockii</taxon_name>
    <place_of_publication>
      <publication_title>Rep. U.S. Geogr. Surv., Wheeler</publication_title>
      <place_in_publication>6: 148. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus townsendia;species rothrockii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067777</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 1–3 cm (± pulvinate).</text>
      <biological_entity id="o4874" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="3" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± erect;</text>
      <biological_entity id="o4875" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes 0.1–1+ mm, glabrous or ± strigose.</text>
      <biological_entity id="o4876" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, ± spatulate to oblanceolate, 10–35 × 2–7 mm, ± fleshy, faces sparsely strigose or glabrous.</text>
      <biological_entity id="o4877" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character char_type="range_value" from="less spatulate" name="shape" src="d0_s3" to="oblanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s3" to="35" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o4878" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads ± sessile or on peduncles 5–25+ mm.</text>
      <biological_entity id="o4879" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="on peduncles" value_original="on peduncles" />
      </biological_entity>
      <biological_entity id="o4880" name="peduncle" name_original="peduncles" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="25" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <relation from="o4879" id="r439" name="on" negation="false" src="d0_s4" to="o4880" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± hemispheric, 12–28+ mm diam.</text>
      <biological_entity id="o4881" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="hemispheric" value_original="hemispheric" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s5" to="28" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 40–60+ in (3–) 4–5+ series, the longer obovate to oblanceolate, 7–9+ mm (l/w = 2.5–5), apices obtuse to acute, abaxial faces glabrous.</text>
      <biological_entity id="o4882" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o4883" from="40" name="quantity" src="d0_s6" to="60" upper_restricted="false" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="longer" value_original="longer" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s6" to="oblanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4883" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s6" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="5" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o4884" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s6" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4885" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 18–40;</text>
      <biological_entity id="o4886" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s7" to="40" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas blue to purplish adaxially, laminae 8–16 mm, glabrous abaxially.</text>
      <biological_entity id="o4887" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="blue" modifier="adaxially" name="coloration" src="d0_s8" to="purplish" />
      </biological_entity>
      <biological_entity id="o4888" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="16" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 60–100+;</text>
      <biological_entity id="o4889" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="60" name="quantity" src="d0_s9" to="100" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 3.5–4.5+ mm.</text>
      <biological_entity id="o4890" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 4 mm, faces nearly glabrous (discs) or sparsely hairy (ray, hairs mostly at bases), hair tips glochidiform;</text>
      <biological_entity id="o4891" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o4892" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="hair" id="o4893" name="tip" name_original="tips" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="glochidiform" value_original="glochidiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi persistent;</text>
    </statement>
    <statement id="d0_s13">
      <text>on ray cypselae 12–20 lanceolate to subulate scales 0.5–1.5 mm;</text>
      <biological_entity id="o4894" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="12" name="quantity" src="d0_s13" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>on disc cypselae 15–30 subulate to setiform scales 3–6 mm. 2n = 36 (apomicts).</text>
      <biological_entity id="o4895" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s13" to="subulate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s14" to="30" />
      </biological_entity>
      <biological_entity id="o4896" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s14" to="setiform" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4897" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>South slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="south slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3600–4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="3600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>The types of Townsendia rothrockii and T. glabella may be conspecific.</discussion>
  
</bio:treatment>