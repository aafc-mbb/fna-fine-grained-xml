<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="mention_page">260</other_info_on_meta>
    <other_info_on_meta type="mention_page">265</other_info_on_meta>
    <other_info_on_meta type="mention_page">301</other_info_on_meta>
    <other_info_on_meta type="treatment_page">300</other_info_on_meta>
    <other_info_on_meta type="illustration_page">300</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Pursh" date="1813" rank="species">compositus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 535. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species compositus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250066575</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">compositus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="variety">discoideus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species compositus;variety discoideus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">compositus</taxon_name>
    <taxon_name authority="Macoun" date="unknown" rank="variety">glabratus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species compositus;variety glabratus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">compositus</taxon_name>
    <taxon_name authority="(Rydberg) J. F. Macbride &amp; Payson" date="unknown" rank="variety">multifidus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species compositus;variety multifidus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">gormanii</taxon_name>
    <taxon_hierarchy>genus Erigeron;species gormanii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–15 (–25 cm);</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices simple or branches usually relatively thick and short, rarely slender and rhizomelike, covered with persistent leaf-bases.</text>
      <biological_entity id="o1172" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s0" to="15" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o1173" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o1174" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually relatively" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" modifier="rarely" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomelike" value_original="rhizomelike" />
      </biological_entity>
      <biological_entity id="o1175" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o1174" id="r106" name="covered with" negation="false" src="d0_s1" to="o1175" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect (simple, ± scapiform), sparsely hispido-pilose, minutely glandular.</text>
      <biological_entity id="o1176" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="hispido-pilose" value_original="hispido-pilose" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal (persistent);</text>
      <biological_entity id="o1177" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o1178" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades spatulate to obovate-spatulate, 5–50 (–70) × (2–) 4–12 mm, margins (1–) 2–3 (–4) -ternately lobed or dissected, cauline bractlike, mostly entire, faces densely hispiduloso-puberulent to glabrate, minutely glandular.</text>
      <biological_entity id="o1179" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s4" to="obovate-spatulate" />
        <character char_type="range_value" from="50" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s4" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1180" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dissected" value_original="dissected" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o1181" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="bractlike" value_original="bractlike" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o1182" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="densely hispiduloso-puberulent" name="pubescence" src="d0_s4" to="glabrate" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (sometimes disciform) 1.</text>
      <biological_entity id="o1183" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–10 × 8–20 mm.</text>
      <biological_entity id="o1184" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series (purple-tipped), hirsute (hairs spreading), minutely glandular.</text>
      <biological_entity id="o1185" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s7" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o1186" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o1185" id="r107" name="in" negation="false" src="d0_s7" to="o1186" />
    </statement>
    <statement id="d0_s8">
      <text>Ray (pistillate) florets 20–60;</text>
      <biological_entity constraint="ray" id="o1187" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white to pink or blue, usually 6–12 mm, often reduced to tubes (heads disciform), laminae not coiling or reflexing.</text>
      <biological_entity id="o1188" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pink or blue" />
        <character char_type="range_value" from="6" from_unit="mm" modifier="usually" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
        <character constraint="to tubes" constraintid="o1189" is_modifier="false" modifier="often" name="size" src="d0_s9" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o1189" name="tube" name_original="tubes" src="d0_s9" type="structure" />
      <biological_entity id="o1190" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3–5 mm.</text>
      <biological_entity constraint="disc" id="o1191" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.6–2.7 mm, 2-nerved, faces sparsely strigose-hirsute;</text>
      <biological_entity id="o1192" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s11" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o1193" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="strigose-hirsute" value_original="strigose-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer usually of setae, sometimes 0, inner of 12–20 bristles.</text>
      <biological_entity id="o1195" name="seta" name_original="setae" src="d0_s12" type="structure">
        <character modifier="sometimes" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o1196" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s12" to="20" />
      </biological_entity>
      <relation from="o1194" id="r108" name="inner of" negation="false" src="d0_s12" to="o1196" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 18, 36, 45, 54.</text>
      <biological_entity id="o1194" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character constraint="of setae" constraintid="o1195" is_modifier="false" name="position" src="d0_s12" value="outer" value_original="outer" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1197" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
        <character name="quantity" src="d0_s13" value="36" value_original="36" />
        <character name="quantity" src="d0_s13" value="45" value_original="45" />
        <character name="quantity" src="d0_s13" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (May–)Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sagebrush, rabbitbrush, aspen to aspen-fir, subalpine meadows, cliffs, talus, and boulders</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="rabbitbrush" />
        <character name="habitat" value="aspen" />
        <character name="habitat" value="aspen-fir" />
        <character name="habitat" value="subalpine meadows" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="boulders" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(in e Canada, possibly Greenland and Arctic bridge gap, 10–200–)1800–4300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" modifier="possibly" to="200" to_unit="" from="10" from_unit="" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Nfld. and Labr. (Nfld.), N.W.T., Nunavut, Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Idaho, Mont., Nev., N.Dak., Oreg., S.Dak., Utah, Wash., Wyo.; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>59.</number>
  <other_name type="past_name">compositum</other_name>
  <other_name type="common_name">Dwarf mountain fleabane</other_name>
  <other_name type="common_name">vergerette à feuilles segmentées</other_name>
  <discussion>Correlations among ploidal level, breeding systems, and morphologic variation have been studied in detail in Erigeron compositus. Five informally designated population systems of diploids are geographically restricted (all of the northwestern United States and adjacent Canada) and primarily sexual, compared to the polyploids, which are agamospermous and apparently of hybrid origin, at least in some cases (R. D. Noyes et al. 1995; Noyes and D. E. Soltis 1996). Reduction in ray floret laminae usually is correlated with polyploidy. Plants with 1-ternately lobed leaves have been identified as var. glabratus, an element of variation that does not have a geographic pattern.</discussion>
  <discussion>Among closely related species, Erigeron compositus is the only one that produces strongly thickened caudex branches; occasional collections show a tendency toward the slender, loose branches characteristic of the other species.</discussion>
  <references>
    <reference>Noyes, R. D. and D. E. Soltis. 1996. Genotypic variation in agamospermous Erigeron compositus (Asteraceae). Amer. J. Bot. 83: 1292–1303.</reference>
    <reference>Noyes, R. D., D. E. Soltis, and P. S. Soltis. 1995. Genetic and cytological investigations in sexual Erigeron compositus (Astereaceae). Syst. Bot. 20: 132–146.</reference>
  </references>
  
</bio:treatment>