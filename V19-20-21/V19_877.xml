<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">518</other_info_on_meta>
    <other_info_on_meta type="treatment_page">516</other_info_on_meta>
    <other_info_on_meta type="illustration_page">514</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">artemisia</taxon_name>
    <taxon_name authority="(Rydberg) McArthur" date="1981" rank="subgenus">tridentatae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="species">tridentata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">tridentata</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus artemisia;subgenus tridentatae;species tridentata;subspecies tridentata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068076</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="(A. Gray) Rydberg" date="unknown" rank="species">angustifolia</taxon_name>
    <taxon_hierarchy>genus Artemisia;species angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tridentata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">xericensis</taxon_name>
    <taxon_hierarchy>genus Artemisia;species tridentata;subspecies xericensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 100–200 (–300) cm.</text>
      <biological_entity id="o16693" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="200" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="300" to_unit="cm" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Vegetative branches nearly equaling flowering branches.</text>
      <biological_entity id="o16694" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <biological_entity id="o16695" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="nearly" name="variability" src="d0_s1" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="flowering" value_original="flowering" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cuneate or lanceolate, 0.5–1.2 (–2.5) × 0.2–0.3 (–0.6) cm, 3-lobed (lobes to 1/3 lengths of blades, rounded).</text>
      <biological_entity id="o16696" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s2" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="0.6" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s2" to="0.3" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in paniculiform arrays 5–15 (–20) × (1.5–) 5–6 cm.</text>
      <biological_entity id="o16697" name="head" name_original="heads" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" modifier="in paniculiform arrays" name="atypical_length" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" modifier="in paniculiform arrays" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" modifier="in paniculiform arrays" name="atypical_width" src="d0_s3" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" modifier="in paniculiform arrays" name="width" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 1.5–2.5 × 1–2 mm.</text>
      <biological_entity id="o16698" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s4" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Florets 4–6.</text>
      <biological_entity id="o16699" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae glabrous.</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = 18, 36.</text>
      <biological_entity id="o16700" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16701" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="18" value_original="18" />
        <character name="quantity" src="d0_s7" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid summer–late fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late fall" from="mid summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deep, well-drained (usually sandy) soils in valley bottoms, lower montane slopes, along drainages</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="( sandy ) soils" modifier="deep well-drained usually" constraint="in valley bottoms , lower montane slopes ," />
        <character name="habitat" value="valley bottoms" modifier="in" />
        <character name="habitat" value="lower montane slopes" />
        <character name="habitat" value="drainages" modifier="along" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Ariz., Calif., Colo., Idaho, Mont., Nev., N.Mex., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17a.</number>
  <other_name type="common_name">Great Basin sagebrush</other_name>
  <other_name type="common_name">big sage</other_name>
  <discussion>Subspecies tridentata is the common sagebrush of deep, well-drained soils in the Great Basin of western North America, where it is often the dominant shrub of valleys and open grasslands. On drier sites and on high plateaus, it is replaced by subsp. wyomingensis, a taxon that appears to be increasing with prolonged droughts and disturbance from grazing. In dry valley bottoms of the Great Basin, subsp. tridentata is conspicuous by its great height and wide arrays of heads along roadways, fencerows, and other areas where moisture is more readily available through runoff or reduced competition.</discussion>
  
</bio:treatment>