<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="treatment_page">101</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Nuttall" date="1834" rank="genus">wyethia</taxon_name>
    <taxon_name authority="(de Candolle) Nuttall" date="1840" rank="species">helenioides</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 353. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus wyethia;species helenioides</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067821</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alarconia</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">helenioides</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 537. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Alarconia;species helenioides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants (15–) 25–40 (–60) cm.</text>
      <biological_entity id="o9544" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="25" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: blades (whitish to grayish) oblong-ovate to lance-elliptic, 18–38 cm, margins usually entire, sometimes serrulate or dentate (often undulate), faces densely tomentose to tomentulose (usually glanddotted as well), glabrescent;</text>
      <biological_entity constraint="basal" id="o9545" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o9546" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s1" to="lance-elliptic" />
        <character char_type="range_value" from="18" from_unit="cm" name="some_measurement" src="d0_s1" to="38" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9547" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s1" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o9548" name="face" name_original="faces" src="d0_s1" type="structure">
        <character char_type="range_value" from="densely tomentose" name="pubescence" src="d0_s1" to="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline leaves (3–6) similar, smaller.</text>
      <biological_entity constraint="basal" id="o9549" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o9550" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s2" to="6" />
        <character is_modifier="false" name="size" src="d0_s2" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads usually borne singly (–2+).</text>
      <biological_entity id="o9551" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="singly" value_original="singly" />
        <character name="quantity" src="d0_s3" value="2+]" value_original="2+]" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres hemispheric or broader, 35–60+ mm diam.</text>
      <biological_entity id="o9552" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="hemispheric" value_original="hemispheric" />
        <character is_modifier="false" name="width" src="d0_s4" value="broader" value_original="broader" />
        <character char_type="range_value" from="35" from_unit="mm" name="diameter" src="d0_s4" to="60" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 16–24, unequal, herbaceous, margins not ciliolate, faces tomentose;</text>
      <biological_entity id="o9553" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s5" to="24" />
        <character is_modifier="false" name="size" src="d0_s5" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s5" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o9554" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o9555" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>outer (30–) 40–80+ mm (foliaceous, much surpassing the discs).</text>
      <biological_entity constraint="outer" id="o9556" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s6" to="80" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 13–21;</text>
      <biological_entity id="o9557" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s7" to="21" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae 20–35 (–50) mm.</text>
      <biological_entity id="o9558" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s8" to="35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 12–15 mm, distally strigillose.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 38.</text>
      <biological_entity id="o9559" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s9" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9560" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Feb–)Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
        <character name="flowering time" char_type="atypical_range" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy slopes, openings in woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy slopes" />
        <character name="habitat" value="openings" constraint="in woodlands" />
        <character name="habitat" value="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10 –1600(–2000) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="10" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="2000" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Wyethia helenioides grows in the Sierra Nevada foothills and west of the Central Valley, avoiding the fog belt.</discussion>
  
</bio:treatment>