<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="mention_page">306</other_info_on_meta>
    <other_info_on_meta type="mention_page">307</other_info_on_meta>
    <other_info_on_meta type="treatment_page">308</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Molina" date="1782" rank="genus">madia</taxon_name>
    <taxon_name authority="(Smith) D. D. Keck" date="1940" rank="species">gracilis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>5: 169. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus madia;species gracilis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067142</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sclerocarpus</taxon_name>
    <taxon_name authority="Smith" date="unknown" rank="species">gracilis</taxon_name>
    <place_of_publication>
      <publication_title>in A. Rees, Cycl.</publication_title>
      <place_in_publication>31: Sclerocarpus no. 2. 1815</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sclerocarpus;species gracilis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Madia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">gracilis</taxon_name>
    <taxon_name authority="D. D. Keck" date="unknown" rank="subspecies">collina</taxon_name>
    <taxon_hierarchy>genus Madia;species gracilis;subspecies collina;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Madia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">gracilis</taxon_name>
    <taxon_name authority="D. D. Keck" date="unknown" rank="subspecies">pilosa</taxon_name>
    <taxon_hierarchy>genus Madia;species gracilis;subspecies pilosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 6–100 cm, self-compatible (heads not showy).</text>
      <biological_entity id="o22905" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="self-compatible" value_original="self-compatible" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally pilose to hirsute, distally glandular-pubescent, glands yelloish, purple, or black, lateral branches seldom surpassing main-stems.</text>
      <biological_entity id="o22906" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="proximally pilose" name="pubescence" src="d0_s1" to="hirsute" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o22907" name="gland" name_original="glands" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="black" value_original="black" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o22908" name="branch" name_original="branches" src="d0_s1" type="structure" />
      <biological_entity id="o22909" name="main-stem" name_original="main-stems" src="d0_s1" type="structure" />
      <relation from="o22908" id="r1569" modifier="seldom" name="surpassing" negation="false" src="d0_s1" to="o22909" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades oblong to linear, 1–10 (–15) cm × 1–8 (–10) mm.</text>
      <biological_entity id="o22910" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s2" to="linear" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s2" to="10" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in ± open, paniculiform or racemiform arrays.</text>
      <biological_entity id="o22911" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o22912" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <relation from="o22911" id="r1570" name="in" negation="false" src="d0_s3" to="o22912" />
    </statement>
    <statement id="d0_s4">
      <text>Involucres depressed-globose to urceolate, 5–10 mm.</text>
      <biological_entity id="o22913" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="depressed-globose" name="shape" src="d0_s4" to="urceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries sometimes hirsute, always finely or coarsely glandular-pubescent, glands yellowish, purple, or black, apices erect or ± reflexed, flat.</text>
      <biological_entity id="o22914" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="always finely; finely; coarsely" name="pubescence" src="d0_s5" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o22915" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="black" value_original="black" />
      </biological_entity>
      <biological_entity id="o22916" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Paleae mostly persistent, connate 1/2+ their lengths.</text>
      <biological_entity id="o22917" name="palea" name_original="paleae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/2" name="lengths" src="d0_s6" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 3–10;</text>
      <biological_entity id="o22918" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas lemon yellow or greenish yellow, laminae 1.5–8 mm.</text>
      <biological_entity id="o22919" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="lemon yellow" value_original="lemon yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish yellow" value_original="greenish yellow" />
      </biological_entity>
      <biological_entity id="o22920" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 2–16+, bisexual, fertile;</text>
      <biological_entity id="o22921" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="16" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 2.5–5 mm, pubescent;</text>
      <biological_entity id="o22922" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers ± dark purple.</text>
      <biological_entity id="o22923" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray cypselae black, purple, or mottled, dull, compressed, beakless (or nearly so).</text>
      <biological_entity constraint="ray" id="o22924" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="mottled" value_original="mottled" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="mottled" value_original="mottled" />
        <character is_modifier="false" name="reflectance" src="d0_s12" value="dull" value_original="dull" />
        <character is_modifier="false" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="beakless" value_original="beakless" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc cypselae similar.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 32, 48.</text>
      <biological_entity constraint="disc" id="o22925" name="cypsela" name_original="cypselae" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o22926" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="32" value_original="32" />
        <character name="quantity" src="d0_s14" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open or partially shaded slopes or flats in grasslands, meadows, shrublands, woodlands, and forests, disturbed sites, stream banks, roadsides, coarse to fine textured soils, sometimes serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open" constraint="in grasslands" />
        <character name="habitat" value="shaded slopes" modifier="partially" constraint="in grasslands" />
        <character name="habitat" value="flats" constraint="in grasslands" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="forests" modifier="and" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="coarse to fine textured soils" />
        <character name="habitat" value="sometimes serpentine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Mont., Nev., Oreg., Utah, Wash.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Gumweed</other_name>
  <discussion>Madia gracilis occurs widely in California (except the warm deserts), is scattered across much of Nevada, Oregon, and Washington (outside the driest regions), and extends into southernmost British Columbia, north-western Montana, and northern Utah. Near the coast, M. gracilis sometimes co-occurs with M. sativa; the two species are partially interfertile (M. gracilis tends to flower earlier than M. sativa; J. Clausen 1951). Reported occurrences of M. gracilis in Maine and South America have not been confirmed.</discussion>
  
</bio:treatment>