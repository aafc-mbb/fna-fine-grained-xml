<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">179</other_info_on_meta>
    <other_info_on_meta type="mention_page">181</other_info_on_meta>
    <other_info_on_meta type="treatment_page">180</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">carthamus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">tinctorius</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 830. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus carthamus;species tinctorius</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200023631</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 30–100+ cm, herbage ± glabrous.</text>
      <biological_entity id="o5356" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5357" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± stramineous, glabrous.</text>
      <biological_entity id="o5358" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="coloration" src="d0_s1" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually all cauline, dark green;</text>
      <biological_entity id="o5359" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s2" value="dark green" value_original="dark green" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o5360" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blades lanceolate to elliptic or broadly ovate, 2–8.5 cm, margins dentate with minutely spine-tipped teeth, veiny, shiny.</text>
      <biological_entity id="o5361" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="elliptic or broadly ovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="8.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5362" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character constraint="with teeth" constraintid="o5363" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="veiny" value_original="veiny" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="shiny" value_original="shiny" />
      </biological_entity>
      <biological_entity id="o5363" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="minutely" name="architecture_or_shape" src="d0_s3" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres ovoid, 20–40 mm diam., ± glabrous.</text>
      <biological_entity id="o5364" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s4" to="40" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Outer phyllaries spreading to reflexed, 1.5–2 times longer than inner, terminal appendages minutely spiny-toothed, minutely spine-tipped.</text>
      <biological_entity constraint="outer" id="o5365" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s5" to="reflexed" />
        <character constraint="phyllary" constraintid="o5366" is_modifier="false" name="length_or_size" src="d0_s5" value="1.5-2 times longer than inner phyllaries" />
      </biological_entity>
      <biological_entity constraint="inner" id="o5366" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure" />
      <biological_entity constraint="terminal" id="o5367" name="appendage" name_original="appendages" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s5" value="spiny-toothed" value_original="spiny-toothed" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s5" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Corollas yellow to red, 20–30 mm, throats abruptly expanded;</text>
      <biological_entity id="o5368" name="corolla" name_original="corollas" src="d0_s6" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s6" to="red" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5369" name="throat" name_original="throats" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="abruptly" name="size" src="d0_s6" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers yellow to red;</text>
      <biological_entity id="o5370" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s7" to="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pollen yellow to red.</text>
      <biological_entity id="o5371" name="pollen" name_original="pollen" src="d0_s8" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s8" to="red" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae white, 7–9 mm, slightly roughened;</text>
      <biological_entity id="o5372" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="relief_or_texture" src="d0_s9" value="roughened" value_original="roughened" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappus-scales absent or if present, 1–4 mm. 2n = 24.</text>
      <biological_entity id="o5373" name="pappus-scale" name_original="pappus-scales" src="d0_s10" type="structure">
        <character constraint="if present" is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s10" value="if present" value_original="if present" />
        <character char_type="range_value" from="1" from_unit="mm" name="distance" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5374" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer (May–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Escaped from cultivation in disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cultivation" modifier="escaped from" />
        <character name="habitat" value="disturbed sites" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C.; Ariz., Calif., Colo., Idaho, Ill., Iowa, Kans., Mass., Mont., Nebr., N.Mex., N.Dak., Ohio, Oreg., Utah, Wash.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Safflower</other_name>
  <discussion>Carthamus tinctorius is apparently native originally to the eastern Mediterranean; it is known only in cultivation and as escapes today. Safflower has been reported from Texas; I have not seen the specimen.</discussion>
  <discussion>Safflower is cultivated as an oil seed, a source of vegetable dye, as birdseed, and as an ornamental. It is one of the earliest known crop plants, with cultivation dating back to prehistoric times. In the United States safflower is grown principally in California and Arizona; it has been a successful crop in every state west of the 100th meridian.</discussion>
  <discussion>Carthamus oxyacantha M. Bieberstein (wild safflower) was collected in 1978 in Monterey County, California. It is considered by the United States Department of Food and Agriculture to be a noxious weed subject to eradication if found. In central and southern Asia it is a pernicious weed of agricultural lands and other disturbed ground. Carthamus oxyacantha most closely resembles cultivated safflower; it has smaller heads and much spinier leaves. Its cypselae are usually darkly pigmented, smaller (4–5 mm versus 5.5–9 mm), and almost always lack pappi.</discussion>
  
</bio:treatment>