<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David J. Keil</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">12</other_info_on_meta>
    <other_info_on_meta type="mention_page">28</other_info_on_meta>
    <other_info_on_meta type="mention_page">64</other_info_on_meta>
    <other_info_on_meta type="mention_page">82</other_info_on_meta>
    <other_info_on_meta type="mention_page">83</other_info_on_meta>
    <other_info_on_meta type="treatment_page">85</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ECHINOPS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 814. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 356. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus ECHINOPS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek echinos, hedgehog, spiny, and ops, face, appearance, alluding to spiny heads</other_info_on_name>
    <other_info_on_name type="fna_id">111246</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 100–200 cm, herbage usually ± tomentose, spiny.</text>
      <biological_entity id="o15485" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o15486" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually more or less" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="spiny" value_original="spiny" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect, simple or branched.</text>
      <biological_entity id="o15487" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>sessile or petiolate;</text>
      <biological_entity id="o15488" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins dentate to pinnately 1–3-pinnately lobed or divided, lobes and teeth spiny, faces ± tomentose, sometimes glandular.</text>
      <biological_entity constraint="blade" id="o15489" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s4" to="pinnately 1-3-pinnately lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o15490" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spiny" value_original="spiny" />
      </biological_entity>
      <biological_entity id="o15491" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spiny" value_original="spiny" />
      </biological_entity>
      <biological_entity id="o15492" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads discoid, many, each with 1 floret, sessile, in pedunculate, spheric secondary heads.</text>
      <biological_entity id="o15493" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="discoid" value_original="discoid" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o15494" name="floret" name_original="floret" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o15495" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pedunculate" value_original="pedunculate" />
        <character is_modifier="true" name="shape" src="d0_s5" value="spheric" value_original="spheric" />
      </biological_entity>
      <relation from="o15493" id="r1414" name="with" negation="false" src="d0_s5" to="o15494" />
      <relation from="o15493" id="r1415" name="in" negation="false" src="d0_s5" to="o15495" />
    </statement>
    <statement id="d0_s6">
      <text>Secondary involucres of reflexed, laciniate-pinnatifid bracts.</text>
      <biological_entity constraint="secondary" id="o15496" name="involucre" name_original="involucres" src="d0_s6" type="structure" />
      <biological_entity id="o15497" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character is_modifier="true" name="shape" src="d0_s6" value="laciniate-pinnatifid" value_original="laciniate-pinnatifid" />
      </biological_entity>
      <relation from="o15496" id="r1416" name="consist_of" negation="false" src="d0_s6" to="o15497" />
    </statement>
    <statement id="d0_s7">
      <text>Primary involucres ellipsoid, subtended by bristles.</text>
      <biological_entity constraint="primary" id="o15498" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity id="o15499" name="bristle" name_original="bristles" src="d0_s7" type="structure" />
      <relation from="o15498" id="r1417" name="subtended by" negation="false" src="d0_s7" to="o15499" />
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries many in several series, unequal, lanceolate (outer) to linear (inner), entire, apices sometimes expanded and fringed, not spine-tipped.</text>
      <biological_entity id="o15500" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character constraint="in series" constraintid="o15501" is_modifier="false" name="quantity" src="d0_s8" value="many" value_original="many" />
        <character is_modifier="false" name="size" notes="" src="d0_s8" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="linear" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15501" name="series" name_original="series" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o15502" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s8" value="expanded" value_original="expanded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="fringed" value_original="fringed" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s8" value="spine-tipped" value_original="spine-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles turbinate, bearing elongate subulate scales.</text>
      <biological_entity id="o15503" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="turbinate" value_original="turbinate" />
      </biological_entity>
      <biological_entity id="o15504" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="shape" src="d0_s9" value="subulate" value_original="subulate" />
      </biological_entity>
      <relation from="o15503" id="r1418" name="bearing" negation="false" src="d0_s9" to="o15504" />
    </statement>
    <statement id="d0_s10">
      <text>Florets 1 per primary head;</text>
      <biological_entity id="o15505" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character constraint="per primary head" constraintid="o15506" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="primary" id="o15506" name="head" name_original="head" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>corollas white to greenish, blue-gray, blue, or purple, tubes elongate, throats very short, lobes linear;</text>
      <biological_entity id="o15507" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="greenish blue-gray blue or purple" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="greenish blue-gray blue or purple" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="greenish blue-gray blue or purple" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="greenish blue-gray blue or purple" />
      </biological_entity>
      <biological_entity id="o15508" name="tube" name_original="tubes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o15509" name="throat" name_original="throats" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o15510" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anther bases sharply tailed, apical appendages narrowly triangular, acute;</text>
      <biological_entity constraint="anther" id="o15511" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s12" value="tailed" value_original="tailed" />
      </biological_entity>
      <biological_entity constraint="apical" id="o15512" name="appendage" name_original="appendages" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style-branches: fused portions with minutely hairy rings, distinct portions divergent, linear-oblong.</text>
      <biological_entity id="o15513" name="style-branch" name_original="style-branches" src="d0_s13" type="structure" />
      <biological_entity id="o15514" name="portion" name_original="portions" src="d0_s13" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s13" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o15515" name="ring" name_original="rings" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="minutely" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o15516" name="portion" name_original="portions" src="d0_s13" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="arrangement" src="d0_s13" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="shape" src="d0_s13" value="linear-oblong" value_original="linear-oblong" />
      </biological_entity>
      <relation from="o15514" id="r1419" name="with" negation="false" src="d0_s13" to="o15515" />
    </statement>
    <statement id="d0_s14">
      <text>Cypselae ± cylindric, 4-angled, apices ± truncate, without crowns, densely villous with long, stiff, appressed or ascending, multicellular hairs, attachment scars basal;</text>
      <biological_entity id="o15517" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s14" value="4-angled" value_original="4-angled" />
      </biological_entity>
      <biological_entity id="o15518" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s14" value="truncate" value_original="truncate" />
        <character constraint="with hairs" constraintid="o15520" is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s14" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o15519" name="crown" name_original="crowns" src="d0_s14" type="structure" />
      <biological_entity id="o15520" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s14" value="long" value_original="long" />
        <character is_modifier="true" name="fragility" src="d0_s14" value="stiff" value_original="stiff" />
        <character is_modifier="true" name="orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
        <character is_modifier="true" name="architecture" src="d0_s14" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <biological_entity constraint="attachment" id="o15521" name="scar" name_original="scars" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="basal" value_original="basal" />
      </biological_entity>
      <relation from="o15518" id="r1420" name="without" negation="false" src="d0_s14" to="o15519" />
    </statement>
    <statement id="d0_s15">
      <text>pappi of many, short, ± connate [or distinct] scales.</text>
      <biological_entity id="o15523" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="many" value_original="many" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s15" value="short" value_original="short" />
        <character is_modifier="true" modifier="more or less" name="fusion" src="d0_s15" value="connate" value_original="connate" />
      </biological_entity>
      <relation from="o15522" id="r1421" name="consist_of" negation="false" src="d0_s15" to="o15523" />
    </statement>
    <statement id="d0_s16">
      <text>x = 13, 14, 15, 16.</text>
      <biological_entity id="o15522" name="pappus" name_original="pappi" src="d0_s15" type="structure" />
      <biological_entity constraint="x" id="o15524" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="13" value_original="13" />
        <character name="quantity" src="d0_s16" value="14" value_original="14" />
        <character name="quantity" src="d0_s16" value="15" value_original="15" />
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <other_name type="common_name">Globe thistle</other_name>
  <discussion>Species ca. 120 (3 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lobes of leaf blades linear or narrowly oblong</description>
      <determination>3 Echinops ritro</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Lobes of leaf blades lanceolate to triangular</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Adaxial leaf faces glandular</description>
      <determination>1 Echinops sphaerocephalus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Adaxial leaf faces glabrous or sparsely strigose</description>
      <determination>2 Echinops exaltatus</determination>
    </key_statement>
  </key>
</bio:treatment>