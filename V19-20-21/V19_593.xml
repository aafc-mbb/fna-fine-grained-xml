<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">370</other_info_on_meta>
    <other_info_on_meta type="treatment_page">371</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="D. Don" date="1829" rank="genus">lygodesmia</taxon_name>
    <taxon_name authority="(Nuttall) Torrey &amp; A. Gray" date="1843" rank="species">grandiflora</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 485. 1843</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus lygodesmia;species grandiflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067132</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erythremia</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">grandiflora</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 445. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erythremia;species grandiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials 5–25 (–60) cm;</text>
      <biological_entity id="o11377" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots or rhizomes vertical, deep.</text>
      <biological_entity id="o11378" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="depth" src="d0_s1" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o11379" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="vertical" value_original="vertical" />
        <character is_modifier="false" name="depth" src="d0_s1" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–5, erect or ascending, green, simple or branched from bases, obscurely striate (glabrous, puberulent or scabrous).</text>
      <biological_entity id="o11380" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="5" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character constraint="from bases" constraintid="o11381" is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="obscurely" name="coloration_or_pubescence_or_relief" notes="" src="d0_s2" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity id="o11381" name="base" name_original="bases" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves (basal not forming rosettes, cauline present at flowering);</text>
      <biological_entity id="o11382" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>basal blades linear to subulate, 5–150 × 1–6 mm, margins entire;</text>
      <biological_entity constraint="basal" id="o11383" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="subulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="150" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11384" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline similar, sometimes reduced to scales distally.</text>
      <biological_entity constraint="cauline" id="o11385" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character constraint="to scales" constraintid="o11386" is_modifier="false" modifier="sometimes" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o11386" name="scale" name_original="scales" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Heads (1–30, showy) borne singly or in loose, corymbiform arrays.</text>
      <biological_entity id="o11387" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in loose , corymbiform arrays" />
      </biological_entity>
      <biological_entity id="o11388" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o11387" id="r1055" name="in" negation="false" src="d0_s6" to="o11388" />
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric, 15–25 × 6–8 mm, apices narrowed or spreading.</text>
      <biological_entity id="o11389" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s7" to="25" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11390" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calyculi of ca. 8, deltate to ovate bractlets 2–5 mm, margins ciliate (faces tomentulose).</text>
      <biological_entity id="o11391" name="calyculus" name_original="calyculi" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" value_original="8" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s8" to="ovate" />
      </biological_entity>
      <biological_entity id="o11392" name="bractlet" name_original="bractlets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11393" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries 5–12, linear, 15–24 mm, margins scarious, apices appendaged (faces glabrous or scabrous).</text>
      <biological_entity id="o11394" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="12" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="24" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11395" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="texture" src="d0_s9" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o11396" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="appendaged" value_original="appendaged" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 5–12;</text>
      <biological_entity id="o11397" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas 20–40 mm, lavender, pink, purple, rose, or white, ligules 5–10 mm wide.</text>
      <biological_entity id="o11398" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s11" to="40" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="rose" value_original="rose" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o11399" name="ligule" name_original="ligules" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae (subcylindric, obscurely 4–5-angled) 10–18 mm (faces smooth or rugose, sometimes sulcate);</text>
      <biological_entity id="o11400" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi 10–13 mm. 2n = 18.</text>
      <biological_entity id="o11401" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s13" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11402" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Idaho, N.Mex., Nev., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Largeflower skeletonplant</other_name>
  <discussion>Varieties 5 (5 in the flora).</discussion>
  <discussion>Lygodesmia grandiflora is recognized mainly by its relatively large corollas. Some variants were segregated as distinct species by A. S. Tomb; because of intermediates, putative hybrids, and associated identification problems, it is probably best to recognize these as varieties pending further investigation (A. Cronquist 1994; S. L. Welsh et al. 2003).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllaries 8–12; florets 8–12</description>
      <determination>2a Lygodesmia grandiflora var. grandiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllaries 5(–6); florets 5(–7)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas white (may turn pinkish when dry); stems (woody) branched from bases; leaves stiff, spreading; involucre apices spreading</description>
      <determination>2e Lygodesmia grandiflora var. entrada</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas lavender, pink, purple, rose, or white; stems simple or branched from bases or distally (if branched from bases, either leaves lax or plants from vicinity of Moab, Utah); involucre apices narrow</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems much branched from bases; proximal leaves narrow, linear-filiform, 1–3 mm wide, rigid</description>
      <determination>2d Lygodesmia grandiflora var. doloresensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems simple or sparsely branched from bases or distally; proximal leaves lanceolate to linear-subulate, (2–)3–6 mm wide, ± lax (widespread in southwestern states).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Distal leaves not reduced to scales (mostly 10+ mm); cypselae 10–13 mm, abaxial faces rugose, adaxial faces strongly sulcate</description>
      <determination>2b Lygodesmia grandiflora var. arizonica</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Distal leaves reduced to linear scales (mostly less than 10 mm); cypselae 13–19 mm, abaxial faces smooth, adaxial faces weakly sulcate</description>
      <determination>2c Lygodesmia grandiflora var. dianthopsis</determination>
    </key_statement>
  </key>
</bio:treatment>