<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">262</other_info_on_meta>
    <other_info_on_meta type="mention_page">267</other_info_on_meta>
    <other_info_on_meta type="treatment_page">333</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1982" rank="species">rybius</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>7: 457, fig. 1. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species rybius</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066670</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 15–35 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, primary rhizomes slender, with systems of lignescent, branched rhizomes and slender, herbaceous, scale-leaved stolons bearing terminal leaf tufts.</text>
      <biological_entity id="o8873" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="primary" id="o8874" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o8875" name="system" name_original="systems" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o8876" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="lignescent" value_original="lignescent" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o8877" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="scale-leaved" value_original="scale-leaved" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o8878" name="tuft" name_original="tufts" src="d0_s1" type="structure" constraint_original="terminal leaf" />
      <relation from="o8874" id="r801" name="with" negation="false" src="d0_s1" to="o8875" />
      <relation from="o8875" id="r802" name="part_of" negation="false" src="d0_s1" to="o8876" />
      <relation from="o8877" id="r803" name="bearing" negation="false" src="d0_s1" to="o8878" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, moderately to densely hirsute to hirtellous (hairs retrorsely spreading), usually eglandular, sometimes sparsely minutely glandular.</text>
      <biological_entity id="o8879" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="hirsute" modifier="moderately to densely; densely" name="pubescence" src="d0_s2" to="hirtellous" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes sparsely minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (and proximal cauline usually withering by flowering) and cauline;</text>
      <biological_entity id="o8880" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal and proximal cauline blades elliptic-ovate to spatulate-obovate, 18–135 × 6–27 mm, margins entire to mucronulate or shallowly serrate, faces short-strigose to hirsute or hirsuto-villous;</text>
      <biological_entity constraint="basal and proximal cauline" id="o8881" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="elliptic-ovate" name="shape" src="d0_s4" to="spatulate-obovate" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s4" to="135" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="27" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8882" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s4" to="mucronulate or shallowly serrate" />
      </biological_entity>
      <biological_entity id="o8883" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="short-strigose" name="pubescence" src="d0_s4" to="hirsute or hirsuto-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline blades becoming lanceolate and entire distal to midstems, nearly even-sized distally or mid largest (bases clasping to subclasping).</text>
      <biological_entity constraint="cauline" id="o8884" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="to midstems" constraintid="o8885" is_modifier="false" name="position_or_shape" src="d0_s5" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s5" value="mid" value_original="mid" />
        <character is_modifier="false" name="size" src="d0_s5" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity id="o8885" name="midstem" name_original="midstems" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="nearly; distally" name="size" notes="" src="d0_s5" value="equal-sized" value_original="even-sized" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–6 (from branches on distal 1/3–1/2 of stems).</text>
      <biological_entity id="o8886" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 6–8 × 9–15 mm.</text>
      <biological_entity id="o8887" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="8" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 3 (–4) series, moderately piloso-hirsute, usually eglandular, sometimes minutely glandular.</text>
      <biological_entity id="o8888" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="moderately" name="pubescence" notes="" src="d0_s8" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" modifier="sometimes minutely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o8889" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s8" to="4" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="3" value_original="3" />
      </biological_entity>
      <relation from="o8888" id="r804" name="in" negation="false" src="d0_s8" to="o8889" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 47–99;</text>
      <biological_entity id="o8890" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="47" name="quantity" src="d0_s9" to="99" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 11–20 mm, laminae coiling at tips, white, drying white or lilac-tinged.</text>
      <biological_entity id="o8891" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8892" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character constraint="at tips" constraintid="o8893" is_modifier="false" name="shape" src="d0_s10" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="lilac-tinged" value_original="lilac-tinged" />
      </biological_entity>
      <biological_entity id="o8893" name="tip" name_original="tips" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas 3.4–4.6 mm.</text>
      <biological_entity constraint="disc" id="o8894" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s11" to="4.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae obovate to 1.8–2.1 mm, 2 (–4) -nerved, faces sparsely strigose;</text>
      <biological_entity id="o8895" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s12" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2(-4)-nerved" value_original="2(-4)-nerved" />
      </biological_entity>
      <biological_entity id="o8896" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer of setae, inner of 20–32 bristles.</text>
      <biological_entity id="o8898" name="seta" name_original="setae" src="d0_s13" type="structure" />
      <biological_entity id="o8899" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s13" to="32" />
      </biological_entity>
      <relation from="o8897" id="r805" name="outer of" negation="false" src="d0_s13" to="o8898" />
      <relation from="o8897" id="r806" name="inner of" negation="false" src="d0_s13" to="o8899" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity id="o8897" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o8900" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, grassy forest openings, and disturbed areas, ponderosa pine, pine-oak-fir, fir, spruce-fir</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="grassy forest openings" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="ponderosa pine" />
        <character name="habitat" value="pine-oak-fir" />
        <character name="habitat" value="fir" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800–2800(–3300) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1800" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="3300" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>142.</number>
  <other_name type="common_name">Sacramento Mountain fleabane</other_name>
  
</bio:treatment>