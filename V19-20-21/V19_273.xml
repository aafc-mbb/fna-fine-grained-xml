<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">222</other_info_on_meta>
    <other_info_on_meta type="illustration_page">221</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cichorium</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">intybus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 813. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus cichorium;species intybus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200023652</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials (sometimes flowering first-year).</text>
      <biological_entity id="o4986" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: blades of basal 5–35+ × 1–8 (–12+) cm;</text>
      <biological_entity id="o4987" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o4988" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="35" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s1" to="12" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s1" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o4989" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <relation from="o4988" id="r484" name="part_of" negation="false" src="d0_s1" to="o4989" />
    </statement>
    <statement id="d0_s2">
      <text>cauline similar, smaller, narrower, distal mostly linear.</text>
      <biological_entity id="o4990" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="cauline" id="o4991" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="width" src="d0_s2" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4992" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles mostly 0–2 mm, some narrowly clavate, 12–45 (–85+) mm.</text>
      <biological_entity id="o4993" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="45" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="85" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s3" to="45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries: outer 5–6 lanceovate to lanceolate, 4–7 mm, basally cartilaginous, distally herbaceous, inner 8+ lance-linear to linear, 6–12 mm, herbaceous, all usually with some gland-tipped hairs 0.5–0.8 mm on margins near bases or on abaxial faces toward tips.</text>
      <biological_entity id="o4994" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure" />
      <biological_entity constraint="outer" id="o4995" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="6" />
        <character char_type="range_value" from="lanceovate" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="basally" name="pubescence_or_texture" src="d0_s4" value="cartilaginous" value_original="cartilaginous" />
        <character is_modifier="false" modifier="distally" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity constraint="inner" id="o4996" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s4" upper_restricted="false" />
        <character char_type="range_value" from="lance-linear" name="arrangement_or_course_or_shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o4997" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
        <character char_type="range_value" constraint="on margins near bases or on abaxial faces" constraintid="o4998" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4998" name="face" name_original="faces" src="d0_s4" type="structure" />
      <relation from="o4996" id="r485" modifier="usually" name="with" negation="false" src="d0_s4" to="o4997" />
    </statement>
    <statement id="d0_s5">
      <text>Cypselae 2–3 mm;</text>
      <biological_entity id="o4999" name="cypsela" name_original="cypselae" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>pappi 0.01–0.2 mm. 2n = 18.</text>
      <biological_entity id="o5000" name="pappus" name_original="pappi" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.01" from_unit="mm" name="some_measurement" src="d0_s6" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5001" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que., Sask.; Ark., Calif., Conn., Ill., Ind., Iowa, Kans., Maine, Mass., Mich., Mo., Nev., N.H., N.Y., N.C., Pa., R.I., Tex., Utah, Vt.; Europe; Asia; introduced also in Africa, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="also in Africa" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Chicory</other_name>
  <other_name type="common_name">chicorée</other_name>
  <discussion>Leaves of Cichorium intybus are sometimes used as salad greens; the roasted roots are sometimes ground and used as an addition to (or adulterant of) coffee.</discussion>
  
</bio:treatment>