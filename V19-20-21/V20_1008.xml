<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">440</other_info_on_meta>
    <other_info_on_meta type="treatment_page">441</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">isocoma</taxon_name>
    <taxon_name authority="(Greene) Greene" date="1894" rank="species">acradenia</taxon_name>
    <taxon_name authority="(Greene) G. L. Nesom" date="1991" rank="variety">bracteosa</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>70: 81. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus isocoma;species acradenia;variety bracteosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068533</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Isocoma</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">bracteosa</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. Bot. Observ. Crit.</publication_title>
      <place_in_publication>1: 170. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Isocoma;species bracteosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(Greene) S. F. Blake" date="unknown" rank="species">acradenius</taxon_name>
    <taxon_name authority="(Greene) H. M. Hall" date="unknown" rank="subspecies">bracteosus</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species acradenius;subspecies bracteosus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaf-blades mostly 50–150 mm, margins of proximal usually shallowly toothed, of distal entire or serrulate.</text>
      <biological_entity id="o23521" name="blade-leaf" name_original="leaf-blades" src="d0_s0" type="structure">
        <character char_type="range_value" from="50" from_unit="mm" name="distance" src="d0_s0" to="150" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23522" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s0" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o23523" name="toothed" name_original="toothed" src="d0_s0" type="structure">
        <character is_modifier="true" name="position" src="d0_s0" value="proximal" value_original="proximal" />
        <character is_modifier="true" modifier="usually shallowly" name="shape" src="d0_s0" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o23524" name="distal" name_original="distal" src="d0_s0" type="structure" />
      <relation from="o23522" id="r2176" name="part_of" negation="false" src="d0_s0" to="o23523" />
      <relation from="o23522" id="r2177" name="part_of" negation="false" src="d0_s0" to="o23524" />
    </statement>
    <statement id="d0_s1">
      <text>Involucres 7–8 mm.</text>
      <biological_entity id="o23525" name="involucre" name_original="involucres" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s1" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Phyllary apices spinulose-aristate.</text>
      <biological_entity constraint="phyllary" id="o23526" name="apex" name_original="apices" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="spinulose-aristate" value_original="spinulose-aristate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Florets 20–27.2n = 12.</text>
      <biological_entity id="o23527" name="floret" name_original="florets" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s3" to="27" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23528" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Salt flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="salt flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1b.</number>
  <discussion>In the limited zone of contact between vars. bracteosa and acradenia, intermediates are rare. The two differ in chromosome number and significantly in habitat (altitudes are essentially non-overlapping). Field studies may show them to warrant distinction at specific rank.</discussion>
  
</bio:treatment>