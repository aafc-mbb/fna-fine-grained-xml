<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">222</other_info_on_meta>
    <other_info_on_meta type="treatment_page">235</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="subtribe">pectidinae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">TAGETES</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 887. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 378. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe pectidinae;genus TAGETES</taxon_hierarchy>
    <other_info_on_name type="etymology">Etymology unknown; possibly from New Latin Tages, an Etruscan god</other_info_on_name>
    <other_info_on_name type="fna_id">132217</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, perennials, subshrubs, or shrubs [perennials], mostly 10–80 (–200) cm.</text>
      <biological_entity id="o18306" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o18308" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" modifier="mostly" name="atypical_some_measurement" notes="" src="d0_s0" to="200" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" modifier="mostly" name="some_measurement" notes="" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched distally or ± throughout.</text>
      <biological_entity id="o18310" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally; distally; more or less throughout; throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>mostly opposite (distal sometimes alternate);</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o18311" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades mostly lanceolate to oblanceolate overall, usually 1–3-pinnately lobed or pinnatisect, ultimate margins toothed or entire, faces glabrous or hairy (oil-glands scattered and/or submarginal).</text>
      <biological_entity id="o18312" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="mostly lanceolate" name="shape" src="d0_s5" to="oblanceolate" />
        <character is_modifier="false" modifier="usually 1-3-pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatisect" value_original="pinnatisect" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o18313" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o18314" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate or discoid, borne singly or in ± corymbiform arrays.</text>
      <biological_entity id="o18315" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="discoid" value_original="discoid" />
        <character constraint="in more or less corymbiform arrays" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in more or less corymbiform arrays" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Calyculi 0.</text>
      <biological_entity id="o18316" name="calyculus" name_original="calyculi" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres narrowly cylindric or fusiform to turbinate or broadly campanulate, 1–12+ mm diam.</text>
      <biological_entity id="o18317" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s8" to="turbinate or broadly campanulate" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s8" to="12" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries persistent, 3–21+ in 1–2 series (connate to 7/8+ their lengths, usually streaked and/or dotted with oil-glands).</text>
      <biological_entity id="o18318" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o18319" from="3" name="quantity" src="d0_s9" to="21" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o18319" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles convex to conic, smooth or finely pitted, epaleate.</text>
      <biological_entity id="o18320" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s10" to="conic" />
        <character is_modifier="false" name="relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s10" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 0 or 1–8 (–13+) (to 100+ in “double” cultivars), pistillate, fertile (except “double” cultivars);</text>
      <biological_entity id="o18321" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="13" upper_restricted="false" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas yellow or orange, redbrown (with or without yellow/orange), or white.</text>
      <biological_entity id="o18322" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 6–120+, bisexual, fertile;</text>
      <biological_entity id="o18323" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s13" to="120" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas greenish yellow to orange, sometimes tipped with red or redbrown, tubes much longer than or about equaling funnelform throats, lobes 5, deltate to lance-linear (equal or 2 sinuses deeper than others).</text>
      <biological_entity id="o18324" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="greenish yellow" name="coloration" src="d0_s14" to="orange" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s14" value="tipped" value_original="tipped" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="redbrown" value_original="redbrown" />
      </biological_entity>
      <biological_entity id="o18325" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than or about equaling funnelform throats" constraintid="o18326" is_modifier="false" name="length_or_size" src="d0_s14" value="much longer" value_original="much longer" />
      </biological_entity>
      <biological_entity id="o18326" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o18327" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character char_type="range_value" from="deltate" name="shape" src="d0_s14" to="lance-linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae narrowly obpyramidal or fusiform-terete, sometimes weakly flattened, glabrous or hairy;</text>
      <biological_entity id="o18328" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="obpyramidal" value_original="obpyramidal" />
        <character is_modifier="false" name="shape" src="d0_s15" value="fusiform-terete" value_original="fusiform-terete" />
        <character is_modifier="false" modifier="sometimes weakly" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>pappi persistent, of 2–5 (–10) dissimilar, distinct or connate scales in ± 1 series: 0–5+ oblong to lanceolate, erose-truncate or laciniate plus 0–2 (–5) longer, subulate to aristate.</text>
      <biological_entity id="o18330" name="scale" name_original="scales" src="d0_s16" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s16" to="10" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s16" to="5" />
        <character is_modifier="true" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
        <character is_modifier="true" name="fusion" src="d0_s16" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity id="o18331" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s16" value="1" value_original="1" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s16" to="5" upper_restricted="false" />
      </biological_entity>
      <relation from="o18329" id="r1250" name="consist_of" negation="false" src="d0_s16" to="o18330" />
      <relation from="o18330" id="r1251" name="in" negation="false" src="d0_s16" to="o18331" />
    </statement>
    <statement id="d0_s17">
      <text>x = 12.</text>
      <biological_entity id="o18329" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s16" to="lanceolate erose-truncate or laciniate" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s16" to="lanceolate erose-truncate or laciniate" />
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="5" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s16" to="2" />
        <character is_modifier="false" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s16" to="aristate" />
      </biological_entity>
      <biological_entity constraint="x" id="o18332" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical and warm-temperate America, especially Mexico; introduced in Old World.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical and warm-temperate America" establishment_means="native" />
        <character name="distribution" value="especially Mexico" establishment_means="native" />
        <character name="distribution" value="in Old World" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>321.</number>
  <other_name type="common_name">Marigold</other_name>
  <discussion>Species 40+ (4 in the flora).</discussion>
  <discussion>Some Tagetes species (e.g., T. erecta) produce nematicidal thiophenes in their roots and have been shown to be effective controls for nematodes in diverse crops (cf., http://www.ncagr.com/agronomi/nnote1.htm).</discussion>
  <discussion>Reports of “Tagetes minima L.” for Pennsylvania (cf. http://plants.usda.gov) are evidently rooted in an error for T. minuta. Report of T. pusilla Kunth (= T. filifolia Lagasca) for Maryland (http://plants.usda.gov) was not verified for this treatment.</discussion>
  <references>
    <reference>Neher, R. T. 1966. Monograph of the Genus Tagetes (Compositae). Ph.D. thesis. Indiana University.</reference>
    <reference>Soule, J. A. 1993. Systematics of Tagetes (Asteraceae–Tageteae) (Mexico, Argentina). Ph.D. thesis. University of Texas.</reference>
    <reference>Rydberg, P. A. 1915. Tagetes. In: N. L. Britton et al., eds. 1905+. North American Flora.... 47+ vols. New York. Vol. 34, pp. 148–159.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf lobes (or simple blades) linear to filiform, 10–25(–35+) × 0.5–1.5 mm</description>
      <determination>1 Tagetes micrantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf lobes lance-elliptic or lanceolate to lance-linear, 12–25(–50+) × (2–)4–8(–12+) mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perennials, subshrubs, or shrubs</description>
      <determination>2 Tagetes lemmonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Annuals</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Heads in ± corymbiform clusters; peduncles 1–5+ mm; involucres 7–10+ × 1.5–3 mm</description>
      <determination>3 Tagetes minuta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Heads borne ± singly; peduncles 30–100(–150+) mm; involucres 10–22 × (3–)5–12+ mm</description>
      <determination>4 Tagetes erecta</determination>
    </key_statement>
  </key>
</bio:treatment>