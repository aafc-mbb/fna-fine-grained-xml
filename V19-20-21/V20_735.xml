<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="treatment_page">326</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">uniflorus</taxon_name>
    <taxon_name authority="(J. Vahl) B. Boivin" date="1972" rank="variety">eriocephalus</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>23: 49. 1972</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species uniflorus;variety eriocephalus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068378</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="J. Vahl" date="unknown" rank="species">eriocephalus</taxon_name>
    <place_of_publication>
      <publication_title>in G. C. Oeder et al., Fl. Dan.</publication_title>
      <place_in_publication>13(39): 6, plate 2299. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Erigeron;species eriocephalus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">unalaschkensis</taxon_name>
    <taxon_name authority="(J. Vahl) A. E. Porsild" date="unknown" rank="variety">eriocephalus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species unalaschkensis;variety eriocephalus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">uniflorus</taxon_name>
    <taxon_name authority="(J. Vahl) Cronquist" date="unknown" rank="subspecies">eriocephalus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species uniflorus;subspecies eriocephalus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 3–25 (–35) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, caudices crownlike, sometimes branched.</text>
      <biological_entity id="o24337" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o24338" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="crownlike" value_original="crownlike" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending, sparsely to densely villous or tomentose, especially distally (hair cross-walls often purplish black), minutely glandular.</text>
      <biological_entity id="o24339" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sparsely to densely; densely" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="especially distally; distally; minutely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o24340" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal oblanceolate to subspatulate, 20–60 (–100) × 3–6 (–10) mm, cauline gradually reduced distally or little reduced on proximal 1/2, rarely all bractlike, margins entire, faces sparsely villosulous to glabrate, sometimes sparsely minutely glandular.</text>
      <biological_entity constraint="basal" id="o24341" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="subspatulate" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o24342" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually; distally; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character constraint="on proximal 1/2" constraintid="o24343" is_modifier="false" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="rarely" name="shape" notes="" src="d0_s4" value="bractlike" value_original="bractlike" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o24343" name="1/2" name_original="1/2" src="d0_s4" type="structure" />
      <biological_entity id="o24344" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24345" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="sparsely villosulous" name="pubescence" src="d0_s4" to="glabrate" />
        <character is_modifier="false" modifier="sometimes sparsely minutely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1.</text>
      <biological_entity id="o24346" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 8–10 (–11) × (10–) 15–20 (–30) mm.</text>
      <biological_entity id="o24347" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_width" src="d0_s6" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="30" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2 (–3) series (reddish purple), densely lanate (hairs tangled, soft), glandular apically.</text>
      <biological_entity id="o24348" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s7" value="lanate" value_original="lanate" />
        <character is_modifier="false" modifier="apically" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o24349" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o24348" id="r2242" name="in" negation="false" src="d0_s7" to="o24349" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 100–200;</text>
      <biological_entity id="o24350" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="100" name="quantity" src="d0_s8" to="200" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas purplish to pink or white, 3–6 (–8) mm, laminae erect (filiform, 0.3–1 mm wide), not coiling or reflexing.</text>
      <biological_entity id="o24351" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="purplish" name="coloration" src="d0_s9" to="pink or white" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24352" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3.8–5 mm.</text>
      <biological_entity constraint="disc" id="o24353" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.8–2.2 mm, 2-nerved, faces finely strigoso-hirsute;</text>
      <biological_entity id="o24354" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o24355" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s11" value="strigoso-hirsute" value_original="strigoso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae (inconspicuous), inner of 20–30 (accrescent, surpassing disc corollas and involucres in fruit) bristles.</text>
      <biological_entity id="o24357" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o24358" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s12" to="30" />
      </biological_entity>
      <relation from="o24356" id="r2243" name="outer of" negation="false" src="d0_s12" to="o24357" />
      <relation from="o24356" id="r2244" name="inner of" negation="false" src="d0_s12" to="o24358" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o24356" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o24359" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jun–)Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>River terraces, snow beds, rocky hills, fell fields, shaley or gravelly slopes, well drained sand or silt, streambeds, peat deposits, herbmats proximal to snowbanks, dryas tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="river terraces" />
        <character name="habitat" value="snow beds" />
        <character name="habitat" value="rocky hills" />
        <character name="habitat" value="fields" modifier="fell" />
        <character name="habitat" value="shaley" />
        <character name="habitat" value="gravelly slopes" />
        <character name="habitat" value="sand" modifier="well drained" />
        <character name="habitat" value="silt" />
        <character name="habitat" value="streambeds" />
        <character name="habitat" value="peat deposits" />
        <character name="habitat" value="snowbanks" modifier="herbmats" />
        <character name="habitat" value="dryas" />
        <character name="habitat" value="proximal to snowbanks" />
        <character name="habitat" value="dryas tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; B.C., Nfld. and Labr. (Labr.), N.W.T., Nunavut, Que., Yukon; Alaska; n Europe; Asia (Russia, Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="n Europe" establishment_means="native" />
        <character name="distribution" value="Asia (Russia)" establishment_means="native" />
        <character name="distribution" value="Asia (Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>127a.</number>
  <other_name type="common_name">Vergerette à capitules laineux</other_name>
  <discussion>Evidence is not at hand that might clarify the biological relationship of and appropriate taxonomic rank for var. eriocephalus. Variety uniflorus occurs in the mountains of Europe and Caucasus, mostly south of var. eriocephalus; apparently the two intergrade [see detailed comments by S. G. Aiken et al. (http://www.mun.ca/biology/delta/arctif)].</discussion>
  
</bio:treatment>