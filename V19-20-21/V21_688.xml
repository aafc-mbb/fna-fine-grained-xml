<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="mention_page">283</other_info_on_meta>
    <other_info_on_meta type="treatment_page">282</other_info_on_meta>
    <other_info_on_meta type="illustrator">Bee F. Gunn</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">deinandra</taxon_name>
    <taxon_name authority="(D. D. Keck) B. G. Baldwin" date="1999" rank="species">arida</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 467. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus deinandra;species arida</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066461</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="D. D. Keck" date="unknown" rank="species">arida</taxon_name>
    <place_of_publication>
      <publication_title>Aliso</publication_title>
      <place_in_publication>4: 109. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemizonia;species arida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 20–80 cm.</text>
      <biological_entity id="o13816" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± solid.</text>
      <biological_entity id="o13817" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal blades toothed to entire, faces hispid-hirsute and stipitate-glandular.</text>
      <biological_entity id="o13818" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o13819" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="toothed" name="shape" src="d0_s2" to="entire" />
      </biological_entity>
      <biological_entity id="o13820" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispid-hirsute" value_original="hispid-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in paniculiform arrays.</text>
      <biological_entity id="o13821" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o13822" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o13821" id="r952" name="in" negation="false" src="d0_s3" to="o13822" />
    </statement>
    <statement id="d0_s4">
      <text>Bracts subtending heads usually not overlapping each involucre.</text>
      <biological_entity id="o13823" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually not" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o13824" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o13825" name="involucre" name_original="involucre" src="d0_s4" type="structure" />
      <relation from="o13823" id="r953" name="subtending" negation="false" src="d0_s4" to="o13824" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries ± evenly stipitate-glandular, including margins and apices, with nonglandular, non-pustule-based hairs as well.</text>
      <biological_entity id="o13826" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less evenly" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o13827" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o13828" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <biological_entity id="o13829" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="non-pustule-based" value_original="non-pustule-based" />
      </biological_entity>
      <relation from="o13826" id="r954" name="including" negation="false" src="d0_s5" to="o13827" />
      <relation from="o13826" id="r955" name="including" negation="false" src="d0_s5" to="o13828" />
      <relation from="o13826" id="r956" modifier="well" name="with" negation="false" src="d0_s5" to="o13829" />
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series.</text>
      <biological_entity id="o13830" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity id="o13831" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o13830" id="r957" name="in" negation="false" src="d0_s6" to="o13831" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (4–) 8 (–10);</text>
      <biological_entity id="o13832" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s7" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="10" />
        <character name="quantity" src="d0_s7" value="8" value_original="8" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae deep yellow, 5–7 mm.</text>
      <biological_entity id="o13833" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 17–25, all or mostly functionally staminate;</text>
      <biological_entity id="o13834" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="17" name="quantity" src="d0_s9" to="25" />
        <character is_modifier="false" modifier="mostly functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers yellow or brownish.</text>
      <biological_entity id="o13835" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pappi usually 0, rarely of 1–5 linear to setiform scales 0.1–0.6 mm. 2n = 24.</text>
      <biological_entity id="o13836" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" modifier="of" name="quantity" src="d0_s11" to="5" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="setiform" />
      </biological_entity>
      <biological_entity id="o13837" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13838" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Washes, edges of springs and seeps, and adjacent slopes, cliffs, or ledges, often in ± alkaline, sandy, gravelly, or clayey soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="washes" />
        <character name="habitat" value="edges" constraint="of springs and seeps" />
        <character name="habitat" value="springs" />
        <character name="habitat" value="seeps" />
        <character name="habitat" value="adjacent slopes" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="± alkaline" modifier="often in" />
        <character name="habitat" value="gravelly" modifier="sandy" />
        <character name="habitat" value="clayey soils" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Deinandra arida is known only from Red Rock and Last Chance canyons and associated tributaries in the El Paso Mountains, western Mojave Desert. Depauperate specimens resemble D. kelloggii, which also has been documented from the Red Rock Canyon area.</discussion>
  
</bio:treatment>