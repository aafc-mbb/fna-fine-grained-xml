<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">67</other_info_on_meta>
    <other_info_on_meta type="treatment_page">127</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="Strother" date="1991" rank="genus">JEFEA</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>33: 22, fig. 2. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus JEFEA</taxon_hierarchy>
    <other_info_on_name type="etymology">Spanish jefe, chief; for Billie Lee Turner, b. 1925, Texan, botanist</other_info_on_name>
    <other_info_on_name type="fna_id">116784</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Subshrubs or shrubs, 10–100 [–200+] cm.</text>
      <biological_entity id="o23534" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="200" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, branched ± throughout.</text>
      <biological_entity id="o23536" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="more or less throughout; throughout" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
    </statement>
    <statement id="d0_s3">
      <text>opposite (proximal) or alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o23537" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (usually 3-nerved) deltate, lanceolate, oblanceolate, or suborbiculate, bases cuneate to truncate, margins entire or lobed [toothed], faces scabrellous to hispid and glanddotted [densely woolly abaxially].</text>
      <biological_entity id="o23538" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
      </biological_entity>
      <biological_entity id="o23539" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate to truncate" value_original="cuneate to truncate" />
      </biological_entity>
      <biological_entity id="o23540" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o23541" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabrellous" value_original="scabrellous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, borne singly.</text>
      <biological_entity id="o23542" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate to hemispheric, 6–10+ mm diam.</text>
      <biological_entity id="o23543" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s7" to="hemispheric" />
        <character char_type="range_value" from="6" from_unit="mm" name="diameter" src="d0_s7" to="10" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, [22–] 26–38+ in 3–4+ series (outer 2–6+ spreading, similar to foliage in shape, texture, and indument, inner appressed, more papery to scarious or membranous).</text>
      <biological_entity id="o23544" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="22" name="atypical_quantity" src="d0_s8" to="26" to_inclusive="false" />
        <character char_type="range_value" constraint="in series" constraintid="o23545" from="26" name="quantity" src="d0_s8" to="38" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o23545" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles convex to conic, paleate (paleae persistent, lance-linear, conduplicate).</text>
      <biological_entity id="o23546" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s9" to="conic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 5–13 [–20], pistillate, fertile;</text>
      <biological_entity id="o23547" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="20" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="13" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow to orange.</text>
      <biological_entity id="o23548" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 30–60 [–100+], bisexual, fertile;</text>
      <biological_entity id="o23549" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="100" upper_restricted="false" />
        <character char_type="range_value" from="30" name="quantity" src="d0_s12" to="60" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow to orange, tubes shorter than or about equaling funnelform throats, lobes 5, deltate.</text>
      <biological_entity id="o23550" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s13" to="orange" />
      </biological_entity>
      <biological_entity id="o23551" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than or about equaling funnelform throats" constraintid="o23552" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o23552" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o23553" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltate" value_original="deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae weakly obcompressed, 3-angled (peripheral) or strongly compressed (inner) and oblanceolate to rounded-cuneate, some or all winged;</text>
      <biological_entity id="o23554" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s14" value="obcompressed" value_original="obcompressed" />
        <character is_modifier="false" name="shape" src="d0_s14" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s14" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s14" to="rounded-cuneate" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi fragile or persistent, of 2–3 subulate scales or awns plus 2–8+ shorter, distinct or basally connate, erose or lacerate scales (often with an additional seta on inner shoulder of each cypsela).</text>
      <biological_entity id="o23556" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s15" to="3" />
        <character is_modifier="true" name="shape" src="d0_s15" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity id="o23557" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s15" to="8" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o23558" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <relation from="o23555" id="r1614" name="consist_of" negation="false" src="d0_s15" to="o23556" />
      <relation from="o23555" id="r1615" name="consist_of" negation="false" src="d0_s15" to="o23557" />
    </statement>
    <statement id="d0_s16">
      <text>x = 14.</text>
      <biological_entity id="o23555" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s15" value="fragile" value_original="fragile" />
        <character is_modifier="false" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s15" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity constraint="x" id="o23559" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, Mexico, Central America (Guatemala).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America (Guatemala)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>288.</number>
  <discussion>Species 5 (1 in the flora).</discussion>
  
</bio:treatment>