<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">259</other_info_on_meta>
    <other_info_on_meta type="mention_page">266</other_info_on_meta>
    <other_info_on_meta type="treatment_page">277</other_info_on_meta>
    <other_info_on_meta type="illustration_page">277</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="species">hyssopifolius</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 123. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species hyssopifolius</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066614</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hyssopifolius</taxon_name>
    <taxon_name authority="Victorin &amp; J. Rousseau" date="unknown" rank="variety">anticostensis</taxon_name>
    <taxon_hierarchy>genus Erigeron;species hyssopifolius;variety anticostensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hyssopifolius</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">villicaulis</taxon_name>
    <taxon_hierarchy>genus Erigeron;species hyssopifolius;variety villicaulis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–35 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, caudices sometimes branched.</text>
      <biological_entity id="o19642" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o19643" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, sparsely and loosely strigose to strigoso-villous, eglandular.</text>
      <biological_entity id="o19644" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="loosely strigose" modifier="sparsely" name="pubescence" src="d0_s2" to="strigoso-villous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline much reduced or present as scales;</text>
      <biological_entity id="o19645" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="much" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character constraint="as scales" constraintid="o19646" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o19646" name="scale" name_original="scales" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>cauline blades linear to linear-oblong or oblong-lanceolate, 10–30 × 1–5 mm, largest at midstem, then relatively even-sized to peduncles, margins entire, sometimes prominently ciliate, faces glabrous or sparsely villous.</text>
      <biological_entity id="o19647" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o19648" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-oblong or oblong-lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character constraint="at midstem" constraintid="o19649" is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity id="o19649" name="midstem" name_original="midstem" src="d0_s4" type="structure">
        <character constraint="to peduncles" constraintid="o19650" is_modifier="false" modifier="relatively" name="size" notes="" src="d0_s4" value="equal-sized" value_original="even-sized" />
      </biological_entity>
      <biological_entity id="o19650" name="peduncle" name_original="peduncles" src="d0_s4" type="structure" />
      <biological_entity id="o19651" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes prominently" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o19652" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1 (–5).</text>
      <biological_entity id="o19653" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="5" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 4–6 × 6–12 mm.</text>
      <biological_entity id="o19654" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series, loosely and sparsely strigose, eglandular.</text>
      <biological_entity id="o19655" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="loosely; sparsely" name="pubescence" notes="" src="d0_s7" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o19656" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o19655" id="r1822" name="in" negation="false" src="d0_s7" to="o19656" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 20–50;</text>
      <biological_entity id="o19657" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s8" to="50" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, less commonly pinkish, or aging pinkish, 4–8 mm, laminae not coiling or reflexing.</text>
      <biological_entity id="o19658" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" modifier="less commonly" name="coloration" src="d0_s9" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" modifier="less commonly" name="coloration" src="d0_s9" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pinkish" value_original="pinkish" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19659" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 3–4.6 mm.</text>
      <biological_entity constraint="disc" id="o19660" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.3–1.6 mm, 5–6-nerved, faces glabrous;</text>
      <biological_entity id="o19661" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s11" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-6-nerved" value_original="5-6-nerved" />
      </biological_entity>
      <biological_entity id="o19662" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of 25–35 bristles.</text>
      <biological_entity id="o19664" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o19665" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="25" is_modifier="true" name="quantity" src="d0_s12" to="35" />
      </biological_entity>
      <relation from="o19663" id="r1823" name="outer of" negation="false" src="d0_s12" to="o19664" />
      <relation from="o19663" id="r1824" name="inner of" negation="false" src="d0_s12" to="o19665" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 18.</text>
      <biological_entity id="o19663" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o19666" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open woods, river gravel, rock ledges and crevices, gravel barrens, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open woods" />
        <character name="habitat" value="river gravel" />
        <character name="habitat" value="rock ledges" />
        <character name="habitat" value="crevices" />
        <character name="habitat" value="gravel barrens" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Nunavut, Ont., Que., Sask., Yukon; Maine, Mich., N.H., N.Y., Vt.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="past_name">hyssopifolium</other_name>
  <other_name type="common_name">Hyssop-leaf fleabane</other_name>
  <other_name type="common_name">vergerette à feuilles d’hysope</other_name>
  <discussion>Variety villicaulis, from Anticosti Island (Quebec) and Newfoundland, has been distinguished on the basis of its relatively shorter stature (mostly 3–15 cm) and greater ratio of peduncle to leafy-stem lengths (stems and peduncles as long as or longer than the leafy parts of stems); the distinction appears to be arbitrary.</discussion>
  
</bio:treatment>