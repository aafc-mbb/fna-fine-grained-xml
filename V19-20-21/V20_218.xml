<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="mention_page">116</other_info_on_meta>
    <other_info_on_meta type="treatment_page">113</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">solidago</taxon_name>
    <taxon_name authority="(Rydberg) Semple" date="2003" rank="subsection">humiles</taxon_name>
    <taxon_name authority="Kunth in A. von Humboldt et al." date="1820" rank="species">simplex</taxon_name>
    <place_of_publication>
      <publication_title>in A. von Humboldt et al., Nov. Gen. Sp.</publication_title>
      <place_in_publication>4(fol.): 81. 1818</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <place_in_publication>4(qto.): 103. 1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section solidago;subsection humiles;species simplex</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067572</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–80 cm;</text>
      <biological_entity id="o22473" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudices branching.</text>
      <biological_entity id="o22474" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1–10+, ascending to erect, proximally glabrous, strigose in arrays.</text>
      <biological_entity id="o22475" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" to="10" upper_restricted="false" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="in arrays" constraintid="o22476" is_modifier="false" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o22476" name="array" name_original="arrays" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline petiolate, blades narrowly oblanceolate, (20–) 50–100 (–160) × (2–) 5–16 (–56) mm, margins serrate to crenate, apices acute to obtuse, glabrous;</text>
      <biological_entity id="o22477" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o22478" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_length" src="d0_s3" to="50" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="160" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s3" to="100" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s3" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="56" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22479" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="serrate" name="shape" src="d0_s3" to="crenate" />
      </biological_entity>
      <biological_entity id="o22480" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>mid and distal sessile, similar, blades lanceolate to linear, 12–45 × 2–19 mm, reduced distally, margins entire to sparsely serrate, sometimes resinous.</text>
      <biological_entity id="o22481" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="mid and distal" id="o22482" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o22483" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s4" to="45" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="19" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o22484" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="entire" name="architecture_or_shape" src="d0_s4" to="sparsely serrate" />
        <character is_modifier="false" modifier="sometimes" name="coating" src="d0_s4" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 3–150, not secund, in narrowly elongate, paniculiform arrays, broadly so in robust plants (12.5–19 × 2.5–3 cm wide), consisting of short axillary and terminal racemiform clusters, proximal branches elongate in larger plants, branches glabrate to strigillose.</text>
      <biological_entity id="o22485" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="150" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o22486" name="plant" name_original="plants" src="d0_s5" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s5" value="robust" value_original="robust" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o22487" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="true" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o22488" name="plant" name_original="plants" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="true" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="arrangement" src="d0_s5" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o22489" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s5" to="strigillose" />
      </biological_entity>
      <relation from="o22485" id="r2073" modifier="in narrowly elongate , paniculiform arrays; broadly" name="in" negation="false" src="d0_s5" to="o22486" />
      <relation from="o22485" id="r2074" name="consisting of" negation="false" src="d0_s5" to="o22487" />
      <relation from="o22485" id="r2075" name="consisting of" negation="false" src="d0_s5" to="o22488" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 3.1–10.3 mm, glabrate to sparsely strigillose;</text>
      <biological_entity id="o22490" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.1" from_unit="mm" name="some_measurement" src="d0_s6" to="10.3" to_unit="mm" />
        <character char_type="range_value" from="glabrate" name="pubescence" src="d0_s6" to="sparsely strigillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracteoles few, linear.</text>
      <biological_entity id="o22491" name="bracteole" name_original="bracteoles" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres campanulate, 3–7 mm.</text>
      <biological_entity id="o22492" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries (in 3–4 series) strongly unequal, often resinous;</text>
      <biological_entity id="o22493" name="phyllarie" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s9" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="often" name="coating" src="d0_s9" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>outer ovate, acute, inner linear-oblong, obtuse.</text>
      <biological_entity constraint="outer" id="o22494" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="inner" id="o22495" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 7–16;</text>
      <biological_entity id="o22496" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s11" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>laminae 2–5 × 0.7–0.9 mm.</text>
      <biological_entity id="o22497" name="lamina" name_original="laminae" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 6–31;</text>
      <biological_entity id="o22498" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s13" to="31" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas 4–4.9 mm, lobes 0.6–1.3 (–2) mm.</text>
      <biological_entity id="o22499" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="4.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22500" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae narrowly obconic, 1.9–3.2 mm, sometimes with dark ridges, strigillose;</text>
      <biological_entity id="o22501" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="obconic" value_original="obconic" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s15" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s15" value="strigillose" value_original="strigillose" />
      </biological_entity>
      <biological_entity id="o22502" name="ridge" name_original="ridges" src="d0_s15" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s15" value="dark" value_original="dark" />
      </biological_entity>
      <relation from="o22501" id="r2076" modifier="sometimes" name="with" negation="false" src="d0_s15" to="o22502" />
    </statement>
    <statement id="d0_s16">
      <text>pappi 1.9–5.2 mm (bristles sometimes clavate).</text>
      <biological_entity id="o22503" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s16" to="5.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.W.T., Ont., Que., Sask., Yukon; Alaska, Ariz., Colo., Idaho, Ind., Maine, Mass., Md., Mich., Minn., Mont., N.Dak., N.H., N.Mex., N.Y., Oreg., Pa., S.Dak., Utah, Vt., W.Va., Wash., Wis., Wyo.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Sticky goldenrod</other_name>
  <discussion>Varieties 7 (7 in the flora).</discussion>
  <discussion>The somewhat viscid-resinous heads of Solidago simplex are its most distinctive feature, separating it from similar sympatric species. G. S. Ringius (1985) did a detailed multivariate analysis of the S. spathulata/S. simplex complex (the latter under the name S. glutinosa). The cytogeography of the species complex was presented by Ringius and J. C. Semple (1987). Neither study included data on the next three species occurring in the southeastern United States.</discussion>
  <discussion>The species is divided into two subspecies and seven varieties following G. S Ringius (1985) and J. C. Semple et al. (1999). Three varieties occur in the diploid transcontinental subsp. simplex: var. simplex, var. nana, and var. chlorolepis. Four varieties occur in the eastern North American tetraploid-hexaploid subsp. randii: var. monticola, var. gillmanii, var. ontarioensis, and var. racemosa. Except for var. simplex, varieties are restricted to different habitats in relatively limited ranges.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal and cauline leaves spatulate to orbiculate, apices obtuse to rounded; cypselae moderately to densely strigose (S. simplex subsp. simplex)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Basal and cauline leaves spatulate to obovate, apices usually acute; cypselae sparsely to moderately strigose (S. simplex subsp. randii)</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaves 5–16 (longest 60+ mm); w North America, disjunct along shores of upper Great Lakes and in s Quebec</description>
      <determination>1 Solidago simplex var. simplex</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaves 2–7 (longest to 60 mm); alpine slopes, w North America, Mt. Albert, Gaspé, Quebec</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Shortest phyllaries 1.4–2 mm; disc corollas 3.9–4.2 mm; serpentine soils, Mt. Albert, Gaspé, Quebec</description>
      <determination>5a.3 Solidago simplex var. chlorolepis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Shortest phyllaries 2.1–3.1 mm; disc corollas 4.2–5.4 mm; alpine slopes, Rocky Mountains, Cascade Mountains, Vancouver Island.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Basal leaves usually spatulate, rarely broadly elliptic, proximal blades gradually attenuated to winged petioles, margins weakly crenate; alpine slopes, Rocky Mountains</description>
      <determination>1 Solidago simplex var. simplex</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Basal leaves orbiculate to broadly elliptic, proximal blades abruptly attenuated to winged petioles, margins distinctly crenate; alpine slopes of Cascade Mountains, Vancouver Island</description>
      <determination>5a.2 Solidago simplex var. nana</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Proximal cauline leaves spatulate to obovate, (5–)10–42 mm wide; leaf margins often sharply serrate</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Proximal cauline leaves lanceolate to narrowly spatulate, 2–10(–21.5) mm wide; leaf margins entire or dentate, rarely serrate</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants (10–)18–53(–83) cm, arrays tending to be compact, virgate; proximal cauline leaves (5–)10–22(–31) mm wide, margins crenate to serrate; barren, rocky, non-alpine uplands; se Quebec, New England, New York</description>
      <determination>5b.1 Solidago simplex var. monticola</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants (20–)38–73(–84) cm, robust, arrays tending to be paniculiform; proximal cauline leaves (6–)7.5–24(–42) mm wide, margins often sharply serrate; sand dunes, Lake Michigan, n Lake Huron</description>
      <determination>5b.4 Solidago simplex var. gillmanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Cauline leaves (3–)12–33(–58); peduncle bracteoles 2+; calcareous rocky riverbanks, e Quebec s to West Virginia and Maryland</description>
      <determination>5b.2 Solidago simplex var. racemosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Cauline leaves (2–)4–13(–24); peduncle bracteoles 1–3(–4); rocky shores, Lake Superior, n Lake Huron</description>
      <determination>5b.3 Solidago simplex var. ontarioensis</determination>
    </key_statement>
  </key>
</bio:treatment>