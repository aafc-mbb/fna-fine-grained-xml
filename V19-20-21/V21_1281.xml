<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="treatment_page">504</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Elliott" date="1823" rank="genus">brickellia</taxon_name>
    <taxon_name authority="A. Gray" date="1885" rank="species">nevinii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>20: 297. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus brickellia;species nevinii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066266</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 30–80 cm.</text>
      <biological_entity id="o6955" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems much branched from near bases, tomentose, glanddotted.</text>
      <biological_entity id="o6956" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="from bases" constraintid="o6957" is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o6957" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves alternate;</text>
      <biological_entity id="o6958" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles 0–1 mm;</text>
      <biological_entity id="o6959" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades obscurely 3–5-nerved from bases, ovate to cordate, 7–11 × 3–8 mm, bases rounded to subcordate, margins dentate-serrate, apices obtuse, faces tomentose, glanddotted.</text>
      <biological_entity id="o6960" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character constraint="from bases" constraintid="o6961" is_modifier="false" modifier="obscurely" name="architecture" src="d0_s4" value="3-5-nerved" value_original="3-5-nerved" />
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s4" to="cordate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="11" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6961" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o6962" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="subcordate" />
      </biological_entity>
      <biological_entity id="o6963" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate-serrate" value_original="dentate-serrate" />
      </biological_entity>
      <biological_entity id="o6964" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o6965" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (terminating elongate lateral branches) in paniculiform arrays.</text>
      <biological_entity id="o6966" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o6967" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o6966" id="r503" name="in" negation="false" src="d0_s5" to="o6967" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 1–5 mm, tomentose.</text>
      <biological_entity id="o6968" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres cylindric to turbinate, 10–12 mm.</text>
      <biological_entity id="o6969" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="cylindric" name="shape" src="d0_s7" to="turbinate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 30–38 in 6–7 series, greenish, often red or purplish-tinged, 3–6 striate, unequal, (recurved, tomentose, glanddotted) margins scarious (ciliate, apices acute to acuminate);</text>
      <biological_entity id="o6970" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o6971" from="30" name="quantity" src="d0_s8" to="38" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purplish-tinged" value_original="purplish-tinged" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="6" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s8" value="striate" value_original="striate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o6971" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
      <biological_entity id="o6972" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>outer lanceovate, inner lanceolate.</text>
      <biological_entity constraint="outer" id="o6973" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceovate" value_original="lanceovate" />
      </biological_entity>
      <biological_entity constraint="inner" id="o6974" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Florets 20–24;</text>
      <biological_entity id="o6975" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s10" to="24" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas pale-yellow or cream, often purple-tinged, 6.2–7.5 mm.</text>
      <biological_entity id="o6976" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="cream" value_original="cream" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="purple-tinged" value_original="purple-tinged" />
        <character char_type="range_value" from="6.2" from_unit="mm" name="some_measurement" src="d0_s11" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 3.5–5 mm, hispidulous;</text>
      <biological_entity id="o6977" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi of 18–24 white or tawny, barbellate bristles.</text>
      <biological_entity id="o6979" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="18" is_modifier="true" name="quantity" src="d0_s13" to="24" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="tawny" value_original="tawny" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <relation from="o6978" id="r504" name="consist_of" negation="false" src="d0_s13" to="o6979" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity id="o6978" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o6980" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock crevices, rocky slopes, desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="past_name">nevini</other_name>
  <discussion>Brickellia nevinii appears to be an incarnate form of B. microphylla. The variability in indument often makes distinguishing these species problematic. Brickellia nevinii is sometimes reported to intergrade with B. californica.</discussion>
  
</bio:treatment>