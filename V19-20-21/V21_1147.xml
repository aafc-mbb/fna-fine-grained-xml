<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="treatment_page">457</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="H. Robinson" date="1978" rank="subtribe">marshalliinae</taxon_name>
    <taxon_name authority="Schreber" date="1791" rank="genus">marshallia</taxon_name>
    <taxon_name authority="(Walter) Trelease" date="1891" rank="species">trinervia</taxon_name>
    <place_of_publication>
      <publication_title>Rep. (Annual) Arkansas Geol. Surv.</publication_title>
      <place_in_publication>1888(4): 196. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe marshalliinae;genus marshallia;species trinervia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067167</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Athanasia</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">trinervia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>201. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Athanasia;species trinervia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 40–60 (–80) cm (rhizomatous).</text>
      <biological_entity id="o22110" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly cauline (not notably reduced distally);</text>
      <biological_entity id="o22111" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o22112" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>proximal petiolate;</text>
      <biological_entity constraint="proximal" id="o22113" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades strongly 3-nerved, ovate, 4–10 cm × 11–30 mm.</text>
      <biological_entity id="o22114" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s3" value="3-nerved" value_original="3-nerved" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="11" from_unit="mm" name="width" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 1–5, 20–33 mm diam.</text>
      <biological_entity id="o22115" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="5" />
        <character char_type="range_value" from="20" from_unit="mm" name="diameter" src="d0_s4" to="33" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 5–15+ cm.</text>
      <biological_entity id="o22116" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="15" to_unit="cm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 8–12 × 1.5–3 mm, apices acute.</text>
      <biological_entity id="o22117" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22118" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Paleae ± linear, apices acute.</text>
      <biological_entity id="o22119" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o22120" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Corollas pink, lobes 3.5–7.5 × 0.5–1 mm.</text>
      <biological_entity id="o22121" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
      </biological_entity>
      <biological_entity id="o22122" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s8" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pappi: scale margins serrulate.</text>
      <biological_entity id="o22123" name="pappus" name_original="pappi" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 18.</text>
      <biological_entity constraint="scale" id="o22124" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22125" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Understories, mixed hardwood forests, along streams, slightly disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="understories" />
        <character name="habitat" value="mixed hardwood forests" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="disturbed sites" modifier="slightly" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ga., La., Miss., N.C., S.C., Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>