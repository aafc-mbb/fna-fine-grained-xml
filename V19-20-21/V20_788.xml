<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">262</other_info_on_meta>
    <other_info_on_meta type="mention_page">263</other_info_on_meta>
    <other_info_on_meta type="mention_page">269</other_info_on_meta>
    <other_info_on_meta type="treatment_page">343</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Cronquist" date="1947" rank="species">religiosus</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>6: 258. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species religiosus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066667</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or short-lived perennials, 6–30 (–40) cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudices simple or branched.</text>
      <biological_entity id="o24005" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o24007" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems decumbent-ascending to erect (branched), usually sparsely strigose (hairs rarely ascending-spreading), sometimes sparsely hirsutulous distally, sometimes sparsely glandular, sometimes minutely glandular distally.</text>
      <biological_entity id="o24008" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="decumbent-ascending" name="orientation" src="d0_s2" to="erect" />
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="sometimes sparsely; distally" name="pubescence" src="d0_s2" value="hirsutulous" value_original="hirsutulous" />
        <character is_modifier="false" modifier="sometimes sparsely" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="sometimes minutely; distally" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline;</text>
      <biological_entity id="o24009" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades oblanceolate-spatulate to spatulate, 10–70 × 2–8 (–13) mm, margins usually entire, sometimes dentate or pinnately divided, faces sparsely strigose, eglandular;</text>
      <biological_entity constraint="basal" id="o24010" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate-spatulate" name="shape" src="d0_s4" to="spatulate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="70" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24011" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o24012" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline blades linear to oblanceolate, reduced distally.</text>
      <biological_entity constraint="cauline" id="o24013" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="oblanceolate" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–50+ in diffuse arrays (first 1 per branch, later more from axillary branches).</text>
      <biological_entity id="o24014" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in arrays" constraintid="o24015" from="1" name="quantity" src="d0_s6" to="50" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o24015" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="density" src="d0_s6" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 2–3.5 × 5.5–7.5 mm.</text>
      <biological_entity id="o24016" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="width" src="d0_s7" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 3–5 series, sparsely to moderately hirtellous, minutely glandular.</text>
      <biological_entity id="o24017" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" notes="" src="d0_s8" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o24018" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <relation from="o24017" id="r2213" name="in" negation="false" src="d0_s8" to="o24018" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 37–85;</text>
      <biological_entity id="o24019" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="37" name="quantity" src="d0_s9" to="85" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white, drying lilac, with abaxial lilac midstripe, 3.5–7 mm;</text>
      <biological_entity id="o24020" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="condition" src="d0_s10" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="lilac" value_original="lilac" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24021" name="midstripe" name_original="midstripe" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="lilac" value_original="lilac" />
      </biological_entity>
      <relation from="o24020" id="r2214" name="with" negation="false" src="d0_s10" to="o24021" />
    </statement>
    <statement id="d0_s11">
      <text>laminae not coiling or reflexing.</text>
      <biological_entity id="o24022" name="lamina" name_original="laminae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="coiling" value_original="coiling" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc corollas 1.6–2.4 mm (throats slightly indurate and inflated).</text>
      <biological_entity constraint="disc" id="o24023" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s12" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 0.8–1.2 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o24024" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o24025" name="face" name_original="faces" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi: outer of setae, inner of 6–12 bristles.</text>
      <biological_entity id="o24027" name="seta" name_original="setae" src="d0_s14" type="structure" />
      <biological_entity id="o24028" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s14" to="12" />
      </biological_entity>
      <relation from="o24026" id="r2215" name="outer of" negation="false" src="d0_s14" to="o24027" />
      <relation from="o24026" id="r2216" name="inner of" negation="false" src="d0_s14" to="o24028" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 18, 27.</text>
      <biological_entity id="o24026" name="pappus" name_original="pappi" src="d0_s14" type="structure" />
      <biological_entity constraint="2n" id="o24029" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
        <character name="quantity" src="d0_s15" value="27" value_original="27" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deep sand, ponderosa pine, pinyon-juniper, oak-maple, riparian</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deep sand" />
        <character name="habitat" value="ponderosa pine" />
        <character name="habitat" value="pinyon-juniper" />
        <character name="habitat" value="oak-maple" />
        <character name="habitat" value="riparian" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>166.</number>
  <other_name type="common_name">Clear Creek fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Erigeron religiosus differs from E. divergens in stem vestiture (strigose, less densely hairy, less glandular) and petiolate basal leaves persistent into flowering. It differs in habit and habitat from E. sionis; it sometimes occurs in proximity; the uncommon presence of deeply lobed leaves in E. religiosus suggests gene flow from E. sionis.</discussion>
  
</bio:treatment>