<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alison McKenzie Mahoney</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">48</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="treatment_page">196</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">arctotideae</taxon_name>
    <taxon_name authority="Gaertner" date="1791" rank="genus">GAZANIA</taxon_name>
    <place_of_publication>
      <publication_title>Fruct. Sem. Pl.</publication_title>
      <place_in_publication>2: 451, plate 173, fig. 2. 1791</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe arctotideae;genus GAZANIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek gaza, riches or royal treasure, alluding to splendor of flowers; or for Theodorus of Gaza (1398–1478), who translated the works of Theophrastus</other_info_on_name>
    <other_info_on_name type="fna_id">113351</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials [annuals, shrubs], [5–] 10–35 cm (often cespitose, not prickly, sometimes with milky sap).</text>
      <biological_entity id="o15683" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect (often ± congested; rootstocks often woody).</text>
      <biological_entity id="o15684" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually mostly basal, sometimes cauline as well;</text>
      <biological_entity id="o15685" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15686" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="usually mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiolate or sessile;</text>
      <biological_entity id="o15687" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades linear to lanceolate, spatulate or oblanceolate, margins entire or pinnately lobed, abaxial faces white-woolly, adaxial usually glabrate or glabrous, sometimes arachnose.</text>
      <biological_entity id="o15688" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="lanceolate spatulate or oblanceolate" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="lanceolate spatulate or oblanceolate" />
      </biological_entity>
      <biological_entity id="o15689" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15690" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="white-woolly" value_original="white-woolly" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o15691" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="arachnose" value_original="arachnose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± campanulate, turbinate, or cylindric, [5–] 10–15+ mm diam.</text>
      <biological_entity id="o15692" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s5" value="turbinate" value_original="turbinate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s5" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="diameter" src="d0_s5" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries in 2–4 series, connate 1/2–3/4 their lengths, margins ± scarious, apices acute, abaxial faces glabrous [arachnose to tomentose].</text>
      <biological_entity id="o15693" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character is_modifier="false" name="fusion" notes="" src="d0_s6" value="connate" value_original="connate" />
        <character char_type="range_value" from="1/2" name="lengths" src="d0_s6" to="3/4" />
      </biological_entity>
      <biological_entity id="o15694" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <biological_entity id="o15695" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o15696" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o15697" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o15693" id="r1432" name="in" negation="false" src="d0_s6" to="o15694" />
    </statement>
    <statement id="d0_s7">
      <text>Receptacles conic or convex, deeply alveolate (pits enclosing cypselae, their margins often ciliate).</text>
      <biological_entity id="o15698" name="receptacle" name_original="receptacles" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="conic" value_original="conic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="deeply" name="relief" src="d0_s7" value="alveolate" value_original="alveolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets neuter;</text>
      <biological_entity id="o15699" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="neuter" value_original="neuter" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas yellow, orange, or red to maroon (usually each with darker abaxial stripe and a darker adaxial spot or blotch near base), laminae 5-veined, 4-toothed.</text>
      <biological_entity id="o15700" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="red" name="coloration" src="d0_s9" to="maroon" />
        <character char_type="range_value" from="red" name="coloration" src="d0_s9" to="maroon" />
      </biological_entity>
      <biological_entity id="o15701" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="5-veined" value_original="5-veined" />
        <character is_modifier="false" name="shape" src="d0_s9" value="4-toothed" value_original="4-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets bisexual, fertile;</text>
      <biological_entity id="o15702" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow to orange.</text>
      <biological_entity id="o15703" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s11" to="orange" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae obovoid, ribs 0, faces villous;</text>
      <biological_entity id="o15704" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obovoid" value_original="obovoid" />
      </biological_entity>
      <biological_entity id="o15705" name="rib" name_original="ribs" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o15706" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi persistent, of 7–8 [–12+], lanceolate to subulate-aristate scales in 2 series (± hidden by hairs on cypselae).</text>
      <biological_entity id="o15708" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s13" to="12" upper_restricted="false" />
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s13" to="8" />
        <character char_type="range_value" from="lanceolate" is_modifier="true" name="shape" src="d0_s13" to="subulate-aristate" />
      </biological_entity>
      <biological_entity id="o15709" name="series" name_original="series" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <relation from="o15707" id="r1433" name="consist_of" negation="false" src="d0_s13" to="o15708" />
      <relation from="o15708" id="r1434" name="in" negation="false" src="d0_s13" to="o15709" />
    </statement>
    <statement id="d0_s14">
      <text>x = 9.</text>
      <biological_entity id="o15707" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character is_modifier="false" name="duration" src="d0_s13" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="x" id="o15710" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; South Africa, Namibia, tropical East Africa; cultivated and/or introduced elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="South Africa" establishment_means="introduced" />
        <character name="distribution" value="Namibia" establishment_means="introduced" />
        <character name="distribution" value="tropical East Africa" establishment_means="introduced" />
        <character name="distribution" value="cultivated and/or  elsewhere" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="common_name">Treasure-flower</other_name>
  <discussion>Species ca. 20 (1 in the flora).</discussion>
  
</bio:treatment>