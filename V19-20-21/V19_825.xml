<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="treatment_page">494</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">anthemideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">achillea</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">alpina</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 899. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe anthemideae;genus achillea;species alpina</taxon_hierarchy>
    <other_info_on_name type="fna_id">200023006</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Achillea</taxon_name>
    <taxon_name authority="Ledebour" date="unknown" rank="species">sibirica</taxon_name>
    <taxon_hierarchy>genus Achillea;species sibirica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 50–80 cm (fibrous-rooted and rhizomatous).</text>
      <biological_entity id="o2402" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems 1, erect, branched or unbranched distally, sparsely villous to glabrate.</text>
      <biological_entity id="o2403" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="sparsely villous" name="pubescence" src="d0_s1" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sessile;</text>
      <biological_entity id="o2404" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades linear-lanceolate to oblong-lanceolate, 5–10 cm × 4–8 mm, (margins serrate to doubly serrate, teeth antrorse) faces sparingly villous or glabrate.</text>
      <biological_entity id="o2405" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s3" to="oblong-lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2406" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sparingly" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads 10–25+, in crowded, simple or compound, corymbiform arrays.</text>
      <biological_entity id="o2407" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s4" to="25" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o2408" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="crowded" value_original="crowded" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="compound" value_original="compound" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o2407" id="r232" name="in" negation="false" src="d0_s4" to="o2408" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 20–30 in ± 3 series, (light green, margins light to dark-brown, midribs dark green or yellow-green) lanceolate to oblanceolate, faces (abaxial) sparingly tomentose.</text>
      <biological_entity id="o2409" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o2410" from="20" name="quantity" src="d0_s5" to="30" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s5" to="oblanceolate" />
      </biological_entity>
      <biological_entity id="o2410" name="series" name_original="series" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o2411" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparingly" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Receptacles convex;</text>
      <biological_entity id="o2412" name="receptacle" name_original="receptacles" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>paleae oblong, 3.5–4.5 mm (apices dark, rounded).</text>
      <biological_entity id="o2413" name="palea" name_original="paleae" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 6–8 (–12), pistillate, fertile;</text>
      <biological_entity id="o2414" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="12" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas white, laminae 1–3 × 2–3 mm.</text>
      <biological_entity id="o2415" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o2416" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 25–30+;</text>
      <biological_entity id="o2417" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="25" name="quantity" src="d0_s10" to="30" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas grayish or yellowish white, 2–3 mm.</text>
      <biological_entity id="o2418" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish white" value_original="yellowish white" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 2.5 mm. 2n = 36.</text>
      <biological_entity id="o2419" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2420" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early Jul–early Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Sep" from="early Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Meadows, forest edges, roadsides, lakeshores, along streams, moist soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="meadows" />
        <character name="habitat" value="forest edges" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="lakeshores" constraint="along streams , moist soils" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="moist soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Ont., Que., Sask., Yukon; Alaska, Minn., N.Dak.; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Siberian yarrow</other_name>
  <other_name type="common_name">achillée de Sibérie</other_name>
  <discussion>Achillea alpina has been reported (as A. sibirica) as occurring in New Jersey and Missouri. Specimens examined from those states were from plants cultivated in botanical gardens; there is no evidence that Achillea alpina has escaped in those states.</discussion>
  
</bio:treatment>