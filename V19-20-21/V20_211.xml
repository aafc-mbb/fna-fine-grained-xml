<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">108</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="mention_page">117</other_info_on_meta>
    <other_info_on_meta type="mention_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">125</other_info_on_meta>
    <other_info_on_meta type="mention_page">130</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
    <other_info_on_meta type="illustrator">Marjorie C. Leggitt</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">solidago</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Solidago</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus solidago;section Solidago</taxon_hierarchy>
    <other_info_on_name type="fna_id">316932</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: basal (rosettes) usually withering by flowering;</text>
      <biological_entity id="o21558" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="basal" id="o21559" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character constraint="by flowering" is_modifier="false" modifier="usually" name="life_cycle" src="d0_s0" value="withering" value_original="withering" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>petiole bases or vasculature not persisting on rhizomes;</text>
      <biological_entity id="o21560" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o21561" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character constraint="on rhizomes" constraintid="o21564" is_modifier="false" modifier="not" name="duration" src="d0_s1" value="persisting" value_original="persisting" />
      </biological_entity>
      <biological_entity id="o21562" name="base" name_original="bases" src="d0_s1" type="structure">
        <character constraint="on rhizomes" constraintid="o21564" is_modifier="false" modifier="not" name="duration" src="d0_s1" value="persisting" value_original="persisting" />
      </biological_entity>
      <biological_entity id="o21563" name="vasculature" name_original="vasculature" src="d0_s1" type="structure">
        <character constraint="on rhizomes" constraintid="o21564" is_modifier="false" modifier="not" name="duration" src="d0_s1" value="persisting" value_original="persisting" />
      </biological_entity>
      <biological_entity id="o21564" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>proximalmost cauline petiolate, sometimes present at flowering;</text>
      <biological_entity id="o21565" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximalmost cauline" id="o21566" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
        <character constraint="at flowering" is_modifier="false" modifier="sometimes" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal and distal sometimes 3-nerved.</text>
      <biological_entity id="o21567" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="3-nerved" value_original="3-nerved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in ± paniculiform or thyrsiform arrays (wand, club, or secund cone-shaped) or in axillary clusters, sometimes in rounded corymbiform arrays.</text>
      <biological_entity id="o21568" name="head" name_original="heads" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries 1-nerved, usually not striate (3–5-nerved and striate in S. glomerata, S. lancifolia, S. roanensis) sometimes minutely stipitate-glandular, sometimes resinous.</text>
      <biological_entity id="o21569" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" modifier="usually not" name="coloration_or_pubescence_or_relief" src="d0_s5" value="striate" value_original="striate" />
        <character is_modifier="false" modifier="sometimes minutely" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="coating" src="d0_s5" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pappus bristles in 2–3 series (shorter, outer setiform scales rarely present, outer, longer bristles apically attenuate, inner bristles longest, weakly to strongly clavate).</text>
      <biological_entity constraint="pappus" id="o21570" name="bristle" name_original="bristles" src="d0_s6" type="structure" />
      <biological_entity id="o21571" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <relation from="o21570" id="r1988" name="in" negation="false" src="d0_s6" to="o21571" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, South America, temperate Eurasia; introduced in tropical Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="temperate Eurasia" establishment_means="native" />
        <character name="distribution" value="in tropical Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>163a.</number>
  <discussion>Species ca. 95 (71 in the flora).</discussion>
  
</bio:treatment>