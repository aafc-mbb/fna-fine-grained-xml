<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">166</other_info_on_meta>
    <other_info_on_meta type="treatment_page">167</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="unknown" rank="tribe">cardueae</taxon_name>
    <taxon_name authority="de Candolle" date="1810" rank="genus">saussurea</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle" date="1810" rank="species">angustifolia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe cardueae;genus saussurea;species angustifolia;variety angustifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068706</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–40 (–50) cm.</text>
      <biological_entity id="o9077" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually simple, ± thinly arachnoid.</text>
      <biological_entity id="o9078" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="more or less thinly" name="pubescence" src="d0_s1" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly 2–8 mm wide;</text>
      <biological_entity id="o9079" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal blades linear or narrowly elliptic, margins entire or denticulate, glabrous or abaxially thinly arachnoid;</text>
      <biological_entity constraint="basal" id="o9080" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s3" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o9081" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="abaxially thinly" name="pubescence" src="d0_s3" value="arachnoid" value_original="arachnoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline linear or oblong, crowded or well separated, usually not surpassing heads.</text>
      <biological_entity constraint="cauline" id="o9082" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="crowded" value_original="crowded" />
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s4" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o9083" name="head" name_original="heads" src="d0_s4" type="structure" />
      <relation from="o9082" id="r841" modifier="usually not" name="surpassing" negation="false" src="d0_s4" to="o9083" />
    </statement>
    <statement id="d0_s5">
      <text>Heads crowded or in ± open arrays.</text>
      <biological_entity id="o9084" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o9085" name="array" name_original="arrays" src="d0_s5" type="structure" />
      <relation from="o9084" id="r842" modifier="more or less" name="open" negation="false" src="d0_s5" to="o9085" />
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries unequal in 3–5 series;</text>
      <biological_entity id="o9086" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character constraint="in series" constraintid="o9087" is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o9087" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>outer usually 1.5–2 times longer than wide.</text>
    </statement>
    <statement id="d0_s8">
      <text>2n = 26, 52.</text>
      <biological_entity constraint="outer" id="o9088" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="l_w_ratio" src="d0_s7" value="1.5-2" value_original="1.5-2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9089" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="26" value_original="26" />
        <character name="quantity" src="d0_s8" value="52" value_original="52" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist to dry sites, arctic and alpine tundras, bogs, riverbanks, grassy slopes, willow thickets, spruce forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry sites" />
        <character name="habitat" value="arctic" />
        <character name="habitat" value="alpine tundras" />
        <character name="habitat" value="bogs" />
        <character name="habitat" value="moist to dry sites" />
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="grassy slopes" />
        <character name="habitat" value="thickets" modifier="willow" />
        <character name="habitat" value="forests" modifier="spruce" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.W.T., Nunavut, Yukon; Alaska; Russian Far East (Chukotka).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Russian Far East (Chukotka)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3a.</number>
  
</bio:treatment>