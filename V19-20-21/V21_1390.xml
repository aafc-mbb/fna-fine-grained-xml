<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">548</other_info_on_meta>
    <other_info_on_meta type="mention_page">550</other_info_on_meta>
    <other_info_on_meta type="mention_page">551</other_info_on_meta>
    <other_info_on_meta type="treatment_page">549</other_info_on_meta>
    <other_info_on_meta type="illustration_page">549</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Spach" date="1841" rank="genus">ageratina</taxon_name>
    <taxon_name authority="(Linnaeus) R. M. King &amp; H. Robinson" date="1970" rank="species">altissima</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>19: 212. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus ageratina;species altissima</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066013</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ageratum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">altissimum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 839. 1753,</place_in_publication>
      <other_info_on_pub>not Eupatorium altissimum Linnaeus 1753</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Ageratum;species altissimum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, (30–) 50–80 (–120) cm (bases usually fibrous-rooted crowns, sometimes rhizomatous).</text>
      <biological_entity id="o12775" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="50" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, sometimes semiscandent, puberulent (hairs minute, crisped).</text>
      <biological_entity id="o12776" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="semiscandent" value_original="semiscandent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite;</text>
      <biological_entity id="o12777" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petioles (5–) 10–30 (–50) mm;</text>
      <biological_entity id="o12778" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades usually deltate-ovate to ovate or broadly lanceolate, sometimes ovatelanceolate, 4–11 (–13) × 2.5–8 (–9) cm, bases usually rounded to truncate or obtuse, sometimes cordate, margins coarsely and doubly incised-serrate, apices usually acuminate.</text>
      <biological_entity id="o12779" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually deltate-ovate" name="shape" src="d0_s4" to="ovate or broadly lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="13" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="11" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12780" name="base" name_original="bases" src="d0_s4" type="structure">
        <character char_type="range_value" from="usually rounded" name="shape" src="d0_s4" to="truncate or obtuse" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o12781" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s4" value="incised-serrate" value_original="incised-serrate" />
      </biological_entity>
      <biological_entity id="o12782" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads clustered.</text>
      <biological_entity id="o12783" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s5" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 1–5 mm, puberulent.</text>
      <biological_entity id="o12784" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 4–5 mm.</text>
      <biological_entity id="o12785" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries: apices acute, abaxial faces glabrous or sparsely and finely villous.</text>
      <biological_entity id="o12786" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure" />
      <biological_entity id="o12787" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12788" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s8" value="sparsely" value_original="sparsely" />
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Corollas white, lobes sparsely short-villous.</text>
      <biological_entity id="o12789" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o12790" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="short-villous" value_original="short-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae glabrous.</text>
      <biological_entity id="o12791" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., Que., Sask.; Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., N.C., N.Dak., N.H., N.J., N.Y., Nebr., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Va., Vt., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">White snakeroot</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllaries 3–5 mm, apices not cuspidate</description>
      <determination>1a Ageratina altissima var. altissima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Phyllaries 4–7 mm, apices cuspidate to acuminate</description>
      <determination>1b Ageratina altissima var. roanensis</determination>
    </key_statement>
  </key>
</bio:treatment>