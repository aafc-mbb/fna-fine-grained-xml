<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="treatment_page">74</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(A. Gray) G. L. Nesom &amp; G. I. Baird" date="1993" rank="species">parryi</taxon_name>
    <taxon_name authority="(A. Nelson &amp; P. B. Kennedy) G. L. Nesom &amp; G. I. Baird" date="1993" rank="variety">monocephala</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>75: 89. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species parryi;variety monocephala</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250068303</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="A. Nelson &amp; P. B. Kennedy" date="unknown" rank="species">monocephalus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>19: 39. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Chrysothamnus;species monocephalus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysothamnus</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">parryi</taxon_name>
    <taxon_name authority="(A. Nelson &amp; P. B. Kennedy) H. M. Hall &amp; Clements" date="unknown" rank="subspecies">monocephalus</taxon_name>
    <taxon_hierarchy>genus Chrysothamnus;species parryi;subspecies monocephalus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–40 cm (rigid).</text>
      <biological_entity id="o25754" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves crowded, grayish;</text>
      <biological_entity id="o25755" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="grayish" value_original="grayish" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades mostly 1-nerved, linear, 10–30 × 0.5–1.5 mm, faces sparsely to densely tomentulose, viscidulous;</text>
      <biological_entity id="o25756" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s2" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s2" value="linear" value_original="linear" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s2" to="30" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25757" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s2" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>distalmost overtopping arrays.</text>
      <biological_entity constraint="distalmost" id="o25758" name="blade" name_original="blades" src="d0_s3" type="structure" />
      <biological_entity id="o25759" name="array" name_original="arrays" src="d0_s3" type="structure" />
      <relation from="o25758" id="r2385" name="overtopping" negation="false" src="d0_s3" to="o25759" />
    </statement>
    <statement id="d0_s4">
      <text>Heads 1–2 (–4) at branch tips.</text>
      <biological_entity id="o25760" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="4" />
        <character char_type="range_value" constraint="at branch tips" constraintid="o25761" from="1" name="quantity" src="d0_s4" to="2" />
      </biological_entity>
      <biological_entity constraint="branch" id="o25761" name="tip" name_original="tips" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Involucres 10–11 mm.</text>
      <biological_entity id="o25762" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 8–12, thinly chartaceous, apices erect to spreading, attenuate.</text>
      <biological_entity id="o25763" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s6" to="12" />
        <character is_modifier="false" modifier="thinly" name="pubescence_or_texture" src="d0_s6" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o25764" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s6" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 5–7;</text>
      <biological_entity id="o25765" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas 8–9 mm, tubes sparsely strigose, throats gradually dilated, lobes 1.5–1.8 mm. 2n = 18.</text>
      <biological_entity id="o25766" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25767" name="tube" name_original="tubes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o25768" name="throat" name_original="throats" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o25769" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25770" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open subalpine forests, talus, and alpine barrens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open subalpine forests" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="alpine barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2800–3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="2800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27g.</number>
  <other_name type="common_name">One-headed rabbitbrush</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Variety monocephala is known from the Sierra Nevada.</discussion>
  
</bio:treatment>