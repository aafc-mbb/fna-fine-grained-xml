<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="mention_page">267</other_info_on_meta>
    <other_info_on_meta type="treatment_page">266</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">prenanthes</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">altissima</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 797. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus prenanthes;species altissima</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417052</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nabalus</taxon_name>
    <taxon_name authority="(Linnaeus) Hooker" date="unknown" rank="species">altissimus</taxon_name>
    <taxon_hierarchy>genus Nabalus;species altissimus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prenanthes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">altissima</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">cinnamomea</taxon_name>
    <taxon_hierarchy>genus Prenanthes;species altissima;variety cinnamomea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Prenanthes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">altissima</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="variety">hispidula</taxon_name>
    <taxon_hierarchy>genus Prenanthes;species altissima;variety hispidula;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 40–250 cm;</text>
      <biological_entity id="o12953" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="250" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproots thickened, knotty, tuberous.</text>
      <biological_entity id="o12954" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s1" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="tuberous" value_original="tuberous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, greenish to purplish, glabrous proximally, often tomentulose distally.</text>
      <biological_entity id="o12955" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s2" to="purplish" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="often; distally" name="pubescence" src="d0_s2" value="tomentulose" value_original="tomentulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: proximal present at flowering;</text>
      <biological_entity id="o12956" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o12957" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petioles winged;</text>
      <biological_entity id="o12958" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o12959" name="petiole" name_original="petioles" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades usually ovate or triangular, 4–15 × 2–16 cm, thin, bases truncate to hastate or cordate, margins entire or shallowly dentate, often deeply 3-lobed, faces glabrous or with scattered hairs on veins;</text>
      <biological_entity id="o12960" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o12961" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s5" to="16" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s5" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o12962" name="base" name_original="bases" src="d0_s5" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="hastate or cordate" />
      </biological_entity>
      <biological_entity id="o12963" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="often deeply" name="shape" src="d0_s5" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o12964" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="with scattered hairs" value_original="with scattered hairs" />
      </biological_entity>
      <biological_entity id="o12965" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o12966" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <relation from="o12964" id="r1180" name="with" negation="false" src="d0_s5" to="o12965" />
      <relation from="o12965" id="r1181" name="on" negation="false" src="d0_s5" to="o12966" />
    </statement>
    <statement id="d0_s6">
      <text>distal reduced in size and lobing.</text>
      <biological_entity id="o12967" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o12968" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lobing" value_original="lobing" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads in narrow or spreading, paniculiform arrays.</text>
      <biological_entity id="o12969" name="head" name_original="heads" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres cylindric, 9–14 × 2–3 mm.</text>
      <biological_entity id="o12970" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calyculi of 4–6, blackish, triangular bractlets 1–4 mm, glabrous.</text>
      <biological_entity id="o12971" name="calyculus" name_original="calyculi" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12972" name="bractlet" name_original="bractlets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s9" to="6" />
        <character is_modifier="true" name="coloration" src="d0_s9" value="blackish" value_original="blackish" />
        <character is_modifier="true" name="shape" src="d0_s9" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o12971" id="r1182" name="consist_of" negation="false" src="d0_s9" to="o12972" />
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries (4–) 5 (–6), pale green, often blackish at bases and apices, linear to lanceolate, 10–12 mm, glabrous or sparsely hairy.</text>
      <biological_entity id="o12973" name="phyllary" name_original="phyllaries" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s10" to="6" />
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale green" value_original="pale green" />
        <character constraint="at apices" constraintid="o12975" is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="blackish" value_original="blackish" />
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s10" to="lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o12974" name="base" name_original="bases" src="d0_s10" type="structure" />
      <biological_entity id="o12975" name="apex" name_original="apices" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Florets (4–) 5 (–6);</text>
      <biological_entity id="o12976" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s11" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="6" />
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas pale-yellow to greenish yellow, 7–15 mm.</text>
      <biological_entity id="o12977" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s12" to="greenish yellow" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae brown to tan, subcylindric, subterete, 4–5 mm, indistinctly 5–10-ribbed;</text>
      <biological_entity id="o12978" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s13" to="tan" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subcylindric" value_original="subcylindric" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subterete" value_original="subterete" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="indistinctly" name="architecture_or_shape" src="d0_s13" value="5-10-ribbed" value_original="5-10-ribbed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi usually whitish or pale-yellow, sometimes reddish-brown, 5–6 mm. 2n = 16.</text>
      <biological_entity id="o12979" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s14" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s14" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12980" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open deciduous hardwood or mixed woods, shaded slopes, bluffs, disturbed areas, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open deciduous hardwood" />
        <character name="habitat" value="mixed woods" />
        <character name="habitat" value="shaded slopes" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50–800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="800" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., Ga., Ill., Ind., Ky., La., Maine, Md., Mass., Mich., Mo., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Tall rattlesnakeroot</other_name>
  <other_name type="common_name">prenanthe élevée</other_name>
  <discussion>Prenanthes altissima is recognized by its narrow involucres with 5 pale green, glabrous phyllaries, (4–)5(–6) florets, and pale yellow to greenish yellow corollas. Pappi in this species are most commonly whitish or pale yellow. Specimens with reddish brown to orange pappi have been recognized as var. cinnamomea, found in Arkansas, Louisiana, and Missouri. Specimens with densely hairy stems and pale yellow pappi have been recognized as var. hispidula, found mostly in New York, New England, and adjacent Canada.</discussion>
  
</bio:treatment>