<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">476</other_info_on_meta>
    <other_info_on_meta type="treatment_page">475</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">eutrochium</taxon_name>
    <taxon_name authority="(Linnaeus) E. E. Lamont" date="2004" rank="species">maculatum</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 902. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus eutrochium;species maculatum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066768</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">maculatum</taxon_name>
    <place_of_publication>
      <publication_title>Cent. Pl. I,</publication_title>
      <place_in_publication>27. 1755</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eupatorium;species maculatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatoriadelphus</taxon_name>
    <taxon_name authority="(Linnaeus) R. M. King &amp; H. Robinson" date="unknown" rank="species">maculatus</taxon_name>
    <taxon_hierarchy>genus Eupatoriadelphus;species maculatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">purpureum</taxon_name>
    <taxon_name authority="(Linnaeus) Á. Löve &amp; D. Löve" date="unknown" rank="subspecies">maculatum</taxon_name>
    <taxon_hierarchy>genus Eupatorium;species purpureum;subspecies maculatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">purpureum</taxon_name>
    <taxon_name authority="(Linnaeus) Voss" date="unknown" rank="variety">maculatum</taxon_name>
    <taxon_hierarchy>genus Eupatorium;species purpureum;variety maculatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eupatorium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">trifoliatum</taxon_name>
    <taxon_name authority="(Linnaeus) Farwell" date="unknown" rank="variety">maculatum</taxon_name>
    <taxon_hierarchy>genus Eupatorium;species trifoliatum;variety maculatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 60–200 cm.</text>
      <biological_entity id="o8640" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually purple-spotted, sometimes uniformly purple, usually solid, sometimes hollow near bases, glabrous proximally to densely puberulent throughout, glandular-puberulent distally.</text>
      <biological_entity id="o8641" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="purple-spotted" value_original="purple-spotted" />
        <character is_modifier="false" modifier="sometimes uniformly" name="coloration_or_density" src="d0_s1" value="purple" value_original="purple" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="solid" value_original="solid" />
        <character constraint="near bases" constraintid="o8642" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally to densely; throughout" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o8642" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves in (3s–) 4s–5s (–6s);</text>
      <biological_entity id="o8643" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>petioles 5–20 mm, glabrous or pubescent;</text>
      <biological_entity id="o8644" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s3" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades pinnately veined, lance-elliptic to lanceolate or lanceovate, mostly (6–) 8–23 (–30) × (1.5–) 2–7 (–9) cm, bases gradually or abruptly tapered, margins sharply serrate or doubly serrate, abaxial faces glanddotted and densely pubescent to glabrate, adaxial faces sparingly hairy or glabrous.</text>
      <biological_entity id="o8645" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="veined" value_original="veined" />
        <character char_type="range_value" from="lance-elliptic" name="shape" src="d0_s4" to="lanceolate or lanceovate" />
        <character char_type="range_value" from="6" from_unit="cm" name="atypical_length" src="d0_s4" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="23" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s4" to="23" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_width" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="9" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8646" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly; abruptly" name="shape" src="d0_s4" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o8647" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sharply" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="doubly" name="architecture_or_shape" src="d0_s4" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8648" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="densely pubescent" name="pubescence" src="d0_s4" to="glabrate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8649" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sparingly" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in flat-topped, corymbiform arrays.</text>
      <biological_entity id="o8650" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o8651" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o8650" id="r602" name="in" negation="false" src="d0_s5" to="o8651" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres often purplish, 6.5–9 × 3.5–7 mm.</text>
      <biological_entity id="o8652" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s6" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="length" src="d0_s6" to="9" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries glabrous or densely pubescent.</text>
      <biological_entity id="o8653" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Florets (8–) 9–20 (–22);</text>
      <biological_entity id="o8654" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s8" to="9" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s8" to="22" />
        <character char_type="range_value" from="9" name="quantity" src="d0_s8" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas purplish, 4.5–7.5 mm.</text>
      <biological_entity id="o8655" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 3–5 mm. 2n = 20.</text>
      <biological_entity id="o8656" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8657" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.S., Nfld. and Labr. (Nfld.), Ont., P.E.I., Que., Sask.; Ariz., Colo., Conn., Idaho, Ill., Ind., Iowa, Kans., Maine, Mass., Mich., Minn., Mo., N.C., N.Dak., N.H., N.J., N.Mex., N.Y., Nebr., Ohio, Pa., S.Dak., Utah, Va., Vt., W.Va., Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Spotted joepyeweed</other_name>
  <other_name type="common_name">eupatoire maculée</other_name>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Eutrochium maculatum has the widest geographic distribution and greatest morphologic variability among species in the genus. The three varieties recognized here show intergradation where the ranges overlap.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems densely puberulent throughout; leaves densely pubescent on abaxial faces, relatively thick; mostly w of Mississippi River</description>
      <determination>2c Eutrochium maculatum var. bruneri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems glabrous proximally, puberulent among heads; leaves usually glabrate (seldom strongly puberulent) on abaxial faces, relatively thin; mostly e of Mississippi River</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distalmost whorls of leaves subtending heads equaling or surpassing arrays of heads; maritime Canada, adjacent New England to ne Minnesota, n Michigan, and s Ontario</description>
      <determination>2b Eutrochium maculatum var. foliosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distalmost whorls of leaves subtending heads not equaling arrays of heads; Newfoundland to sw Ontario, s to n and w New England, Pennsylvania, North Carolina, Ohio, Indiana, Illinois, and Iowa</description>
      <determination>2a Eutrochium maculatum var. maculatum</determination>
    </key_statement>
  </key>
</bio:treatment>