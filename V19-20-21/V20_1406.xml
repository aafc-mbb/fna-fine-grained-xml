<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>John L. Strother</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">543</other_info_on_meta>
    <other_info_on_meta type="treatment_page">627</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">senecioneae</taxon_name>
    <taxon_name authority="A. Gray" date="1883" rank="genus">CACALIOPSIS</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>19: 50. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe senecioneae;genus CACALIOPSIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus name Cacalia and Greek - opsis, like</other_info_on_name>
    <other_info_on_name type="fna_id">104961</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 15–90 cm (rhizomes fibrous-rooted; plants glabrous or sparsely to densely tomentose).</text>
      <biological_entity id="o20567" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="90" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually 1, erect.</text>
      <biological_entity id="o20568" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o20570" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o20569" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades palmately nerved (and lobed, lobes further digitately divided), ± reniform to orbiculate, ultimate margins toothed, abaxial faces lanate to floccose-tomentose, adaxial faces tomentulose and glabrescent or glabrous.</text>
      <biological_entity id="o20571" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="palmately" name="architecture" src="d0_s5" value="nerved" value_original="nerved" />
        <character char_type="range_value" from="less reniform" name="shape" src="d0_s5" to="orbiculate" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o20572" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20573" name="face" name_original="faces" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanate" name="pubescence" src="d0_s5" to="floccose-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20574" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="tomentulose" value_original="tomentulose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads discoid, in racemiform to corymbiform or subumbelliform arrays.</text>
      <biological_entity id="o20575" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="discoid" value_original="discoid" />
      </biological_entity>
      <biological_entity id="o20576" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character char_type="range_value" from="racemiform" is_modifier="true" name="architecture" src="d0_s6" to="corymbiform or subumbelliform" />
      </biological_entity>
      <relation from="o20575" id="r1904" name="in" negation="false" src="d0_s6" to="o20576" />
    </statement>
    <statement id="d0_s7">
      <text>Calyculi 0.</text>
      <biological_entity id="o20577" name="calyculus" name_original="calyculi" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Involucres broadly turbinate to campanulate, 7–15+ mm diam.</text>
    </statement>
    <statement id="d0_s9">
      <text>(broader in fruit).</text>
      <biological_entity id="o20578" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character char_type="range_value" from="broadly turbinate" name="shape" src="d0_s8" to="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="diameter" src="d0_s8" to="15" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Phyllaries persistent, 8–25 in 1–2 series, erect to spreading, distinct, lanceolate to linear, margins scarious.</text>
      <biological_entity id="o20579" name="phyllarie" name_original="phyllaries" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="persistent" value_original="persistent" />
        <character char_type="range_value" constraint="in series" constraintid="o20580" from="8" name="quantity" src="d0_s10" to="25" />
        <character char_type="range_value" from="erect" name="orientation" notes="" src="d0_s10" to="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="linear" />
      </biological_entity>
      <biological_entity id="o20580" name="series" name_original="series" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="2" />
      </biological_entity>
      <biological_entity id="o20581" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Receptacles flat, foveolate, epaleate.</text>
      <biological_entity id="o20582" name="receptacle" name_original="receptacles" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s11" value="foveolate" value_original="foveolate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Ray-florets 0.</text>
      <biological_entity id="o20583" name="ray-floret" name_original="ray-florets" src="d0_s12" type="structure">
        <character name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 20–50, bisexual, fertile;</text>
      <biological_entity id="o20584" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s13" to="50" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas yellow to orange, tubes longer than cylindric to funnelform throats, lobes 5, spreading, lanceolate;</text>
      <biological_entity id="o20585" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s14" to="orange" />
      </biological_entity>
      <biological_entity id="o20586" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character constraint="than cylindric to funnelform throats" constraintid="o20587, o20588" is_modifier="false" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o20587" name="funnelform" name_original="funnelform" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o20588" name="throat" name_original="throats" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o20589" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style-branches: stigmatic areas continuous, apices with deltoid to conic appendages.</text>
      <biological_entity id="o20590" name="style-branch" name_original="style-branches" src="d0_s15" type="structure" />
      <biological_entity constraint="stigmatic" id="o20591" name="area" name_original="areas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o20592" name="apex" name_original="apices" src="d0_s15" type="structure" />
      <biological_entity id="o20593" name="appendage" name_original="appendages" src="d0_s15" type="structure">
        <character char_type="range_value" from="deltoid" is_modifier="true" name="shape" src="d0_s15" to="conic" />
      </biological_entity>
      <relation from="o20592" id="r1905" name="with" negation="false" src="d0_s15" to="o20593" />
    </statement>
    <statement id="d0_s16">
      <text>Cypselae cylindric, 12–15-veined, glabrous;</text>
      <biological_entity id="o20594" name="cypsela" name_original="cypselae" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="12-15-veined" value_original="12-15-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi readily falling or fragile, of 120–140, white, barbellate bristles.</text>
      <biological_entity id="o20596" name="bristle" name_original="bristles" src="d0_s17" type="structure">
        <character char_type="range_value" from="120" is_modifier="true" name="quantity" src="d0_s17" to="140" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="barbellate" value_original="barbellate" />
      </biological_entity>
      <relation from="o20595" id="r1906" name="consist_of" negation="false" src="d0_s17" to="o20596" />
    </statement>
    <statement id="d0_s18">
      <text>x = 30.</text>
      <biological_entity id="o20595" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="readily" name="life_cycle" src="d0_s17" value="falling" value_original="falling" />
        <character is_modifier="false" name="fragility" src="d0_s17" value="fragile" value_original="fragile" />
      </biological_entity>
      <biological_entity constraint="x" id="o20597" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>234.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>