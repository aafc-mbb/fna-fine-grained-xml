<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="treatment_page">284</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">deinandra</taxon_name>
    <taxon_name authority="(D. D. Keck) B. G. Baldwin" date="1999" rank="species">halliana</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 468. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus deinandra;species halliana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066468</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="D. D. Keck" date="unknown" rank="species">halliana</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>3: 12. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemizonia;species halliana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 15–120 cm.</text>
      <biological_entity id="o23092" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="120" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems fistulose.</text>
      <biological_entity id="o23093" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="fistulose" value_original="fistulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal blades entire or serrate, faces glabrous (margins and midribs sometimes scabrous or hispid).</text>
      <biological_entity id="o23094" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o23095" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o23096" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in open, corymbiform, racemiform, or paniculiform arrays.</text>
      <biological_entity id="o23097" name="head" name_original="heads" src="d0_s3" type="structure" />
      <biological_entity id="o23098" name="array" name_original="arrays" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="racemiform" value_original="racemiform" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o23097" id="r1578" name="in" negation="false" src="d0_s3" to="o23098" />
    </statement>
    <statement id="d0_s4">
      <text>Bracts subtending heads usually not overlapping involucres.</text>
      <biological_entity id="o23099" name="bract" name_original="bracts" src="d0_s4" type="structure" />
      <biological_entity id="o23100" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o23101" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="usually not" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <relation from="o23099" id="r1579" name="subtending" negation="false" src="d0_s4" to="o23100" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries evenly stipitate-glandular, including margins and apices, usually with some nonglandular, non-pustule-based hairs as well.</text>
      <biological_entity id="o23102" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o23103" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o23104" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <biological_entity id="o23105" name="nonglandular" name_original="nonglandular" src="d0_s5" type="structure" />
      <biological_entity id="o23106" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="non-pustule-based" value_original="non-pustule-based" />
      </biological_entity>
      <relation from="o23102" id="r1580" name="including" negation="false" src="d0_s5" to="o23103" />
      <relation from="o23102" id="r1581" name="including" negation="false" src="d0_s5" to="o23104" />
      <relation from="o23102" id="r1582" modifier="usually" name="with" negation="false" src="d0_s5" to="o23105" />
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series.</text>
      <biological_entity id="o23107" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity id="o23108" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o23107" id="r1583" name="in" negation="false" src="d0_s6" to="o23108" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (8–) 10–14;</text>
      <biological_entity id="o23109" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s7" to="10" to_inclusive="false" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="14" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae deep yellow, 5–10 mm.</text>
      <biological_entity id="o23110" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="depth" src="d0_s8" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 28–60, all functionally staminate;</text>
      <biological_entity id="o23111" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="28" name="quantity" src="d0_s9" to="60" />
        <character is_modifier="false" modifier="functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers yellow or brownish.</text>
      <biological_entity id="o23112" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pappi 0 (reportedly sometimes rudimentary).</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 20.</text>
      <biological_entity id="o23113" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23114" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, edges of alkali sinks, open muddy slopes, heavy, ± alkaline, clayey soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="edges" constraint="of alkali sinks" />
        <character name="habitat" value="alkali sinks" />
        <character name="habitat" value="open muddy slopes" />
        <character name="habitat" value="alkaline" modifier="heavy" />
        <character name="habitat" value="clayey soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Deinandra halliana occurs in the Inner South Coast Ranges. Evident population sizes vary greatly from year to year; active plants may be absent in dry years and form dense stands in some wet years. Madia radiata is morphologically similar to D. halliana; the two species sometimes co-occur and can be difficult to distinguish without close examination of phyllaries and cypselae.</discussion>
  
</bio:treatment>