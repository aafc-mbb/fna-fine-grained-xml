<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="treatment_page">56</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">ericameria</taxon_name>
    <taxon_name authority="(H. M. Hall) G. L. Nesom" date="1990" rank="species">compacta</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>68: 152. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus ericameria;species compacta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066513</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">bloomeri</taxon_name>
    <taxon_name authority="H. M. Hall" date="unknown" rank="subspecies">compactus</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Carnegie Inst. Wash.</publication_title>
      <place_in_publication>389: 199, fig. 68. 1928</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Haplopappus;species bloomeri;subspecies compactus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="(H. M. Hall) L. C. Anderson" date="unknown" rank="species">bloomeri</taxon_name>
    <taxon_name authority="(H. M. Hall) S. F. Blake" date="unknown" rank="variety">compactus</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species bloomeri;variety compactus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Haplopappus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">compactus</taxon_name>
    <taxon_hierarchy>genus Haplopappus;species compactus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 15–50 cm.</text>
      <biological_entity id="o25800" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to spreading, green when young, becoming tan, branched proximally, often with scattered, crinkled hairs and short-stipitate-glandular hairs.</text>
      <biological_entity id="o25801" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="spreading" />
        <character is_modifier="false" modifier="when young" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="becoming" name="coloration" src="d0_s1" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o25802" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s1" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="shape" src="d0_s1" value="crinkled" value_original="crinkled" />
      </biological_entity>
      <biological_entity id="o25803" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s1" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
      </biological_entity>
      <relation from="o25801" id="r2390" modifier="often" name="with" negation="false" src="d0_s1" to="o25802" />
      <relation from="o25801" id="r2391" modifier="often" name="with" negation="false" src="d0_s1" to="o25803" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly erect or ascending;</text>
      <biological_entity id="o25804" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblanceolate to narrowly spatulate, 20–40 × 2–3 (–5) mm, midnerves evident, apices acute to acuminate, faces short-stipitate-glandular, sometimes gland-dotted (sessile), resinous;</text>
      <biological_entity id="o25805" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="narrowly spatulate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s3" to="40" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25806" name="midnerve" name_original="midnerves" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o25807" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s3" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>axillary fascicles of 1–7 reduced leaves often proximally present, distally absent.</text>
      <biological_entity id="o25808" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-stipitate-glandular" value_original="short-stipitate-glandular" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s3" value="gland-dotted" value_original="gland-dotted" />
        <character is_modifier="false" name="coating" src="d0_s3" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="position" src="d0_s4" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o25809" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="fascicles" value_original="fascicles" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="true" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="often proximally" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="distally" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads usually in paniculiform or cymiform arrays, sometimes borne singly.</text>
      <biological_entity id="o25810" name="head" name_original="heads" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement" notes="" src="d0_s5" value="singly" value_original="singly" />
      </biological_entity>
      <biological_entity id="o25811" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o25810" id="r2392" name="in" negation="false" src="d0_s5" to="o25811" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 1–15 mm (bracts 0–3, usually resembling phyllaries).</text>
      <biological_entity id="o25812" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres narrowly campanulate, 12–15 × 4.5–5.5 mm.</text>
      <biological_entity id="o25813" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="width" src="d0_s7" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 18–24 in (3–) 4 series, tan, ovate or lanceolate to elliptic, 4–11 × 0.6–1.2 mm, subequal, mostly chartaceous, midnerves raised, evident, apices acute to cuspidate, herbaceous-tipped, abaxial faces resinous.</text>
      <biological_entity id="o25814" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o25815" from="18" name="quantity" src="d0_s8" to="24" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s8" value="tan" value_original="tan" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="elliptic" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="11" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="mostly" name="pubescence_or_texture" src="d0_s8" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
      <biological_entity id="o25815" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="atypical_quantity" src="d0_s8" to="4" to_inclusive="false" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o25816" name="midnerve" name_original="midnerves" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="raised" value_original="raised" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o25817" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute to cuspidate" value_original="acute to cuspidate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="herbaceous-tipped" value_original="herbaceous-tipped" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25818" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="coating" src="d0_s8" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 0.</text>
      <biological_entity id="o25819" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc-florets 10–16;</text>
      <biological_entity id="o25820" name="disc-floret" name_original="disc-florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s10" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas 9.1–11.5 mm.</text>
      <biological_entity id="o25821" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="9.1" from_unit="mm" name="some_measurement" src="d0_s11" to="11.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae tan to reddish, ellipsoid, 6–10 mm, glabrate or apically sparsely hairy;</text>
      <biological_entity id="o25822" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s12" to="reddish" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="apically sparsely" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi off-white to brown, sometimes reddish, 7–9 mm. 2n = 18.</text>
      <biological_entity id="o25823" name="pappus" name_original="pappi" src="d0_s13" type="structure">
        <character char_type="range_value" from="off-white" name="coloration" src="d0_s13" to="brown" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25824" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="late summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sheltered, rocky to gravelly slopes in pine forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sheltered rocky to gravelly slopes" />
        <character name="habitat" value="pine forests" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2500–3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="2500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Charleston Mountain goldenbush</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>