<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="treatment_page">531</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Gaertner ex Schreber" date="1791" rank="genus">liatris</taxon_name>
    <taxon_name authority="G. L. Nesom &amp; Kral" date="2003" rank="species">patens</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>20: 1579, fig. 2. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus liatris;species patens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250067111</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 35–95 cm.</text>
      <biological_entity id="o2110" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s0" to="95" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Corms globose to depressed-globose.</text>
      <biological_entity id="o2111" name="corm" name_original="corms" src="d0_s1" type="structure">
        <character char_type="range_value" from="globose" name="shape" src="d0_s1" to="depressed-globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems moderately to densely strigoso-hirtellous.</text>
      <biological_entity id="o2112" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s2" value="strigoso-hirtellous" value_original="strigoso-hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal and proximal cauline 1-nerved, narrowly oblanceolate to linear-oblanceolate, 90–180 × 2–4 (–7) mm, gradually or abruptly reduced distally, essentially glabrous (proximal margins sparsely ciliate).</text>
      <biological_entity id="o2113" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="1-nerved" value_original="1-nerved" />
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s3" to="linear-oblanceolate" />
        <character char_type="range_value" from="90" from_unit="mm" name="length" src="d0_s3" to="180" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="abruptly; abruptly; distally" name="size" src="d0_s3" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="essentially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads in loose, (columnar) racemiform arrays.</text>
      <biological_entity id="o2114" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o2115" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s4" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="racemiform" value_original="racemiform" />
      </biological_entity>
      <relation from="o2114" id="r158" name="in" negation="false" src="d0_s4" to="o2115" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles (divergent-ascending, initially diverging at angles of 45–90°) 10–25 (–30) mm.</text>
      <biological_entity id="o2116" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres turbinate-campanulate, (5–) 6–7.5 × 5–7 mm.</text>
      <biological_entity id="o2117" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="turbinate-campanulate" value_original="turbinate-campanulate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s6" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in (2–) 3–4 (–5) series, broadly obovate (outer) to broadly oblong-obovate, strongly unequal, sparsely strigoso-villous (outer) or essentially glabrous, margins with (pinkish purple) hyaline borders, sometimes slightly erose, usually densely ciliate, apices rounded to subtruncate.</text>
      <biological_entity id="o2118" name="phyllary" name_original="phyllaries" src="d0_s7" type="structure">
        <character char_type="range_value" from="broadly obovate" name="shape" notes="" src="d0_s7" to="broadly oblong-obovate" />
        <character is_modifier="false" modifier="strongly" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="strigoso-villous" value_original="strigoso-villous" />
        <character is_modifier="false" modifier="essentially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2119" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s7" to="3" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="5" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <biological_entity id="o2120" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes slightly" name="architecture_or_relief" notes="" src="d0_s7" value="erose" value_original="erose" />
        <character is_modifier="false" modifier="usually densely" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o2121" name="border" name_original="borders" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o2122" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="subtruncate" />
      </biological_entity>
      <relation from="o2118" id="r159" name="in" negation="false" src="d0_s7" to="o2119" />
      <relation from="o2120" id="r160" name="with" negation="false" src="d0_s7" to="o2121" />
    </statement>
    <statement id="d0_s8">
      <text>Florets 7–12;</text>
      <biological_entity id="o2123" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s8" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla-tubes pilose inside.</text>
      <biological_entity id="o2124" name="corolla-tube" name_original="corolla-tubes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 2.5–2.8 mm;</text>
      <biological_entity id="o2125" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: lengths equaling or surpassing corollas, bristles barbellate.</text>
      <biological_entity id="o2126" name="pappus" name_original="pappi" src="d0_s11" type="structure">
        <character is_modifier="false" name="lengths" src="d0_s11" value="equaling" value_original="equaling" />
        <character name="lengths" src="d0_s11" value="surpassing corollas" value_original="surpassing corollas" />
      </biological_entity>
      <biological_entity id="o2127" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="barbellate" value_original="barbellate" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Sep–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Sep" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy pinelands, usually with longleaf pine or slash pine, pine-palmetto flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy pinelands" />
        <character name="habitat" value="longleaf" modifier="usually" />
        <character name="habitat" value="pine" modifier="pine or slash" />
        <character name="habitat" value="pine-palmetto flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  <other_name type="common_name">Georgia or spreading gayfeather</other_name>
  
</bio:treatment>