<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">24</other_info_on_meta>
    <other_info_on_meta type="treatment_page">33</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">baccharis</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray" date="1849" rank="species">texana</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 75. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus baccharis;species texana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066191</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Linosyris</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">texana</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>2: 232. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Linosyris;species texana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials or subshrubs, 25–60 cm (rhizomatous, bases woody).</text>
      <biological_entity id="o24847" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o24848" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems simple, erect or procumbent, rigid (woody proximally), herbaceous and leafy distally (dying back annually), striate-angled, glabrous, non-resinous.</text>
      <biological_entity id="o24849" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="texture" src="d0_s1" value="rigid" value_original="rigid" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="shape" src="d0_s1" value="striate-angled" value_original="striate-angled" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s1" value="non-resinous" value_original="non-resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves present at flowering;</text>
    </statement>
    <statement id="d0_s3">
      <text>sessile;</text>
      <biological_entity id="o24850" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="at flowering" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (1-nerved) linear to narrowly lanceolate, 10–40 × 1–4 mm, bases narrowed, margins minutely undulate, apices acute, faces glabrous, gland-dotted (distal leaves reduced, scalelike).</text>
      <biological_entity id="o24851" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24852" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o24853" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s4" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o24854" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o24855" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="gland-dotted" value_original="gland-dotted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (on short peduncles) in loose corymbiform arrays.</text>
      <biological_entity id="o24856" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o24857" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s5" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
      <relation from="o24856" id="r2287" name="in" negation="false" src="d0_s5" to="o24857" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres campanulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>staminate 4–7 mm, pistillate 7–9 mm.</text>
      <biological_entity id="o24858" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries lanceolate, 1–7 mm, margins scarious, erose-ciliate, medians green (midribs dark, keeled, dilated), apices acute to acuminate (erose-ciliate, abaxial faces glabrous, minutely papillose-gland-dotted).</text>
      <biological_entity id="o24859" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24860" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="erose-ciliate" value_original="erose-ciliate" />
      </biological_entity>
      <biological_entity id="o24861" name="median" name_original="medians" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o24862" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Staminate florets 15–20;</text>
      <biological_entity id="o24863" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s9" to="20" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 4–5 mm.</text>
      <biological_entity id="o24864" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate florets 20–30;</text>
      <biological_entity id="o24865" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s11" to="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas 3.5–4 mm.</text>
      <biological_entity id="o24866" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Cypselae 3–5 mm, prominently 6–8-nerved, glabrous;</text>
      <biological_entity id="o24867" name="cypsela" name_original="cypselae" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s13" value="6-8-nerved" value_original="6-8-nerved" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pappi 11–14 mm.</text>
      <biological_entity id="o24868" name="pappus" name_original="pappi" src="d0_s14" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s14" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry prairies, hillsides, mesas, brushy flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry prairies" />
        <character name="habitat" value="hillsides" />
        <character name="habitat" value="mesas" />
        <character name="habitat" value="brushy flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Okla., Tex.; Mexico (Coahuila, Nuevo León, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18.</number>
  <other_name type="common_name">Prairie baccharis or false willow</other_name>
  <discussion>Baccharis texana is recognized by its low, subshrub habit, simple, more or less herbaceous and leafy stems arising from woody bases, narrow leaves with minutely undulate margins, large pedunculate heads, and erose-ciliate phyllaries with dilated midribs.</discussion>
  
</bio:treatment>