<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">286</other_info_on_meta>
    <other_info_on_meta type="treatment_page">284</other_info_on_meta>
    <other_info_on_meta type="illustration_page">278</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">deinandra</taxon_name>
    <taxon_name authority="(H. M. Hall ex D. D. Keck) B. G. Baldwin" date="1999" rank="species">increscens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">increscens</taxon_name>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus deinandra;species increscens;subspecies increscens</taxon_hierarchy>
    <other_info_on_name type="fna_id">250068241</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems usually erect, sparsely to densely leafy.</text>
      <biological_entity id="o7207" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sparsely to densely" name="architecture" src="d0_s0" value="leafy" value_original="leafy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Heads in open, paniculiform arrays.</text>
      <biological_entity id="o7208" name="head" name_original="heads" src="d0_s1" type="structure" />
      <biological_entity id="o7209" name="array" name_original="arrays" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="open" value_original="open" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o7208" id="r517" name="in" negation="false" src="d0_s1" to="o7209" />
    </statement>
    <statement id="d0_s2">
      <text>Ray-florets 8–13 (–15).</text>
      <biological_entity id="o7210" name="ray-floret" name_original="ray-florets" src="d0_s2" type="structure">
        <character char_type="range_value" from="13" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="15" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s2" to="13" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Disc-florets 11–32.2n = 24.</text>
      <biological_entity id="o7211" name="disc-floret" name_original="disc-florets" src="d0_s3" type="structure">
        <character char_type="range_value" from="11" name="quantity" src="d0_s3" to="32" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7212" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grass-lands, openings in woodlands, chaparral, and coastal scrub, disturbed sites (e.g., roadsides, fallow fields), often sandy or clayey soils, sometimes serpentine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grass-lands" />
        <character name="habitat" value="openings" constraint="in woodlands" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fallow fields" />
        <character name="habitat" value="sandy" modifier="often" />
        <character name="habitat" value="clayey soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9a.</number>
  <discussion>Subspecies increscens occurs in foothills and valleys of the South Coast Ranges, on the immediate Central Coast and northern South Coast, and on Santa Rosa Island.</discussion>
  
</bio:treatment>