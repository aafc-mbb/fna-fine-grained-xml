<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>William A. Weber</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">65</other_info_on_meta>
    <other_info_on_meta type="mention_page">66</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="treatment_page">104</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Lessing" date="1831" rank="subtribe">ecliptinae</taxon_name>
    <taxon_name authority="(Jepson) W. A. Weber" date="1999" rank="genus">AGNORHIZA</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>85: 19. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe ecliptinae;genus AGNORHIZA</taxon_hierarchy>
    <other_info_on_name type="etymology">Possibly Greek agnostos, unknown, and rhiza, root, alluding to the initially unknown roots; in protologue of basionym of type species, Greene stated, “Root unknown.”</other_info_on_name>
    <other_info_on_name type="fna_id">316912</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Nuttall" date="unknown" rank="genus">Balsamorhiza</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="section">Agnorhiza</taxon_name>
    <place_of_publication>
      <publication_title>Man. Fl. Pl. Calif.,</publication_title>
      <place_in_publication>1077. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Balsamorhiza;section Agnorhiza;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 5–60 (–100) cm (taproots relatively massive; caudices sometimes branched).</text>
      <biological_entity id="o1903" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect and branched (mostly distally), or decumbent (and seldom branched distally).</text>
      <biological_entity id="o1904" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline (basal usually scalelike on flowering-stems, well-formed on nonflowering shoots);</text>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate;</text>
      <biological_entity id="o1905" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (3-or 5-nerved or pinnately nerved), cordate-ovate, deltate, deltate-triangular, ovate, ovatelanceolate, or suborbiculate, bases broadly cuneate, subcordate, or truncate, margins entire or dentate to crenate, faces glabrous or hairy (often glanddotted).</text>
      <biological_entity id="o1906" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate-ovate" value_original="cordate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate-triangular" value_original="deltate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbiculate" value_original="suborbiculate" />
      </biological_entity>
      <biological_entity id="o1907" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o1908" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s5" value="dentate to crenate" value_original="dentate to crenate" />
      </biological_entity>
      <biological_entity id="o1909" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate or discoid, borne singly (terminal) or in ± racemiform arrays.</text>
      <biological_entity id="o1910" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="radiate" value_original="radiate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="discoid" value_original="discoid" />
        <character constraint="in more or less racemiform arrays" is_modifier="false" name="arrangement" src="d0_s6" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="in more or less racemiform arrays" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate to ± hemispheric, 12–40 mm diam.</text>
      <biological_entity id="o1911" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="campanulate" name="shape" src="d0_s7" to="more or less hemispheric" />
        <character char_type="range_value" from="12" from_unit="mm" name="diameter" src="d0_s7" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries persistent, (8–) 12–26+ in 2–3+ series (subequal to unequal, outer usually surpassing inner).</text>
      <biological_entity id="o1912" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="8" name="atypical_quantity" src="d0_s8" to="12" to_inclusive="false" />
        <character char_type="range_value" constraint="in series" constraintid="o1913" from="12" name="quantity" src="d0_s8" to="26" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o1913" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="3" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Receptacles flat to convex, paleate (paleae persistent, conduplicate, chartaceous).</text>
      <biological_entity id="o1914" name="receptacle" name_original="receptacles" src="d0_s9" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s9" to="convex" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ray-florets 0 or (1–) 5–21+, pistillate, fertile;</text>
      <biological_entity id="o1915" name="ray-floret" name_original="ray-florets" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s10" to="5" to_inclusive="false" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s10" to="21" upper_restricted="false" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>corollas yellow.</text>
      <biological_entity id="o1916" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Disc-florets 50–150+, bisexual, fertile;</text>
      <biological_entity id="o1917" name="disc-floret" name_original="disc-florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s12" to="150" upper_restricted="false" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>corollas yellow, tubes shorter than cylindric throats, lobes 5, lance-deltate (style-branches stigmatic in 2, barely distinct lines, appendages filiform).</text>
      <biological_entity id="o1918" name="corolla" name_original="corollas" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o1919" name="tube" name_original="tubes" src="d0_s13" type="structure">
        <character constraint="than cylindric throats" constraintid="o1920" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o1920" name="throat" name_original="throats" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity id="o1921" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lance-deltate" value_original="lance-deltate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Cypselae ± prismatic, 3–4-angled (faces glabrous or strigillose);</text>
      <biological_entity id="o1922" name="cypsela" name_original="cypselae" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s14" value="prismatic" value_original="prismatic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="3-4-angled" value_original="3-4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>pappi usually persistent, coroniform (± lacerate, sometimes with 1–4 teeth or scales), sometimes 0.</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 19.</text>
      <biological_entity id="o1923" name="pappus" name_original="pappi" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s15" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="coroniform" value_original="coroniform" />
        <character modifier="sometimes" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o1924" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="19" value_original="19" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>276.</number>
  <discussion>Species 5 (5 in the flora).</discussion>
  <references>
    <reference>Weber, W. A. 1946. A taxonomic and cytological study of the genus Wyethia, family Compositae, with notes on the related genus Balsamorhiza. Amer. Midl. Naturalist 35: 400–452.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants (5–)15–30(–45+) cm; stems decumbent, seldom distally branched; cauline leaves: blades ovate, suborbiculate, elliptic, rounded-deltate, or oblong; involucres ± campanulate, 12–25(–30+) mm diam</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants (20–)40–100 cm; stems erect, usually distally branched; cauline leaves: blades deltate, ovate, or ovate-lanceolate; involucres ± hemispheric, (15–)20–40 mm diam</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves glabrous (shining, finely gland-dotted, glutinous); heads held among or beyond leaves; ray florets (7–)8–12, laminae (12–)20–30(–35) mm; cypselae 7–9 mm; pappi 0 or coroniform, 0.1–0.3 mm</description>
      <determination>1 Agnorhiza bolanderi</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves silky-villous to strigillose, glabrescent; heads in axils of (and overtopped by) leaves; ray florets 5–8(–9), laminae 8–18 mm; cypselae 9–10 mm; pappi coroniform, 1–1.5+ mm</description>
      <determination>2 Agnorhiza ovata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ray florets 0 or 2–3</description>
      <determination>3 Agnorhiza invenusta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Ray florets 10–23</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades: faces tomentulose to pilosulous (and gland-dotted); cypselae 8–12 mm; pappi (0.5–)1–2(–3) mm</description>
      <determination>4 Agnorhiza elata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades: faces sparsely hispid or scabrous (and finely gland-dotted, green, shin-ing, often vernicose); cypselae ca. 6 mm; pappi 0.1–1 mm</description>
      <determination>5 Agnorhiza reticulata</determination>
    </key_statement>
  </key>
</bio:treatment>