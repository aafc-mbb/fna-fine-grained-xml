<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">267</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="treatment_page">324</other_info_on_meta>
    <other_info_on_meta type="illustration_page">321</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="Hooker" date="1834" rank="species">grandiflorus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 18, plate 123. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species grandiflorus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066607</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">simplex</taxon_name>
    <taxon_hierarchy>genus Erigeron;species simplex;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 2–25 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, fibrous-rooted, caudices or rhizomes crownlike or branches relatively short and thick.</text>
      <biological_entity id="o4687" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="25" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o4688" name="caudex" name_original="caudices" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crownlike" value_original="crownlike" />
      </biological_entity>
      <biological_entity id="o4689" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character is_modifier="false" name="shape" src="d0_s1" value="crownlike" value_original="crownlike" />
      </biological_entity>
      <biological_entity id="o4690" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to decumbent-ascending, sparsely to moderately pilose to villoso-hirsute, often stipitate-glandular over all or part.</text>
      <biological_entity id="o4691" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="decumbent-ascending" />
        <character char_type="range_value" from="pilose" modifier="sparsely to moderately; moderately" name="pubescence" src="d0_s2" to="villoso-hirsute" />
        <character constraint="over part" constraintid="o4692" is_modifier="false" modifier="often" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o4692" name="part" name_original="part" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline (petioles equaling or shorter than blades);</text>
      <biological_entity id="o4693" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades oblanceolate to obovate or spatulate, 10–60 (–90) × 3–8 (–14) mm, cauline abruptly or gradually reduced distally, margins entire (apices rounded), faces sparsely hirsutulous or villous to sparsely strigose or glabrate, sometimes sparsely glandular.</text>
      <biological_entity id="o4694" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate or spatulate" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o4695" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o4696" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4697" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="villous" name="pubescence" src="d0_s4" to="sparsely strigose or glabrate" />
        <character is_modifier="false" modifier="sometimes sparsely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads 1.</text>
      <biological_entity id="o4698" name="head" name_original="heads" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 5–8 (–10) × 8–20 mm.</text>
      <biological_entity id="o4699" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series (green or purplish), moderately to densely woolly-villous (hairs flattened, cross-walls sometimes reddish), minutely glandular at least apically.</text>
      <biological_entity id="o4700" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" notes="" src="d0_s7" value="woolly-villous" value_original="woolly-villous" />
        <character is_modifier="false" modifier="minutely; at-least apically; apically" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o4701" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o4700" id="r426" name="in" negation="false" src="d0_s7" to="o4701" />
    </statement>
    <statement id="d0_s8">
      <text>Ray-florets 50–130;</text>
      <biological_entity id="o4702" name="ray-floret" name_original="ray-florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="50" name="quantity" src="d0_s8" to="130" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corollas blue to pink or purplish, rarely white, 7–11 (–15) mm (mostly 1–2 mm wide), laminae coiling.</text>
      <biological_entity id="o4703" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="blue" name="coloration" src="d0_s9" to="pink or purplish" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4704" name="lamina" name_original="laminae" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="coiling" value_original="coiling" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Disc corollas 2.4–4 (–5) mm.</text>
      <biological_entity constraint="disc" id="o4705" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 1.8–2.4 mm, 2-nerved, faces strigose;</text>
      <biological_entity id="o4706" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o4707" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi: outer of setae, inner of (7–) 10–18 (–22) bristles.</text>
      <biological_entity id="o4709" name="seta" name_original="setae" src="d0_s12" type="structure" />
      <biological_entity id="o4710" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="atypical_quantity" src="d0_s12" to="10" to_inclusive="false" />
        <character char_type="range_value" from="18" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s12" to="22" />
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s12" to="18" />
      </biological_entity>
      <relation from="o4708" id="r427" name="outer of" negation="false" src="d0_s12" to="o4709" />
      <relation from="o4708" id="r428" name="inner of" negation="false" src="d0_s12" to="o4710" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 18, 27.</text>
      <biological_entity id="o4708" name="pappus" name_original="pappi" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o4711" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
        <character name="quantity" src="d0_s13" value="27" value_original="27" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug(–Sep).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
        <character name="flowering time" char_type="atypical_range" to="Sep" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky sites, meadows, alpine or near timberline</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky sites" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="timberline" modifier="alpine or near" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2900–4200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4200" to_unit="m" from="2900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Ariz., Colo., Idaho, Mont., N.Mex., Oreg., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>122.</number>
  <other_name type="common_name">Rocky Mountain alpine fleabane</other_name>
  <discussion>S. A. Spongberg (1971) recognized only the triploid populations as Erigeron grandiflorus and assigned the diploid ones to E. simplex. He hypothesized that the triploids incorporate genomic elements from an ancestor other than E. simplex. Based on his comments and annotations, however, triploids in southern Canada and the western United States apparently differ from the much more widespread diploids only quantitatively, having involucres and florets at the higher end of size ranges. Morphologic distinctions between the ploidal races do not provide a basis for consistent distinction. Spongberg (p. 200) also noted that “because of the intergrading of morphologic features of plants of Erigeron grandiflorus...the single most important criterion indicative of this taxon is highly irregular [in shape] and greatly abortive pollen.” These pollen features result from meiotic anomalies associated with the triploid condition.</discussion>
  <discussion>Specimen citations by A. Cronquist (1947) for Erigeron grandiflorus were mostly from collections of the species treated here as E. porsildii. He also cited two collections from southwestern Alberta; those and the type collection of E. grandiflorus (from the same region) are disjunct by more than 1500 kilometers from the more northern range of E. porsildii and instead lie at the northern extremity of the range of what previously has generally been identified as E. simplex.</discussion>
  
</bio:treatment>