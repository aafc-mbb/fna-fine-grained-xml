<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">461</other_info_on_meta>
    <other_info_on_meta type="mention_page">462</other_info_on_meta>
    <other_info_on_meta type="mention_page">463</other_info_on_meta>
    <other_info_on_meta type="mention_page">472</other_info_on_meta>
    <other_info_on_meta type="treatment_page">466</other_info_on_meta>
    <other_info_on_meta type="illustration_page">467</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">eupatorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">eupatorium</taxon_name>
    <taxon_name authority="(Lamarck) Small" date="1894" rank="species">capillifolium</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Torrey Bot. Club</publication_title>
      <place_in_publication>5: 311. 1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe eupatorieae;genus eupatorium;species capillifolium</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416526</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Artemisia</taxon_name>
    <taxon_name authority="Lamarck" date="unknown" rank="species">capillifolia</taxon_name>
    <place_of_publication>
      <publication_title>in J. Lamarck et al., Encycl.</publication_title>
      <place_in_publication>1: 267. 1783</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Artemisia;species capillifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 50–200 cm.</text>
      <biological_entity id="o13770" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="200" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (from short caudices) multiple, branched distally, puberulent throughout.</text>
      <biological_entity id="o13771" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="multiple" value_original="multiple" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves opposite (proximal) or alternate (nodes often appearing leafy because of development of leaves on lateral buds without axis elongation);</text>
    </statement>
    <statement id="d0_s3">
      <text>sessile;</text>
      <biological_entity id="o13772" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades (often ternately lobed) or lobes linear, 5–100 × 0.2–0.5 (–1) mm, bases ± cuneate, margins entire (strongly revolute), apices rounded to acute, faces glabrate, glanddotted.</text>
      <biological_entity id="o13773" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13774" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13775" name="base" name_original="bases" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o13776" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13777" name="apex" name_original="apices" src="d0_s4" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
      <biological_entity id="o13778" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads in dense, paniculiform arrays.</text>
      <biological_entity id="o13779" name="head" name_original="heads" src="d0_s5" type="structure" />
      <biological_entity id="o13780" name="array" name_original="arrays" src="d0_s5" type="structure">
        <character is_modifier="true" name="density" src="d0_s5" value="dense" value_original="dense" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o13779" id="r944" name="in" negation="false" src="d0_s5" to="o13780" />
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 8–10 in 2–3 series, oblong, 0.5–2.5 × 0.2–0.5 mm, apices acuminate and mucronate, abaxial faces glabrous or glabrate, not or little, if at all, glanddotted.</text>
      <biological_entity id="o13781" name="phyllary" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o13782" from="8" name="quantity" src="d0_s6" to="10" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s6" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13782" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o13783" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13784" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Florets 5;</text>
      <biological_entity id="o13785" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas 2–2.5 mm.</text>
      <biological_entity id="o13786" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cypselae 1–1.7 mm;</text>
      <biological_entity id="o13787" name="cypsela" name_original="cypselae" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pappi of 20–30 bristles 2–2.5 mm. 2n = 20.</text>
      <biological_entity id="o13788" name="pappus" name_original="pappi" src="d0_s10" type="structure" />
      <biological_entity id="o13789" name="bristle" name_original="bristles" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s10" to="30" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13790" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="20" value_original="20" />
      </biological_entity>
      <relation from="o13788" id="r945" name="consist_of" negation="false" src="d0_s10" to="o13789" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Old fields, open sites, roadsides, flatwoods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="old fields" />
        <character name="habitat" value="open sites" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="flatwoods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Conn., Del., Fla., Ga., Ky., La., Md., Mass., Miss., Mo., N.J., N.C., Okla., Pa., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Dogfennel</other_name>
  
</bio:treatment>