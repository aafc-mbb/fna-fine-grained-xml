<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Robert L. Carr,Gerald D. Carr</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">256</other_info_on_meta>
    <other_info_on_meta type="mention_page">276</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="mention_page">291</other_info_on_meta>
    <other_info_on_meta type="treatment_page">269</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Nuttall" date="1841" rank="genus">OSMADENIA</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Amer. Philos. Soc., n. s.</publication_title>
      <place_in_publication>7: 391. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus OSMADENIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek osma, odor, and aden, gland, alluding to strong-scented, glandular herbage</other_info_on_name>
    <other_info_on_name type="fna_id">123327</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 5–40 cm (self-incompatible; herbage strongly scented).</text>
      <biological_entity id="o9325" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect (branched, branchlets relatively many, commonly spreading, threadlike, densely glandular, scabrous to shaggy-hairy).</text>
      <biological_entity id="o9326" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o9328" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s4">
      <text>sessile;</text>
      <biological_entity id="o9327" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades linear, margins entire, faces hispidulous and glandular (proximally ± ciliate or shaggy-hairy).</text>
      <biological_entity id="o9329" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o9330" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o9331" name="face" name_original="faces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispidulous" value_original="hispidulous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Heads radiate, in loose, cymiform arrays.</text>
      <biological_entity id="o9332" name="head" name_original="heads" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s6" value="radiate" value_original="radiate" />
      </biological_entity>
      <biological_entity id="o9333" name="array" name_original="arrays" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="cymiform" value_original="cymiform" />
      </biological_entity>
      <relation from="o9332" id="r647" name="in" negation="false" src="d0_s6" to="o9333" />
    </statement>
    <statement id="d0_s7">
      <text>Peduncular bracts with tack-glands 0 (strigillose, strongly glandular, margins often proximally pectinate, ciliate).</text>
      <biological_entity constraint="peduncular" id="o9334" name="bract" name_original="bracts" src="d0_s7" type="structure" />
      <biological_entity id="o9335" name="tack-gland" name_original="tack-glands" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o9334" id="r648" name="with" negation="false" src="d0_s7" to="o9335" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres ovoid, 2–4 mm diam.</text>
      <biological_entity id="o9336" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries falling, 3–5 in 1 series (each partly enveloping a ray cypsela, apices acute to attenuate).</text>
      <biological_entity id="o9337" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="falling" value_original="falling" />
        <character char_type="range_value" constraint="in series" constraintid="o9338" from="3" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <biological_entity id="o9338" name="series" name_original="series" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat, glabrous, paleate (paleae persistent, in 1 series between rays and discs, connate, forming cups, distinct apices acute, often apiculate).</text>
      <biological_entity id="o9339" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="paleate" value_original="paleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Ray-florets 3–5, pistillate, fertile;</text>
      <biological_entity id="o9340" name="ray-floret" name_original="ray-florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s11" to="5" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas wholly or mostly white (laminae 3-lobed, lobes sometimes each with prominent medial red blotch, overall often fading reddish, sinuses ± equaling laminae).</text>
      <biological_entity id="o9341" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Disc-florets 3–10, bisexual, fertile;</text>
      <biological_entity id="o9342" name="disc-floret" name_original="disc-florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s13" to="10" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas usually wholly white, sometimes lobes each with prominent medial red blotch or reddish overall, tubes shorter than throats, lobes 5, deltate (anthers ± dark purple; styles glabrous proximal to branches).</text>
      <biological_entity id="o9343" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually wholly" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o9344" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character constraint="than throats" constraintid="o9347" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="medial" id="o9345" name="blotch" name_original="blotch" src="d0_s14" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s14" value="red" value_original="red" />
      </biological_entity>
      <biological_entity id="o9346" name="tube" name_original="tubes" src="d0_s14" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
        <character constraint="than throats" constraintid="o9347" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o9347" name="throat" name_original="throats" src="d0_s14" type="structure" />
      <biological_entity id="o9348" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="5" value_original="5" />
        <character is_modifier="false" name="shape" src="d0_s14" value="deltate" value_original="deltate" />
      </biological_entity>
      <relation from="o9344" id="r649" name="with" negation="false" src="d0_s14" to="o9345" />
      <relation from="o9344" id="r650" name="with" negation="false" src="d0_s14" to="o9346" />
    </statement>
    <statement id="d0_s15">
      <text>Cypselae (rays) ± obcompressed (± 3-angled, abaxial sides usually broadly 2-faced, angles between those faces usually 90+°, adaxial sides nearly flat), apices beaked, beaks off-center, faces rugose, glabrous;</text>
      <biological_entity id="o9349" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s15" value="obcompressed" value_original="obcompressed" />
      </biological_entity>
      <biological_entity id="o9350" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="beaked" value_original="beaked" />
      </biological_entity>
      <biological_entity id="o9351" name="beak" name_original="beaks" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="off-center" value_original="off-center" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>or (discs) narrowly clavate, appressed-hairy;</text>
      <biological_entity id="o9352" name="face" name_original="faces" src="d0_s15" type="structure">
        <character is_modifier="false" name="relief" src="d0_s15" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s16" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="appressed-hairy" value_original="appressed-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>pappi (rays) 0, or (discs) of 4–5 lance-attenuate to aristate scales alternating with 4–5 shorter, ± fimbriate scales.</text>
      <biological_entity id="o9353" name="pappus" name_original="pappi" src="d0_s17" type="structure">
        <character name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s17" to="5" />
        <character char_type="range_value" from="lance-attenuate" name="shape" src="d0_s17" to="aristate" />
      </biological_entity>
      <biological_entity id="o9355" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s17" to="5" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s17" value="shorter" value_original="shorter" />
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s17" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>x = 9.</text>
      <biological_entity id="o9354" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character constraint="with scales" constraintid="o9355" is_modifier="false" name="arrangement" src="d0_s17" value="alternating" value_original="alternating" />
      </biological_entity>
      <biological_entity constraint="x" id="o9356" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., nw Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="nw Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>336.</number>
  <discussion>Species 1.</discussion>
  <references>
    <reference>Carr, G. D. 1977. A cytological conspectus of the genus Calycadenia (Asteraceae): An example of contrasting modes of evolution. Amer. J. Bot. 64: 694–703.</reference>
  </references>
  
</bio:treatment>