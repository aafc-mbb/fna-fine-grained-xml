<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">261</other_info_on_meta>
    <other_info_on_meta type="treatment_page">318</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="A. Gray" date="1868" rank="species">supplex</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 353. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species supplex</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250066685</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials 15–40 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted (taproots/primary-axes usually not evident or not collected), forming systems of relatively slender, rhizomelike caudex branches.</text>
      <biological_entity id="o6893" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o6894" name="system" name_original="systems" src="d0_s1" type="structure" />
      <biological_entity constraint="caudex" id="o6895" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="relatively" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="true" name="architecture" src="d0_s1" value="rhizomelike" value_original="rhizomelike" />
      </biological_entity>
      <relation from="o6893" id="r627" name="forming" negation="false" src="d0_s1" to="o6894" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending, sparsely spreading-hairy, eglandular.</text>
      <biological_entity id="o6896" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="spreading-hairy" value_original="spreading-hairy" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline;</text>
      <biological_entity id="o6897" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>proximal blades narrowly oblanceolate, 40–80 × 4–7 mm, gradually reduced and linear distally (bases not clasping), margins entire, ciliate, faces glabrous or glabrate, eglandular.</text>
      <biological_entity constraint="proximal" id="o6898" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s4" to="80" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character is_modifier="false" modifier="distally" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity id="o6899" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o6900" name="face" name_original="faces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Heads (discoid) 1 (– 2).</text>
      <biological_entity id="o6901" name="head" name_original="heads" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="2" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Involucres 7–11 × 14–20 mm.</text>
      <biological_entity id="o6902" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s6" to="11" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Phyllaries in 2–3 series, sparsely to densely villoso-hirsute, densely minutely glandular.</text>
      <biological_entity id="o6903" name="phyllarie" name_original="phyllaries" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" notes="" src="d0_s7" value="villoso-hirsute" value_original="villoso-hirsute" />
        <character is_modifier="false" modifier="densely minutely" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o6904" name="series" name_original="series" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o6903" id="r628" name="in" negation="false" src="d0_s7" to="o6904" />
    </statement>
    <statement id="d0_s8">
      <text>Ray (pistillate) florets 0.</text>
      <biological_entity constraint="ray" id="o6905" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc corollas 4.7–7 mm.</text>
      <biological_entity constraint="disc" id="o6906" name="corolla" name_original="corollas" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.7" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cypselae 1.8–2.2 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o6907" name="cypsela" name_original="cypselae" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o6908" name="face" name_original="faces" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pappi: outer of setae, inner of 17–30 bristles.</text>
      <biological_entity id="o6909" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o6910" name="seta" name_original="setae" src="d0_s11" type="structure" />
      <biological_entity id="o6911" name="bristle" name_original="bristles" src="d0_s11" type="structure">
        <character char_type="range_value" from="17" is_modifier="true" name="quantity" src="d0_s11" to="30" />
      </biological_entity>
      <relation from="o6909" id="r629" name="outer of" negation="false" src="d0_s11" to="o6910" />
      <relation from="o6909" id="r630" name="inner of" negation="false" src="d0_s11" to="o6911" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal areas, bluffs, meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal areas" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>108.</number>
  <other_name type="common_name">Supple fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>