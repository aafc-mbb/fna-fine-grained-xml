<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">289</other_info_on_meta>
    <other_info_on_meta type="treatment_page">290</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="(Hooker &amp; Arnott) Torrey &amp; A. Gray" date="1841" rank="species">concinnus</taxon_name>
    <taxon_name authority="D. C. Eaton in S. Watson" date="1871" rank="variety">condensatus</taxon_name>
    <place_of_publication>
      <publication_title>in S. Watson, Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>151. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species concinnus;variety condensatus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250068328</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="(D. C. Eaton) Greene" date="unknown" rank="species">condensatus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species condensatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erigeron</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">pumilus</taxon_name>
    <taxon_name authority="(D. C. Eaton) Cronquist" date="unknown" rank="variety">condensatus</taxon_name>
    <taxon_hierarchy>genus Erigeron;species pumilus;variety condensatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mostly 4–10 cm.</text>
      <biological_entity id="o16007" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± scapiform, densely piloso-hispid.</text>
      <biological_entity id="o16008" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s1" value="scapiform" value_original="scapiform" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="piloso-hispid" value_original="piloso-hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly basal, cauline relatively few, greatly reduced.</text>
      <biological_entity id="o16009" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16010" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o16011" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="relatively" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="false" modifier="greatly" name="size" src="d0_s2" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads 1.</text>
      <biological_entity id="o16012" name="head" name_original="heads" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open, gravelly or sandy slopes, sagebrush, juniper, pinyon-juniper</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly" modifier="open" />
        <character name="habitat" value="sandy slopes" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="juniper" />
        <character name="habitat" value="pinyon-juniper" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>30b.</number>
  <other_name type="past_name">concinnum var. condensatum</other_name>
  <discussion>Variety condensatus is distinctive in habit (single heads on nearly scapiform stems), but intergrades with var. concinnus are common in some areas. And although populations of var. condensatus appear to be concentrated in the southern and eastern part of the range of the species, even there those enclaves are scattered within the larger range of var. concinnus, and the evolutionary nature of the variation pattern is not clear.</discussion>
  
</bio:treatment>