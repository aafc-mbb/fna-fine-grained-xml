<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">272</other_info_on_meta>
    <other_info_on_meta type="treatment_page">309</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erigeron</taxon_name>
    <taxon_name authority="G. L. Nesom" date="1990" rank="species">anchana</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>69: 231. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus erigeron;species anchana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066549</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials 7–22 cm;</text>
    </statement>
    <statement id="d0_s1">
      <text>taprooted, caudex branches relatively thick, short, retaining old leaf-bases.</text>
      <biological_entity id="o25593" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s0" to="22" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o25594" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="relatively" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o25595" name="leaf-base" name_original="leaf-bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s1" value="old" value_original="old" />
      </biological_entity>
      <relation from="o25594" id="r2365" name="retaining" negation="false" src="d0_s1" to="o25595" />
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to somewhat pendent, sparsely strigose to glabrate, eglandular.</text>
      <biological_entity id="o25596" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending to somewhat" value_original="ascending to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s2" value="pendent" value_original="pendent" />
        <character char_type="range_value" from="sparsely strigose" name="pubescence" src="d0_s2" to="glabrate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="eglandular" value_original="eglandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal (persistent) and cauline (petioles about 2 times blade lengths);</text>
      <biological_entity id="o25597" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal blades obovate-spatulate, (30–) 40–90 × 3–8 mm, margins entire or with 1 (–2) pairs of deep lobes or teeth, faces sparsely ascending-strigose to glabrate, eglandular;</text>
      <biological_entity constraint="basal" id="o25598" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate-spatulate" value_original="obovate-spatulate" />
        <character char_type="range_value" from="30" from_unit="mm" name="atypical_length" src="d0_s4" to="40" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_unit="mm" name="length" src="d0_s4" to="90" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25599" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="with 1(-2) pairs" value_original="with 1(-2) pairs" />
      </biological_entity>
      <biological_entity id="o25600" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s4" to="2" />
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o25601" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character is_modifier="true" name="depth" src="d0_s4" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o25602" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity id="o25603" name="face" name_original="faces" src="d0_s4" type="structure">
        <character char_type="range_value" from="sparsely ascending-strigose" name="pubescence" src="d0_s4" to="glabrate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o25599" id="r2366" name="with" negation="false" src="d0_s4" to="o25600" />
      <relation from="o25600" id="r2367" name="part_of" negation="false" src="d0_s4" to="o25601" />
      <relation from="o25600" id="r2368" name="part_of" negation="false" src="d0_s4" to="o25602" />
    </statement>
    <statement id="d0_s5">
      <text>cauline blades oblanceolate at midstem, only slightly reduced distally.</text>
      <biological_entity constraint="cauline" id="o25604" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character constraint="at midstem" constraintid="o25605" is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" modifier="only slightly; distally" name="size" notes="" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o25605" name="midstem" name_original="midstem" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Heads 1–3 (from distal branches).</text>
      <biological_entity id="o25606" name="head" name_original="heads" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres 3–3.5 × 5–7 mm.</text>
      <biological_entity id="o25607" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries in 3–4 series, (purplish, midveins orange-resinous, slightly swollen) glabrous or sparsely strigose, sometimes sparsely minutely glandular.</text>
      <biological_entity id="o25608" name="phyllarie" name_original="phyllaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="strigose" value_original="strigose" />
        <character is_modifier="false" modifier="sometimes sparsely minutely" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o25609" name="series" name_original="series" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <relation from="o25608" id="r2369" name="in" negation="false" src="d0_s8" to="o25609" />
    </statement>
    <statement id="d0_s9">
      <text>Ray-florets 14–36;</text>
      <biological_entity id="o25610" name="ray-floret" name_original="ray-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s9" to="36" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas white, drying white to lavender, 6–8 mm, laminae reflexing.</text>
      <biological_entity id="o25611" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="drying" value_original="drying" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="lavender" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25612" name="lamina" name_original="laminae" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexing" value_original="reflexing" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Disc corollas 2–2.8 mm (veins prominently orange-resinous).</text>
      <biological_entity constraint="disc" id="o25613" name="corolla" name_original="corollas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Cypselae 1.2–1.4 mm, 2-nerved, faces sparsely strigose;</text>
      <biological_entity id="o25614" name="cypsela" name_original="cypselae" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-nerved" value_original="2-nerved" />
      </biological_entity>
      <biological_entity id="o25615" name="face" name_original="faces" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pappi: outer of setae or scales, inner of 19–26 bristles.</text>
      <biological_entity id="o25617" name="seta" name_original="setae" src="d0_s13" type="structure" />
      <biological_entity id="o25618" name="scale" name_original="scales" src="d0_s13" type="structure" />
      <biological_entity id="o25619" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="19" is_modifier="true" name="quantity" src="d0_s13" to="26" />
      </biological_entity>
      <relation from="o25616" id="r2370" name="outer of" negation="false" src="d0_s13" to="o25617" />
      <relation from="o25616" id="r2371" name="outer of" negation="false" src="d0_s13" to="o25618" />
      <relation from="o25616" id="r2372" name="inner of" negation="false" src="d0_s13" to="o25619" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity id="o25616" name="pappus" name_original="pappi" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o25620" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Apr–)Jun–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliff crevices, ledges, soil pockets among boulders</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliff crevices" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="soil pockets" constraint="among boulders" />
        <character name="habitat" value="boulders" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>88.</number>
  <other_name type="common_name">Sierra Ancha fleabane</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>