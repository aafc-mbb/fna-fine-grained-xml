<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="treatment_page">200</other_info_on_meta>
    <other_info_on_meta type="illustration_page">200</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Hooker" date="1834" rank="genus">townsendia</taxon_name>
    <taxon_name authority="Beaman" date="1957" rank="species">hookeri</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>183: 95, plates 17, 20, figs. 5, 6. 1957</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus townsendia;species hookeri</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250067768</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Townsendia</taxon_name>
    <taxon_name authority="Dorn" date="unknown" rank="species">nuttallii</taxon_name>
    <taxon_hierarchy>genus Townsendia;species nuttallii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials, 1–3 cm (± pulvinate).</text>
      <biological_entity id="o25411" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s0" to="3" to_unit="cm" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± erect;</text>
      <biological_entity id="o25412" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes 0.1–1 mm, ± strigose.</text>
      <biological_entity id="o25413" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline, blades narrowly oblanceolate to linear, 5–25 (–40+) × 1–1.5 (–3) mm, fleshy, faces ± strigoso-sericeous, little, if at all.</text>
      <biological_entity id="o25414" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
      <biological_entity id="o25415" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s3" to="linear" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="40" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s3" to="25" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o25416" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="strigoso-sericeous" value_original="strigoso-sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads ± sessile.</text>
      <biological_entity id="o25417" name="head" name_original="heads" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Involucres ± campanulate, 8–18+ mm diam.</text>
      <biological_entity id="o25418" name="involucre" name_original="involucres" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s5" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s5" to="18" to_unit="mm" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Phyllaries 50–65+ in 5+ series, the longer ± lance-linear, 7–11 (–13) mm (l/w = 6–9), apices attenuate, abaxial faces sparsely strigose or glabrate.</text>
      <biological_entity id="o25419" name="phyllarie" name_original="phyllaries" src="d0_s6" type="structure">
        <character char_type="range_value" constraint="in series" constraintid="o25420" from="50" name="quantity" src="d0_s6" to="65" upper_restricted="false" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s6" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="more or less" name="arrangement_or_course_or_shape" src="d0_s6" value="lance-linear" value_original="lance-linear" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="13" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25420" name="series" name_original="series" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o25421" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o25422" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets 15–35;</text>
      <biological_entity id="o25423" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" name="quantity" src="d0_s7" to="35" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>corollas white adaxially, laminae 8–12+ mm, glabrous abaxially.</text>
      <biological_entity id="o25424" name="corolla" name_original="corollas" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s8" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o25425" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" upper_restricted="false" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 120–180+;</text>
      <biological_entity id="o25426" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="120" name="quantity" src="d0_s9" to="180" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas 4–5 (–6+) mm.</text>
      <biological_entity id="o25427" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="6" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae 3.5–4.5 mm, faces hairy, hair tips glochidiform;</text>
      <biological_entity id="o25428" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25429" name="face" name_original="faces" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="hair" id="o25430" name="tip" name_original="tips" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="glochidiform" value_original="glochidiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi persistent;</text>
    </statement>
    <statement id="d0_s13">
      <text>on ray cypselae ca. 30 subulate to setiform scales (1–) 5–7 mm;</text>
      <biological_entity id="o25431" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character name="quantity" src="d0_s13" value="30" value_original="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>on disc cypselae ca. 30 subulate to setiform scales 5.5–7.5+ mm.</text>
      <biological_entity id="o25432" name="scale" name_original="scales" src="d0_s13" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s13" to="setiform" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character name="quantity" src="d0_s14" value="30" value_original="30" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 18.</text>
      <biological_entity id="o25433" name="scale" name_original="scales" src="d0_s14" type="structure">
        <character char_type="range_value" from="subulate" name="shape" src="d0_s14" to="setiform" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s14" to="7.5" to_unit="mm" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25434" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly benches, sandy slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly benches" />
        <character name="habitat" value="sandy slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Sask., Yukon; Alaska, Colo., Idaho, Mont., Nebr., N.Dak., S.Dak., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16.</number>
  
</bio:treatment>