<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="treatment_page">286</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">deinandra</taxon_name>
    <taxon_name authority="(D. D. Keck) B. G. Baldwin" date="1999" rank="species">pallida</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>9: 469. 1999</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus deinandra;species pallida</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066474</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="D. D. Keck" date="unknown" rank="species">pallida</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>3: 8. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemizonia;species pallida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 9–100 cm.</text>
      <biological_entity id="o14671" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± solid or fistulose.</text>
      <biological_entity id="o14672" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="solid" value_original="solid" />
        <character is_modifier="false" name="shape" src="d0_s1" value="fistulose" value_original="fistulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: proximal blades pinnatifid to toothed, faces ± hirsute and sometimes sparsely stipitate-glandular as well.</text>
      <biological_entity id="o14673" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o14674" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s2" to="toothed" />
      </biological_entity>
      <biological_entity id="o14675" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sometimes sparsely; well" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Heads in crowded to open, corymbiform or paniculiform arrays.</text>
      <biological_entity id="o14676" name="head" name_original="heads" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="in crowded to open" name="architecture" src="d0_s3" value="corymbiform" value_original="corymbiform" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Bracts subtending heads sometimes overlapping proximal 0–1/2 of each involucre.</text>
      <biological_entity id="o14677" name="bract" name_original="bracts" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s4" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="position" src="d0_s4" value="proximal" value_original="proximal" />
        <character char_type="range_value" constraint="of involucre" constraintid="o14679" from="0" name="quantity" src="d0_s4" to="1/2" />
      </biological_entity>
      <biological_entity id="o14678" name="head" name_original="heads" src="d0_s4" type="structure" />
      <biological_entity id="o14679" name="involucre" name_original="involucre" src="d0_s4" type="structure" />
      <relation from="o14677" id="r1007" name="subtending" negation="false" src="d0_s4" to="o14678" />
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries ± evenly and minutely stipitate-glandular, including margins and apices, with nonglandular, non-pustule-based hairs as well.</text>
      <biological_entity id="o14680" name="phyllary" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o14681" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o14682" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <biological_entity id="o14683" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="non-pustule-based" value_original="non-pustule-based" />
      </biological_entity>
      <relation from="o14680" id="r1008" name="including" negation="false" src="d0_s5" to="o14681" />
      <relation from="o14680" id="r1009" name="including" negation="false" src="d0_s5" to="o14682" />
      <relation from="o14680" id="r1010" modifier="well" name="with" negation="false" src="d0_s5" to="o14683" />
    </statement>
    <statement id="d0_s6">
      <text>Paleae in 1 series.</text>
      <biological_entity id="o14684" name="palea" name_original="paleae" src="d0_s6" type="structure" />
      <biological_entity id="o14685" name="series" name_original="series" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o14684" id="r1011" name="in" negation="false" src="d0_s6" to="o14685" />
    </statement>
    <statement id="d0_s7">
      <text>Ray-florets (7–) 8–12;</text>
      <biological_entity id="o14686" name="ray-floret" name_original="ray-florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" name="atypical_quantity" src="d0_s7" to="8" to_inclusive="false" />
        <character char_type="range_value" from="8" name="quantity" src="d0_s7" to="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>laminae pale-yellow, 6–12 mm.</text>
      <biological_entity id="o14687" name="lamina" name_original="laminae" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Disc-florets 10–21, all or mostly functionally staminate;</text>
      <biological_entity id="o14688" name="disc-floret" name_original="disc-florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="21" />
        <character is_modifier="false" modifier="mostly functionally" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers yellow or brownish.</text>
      <biological_entity id="o14689" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pappi usually of 4–9 linear or oblong scales 0.8–1.1 mm, or of 1–5 subulate to setiform scales 0.1–0.9 mm, rarely 0.2n = 18.</text>
      <biological_entity id="o14690" name="pappus" name_original="pappi" src="d0_s11" type="structure" />
      <biological_entity id="o14691" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s11" to="9" />
        <character is_modifier="true" name="shape" src="d0_s11" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14692" name="scale" name_original="scales" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="5" />
        <character char_type="range_value" from="subulate" is_modifier="true" name="shape" src="d0_s11" to="setiform" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.9" to_unit="mm" />
        <character modifier="rarely" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14693" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="18" value_original="18" />
      </biological_entity>
      <relation from="o14690" id="r1012" name="consist_of" negation="false" src="d0_s11" to="o14691" />
      <relation from="o14690" id="r1013" name="consist_of" negation="false" src="d0_s11" to="o14692" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grasslands, open woodlands and shrublands, barrens, disturbed sites, sandy, silty, or clayey soils, often ± alkaline</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="open woodlands" />
        <character name="habitat" value="shrublands" />
        <character name="habitat" value="barrens" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="silty" modifier="sandy" />
        <character name="habitat" value="clayey soils" />
        <character name="habitat" value="alkaline" modifier="often" />
        <character name="habitat" value="sandy" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>70–900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="70" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>Deinandra pallida occurs in the southern San Joaquin Valley and the bordering Inner South Coast Ranges and southern Sierra Nevada foothills.</discussion>
  
</bio:treatment>