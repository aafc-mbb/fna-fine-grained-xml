<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 15:57:01</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="treatment_page">288</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">heliantheae</taxon_name>
    <taxon_name authority="Bentham &amp; Hooker f." date="1873" rank="subtribe">madiinae</taxon_name>
    <taxon_name authority="Greene" date="1897" rank="genus">holocarpha</taxon_name>
    <taxon_name authority="(de Candolle) Greene" date="1897" rank="species">macradenia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Francisc.</publication_title>
      <place_in_publication>4: 426. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe heliantheae;subtribe madiinae;genus holocarpha;species macradenia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">220006462</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hemizonia</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="species">macradenia</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>5: 693. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hemizonia;species macradenia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–50 cm;</text>
      <biological_entity id="o9719" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stems notably stipitate-glandular.</text>
      <biological_entity id="o9720" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="notably" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads borne singly or in glomerules or spiciform-glomerulate arrays.</text>
      <biological_entity id="o9721" name="head" name_original="heads" src="d0_s2" type="structure" />
      <biological_entity id="o9722" name="glomerule" name_original="glomerules" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="spiciform-glomerulate" value_original="spiciform-glomerulate" />
        <character is_modifier="true" name="arrangement" src="d0_s2" value="singly" value_original="singly" />
      </biological_entity>
      <relation from="o9721" id="r671" name="borne" negation="false" src="d0_s2" to="o9722" />
    </statement>
    <statement id="d0_s3">
      <text>Involucres ± globose.</text>
      <biological_entity id="o9723" name="involucre" name_original="involucres" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Phyllaries each bearing ± 25 gland-tipped processes and minutely sessile or stipitate-glandular.</text>
      <biological_entity id="o9724" name="phyllary" name_original="phyllaries" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="more or less; minutely" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s4" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o9725" name="process" name_original="processes" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less" name="quantity" src="d0_s4" value="25" value_original="25" />
        <character is_modifier="true" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o9724" id="r672" name="bearing" negation="false" src="d0_s4" to="o9725" />
    </statement>
    <statement id="d0_s5">
      <text>Ray-florets 8–16.</text>
      <biological_entity id="o9726" name="ray-floret" name_original="ray-florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s5" to="16" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Disc-florets 40–90;</text>
      <biological_entity id="o9727" name="disc-floret" name_original="disc-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="40" name="quantity" src="d0_s6" to="90" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers reddish to dark purple.</text>
    </statement>
    <statement id="d0_s8">
      <text>2n = 8.</text>
      <biological_entity id="o9728" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s7" to="dark purple" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9729" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Nov" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy areas, clay soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy areas" />
        <character name="habitat" value="clay soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10–200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Holocarpha macradenia occurs on the coast from near Santa Cruz to central Monterey Bay area (most populations in the San Francisco Bay area are extirpated). Populations are reportedly highly interfertile; crosses to H. virgata have sometimes yielded moderately to highly fertile hybrids (the two species do not co-occur). J. Clausen (1951) and R. E. Palmer (1982) suggested that H. macradenia is most closely related to H. virgata; that hypothesis has been confirmed by molecular data (B. G. Baldwin, unpubl.).</discussion>
  
</bio:treatment>