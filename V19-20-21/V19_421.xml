<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">283</other_info_on_meta>
    <other_info_on_meta type="treatment_page">293</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hieracium</taxon_name>
    <taxon_name authority="Lessing" date="1830" rank="species">abscissum</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>5: 132. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus hieracium;species abscissum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066933</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hieracium</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">rusbyi</taxon_name>
    <taxon_hierarchy>genus Hieracium;species rusbyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 40–75+ cm.</text>
      <biological_entity id="o9946" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s0" to="75" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally usually glabrous, sometimes piloso-hirsute (hairs 3–6+ mm), distally glabrous or stellate-pubescent, sometimes piloso-hirsute (hairs 1–3+ mm) and stipitate-glandular as well.</text>
      <biological_entity id="o9947" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="well" name="pubescence" src="d0_s1" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 3–12+, cauline (1–) 3–6;</text>
      <biological_entity id="o9948" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o9949" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="12" upper_restricted="false" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o9950" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s2" to="3" to_inclusive="false" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades oblanceolate to lanceolate, 35–120 (–200) × 10–25 (–30+) mm, lengths 3–6+ times widths, bases cuneate to truncate (distal leaves), margins usually entire, sometimes denticulate or toothed, apices obtuse to acute, faces piloso-hirsute (hairs 2–3+ mm).</text>
      <biological_entity id="o9951" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9952" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="120" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="200" to_unit="mm" />
        <character char_type="range_value" from="35" from_unit="mm" name="length" src="d0_s3" to="120" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="30" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="25" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="3-6+" value_original="3-6+" />
      </biological_entity>
      <biological_entity id="o9953" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate to truncate" value_original="cuneate to truncate" />
      </biological_entity>
      <biological_entity id="o9954" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o9955" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity id="o9956" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="piloso-hirsute" value_original="piloso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Heads (5–) 12–30 (–60+) in paniculiform arrays.</text>
      <biological_entity id="o9957" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s4" to="12" to_inclusive="false" />
        <character char_type="range_value" from="30" from_inclusive="false" name="atypical_quantity" src="d0_s4" to="60" upper_restricted="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o9958" from="12" name="quantity" src="d0_s4" to="30" />
      </biological_entity>
      <biological_entity id="o9958" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles piloso-hirsute (hairs 0.5–1+ mm), stellate-pubescent, and stipitate-glandular.</text>
      <biological_entity id="o9959" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi: bractlets 3–8+.</text>
      <biological_entity id="o9960" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o9961" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="8" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres ± campanulate, 6–8+ (× 4–5) mm.</text>
      <biological_entity id="o9962" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s7" to="8+[" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="5]" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 13–18+, apices ± rounded, abaxial faces piloso-hirsute (hairs 0.5–1+ mm), stellate-pubescent, and stipitate-glandular.</text>
      <biological_entity id="o9963" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="13" name="quantity" src="d0_s8" to="18" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o9964" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9965" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 20–24+;</text>
      <biological_entity id="o9966" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s9" to="24" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 6–9 mm.</text>
      <biological_entity id="o9967" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae (usually black, sometimes redbrown) columnar, 2–3 mm;</text>
      <biological_entity id="o9968" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="columnar" value_original="columnar" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of 36–40+, white or stramineous bristles in ± 2 series, 3.5–5 mm.</text>
      <biological_entity id="o9969" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9970" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="36" is_modifier="true" name="quantity" src="d0_s12" to="40" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="stramineous" value_original="stramineous" />
        <character modifier="more or less" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9971" name="series" name_original="series" src="d0_s12" type="structure" />
      <relation from="o9969" id="r924" name="consist_of" negation="false" src="d0_s12" to="o9970" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr, Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Apr" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, openings in pine, oak, and pine-oak forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sites" modifier="disturbed" />
        <character name="habitat" value="openings" constraint="in pine , oak" />
        <character name="habitat" value="pine" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="pine-oak forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000–2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex.; Mexico; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34.</number>
  
</bio:treatment>