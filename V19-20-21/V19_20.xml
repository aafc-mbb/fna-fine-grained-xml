<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">79</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">mutisieae</taxon_name>
    <taxon_name authority="Ventenat" date="1802" rank="genus">chaptalia</taxon_name>
    <taxon_name authority="(Swartz) Ventenat ex B. D. Jackson" date="1893" rank="species">albicans</taxon_name>
    <place_of_publication>
      <publication_title>Index Kew</publication_title>
      <place_in_publication>1: 506. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe mutisieae;genus chaptalia;species albicans</taxon_hierarchy>
    <other_info_on_name type="fna_id">250066329</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Tussilago</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="species">albicans</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>113. 1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Tussilago;species albicans;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chaptalia</taxon_name>
    <taxon_name authority="(de Candolle) Urban" date="unknown" rank="species">leiocarpa</taxon_name>
    <taxon_hierarchy>genus Chaptalia;species leiocarpa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves sessile or nearly so;</text>
      <biological_entity id="o16293" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s0" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blades obovate to obovate-elliptic, 2–14 cm, margins retrorsely serrulate to denticulate-apiculate, abaxial faces white-tomentose, adaxial faces green, glabrous or glabrate.</text>
      <biological_entity id="o16294" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s1" to="obovate-elliptic" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s1" to="14" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16295" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="retrorsely serrulate" name="architecture_or_shape" src="d0_s1" to="denticulate-apiculate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16296" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="white-tomentose" value_original="white-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16297" name="face" name_original="faces" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Heads erect in bud, flowering, and fruit.</text>
      <biological_entity id="o16298" name="head" name_original="heads" src="d0_s2" type="structure">
        <character constraint="in bud" constraintid="o16299" is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="life_cycle" notes="" src="d0_s2" value="flowering" value_original="flowering" />
      </biological_entity>
      <biological_entity id="o16299" name="bud" name_original="bud" src="d0_s2" type="structure" />
      <biological_entity id="o16300" name="fruit" name_original="fruit" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Peduncles ebracteate, 6–15 cm in flowering, 12–37 cm in fruit, dilated distally.</text>
      <biological_entity id="o16301" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="ebracteate" value_original="ebracteate" />
        <character char_type="range_value" constraint="in fruit" constraintid="o16302" from="6" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="distally" name="shape" notes="" src="d0_s3" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o16302" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Florets: outer pistillate, corollas creamy white, rarely purple-tinged, laminae 0.2–0.3 mm wide;</text>
      <biological_entity id="o16303" name="floret" name_original="florets" src="d0_s4" type="structure" />
      <biological_entity constraint="outer" id="o16304" name="floret" name_original="florets" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16305" name="corolla" name_original="corollas" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s4" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
      <biological_entity id="o16306" name="lamina" name_original="laminae" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>inner florets bisexual, fertile.</text>
      <biological_entity id="o16307" name="floret" name_original="florets" src="d0_s5" type="structure" />
      <biological_entity constraint="inner" id="o16308" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s5" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s5" value="fertile" value_original="fertile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cypselae 8.4–11.2 mm, beaks filiform, lengths 0.5–0.6+ times bodies, faces glabrous or sparsely glandular (usually only along the nerves).</text>
      <biological_entity id="o16309" name="cypsela" name_original="cypselae" src="d0_s6" type="structure">
        <character char_type="range_value" from="8.4" from_unit="mm" name="some_measurement" src="d0_s6" to="11.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16310" name="beak" name_original="beaks" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="filiform" value_original="filiform" />
        <character constraint="body" constraintid="o16311" is_modifier="false" name="length" src="d0_s6" value="0.5-0.6+ times bodies" value_original="0.5-0.6+ times bodies" />
      </biological_entity>
      <biological_entity id="o16311" name="body" name_original="bodies" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 24, ca. 29.</text>
      <biological_entity id="o16312" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16313" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="24" value_original="24" />
        <character name="quantity" src="d0_s7" value="29" value_original="29" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar–)Apr–Jul(–Nov).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Nov" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy areas or open savannas, sometimes near evergreen oaks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy areas" />
        <character name="habitat" value="open savannas" />
        <character name="habitat" value="evergreen oaks" modifier="sometimes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">White sunbonnet</other_name>
  <discussion>Chaptalia albicans was treated by A. Cronquist (1980) as C. dentata (Linnaeus) Cassini. The latter is known only from the West Indies (G. Nesom 1984) and contrasts with C. albicans in having outer pistillate florets with corollas white to greenish (versus white) and laminae (0.2–)0.4–0.7 mm (versus 0.2–0.3 mm) wide, style branches of pistillate florets (0.5–)0.7–0.9 mm (versus 0.8–1.3 mm), cypselae 5.5–7.5(–8.5) mm (versus 8.4–11.2 mm), orange (versus white) carpopodia, and pappus bristles 5.8–8.5 mm (versus 7.8–10.5 mm).</discussion>
  
</bio:treatment>