<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Guy L. Nesom</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">429</other_info_on_meta>
    <other_info_on_meta type="treatment_page">431</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini ex Lecoq &amp; Juillet" date="1831" rank="tribe">gnaphalieae</taxon_name>
    <taxon_name authority="Weddell" date="1856" rank="genus">GAMOCHAETA</taxon_name>
    <place_of_publication>
      <publication_title>Chlor. Andina</publication_title>
      <place_in_publication>1: 151. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe gnaphalieae;genus GAMOCHAETA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek gamos, union, and chaete, loose and flowing hair, alluding to basally connate pappus bristles</other_info_on_name>
    <other_info_on_name type="fna_id">113220</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="(Weddell) O. Hoffmann" date="unknown" rank="section">Gamochaeta</taxon_name>
    <taxon_hierarchy>genus Gnaphalium;section Gamochaeta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gnaphalium</taxon_name>
    <taxon_name authority="(Weddell) Grenier" date="unknown" rank="subgenus">Gamochaeta</taxon_name>
    <taxon_hierarchy>genus Gnaphalium;subgenus Gamochaeta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials, (1–) 5–65 cm;</text>
      <biological_entity id="o17827" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taprooted or fibrous-rooted [subrhizomatous].</text>
      <biological_entity id="o17828" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="65" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="65" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="taprooted" value_original="taprooted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="fibrous-rooted" value_original="fibrous-rooted" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 1+, usually erect, sometimes decumbent-ascending.</text>
      <biological_entity id="o17830" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s2" upper_restricted="false" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s2" value="decumbent-ascending" value_original="decumbent-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s4">
      <text>alternate;</text>
    </statement>
    <statement id="d0_s5">
      <text>sessile;</text>
      <biological_entity id="o17831" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades mostly linear to oblanceolate or spatulate, bases cuneate to ± cordate, margins entire, sometimes sinuate, abaxial faces mostly white or gray and tomentose or pannose-tomentose, adaxial green and glabrescent or glabrous, or grayish and arachnose, loosely tomentose, or subpannose.</text>
      <biological_entity id="o17832" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="mostly linear" name="shape" src="d0_s6" to="oblanceolate or spatulate" />
      </biological_entity>
      <biological_entity id="o17833" name="base" name_original="bases" src="d0_s6" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s6" to="more or less cordate" />
      </biological_entity>
      <biological_entity id="o17834" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17835" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="gray" value_original="gray" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pannose-tomentose" value_original="pannose-tomentose" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17836" name="face" name_original="faces" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="grayish" value_original="grayish" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="arachnose" value_original="arachnose" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="subpannose" value_original="subpannose" />
        <character is_modifier="false" modifier="loosely" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="subpannose" value_original="subpannose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Heads disciform, usually in glomerules borne in continuous or interrupted, usually spiciform, sometimes paniculiform, arrays (reduced to terminal glomerules in depauperate individuals).</text>
      <biological_entity id="o17837" name="head" name_original="heads" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="disciform" value_original="disciform" />
      </biological_entity>
      <biological_entity id="o17838" name="glomerule" name_original="glomerules" src="d0_s7" type="structure" />
      <biological_entity id="o17839" name="array" name_original="arrays" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="continuous" value_original="continuous" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="interrupted" value_original="interrupted" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s7" value="spiciform" value_original="spiciform" />
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s7" value="paniculiform" value_original="paniculiform" />
      </biological_entity>
      <relation from="o17837" id="r1603" modifier="usually" name="in" negation="false" src="d0_s7" to="o17838" />
      <relation from="o17838" id="r1604" name="borne in" negation="false" src="d0_s7" to="o17839" />
    </statement>
    <statement id="d0_s8">
      <text>Involucres narrowly to broadly campanulate, 2.5–5 mm.</text>
      <biological_entity id="o17840" name="involucre" name_original="involucres" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s8" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Phyllaries in 3–7 series, unequal, mostly brownish to stramineous, sometimes purplish, hyaline, often shiny, distally chartaceous to scarious, eglandular.</text>
      <biological_entity id="o17841" name="phyllary" name_original="phyllaries" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s9" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="mostly brownish" name="coloration" src="d0_s9" to="stramineous" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" modifier="often" name="reflectance" src="d0_s9" value="shiny" value_original="shiny" />
        <character char_type="range_value" from="distally chartaceous" name="texture" src="d0_s9" to="scarious" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <biological_entity id="o17842" name="series" name_original="series" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="7" />
      </biological_entity>
      <relation from="o17841" id="r1605" name="in" negation="false" src="d0_s9" to="o17842" />
    </statement>
    <statement id="d0_s10">
      <text>Receptacles flat (concave in fruit), glabrous, epaleate.</text>
      <biological_entity id="o17843" name="receptacle" name_original="receptacles" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="epaleate" value_original="epaleate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pistillate florets 50–130, more numerous than bisexual florets;</text>
      <biological_entity id="o17844" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="50" name="quantity" src="d0_s11" to="130" />
        <character constraint="than bisexual florets" constraintid="o17845" is_modifier="false" name="quantity" src="d0_s11" value="more numerous" value_original="more numerous" />
      </biological_entity>
      <biological_entity id="o17845" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>corollas all yellow or purplish-tipped.</text>
      <biological_entity id="o17846" name="corolla" name_original="corollas" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish-tipped" value_original="purplish-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Bisexual florets 2–7;</text>
      <biological_entity id="o17847" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s13" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>corollas all yellow or distally purplish.</text>
      <biological_entity id="o17848" name="corolla" name_original="corollas" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cypselae oblong, slightly flattened, faces with papilliform hairs (myxogenic, their lengths about equaling diams.);</text>
      <biological_entity id="o17849" name="cypsela" name_original="cypselae" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o17850" name="face" name_original="faces" src="d0_s15" type="structure" />
      <biological_entity id="o17851" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="papilliform" value_original="papilliform" />
      </biological_entity>
      <relation from="o17850" id="r1606" name="with" negation="false" src="d0_s15" to="o17851" />
    </statement>
    <statement id="d0_s16">
      <text>pappi readily falling, of 12–28 barbellulate bristles in 1 series (basally connate in smooth rings, falling as units).</text>
      <biological_entity id="o17853" name="bristle" name_original="bristles" src="d0_s16" type="structure">
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s16" to="28" />
        <character is_modifier="true" name="architecture" src="d0_s16" value="barbellulate" value_original="barbellulate" />
      </biological_entity>
      <biological_entity id="o17854" name="series" name_original="series" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <relation from="o17852" id="r1607" name="consist_of" negation="false" src="d0_s16" to="o17853" />
      <relation from="o17853" id="r1608" name="in" negation="false" src="d0_s16" to="o17854" />
    </statement>
    <statement id="d0_s17">
      <text>x = 7.</text>
      <biological_entity id="o17852" name="pappus" name_original="pappi" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="readily" name="life_cycle" src="d0_s16" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity constraint="x" id="o17855" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, West Indies, Central America, South America; some species adventive and naturalized in Europe, Asia, Australia, and elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="some species adventive and naturalized in Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Australia" establishment_means="native" />
        <character name="distribution" value="and elsewhere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>93.</number>
  <discussion>Species ca. 50 (12 in the flora).</discussion>
  <discussion>Gamochaeta comprises about 50 species (A. L. Cabrera 1961; S. E. Freire and L. Iharlegui 1997) or about 80 species (Cabrera 1977+, part 10), all native to the Americas. Most are known only from South America; some apparently are native to Mexico and the flora area (G. L. Nesom 2004b). Some species are strongly weedy and have extended non-native ranges. Because of inconsistencies in the identification of those species, it has been difficult to evaluate the overall distributions of the widespread species.</discussion>
  <discussion>The distinctiveness of Gamochaeta was emphasized by A. L. Cabrera (1961, and in later floristic treatments of South American species) and by other botanists who have treated it as a separate genus in the last decade. The genus is distinguished by its combination of relatively small heads in spiciform arrays, concave post-fruiting receptacles, truncate collecting appendages of style branches in bisexual florets, relatively small cypselae with minute, mucilage-producing papilliform hairs on the faces, and pappus bristles basally connate in smooth rings and released as single units. It seems likely that most species are primarily autogamous, in view of the tiny, non-showy heads that barely open through flowering. The consistency of vegetative and floral features in some of the species supports this hypothesis.</discussion>
  <discussion>In the flora area, some species have commonly been treated as variants within Gamochaeta purpurea; distinctions are evident in the field, where it is common to find as many as five species growing in proximity without intergradation. Species of Gamochaeta are distinguished by differences primarily in root form, leaf shape, nature and distribution of indument, and phyllary morphology. Chromosome counts have been reported for some species; because of the unreliability of identifications, vouchers for those counts should be restudied.</discussion>
  <references>
    <reference>Freire, S. E. and L. Iharlegui. 1997. Sinopsis preliminar del género Gamochaeta (Asteraceae, Gnaphalieae). Bol. Soc. Argent. Bot. 33: 23–35.</reference>
    <reference>Nesom, G. L. 1990f. The taxonomic status of Gamochaeta (Asteraceae: Inuleae) and the species of the United States. Phytologia 68: 186–198.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves bicolor (abaxial faces closely white-pannose to pannose-tomentose, indument obscuring epidermis, adaxial faces glabrous or glabrate to sparsely arachnose)</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves concolor or weakly bicolor (abaxial and adaxial faces ± equally greenish to gray-greenish, indument usually loosely tomentose or arachnose, sometimes subpannose, adaxial sometimes glabrescent in G. stagnalis)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal and proximal cauline leaves usually withering before flowering (clusters of smaller leaves usually present in cauline axils); stems erect or ascending; plants (30–)50–85 cm; apices of inner phyllaries acute-acuminate; flowering mostly Jul–Aug</description>
      <determination>3 Gamochaeta simplicicaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal and proximal cauline leaves present or not at flowering; stems erect to decumbent-ascending; plants mostly 10–50 cm; apices of inner phyllaries acute to obtuse, rounded, or blunt; flowering mostly Apr–Jun(–Jul in G. calviceps)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Adaxial leaf faces glabrous or glabrate; involucres (± purplish) 2.5–3 mm, bases glabrous; outer phyllaries elliptic-obovate to broadly ovate-elliptic, apices rounded to obtuse; bisexual florets 2–3</description>
      <determination>7 Gamochaeta coarctata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Adaxial leaf faces sparsely arachnose (hairs persistent, evident at 10×); involucres (sometimes purplish) 3–4.5(–5) mm, bases (imbedded in tomentum) often sparsely arachnose on proximal 1/5–1/2; outer phyllaries ovate, ovate-triangular, or ovate-lanceolate, apices acute to acuminate; bisexual florets 2–6</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems not pannose (induments whitish, like closely appressed, polished cloth, hairs usually not individually evident); involucres 3–3.5(–4) mm; apices of inner phyllaries acute to acute-acuminate; bisexual florets 2–4 (cypselae purple)</description>
      <determination>4 Gamochaeta chionesthes</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems usually ± pannose or pannose-tomentose (hairs individually evident, longitudinally arranged); involucres 3–4.5 mm; apices of inner phyllaries acute, obtuse, or truncate-rounded, sometimes apiculate; bisexual florets 3–6 (cypselae tan to brownish)</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Blades of cauline leaves oblanceolate to spatulate (basal cells of hairs on adaxial faces persistent, expanded, glassy); involucres 4–4.5 mm; laminae of inner phyllaries triangular, apices acute (not apiculate); bisexual florets 3–4; plants fibrous-rooted or taprooted</description>
      <determination>1 Gamochaeta purpurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Blades of cauline leaves oblanceolate to oblanceolate-oblong or oblanceolate-obovate; involucres 4.5–5 or 3–3.5 mm; laminae of inner phyllaries elliptic-oblong to oblong, apices truncate-rounded or obtuse and apiculate; bisexual florets (3–)4–6; plants usually fibrous-rooted, rarely taprooted</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Arrays of heads uninterrupted (1–5 cm in early flowering) to strongly interrupted, mostly 5–18 cm × 10–12 mm (pressed); involucres 3–3.5 mm; outer phyllaries (tawny-transparent, never dark brown) ovate to ovate-lanceolate; cypselae 0.5–0.6 mm</description>
      <determination>5 Gamochaeta argyrinea</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Arrays of heads usually continuous, rarely interrupted (then proximally), mostly 1–6(–8) cm × 12–18 mm (pressed); involucres 4.5–5 mm; outer phyllaries (and, often, laminae of inner, dark or greenish brown) broadly ovate-triangular (mid phyllaries ± keeled near apices); cypselae 0.7–0.8 mm</description>
      <determination>6 Gamochaeta ustulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades linear to narrowly oblanceolate (often folded along midveins, distally becoming arcuate, ± patent bracts surpassing the heads; basal cells of hairs on adaxial faces expanded, glassy); apices of outer phyllaries (brown) acute-acuminate (usually involute and spreading or recurving); Texas, near Mexican border</description>
      <determination>2 Gamochaeta sphacelata</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades linear or linear-oblanceolate to spatulate, narrowly lanceolate, oblong-oblanceolate, oblanceolate-obovate, or oblanceolate (folded or not; if distal leaves or bracts surpassing heads, not arcuate and ± patent); apices of outer phyllaries (sometimes brownish) sometimes acute to acute-acuminate or attenuate-apiculate (not involute, except in G. calviceps); mostly (not always) se United States.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Blades of basal and proximal cauline leaves 4–16 mm wide (bracts among heads spatulate to oblanceolate, at least the proximal surpassing the glomerules of heads)</description>
      <determination>11 Gamochaeta pensylvanica</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Blades of basal and proximal cauline leaves 2–6(–10) mm wide (mid and distal cauline becoming oblanceolate to linear, bracts among heads linear, oblanceolate, or oblong-oblanceolate, surpassing glomerules or not)</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Blades of mid and distal cauline leaves oblanceolate (bases subclasping; bracts among heads mostly shorter than glomerules); involucres (not purplish) 3.5–4 mm; phyl- laries in 4–5 series</description>
      <determination>12 Gamochaeta stachydifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Blades of mid and distal cauline leaves oblong-oblanceolate or oblanceolate to spatulate, narrowly lanceolate, linear-oblanceolate, or linear (bases not subclasping; bracts among heads shorter or longer than glomerules); involucres (sometimes purplish) 2.5–3.5 mm; phyllaries in 5–7 or 3–4(–5) series</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Involucres (not purplish) 3–3.5 mm, bases sparsely arachnose or glabrous; arrays of heads interrupted (at least distally, main axes visible between heads); phyllaries in 5–7 series, outer ovate-triangular, lengths 1/3–1/2 inner, apices acute-acuminate; flowering May–Jul</description>
      <determination>8 Gamochaeta calviceps</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Involucres (usually purplish, at stereome-lamina junctions of phyllaries) 2.5–3 mm, bases sparsely arachnose; arrays of heads initially capitate clusters or cylindric and uninterrupted (at least distally, main axes obscured by heads); phyllaries in 3–4(–5) series, outer ovate-lanceolate, lengths 1/2–2/3 inner, apices narrowly to broadly acute; flowering (Feb–)Mar–May (later with moisture)</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Cauline leaves and bracts among heads narrowly lanceolate, linear-oblanceolate, or linear; arrays of heads initially uninterrupted cylindro- spiciform (becoming glomerulate-interrupted in late flowering)</description>
      <determination>9 Gamochaeta antillana</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Cauline leaves and bracts among heads mostly oblanceolate to oblong-oblanceolate (± uniform in size and shape); arrays of heads capitate clusters or interrupted spiciform arrays (from early through late flowering)</description>
      <determination>10 Gamochaeta stagnalis</determination>
    </key_statement>
  </key>
</bio:treatment>