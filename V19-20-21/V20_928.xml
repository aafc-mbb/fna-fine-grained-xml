<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/23 14:46:47</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">406</other_info_on_meta>
    <other_info_on_meta type="treatment_page">407</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Cassini" date="1819" rank="tribe">astereae</taxon_name>
    <taxon_name authority="Nuttall" date="1840" rank="genus">xylorhiza</taxon_name>
    <taxon_name authority="(H. M. Hall) T. J. Watson" date="1977" rank="species">cognata</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>29: 204. 1977</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe astereae;genus xylorhiza;species cognata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250067834</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aster</taxon_name>
    <taxon_name authority="H. M. Hall" date="unknown" rank="species">cognatus</taxon_name>
    <place_of_publication>
      <publication_title>Univ. Calif. Publ. Bot.</publication_title>
      <place_in_publication>6: 173. 1915</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aster;species cognatus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Machaeranthera</taxon_name>
    <taxon_name authority="(H. M. Hall) Cronquist &amp; D. D. Keck" date="unknown" rank="species">cognata</taxon_name>
    <taxon_hierarchy>genus Machaeranthera;species cognata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 50–150 cm.</text>
      <biological_entity id="o14490" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="150" to_unit="cm" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems branched to near apices, glabrescent (younger stems and branches stipitate-glandular).</text>
      <biological_entity id="o14491" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="to apices" constraintid="o14492" is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s1" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o14492" name="apex" name_original="apices" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades spatulate to ovate, 3–20 mm wide, bases attenuate, subclasping, margins flat, coarsely spinulose-toothed, faces glabrous, sparsely stipitate-glandular.</text>
      <biological_entity id="o14493" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s2" to="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14494" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subclasping" value_original="subclasping" />
      </biological_entity>
      <biological_entity id="o14495" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s2" value="spinulose-toothed" value_original="spinulose-toothed" />
      </biological_entity>
      <biological_entity id="o14496" name="face" name_original="faces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Peduncles 5–11 (–20) cm.</text>
      <biological_entity id="o14497" name="peduncle" name_original="peduncles" src="d0_s3" type="structure">
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="11" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres 15–20 × 20–40 mm.</text>
      <biological_entity id="o14498" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Phyllaries loosely appressed, outermost (at least margins) stipitate-glandular, innermost shorter than mid.</text>
      <biological_entity id="o14499" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="outermost" id="o14500" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
      </biological_entity>
      <biological_entity constraint="innermost" id="o14501" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure">
        <character constraint="than mid phyllaries" constraintid="o14502" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="mid" id="o14502" name="phyllarie" name_original="phyllaries" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Ray-florets (12–) 20–29 (–32);</text>
      <biological_entity id="o14503" name="ray-floret" name_original="ray-florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" name="atypical_quantity" src="d0_s6" to="20" to_inclusive="false" />
        <character char_type="range_value" from="29" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="32" />
        <character char_type="range_value" from="20" name="quantity" src="d0_s6" to="29" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>corollas light blue to violet.</text>
      <biological_entity id="o14504" name="corolla" name_original="corollas" src="d0_s7" type="structure">
        <character char_type="range_value" from="light blue" name="coloration" src="d0_s7" to="violet" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Style-branch appendages shorter than stigmatic lines.</text>
      <biological_entity constraint="stigmatic" id="o14506" name="line" name_original="lines" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>2n = 12.</text>
      <biological_entity constraint="style-branch" id="o14505" name="appendage" name_original="appendages" src="d0_s8" type="structure">
        <character constraint="than stigmatic lines" constraintid="o14506" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14507" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–Apr(–Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Jan" />
        <character name="flowering time" char_type="atypical_range" to="Jun" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Slopes and bottoms of deep ravines in clay, rocky sand, and gravel</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="slopes" constraint="of deep ravines in clay , rocky sand , and gravel" />
        <character name="habitat" value="bottoms" constraint="of deep ravines in clay , rocky sand , and gravel" />
        <character name="habitat" value="deep ravines" constraint="in clay , rocky sand , and gravel" />
        <character name="habitat" value="clay" />
        <character name="habitat" value="rocky sand" />
        <character name="habitat" value="gravel" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20–300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Mecca woody-aster</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Xylorhiza cognata is known only from Riverside County.</discussion>
  
</bio:treatment>