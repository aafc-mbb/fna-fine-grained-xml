<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/22 18:37:50</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">19</other_info_on_meta>
    <other_info_on_meta type="mention_page">281</other_info_on_meta>
    <other_info_on_meta type="treatment_page">289</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">Asteraceae</taxon_name>
    <taxon_name authority="Lamarck &amp; de Candolle" date="1806" rank="tribe">cichorieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">hieracium</taxon_name>
    <taxon_name authority="Howell" date="1901" rank="species">longiberbe</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N.W. Amer.,</publication_title>
      <place_in_publication>395. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family asteraceae;tribe cichorieae;genus hieracium;species longiberbe</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250066946</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 25–50+ cm.</text>
      <biological_entity id="o7723" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems proximally piloso-hirsute (hairs 2–8+ mm), sometimes glabrate, distally glabrous or piloso-hirsute (hairs 2–5+ mm).</text>
      <biological_entity id="o7724" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s1" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="piloso-hirsute" value_original="piloso-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: basal 0 (–3+), cauline 6–12+;</text>
      <biological_entity id="o7725" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o7726" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_inclusive="false" name="atypical_quantity" src="d0_s2" to="3" upper_restricted="false" />
        <character name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o7727" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s2" to="12" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades spatulate to oblanceolate, 25–80 (–100+) × 8–15 (–25+) mm, lengths 3–5 times widths, bases ± cuneate, margins usually entire, rarely denticulate, apices obtuse to acute, abaxial faces piloso-hirsute (hairs 2–5+ mm), adaxial usually piloso-hirsute at margins (hairs 2–5+ mm), rarely glabrous.</text>
      <biological_entity id="o7728" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o7729" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s3" to="oblanceolate" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s3" to="100" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="25" from_unit="mm" name="length" src="d0_s3" to="80" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="25" to_unit="mm" upper_restricted="false" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="3-5" value_original="3-5" />
      </biological_entity>
      <biological_entity id="o7730" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o7731" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o7732" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s3" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7733" name="face" name_original="faces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="piloso-hirsute" value_original="piloso-hirsute" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o7734" name="face" name_original="faces" src="d0_s3" type="structure">
        <character constraint="at margins" constraintid="o7735" is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" modifier="rarely" name="pubescence" notes="" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o7735" name="margin" name_original="margins" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Heads (3–) 6–12+ in corymbiform arrays.</text>
      <biological_entity id="o7736" name="head" name_original="heads" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="6" to_inclusive="false" />
        <character char_type="range_value" constraint="in arrays" constraintid="o7737" from="6" name="quantity" src="d0_s4" to="12" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7737" name="array" name_original="arrays" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="corymbiform" value_original="corymbiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles usually stellate-pubescent, sometimes piloso-hirsute and/or stipitate-glandular as well, rarely glabrous.</text>
      <biological_entity id="o7738" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="stellate-pubescent" value_original="stellate-pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s5" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="stipitate-glandular" value_original="stipitate-glandular" />
        <character is_modifier="false" modifier="well; rarely" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Calyculi: bractlets 9–13+.</text>
      <biological_entity id="o7739" name="calyculus" name_original="calyculi" src="d0_s6" type="structure" />
      <biological_entity id="o7740" name="bractlet" name_original="bractlets" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s6" to="13" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Involucres campanulate, 7–10 mm.</text>
      <biological_entity id="o7741" name="involucre" name_original="involucres" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="campanulate" value_original="campanulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Phyllaries 12–15+, apices ± rounded, abaxial faces piloso-hirsute and stellate-pubescent.</text>
      <biological_entity id="o7742" name="phyllary" name_original="phyllaries" src="d0_s8" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s8" to="15" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o7743" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o7744" name="face" name_original="faces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="piloso-hirsute" value_original="piloso-hirsute" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Florets 12–24+;</text>
      <biological_entity id="o7745" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s9" to="24" upper_restricted="false" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corollas yellow, 7–12 mm.</text>
      <biological_entity id="o7746" name="corolla" name_original="corollas" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Cypselae columnar, ca. 3.5 mm;</text>
      <biological_entity id="o7747" name="cypsela" name_original="cypselae" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="columnar" value_original="columnar" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="3.5" value_original="3.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pappi of 32–40+, white or stramineous bristles in ± 2 series, 5–6 mm.</text>
      <biological_entity id="o7748" name="pappus" name_original="pappi" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7749" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="32" is_modifier="true" name="quantity" src="d0_s12" to="40" upper_restricted="false" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s12" value="stramineous" value_original="stramineous" />
        <character modifier="more or less" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o7750" name="series" name_original="series" src="d0_s12" type="structure" />
      <relation from="o7748" id="r717" name="consist_of" negation="false" src="d0_s12" to="o7749" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <discussion>Hieracium longiberbe is known only from along the Columbia River.</discussion>
  
</bio:treatment>