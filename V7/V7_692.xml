<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">454</other_info_on_meta>
    <other_info_on_meta type="illustration_page">452</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">camelineae</taxon_name>
    <taxon_name authority="Medikus" date="unknown" rank="genus">capsella</taxon_name>
    <taxon_name authority="(Linnaeus) Medikus" date="1792" rank="species">bursa-pastoris</taxon_name>
    <place_of_publication>
      <publication_title>Pfl.-Gatt.,</publication_title>
      <place_in_publication>85. 1792</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe camelineae;genus capsella;species bursa-pastoris</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200009292</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thlaspi</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">bursa-pastoris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 647. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Thlaspi;species bursa-pastoris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants mostly sparsely to densely pubescent, trichomes sessile, 3–5-rayed, stellate (base of plant often mixed with much longer, simple ones).</text>
      <biological_entity id="o14335" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o14336" name="trichome" name_original="trichomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s0" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (0.2–) 1–5 (–7) dm.</text>
      <biological_entity id="o14337" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="7" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: petiole 0.5–4 (–6) cm;</text>
      <biological_entity constraint="basal" id="o14338" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o14339" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblong or oblanceolate, (0.5–) 1.5–10 (–15) cm × 2–25 (–50) mm, base cuneate or attenuate, apex acute or acuminate.</text>
      <biological_entity constraint="basal" id="o14340" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14341" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s3" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14342" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o14343" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves: blade narrowly oblong, lanceolate, or linear, 1–5.5 (–8) cm × 1–15 (–20) mm, base sagittate, amplexicaul, or, rarely, auriculate.</text>
      <biological_entity constraint="cauline" id="o14344" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o14345" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruiting pedicels usually straight, (0.3–) 0.5–1.5 (–2) cm, glabrous.</text>
      <biological_entity id="o14346" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="sagittate" value_original="sagittate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="amplexicaul" value_original="amplexicaul" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="0.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14347" name="pedicel" name_original="pedicels" src="d0_s5" type="structure" />
      <relation from="o14346" id="r997" name="fruiting" negation="false" src="d0_s5" to="o14347" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals green or reddish, 1.5–2 × 0.7–1 mm (margins membranous);</text>
      <biological_entity id="o14348" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o14349" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s6" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals (1.5–) 2–4 (–5) × 1–1.5 mm;</text>
      <biological_entity id="o14350" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o14351" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_length" src="d0_s7" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 1–2 mm;</text>
      <biological_entity id="o14352" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o14353" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers to 0.5 mm.</text>
      <biological_entity id="o14354" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o14355" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits (0.3–) 0.4–0.9 (–1) cm × (2–) 3–7 (–9) mm, base cuneate, apex emarginate or truncate;</text>
      <biological_entity id="o14356" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="atypical_length" src="d0_s10" to="0.4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s10" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s10" to="0.9" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s10" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14357" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o14358" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>valves each with subparallel lateral-veins;</text>
      <biological_entity id="o14359" name="valve" name_original="valves" src="d0_s11" type="structure" />
      <biological_entity id="o14360" name="lateral-vein" name_original="lateral-veins" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="subparallel" value_original="subparallel" />
      </biological_entity>
      <relation from="o14359" id="r998" name="with" negation="false" src="d0_s11" to="o14360" />
    </statement>
    <statement id="d0_s12">
      <text>style 0.2–0.7 mm.</text>
      <biological_entity id="o14361" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds brown, 0.9–1.1 × 0.4–0.6 mm. 2n = 32.</text>
      <biological_entity id="o14362" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s13" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s13" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14363" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering and fruiting Jan–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Jan" />
        <character name="fruiting time" char_type="range_value" to="Oct" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, gardens, fields, barren gravel, pastures, plantations, lawns, orchards, cultivated ground, waste areas, vineyards, mountain slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="gardens" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="barren gravel" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="plantations" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="orchards" />
        <character name="habitat" value="cultivated ground" />
        <character name="habitat" value="waste areas" />
        <character name="habitat" value="vineyards" />
        <character name="habitat" value="mountain slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Greenland; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; Asia; n Africa; introduced also in Mexico, West Indies, Central America, South America, Atlantic Islands, Pacific Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>According to M. Coquillat (1951), Capsella bursa-pastoris is the second most common weed on earth, after Polygonum aviculare.</discussion>
  
</bio:treatment>