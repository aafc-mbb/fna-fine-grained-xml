<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">218</other_info_on_meta>
    <other_info_on_meta type="illustration_page">217</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="H. H. Iltis" date="2007" rank="genus">cleoserrata</taxon_name>
    <taxon_name authority="(Jacquin) H. H. Iltis" date="2007" rank="species">serrata</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>17: 448. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus cleoserrata;species serrata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094783</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cleome</taxon_name>
    <taxon_name authority="Jacquin" date="unknown" rank="species">serrata</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Syst. Pl.,</publication_title>
      <place_in_publication>26. 1760</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cleome;species serrata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cleome</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">polygama</taxon_name>
    <taxon_hierarchy>genus Cleome;species polygama;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants [perennial], 15–50 cm.</text>
      <biological_entity id="o40961" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched or slightly branched;</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous.</text>
      <biological_entity id="o40962" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole 2–5 cm;</text>
      <biological_entity id="o40963" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o40964" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>leaflets 3, blade lanceolate to ovate-elliptic, 5–12 × 0.4–1.4 cm, margins serrulate, apex acute to long-acuminate, surfaces glabrous.</text>
      <biological_entity id="o40965" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o40966" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o40967" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate-elliptic" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s4" to="1.4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o40968" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o40969" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="long-acuminate" />
      </biological_entity>
      <biological_entity id="o40970" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes 6–25 cm;</text>
      <biological_entity id="o40971" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s5" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts unifoliate, subulate, to 1 mm.</text>
      <biological_entity id="o40972" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels 4–8 mm.</text>
      <biological_entity id="o40973" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals deciduous, green, subulate, 2–2.5 × 0.4–0.5 mm, glabrous;</text>
      <biological_entity id="o40974" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o40975" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="subulate" value_original="subulate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s8" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white or whitish, with pinkish or red distally, obovate, 5–6 × 1.8–2 mm, shortly clawed;</text>
      <biological_entity id="o40976" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o40977" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s9" value="clawed" value_original="clawed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens yellow, 2–5 mm;</text>
      <biological_entity id="o40978" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o40979" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 2–3 mm;</text>
      <biological_entity id="o40980" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o40981" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>gynophore 1 (–2) mm in fruit;</text>
      <biological_entity id="o40982" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o40983" name="gynophore" name_original="gynophore" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="2" to_unit="mm" />
        <character constraint="in fruit" constraintid="o40984" name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o40984" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>ovary 2.5–4 mm;</text>
      <biological_entity id="o40985" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o40986" name="ovary" name_original="ovary" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>style 0.2–0.3 mm.</text>
      <biological_entity id="o40987" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o40988" name="style" name_original="style" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s14" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Capsules 30–50 × 2.5–3 mm.</text>
      <biological_entity id="o40989" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s15" to="50" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds pale green to brown, 1.4–1.5 mm, inconspicuously ridged.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 24.</text>
      <biological_entity id="o40990" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s16" to="brown" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="inconspicuously" name="shape" src="d0_s16" value="ridged" value_original="ridged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o40991" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ga.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Spiderflower</other_name>
  
</bio:treatment>