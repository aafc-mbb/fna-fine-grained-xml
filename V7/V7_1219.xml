<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">702</other_info_on_meta>
    <other_info_on_meta type="mention_page">703</other_info_on_meta>
    <other_info_on_meta type="treatment_page">707</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="S. Watson" date="1890" rank="species">campestris</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>25: 125. 1890</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species campestris</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250095127</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(short-lived, caudex not woody);</text>
    </statement>
    <statement id="d0_s2">
      <text>usually glabrous, (basal leaf-blade margins pubescent, sometimes sepals).</text>
      <biological_entity id="o6385" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems unbranched or branched, (few, glaucous), 6–15 (–18) dm.</text>
      <biological_entity id="o6386" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="18" to_unit="dm" />
        <character char_type="range_value" from="6" from_unit="dm" name="some_measurement" src="d0_s3" to="15" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves often rosulate;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o6387" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade (fleshy), oblanceolate to obovate, 3.5–21 cm, margins dentate, (bristly ciliate throughout or only teeth and petiole ciliate).</text>
      <biological_entity id="o6388" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s6" to="21" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6389" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves: blade lanceolate to narrowly ovate, 3.5–11 (–15) cm × 6–14 mm (smaller distally), base auriculate to amplexicaul, margins usually entire or undulate, rarely dentate.</text>
      <biological_entity constraint="cauline" id="o6390" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o6391" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s7" to="narrowly ovate" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s7" to="11" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6392" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="auriculate to amplexicaul" value_original="auriculate to amplexicaul" />
      </biological_entity>
      <biological_entity id="o6393" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes ebracteate, (with densely clustered buds, later lax).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate-ascending, (straight), 5–18 mm.</text>
      <biological_entity id="o6394" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6395" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o6394" id="r469" name="fruiting" negation="false" src="d0_s9" to="o6395" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx campanulate;</text>
      <biological_entity id="o6396" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6397" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals (suberect), purple, (broadly ovate or oblong), 7–10 mm, not keeled, (apically bristly or not);</text>
      <biological_entity id="o6398" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" notes="" src="d0_s11" value="purple" value_original="purple" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o6399" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals light purple (with pale-yellow claw), 9–12 mm, blade 2–3.5 × 0.5–1 mm, margins not crisped, claw 6–9 mm, wider than blade;</text>
      <biological_entity id="o6400" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o6401" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="light purple" value_original="light purple" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6402" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6403" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o6404" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
        <character constraint="than blade" constraintid="o6405" is_modifier="false" name="width" src="d0_s12" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o6405" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o6406" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o6407" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments: median pairs (distinct), 6–8 mm, lateral pair 4–6 mm;</text>
      <biological_entity id="o6408" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="median" value_original="median" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers (all) fertile, 3–4 mm;</text>
      <biological_entity id="o6409" name="filament" name_original="filaments" src="d0_s15" type="structure" />
      <biological_entity id="o6410" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>gynophore 0.5–1.5 mm.</text>
      <biological_entity id="o6411" name="filament" name_original="filaments" src="d0_s16" type="structure" />
      <biological_entity id="o6412" name="gynophore" name_original="gynophore" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits spreading to ascending, smooth, slightly curved to straight, flattened, 6–14 cm × 2–3.5 mm;</text>
      <biological_entity id="o6413" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s17" to="ascending" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="slightly curved" name="course" src="d0_s17" to="straight" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s17" to="14" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s17" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each with obscure midvein;</text>
      <biological_entity id="o6414" name="valve" name_original="valves" src="d0_s18" type="structure" />
      <biological_entity id="o6415" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="obscure" value_original="obscure" />
      </biological_entity>
      <relation from="o6414" id="r470" name="with" negation="false" src="d0_s18" to="o6415" />
    </statement>
    <statement id="d0_s19">
      <text>replum straight;</text>
      <biological_entity id="o6416" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 50–102 per ovary;</text>
      <biological_entity id="o6417" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o6418" from="50" name="quantity" src="d0_s20" to="102" />
      </biological_entity>
      <biological_entity id="o6418" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 1–3 mm;</text>
      <biological_entity id="o6419" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s21" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma 2-lobed.</text>
      <biological_entity id="o6420" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds oblong, 2–3 × 1.4–2 mm;</text>
      <biological_entity id="o6421" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s23" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s23" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>wing 0.1–0.2 mm wide at apex.</text>
      <biological_entity id="o6422" name="wing" name_original="wing" src="d0_s24" type="structure">
        <character char_type="range_value" constraint="at apex" constraintid="o6423" from="0.1" from_unit="mm" name="width" src="d0_s24" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6423" name="apex" name_original="apex" src="d0_s24" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky openings in chaparral, open conifer forests, openings and after fires in chaparral-oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky openings" constraint="in chaparral" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="open conifer forests" />
        <character name="habitat" value="fires" modifier="openings and after" constraint="in chaparral-oak woodlands" />
        <character name="habitat" value="chaparral-oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>900-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Streptanthus campestris is distributed in California in Riverside, San Bernardino, San Diego, Santa Barbara, and Ventura counties, and in Baja California in Sierra San Pedro Mártir and Sierra Juárez.</discussion>
  
</bio:treatment>