<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">239</other_info_on_meta>
    <other_info_on_meta type="treatment_page">566</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="A. Nelson &amp; J. F. Macbride" date="1913" rank="genus">IDAHOA</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>56: 474. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;genus IDAHOA</taxon_hierarchy>
    <other_info_on_name type="etymology">For the state Idaho</other_info_on_name>
    <other_info_on_name type="fna_id">116338</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Hooker" date="unknown" rank="genus">Platyspermum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 68, plate 18, fig. B. 1830,</place_in_publication>
      <other_info_on_pub>not Hoffmann 1814</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Platyspermum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants scapose.</text>
      <biological_entity id="o11392" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems absent.</text>
      <biological_entity id="o11393" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal, rosulate, petiolate, blade margins entire or lyrately lobed.</text>
      <biological_entity id="o11394" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o11395" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="lyrately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences (few-to-many) on peduncle from basal rosette.</text>
      <biological_entity id="o11397" name="peduncle" name_original="peduncle" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o11398" name="rosette" name_original="rosette" src="d0_s3" type="structure" />
      <relation from="o11396" id="r792" name="on" negation="false" src="d0_s3" to="o11397" />
      <relation from="o11397" id="r793" name="from" negation="false" src="d0_s3" to="o11398" />
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels ascending or erect, slender.</text>
      <biological_entity id="o11396" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o11399" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o11396" id="r794" name="fruiting" negation="false" src="d0_s4" to="o11399" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals ovate or oblong;</text>
      <biological_entity id="o11400" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o11401" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals oblanceolate, (slightly longer than sepals, apex obtuse);</text>
      <biological_entity id="o11402" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o11403" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stamens (erect), slightly tetradynamous;</text>
      <biological_entity id="o11404" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" notes="" src="d0_s7" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
      <biological_entity id="o11405" name="stamen" name_original="stamens" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>filaments not dilated basally;</text>
      <biological_entity id="o11406" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o11407" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers ovate or broadly so, (apex obtuse);</text>
      <biological_entity id="o11408" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o11409" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character name="shape" src="d0_s9" value="broadly" value_original="broadly" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>nectar glands lateral, semiannular, intrastaminal, median glands present (smaller, distinct).</text>
      <biological_entity id="o11410" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="nectar" id="o11411" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="semiannular" value_original="semiannular" />
        <character is_modifier="false" name="position" src="d0_s10" value="intrastaminal" value_original="intrastaminal" />
      </biological_entity>
      <biological_entity constraint="median" id="o11412" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits sessile or, rarely, stipitate, orbicular or orbicular-ovate, smooth, strongly latiseptate;</text>
      <biological_entity id="o11413" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s11" value="," value_original="," />
        <character is_modifier="false" name="architecture" src="d0_s11" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="orbicular-ovate" value_original="orbicular-ovate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s11" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves not veined or obscurely veined;</text>
      <biological_entity id="o11414" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="veined" value_original="veined" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s12" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>replum slightly flattened;</text>
      <biological_entity id="o11415" name="replum" name_original="replum" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>septum complete, (hyaline);</text>
      <biological_entity id="o11416" name="septum" name_original="septum" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma capitate.</text>
      <biological_entity id="o11417" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds flattened, broadly winged, orbicular;</text>
      <biological_entity id="o11418" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s16" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s16" value="orbicular" value_original="orbicular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>seed-coat (coarsely reticulate at center), not mucilaginous when wetted;</text>
      <biological_entity id="o11419" name="seed-coat" name_original="seed-coat" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s17" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>cotyledons accumbent.</text>
    </statement>
    <statement id="d0_s19">
      <text>x = 8.</text>
      <biological_entity id="o11420" name="cotyledon" name_original="cotyledons" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s18" value="accumbent" value_original="accumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o11421" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>w North America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="w North America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>65.</number>
  <discussion>Species 1.</discussion>
  
</bio:treatment>