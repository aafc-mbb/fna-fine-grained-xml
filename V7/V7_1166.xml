<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">678</other_info_on_meta>
    <other_info_on_meta type="treatment_page">680</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="S. Watson" date="1871" rank="genus">caulanthus</taxon_name>
    <taxon_name authority="(S. Watson) Payson" date="1923" rank="species">cooperi</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>9: 293. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus caulanthus;species cooperi</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094871</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelypodium</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">cooperi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>12: 246. 1877</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Thelypodium;species cooperi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Guillenia</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="unknown" rank="species">cooperi</taxon_name>
    <taxon_hierarchy>genus Guillenia;species cooperi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>puberulent or glabrous (trichomes simple and subappressed, and 2-rayed).</text>
      <biological_entity id="o8264" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect to ascending (often flexuous, weak, often tangled with desert shrubs), usually branched distally, 1–8 dm, glabrous or puberulent.</text>
      <biological_entity id="o8265" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="8" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves rosulate;</text>
      <biological_entity constraint="basal" id="o8266" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.3–2.5 cm;</text>
      <biological_entity id="o8267" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s4" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblanceolate to spatulate, 0.7–6 cm × 2–27 mm, margins usually coarsely dentate or somewhat pinnatifid, rarely entire, (surfaces glabrous).</text>
      <biological_entity id="o8268" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="spatulate" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="27" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8269" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually coarsely" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="somewhat; somewhat" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (median) sessile;</text>
      <biological_entity constraint="cauline" id="o8270" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade lanceolate or oblong, 1.5–7.5 cm × 5–20 mm (smaller distally, base amplexicaul to sagittate), margins dentate or entire, (surfaces glabrous).</text>
      <biological_entity id="o8271" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s7" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8272" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (lax), without a terminal cluster of sterile flowers.</text>
      <biological_entity constraint="terminal" id="o8274" name="cluster" name_original="cluster" src="d0_s8" type="structure" />
      <biological_entity id="o8275" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o8273" id="r595" name="without" negation="false" src="d0_s8" to="o8274" />
      <relation from="o8274" id="r596" name="part_of" negation="false" src="d0_s8" to="o8275" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels reflexed, 1–4.5 mm, usually glabrous, rarely puberulent.</text>
      <biological_entity id="o8273" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o8276" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o8273" id="r597" name="fruiting" negation="false" src="d0_s9" to="o8276" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals erect, (purplish or yellow-green), narrowly lanceolate, 3–6.5 × 0.8–1.5 mm (equal);</text>
      <biological_entity id="o8277" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8278" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals yellow-green to purplish (often with purple veins), 4.5–9 mm, blade 2–3 × 0.7–1.5 mm, not crisped, claw narrowly oblong-oblanceolate, 2.5–7 × 1–1.5 mm;</text>
      <biological_entity id="o8279" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8280" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="yellow-green" name="coloration" src="d0_s11" to="purplish" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8281" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o8282" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="oblong-oblanceolate" value_original="oblong-oblanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments slightly tetradynamous, median pairs 2–4.5 mm, lateral pair 1.5–3.5 mm;</text>
      <biological_entity id="o8283" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8284" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
        <character is_modifier="false" name="position" src="d0_s12" value="median" value_original="median" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers oblong, equal, 1.5–2 mm.</text>
      <biological_entity id="o8285" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o8286" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits usually reflexed, rarely divaricate (often subfalcate), terete, 2–6 cm × 1.5–2.5 mm;</text>
      <biological_entity id="o8287" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s14" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="terete" value_original="terete" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s14" to="6" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves each with prominent midvein, (glabrous or puberulent);</text>
      <biological_entity id="o8288" name="valve" name_original="valves" src="d0_s15" type="structure" />
      <biological_entity id="o8289" name="midvein" name_original="midvein" src="d0_s15" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s15" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o8288" id="r598" name="with" negation="false" src="d0_s15" to="o8289" />
    </statement>
    <statement id="d0_s16">
      <text>ovules 24–48 per ovary;</text>
      <biological_entity id="o8290" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o8291" from="24" name="quantity" src="d0_s16" to="48" />
      </biological_entity>
      <biological_entity id="o8291" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.2–2.7 mm;</text>
      <biological_entity id="o8292" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s17" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma slightly 2-lobed.</text>
      <biological_entity id="o8293" name="stigma" name_original="stigma" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s18" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 1–2 × 1–1.2 mm. 2n = 28.</text>
      <biological_entity id="o8294" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s19" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s19" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8295" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Jan-)Feb–Mar.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Mar" from="Feb" />
        <character name="flowering time" char_type="atypical_range" to="Mar" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Desert shrubs, woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="desert shrubs" />
        <character name="habitat" value="woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev., Utah; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <discussion>Caulanthus cooperi is distributed in the Colorado and Mojave deserts in western Arizona, central and southern California, southern Nevada, and southern Utah.</discussion>
  
</bio:treatment>