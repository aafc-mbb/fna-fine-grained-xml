<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">351</other_info_on_meta>
    <other_info_on_meta type="treatment_page">393</other_info_on_meta>
    <other_info_on_meta type="illustration_page">390</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="(S. Watson) Al-Shehbaz" date="2003" rank="species">parishii</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>13: 388. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species parishii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094502</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">parishii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>22: 468. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arabis;species parishii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>short to long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>sexual;</text>
      <biological_entity id="o7007" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="sexual" value_original="sexual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>caudex often woody.</text>
      <biological_entity id="o7008" name="caudex" name_original="caudex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="texture" src="d0_s3" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems usually 1 per caudex branch, arising from center of rosette near ground surface, 0.3–1.4 dm, densely pubescent proximally, trichomes short-stalked, 2–8-rayed, 0.2–0.4 mm, sparsely pubescent distally.</text>
      <biological_entity id="o7009" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character constraint="per caudex branch" constraintid="o7010" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="from center" constraintid="o7011" is_modifier="false" name="orientation" notes="" src="d0_s4" value="arising" value_original="arising" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="1.4" to_unit="dm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o7010" name="branch" name_original="branch" src="d0_s4" type="structure" />
      <biological_entity id="o7011" name="center" name_original="center" src="d0_s4" type="structure" />
      <biological_entity id="o7012" name="rosette" name_original="rosette" src="d0_s4" type="structure" />
      <biological_entity id="o7013" name="ground" name_original="ground" src="d0_s4" type="structure" />
      <biological_entity id="o7014" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o7015" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o7011" id="r505" name="part_of" negation="false" src="d0_s4" to="o7012" />
      <relation from="o7011" id="r506" name="near" negation="false" src="d0_s4" to="o7013" />
      <relation from="o7011" id="r507" name="near" negation="false" src="d0_s4" to="o7014" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade linear to linear-oblanceolate, 0.5–2 mm wide, margins entire, often ciliate along petiole, trichomes (branched), to 0.6 mm, surfaces densely pubescent, trichomes short-stalked, 6–12-rayed, 0.07–0.15 mm.</text>
      <biological_entity constraint="basal" id="o7016" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o7017" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="linear-oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7018" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="along petiole" constraintid="o7019" is_modifier="false" modifier="often" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o7019" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
      <biological_entity id="o7020" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7021" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o7022" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.07" from_unit="mm" name="some_measurement" src="d0_s5" to="0.15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: 2–8, rarely concealing stem proximally;</text>
      <biological_entity constraint="cauline" id="o7023" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s6" to="8" />
      </biological_entity>
      <biological_entity id="o7024" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o7023" id="r508" modifier="proximally" name="rarely concealing" negation="false" src="d0_s6" to="o7024" />
    </statement>
    <statement id="d0_s7">
      <text>blade auricles absent, surfaces of distalmost leaves densely pubescent.</text>
      <biological_entity constraint="cauline" id="o7025" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o7026" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7027" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o7028" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o7027" id="r509" name="part_of" negation="false" src="d0_s7" to="o7028" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes 5–20-flowered, unbranched.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels ascending, straight, 3–7 mm, pubescent, trichomes appressed, branched.</text>
      <biological_entity id="o7029" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-20-flowered" value_original="5-20-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o7030" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o7031" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o7032" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o7029" id="r510" name="fruiting" negation="false" src="d0_s9" to="o7030" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers ascending at anthesis;</text>
      <biological_entity id="o7033" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals pubescent;</text>
      <biological_entity id="o7034" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals lavender to purple, 8–13 × 2.5–4 mm, glabrous;</text>
      <biological_entity id="o7035" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="lavender" name="coloration" src="d0_s12" to="purple" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s12" to="13" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s12" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollen ellipsoid.</text>
      <biological_entity id="o7036" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits ascending, not appressed to rachis, not secund, straight, edges parallel, 1.5–2.5 cm × 1.8–2.5 mm;</text>
      <biological_entity id="o7037" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
        <character constraint="to rachis" constraintid="o7038" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s14" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o7038" name="rachis" name_original="rachis" src="d0_s14" type="structure" />
      <biological_entity id="o7039" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s14" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves glabrous;</text>
      <biological_entity id="o7040" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 12–20 per ovary;</text>
      <biological_entity id="o7041" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o7042" from="12" name="quantity" src="d0_s16" to="20" />
      </biological_entity>
      <biological_entity id="o7042" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 3–8 mm.</text>
      <biological_entity id="o7043" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s17" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds uniseriate, 1.5–2 × 1–1.5 mm;</text>
      <biological_entity id="o7044" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s18" value="uniseriate" value_original="uniseriate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s18" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>wing distal or continuous, 0.05–0.2 mm wide.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o7045" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s19" value="distal" value_original="distal" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="continuous" value_original="continuous" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s19" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7046" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly hillsides in sagebrush- juniper-pine areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly hillsides" constraint="in sagebrush" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="juniper-pine areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1900-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>65.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Boechera parishii is a distinctive, long-styled, sexual diploid known only from San Bernardino Mountains in San Bernardino County.</discussion>
  
</bio:treatment>