<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">143</other_info_on_meta>
    <other_info_on_meta type="treatment_page">144</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="(Andersson) Koehne" date="1893" rank="section">lanatae</taxon_name>
    <taxon_name authority="Fernald &amp; Wiegand" date="1911" rank="species">calcicola</taxon_name>
    <taxon_name authority="B. Boivin" date="1948" rank="variety">glandulosior</taxon_name>
    <place_of_publication>
      <publication_title>Naturaliste Canad.</publication_title>
      <place_in_publication>75: 221. 1948</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section lanatae;species calcicola;variety glandulosior</taxon_hierarchy>
    <other_info_on_name type="fna_id">242445675</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves: largest medial blade margins green, teeth or glands 5–22 per cm, apex acuminate or acute, adaxial surface slightly glossy or dull.</text>
      <biological_entity id="o18383" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity constraint="blade" id="o18384" name="margin" name_original="margins" src="d0_s0" type="structure" constraint_original="largest medial blade" />
      <biological_entity constraint="blade" id="o18385" name="tooth" name_original="teeth" src="d0_s0" type="structure" constraint_original="largest medial blade">
        <character is_modifier="true" name="coloration" src="d0_s0" value="green" value_original="green" />
      </biological_entity>
      <biological_entity constraint="blade" id="o18386" name="gland" name_original="glands" src="d0_s0" type="structure" constraint_original="largest medial blade">
        <character is_modifier="true" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" constraint="per cm" constraintid="o18387" from="5" name="quantity" src="d0_s0" to="22" />
      </biological_entity>
      <biological_entity id="o18387" name="cm" name_original="cm" src="d0_s0" type="structure" />
      <biological_entity id="o18388" name="apex" name_original="apex" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s0" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18389" name="surface" name_original="surface" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s0" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="reflectance" src="d0_s0" value="dull" value_original="dull" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Pistillate flowers: adaxial nectary 0.2–1.1 mm;</text>
      <biological_entity id="o18390" name="flower" name_original="flowers" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18391" name="nectary" name_original="nectary" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s1" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stipe 0.3–0.6 mm;</text>
      <biological_entity id="o18392" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18393" name="stipe" name_original="stipe" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s2" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ovary pyriform;</text>
      <biological_entity id="o18394" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18395" name="ovary" name_original="ovary" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="pyriform" value_original="pyriform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>styles 0.9–1.8 mm;</text>
      <biological_entity id="o18396" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18397" name="style" name_original="styles" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s4" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stigmas 0.2–0.28–0.36 mm.</text>
      <biological_entity id="o18398" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18399" name="stigma" name_original="stigmas" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.28-0.36" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>No data are available on flowering time (probably early).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Boreal floodplains, alpine wet meadows, dwarf birch thickets, limestone substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="boreal floodplains" />
        <character name="habitat" value="alpine wet meadows" />
        <character name="habitat" value="birch thickets" modifier="dwarf" />
        <character name="habitat" value="limestone substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-3700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3700" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.; Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>92b.</number>
  
</bio:treatment>