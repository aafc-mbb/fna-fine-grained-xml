<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">185</other_info_on_meta>
    <other_info_on_meta type="illustration_page">177</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Engler" date="unknown" rank="family">koeberliniaceae</taxon_name>
    <taxon_name authority="Zuccarini" date="1832" rank="genus">koeberlinia</taxon_name>
    <taxon_name authority="Zuccarini" date="1832" rank="species">spinosa</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>15(2, Beibl.): 74. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family koeberliniaceae;genus koeberlinia;species spinosa</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">220007142</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.5–10 m, spreading, globose, compact to somewhat open.</text>
      <biological_entity id="o15420" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s0" value="globose" value_original="globose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="compact to somewhat" value_original="compact to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="architecture" src="d0_s0" value="open" value_original="open" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± terete, green twigs thorn-tipped, 25–70 cm.</text>
      <biological_entity id="o15421" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s1" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o15422" name="twig" name_original="twigs" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="thorn-tipped" value_original="thorn-tipped" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 0.2–1.5 × 0.5 mm.</text>
      <biological_entity id="o15423" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s2" to="1.5" to_unit="mm" />
        <character name="width" src="d0_s2" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences 3–15 mm.</text>
      <biological_entity id="o15424" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pedicels 3–6 mm.</text>
      <biological_entity id="o15425" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals greenish white, ovate-deltate, 1–2 mm, glabrous;</text>
      <biological_entity id="o15426" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o15427" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate-deltate" value_original="ovate-deltate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals greenish white or cream, obovate or oblanceolate, 3–4.8 × 0.8–1.4 mm;</text>
      <biological_entity id="o15428" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o15429" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="cream" value_original="cream" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="4.8" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s6" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments brownish, 2.8–4 mm, flat;</text>
      <biological_entity id="o15430" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o15431" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 0.8–1 mm, slightly curved;</text>
      <biological_entity id="o15432" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o15433" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s8" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>gynophore (stipe) 0.3–0.8 mm;</text>
      <biological_entity id="o15434" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o15435" name="gynophore" name_original="gynophore" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>ovary ovoid, 1–1.2 mm;</text>
      <biological_entity id="o15436" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15437" name="ovary" name_original="ovary" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>style 0.4–1.4 mm.</text>
      <biological_entity id="o15438" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15439" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits ca. 5 mm, mucronate from persistent style.</text>
      <biological_entity id="o15440" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="5" value_original="5" />
        <character constraint="from style" constraintid="o15441" is_modifier="false" name="shape" src="d0_s12" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o15441" name="style" name_original="style" src="d0_s12" type="structure">
        <character is_modifier="true" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 3–3.5 mm. 2n = 44, 88.</text>
      <biological_entity id="o15442" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15443" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="44" value_original="44" />
        <character name="quantity" src="d0_s13" value="88" value_original="88" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., N.Mex., Tex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <discussion>Infraspecific variation has long been noted in the genus. W. C. Holmes et al. (2008) recognized three largely allopatric varieties.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stipes 0.3-0.5 mm; styles 0.4-0.7 mm; mature thorns less than 15 times longer than base width.</description>
      <determination>1c Koeberlinia spinosa var. wivaggii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stipes 0.4-0.8 mm; styles 0.8-1.4 mm; mature thorns 20+ times longer than base width</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants less than 0.5-2.5 m; young thorns glabrous or finely puberulent; petals 3-4 mm.</description>
      <determination>1a Koeberlinia spinosa var. spinosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants 1-10 m; young thorns densely puberulent; petals 4-4.8 mm.</description>
      <determination>1b Koeberlinia spinosa var. tenuispina</determination>
    </key_statement>
  </key>
</bio:treatment>