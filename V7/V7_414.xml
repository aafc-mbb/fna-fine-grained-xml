<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">276</other_info_on_meta>
    <other_info_on_meta type="mention_page">278</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="mention_page">283</other_info_on_meta>
    <other_info_on_meta type="mention_page">286</other_info_on_meta>
    <other_info_on_meta type="treatment_page">313</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Payson" date="1917" rank="species">incerta</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>4: 261. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species incerta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094673</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="E. Ekman" date="unknown" rank="species">exalata</taxon_name>
    <taxon_hierarchy>genus Draba;species exalata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Payson" date="unknown" rank="species">incerta</taxon_name>
    <taxon_name authority="(Payson) Payson &amp; H. St. John" date="unknown" rank="variety">laevicapsula</taxon_name>
    <taxon_hierarchy>genus Draba;species incerta;variety laevicapsula;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Fernald" date="unknown" rank="species">incerta</taxon_name>
    <taxon_name authority="(Fernald) Rollins" date="unknown" rank="variety">peasei</taxon_name>
    <taxon_hierarchy>genus Draba;species incerta;variety peasei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laevicapsula</taxon_name>
    <taxon_hierarchy>genus Draba;species laevicapsula;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">peasei</taxon_name>
    <taxon_hierarchy>genus Draba;species peasei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(cespitose, often pulvinate);</text>
      <biological_entity id="o5161" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (dense with persistent leaf remains, branches sometimes terminating in sterile rosettes);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o5162" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, (0.2–) 0.4–1.4 (–2.1) dm, often pubescent throughout, sometimes glabrous distally, trichomes often simple and 2–5-rayed, 0.1–0.5 mm, (sometimes with mostly subpectinate ones).</text>
      <biological_entity id="o5163" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.4" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="2.1" to_unit="dm" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s4" to="1.4" to_unit="dm" />
        <character is_modifier="false" modifier="often; throughout" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes; distally" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5164" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves rosulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o5165" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole (0–1 cm), ciliate throughout;</text>
      <biological_entity id="o5166" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="throughout" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade narrowly oblanceolate to linear, (0.4–) 0.6–1.7 (–2.5) cm × (1–) 1.5–3.5 (–5) mm, margins entire, (ciliate, trichomes usually simple, rarely 2-rayed, 0.2–1.1 mm), surfaces usually pubescent with short-stalked, pectinate trichomes, 0.15–0.5 mm, sometimes also with 4–6-rayed ones, (midvein usually obscure abaxially), sometimes glabrous adaxially.</text>
      <biological_entity id="o5167" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s8" to="linear" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_length" src="d0_s8" to="0.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s8" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s8" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5168" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5169" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character constraint="with trichomes" constraintid="o5170" is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.15" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="0.5" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes; adaxially" name="pubescence" notes="" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5170" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="true" name="shape" src="d0_s8" value="pectinate" value_original="pectinate" />
      </biological_entity>
      <biological_entity id="o5171" name="one" name_original="ones" src="d0_s8" type="structure" />
      <relation from="o5169" id="r393" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o5171" />
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves usually 0 (or 1, as a bract);</text>
    </statement>
    <statement id="d0_s10">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o5172" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>blade linear to oblong, margins entire, surfaces pubescent as basal.</text>
      <biological_entity id="o5173" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="oblong" />
      </biological_entity>
      <biological_entity id="o5174" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o5175" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character constraint="as basal" constraintid="o5176" is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o5176" name="basal" name_original="basal" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Racemes 3–14 (–30) -flowered, usually ebracteate, rarely proximalmost flowers bracteate, elongated in fruit;</text>
      <biological_entity id="o5177" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-14(-30)-flowered" value_original="3-14(-30)-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
      </biological_entity>
      <biological_entity id="o5178" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="rarely" name="position" src="d0_s12" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="bracteate" value_original="bracteate" />
        <character constraint="in fruit" constraintid="o5179" is_modifier="false" name="length" src="d0_s12" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o5179" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>rachis not flexuous, glabrous or pubescent as stem.</text>
      <biological_entity id="o5181" name="stem" name_original="stem" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruiting pedicels ascending, straight, (2.5–) 4–11 (–27) mm, glabrous or pubescent, trichomes 2–5-rayed or pectinate.</text>
      <biological_entity id="o5180" name="rachis" name_original="rachis" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character constraint="as stem" constraintid="o5181" is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o5182" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <biological_entity id="o5183" name="whole-organism" name_original="" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="27" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o5184" name="trichome" name_original="trichomes" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="pectinate" value_original="pectinate" />
      </biological_entity>
      <relation from="o5180" id="r394" name="fruiting" negation="false" src="d0_s14" to="o5182" />
    </statement>
    <statement id="d0_s15">
      <text>Flowers: sepals broadly ovate, 2.5–3.5 (–4) mm, pubescent, (trichomes simple and 2-rayed or 3-rayed);</text>
      <biological_entity id="o5185" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o5186" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals yellow (fading white), oblanceolate to obovate, 4–6 × 1.5–2.5 mm;</text>
      <biological_entity id="o5187" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o5188" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s16" to="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s16" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers ovate, 0.3–0.5 mm.</text>
      <biological_entity id="o5189" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o5190" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits broadly ovate to lanceolate, plane, flattened, 5–9 (–11) × 2–4 mm;</text>
      <biological_entity id="o5191" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s18" to="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s18" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s18" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s18" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s18" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>valves glabrous or puberulent, trichomes simple and 2-rayed, 0.05–0.3 mm;</text>
      <biological_entity id="o5192" name="valve" name_original="valves" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o5193" name="trichome" name_original="trichomes" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s19" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 8–16 (–20) per ovary;</text>
      <biological_entity id="o5194" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" from="16" from_inclusive="false" name="atypical_quantity" src="d0_s20" to="20" />
        <character char_type="range_value" constraint="per ovary" constraintid="o5195" from="8" name="quantity" src="d0_s20" to="16" />
      </biological_entity>
      <biological_entity id="o5195" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 0.2–0.9 mm.</text>
      <biological_entity id="o5196" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s21" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds oblong, 1.1–1.5 × 0.7–1 mm. 2n = 112.</text>
      <biological_entity id="o5197" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s22" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s22" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5198" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="112" value_original="112" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops, talus, gravelly areas, tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="gravelly areas" />
        <character name="habitat" value="tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-3300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Que., Yukon; Alaska, Colo., Idaho, Mont., Nev., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>48.</number>
  <discussion>Draba incerta was shown by G. A. Mulligan (1972) to be sexually reproducing and 14-ploid with x = 8. It is often confused with the apomict D. oligosperma (2n = 32, 64). Draba incerta is readily separated from D. oligosperma by having well-formed (versus abortive) anthers and pollen, stalked (versus sessile) leaf trichomes, and ciliate (versus non-ciliate) basal leaves with obscure (versus prominent) midveins. Although both species have leafless scapes, one often finds a bract adnate to, or subtending, the proximalmost pedicel in D. incerta.</discussion>
  <discussion>Draba incerta is found near sea level in Alaska.</discussion>
  
</bio:treatment>