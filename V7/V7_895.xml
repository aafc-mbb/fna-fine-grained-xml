<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">558</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">halimolobeae</taxon_name>
    <taxon_name authority="Tausch" date="1836" rank="genus">halimolobos</taxon_name>
    <taxon_name authority="(A. Gray) O. E. Schulz in H. G. A. Engler" date="unknown" rank="species">diffusa</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler, Pflanzenr.</publication_title>
      <place_in_publication>86[IV,105]: 228. 1924</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe halimolobeae;genus halimolobos;species diffusa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095010</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisymbrium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">diffusum</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>3(5): 8. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sisymbrium;species diffusum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hesperis</taxon_name>
    <taxon_name authority="(A. Gray) Kuntze" date="unknown" rank="species">diffusa</taxon_name>
    <taxon_hierarchy>genus Hesperis;species diffusa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sophia</taxon_name>
    <taxon_name authority="(A. Gray) Tidestrom" date="unknown" rank="species">diffusa</taxon_name>
    <taxon_hierarchy>genus Sophia;species diffusa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials.</text>
      <biological_entity id="o26808" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, often paniculately branched distally, (1.2–) 3–7.5 (–12) dm, trichomes sessile or subsessile.</text>
      <biological_entity id="o26809" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character is_modifier="false" modifier="often paniculately; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="1.2" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="12" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s1" to="7.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o26810" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves absent on older plants.</text>
      <biological_entity constraint="basal" id="o26811" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="on plants" constraintid="o26812" is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o26812" name="plant" name_original="plants" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="older" value_original="older" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves petiolate or (distal) sessile;</text>
      <biological_entity constraint="cauline" id="o26813" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–1 cm;</text>
      <biological_entity id="o26814" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblanceolate, lanceolate to oblong, or elliptic, (1–) 2–5.5 (–7) cm × (4–) 7–20 (–30) mm (smaller distally), base cuneate, margins sinuately lobed or dentate, surfaces with minutely stalked to subsessile trichomes.</text>
      <biological_entity id="o26815" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblong or elliptic" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="oblong or elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s5" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s5" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26816" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o26817" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sinuately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o26818" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o26819" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character char_type="range_value" from="minutely stalked" is_modifier="true" name="architecture" src="d0_s5" to="subsessile" />
      </biological_entity>
      <relation from="o26818" id="r1809" name="with" negation="false" src="d0_s5" to="o26819" />
    </statement>
    <statement id="d0_s6">
      <text>Racemes slightly to considerably elongated in fruit.</text>
      <biological_entity id="o26821" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting pedicels usually divaricate, rarely slightly descending, (2–) 3–6 (–9) mm.</text>
      <biological_entity id="o26820" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o26821" is_modifier="false" modifier="slightly to considerably" name="length" src="d0_s6" value="elongated" value_original="elongated" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s7" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="rarely slightly" name="orientation" src="d0_s7" value="descending" value_original="descending" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26822" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <relation from="o26820" id="r1810" name="fruiting" negation="false" src="d0_s7" to="o26822" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals slightly spreading, 1.2–2 × 0.4–0.7 mm;</text>
      <biological_entity id="o26823" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o26824" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s8" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s8" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals (slightly spreading), spatulate, (slender), 1.8–2.5 × 0.7–1 mm, claw distinctly differentiated from blade, 0.5–1 mm;</text>
      <biological_entity id="o26825" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s9" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26826" name="petal" name_original="petals" src="d0_s9" type="structure" />
      <biological_entity id="o26827" name="claw" name_original="claw" src="d0_s9" type="structure">
        <character constraint="from blade" constraintid="o26828" is_modifier="false" modifier="distinctly" name="variability" src="d0_s9" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26828" name="blade" name_original="blade" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>filaments spreading, 2.2–3 mm, longer than petals;</text>
      <biological_entity id="o26829" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o26830" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character constraint="than petals" constraintid="o26831" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o26831" name="petal" name_original="petals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>anthers 0.3–0.5 mm.</text>
      <biological_entity id="o26832" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o26833" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits divaricate or slightly descending, straight, subtorulose, linear, terete, 0.6–1.4 (–1.7) cm × 0.5–0.8 mm;</text>
      <biological_entity id="o26834" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s12" value="descending" value_original="descending" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subtorulose" value_original="subtorulose" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" value_original="terete" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s12" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s12" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules 16–24 per ovary;</text>
      <biological_entity id="o26835" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o26836" from="16" name="quantity" src="d0_s13" to="24" />
      </biological_entity>
      <biological_entity id="o26836" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>style 0.7–1 (–1.5) mm.</text>
      <biological_entity id="o26837" name="style" name_original="style" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds uniseriate, 0.8–1 × 0.4–0.6 mm.</text>
      <biological_entity id="o26838" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s15" value="uniseriate" value_original="uniseriate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s15" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s15" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Jul-mid Nov.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Nov" from="mid Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded talus, ravines, granite outcrops, rock crevices, bluffs, steep canyons, limestone slopes, oak-juniper communities, igneous slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded talus" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="granite outcrops" />
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="steep canyons" />
        <character name="habitat" value="limestone slopes" />
        <character name="habitat" value="oak-juniper communities" />
        <character name="habitat" value="igneous slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="past_name">Halimolobus diffusus</other_name>
  
</bio:treatment>