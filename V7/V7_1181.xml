<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">245</other_info_on_meta>
    <other_info_on_meta type="treatment_page">687</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="O. E. Schulz in H. G. A. Engler" date="1924" rank="genus">COELOPHRAGMUS</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler, Pflanzenr.</publication_title>
      <place_in_publication>86[IV,105]: 157, fig. 25. 1924</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus COELOPHRAGMUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kilos (Latin coelus), hollow, and phragmos, partition, alluding to deep pits on sides of fruit septum where seeds are located</other_info_on_name>
    <other_info_on_name type="fna_id">107574</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous or pilose.</text>
      <biological_entity id="o11030" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, unbranched or branched distally.</text>
      <biological_entity id="o11031" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate;</text>
      <biological_entity id="o11032" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal not rosulate, blade margins pinnatifid or runcinate, lobes dentate or entire;</text>
      <biological_entity constraint="basal" id="o11033" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o11034" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="runcinate" value_original="runcinate" />
      </biological_entity>
      <biological_entity id="o11035" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline blade (base auriculate or amplexicaul), margins similar to basal.</text>
      <biological_entity constraint="cauline" id="o11036" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o11037" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (corymbose), considerably elongated in fruit.</text>
      <biological_entity id="o11039" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels horizontal, stout.</text>
      <biological_entity id="o11038" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o11039" is_modifier="false" modifier="considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o11040" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o11038" id="r777" name="fruiting" negation="false" src="d0_s9" to="o11040" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals erect to ascending, oblong, lateral pair not saccate basally;</text>
      <biological_entity id="o11041" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o11042" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s10" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white to lavender, (longer than sepals), spatulate, claw and blade obscurely differentiated;</text>
      <biological_entity id="o11043" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o11044" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="lavender" />
      </biological_entity>
      <biological_entity id="o11045" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="obscurely" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o11046" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="obscurely" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o11047" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o11048" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments dilated basally;</text>
      <biological_entity id="o11049" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o11050" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers oblong;</text>
      <biological_entity id="o11051" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o11052" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectar glands: lateral annular, median glands confluent with lateral.</text>
      <biological_entity constraint="nectar" id="o11053" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s15" value="annular" value_original="annular" />
      </biological_entity>
      <biological_entity constraint="median" id="o11054" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character constraint="with lateral" constraintid="o11055" is_modifier="false" name="arrangement" src="d0_s15" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o11055" name="lateral" name_original="lateral" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits sessile or shortly stipitate, linear, straight, terete;</text>
      <biological_entity id="o11056" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s16" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s16" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves each with distinct midvein;</text>
      <biological_entity id="o11057" name="valve" name_original="valves" src="d0_s17" type="structure" />
      <biological_entity id="o11058" name="midvein" name_original="midvein" src="d0_s17" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o11057" id="r778" name="with" negation="false" src="d0_s17" to="o11058" />
    </statement>
    <statement id="d0_s18">
      <text>replum rounded;</text>
      <biological_entity id="o11059" name="replum" name_original="replum" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>septum complete, (deeply partitioned between seeds);</text>
      <biological_entity id="o11060" name="septum" name_original="septum" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 78–120 per ovary;</text>
      <biological_entity id="o11061" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o11062" from="78" name="quantity" src="d0_s20" to="120" />
      </biological_entity>
      <biological_entity id="o11062" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style distinct;</text>
      <biological_entity id="o11063" name="style" name_original="style" src="d0_s21" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s21" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma capitate, deeply 2-lobed.</text>
      <biological_entity id="o11064" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s22" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds uniseriate, plump, not winged, ovoid;</text>
      <biological_entity id="o11065" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s23" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="size" src="d0_s23" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s23" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s23" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>seed-coat not mucilaginous when wetted;</text>
      <biological_entity id="o11066" name="seed-coat" name_original="seed-coat" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s24" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>cotyledons incumbent.</text>
      <biological_entity id="o11067" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s25" value="incumbent" value_original="incumbent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>84.</number>
  <discussion>Species 1.</discussion>
  <discussion>Coelophragmus was described as consisting of two species. The type of one, C. umbrosus O. E. Schulz (1924), belongs in Dryopetalon and is conspecific with the type of D. runcinatum A. Gray (1853). Because removal of that species does not constitute effective lectotypification of the genus name, C. auriculatus O. E. Schulz is designated here as type of Coelophragmus.</discussion>
  <discussion>Although Coelophragmus auriculatus was originally described as Sisymbrium auriculatum and recognized as such by every North American botanist, including E. B. Payson (1922) and R. C. Rollins (1993), the species clearly is not a Sisymbrium (S. I. Warwick et al. 2002, 2006b), a genus almost exclusively restricted to the Old World. Coelophragmus is recognized herein as a monotypic genus, and its affinities are nearest Dryopetalon (I. A. Al-Shehbaz et al. 2006). It is unique among all North American genera of the family by having fruit septums deeply partitioned between seeds.</discussion>
  
</bio:treatment>