<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">363</other_info_on_meta>
    <other_info_on_meta type="mention_page">377</other_info_on_meta>
    <other_info_on_meta type="treatment_page">376</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="(Rollins) Dorn" date="2003" rank="species">falcatoria</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>55: 3. 2003</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species falcatoria</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094559</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="species">falcatoria</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>212: 106. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arabis;species falcatoria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>short to long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>apomictic;</text>
      <biological_entity id="o20097" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="apomictic" value_original="apomictic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>caudex sometimes woody.</text>
      <biological_entity id="o20098" name="caudex" name_original="caudex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes" name="texture" src="d0_s3" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems usually 2–7 per caudex branch, arising from margin of rosette near ground surface, 0.5–2 (–3) dm, densely pubescent proximally, trichomes simple and stalked, 2-rayed, 0.3–0.8 mm, glabrous distally.</text>
      <biological_entity id="o20099" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per caudex branch" constraintid="o20100" from="2" name="quantity" src="d0_s4" to="7" />
        <character constraint="from margin" constraintid="o20101" is_modifier="false" name="orientation" notes="" src="d0_s4" value="arising" value_original="arising" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s4" to="3" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="2" to_unit="dm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o20100" name="branch" name_original="branch" src="d0_s4" type="structure" />
      <biological_entity id="o20101" name="margin" name_original="margin" src="d0_s4" type="structure" />
      <biological_entity id="o20102" name="rosette" name_original="rosette" src="d0_s4" type="structure" />
      <biological_entity id="o20103" name="ground" name_original="ground" src="d0_s4" type="structure" />
      <biological_entity id="o20104" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o20105" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="stalked" value_original="stalked" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s4" to="0.8" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o20101" id="r1385" name="part_of" negation="false" src="d0_s4" to="o20102" />
      <relation from="o20101" id="r1386" name="near" negation="false" src="d0_s4" to="o20103" />
      <relation from="o20101" id="r1387" name="near" negation="false" src="d0_s4" to="o20104" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade narrowly oblanceolate, 1–3 mm wide, margins entire, ciliate, trichomes to 1 mm, surfaces pubescent, trichomes stalked, 2–4-rayed, 0.3–0.8 mm.</text>
      <biological_entity constraint="basal" id="o20106" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o20107" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20108" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o20109" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20110" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20111" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="stalked" value_original="stalked" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: 3–7 (–10), not concealing stem;</text>
      <biological_entity constraint="cauline" id="o20112" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="10" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="7" />
      </biological_entity>
      <biological_entity id="o20113" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o20112" id="r1388" name="concealing" negation="true" src="d0_s6" to="o20113" />
    </statement>
    <statement id="d0_s7">
      <text>blade auricles 0.2–1 mm, surfaces of distalmost leaves glabrous.</text>
      <biological_entity constraint="cauline" id="o20114" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o20115" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20116" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o20117" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o20116" id="r1389" name="part_of" negation="false" src="d0_s7" to="o20117" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes 6–15-flowered, unbranched.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels horizontal to divaricate-ascending, straight, 3–9 mm, glabrous.</text>
      <biological_entity id="o20118" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="6-15-flowered" value_original="6-15-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s9" to="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20119" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o20118" id="r1390" name="fruiting" negation="false" src="d0_s9" to="o20119" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers ascending at anthesis;</text>
      <biological_entity id="o20120" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals pubescent;</text>
      <biological_entity id="o20121" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals white or lavender, 5–7 × 1.5–2.5 mm, glabrous;</text>
      <biological_entity id="o20122" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="lavender" value_original="lavender" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollen spheroid.</text>
      <biological_entity id="o20123" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="spheroid" value_original="spheroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits horizontal to divaricate-ascending, not appressed to rachis, not secund, usually strongly curved, edges parallel, 3.5–6.5 cm × 1.7–2 mm;</text>
      <biological_entity id="o20124" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s14" to="divaricate-ascending" />
        <character constraint="to rachis" constraintid="o20125" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s14" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="usually strongly" name="course" src="d0_s14" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o20125" name="rachis" name_original="rachis" src="d0_s14" type="structure" />
      <biological_entity id="o20126" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s14" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves glabrous;</text>
      <biological_entity id="o20127" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 50–84 per ovary;</text>
      <biological_entity id="o20128" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o20129" from="50" name="quantity" src="d0_s16" to="84" />
      </biological_entity>
      <biological_entity id="o20129" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.05–0.5 mm.</text>
      <biological_entity id="o20130" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds uniseriate, 1.5–2 × 1.1–1.5 mm;</text>
      <biological_entity id="o20131" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s18" value="uniseriate" value_original="uniseriate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s18" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>wing continuous, 0.1–0.2 mm wide.</text>
      <biological_entity id="o20132" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="continuous" value_original="continuous" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s19" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops and gravelly soil in sagebrush and mountain shrub communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="gravelly soil" constraint="in sagebrush and mountain" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="mountain" />
        <character name="habitat" value="shrub communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Although the exact parentage of Boechera falcatoria, an apomictic hybrid, remains in doubt, it is virtually certain that B. cusickii contributed at least one genome. Boechera falcatoria has been confused with both B. pendulina and B. perennans, but is amply distinct from both (see M. D. Windham and I. A. Al-Shehbaz 2007 for detailed comparison). Despite the proximity of the type locality to both Idaho and Nevada, the species is known only from northwestern Box Elder County.</discussion>
  
</bio:treatment>