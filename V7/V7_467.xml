<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="treatment_page">337</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Elven &amp; Al-Shehbaz" date="2008" rank="species">simmonsii</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>18: 326. 2008</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species simmonsii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094832</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">alpina</taxon_name>
    <taxon_name authority="Simmons" date="unknown" rank="variety">gracilescens</taxon_name>
    <place_of_publication>
      <publication_title>Vasc. Pl. Ellesmereland,</publication_title>
      <place_in_publication>83, plate 6, figs. 1–3. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Draba;species alpina;variety gracilescens;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o20194" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex branched (with persistent leaf-bases and petioles);</text>
    </statement>
    <statement id="d0_s2">
      <text>scapose.</text>
      <biological_entity id="o20195" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems unbranched, (0.15–) 0.3–1.1 (–1.3) dm, often pubescent throughout, sometimes sparsely pubescent distally, trichomes stalked, 2–4-rayed, and fewer, simple ones, 0.1–0.3 (–0.6) mm.</text>
      <biological_entity id="o20196" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.15" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="0.3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.1" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="1.3" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s3" to="1.1" to_unit="dm" />
        <character is_modifier="false" modifier="often; throughout" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes sparsely; distally" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20197" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="stalked" value_original="stalked" />
        <character is_modifier="false" name="quantity" src="d0_s3" value="fewer" value_original="fewer" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s3" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves (not imbricate);</text>
    </statement>
    <statement id="d0_s5">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o20198" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole (base somewhat thickened), margin ciliate, (trichomes simple, 0.5–1 mm);</text>
      <biological_entity id="o20199" name="petiole" name_original="petiole" src="d0_s7" type="structure" />
      <biological_entity id="o20200" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblong to oblong-lanceolate, 0.5–1.6 cm × 2–5 mm, margins entire (apex subacute to obtuse), surfaces pubescent throughout or glabrous adaxially, with simple and stalked, 2–4 (–6) -rayed, stellate to subdendritic trichomes.</text>
      <biological_entity id="o20201" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="oblong-lanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s8" to="1.6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20202" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o20203" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o20204" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s8" value="stellate" value_original="stellate" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="subdendritic" value_original="subdendritic" />
      </biological_entity>
      <relation from="o20203" id="r1396" name="with" negation="false" src="d0_s8" to="o20204" />
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 0.</text>
      <biological_entity constraint="cauline" id="o20205" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Racemes (2–) 4–10 (–14) -flowered, ebracteate, slightly elongated in fruit;</text>
      <biological_entity id="o20206" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="(2-)4-10(-14)-flowered" value_original="(2-)4-10(-14)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o20207" is_modifier="false" modifier="slightly" name="length" src="d0_s10" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o20207" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>rachis not flexuous (straight), pubescent as stem.</text>
      <biological_entity id="o20209" name="stem" name_original="stem" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting pedicels divaricate, straight, 2.5–10 mm, pubescent as stem.</text>
      <biological_entity id="o20208" name="rachis" name_original="rachis" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s11" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o20209" is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20210" name="pedicel" name_original="pedicels" src="d0_s12" type="structure" />
      <biological_entity id="o20211" name="stem" name_original="stem" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o20208" id="r1397" name="fruiting" negation="false" src="d0_s12" to="o20210" />
    </statement>
    <statement id="d0_s13">
      <text>Flowers: sepals (purplish green), oblong, (2.5–) 2.8–3.5 (–3.8) mm, pubescent, (trichomes simple and sometimes short-stalked, 2-rayed);</text>
      <biological_entity id="o20212" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="2.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20213" name="sepal" name_original="sepals" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>petals pale-yellow, narrowly obovate, (sides non-parallel), (3.5–) 3.8–5.5 (–5.8) × (2.5–) 2.8–4 (–4.6) mm, (emarginate);</text>
      <biological_entity id="o20214" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o20215" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s14" to="3.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s14" to="5.8" to_unit="mm" />
        <character char_type="range_value" from="3.8" from_unit="mm" name="length" src="d0_s14" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_width" src="d0_s14" to="2.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s14" to="4.6" to_unit="mm" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="width" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate, ca. 0.5 mm.</text>
      <biological_entity id="o20216" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o20217" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits lanceolate, plane, slightly flattened, 5.5–9 (–11) × 2.3–3.8 mm;</text>
      <biological_entity id="o20218" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s16" to="11" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="length" src="d0_s16" to="9" to_unit="mm" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="width" src="d0_s16" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves pubescent, trichomes simple, sometimes with fewer, 2-rayed ones, 0.1–0.3 mm;</text>
      <biological_entity id="o20219" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20220" name="trichome" name_original="trichomes" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s17" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20221" name="one" name_original="ones" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="fewer" value_original="fewer" />
      </biological_entity>
      <relation from="o20220" id="r1398" modifier="sometimes" name="with" negation="false" src="d0_s17" to="o20221" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 16–24 (–28) per ovary;</text>
      <biological_entity id="o20222" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" from="24" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="28" />
        <character char_type="range_value" constraint="per ovary" constraintid="o20223" from="16" name="quantity" src="d0_s18" to="24" />
      </biological_entity>
      <biological_entity id="o20223" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style 0.1–0.3 mm (stigma as wide as style).</text>
      <biological_entity id="o20224" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s19" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds (dark-brown), oblong-ovate, 1–1.3 × 0.7–0.8 mm.</text>
      <biological_entity id="o20225" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="oblong-ovate" value_original="oblong-ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s20" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s20" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="late Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry open ground, open patches in meadows or heath vegetation, outwash plains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry open ground" />
        <character name="habitat" value="open patches" constraint="in meadows or heath vegetation" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="heath vegetation" />
        <character name="habitat" value="plains" modifier="outwash" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Nunavut.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>101.</number>
  
</bio:treatment>