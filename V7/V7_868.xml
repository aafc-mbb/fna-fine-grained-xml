<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>James G. Harris</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">234</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="mention_page">236</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="mention_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="mention_page">547</other_info_on_meta>
    <other_info_on_meta type="mention_page">549</other_info_on_meta>
    <other_info_on_meta type="mention_page">551</other_info_on_meta>
    <other_info_on_meta type="treatment_page">546</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">euclidieae</taxon_name>
    <taxon_name authority="Sternberg &amp; Hoppe" date="1815" rank="genus">BRAYA</taxon_name>
    <place_of_publication>
      <publication_title>Denkschr. Königl.-Baier. Bot. Ges. Regensburg</publication_title>
      <place_in_publication>1(1): 65. 1815</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe euclidieae;genus BRAYA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Franz Gabriel de Bray, 1765–1832, French ambassador to Bavaria, head of Regensberg Botanical Society</other_info_on_name>
    <other_info_on_name type="fna_id">104570</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="R. Brown" date="unknown" rank="genus">Platypetalum</taxon_name>
    <taxon_hierarchy>genus Platypetalum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(sometimes pulvinate, caudex simple or many-branched);</text>
    </statement>
    <statement id="d0_s2">
      <text>scapose or not;</text>
    </statement>
    <statement id="d0_s3">
      <text>usually pubescent or pilose, sometimes glabrous, trichomes short-stalked, forked, subdendritic, or submalpighiaceous, mixed with simple ones (rarely exclusively).</text>
      <biological_entity id="o39420" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character name="architecture" src="d0_s2" value="not" value_original="not" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o39421" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="false" name="shape" src="d0_s3" value="forked" value_original="forked" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subdendritic" value_original="subdendritic" />
        <character constraint="with ones" constraintid="o39422" is_modifier="false" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o39422" name="one" name_original="ones" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems erect to decumbent or ascending, unbranched or branched.</text>
      <biological_entity id="o39423" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s4" to="decumbent or ascending" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and, sometimes, cauline;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate or sessile;</text>
      <biological_entity id="o39424" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal rosulate, petiolate, blade margins entire, sinuate, dentate, or, rarely, pinnately lobed;</text>
      <biological_entity constraint="basal" id="o39425" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o39426" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="rarely; pinnately" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cauline usually absent, rarely few present, (sub) sessile, blade margins usually entire, rarely dentate or pinnately lobed.</text>
      <biological_entity constraint="cauline" id="o39427" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely" name="quantity" src="d0_s8" value="few" value_original="few" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="blade" id="o39428" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes (corymbose, sometimes bracteate basally or throughout), elongated or not in fruit.</text>
      <biological_entity id="o39430" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <relation from="o39429" id="r2666" name="in" negation="false" src="d0_s9" to="o39430" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels erect, divaricate, or ascending, slender (much narrower than fruit).</text>
      <biological_entity id="o39429" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character name="length" src="d0_s9" value="not" value_original="not" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o39431" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o39429" id="r2667" name="fruiting" negation="false" src="d0_s10" to="o39431" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals [sometimes persistent], oblong [ovate], lateral pair not saccate basally (sometimes slightly so in B. humilis and B. linearis);</text>
      <biological_entity id="o39432" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s11" value="saccate" value_original="saccate" />
      </biological_entity>
      <biological_entity id="o39433" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals white, pink, or purple [rarely pale-yellow], obovate, oblanceolate, or spatulate, (slightly to much longer than sepals), claw distinct or not, (shorter than sepal, apex obtuse or rounded);</text>
      <biological_entity id="o39434" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o39435" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o39436" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character name="fusion" src="d0_s12" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o39437" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o39438" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments dilated or not basally;</text>
      <biological_entity id="o39439" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o39440" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
        <character name="shape" src="d0_s14" value="not basally" value_original="not basally" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate or oblong, (apex usually obtuse, sometimes apiculate);</text>
      <biological_entity id="o39441" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o39442" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands (4), lateral, 1 on each side of lateral stamen.</text>
      <biological_entity id="o39443" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity constraint="nectar" id="o39444" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character name="atypical_quantity" src="d0_s16" value="4" value_original="4" />
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character constraint="on side" constraintid="o39445" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o39445" name="side" name_original="side" src="d0_s16" type="structure" />
      <biological_entity constraint="lateral" id="o39446" name="stamen" name_original="stamen" src="d0_s16" type="structure" />
      <relation from="o39445" id="r2668" name="part_of" negation="false" src="d0_s16" to="o39446" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits siliques or silicles, sessile, linear, oblong, cylindrical, oval-elliptic, ovoid, lanceoloid, lanceoloid-subulate, or globose, smooth or torulose, terete or slightly latiseptate;</text>
      <biological_entity id="o39447" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s17" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s17" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oval-elliptic" value_original="oval-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceoloid" value_original="lanceoloid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceoloid-subulate" value_original="lanceoloid-subulate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceoloid-subulate" value_original="lanceoloid-subulate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s17" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
      <biological_entity id="o39448" name="silique" name_original="siliques" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s17" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s17" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oval-elliptic" value_original="oval-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceoloid" value_original="lanceoloid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceoloid-subulate" value_original="lanceoloid-subulate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceoloid-subulate" value_original="lanceoloid-subulate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s17" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
      <biological_entity id="o39449" name="silicle" name_original="silicles" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s17" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s17" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oval-elliptic" value_original="oval-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceoloid" value_original="lanceoloid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceoloid-subulate" value_original="lanceoloid-subulate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="lanceoloid-subulate" value_original="lanceoloid-subulate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s17" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each often with prominent midvein, glabrous or pubescent;</text>
      <biological_entity id="o39450" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o39451" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o39450" id="r2669" modifier="often" name="with" negation="false" src="d0_s18" to="o39451" />
    </statement>
    <statement id="d0_s19">
      <text>replum rounded;</text>
      <biological_entity id="o39452" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum complete, (membranous, translucent);</text>
      <biological_entity id="o39453" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules (5–) 14–44 per ovary;</text>
      <biological_entity id="o39454" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" from="5" name="atypical_quantity" src="d0_s21" to="14" to_inclusive="false" />
        <character char_type="range_value" constraint="per ovary" constraintid="o39455" from="14" name="quantity" src="d0_s21" to="44" />
      </biological_entity>
      <biological_entity id="o39455" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>stigma capitate, entire or slightly 2-lobed.</text>
      <biological_entity id="o39456" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s22" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s22" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds plump, not winged, oblong or ovoid;</text>
      <biological_entity id="o39457" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="size" src="d0_s23" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s23" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s23" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s23" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>seed-coat (minutely reticulate), not mucilaginous when wetted;</text>
      <biological_entity id="o39458" name="seed-coat" name_original="seed-coat" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s24" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>cotyledons incumbent.</text>
    </statement>
    <statement id="d0_s26">
      <text>x = 7.</text>
      <biological_entity id="o39459" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s25" value="incumbent" value_original="incumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o39460" name="chromosome" name_original="" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, n Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="n Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>56.</number>
  <discussion>Species 17 (7 in the flora).</discussion>
  <references>
    <reference>Abbe, E. C. 1948. Braya in boreal eastern America. Rhodora 50: 1–15.</reference>
    <reference>Harris, J. G. 1985. A Revision of the Genus Braya (Cruciferae) in North America. Ph.D. thesis. University of Alberta.</reference>
    <reference>Parsons, K. A. 2002. Reproductive Biology and Floral Variation in the Endangered Braya longii and Threatened B. fernaldii (Brassicaceae): Implications for Conservation Management of Rare Plants. M.S. thesis. Memorial University of Newfoundland.</reference>
    <reference>Warwick, S. I. et al. 2004. Phylogeny of Braya and Neotorularia (Brassicaceae) based on nuclear ribosomal internal transcribed spacer and chloroplast trnL intron sequences. Canad. J. Bot. 82: 376–392.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants not scapose; cauline leaves (1 or) 2-4; fruits linear; seeds usually uniseriate, rarely weakly biseriate</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants scapose; cauline leaves 0 or 1 (or with leaflike bract subtending proximalmost pedicel); fruits ovoid, globose, oval-elliptic, oblong, oblong-elliptic, oblong-lanceoloid, or lanceoloid-subulate, not linear; seeds usually biseriate (uniseriate in B. fernaldii and, sometimes, B. longii)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaves 3 or more; basal leaves (0.3-)0.5-2(-3.5) cm × 1-8(-10) mm; racemes elongated in fruit; fruits (0.9-)1.2-2.5(-3.2) cm</description>
      <determination>3 Braya humilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaves 1-4; basal leaves 0.5-3 cm × 0.5-2(-3) mm; racemes not elongated in fruit; fruits (0.5-)0.9-1.2(-1.4) cm.</description>
      <determination>4 Braya linearis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruits ovoid or globose</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Fruits oval-elliptic, oblong, oblong-elliptic, oblong-lanceoloid, or lanceoloid-subulate</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals 4.7-6.6 × 3-5.1 mm; styles 1.2-2(-2.5) mm; stems erect to ascending.</description>
      <determination>6 Braya pilosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals 2-3.7 × 1-1.5 mm; styles obsolete to 0.7(-1) mm; stems usually decumbent to prostrate, sometimes ascending.</description>
      <determination>7 Braya thorild-wulffii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Fruits oval-elliptic, oblong-cylindrical, or lanceoloid; septum margins not expanded basally (not forming sacklike pouch around proximalmost seeds); seeds biseriate.</description>
      <determination>2 Braya glabella</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Fruits lanceoloid-subulate; septum margins broadly expanded basally (forming sacklike pouch around proximalmost seeds); seeds somewhat to nearly uniseriate</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Fruit valves pubescent; petals 2.4-3.8(-4) × (0.8-)1-1.3(-2) mm, (claws often not well- differentiated from blades).</description>
      <determination>1 Braya fernaldii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Fruit valves glabrous or sparsely pubescent; petals (3-)3.3-4.8(-5) × (1.2-)1.4-2.5(-3) mm, (claws usually well-differentiated from blades).</description>
      <determination>5 Braya longii</determination>
    </key_statement>
  </key>
</bio:treatment>