<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="mention_page">358</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="treatment_page">398</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) Windham" date="2007" rank="species">porphyrea</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>11: 272. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species porphyrea</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094508</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="unknown" rank="species">porphyrea</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>16: 123. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arabis;species porphyrea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>apomictic;</text>
      <biological_entity id="o769" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="apomictic" value_original="apomictic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>caudex often woody.</text>
      <biological_entity id="o770" name="caudex" name_original="caudex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="texture" src="d0_s3" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems 1 to several per caudex branch, arising from center of rosette or arising laterally proximal to sterile shoots, somewhat elevated on woody base or from ground surface, 2–5 (–9) dm, usually glabrous proximally, rarely sparsely pubescent, trichomes simple and short-stalked, 2-rayed or 3-rayed, 0.1–0.5 mm, glabrous distally.</text>
      <biological_entity id="o771" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="per caudex branch" constraintid="o772" is_modifier="false" name="quantity" src="d0_s4" value="several" value_original="several" />
        <character constraint="from center" constraintid="o773" is_modifier="false" name="orientation" notes="" src="d0_s4" value="arising" value_original="arising" />
        <character constraint="on " constraintid="o777, o778" is_modifier="false" modifier="somewhat" name="prominence" notes="" src="d0_s4" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s4" to="9" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="5" to_unit="dm" />
        <character is_modifier="false" modifier="usually; proximally" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o772" name="branch" name_original="branch" src="d0_s4" type="structure" />
      <biological_entity id="o773" name="center" name_original="center" src="d0_s4" type="structure" />
      <biological_entity id="o774" name="rosette" name_original="rosette" src="d0_s4" type="structure" />
      <biological_entity id="o775" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="sterile" value_original="sterile" />
        <character is_modifier="true" name="orientation" src="d0_s4" value="arising" value_original="arising" />
        <character is_modifier="true" modifier="laterally" name="position" src="d0_s4" value="proximal" value_original="proximal" />
      </biological_entity>
      <biological_entity id="o776" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o777" name="ground" name_original="ground" src="d0_s4" type="structure" />
      <biological_entity id="o778" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o779" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o773" id="r57" name="part_of" negation="false" src="d0_s4" to="o774" />
      <relation from="o773" id="r58" name="part_of" negation="false" src="d0_s4" to="o775" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade oblanceolate, (3–) 5–15 mm wide, margins dentate, ciliate at least along petiole, trichomes to 1 mm, surfaces pubescent, trichomes short-stalked, 2–6-rayed, 0.1–0.5 mm.</text>
      <biological_entity constraint="basal" id="o780" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o781" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o782" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character constraint="along petiole" constraintid="o783" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o783" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
      <biological_entity id="o784" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o785" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o786" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: 5–17 (–21), sometimes concealing stem proximally;</text>
      <biological_entity constraint="cauline" id="o787" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="17" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="21" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="17" />
      </biological_entity>
      <biological_entity id="o788" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o787" id="r59" modifier="proximally" name="sometimes concealing" negation="false" src="d0_s6" to="o788" />
    </statement>
    <statement id="d0_s7">
      <text>blade auricles 0.5–3.5 (–5.5) mm, surfaces of distalmost leaves glabrous.</text>
      <biological_entity constraint="cauline" id="o789" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o790" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o791" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o792" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o791" id="r60" name="part_of" negation="false" src="d0_s7" to="o792" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes 10–30 (–70) -flowered, usually unbranched.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels horizontal, gently curved downward, 10–27 mm, glabrous.</text>
      <biological_entity id="o793" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="10-30(-70)-flowered" value_original="10-30(-70)-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" modifier="gently" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="downward" value_original="downward" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="27" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o794" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o793" id="r61" name="fruiting" negation="false" src="d0_s9" to="o794" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers ascending at anthesis;</text>
      <biological_entity id="o795" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals glabrous;</text>
      <biological_entity id="o796" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals lavender, 6.5–8.5 × 1–2.5 mm, glabrous;</text>
      <biological_entity id="o797" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_odor" src="d0_s12" value="lavender" value_original="lavender" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="length" src="d0_s12" to="8.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollen spheroid.</text>
      <biological_entity id="o798" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="spheroid" value_original="spheroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits widely pendent, not appressed to rachis, not secund, usually curved, edges parallel, 3–7 cm × 1.8–2.5 mm;</text>
      <biological_entity id="o799" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s14" value="pendent" value_original="pendent" />
        <character constraint="to rachis" constraintid="o800" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s14" value="secund" value_original="secund" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s14" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o800" name="rachis" name_original="rachis" src="d0_s14" type="structure" />
      <biological_entity id="o801" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s14" to="7" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves glabrous;</text>
      <biological_entity id="o802" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 100–160 per ovary;</text>
      <biological_entity id="o803" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o804" from="100" name="quantity" src="d0_s16" to="160" />
      </biological_entity>
      <biological_entity id="o804" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.2–0.5 mm.</text>
      <biological_entity id="o805" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds sub-biseriate, 1.1–1.6 × 0.9–1.1 mm;</text>
      <biological_entity id="o806" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s18" value="sub-biseriate" value_original="sub-biseriate" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s18" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s18" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>wing continuous, 0.1–0.2 mm wide.</text>
      <biological_entity id="o807" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="continuous" value_original="continuous" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s19" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Mar-early May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early May" from="late Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes in evergreen woodlands or desert scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" constraint="in evergreen woodlands or desert scrub" />
        <character name="habitat" value="evergreen woodlands" />
        <character name="habitat" value="desert scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>77.</number>
  <discussion>Morphological evidence suggests that Boechera porphyrea is an apomictic species that arose through hybridization between B. texana and B. perennans (see M. D. Windham and I. A. Al-Shehbaz 2007 for detailed comparison).</discussion>
  
</bio:treatment>