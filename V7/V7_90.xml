<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">61</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">87</other_info_on_meta>
    <other_info_on_meta type="treatment_page">85</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="(Borrer) Andersson in A. P. de Candolle and A. L. P. P. de Candolle" date="1868" rank="section">myrtilloides</taxon_name>
    <taxon_name authority="Fernald" date="1905" rank="species">chlorolepis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>7: 186. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section myrtilloides;species chlorolepis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242445687</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.15–0.2 m, not clonal.</text>
      <biological_entity id="o18088" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.15" from_unit="m" name="some_measurement" src="d0_s0" to="0.2" to_unit="m" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="clonal" value_original="clonal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect;</text>
      <biological_entity id="o18089" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches redbrown, (weakly glaucous), glabrous;</text>
      <biological_entity id="o18090" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets yellowbrown, glabrous.</text>
      <biological_entity id="o18091" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules absent or rudimentary;</text>
      <biological_entity id="o18092" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o18093" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole (deeply to shallowly grooved adaxially), 1–3.5 mm;</text>
      <biological_entity id="o18094" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18095" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>largest medial blade elliptic, oblanceolate, or obovate, 14–33 × 7–12 mm, 1.9–3.4 times as long as wide, base cuneate or convex, margins flat or slightly revolute, entire, ciliate, apex acute, convex, or rounded, abaxial surface glabrous, adaxial slightly glossy, glabrous;</text>
      <biological_entity id="o18096" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="largest medial" id="o18097" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="14" from_unit="mm" name="length" src="d0_s6" to="33" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1.9-3.4" value_original="1.9-3.4" />
      </biological_entity>
      <biological_entity id="o18098" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o18099" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o18100" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18101" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18102" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal blade margins entire;</text>
      <biological_entity id="o18103" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o18104" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>juvenile blade (sometimes reddish), glabrous, ciliate.</text>
      <biological_entity id="o18105" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o18106" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="juvenile" value_original="juvenile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Catkins: staminate 6.5 × 5–8 mm, flowering branchlet 1–3 mm;</text>
      <biological_entity id="o18107" name="catkin" name_original="catkins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character name="length" src="d0_s9" unit="mm" value="6.5" value_original="6.5" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18108" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate densely flowered, stout or subglobose, 7–12 × 3–6 mm, flowering branchlet 1.8–12 mm;</text>
      <biological_entity id="o18109" name="catkin" name_original="catkins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s10" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s10" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18110" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract tawny, brown, or greenish, 1–2.6 mm, apex broadly rounded to retuse, entire, abaxially glabrous.</text>
      <biological_entity id="o18111" name="catkin" name_original="catkins" src="d0_s11" type="structure" />
      <biological_entity constraint="floral" id="o18112" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18113" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="broadly rounded" name="shape" src="d0_s11" to="retuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: abaxial nectary 0.3–0.4 mm, adaxial nectary oblong, 0.5–0.8 mm, nectaries distinct;</text>
      <biological_entity id="o18114" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18115" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18116" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18117" name="nectary" name_original="nectaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, glabrous;</text>
      <biological_entity id="o18118" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o18119" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ellipsoid, 0.4–0.6 mm.</text>
      <biological_entity id="o18120" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o18121" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: adaxial nectary narrowly oblong, 0.9–1.6 mm, longer than stipe, nectaries distinct or connate and shallowly cupshaped;</text>
      <biological_entity id="o18122" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o18123" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s15" to="1.6" to_unit="mm" />
        <character constraint="than stipe" constraintid="o18124" is_modifier="false" name="length_or_size" src="d0_s15" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o18124" name="stipe" name_original="stipe" src="d0_s15" type="structure" />
      <biological_entity id="o18125" name="nectary" name_original="nectaries" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s15" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stipe 0–0.4 mm;</text>
      <biological_entity id="o18126" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18127" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary pyriform, glabrous, beak abruptly tapering to or slightly bulged below styles;</text>
      <biological_entity id="o18128" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18129" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o18130" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="abruptly; slightly" name="shape" src="d0_s17" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o18131" name="style" name_original="styles" src="d0_s17" type="structure" />
      <relation from="o18130" id="r1252" modifier="below" name="bulged" negation="false" src="d0_s17" to="o18131" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 8–10 per ovary;</text>
      <biological_entity id="o18132" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18133" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o18134" from="8" name="quantity" src="d0_s18" to="10" />
      </biological_entity>
      <biological_entity id="o18134" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles connate to distinct 1/2 their lengths, 0.5–1.3 mm;</text>
      <biological_entity id="o18135" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18136" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="connate" name="fusion" src="d0_s19" to="distinct" />
        <character name="lengths" src="d0_s19" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s19" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas flat, abaxially non-papillate with rounded tip, or slenderly cylindrical, 0.3–0.6 mm.</text>
      <biological_entity id="o18137" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o18138" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o18139" is_modifier="false" modifier="abaxially" name="relief" src="d0_s20" value="non-papillate" value_original="non-papillate" />
        <character is_modifier="false" modifier="slenderly" name="shape" notes="" src="d0_s20" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s20" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18139" name="tip" name_original="tip" src="d0_s20" type="structure">
        <character is_modifier="true" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 3.6–6 mm. 2n = 38.</text>
      <biological_entity id="o18140" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="3.6" from_unit="mm" name="some_measurement" src="d0_s21" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18141" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul-early Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet Sphagnum bog on alpine, serpentine barrens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet sphagnum bog" constraint="on alpine , serpentine barrens" />
        <character name="habitat" value="alpine" />
        <character name="habitat" value="serpentine barrens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Que.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>46.</number>
  <other_name type="common_name">Green-bract willow</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Salix chlorolepis, known from Mt. Albert, is characterized by its general glabrousness. It seems to have a relationship with S. brachycarpa similar to that of S. raupii to S. glauca. Both may have originated through mutation or hybridization.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Salix chlorolepis forms natural hybrids with S. brachycarpa var. brachycarpa.</discussion>
  <discussion>Salix chlorolepis × S. pedicellaris is a putative hybrid that has relatively small, glabrous leaves. Both parents occur together on Mt. Albert, Quebec.</discussion>
  
</bio:treatment>