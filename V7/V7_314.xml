<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="treatment_page">222</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="unknown" rank="genus">GYNANDROPSIS</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>1: 237. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus GYNANDROPSIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Gynandra (Orchidaceae), and Greek opsis, resemblance</other_info_on_name>
    <other_info_on_name type="fna_id">114333</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual [perennial].</text>
      <biological_entity id="o36226" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched or sparsely branched;</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrate or glandular-pubescent.</text>
      <biological_entity id="o36227" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules absent;</text>
      <biological_entity id="o36228" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o36229" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole with pulvinus basally or distally, (petiolule base adnate, forming pulvinar disc);</text>
      <biological_entity id="o36230" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o36231" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o36232" name="pulvinu" name_original="pulvinus" src="d0_s4" type="structure" />
      <relation from="o36231" id="r2436" modifier="distally" name="with" negation="false" src="d0_s4" to="o36232" />
    </statement>
    <statement id="d0_s5">
      <text>leaflets 3 or 5.</text>
      <biological_entity id="o36233" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o36234" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s5" unit="or" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, racemes (elongated);</text>
      <biological_entity id="o36235" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o36236" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o36237" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers zygomorphic;</text>
      <biological_entity id="o36238" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="zygomorphic" value_original="zygomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals persistent, distinct, equal (each often subtending a nectary);</text>
      <biological_entity id="o36239" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals equal;</text>
      <biological_entity id="o36240" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 6;</text>
      <biological_entity id="o36241" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments adnate basally to gynophore (about as long as petals), glabrous;</text>
      <biological_entity id="o36242" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character constraint="to gynophore" constraintid="o36243" is_modifier="false" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o36243" name="gynophore" name_original="gynophore" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers coiling as pollen released;</text>
      <biological_entity id="o36244" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character constraint="as pollen" constraintid="o36245" is_modifier="false" name="shape" src="d0_s13" value="coiling" value_original="coiling" />
      </biological_entity>
      <biological_entity id="o36245" name="pollen" name_original="pollen" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>gynophore recurved in fruit (filament scars visible ca. 1/3–1/2 its length).</text>
      <biological_entity id="o36246" name="gynophore" name_original="gynophore" src="d0_s14" type="structure">
        <character constraint="in fruit" constraintid="o36247" is_modifier="false" name="orientation" src="d0_s14" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o36247" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsules, dehiscent, oblong.</text>
      <biological_entity constraint="fruits" id="o36248" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 10–20 [–40], subglobose, not arillate, (cleft fused between ends).</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 10, 17.</text>
      <biological_entity id="o36249" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="40" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s16" to="20" />
        <character is_modifier="false" name="shape" src="d0_s16" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity constraint="x" id="o36250" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="10" value_original="10" />
        <character name="quantity" src="d0_s17" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; sw Asia; tropical and warm temperate climates.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw Asia" establishment_means="introduced" />
        <character name="distribution" value="tropical and warm temperate climates" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Spider-wisp</other_name>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>Gynandropsis is allied to Cleome; it is distinguished by relatively long androgynophores. It has been included in Cleome; most regional accounts of Cleomaceae (including Capparaceae) in the Old World have given this taxon generic status, an approach followed here.</discussion>
  
</bio:treatment>