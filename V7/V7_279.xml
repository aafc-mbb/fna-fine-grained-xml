<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="treatment_page">208</other_info_on_meta>
    <other_info_on_meta type="illustration_page">202</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1824" rank="genus">peritoma</taxon_name>
    <taxon_name authority="(Torrey) H. H. Iltis" date="2007" rank="species">platycarpa</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>17: 449. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus peritoma;species platycarpa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094648</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cleome</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">platycarpa</taxon_name>
    <place_of_publication>
      <publication_title>in C. Wilkes et al., U.S. Expl. Exped.</publication_title>
      <place_in_publication>17: 235, plate 2. 1874</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cleome;species platycarpa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Celome</taxon_name>
    <taxon_name authority="(Torrey) Greene" date="unknown" rank="species">platycarpa</taxon_name>
    <taxon_hierarchy>genus Celome;species platycarpa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, 10–60 cm.</text>
      <biological_entity id="o8955" name="whole-organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="60" to_unit="cm" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems densely branched basally;</text>
    </statement>
    <statement id="d0_s2">
      <text>purple-tinged;</text>
    </statement>
    <statement id="d0_s3">
      <text>glandular-pubescent.</text>
      <biological_entity id="o8956" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely; basally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: (stipules scalelike, minute, obscured by pubescence);</text>
      <biological_entity id="o8957" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 1.5–4.5 cm;</text>
      <biological_entity id="o8958" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o8959" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="4.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leaflets 3, blade flat, ovate to obovate, 1–3.5 × 0.5–1.3 cm, margins serrulate-denticulate or entire, apex obtuse, surfaces glandular-pubescent.</text>
      <biological_entity id="o8960" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o8961" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o8962" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s6" to="1.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8963" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="serrulate-denticulate" value_original="serrulate-denticulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8964" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o8965" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes 1–3.5 cm (5–40 cm in fruit);</text>
      <biological_entity id="o8966" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts unifoliate, obovate to spatulate, 3–25 mm.</text>
      <biological_entity id="o8967" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="spatulate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 7–17 mm.</text>
      <biological_entity id="o8968" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals deciduous, distinct, yellow, awl-shaped, (3–) 4–5 (–6) × 3–7 mm, margins entire, densely glandular-hairy;</text>
      <biological_entity id="o8969" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8970" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="awl--shaped" value_original="awl--shaped" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8971" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="glandular-hairy" value_original="glandular-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals golden yellow, oblong, 6–12 × 0.3–0.4 (–0.6) mm;</text>
      <biological_entity id="o8972" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8973" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="golden yellow" value_original="golden yellow" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s11" to="12" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens yellow, 10–17 mm;</text>
      <biological_entity id="o8974" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8975" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 1.8–2 mm;</text>
      <biological_entity id="o8976" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o8977" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>gynophore 10–18 mm in fruit;</text>
      <biological_entity id="o8978" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o8979" name="gynophore" name_original="gynophore" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o8980" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8980" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>ovary (compressed), 3.5–5 mm;</text>
      <biological_entity id="o8981" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" notes="" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8982" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style 1–3 mm.</text>
      <biological_entity id="o8983" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o8984" name="style" name_original="style" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Capsules (pendent) not inflated, 12–25 × 8–12 mm, striate, (glandular-pubescent).</text>
      <biological_entity id="o8985" name="capsule" name_original="capsules" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s17" to="25" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s17" to="12" to_unit="mm" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s17" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds 10–20, brownish black, spheroidal, 3–3.2 mm, (glossy) smooth.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 40.</text>
      <biological_entity id="o8986" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s18" to="20" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brownish black" value_original="brownish black" />
        <character is_modifier="false" name="shape" src="d0_s18" value="spheroidal" value_original="spheroidal" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s18" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8987" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline clay soils, volcanic tuff, dry foothills, among junipers, in sagebrush scrub, fields, railroads</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline clay soils" />
        <character name="habitat" value="volcanic tuff" />
        <character name="habitat" value="dry foothills" constraint="among junipers" />
        <character name="habitat" value="junipers" />
        <character name="habitat" value="sagebrush scrub" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="railroads" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Golden bee-plant</other_name>
  <discussion>Peritoma platycarpa resembles some species of Cleomella in the absence of a nectary-disc, shape of the replum, and indument.</discussion>
  
</bio:treatment>