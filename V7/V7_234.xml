<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="treatment_page">184</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Engler" date="unknown" rank="family">koeberliniaceae</taxon_name>
    <taxon_name authority="Zuccarini" date="1832" rank="genus">KOEBERLINIA</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>15(2, Beibl.): 73. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family koeberliniaceae;genus KOEBERLINIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Christoph Ludwig Koeberlin, 1794–1862, German clergyman and botanist</other_info_on_name>
    <other_info_on_name type="fna_id">117186</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants unscented.</text>
      <biological_entity id="o4148" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="odor" src="d0_s0" value="unscented" value_original="unscented" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves evanescent.</text>
      <biological_entity id="o4149" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="evanescent" value_original="evanescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers: sepals each often subtending a nectary;</text>
      <biological_entity id="o4150" name="flower" name_original="flowers" src="d0_s2" type="structure" />
      <biological_entity id="o4151" name="sepal" name_original="sepals" src="d0_s2" type="structure" />
      <biological_entity id="o4152" name="nectary" name_original="nectary" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="often" name="position" src="d0_s2" value="subtending" value_original="subtending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>filaments thickened in middle;</text>
      <biological_entity id="o4153" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o4154" name="filament" name_original="filaments" src="d0_s3" type="structure">
        <character constraint="in middle flowers" constraintid="o4155" is_modifier="false" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity constraint="middle" id="o4155" name="flower" name_original="flowers" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>anthers ellipsoid;</text>
      <biological_entity id="o4156" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o4157" name="anther" name_original="anthers" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>gynophore slender, elongating in fruit;</text>
      <biological_entity id="o4158" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o4159" name="gynophore" name_original="gynophore" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="slender" value_original="slender" />
        <character constraint="in fruit" constraintid="o4160" is_modifier="false" name="length" src="d0_s5" value="elongating" value_original="elongating" />
      </biological_entity>
      <biological_entity id="o4160" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>style straight;</text>
      <biological_entity id="o4161" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o4162" name="style" name_original="style" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stigma minute.</text>
      <biological_entity id="o4163" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4164" name="stigma" name_original="stigma" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Berries globose, ± fleshy.</text>
      <biological_entity id="o4165" name="berry" name_original="berries" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s8" value="fleshy" value_original="fleshy" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds cochleate-reniform or orbicular to reniform, slightly rugulose.</text>
      <biological_entity id="o4166" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="orbicular" name="shape" src="d0_s9" to="reniform" />
        <character is_modifier="false" modifier="slightly" name="relief" src="d0_s9" value="rugulose" value_original="rugulose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, n Mexico, South America (Bolivia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
        <character name="distribution" value="South America (Bolivia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Allthorn</other_name>
  <other_name type="common_name">junco</other_name>
  <other_name type="common_name">c rown-of-thorns</other_name>
  <other_name type="common_name">corona-de-Cristo</other_name>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>Koeberlinia holacantha W. C. Holmes, K. L. Yip &amp; Rushing is known only from Departamento Santa Cruz, Bolivia. The Seri tribe of coastal Sonora, Mexico, traditionally fumigate their huts with the black, oily smoke from burning wood of Koeberlinia (R. S. Felger and M. B. Moser 1985).</discussion>
  
</bio:treatment>