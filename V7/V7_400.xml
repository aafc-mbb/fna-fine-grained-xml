<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="treatment_page">305</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="(C. L. Hitchcock) Rollins" date="1984" rank="species">daviesiae</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>214: 5. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species daviesiae</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094686</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="C. L. Hitchcock" date="unknown" rank="species">apiculata</taxon_name>
    <taxon_name authority="C. L. Hitchcock" date="unknown" rank="variety">daviesiae</taxon_name>
    <place_of_publication>
      <publication_title>in C. L. Hitchcock et al., Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>2: 489, fig. p. 493 [upper right]. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Draba;species apiculata;variety daviesiae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">densifolia</taxon_name>
    <taxon_name authority="(C. L. Hitchcock) S. L. Welsh &amp; Reveal" date="unknown" rank="variety">daviesiae</taxon_name>
    <taxon_hierarchy>genus Draba;species densifolia;variety daviesiae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(densely pulvinate);</text>
      <biological_entity id="o12369" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (branches elongated, loose, with persistent leaf remains, terminating in flowering or sterile shoots);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o12370" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, (0.05–) 0.2–0.6 dm, glabrous.</text>
      <biological_entity id="o12371" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.05" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="some_measurement" src="d0_s4" to="0.6" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves (densely imbricate);</text>
    </statement>
    <statement id="d0_s6">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o12372" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petiole ciliate throughout;</text>
      <biological_entity id="o12373" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="throughout" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade (fleshy), oblong to obovate or oblanceolate, 0.3–0.7 (–1) cm × 1–2 (–2.5) mm, margins entire, (ciliate, trichomes simple, 0.1–0.5 mm, apex obtuse), surfaces glabrous (midvein obscure abaxially).</text>
      <biological_entity id="o12374" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="obovate or oblanceolate" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s9" to="0.7" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12375" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o12376" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cauline leaves 0.</text>
      <biological_entity constraint="cauline" id="o12377" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Racemes 2–8 (–10) -flowered, ebracteate, (subcorymbose), slightly elongated in fruit;</text>
      <biological_entity id="o12378" name="raceme" name_original="racemes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-8(-10)-flowered" value_original="2-8(-10)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o12379" is_modifier="false" modifier="slightly" name="length" src="d0_s11" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o12379" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>rachis not flexuous, glabrous.</text>
    </statement>
    <statement id="d0_s13">
      <text>Fruiting pedicels divaricate-ascending (not decurrent basally), straight, 4–10 mm, glabrous.</text>
      <biological_entity id="o12380" name="rachis" name_original="rachis" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s12" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12381" name="pedicel" name_original="pedicels" src="d0_s13" type="structure" />
      <relation from="o12380" id="r848" name="fruiting" negation="false" src="d0_s13" to="o12381" />
    </statement>
    <statement id="d0_s14">
      <text>Flowers: sepals oblong, 1.5–2.2 mm, glabrous;</text>
      <biological_entity id="o12382" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o12383" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals pale to bright-yellow, spatulate, 3.5–4 × 1–2 mm;</text>
      <biological_entity id="o12384" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o12385" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s15" to="bright-yellow" />
        <character is_modifier="false" name="shape" src="d0_s15" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s15" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers ovate, 0.3–0.4 mm.</text>
      <biological_entity id="o12386" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o12387" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits ovate to oblongelliptic, plane, flattened, 4–8 × 2–4 mm;</text>
      <biological_entity id="o12388" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s17" to="oblongelliptic" />
        <character is_modifier="false" name="shape" src="d0_s17" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s17" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s17" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves (obscurely veined), glabrous;</text>
      <biological_entity id="o12389" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 6–14 per ovary;</text>
      <biological_entity id="o12390" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o12391" from="6" name="quantity" src="d0_s19" to="14" />
      </biological_entity>
      <biological_entity id="o12391" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style 0.1–0.5 mm.</text>
      <biological_entity id="o12392" name="style" name_original="style" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s20" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds ovoid, 1.2–1.5 × 0.8–1 mm.</text>
      <biological_entity id="o12393" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s21" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s21" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Talus slopes, rock crevices and cracks, rocky ridges and slides, alpine meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="cracks" />
        <character name="habitat" value="alpine meadows" modifier="slides" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2700-2900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2900" to_unit="m" from="2700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34.</number>
  <discussion>Although originally described as a variety of Draba apiculata (= D. globosa), D. daviesiae is distinct morphologically. It is easily distinguished from the former by its densely pulvinate habit, obtuse leaf blades, and obscurely veined fruit valves. By contrast, D. globosa exhibits a cespitose but non-pulvinate habit, acute leaf blades, and prominently veined fruit valves. Draba daviesiae is known from the Bitterroot Mountains in Ravalli County.</discussion>
  
</bio:treatment>