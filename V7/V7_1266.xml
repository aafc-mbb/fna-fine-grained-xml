<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">724</other_info_on_meta>
    <other_info_on_meta type="treatment_page">726</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Rydberg" date="1907" rank="genus">thelypodiopsis</taxon_name>
    <taxon_name authority="(Brandegee) Rollins" date="1976" rank="species">purpusii</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>206: 14. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus thelypodiopsis;species purpusii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094931</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelypodium</taxon_name>
    <taxon_name authority="Brandegee" date="unknown" rank="species">purpusii</taxon_name>
    <place_of_publication>
      <publication_title>Zoë</publication_title>
      <place_in_publication>5: 232. 1906 (as purpusi)</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Thelypodium;species purpusii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisymbrium</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="species">kearneyi</taxon_name>
    <taxon_hierarchy>genus Sisymbrium;species kearneyi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisymbrium</taxon_name>
    <taxon_name authority="(Brandegee) O. E. Schulz" date="unknown" rank="species">purpusii</taxon_name>
    <taxon_hierarchy>genus Sisymbrium;species purpusii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisymbrium</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) O. E. Schulz" date="unknown" rank="species">vernale</taxon_name>
    <taxon_hierarchy>genus Sisymbrium;species vernale;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelypodium</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="unknown" rank="species">vernale</taxon_name>
    <taxon_hierarchy>genus Thelypodium;species vernale;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>(often glaucous), glabrous throughout.</text>
      <biological_entity id="o37446" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unbranched or branched distally, 1.5–5 (–7) dm.</text>
      <biological_entity id="o37447" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="7" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s2" to="5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (soon withered);</text>
    </statement>
    <statement id="d0_s4">
      <text>not rosulate;</text>
      <biological_entity constraint="basal" id="o37448" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5–3 cm;</text>
      <biological_entity id="o37449" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblanceolate, 1.5–9 cm × 5–30 mm, margins pinnatifid to dentate-sinuate.</text>
      <biological_entity id="o37450" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="9" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37451" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s6" to="dentate-sinuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves (proximalmost) petiolate or (distal) sessile;</text>
      <biological_entity constraint="cauline" id="o37452" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade (proximalmost) oblanceolate, (distal) ovate to oblong, base subamplexicaul or auriculate, margins (proximalmost) pinnatifid to dentate-sinuate, or (distal) entire.</text>
      <biological_entity id="o37453" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s8" to="oblong" />
      </biological_entity>
      <biological_entity id="o37454" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="subamplexicaul" value_original="subamplexicaul" />
        <character is_modifier="false" name="shape" src="d0_s8" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o37455" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s8" to="dentate-sinuate or entire" />
        <character char_type="range_value" from="pinnatifid" name="shape" src="d0_s8" to="dentate-sinuate or entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes lax.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels ascending to divaricate, straight, 3–7 (–10) mm.</text>
      <biological_entity id="o37456" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37457" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o37456" id="r2517" name="fruiting" negation="false" src="d0_s10" to="o37457" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals erect, green or purplish, 3–4.5 × 0.7–1.2 mm;</text>
      <biological_entity id="o37458" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o37459" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals white, oblanceolate, 4–5.5 × 1–1.5 mm, claw 2–3 mm;</text>
      <biological_entity id="o37460" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o37461" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o37462" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>median filament pairs 3–4 mm;</text>
      <biological_entity id="o37463" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="median" id="o37464" name="filament" name_original="filament" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ovate, 0.5–0.8 mm;</text>
      <biological_entity id="o37465" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o37466" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>gynophore 0.2–0.4 mm.</text>
      <biological_entity id="o37467" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o37468" name="gynophore" name_original="gynophore" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s15" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits divaricate to ascending or spreading, straight or curved, torulose, 3–6.5 cm × 1–1.2 mm;</text>
      <biological_entity id="o37469" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s16" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s16" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s16" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s16" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 46–114 per ovary;</text>
      <biological_entity id="o37470" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o37471" from="46" name="quantity" src="d0_s17" to="114" />
      </biological_entity>
      <biological_entity id="o37471" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style subclavate, 0.7–1.5 mm;</text>
      <biological_entity id="o37472" name="style" name_original="style" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="subclavate" value_original="subclavate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigma entire or obscurely 2-lobed.</text>
      <biological_entity id="o37473" name="stigma" name_original="stigma" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s19" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 0.9–1.2 × 0.5–0.7 mm.</text>
      <biological_entity id="o37474" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s20" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s20" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Juniper woodlands, rocky slopes, shale grounds, loose gypsum, barren areas, clay banks of rocky hillsides, shrub communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="juniper woodlands" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="shale grounds" />
        <character name="habitat" value="loose gypsum" />
        <character name="habitat" value="barren areas" />
        <character name="habitat" value="clay banks" constraint="of rocky hillsides" />
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="shrub communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  
</bio:treatment>