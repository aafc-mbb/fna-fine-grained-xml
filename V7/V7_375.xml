<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">270</other_info_on_meta>
    <other_info_on_meta type="mention_page">276</other_info_on_meta>
    <other_info_on_meta type="mention_page">278</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="treatment_page">294</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Payson" date="1917" rank="species">asterophora</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>4: 263. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species asterophora</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094714</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">asterophora</taxon_name>
    <taxon_name authority="C. L. Hitchcock" date="unknown" rank="variety">macrocarpa</taxon_name>
    <taxon_hierarchy>genus Draba;species asterophora;variety macrocarpa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(loosely cespitose);</text>
      <biological_entity id="o19749" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (somewhat surculose, with persistent leaf-bases, branches sometimes terminating in sterile rosettes);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o19750" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, 0.3–1.1 dm, glabrous throughout or sparsely pubescent proximally, trichomes (2–) 4-rayed, 0.1–0.3 mm.</text>
      <biological_entity id="o19751" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s4" to="1.1" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; sparsely; proximally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o19752" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves rosulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o19753" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole obsolete, margin rarely ciliate proximally;</text>
      <biological_entity id="o19754" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="obsolete" value_original="obsolete" />
      </biological_entity>
      <biological_entity id="o19755" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="rarely; proximally" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade (somewhat fleshy), broadly obovate to suborbicular or spatulate, 0.4–1.4 (–1.7) cm × (2–) 3–6 (–7) mm, margins entire, surfaces pubescent, trichomes stalked, cruciform, and 2-rayed, 3-rayed, or 5-rayed, 0.2–0.6 mm.</text>
      <biological_entity id="o19756" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s8" to="suborbicular or spatulate" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s8" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s8" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19757" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o19758" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o19759" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
        <character is_modifier="false" name="shape" src="d0_s8" value="cruciform" value_original="cruciform" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 0 (or 1, as a bract).</text>
      <biological_entity constraint="cauline" id="o19760" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Racemes (5–) 8–20 (–27) -flowered, usually ebracteate, rarely proximalmost flowers bracteate, elongated in fruit;</text>
      <biological_entity id="o19761" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="(5-)8-20(-27)-flowered" value_original="(5-)8-20(-27)-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s10" value="ebracteate" value_original="ebracteate" />
      </biological_entity>
      <biological_entity id="o19762" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="rarely" name="position" src="d0_s10" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="bracteate" value_original="bracteate" />
        <character constraint="in fruit" constraintid="o19763" is_modifier="false" name="length" src="d0_s10" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o19763" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>rachis not flexuous, glabrous.</text>
    </statement>
    <statement id="d0_s12">
      <text>Fruiting pedicels horizontal to divaricate-ascending, straight or curved upward, 3–9 mm, glabrous.</text>
      <biological_entity id="o19764" name="rachis" name_original="rachis" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s11" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s12" to="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19765" name="pedicel" name_original="pedicels" src="d0_s12" type="structure" />
      <relation from="o19764" id="r1345" name="fruiting" negation="false" src="d0_s12" to="o19765" />
    </statement>
    <statement id="d0_s13">
      <text>Flowers: sepals oblong, 3–4 mm, glabrous or sparsely pubescent, (trichomes subapical, short-stalked, 2–4-rayed);</text>
      <biological_entity id="o19766" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o19767" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals bright-yellow, oblanceolate, 5–7 × 1.5–2.5 mm;</text>
      <biological_entity id="o19768" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o19769" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="bright-yellow" value_original="bright-yellow" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s14" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate, 0.5–0.6 mm.</text>
      <biological_entity id="o19770" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o19771" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits lanceolate-ovate to broadly ovate or oblong, slightly twisted or plane, strongly flattened, 5–11 (–14) × (3.5–) 4–6 mm;</text>
      <biological_entity id="o19772" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="lanceolate-ovate" name="shape" src="d0_s16" to="broadly ovate or oblong" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s16" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="shape" src="d0_s16" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s16" to="14" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s16" to="11" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_width" src="d0_s16" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves glabrous or, rarely, puberulent, trichomes simple and short-stalked, 2-rayed or 3-rayed, 0.05–0.3 mm;</text>
      <biological_entity id="o19773" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s17" value="," value_original="," />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o19774" name="trichome" name_original="trichomes" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s17" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules 12–18 per ovary;</text>
      <biological_entity id="o19775" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o19776" from="12" name="quantity" src="d0_s18" to="18" />
      </biological_entity>
      <biological_entity id="o19776" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style 0.2–1.6 (–2) mm.</text>
      <biological_entity id="o19777" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s19" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds (winged), ovate, 1.8–2.8 × 1.2–2 mm;</text>
    </statement>
    <statement id="d0_s21">
      <text>(wing 0.5–1 mm wide).</text>
    </statement>
    <statement id="d0_s22">
      <text>2n = 40.</text>
      <biological_entity id="o19778" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s20" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s20" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19779" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Granitic rock outcrops, talus, gravelly soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="granitic rock outcrops" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="gravelly soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2600-3300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3300" to_unit="m" from="2600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <discussion>C. L. Hitchcock (1941) and R. C. Rollins (1993) divided Draba asterophora into two varieties based on minor differences in style and fruit lengths. These do not appear to define genetically discrete taxa. The species apparently is restricted to El Dorado County, California, and the Carson Range in Washoe County, Nevada.</discussion>
  
</bio:treatment>