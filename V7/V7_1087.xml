<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">617</other_info_on_meta>
    <other_info_on_meta type="treatment_page">648</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="Rollins" date="1981" rank="species">lepidota</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>33: 335, figs. 1, 2. 1981</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species lepidota</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094886</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o15379" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex simple, (with deep roots, thickened);</text>
    </statement>
    <statement id="d0_s2">
      <text>densely (silvery) pubescent throughout (densely covering leaves with several appressed layers), less dense on stems, trichomes (stellate-scalelike), rays fused (webbed) in proximal 1/2 or to tips, (umbonate, nearly smooth to moderately tuberculate).</text>
      <biological_entity id="o15380" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="densely; throughout" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character constraint="on stems" constraintid="o15381" is_modifier="false" modifier="less" name="density" src="d0_s2" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o15381" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o15382" name="trichome" name_original="trichomes" src="d0_s2" type="structure" />
      <biological_entity id="o15383" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character constraint="in " constraintid="o15385" is_modifier="false" name="fusion" src="d0_s2" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o15384" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
      <biological_entity id="o15385" name="tip" name_original="tips" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems simple from base, erect or outer ones slightly decumbent toward base, (from below or in basal leaves, unbranched), (0.5–) 0.8–1.6 (–2) dm.</text>
      <biological_entity id="o15386" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character constraint="from base" constraintid="o15387" is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o15387" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o15388" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="slightly" name="growth_form_or_orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="0.8" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.8" from_unit="dm" name="some_measurement" src="d0_s3" to="1.6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves (erect, petiole long, slender);</text>
      <biological_entity constraint="basal" id="o15389" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade spatulate to broadly oblanceolate, (3–) 5–7 (–12) cm, (base gradually tapering to petiole), margins entire, (apex rounded or obtuse).</text>
      <biological_entity id="o15390" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="broadly oblanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o15391" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: blade oblanceolate, similar to basal, (base cuneate), margins entire.</text>
      <biological_entity constraint="cauline" id="o15392" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o15393" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15394" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o15395" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o15393" id="r1070" name="to" negation="false" src="d0_s6" to="o15394" />
    </statement>
    <statement id="d0_s7">
      <text>Racemes dense.</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels (divaricate-ascending, straight or slightly curved), 10–15 mm.</text>
      <biological_entity id="o15396" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15397" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o15396" id="r1071" name="fruiting" negation="false" src="d0_s8" to="o15397" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals (erect), linear to linear-oblong, somewhat boatshaped, 7–10 mm;</text>
      <biological_entity id="o15398" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s9" to="linear-oblong" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s9" value="boat-shaped" value_original="boat-shaped" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15399" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals (erect at anthesis), lingulate, 11–15 mm, (claw undifferentiated from blade).</text>
      <biological_entity id="o15400" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="lingulate" value_original="lingulate" />
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15401" name="petal" name_original="petals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruits (purplish in age), strongly didymous, semiorbicular, highly inflated, 10–18 × 14–19 mm, (papery), basal sinus usually shallow, rarely absent, apical sinus deep, narrowly V-shaped;</text>
      <biological_entity id="o15402" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture_or_arrangement" src="d0_s11" value="didymous" value_original="didymous" />
        <character is_modifier="false" name="shape" src="d0_s11" value="semiorbicular" value_original="semiorbicular" />
        <character is_modifier="false" modifier="highly" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s11" to="18" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="width" src="d0_s11" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o15403" name="sinus" name_original="sinus" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="depth" src="d0_s11" value="shallow" value_original="shallow" />
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="apical" id="o15404" name="sinus" name_original="sinus" src="d0_s11" type="structure">
        <character is_modifier="false" name="depth" src="d0_s11" value="deep" value_original="deep" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="v--shaped" value_original="v--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves retaining seeds after dehiscence, sides flat, back rounded, margins keeled, base and apex obtuse;</text>
      <biological_entity id="o15405" name="valve" name_original="valves" src="d0_s12" type="structure" />
      <biological_entity id="o15406" name="seed" name_original="seeds" src="d0_s12" type="structure" />
      <biological_entity id="o15407" name="side" name_original="sides" src="d0_s12" type="structure">
        <character is_modifier="true" name="character" src="d0_s12" value="dehiscence" value_original="dehiscence" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s12" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="back" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o15408" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="true" name="character" src="d0_s12" value="dehiscence" value_original="dehiscence" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s12" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="back" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o15409" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="true" name="character" src="d0_s12" value="dehiscence" value_original="dehiscence" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s12" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="back" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o15410" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o15405" id="r1072" name="retaining" negation="false" src="d0_s12" to="o15406" />
      <relation from="o15405" id="r1073" name="after" negation="false" src="d0_s12" to="o15407" />
      <relation from="o15405" id="r1074" name="after" negation="false" src="d0_s12" to="o15408" />
      <relation from="o15405" id="r1075" name="after" negation="false" src="d0_s12" to="o15409" />
    </statement>
    <statement id="d0_s13">
      <text>replum narrowly oblong to linear, as wide as or wider than fruit, base slightly narrowed, apex obtusely rounded;</text>
      <biological_entity id="o15411" name="replum" name_original="replum" src="d0_s13" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s13" to="linear" />
      </biological_entity>
      <biological_entity id="o15413" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="true" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o15412" name="fruit" name_original="fruit" src="d0_s13" type="structure">
        <character is_modifier="true" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o15414" name="base" name_original="base" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o15415" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o15411" id="r1076" name="as wide as" negation="false" src="d0_s13" to="o15413" />
    </statement>
    <statement id="d0_s14">
      <text>ovules 4 per ovary;</text>
      <biological_entity id="o15416" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character constraint="per ovary" constraintid="o15417" name="quantity" src="d0_s14" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o15417" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style 3–5 mm, (slender).</text>
      <biological_entity id="o15418" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds slightly flattened.</text>
      <biological_entity id="o15419" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>47.</number>
  <other_name type="common_name">Kane County twinpod</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Trichomes: rays fused nearly to tips; fruits with deep sinuses, or shallow basally, deep apically.</description>
      <determination>47a Physaria lepidota subsp. lepidota</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Trichomes: rays fused in proximal 1/2; fruits with sinuses absent or shallow basally, deep apically.</description>
      <determination>47b Physaria lepidota subsp. membranacea</determination>
    </key_statement>
  </key>
</bio:treatment>