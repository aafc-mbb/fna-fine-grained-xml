<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">703</other_info_on_meta>
    <other_info_on_meta type="treatment_page">721</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="Goodman" date="1957" rank="species">squamiformis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>58: 354. 1957</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species squamiformis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094940</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>sparsely to densely pubescent or glabrous.</text>
      <biological_entity id="o16478" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems (simple from base), unbranched or branched distally, 1.5–8.5 (–11) dm, (sparsely to densely hirsute basally, usually glabrous distally, rarely throughout).</text>
      <biological_entity id="o16479" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="11" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s2" to="8.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (withered by flowering);</text>
    </statement>
    <statement id="d0_s4">
      <text>not rosulate;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o16480" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade similar to cauline.</text>
      <biological_entity id="o16481" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <biological_entity constraint="cauline" id="o16482" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <relation from="o16481" id="r1133" name="to" negation="false" src="d0_s6" to="o16482" />
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves: blade ovate to oblong or lanceolate, 1–9 cm × 5–30 mm, (smaller distally), base amplexicaul, margins usually entire, rarely shallowly dentate, (midvein abaxially glabrous or pubescent, surfaces glabrous distally).</text>
      <biological_entity constraint="cauline" id="o16483" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o16484" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="oblong or lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="9" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16485" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="amplexicaul" value_original="amplexicaul" />
      </biological_entity>
      <biological_entity id="o16486" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely shallowly" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes ebracteate, (dense to lax).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate-ascending, (straight), 5–16 mm, (glabrous or pubescent).</text>
      <biological_entity id="o16487" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16488" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o16487" id="r1134" name="fruiting" negation="false" src="d0_s9" to="o16488" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx campanulate;</text>
      <biological_entity id="o16489" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o16490" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals purplish, 4–7 mm, not keeled, (hirsute, trichomes 0.7–2 mm, both pairs with subapical callosities, 1–2.5 mm);</text>
      <biological_entity id="o16491" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o16492" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals purple (with dark purple center), 9–17 (–20) mm, blade 5–10 × 5–7 mm, margins not crisped, claw 4–7 mm, narrower than blade;</text>
      <biological_entity id="o16493" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o16494" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s12" value="purple" value_original="purple" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="20" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16495" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="10" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16496" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o16497" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
        <character constraint="than blade" constraintid="o16498" is_modifier="false" name="width" src="d0_s12" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o16498" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o16499" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o16500" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments: median pairs (distinct), 5–6 mm, lateral pair 3–4 mm;</text>
      <biological_entity id="o16501" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="median" value_original="median" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers (all) fertile, 1.5–2.8 mm;</text>
      <biological_entity id="o16502" name="filament" name_original="filaments" src="d0_s15" type="structure" />
      <biological_entity id="o16503" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>gynophore 1–1.5 mm.</text>
      <biological_entity id="o16504" name="filament" name_original="filaments" src="d0_s16" type="structure" />
      <biological_entity id="o16505" name="gynophore" name_original="gynophore" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits ascending to suberect, smooth, straight, flattened, 0.6–1.4 cm × 2–3 mm;</text>
      <biological_entity id="o16506" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s17" to="suberect" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="length" src="d0_s17" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each with prominent midvein;</text>
      <biological_entity id="o16507" name="valve" name_original="valves" src="d0_s18" type="structure" />
      <biological_entity id="o16508" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o16507" id="r1135" name="with" negation="false" src="d0_s18" to="o16508" />
    </statement>
    <statement id="d0_s19">
      <text>replum straight;</text>
      <biological_entity id="o16509" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 54–86 per ovary;</text>
      <biological_entity id="o16510" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o16511" from="54" name="quantity" src="d0_s20" to="86" />
      </biological_entity>
      <biological_entity id="o16511" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 1–1.5 mm;</text>
      <biological_entity id="o16512" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s21" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma strongly 2-lobed.</text>
      <biological_entity id="o16513" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s22" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds broadly oblong, 2–2.7 × 1–1.2 mm;</text>
      <biological_entity id="o16514" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s23" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s23" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s23" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>wing 0.1–0.25 mm wide, continuous.</text>
      <biological_entity id="o16515" name="wing" name_original="wing" src="d0_s24" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s24" to="0.25" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="continuous" value_original="continuous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Glades, steep slopes, sandstone and soft shale in ravines or rocky openings in oak-pine-hickory forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="glades" />
        <character name="habitat" value="steep slopes" />
        <character name="habitat" value="sandstone" constraint="in ravines or rocky openings in oak-pine-hickory forests" />
        <character name="habitat" value="soft shale" constraint="in ravines or rocky openings in oak-pine-hickory forests" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="rocky openings" constraint="in oak-pine-hickory forests" />
        <character name="habitat" value="oak-pine-hickory forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark., Okla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>32.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Streptanthus squamiformis is known from the Ouachita Mountains in Arkansas (Howard, Polk, and Sevier counties) and Oklahoma (McCurtain County).</discussion>
  <discussion>The similarities between Streptanthus squamiformis and S. maculatus in most aspects are truly remarkable, especially in foliage, fruits, seeds, and flower color and size. The former differs by having the sepals densely pubescent (versus glabrous) with trichomes to 2 mm, both sepal pairs with subapical callosities 1–2 mm (versus callosities absent or, rarely, present on lateral sepals and 0.1–0.3 mm), smaller anthers (1.5–2.7 versus 3–4 mm), and pubescent or, sometimes, glabrous (versus always glabrous) fruiting pedicels.</discussion>
  
</bio:treatment>