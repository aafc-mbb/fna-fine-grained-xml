<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="treatment_page">420</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">brassica</taxon_name>
    <taxon_name authority="Ehrhart" date="1792" rank="species">elongata</taxon_name>
    <place_of_publication>
      <publication_title>Beitr. Naturk.</publication_title>
      <place_in_publication>7: 159. 1792</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus brassica;species elongata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">241000034</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(short-lived, often woody basally);</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous or hirsute.</text>
      <biological_entity id="o10928" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems (several from base), branched basally, 5–10 dm, (usually glabrous, rarely sparsely hirsute).</text>
      <biological_entity id="o10930" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s3" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: blade (usually bright green), obovate to elliptic (not lobed), (3–) 5–20 (–30) cm × (5–) 10–35 (–60) mm, (base cuneate), margins subentire to dentate, (surfaces glabrous or often with trichomes minute, tubercled-based, curved, coarse).</text>
      <biological_entity constraint="basal" id="o10931" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="obovate" name="shape" notes="" src="d0_s4" to="elliptic" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_length" src="d0_s4" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="60" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10932" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity id="o10933" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character char_type="range_value" from="subentire" name="shape" src="d0_s4" to="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves (distal) shortly petiolate;</text>
      <biological_entity constraint="cauline" id="o10934" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade (oblong or lanceolate, to 10 cm) base not auriculate or amplexicaul.</text>
      <biological_entity constraint="blade" id="o10935" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="amplexicaul" value_original="amplexicaul" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes paniculately branched.</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels spreading to divaricately ascending, (6–) 8–18 mm.</text>
      <biological_entity id="o10936" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="paniculately" name="architecture" src="d0_s7" value="branched" value_original="branched" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s8" to="divaricately ascending" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10937" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o10936" id="r768" name="fruiting" negation="false" src="d0_s8" to="o10937" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 3–4 (–4.5) × 1–1.5 mm;</text>
      <biological_entity id="o10938" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10939" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals bright-yellow to orange-yellow, obovate, (5–) 7–10 × 2.5–3.5 (–4) mm, claw 2.5–4 mm, apex rounded;</text>
      <biological_entity id="o10940" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10941" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="bright-yellow" name="coloration" src="d0_s10" to="orange-yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s10" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="10" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10942" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10943" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 3.5–4.5 mm;</text>
      <biological_entity id="o10944" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10945" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 1–1.5 mm;</text>
      <biological_entity id="o10946" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o10947" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>gynophore 1.5–4 (–5) mm in fruit.</text>
      <biological_entity id="o10948" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o10949" name="gynophore" name_original="gynophore" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o10950" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10950" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruits (stipitate), spreading to ascending (not appressed to rachis), torulose, terete, (1.5–) 2–4 (–4.8) cm × (1–) 1.5–2 mm;</text>
      <biological_entity id="o10951" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s14" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s14" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s14" value="terete" value_original="terete" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s14" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s14" to="4.8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s14" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s14" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valvular segment with (2–) 5–11 (–13) seeds per locule, (1.2–) 1.6–4 (–4.5) cm, terminal segment seedless, 0.5–2.5 (–3) mm.</text>
      <biological_entity constraint="valvular" id="o10952" name="segment" name_original="segment" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s15" to="1.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s15" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="some_measurement" notes="" src="d0_s15" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10953" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="atypical_quantity" src="d0_s15" to="5" to_inclusive="false" />
        <character char_type="range_value" from="11" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s15" to="13" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s15" to="11" />
      </biological_entity>
      <biological_entity id="o10954" name="locule" name_original="locule" src="d0_s15" type="structure" />
      <biological_entity constraint="terminal" id="o10955" name="segment" name_original="segment" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="seedless" value_original="seedless" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <relation from="o10952" id="r769" name="with" negation="false" src="d0_s15" to="o10953" />
      <relation from="o10953" id="r770" name="per" negation="false" src="d0_s15" to="o10954" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds grey to brown, 1–1.6 mm diam.;</text>
      <biological_entity id="o10956" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s16" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>seed-coat reticulate, mucilaginous when wetted.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 22.</text>
      <biological_entity id="o10957" name="seed-coat" name_original="seed-coat" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s17" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s17" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10958" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, disturbed ground, adjacent open juniper and sagebrush desert areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="disturbed ground" />
        <character name="habitat" value="adjacent open juniper" />
        <character name="habitat" value="sagebrush desert areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Nev., Oreg., Wash.; Europe; Asia; n Africa; introduced also in Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>The earliest North American collections of Brassica elongata were from ballast at Linnton, near Portland, Oregon, in 1911, and from a garden in Bingen, Klickitat County, Washington, in 1915. The species does not appear to have persisted at, or spread from, either location (R. C. Rollins and I. A. Al-Shehbaz 1986). It was next collected in 1968 from east-central Nevada, where it is now well-established in Eureka and White Pine counties, and just into Lander County, and spreading rapidly along both roadsides and adjacent high desert (Rollins 1980; Rollins and Al-Shehbaz; Rollins 1993). The semiarid region of North America appears to be a well-suited habitat for B. elongata and the species appears destined to become a permanent part of the flora of the Intermountain Basin (Rollins and Al-Shehbaz).</discussion>
  <discussion>According to R. C. Rollins (1980), the Nevada plants belong to subsp. integrifolia (Boissier) Breistroffer, but the species is so variable that dividing it into infraspecific taxa is not practical.</discussion>
  
</bio:treatment>