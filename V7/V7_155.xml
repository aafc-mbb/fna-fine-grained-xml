<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">25</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">87</other_info_on_meta>
    <other_info_on_meta type="mention_page">93</other_info_on_meta>
    <other_info_on_meta type="mention_page">99</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="mention_page">110</other_info_on_meta>
    <other_info_on_meta type="mention_page">121</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="mention_page">127</other_info_on_meta>
    <other_info_on_meta type="mention_page">135</other_info_on_meta>
    <other_info_on_meta type="mention_page">139</other_info_on_meta>
    <other_info_on_meta type="mention_page">1</other_info_on_meta>
    <other_info_on_meta type="treatment_page">141</other_info_on_meta>
    <other_info_on_meta type="illustration_page">142</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="C. K. Schneider" date="1904" rank="section">candidae</taxon_name>
    <taxon_name authority="Flüggé ex Willdenow" date="1806" rank="species">candida</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>4: 708. 1806</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section candidae;species candida</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445679</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">candida</taxon_name>
    <taxon_name authority="Andersson" date="unknown" rank="variety">denudata</taxon_name>
    <taxon_hierarchy>genus Salix;species candida;variety denudata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants often forming clones by layering.</text>
      <biological_entity id="o16425" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16426" name="clone" name_original="clones" src="d0_s0" type="structure" />
      <relation from="o16425" id="r1131" name="forming" negation="false" src="d0_s0" to="o16426" />
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches dark gray-brown to yellowbrown, not glaucous, woolly in patches or floccose to glabrescent;</text>
      <biological_entity id="o16427" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o16428" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character char_type="range_value" from="dark gray-brown" name="coloration" src="d0_s1" to="yellowbrown" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character constraint="in patches" constraintid="o16429" is_modifier="false" name="pubescence" src="d0_s1" value="woolly" value_original="woolly" />
        <character char_type="range_value" from="floccose" name="pubescence" src="d0_s1" to="glabrescent" />
      </biological_entity>
      <biological_entity id="o16429" name="patch" name_original="patches" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>branchlets yellowbrown to redbrown or gray-brown, densely (white) woolly or tomentose, sometimes floccose.</text>
      <biological_entity id="o16430" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o16431" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character char_type="range_value" from="yellowbrown" name="coloration" src="d0_s2" to="redbrown or gray-brown" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules rudimentary or foliaceous on early ones, late ones 2–3.6 mm, apex acute;</text>
      <biological_entity id="o16432" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o16433" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="rudimentary" value_original="rudimentary" />
        <character constraint="on ones" constraintid="o16434" is_modifier="false" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" notes="" src="d0_s3" to="3.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16434" name="one" name_original="ones" src="d0_s3" type="structure" />
      <biological_entity id="o16435" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole shallowly to deeply grooved adaxially, 3–10 mm, tomentose or densely (white) woolly adaxially (obscured by hairs);</text>
      <biological_entity id="o16436" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o16437" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shallowly to deeply; adaxially" name="architecture" src="d0_s4" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="densely; adaxially" name="pubescence" src="d0_s4" value="woolly" value_original="woolly" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>largest medial blade lorate, narrowly elliptic or oblanceolate, 47–103 × 5–20 mm, base convex or cuneate, margins strongly to slightly revolute, entire, or sinuate, apex acute or convex, abaxial surface glaucous (generally obscured by hairs), very densely to sparsely tomentose-woolly (cobwebby in age), hairs dull white, crinkled, adaxial dull or slightly glossy, moderately densely to sparsely tomentose, floccose, hairs dull white;</text>
      <biological_entity id="o16438" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="largest medial" id="o16439" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lorate" value_original="lorate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="47" from_unit="mm" name="length" src="d0_s5" to="103" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16440" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o16441" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o16442" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o16443" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="very densely; densely to sparsely" name="pubescence" src="d0_s5" value="tomentose-woolly" value_original="tomentose-woolly" />
      </biological_entity>
      <biological_entity id="o16444" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crinkled" value_original="crinkled" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16445" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s5" value="glossy" value_original="glossy" />
        <character is_modifier="false" modifier="moderately densely; densely to sparsely" name="pubescence" src="d0_s5" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o16446" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal blade margins entire;</text>
      <biological_entity id="o16447" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="blade" id="o16448" name="margin" name_original="margins" src="d0_s6" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>juvenile blade yellowish green, very densely tomentose abaxially, hairs white.</text>
      <biological_entity id="o16449" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o16450" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="very densely; abaxially" name="pubescence" src="d0_s7" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o16451" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Catkins flowering as leaves emerge;</text>
      <biological_entity id="o16453" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>staminate stout or subglobose, 17–39 × 8–16 mm, flowering branchlet 0.5–7 mm;</text>
      <biological_entity id="o16452" name="catkin" name_original="catkins" src="d0_s8" type="structure">
        <character constraint="as leaves" constraintid="o16453" is_modifier="false" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate densely to moderately densely flowered, stout or slender, 20–66 × 9–18 mm, flowering branchlet 1–24 mm;</text>
      <biological_entity id="o16454" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s9" value="subglobose" value_original="subglobose" />
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="pistillate" name="architecture" src="d0_s10" to="densely moderately densely flowered" />
        <character is_modifier="false" name="size" src="d0_s10" value="stout" value_original="stout" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s10" to="66" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="width" src="d0_s10" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16455" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract tawny or brown, 1.2–1.8 mm, apex rounded or acute, abaxially hairy, hairs straight to wavy.</text>
      <biological_entity constraint="floral" id="o16456" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16457" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o16458" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s11" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: adaxial nectary narrowly oblong to oblong, 0.6–1 mm;</text>
      <biological_entity id="o16459" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16460" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s12" to="oblong" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct or connate less than 1/2 their lengths;</text>
      <biological_entity id="o16461" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o16462" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s13" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers purple turning yellow, ellipsoid, long-cylindrical, or globose, 0.4–0.6 mm.</text>
      <biological_entity id="o16463" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o16464" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="purple turning yellow" value_original="purple turning yellow" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="long-cylindrical" value_original="long-cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s14" value="long-cylindrical" value_original="long-cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: adaxial nectary oblong, 0.4–1 mm;</text>
      <biological_entity id="o16465" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16466" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovary pyriform, beak sometimes slightly bulged below styles;</text>
      <biological_entity id="o16467" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16468" name="ovary" name_original="ovary" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="pyriform" value_original="pyriform" />
      </biological_entity>
      <biological_entity id="o16469" name="beak" name_original="beak" src="d0_s16" type="structure" />
      <biological_entity id="o16470" name="style" name_original="styles" src="d0_s16" type="structure" />
      <relation from="o16469" id="r1132" name="bulged" negation="false" src="d0_s16" to="o16470" />
    </statement>
    <statement id="d0_s17">
      <text>ovules 12–18 per ovary;</text>
      <biological_entity id="o16471" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16472" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o16473" from="12" name="quantity" src="d0_s17" to="18" />
      </biological_entity>
      <biological_entity id="o16473" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>styles 0.3–1.9 mm.</text>
      <biological_entity id="o16474" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o16475" name="style" name_original="styles" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s18" to="1.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Capsules 4–6 mm. 2n = 38.</text>
      <biological_entity id="o16476" name="capsule" name_original="capsules" src="d0_s19" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s19" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16477" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Apr-early Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jul" from="mid Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Floodplains, marl bogs, fens, and meadows, calcareous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="floodplains" />
        <character name="habitat" value="marl bogs" />
        <character name="habitat" value="fens" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="substrates" modifier="calcareous" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Colo., Conn., Idaho, Ill., Ind., Iowa, Maine, Mass., Mich., Minn., Mont., N.H., N.J., N.Y., N.Dak., Ohio, Pa., S.Dak., Vt., Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>91.</number>
  <other_name type="common_name">Sage or sage-leaf willow</other_name>
  <discussion>Occurrence of Salix candida in Nunavut is on Akimiski Island in James Bay.</discussion>
  <discussion>Salix candida is geographically wide-ranging but limited to calcareous habitats and, for that reason, it is quite local or even rare in some parts of its range.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Salix candida forms natural hybrids with S. bebbiana, S. brachycarpa var. brachycarpa, S. calcicola, S. eriocephala, S. famelica, S. myrtillifolia, S. petiolaris, and S. planifolia. Hybrids with S. discolor, S. petiolaris, and S. sericea have been reported (the latter also by C. K. Schneider 1921; M. L. Fernald 1950) but no convincing specimens have been seen. Salix candida hybrids are recognized from their woolly indumentum that often is conspicuous on leaves, stems, and ovaries. In hybrids, these characters, especially woolly patches on ovaries, stand out as discordant variation.</discussion>
  <discussion>Salix candida × S. eriocephala (S. ×rubella Bebb ex C. K. Schneider) was described by W. W. Rowlee and K. M. Wiegand (1896) as S. candida × S. cordata. In addition to woolly patches on the ovaries, they noted that buds of the hybrids usually are shorter, more divergent, and blunter than those in S. eriocephala, and are glabrous or hairy. Known from New York and Newfoundland; it should be expected throughout the sympatric range of the parental species.</discussion>
  <discussion>Salix candida × S. famelica: The Saskatchewan specimen resembles S. famelica but has the leaf indumentum of S. candida.</discussion>
  <discussion>Salix candida × S. myrtillifolia: Saskatchewan specimens combine characters of the two parents.</discussion>
  <discussion>Salix candida × S. petiolaris: Intermediates between these species are known from Michigan and New York (W. W. Rowlee and K. M. Wiegand 1896) as well as Ontario and Saskatchewan, but can be expected wherever the two grow together. The invalid name “S. ×clarkei” is sometimes used for this hybrid.</discussion>
  <discussion>The glabrescent form of Salix candida, forma denudata (Andersson) Rouleau, may be of hybrid origin.</discussion>
  
</bio:treatment>