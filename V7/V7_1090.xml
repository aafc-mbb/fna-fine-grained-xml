<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">622</other_info_on_meta>
    <other_info_on_meta type="mention_page">623</other_info_on_meta>
    <other_info_on_meta type="treatment_page">649</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(Rollins) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">lesicii</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 325. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species lesicii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094885</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="species">lesicii</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>5: 71. 1995</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lesquerella;species lesicii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(delicate, short-lived);</text>
      <biological_entity id="o16264" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex simple, (sometimes elongated, covered with persistent leaf-bases);</text>
    </statement>
    <statement id="d0_s3">
      <text>usually sparsely pubescent, trichomes 7–12-rayed, rays furcate near base.</text>
      <biological_entity id="o16265" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="usually sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o16266" name="trichome" name_original="trichomes" src="d0_s3" type="structure" />
      <biological_entity id="o16267" name="ray" name_original="rays" src="d0_s3" type="structure">
        <character constraint="near base" constraintid="o16268" is_modifier="false" name="shape" src="d0_s3" value="furcate" value_original="furcate" />
      </biological_entity>
      <biological_entity id="o16268" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Stems simple from base, erect to decumbent, (unbranched, mostly filiform, slender), 1–1.5 dm.</text>
      <biological_entity id="o16269" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character constraint="from base" constraintid="o16270" is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="erect" name="orientation" notes="" src="d0_s4" to="decumbent" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s4" to="1.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o16270" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves (erect, petiole slender);</text>
      <biological_entity constraint="basal" id="o16271" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blades broadly ovate to elliptic, 0.5–1 cm, (base abruptly narrowing to petiole), margins entire.</text>
      <biological_entity id="o16272" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s6" to="elliptic" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o16273" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves (remote, distally shortly petiolate);</text>
      <biological_entity constraint="cauline" id="o16274" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>blade ± spatulate, (base cuneate), margins entire.</text>
      <biological_entity id="o16275" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s8" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o16276" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes lax, (elongated, few-flowered).</text>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels (recurved to widely spreading, filiform, slender), 5–10 mm.</text>
      <biological_entity id="o16277" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16278" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o16277" id="r1124" name="fruiting" negation="false" src="d0_s10" to="o16278" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals (erect), oblong, 3.5–4 mm, (lateral pair not saccate);</text>
      <biological_entity id="o16279" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16280" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals (often fading to light purple apically), spatulate to nearly lingulate, 6–7 mm.</text>
      <biological_entity id="o16281" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" notes="" src="d0_s12" to="nearly lingulate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16282" name="petal" name_original="petals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Fruits (pendent), globose or subglobose, compressed, 3–4 mm;</text>
      <biological_entity id="o16283" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s13" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>valves ± densely pubescent;</text>
      <biological_entity id="o16284" name="valve" name_original="valves" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="more or less densely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovules 6–10 per ovary;</text>
      <biological_entity id="o16285" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o16286" from="6" name="quantity" src="d0_s15" to="10" />
      </biological_entity>
      <biological_entity id="o16286" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style ca. 1.5 mm.</text>
      <biological_entity id="o16287" name="style" name_original="style" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds not seen.</text>
      <biological_entity id="o16288" name="seed" name_original="seeds" src="d0_s17" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun(-early Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Jun" />
        <character name="flowering time" char_type="atypical_range" to="early Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Pryor Mountains, on limestone soils in woodlands of Rocky Mountain juniper and/or mountain mahogany, and widely scattered Douglas-fir, fellfields dominated by bluebunch wheatgrass and cushion plants</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone soils" constraint="in woodlands of rocky mountain juniper and\/or mountain mahogany , and widely scattered douglas-fir" />
        <character name="habitat" value="woodlands" constraint="of rocky mountain juniper and\/or mountain mahogany , and widely scattered douglas-fir" />
        <character name="habitat" value="rocky mountain juniper and\/or mountain mahogany" />
        <character name="habitat" value="bluebunch wheatgrass" modifier="fellfields dominated by" />
        <character name="habitat" value="cushion plants" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>48.</number>
  <other_name type="common_name">Pryor Mountains bladderpod</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>