<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">129</other_info_on_meta>
    <other_info_on_meta type="illustration_page">130</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="Seringe" date="1824" rank="section">cinerella</taxon_name>
    <taxon_name authority="Marshall" date="1785" rank="species">humilis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">humilis</taxon_name>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section cinerella;species humilis;variety humilis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445744</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">humilis</taxon_name>
    <taxon_name authority="Farwell" date="unknown" rank="variety">hyporhysa</taxon_name>
    <taxon_hierarchy>genus Salix;species humilis;variety hyporhysa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">humilis</taxon_name>
    <taxon_name authority="(Andersson) B. L. Robinson &amp; Fernald" date="unknown" rank="variety">keweenawensis</taxon_name>
    <taxon_hierarchy>genus Salix;species humilis;variety keweenawensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">humilis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">rigidiuscula</taxon_name>
    <taxon_hierarchy>genus Salix;species humilis;variety rigidiuscula;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Mid shrubs, 0.3–3 m.</text>
      <biological_entity constraint="mid" id="o10559" name="shrub" name_original="shrubs" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems usually erect, sometimes decumbent;</text>
      <biological_entity id="o10560" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches tomentose to glabrescent, peeled wood smooth or striate, striae sometimes very dense, to 20 mm;</text>
      <biological_entity id="o10561" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="tomentose" name="pubescence" src="d0_s2" to="glabrescent" />
      </biological_entity>
      <biological_entity id="o10562" name="wood" name_original="wood" src="d0_s2" type="structure">
        <character is_modifier="true" name="condition" src="d0_s2" value="peeled" value_original="peeled" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity id="o10563" name="stria" name_original="striae" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes very" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s2" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets redbrown or greenish brown.</text>
      <biological_entity id="o10564" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="greenish brown" value_original="greenish brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules usually foliaceous (rarely rudimentary) on late ones;</text>
      <biological_entity id="o10565" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o10566" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character constraint="on ones" constraintid="o10567" is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o10567" name="one" name_original="ones" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole (1.5–) 3–7 (–12) mm, velvety or pilose adaxially;</text>
      <biological_entity id="o10568" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o10569" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="velvety" value_original="velvety" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>largest medial blade narrowly oblong, narrowly elliptic, elliptic, oblanceolate, obovate, or broadly obovate, (20–) 50–90 (–135) × (7–) 13–23 (–35) mm, 2.3–4–7.5 times as long as wide, margins strongly revolute to flat, abaxial surface with hairs rarely also ferruginous, adaxial slightly or highly glossy, glabrous, pubescent, or pilose (hairs white, sometimes also ferruginous);</text>
      <biological_entity id="o10570" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="largest medial" id="o10571" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_length" src="d0_s6" to="50" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="90" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="135" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="length" src="d0_s6" to="90" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_width" src="d0_s6" to="13" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="23" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="35" to_unit="mm" />
        <character char_type="range_value" from="13" from_unit="mm" name="width" src="d0_s6" to="23" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="2.3-4-7.5" value_original="2.3-4-7.5" />
      </biological_entity>
      <biological_entity id="o10572" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character char_type="range_value" from="strongly revolute" name="shape" src="d0_s6" to="flat" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10573" name="surface" name_original="surface" src="d0_s6" type="structure" />
      <biological_entity id="o10574" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="ferruginous" value_original="ferruginous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10575" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="highly" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
      <relation from="o10573" id="r738" name="with" negation="false" src="d0_s6" to="o10574" />
    </statement>
    <statement id="d0_s7">
      <text>proximal blade margins usually entire, sometimes serrulate.</text>
      <biological_entity id="o10576" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o10577" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Catkins: staminate 14.5–34 × 7–19 mm, flowering branchlet 0 mm;</text>
      <biological_entity id="o10578" name="catkin" name_original="catkins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="14.5" from_unit="mm" name="length" src="d0_s8" to="34" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s8" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10579" name="branchlet" name_original="branchlet" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="0" value_original="0" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pistillate 9–47 (–55 in fruit) × 5.5–19 mm, flowering branchlet 0–4 mm;</text>
      <biological_entity id="o10580" name="catkin" name_original="catkins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s9" to="47" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="width" src="d0_s9" to="19" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10581" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>floral bract 1.2–2 mm.</text>
      <biological_entity id="o10582" name="catkin" name_original="catkins" src="d0_s10" type="structure" />
      <biological_entity constraint="floral" id="o10583" name="bract" name_original="bract" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: filaments glabrous.</text>
      <biological_entity id="o10584" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o10585" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pistillate flowers: ovary obclavate;</text>
      <biological_entity id="o10586" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10587" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obclavate" value_original="obclavate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stigmas 0.24–0.33–0.56 mm;</text>
      <biological_entity id="o10588" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10589" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.24" from_unit="mm" name="some_measurement" src="d0_s13" to="0.33-0.56" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovules 6–12 per ovary.</text>
      <biological_entity id="o10590" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10591" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o10592" from="6" name="quantity" src="d0_s14" to="12" />
      </biological_entity>
      <biological_entity id="o10592" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Capsules 7–12 mm. 2n = 38 and 76.</text>
      <biological_entity id="o10593" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s15" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10594" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" unit="and" value="38" value_original="38" />
        <character name="quantity" src="d0_s15" unit="and" value="76" value_original="76" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (north) early Mar-early Jun, (south) late Jan-late Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="north" to="early Jun" from="early Mar" />
        <character name="flowering time" char_type="range_value" modifier="south" to="late Apr" from="late Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry mixed woods and forests, Picea mariana-lichen woods, Picea glauca-Abies balsamea forests, wet to dry prairies, grassy balds, loess bluffs, sandy stream terraces, coastal barrens, Carex-Typha meadows, fine sand to rocky granitic, gneissic, limestone, and serpentine substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry mixed woods" />
        <character name="habitat" value="forests" />
        <character name="habitat" value="picea mariana-lichen woods" />
        <character name="habitat" value="picea" />
        <character name="habitat" value="balsamea forests" modifier="glauca-abies" />
        <character name="habitat" value="wet to dry prairies" />
        <character name="habitat" value="grassy balds" />
        <character name="habitat" value="loess bluffs" />
        <character name="habitat" value="sandy stream" />
        <character name="habitat" value="coastal barrens" />
        <character name="habitat" value="carex-typha meadows" />
        <character name="habitat" value="fine sand" />
        <character name="habitat" value="rocky granitic" />
        <character name="habitat" value="gneissic" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="serpentine substrates" modifier="and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>20-1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="20" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Man., N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.H., N.J., N.Y., N.C., N.Dak., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>78a.</number>
  <other_name type="common_name">Prairie willow</other_name>
  <discussion>See 79. Salix scouleriana and 82. S. atrocinerea for comparative descriptions.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Variety humilis forms natural hybrids with Salix bebbiana, S. discolor, S. eriocephala, and S. planifolia. Hybrids with S. discolor or S. planifolia often are suggested by glabrous or glabrate leaves.</discussion>
  
</bio:treatment>