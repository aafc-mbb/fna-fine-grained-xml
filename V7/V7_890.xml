<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="treatment_page">555</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">eutremeae</taxon_name>
    <taxon_name authority="R. Brown" date="1823" rank="genus">EUTREMA</taxon_name>
    <place_of_publication>
      <publication_title>Chlor. Melvill.,</publication_title>
      <place_in_publication>9, plate A. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe eutremeae;genus EUTREMA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek eu- , well, and trema, hole, alluding to perforation in fruit septum</other_info_on_name>
    <other_info_on_name type="fna_id">112471</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Pilger" date="unknown" rank="genus">Neomartinella</taxon_name>
    <taxon_hierarchy>genus Neomartinella;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="O. E. Schulz" date="unknown" rank="genus">Platycraspedum</taxon_name>
    <taxon_hierarchy>genus Platycraspedum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="O. E. Schulz" date="unknown" rank="genus">Thellungiella</taxon_name>
    <taxon_hierarchy>genus Thellungiella;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Matsumura" date="unknown" rank="genus">Wasabia</taxon_name>
    <taxon_hierarchy>genus Wasabia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(caudex simple or branched), [rhizomatous];</text>
    </statement>
    <statement id="d0_s2">
      <text>not scapose.</text>
      <biological_entity id="o32481" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect or ascending [decumbent], unbranched or branched distally.</text>
      <biological_entity id="o32483" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate or sessile;</text>
      <biological_entity id="o32484" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal rosulate or not, petiolate, blade margins usually entire or palmately lobed, rarely dentate or pinnatifid, (pinnately or palmately veined);</text>
      <biological_entity constraint="basal" id="o32485" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character name="arrangement" src="d0_s6" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o32486" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="palmately" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline petiolate or sessile, blade (base cuneate or sagittate-amplexicaul), margins entire or repand [dentate, crenate], (pinnately or palmately veined, ultimate veins sometimes ending with apiculate callosities).</text>
      <biological_entity constraint="cauline" id="o32487" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o32488" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o32489" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (corymbose).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate to ascending, slender or stout.</text>
      <biological_entity id="o32490" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s9" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o32491" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o32490" id="r2176" name="fruiting" negation="false" src="d0_s9" to="o32491" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals ovate or oblong;</text>
      <biological_entity id="o32492" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o32493" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals usually spatulate, rarely obovate, claw not differentiated from blade, (apex obtuse to emarginate);</text>
      <biological_entity id="o32494" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o32495" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o32496" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character constraint="from blade" constraintid="o32497" is_modifier="false" modifier="not" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o32497" name="blade" name_original="blade" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stamens slightly tetradynamous;</text>
      <biological_entity id="o32498" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o32499" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments slightly dilated basally;</text>
      <biological_entity id="o32500" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o32501" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="slightly; basally" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ovate or oblong, (apex obtuse);</text>
      <biological_entity id="o32502" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o32503" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectar glands confluent, often subtending bases of stamens, median glands present or absent.</text>
      <biological_entity id="o32504" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity constraint="nectar" id="o32505" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o32506" name="base" name_original="bases" src="d0_s15" type="structure" />
      <biological_entity id="o32507" name="stamen" name_original="stamens" src="d0_s15" type="structure" />
      <biological_entity constraint="median" id="o32508" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o32505" id="r2177" name="often subtending" negation="false" src="d0_s15" to="o32506" />
      <relation from="o32505" id="r2178" name="part_of" negation="false" src="d0_s15" to="o32507" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits sessile or shortly stipitate, linear or oblong [ovoid, lanceoloid], smooth or torulose, terete, slightly 4-angled, latiseptate, or angustiseptate;</text>
      <biological_entity id="o32509" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s16" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s16" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s16" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="latiseptate" value_original="latiseptate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="angustiseptate" value_original="angustiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves each with obscure or prominent midvein;</text>
      <biological_entity id="o32510" name="valve" name_original="valves" src="d0_s17" type="structure" />
      <biological_entity id="o32511" name="midvein" name_original="midvein" src="d0_s17" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s17" value="obscure" value_original="obscure" />
        <character is_modifier="true" name="prominence" src="d0_s17" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o32510" id="r2179" name="with" negation="false" src="d0_s17" to="o32511" />
    </statement>
    <statement id="d0_s18">
      <text>replum rounded;</text>
      <biological_entity id="o32512" name="replum" name_original="replum" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>septum complete or perforated;</text>
      <biological_entity id="o32513" name="septum" name_original="septum" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="complete" value_original="complete" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="perforated" value_original="perforated" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules [2–] (6–) 8–96 per ovary;</text>
      <biological_entity id="o32514" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" from="2" name="atypical_quantity" src="d0_s20" to="#8" to_inclusive="false" />
        <character char_type="range_value" constraint="per ovary" constraintid="o32515" from="#8" name="quantity" src="d0_s20" to="96" />
      </biological_entity>
      <biological_entity id="o32515" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>stigma capitate.</text>
      <biological_entity id="o32516" name="stigma" name_original="stigma" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s21" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds usually uniseriate, rarely biseriate, plump, not winged, oblong or ovoid;</text>
      <biological_entity id="o32517" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s22" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s22" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" name="size" src="d0_s22" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s22" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s22" value="ovoid" value_original="ovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>seed-coat (usually obscurely reticulate, rarely foveolate or papillate), often not mucilaginous when wetted;</text>
      <biological_entity id="o32518" name="seed-coat" name_original="seed-coat" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s23" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>cotyledons incumbent [rarely accumbent].</text>
      <biological_entity id="o32519" name="cotyledon" name_original="cotyledons" src="d0_s24" type="structure">
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s24" value="incumbent" value_original="incumbent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, c, e Asia (Himalayas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="e Asia (Himalayas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>59.</number>
  <discussion>Species 26 (2 in the flora).</discussion>
  <discussion>The fleshy rhizomes of the eastern Asian Eutrema japonicum (Siebold) Maximowicz are pungent and are the main source of true wasabi (W. H. Hodge 1974). Paste of horseradish (Armoracia rusticana) is often used as an inexpensive substitute.</discussion>
  <discussion>Molecular studies, combined with a critical evaluation of morphology, have shown that Thellungiella is nested within, and hardly distinct from, the earlier-published Eutrema, and therefore the two genera have been combined (I. A. Al-Shehbaz and S. I. Warwick 2005; Warwick et al. 2006).</discussion>
  <references>
    <reference>Al-Shehbaz, I. A. and S. I. Warwick. 2005. A synopsis of Eutrema (Brassicaceae). Harvard Pap. Bot. 10: 129–135.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Perennials; cauline leaf blades: bases cuneate, not auriculate or sagittate; ovules (6-)8-12 (-14) per ovary.</description>
      <determination>1 Eutrema edwardsii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Annuals; cauline leaf blades: bases usually sagittate-amplexicaul, rarely auriculate; ovules 55-96 per ovary.</description>
      <determination>2 Eutrema salsugineum</determination>
    </key_statement>
  </key>
</bio:treatment>