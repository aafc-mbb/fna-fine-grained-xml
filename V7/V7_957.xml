<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">571</other_info_on_meta>
    <other_info_on_meta type="treatment_page">590</other_info_on_meta>
    <other_info_on_meta type="illustration_page">586</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">lepidieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lepidium</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">perfoliatum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 643. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe lepidieae;genus lepidium;species perfoliatum</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200009587</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nasturtium</taxon_name>
    <taxon_name authority="(Linnaeus) Besser" date="unknown" rank="species">perfoliatum</taxon_name>
    <taxon_hierarchy>genus Nasturtium;species perfoliatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(glaucous), glabrous or sparsely pubescent proximally.</text>
      <biological_entity id="o2167" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple from base, erect, branched distally, (0.7–) 1.5–4.3 (–5.6) dm.</text>
      <biological_entity id="o2169" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o2170" is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="4.3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="5.6" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s2" to="4.3" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o2170" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves rosulate;</text>
      <biological_entity constraint="basal" id="o2171" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole (0.5–) 1–2 (–4) cm;</text>
      <biological_entity id="o2172" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade 2-pinnatifid or 3-pinnatifid or pinnatisect (lobes linear to oblong), (1–) 3–8 (–15) cm, margins entire.</text>
      <biological_entity id="o2173" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="2-pinnatifid" value_original="2-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3-pinnatifid" value_original="3-pinnatifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="pinnatisect" value_original="pinnatisect" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2174" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves sessile;</text>
      <biological_entity constraint="cauline" id="o2175" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade ovate to cordate or suborbicular, (0.5–) 1–3 (–4) cm × (5–) 10–25 (–35) mm, base deeply cordate-amplexicaul, not auriculate, margins entire.</text>
      <biological_entity id="o2176" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="cordate or suborbicular" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s7" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s7" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="35" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2177" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="deeply" name="architecture" src="d0_s7" value="cordate-amplexicaul" value_original="cordate-amplexicaul" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o2178" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes considerably elongated in fruit;</text>
      <biological_entity id="o2179" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o2180" is_modifier="false" modifier="considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o2180" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>rachis glabrous.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels divaricate-ascending to horizontal, straight, (terete), 3–6 (–7) × 0.2–0.3 mm, glabrous.</text>
      <biological_entity id="o2181" name="rachis" name_original="rachis" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="divaricate-ascending" name="orientation" src="d0_s10" to="horizontal" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2182" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o2181" id="r193" name="fruiting" negation="false" src="d0_s10" to="o2182" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals oblong, 0.8–1 (–1.3) × 0.5–0.8 mm;</text>
      <biological_entity id="o2183" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2184" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s11" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals pale-yellow, narrowly spatulate, 1–1.5 (–1.9) × 0.2–0.5 mm, claw 0.5–1 mm;</text>
      <biological_entity id="o2185" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o2186" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s12" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2187" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 6;</text>
      <biological_entity id="o2188" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o2189" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments 0.6–0.9 mm, (glabrous);</text>
      <biological_entity id="o2190" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o2191" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 0.1–0.2 mm.</text>
      <biological_entity id="o2192" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o2193" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s15" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits orbicular to rhombic or broadly obovate, 3–4.5 (–5) × 3–4.1 mm, apically winged, apical notch 0.1–0.3 mm deep;</text>
      <biological_entity id="o2194" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="orbicular" name="shape" src="d0_s16" to="rhombic or broadly obovate" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s16" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s16" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s16" to="4.1" to_unit="mm" />
        <character is_modifier="false" modifier="apically" name="architecture" src="d0_s16" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="apical" id="o2195" name="notch" name_original="notch" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s16" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s16" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves thin, smooth, not or obscurely veined, glabrous;</text>
      <biological_entity id="o2196" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="not; obscurely" name="architecture" src="d0_s17" value="veined" value_original="veined" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style 0.1–0.4 mm, subequaling or slightly exserted beyond apical notch.</text>
      <biological_entity id="o2197" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s18" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="position_relational_or_variability" src="d0_s18" value="subequaling" value_original="subequaling" />
        <character name="position_relational_or_variability" src="d0_s18" value="slightly exserted beyond apical notch" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds (dark-brown), ovate, 1.6–2 (–2.3) × 1.2–1.4 mm. 2n = 16.</text>
      <biological_entity id="o2198" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s19" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s19" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s19" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2199" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, dry sandy slopes, pinyon-juniper woodlands, sagebrush flats, open deserts, roadsides, pastures, meadows, open grasslands, alkaline flats and sinks, fields, disturbed sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="dry sandy slopes" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="flats" modifier="sagebrush" />
        <character name="habitat" value="open deserts" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="open grasslands" />
        <character name="habitat" value="alkaline flats" />
        <character name="habitat" value="sinks" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="disturbed sites" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., Ont., Sask.; Ariz., Ark., Calif., Colo., Conn., Ga., Idaho, Ind., Iowa, Kans., Maine, Mass., Mich., Miss., Mo., Mont., Nebr., Nev., N.Mex., N.Y., N.C., Ohio, Okla., Oreg., Pa., S.Dak., Tenn., Utah, Wash., Wis., Wyo.; Europe; Asia; n Africa; introduced also in Mexico (Baja California), South America (Argentina), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in Mexico (Baja California)" establishment_means="introduced" />
        <character name="distribution" value="South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  
</bio:treatment>