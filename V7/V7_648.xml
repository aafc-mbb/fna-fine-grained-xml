<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Juan B. Martínez-Laborde</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">234</other_info_on_meta>
    <other_info_on_meta type="mention_page">243</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="mention_page">436</other_info_on_meta>
    <other_info_on_meta type="treatment_page">432</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="de Candolle" date="1821" rank="genus">DIPLOTAXIS</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Mus. Hist. Nat.</publication_title>
      <place_in_publication>7: 243. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus DIPLOTAXIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek diplo- , double, and taxis, arrangement, alluding to number of seed rows in each locule of fruit</other_info_on_name>
    <other_info_on_name type="fna_id">110479</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials;</text>
      <biological_entity id="o36994" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>(sometimes suffrutescent);</text>
    </statement>
    <statement id="d0_s2">
      <text>scapose or not;</text>
    </statement>
    <statement id="d0_s3">
      <text>glabrous, glabrescent, or pubescent.</text>
      <biological_entity id="o36995" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character name="architecture" src="d0_s2" value="not" value_original="not" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems erect or ascending, branched.</text>
      <biological_entity id="o36997" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and, sometimes, cauline;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate or sessile;</text>
      <biological_entity id="o36998" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="sometimes" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal rosulate or not, petiolate, blade margins dentate, sinuate, or pinnatisect;</text>
      <biological_entity constraint="basal" id="o36999" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="rosulate" value_original="rosulate" />
        <character name="arrangement" src="d0_s7" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o37000" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pinnatisect" value_original="pinnatisect" />
        <character is_modifier="false" name="shape" src="d0_s7" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pinnatisect" value_original="pinnatisect" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cauline petiolate or sessile, blade (base not auriculate), margins entire or dentate.</text>
      <biological_entity constraint="cauline" id="o37001" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o37002" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity id="o37003" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes (corymbose, sometimes shortly bracteate basally), considerably elongated in fruit.</text>
      <biological_entity id="o37005" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels ascending, divaricate, or reflexed, stout to slender.</text>
      <biological_entity id="o37004" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o37005" is_modifier="false" modifier="considerably" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="stout" name="size" src="d0_s10" to="slender" />
      </biological_entity>
      <biological_entity id="o37006" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o37004" id="r2494" name="fruiting" negation="false" src="d0_s10" to="o37006" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals ascending to spreading, oblong, lateral pair not saccate basally;</text>
      <biological_entity id="o37007" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o37008" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s11" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s11" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals yellow or white [purple], obovate, (apex rounded or truncate);</text>
      <biological_entity id="o37009" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o37010" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o37011" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o37012" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments not dilated basally;</text>
      <biological_entity id="o37013" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o37014" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers oblong to ovate, (apex obtuse);</text>
      <biological_entity id="o37015" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o37016" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s15" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands (4): lateral cushionlike, median cylindrical.</text>
      <biological_entity constraint="nectar" id="o37017" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character name="atypical_quantity" src="d0_s16" value="4" value_original="4" />
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cushionlike" value_original="cushionlike" />
        <character is_modifier="false" name="position" src="d0_s16" value="median" value_original="median" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits siliques, dehiscent, sessile or (long-) stipitate, segments 1 or 2, linear to linear-oblong, torulose, latiseptate or terete;</text>
      <biological_entity constraint="fruits" id="o37018" name="silique" name_original="siliques" src="d0_s17" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="stipitate" value_original="stipitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>(proximal segment numerous-seeded, 1-veined; terminal segment 0–2-seeded);</text>
      <biological_entity id="o37019" name="segment" name_original="segments" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="count" value_original="count" />
        <character is_modifier="false" name="shape" src="d0_s17" value="list" value_original="list" />
        <character name="quantity" src="d0_s17" unit="or punct linear to linear-oblong" value="1" value_original="1" />
        <character name="quantity" src="d0_s17" unit="or punct linear to linear-oblong" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="latiseptate" value_original="latiseptate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>valves glabrous;</text>
      <biological_entity id="o37020" name="valve" name_original="valves" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>replum rounded;</text>
      <biological_entity id="o37021" name="replum" name_original="replum" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>septum complete;</text>
      <biological_entity id="o37022" name="septum" name_original="septum" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s21" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>ovules [12–] 20–36 (–46) [–276] per ovary;</text>
      <biological_entity id="o37024" name="ovary" name_original="ovary" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>(style obsolete or distinct);</text>
      <biological_entity id="o37023" name="ovule" name_original="ovules" src="d0_s22" type="structure">
        <character char_type="range_value" from="12" name="atypical_quantity" src="d0_s22" to="20" to_inclusive="false" />
        <character char_type="range_value" from="36" from_inclusive="false" name="atypical_quantity" src="d0_s22" to="46" />
        <character char_type="range_value" constraint="per ovary" constraintid="o37024" from="20" name="quantity" src="d0_s22" to="36" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>stigma capitate or somewhat decurrent, 2-lobed.</text>
      <biological_entity id="o37025" name="stigma" name_original="stigma" src="d0_s24" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s24" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s24" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="shape" src="d0_s24" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>Seeds usually biseriate, rarely uniseriate, not winged, ovoid or ellipsoid;</text>
      <biological_entity id="o37026" name="seed" name_original="seeds" src="d0_s25" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s25" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_arrangement" src="d0_s25" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s25" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s25" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s25" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>seed-coat (smooth or minutely reticulate), slightly mucilaginous or not when wetted;</text>
      <biological_entity id="o37027" name="seed-coat" name_original="seed-coat" src="d0_s26" type="structure">
        <character is_modifier="false" modifier="slightly" name="coating" src="d0_s26" value="mucilaginous" value_original="mucilaginous" />
        <character name="coating" src="d0_s26" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>cotyledons conduplicate.</text>
    </statement>
    <statement id="d0_s28">
      <text>x = 7, [8, 9, 10,] 11, [13,] 21.</text>
      <biological_entity id="o37028" name="cotyledon" name_original="cotyledons" src="d0_s27" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s27" value="conduplicate" value_original="conduplicate" />
      </biological_entity>
      <biological_entity constraint="x" id="o37029" name="chromosome" name_original="" src="d0_s28" type="structure">
        <character name="quantity" src="d0_s28" value="7" value_original="7" />
        <character name="quantity" src="d0_s28" value="[8" value_original="[8" />
        <character name="quantity" src="d0_s28" value="9" value_original="9" />
        <character name="quantity" src="d0_s28" value="10" value_original="10" />
        <character name="quantity" src="d0_s28" value="11" value_original="11" />
        <character name="quantity" src="d0_s28" value="[13" value_original="[13" />
        <character name="quantity" src="d0_s28" value="21" value_original="21" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, Asia, n Africa; introduced also in n Mexico, West Indies (Bahamas), Bermuda, South America, Pacific Islands (New Zealand), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="also in n Mexico" establishment_means="introduced" />
        <character name="distribution" value="West Indies (Bahamas)" establishment_means="introduced" />
        <character name="distribution" value="Bermuda" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (New Zealand)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>22.</number>
  <other_name type="common_name">Wall-rocket</other_name>
  <discussion>Species 25–30 (3 in the flora).</discussion>
  <references>
    <reference>Eschmann-Grupe, G., H. Hurka, and B. Neuffer. 2003. Species relationships within Diplotaxis (Brassicaceae) and the phylogenetic origin of D. muralis. Pl. Syst. Evol. 243: 13–29.</reference>
    <reference>Martínez-Laborde, J. B. 1988. Estudio Sistemático del Género Diplotaxis DC. (Cruciferae, Brassiceae). Ph.D. dissertation. Universidad Politécnica de Madrid.</reference>
    <reference>Mummenhoff, K., G. Eschmann-Grupe, and K. Zunk. 1993. Subunit polypeptide composition of rubisco indicates Diplotaxis viminea as maternal parent species of amphidiploid Diplotaxis muralis. Phytochemistry 34: 429–431.</reference>
    <reference>Sánchez-Yélamo, M. D. and J. B. Martínez-Laborde. 1991. Chemotaxonomic approach to Diplotaxis muralis (Cruciferae, Brassiceae) and related species. Biochem. Syst. &amp; Ecol. 19: 477–482.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems densely pubescent throughout; leaf blade surfaces shortly pubescent throughout; sepals pubescent, trichomes ± flexuous; petals white (turning purple); fruits: distal segment 1- or 2-seeded.</description>
      <determination>3 Diplotaxis erucoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems glabrescent to moderately pubescent basally or distally; leaf blade surfaces glabrescent, or margins and veins glabrescent to sparsely pubescent; sepals glabrous or pubescent, trichomes straight; petals yellow; fruits: distal segment seedless</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Perennials, with adventitious buds on roots; stems frequently foliose, glabrescent or sparsely pubescent basally; fruits erect; gynophores 0.5-3 mm.</description>
      <determination>1 Diplotaxis tenuifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Annuals or perennials (short-lived), without buds on roots; stems frequently scapose, moderately pubescent; fruits erect-patent; gynophores obsolete or to 0.5 mm.</description>
      <determination>2 Diplotaxis muralis</determination>
    </key_statement>
  </key>
</bio:treatment>