<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">485</other_info_on_meta>
    <other_info_on_meta type="illustration_page">481</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Steudel" date="1840" rank="genus">iodanthus</taxon_name>
    <taxon_name authority="(Michaux) Steudel" date="1840" rank="species">pinnatifidus</taxon_name>
    <place_of_publication>
      <publication_title>Nomencl. Bot., ed.</publication_title>
      <place_in_publication>2, 1: 812. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus iodanthus;species pinnatifidus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416693</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hesperis</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">pinnatifida</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 31. 1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hesperis;species pinnatifida;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) A. Gray" date="unknown" rank="species">hesperidoides</taxon_name>
    <taxon_hierarchy>genus Arabis;species hesperidoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheiranthus</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">hesperidoides</taxon_name>
    <taxon_hierarchy>genus Cheiranthus;species hesperidoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Iodanthus</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Torrey &amp; A. Gray" date="unknown" rank="species">hesperidoides</taxon_name>
    <taxon_hierarchy>genus Iodanthus;species hesperidoides;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelypodium</taxon_name>
    <taxon_name authority="(Michaux) S. Watson" date="unknown" rank="species">pinnatifidum</taxon_name>
    <taxon_hierarchy>genus Thelypodium;species pinnatifidum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 3–8 (–10) dm, striate.</text>
      <biological_entity id="o2095" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s0" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves: (proximal) petiole (0.5–) 1–4 cm, (narrowly to broadly winged), distal sessile;</text>
      <biological_entity constraint="cauline" id="o2096" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o2097" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s1" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o2098" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade lanceolate, ovate, elliptic, or oblong, (3–) 4–12 (–15) cm × (12–) 20–55 (–70) mm, base cuneate to attenuate, or (distalmost) minutely to coarsely auriculate, margins usually minutely to coarsely, regularly or irregularly, dentate or serrate, rarely subentire, apex acute to acuminate, surfaces glabrous or pubescent.</text>
      <biological_entity constraint="cauline" id="o2099" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o2100" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_length" src="d0_s2" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="12" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_width" src="d0_s2" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="55" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="70" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s2" to="55" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2101" name="base" name_original="base" src="d0_s2" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s2" to="attenuate or" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s2" to="attenuate or" />
      </biological_entity>
      <biological_entity id="o2102" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="coarsely; regularly; irregularly" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="serrate" value_original="serrate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="subentire" value_original="subentire" />
      </biological_entity>
      <biological_entity id="o2103" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Fruiting pedicels straight or slightly curved upward, (2–) 3–8 (–9) mm (nearly as thick as fruit).</text>
      <biological_entity id="o2104" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s3" value="curved" value_original="curved" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2105" name="pedicel" name_original="pedicels" src="d0_s3" type="structure" />
      <relation from="o2104" id="r186" name="fruiting" negation="false" src="d0_s3" to="o2105" />
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepals 3–6 × 1–1.5 mm, glabrous or subapically pilose;</text>
      <biological_entity id="o2106" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o2107" name="sepal" name_original="sepals" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s4" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="subapically" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petals 7–12 (–14) × 1.5–2.5 (–3) mm, attenuate to claw (claw 4–7 mm);</text>
      <biological_entity id="o2108" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o2109" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s5" to="14" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s5" to="12" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="2.5" to_unit="mm" />
        <character constraint="to claw" constraintid="o2110" is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o2110" name="claw" name_original="claw" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>filaments 3–6 mm;</text>
      <biological_entity id="o2111" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2112" name="filament" name_original="filaments" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>anthers 2–2.5 mm;</text>
      <biological_entity id="o2113" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2114" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>gynophore obsolete or to 1 mm.</text>
      <biological_entity id="o2115" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2116" name="gynophore" name_original="gynophore" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="obsolete" value_original="obsolete" />
        <character name="prominence" src="d0_s8" value="0-1 mm" value_original="0-1 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits usually divaricate to ascending, rarely erect, (1.5–) 2–3.5 (–4) cm × 1–1.5 mm;</text>
      <biological_entity id="o2117" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s9" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s9" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style (1–) 2–4 mm.</text>
      <biological_entity id="o2118" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds 1.2–1.5 × 0.7–1 mm.</text>
      <biological_entity id="o2119" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s11" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Apr-early Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jul" from="late Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded banks, thickets, wooded ravines, limestone or sandstone bluffs, bottomland woods, swamps, flood plains, creeks, streamsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded banks" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="wooded ravines" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="sandstone bluffs" />
        <character name="habitat" value="bottomland woods" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="creeks" />
        <character name="habitat" value="streamsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Ill., Ind., Iowa, Kans., Ky., Md., Minn., Mo., Ohio, Okla., Pa., Tenn., Tex., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Purple- or violet-rocket</other_name>
  
</bio:treatment>