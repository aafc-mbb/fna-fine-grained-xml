<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">605</other_info_on_meta>
    <other_info_on_meta type="treatment_page">606</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="Rollins" date="1979" rank="genus">dimorphocarpa</taxon_name>
    <taxon_name authority="(Engelmann) Rollins" date="unknown" rank="species">wislizeni</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Bussey Inst. Harvard Univ.</publication_title>
      <place_in_publication>1979: 24. 1979</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus dimorphocarpa;species wislizeni</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250095158</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dithyrea</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">wislizeni</taxon_name>
    <place_of_publication>
      <publication_title>in F. A. Wislizenius, Mem. Tour N. Mexico,</publication_title>
      <place_in_publication>95. 1848</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Dithyrea;species wislizeni;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Biscutella</taxon_name>
    <taxon_name authority="(Engelmann) Bentham &amp; J. D. Hooker ex W. H. Brewer &amp; S.&#xA;            Watson" date="unknown" rank="species">wislizeni</taxon_name>
    <taxon_hierarchy>genus Biscutella;species wislizeni;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dithyrea</taxon_name>
    <taxon_name authority="Wooton &amp; Standley" date="unknown" rank="species">griffithsii</taxon_name>
    <taxon_hierarchy>genus Dithyrea;species griffithsii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dithyrea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">wislizeni</taxon_name>
    <taxon_name authority="(Wooton &amp; Standley) Payson" date="unknown" rank="variety">griffithsii</taxon_name>
    <taxon_hierarchy>genus Dithyrea;species wislizeni;variety griffithsii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o41177" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched or branched basally, branched distally, (1–) 2–6 (–8) dm.</text>
      <biological_entity id="o41178" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="8" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: petiole 1–4 (–5) cm;</text>
      <biological_entity constraint="basal" id="o41179" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o41180" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade lanceolate to linear-lanceolate, (2–) 3–7 (–10) cm × 4–15 (–20) mm, base cuneate to attenuate, margins pinnately lobed to coarsely dentate.</text>
      <biological_entity constraint="basal" id="o41181" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o41182" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="linear-lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41183" name="base" name_original="base" src="d0_s3" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s3" to="attenuate" />
      </biological_entity>
      <biological_entity id="o41184" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="pinnately lobed" name="shape" src="d0_s3" to="coarsely dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves: (proximalmost) petiole 1–4 (–5) cm, (distalmost) shortly petiolate or subsessile;</text>
      <biological_entity constraint="cauline" id="o41185" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o41186" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="4" to_unit="cm" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear to narrowly lanceolate, base cuneate, margins usually entire, rarely dentate, or repand.</text>
      <biological_entity constraint="cauline" id="o41187" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o41188" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o41189" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels divaricate to slightly reflexed, (6–) 8–14 (–22) mm.</text>
      <biological_entity id="o41190" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="22" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s6" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41191" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <relation from="o41190" id="r2784" name="fruiting" negation="false" src="d0_s6" to="o41191" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals (2.5–) 3–4 × 1–1.5 mm, pubescent abaxially;</text>
      <biological_entity id="o41192" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o41193" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" src="d0_s7" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white or lavender, 4–7 (–8) × (2.5–) 3–4 (–5) mm, attenuate to claw, claw 1–1.5 mm, expanded basally;</text>
      <biological_entity id="o41194" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o41195" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="lavender" value_original="lavender" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_width" src="d0_s8" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
        <character constraint="to claw" constraintid="o41196" is_modifier="false" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o41196" name="claw" name_original="claw" src="d0_s8" type="structure" />
      <biological_entity id="o41197" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="basally" name="size" src="d0_s8" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments white, 2–3.5 mm;</text>
      <biological_entity id="o41198" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o41199" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 0.7–1 mm.</text>
      <biological_entity id="o41200" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o41201" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits: each valve usually transversely ovoid-oblong, rarely suborbicular, 4–5.5 (–6) × (4–) 5–7 (–7.5) mm (often longer than wide), base slightly rounded, apex truncate, with or without narrow wing beyond indurated part surrounding seeds, glabrous or pubescent;</text>
      <biological_entity id="o41202" name="fruit" name_original="fruits" src="d0_s11" type="structure" />
      <biological_entity id="o41203" name="valve" name_original="valve" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually transversely" name="shape" src="d0_s11" value="ovoid-oblong" value_original="ovoid-oblong" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s11" value="suborbicular" value_original="suborbicular" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s11" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s11" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s11" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41204" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o41205" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o41206" name="wing" name_original="wing" src="d0_s11" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s11" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o41207" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s11" value="surrounding" value_original="surrounding" />
      </biological_entity>
      <relation from="o41205" id="r2785" name="with or without" negation="false" src="d0_s11" to="o41206" />
    </statement>
    <statement id="d0_s12">
      <text>style 0.5–1 (–1.2) mm.</text>
      <biological_entity id="o41208" name="fruit" name_original="fruits" src="d0_s12" type="structure" />
      <biological_entity id="o41209" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds suborbicular-ovoid, 2–3 × 1.5–2 mm. 2n = 18.</text>
      <biological_entity id="o41210" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="suborbicular-ovoid" value_original="suborbicular-ovoid" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o41211" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy roadsides, sandstone knolls, sand hills and dunes, sandy streambeds and dry washes, desert flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy roadsides" />
        <character name="habitat" value="sandstone knolls" />
        <character name="habitat" value="sand hills" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="sandy streambeds" />
        <character name="habitat" value="dry washes" />
        <character name="habitat" value="desert flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Nev., N.Mex., Tex., Utah; Mexico (Chihuahua, Coahuila).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="past_name">wislizenii</other_name>
  
</bio:treatment>