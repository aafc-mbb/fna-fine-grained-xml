<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">88</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">120</other_info_on_meta>
    <other_info_on_meta type="treatment_page">122</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="Barratt ex Hooker" date="1838" rank="section">cordatae</taxon_name>
    <taxon_name authority="Raup" date="1936" rank="species">turnorii</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>17: 234, plate 193. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section cordatae;species turnorii</taxon_hierarchy>
    <other_info_on_name type="fna_id">242445885</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">lutea</taxon_name>
    <taxon_name authority="(Raup) B. Boivin" date="unknown" rank="variety">turnorii</taxon_name>
    <taxon_hierarchy>genus Salix;species lutea;variety turnorii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 1–2.5 m, (forming clones by layering).</text>
      <biological_entity id="o42331" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="2.5" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches yellowbrown or yellow-gray, not or weakly glaucous, (with sparkling wax crystals, dull or slightly glossy), pilose or villous;</text>
      <biological_entity id="o42332" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o42333" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow-gray" value_original="yellow-gray" />
        <character is_modifier="false" modifier="not; weakly" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="punct" value_original="punct" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branchlets gray-brown or redbrown, pubescent, villous, or velvety.</text>
      <biological_entity id="o42334" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o42335" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="velvety" value_original="velvety" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="velvety" value_original="velvety" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules foliaceous, apex acute or convex;</text>
      <biological_entity id="o42336" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o42337" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o42338" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole shallowly grooved adaxially, 4–13 mm, villous or pubescent adaxially;</text>
      <biological_entity id="o42339" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o42340" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shallowly; adaxially" name="architecture" src="d0_s4" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="13" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>largest medial blade (amphistomatous), narrowly oblong, narrowly elliptic, elliptic, oblanceolate, or lanceolate, 26–47 × 7.5–15 mm, 2.8–4.1 times as long as wide, base convex, rounded, or subcordate, margins slightly revolute or flat, serrate or serrulate, apex acuminate to acute, abaxial surface glaucous, glabrous, pilose, villous, or long-silky, hairs straight or wavy, adaxial dull, sparsely or moderately densely pilose or long-silky, especially on midrib;</text>
      <biological_entity id="o42341" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="26" from_unit="mm" name="length" src="d0_s5" to="47" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="2.8-4.1" value_original="2.8-4.1" />
      </biological_entity>
      <biological_entity constraint="largest medial" id="o42342" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <biological_entity id="o42343" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o42344" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o42345" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o42346" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="long-silky" value_original="long-silky" />
      </biological_entity>
      <biological_entity id="o42347" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o42348" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="sparsely; moderately densely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="long-silky" value_original="long-silky" />
      </biological_entity>
      <biological_entity id="o42349" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
      <relation from="o42348" id="r2850" modifier="especially" name="on" negation="false" src="d0_s5" to="o42349" />
    </statement>
    <statement id="d0_s6">
      <text>proximal blade margins entire or serrulate;</text>
      <biological_entity id="o42350" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="blade" id="o42351" name="margin" name_original="margins" src="d0_s6" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>juvenile blade reddish or yellowish green, sparsely to moderately densely long-silky or pubescent abaxially, hairs white.</text>
      <biological_entity id="o42352" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o42353" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="sparsely to moderately; moderately densely" name="pubescence" src="d0_s7" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o42354" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Catkins flowering just before or as leaves emerge;</text>
      <biological_entity id="o42356" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>staminate stout, 16–30 × 8–10 mm, flowering branchlet 1.5–4 mm;</text>
      <biological_entity id="o42355" name="catkin" name_original="catkins" src="d0_s8" type="structure">
        <character constraint="before leaves" constraintid="o42356" is_modifier="false" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate loosely flowered, stout, 18–22 × 9–11 mm, flowering branchlet 3–4 mm;</text>
      <biological_entity id="o42357" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o42358" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="loosely" name="architecture" src="d0_s10" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s10" value="stout" value_original="stout" />
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract brown or tawny, 1.2–1.6 mm, apex acute or rounded, abaxially hairy throughout or proximally, hairs straight or wavy.</text>
      <biological_entity constraint="floral" id="o42359" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="tawny" value_original="tawny" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o42360" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abaxially; throughout; throughout; proximally" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o42361" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s11" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: adaxial nectary narrowly oblong or ovate, 0.7–1.1 mm;</text>
      <biological_entity id="o42362" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o42363" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct or connate, hairy basally;</text>
      <biological_entity id="o42364" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o42365" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers yellow, 0.6–0.8 mm.</text>
      <biological_entity id="o42366" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o42367" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: adaxial nectary oblong, flask-shaped, or narrowly ovate, 0.4–1 mm, shorter than stipe;</text>
      <biological_entity id="o42368" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o42369" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="flask--shaped" value_original="flask--shaped" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="flask--shaped" value_original="flask--shaped" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="1" to_unit="mm" />
        <character constraint="than stipe" constraintid="o42370" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o42370" name="stipe" name_original="stipe" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>stipe 2–4 mm;</text>
      <biological_entity id="o42371" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o42372" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary pyriform, glabrous, beaks slightly bulged below styles;</text>
      <biological_entity id="o42373" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o42374" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o42375" name="beak" name_original="beaks" src="d0_s17" type="structure" />
      <biological_entity id="o42376" name="style" name_original="styles" src="d0_s17" type="structure" />
      <relation from="o42375" id="r2851" name="bulged" negation="false" src="d0_s17" to="o42376" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 14–18 per ovary;</text>
      <biological_entity id="o42377" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o42378" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o42379" from="14" name="quantity" src="d0_s18" to="18" />
      </biological_entity>
      <biological_entity id="o42379" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles 0.3–0.5 mm;</text>
      <biological_entity id="o42380" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o42381" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s19" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas flat, abaxially non-papillate with rounded or pointed tip, 0.16–0.23–0.28 mm.</text>
      <biological_entity id="o42382" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o42383" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o42384" is_modifier="false" modifier="abaxially" name="relief" src="d0_s20" value="non-papillate" value_original="non-papillate" />
        <character char_type="range_value" from="0.16" from_unit="mm" name="some_measurement" notes="" src="d0_s20" to="0.23-0.28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o42384" name="tip" name_original="tip" src="d0_s20" type="structure">
        <character is_modifier="true" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s20" value="pointed" value_original="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 2.5–5 mm.</text>
      <biological_entity id="o42385" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s21" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>No flowering time data are available (probably May or Jun).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="probably" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Active sand dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="active sand dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Sask.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>70.</number>
  <other_name type="common_name">Turnor’s willow</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Salix turnorii is known from the Lake Athabasca sand dunes in northwestern Saskatchewan. Salix famelica in the Great Sand Hills, southern Saskatchewan, is very similar morphologically and may have been the source of populations ancestral to S. turnorii that moved into northern Saskatchewan during the warm Holocene Hypsithermal Period (ca. 9000–6000 yrs. B.P.).</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Salix turnorii forms natural hybrids with S. brachycarpa var. psammophila.</discussion>
  
</bio:treatment>