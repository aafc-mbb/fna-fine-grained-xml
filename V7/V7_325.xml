<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">237</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="treatment_page">251</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">alysseae</taxon_name>
    <taxon_name authority="Desvaux" date="1815" rank="genus">AURINIA</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Agric.</publication_title>
      <place_in_publication>3: 162. 1815</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe alysseae;genus AURINIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin aurum, gold, and -inia, colored, alluding to flower</other_info_on_name>
    <other_info_on_name type="fna_id">103199</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials [biennials, subshrubs];</text>
    </statement>
    <statement id="d0_s1">
      <text>(caudex woody);</text>
    </statement>
    <statement id="d0_s2">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s3">
      <text>pubescent, trichomes minutely stalked to sessile, stellate, 6–10-rayed [lepidote].</text>
      <biological_entity id="o32898" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o32899" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character char_type="range_value" from="minutely stalked" name="architecture" src="d0_s3" to="sessile" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s3" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems erect or ascending, often (paniculately) branched distally.</text>
      <biological_entity id="o32900" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="often; distally" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate;</text>
      <biological_entity id="o32901" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal rosulate, petiolate (petioles deeply grooved), blade margins repand, sinuate, dentate, or pinnatifid;</text>
      <biological_entity constraint="basal" id="o32902" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o32903" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="repand" value_original="repand" />
        <character is_modifier="false" name="shape" src="d0_s7" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cauline petiolate, blade (much smaller than basal), margins entire [dentate, sinuate].</text>
      <biological_entity constraint="cauline" id="o32904" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o32905" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity id="o32906" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes (corymbose, several-flowered, buds globose), slightly or considerably elongated in fruit.</text>
      <biological_entity id="o32908" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels divaricate or ascending, slender.</text>
      <biological_entity id="o32907" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o32908" is_modifier="false" modifier="slightly; considerably" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o32909" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o32907" id="r2213" name="fruiting" negation="false" src="d0_s10" to="o32909" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals spreading, ovate, lateral pair not saccate basally;</text>
      <biological_entity id="o32910" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o32911" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s11" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals yellow [white], obovate to spatulate, claw slightly differentiated from blade, (apex emarginate, [2-fid or obtuse]);</text>
      <biological_entity id="o32912" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o32913" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s12" to="spatulate" />
      </biological_entity>
      <biological_entity id="o32914" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character constraint="from blade" constraintid="o32915" is_modifier="false" modifier="slightly" name="variability" src="d0_s12" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o32915" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o32916" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o32917" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments dilated (winged or minutely appendaged) basally;</text>
      <biological_entity id="o32918" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o32919" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate;</text>
      <biological_entity id="o32920" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o32921" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands lateral, 1 on each side of lateral stamen.</text>
      <biological_entity id="o32922" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity constraint="nectar" id="o32923" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character constraint="on side" constraintid="o32924" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o32924" name="side" name_original="side" src="d0_s16" type="structure" />
      <biological_entity constraint="lateral" id="o32925" name="stamen" name_original="stamen" src="d0_s16" type="structure" />
      <relation from="o32924" id="r2214" name="part_of" negation="false" src="d0_s16" to="o32925" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits sessile, ellipsoid to obovoid, or broadly obovate to orbicular [globose, elliptic], not torulose, terete or latiseptate;</text>
      <biological_entity id="o32926" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s17" to="obovoid or broadly obovate" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s17" to="obovoid or broadly obovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each not veined, glabrous [pubescent];</text>
      <biological_entity id="o32927" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s18" value="veined" value_original="veined" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>replum rounded;</text>
      <biological_entity id="o32928" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum usually complete, rarely perforate;</text>
      <biological_entity id="o32929" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s20" value="complete" value_original="complete" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s20" value="perforate" value_original="perforate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 4–8 (–16) per ovary;</text>
      <biological_entity id="o32930" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" from="8" from_inclusive="false" name="atypical_quantity" src="d0_s21" to="16" />
        <character char_type="range_value" constraint="per ovary" constraintid="o32931" from="4" name="quantity" src="d0_s21" to="8" />
      </biological_entity>
      <biological_entity id="o32931" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>stigma capitate, usually 2-lobed, rarely subentire.</text>
      <biological_entity id="o32932" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s22" value="2-lobed" value_original="2-lobed" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s22" value="subentire" value_original="subentire" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds uniseriate or biseriate, flattened, winged [not winged], suborbicular [orbicular or elliptic];</text>
      <biological_entity id="o32933" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s23" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="arrangement" src="d0_s23" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" name="shape" src="d0_s23" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s23" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s23" value="suborbicular" value_original="suborbicular" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>seed-coat not mucilaginous when wetted;</text>
      <biological_entity id="o32934" name="seed-coat" name_original="seed-coat" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s24" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>cotyledons incumbent or oblique.</text>
    </statement>
    <statement id="d0_s26">
      <text>x = 8.</text>
      <biological_entity id="o32935" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s25" value="incumbent" value_original="incumbent" />
        <character is_modifier="false" name="orientation" src="d0_s25" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity constraint="x" id="o32936" name="chromosome" name_original="" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; c, se Europe (Caucasus), sw Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="introduced" />
        <character name="distribution" value="se Europe (Caucasus)" establishment_means="introduced" />
        <character name="distribution" value="sw Asia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <other_name type="common_name">Goldentuft</other_name>
  <other_name type="common_name">rock-alyssum</other_name>
  <discussion>Species 10 (2 in the flora).</discussion>
  <references>
    <reference>Dudley, T. R. 1966. Ornamental madworts (Alyssum) and the correct name of the goldentuft alyssum. Arnoldia (Jamaica Plain) 26: 33–48.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits ellipsoid to obovoid, inflated; seeds 1.5-1.8 mm diam.; wing 0.1-0.3 mm wide.</description>
      <determination>1 Aurinia petraea</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits broadly obovate to orbicular, flattened; seeds 2-3 mm diam.; wing 0.3-1.1 mm wide.</description>
      <determination>2 Aurinia saxatilis</determination>
    </key_statement>
  </key>
</bio:treatment>