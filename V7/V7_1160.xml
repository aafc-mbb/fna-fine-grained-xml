<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="mention_page">257</other_info_on_meta>
    <other_info_on_meta type="mention_page">485</other_info_on_meta>
    <other_info_on_meta type="mention_page">667</other_info_on_meta>
    <other_info_on_meta type="treatment_page">676</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">Thelypodieae</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>55[III,2]: 155. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe Thelypodieae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20867</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, perennials, shrubs, or subshrubs;</text>
      <biological_entity id="o19045" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>eglandular.</text>
      <biological_entity id="o19046" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o19048" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character name="growth_form" value="shrub" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Trichomes usually simple, rarely forked or dendritic [subdendritic], sometimes absent.</text>
      <biological_entity id="o19050" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="forked" value_original="forked" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="dendritic" value_original="dendritic" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves petiolate or sessile;</text>
      <biological_entity constraint="cauline" id="o19051" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade base auriculate or not, margins entire, dentate, or pinnately lobed.</text>
      <biological_entity constraint="blade" id="o19052" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="auriculate" value_original="auriculate" />
        <character name="shape" src="d0_s4" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o19053" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes usually ebracteate, often elongated in fruit.</text>
      <biological_entity id="o19054" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o19055" is_modifier="false" modifier="often" name="length" src="d0_s5" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o19055" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers usually actinomorphic, rarely zygomorphic;</text>
      <biological_entity id="o19056" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s6" value="actinomorphic" value_original="actinomorphic" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s6" value="zygomorphic" value_original="zygomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals erect, ascending, spreading, or reflexed, lateral pair saccate or not basally;</text>
      <biological_entity id="o19057" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="saccate" value_original="saccate" />
        <character name="architecture_or_shape" src="d0_s7" value="not basally" value_original="not basally" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, yellow, orange, pink, lilac, lavender, purple, green, brown, or nearly black, claw present, often distinct;</text>
      <biological_entity id="o19058" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="lilac" value_original="lilac" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s8" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s8" value="black" value_original="black" />
      </biological_entity>
      <biological_entity id="o19059" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments unappendaged, not winged;</text>
      <biological_entity id="o19060" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="unappendaged" value_original="unappendaged" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pollen 3-colpate.</text>
      <biological_entity id="o19061" name="pollen" name_original="pollen" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-colpate" value_original="3-colpate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits usually siliques, rarely silicles, usually dehiscent, unsegmented, usually terete, 4-angled, or latiseptate;</text>
      <biological_entity id="o19062" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="dehiscence" notes="" src="d0_s11" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
      <biological_entity id="o19063" name="silique" name_original="siliques" src="d0_s11" type="structure" />
      <biological_entity id="o19064" name="silicle" name_original="silicles" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>ovules 1–210 [–numerous] per ovary;</text>
      <biological_entity id="o19065" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o19066" from="1" name="quantity" src="d0_s12" to="210" />
      </biological_entity>
      <biological_entity id="o19066" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>style obsolete, distinct, or absent;</text>
      <biological_entity id="o19067" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="obsolete" value_original="obsolete" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma usually entire or 2-lobed (subentire in Sibaropsis, Streptanthella).</text>
      <biological_entity id="o19068" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s14" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds usually biseriate or uniseriate, rarely aseriate;</text>
      <biological_entity id="o19069" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s15" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s15" value="aseriate" value_original="aseriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>cotyledons accumbent or incumbent.</text>
      <biological_entity id="o19070" name="cotyledon" name_original="cotyledons" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="accumbent" value_original="accumbent" />
        <character is_modifier="false" name="arrangement" src="d0_s16" value="incumbent" value_original="incumbent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>dd.</number>
  <discussion>Genera 27, species ca. 215 (14 genera, 105 species in the flora).</discussion>
  
</bio:treatment>