<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">535</other_info_on_meta>
    <other_info_on_meta type="mention_page">536</other_info_on_meta>
    <other_info_on_meta type="mention_page">538</other_info_on_meta>
    <other_info_on_meta type="mention_page">544</other_info_on_meta>
    <other_info_on_meta type="treatment_page">537</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">erysimeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erysimum</taxon_name>
    <taxon_name authority="(Douglas ex Hooker) Greene" date="1891" rank="species">capitatum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Francisc.,</publication_title>
      <place_in_publication>269. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe erysimeae;genus erysimum;species capitatum</taxon_hierarchy>
    <other_info_on_name type="fna_id">242416519</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheiranthus</taxon_name>
    <taxon_name authority="Douglas ex Hooker" date="unknown" rank="species">capitatus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 38. 1829,</place_in_publication>
      <other_info_on_pub>based on C. asper Chamisso &amp; Schlechtendal, Linnaea 1: 14. 1826, not Nuttall 1818</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Cheiranthus;species capitatus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(short-lived).</text>
      <biological_entity id="o4111" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="biennial" value_original="biennial" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Trichomes of leaves 2–4 (–7) -rayed.</text>
      <biological_entity id="o4113" name="trichome" name_original="trichomes" src="d0_s2" type="structure" />
      <biological_entity id="o4114" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <relation from="o4113" id="r296" name="part_of" negation="false" src="d0_s2" to="o4114" />
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, often branched distally, sometimes proximally, (0.5–) 1.2–10 (–12) dm.</text>
      <biological_entity id="o4115" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often; distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.5" from_unit="dm" modifier="sometimes proximally; proximally" name="atypical_some_measurement" src="d0_s3" to="1.2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" modifier="sometimes proximally; proximally" name="atypical_some_measurement" src="d0_s3" to="12" to_unit="dm" />
        <character char_type="range_value" from="1.2" from_unit="dm" modifier="sometimes proximally; proximally" name="some_measurement" src="d0_s3" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves (often withered by flowering);</text>
      <biological_entity constraint="basal" id="o4116" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade spatulate to narrowly oblanceolate or linear, 2–18 (–27) cm × 3–15 (–30) mm, base attenuate, margins entire or dentate to denticulate, apex acute.</text>
      <biological_entity id="o4117" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s5" to="narrowly oblanceolate or linear" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="27" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="18" to_unit="cm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4118" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o4119" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s5" value="dentate to denticulate" value_original="dentate to denticulate" />
      </biological_entity>
      <biological_entity id="o4120" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (distal) sessile;</text>
      <biological_entity constraint="cauline" id="o4121" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade margins entire or denticulate.</text>
      <biological_entity constraint="blade" id="o4122" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes considerably elongated in fruit.</text>
      <biological_entity id="o4124" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate to ascending, stout or slender, narrower than fruit, 4–17 (–25) mm.</text>
      <biological_entity id="o4123" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o4124" is_modifier="false" modifier="considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character constraint="than fruit" constraintid="o4126" is_modifier="false" name="width" src="d0_s9" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o4125" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o4126" name="fruit" name_original="fruit" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="size" src="d0_s9" value="stout" value_original="stout" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="17" to_unit="mm" />
      </biological_entity>
      <relation from="o4123" id="r297" name="fruiting" negation="false" src="d0_s9" to="o4125" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals narrowly oblong, 7–14 mm, lateral pair saccate basally;</text>
      <biological_entity id="o4127" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o4128" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="14" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals usually orange to yellow, rarely lavender or purplish, suborbicular to obovate, 12–25 (–30) × (5–) 6–10 (–13) mm, claw 8–16 mm, apex rounded;</text>
      <biological_entity id="o4129" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4130" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character char_type="range_value" from="usually orange" name="coloration" src="d0_s11" to="yellow rarely lavender or purplish" />
        <character char_type="range_value" from="usually orange" name="coloration" src="d0_s11" to="yellow rarely lavender or purplish" />
        <character char_type="range_value" from="suborbicular" name="shape" src="d0_s11" to="obovate" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="30" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s11" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s11" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="13" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4131" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4132" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>median filaments 9–18 mm;</text>
      <biological_entity id="o4133" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity constraint="median" id="o4134" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="18" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers linear, 3–4 mm.</text>
      <biological_entity id="o4135" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o4136" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits divaricate or ascending to erect, narrowly linear, straight or curved upward, not torulose, 3.5–11 (–15) cm × 1.3–3.3 mm, 4-angled to latiseptate, not striped;</text>
      <biological_entity id="o4137" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="divaricate" value_original="divaricate" />
        <character name="arrangement" src="d0_s14" value="ascending to erect" value_original="ascending to erect" />
        <character is_modifier="false" modifier="narrowly" name="course" src="d0_s14" value="linear" value_original="linear" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s14" to="15" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s14" to="11" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s14" to="3.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="latiseptate" value_original="latiseptate" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s14" value="striped" value_original="striped" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves with prominent midvein, pubescent outside, trichomes 2–5-rayed, glabrous inside;</text>
      <biological_entity id="o4138" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o4139" name="midvein" name_original="midvein" src="d0_s15" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s15" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o4140" name="trichome" name_original="trichomes" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o4138" id="r298" name="with" negation="false" src="d0_s15" to="o4139" />
    </statement>
    <statement id="d0_s16">
      <text>ovules (40–) 54–82 per ovary;</text>
      <biological_entity id="o4141" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" from="40" name="atypical_quantity" src="d0_s16" to="54" to_inclusive="false" />
        <character char_type="range_value" constraint="per ovary" constraintid="o4142" from="54" name="quantity" src="d0_s16" to="82" />
      </biological_entity>
      <biological_entity id="o4142" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style cylindrical, usually stout, rarely slender, 0.2–2.5 (–3) mm, sparsely pubescent;</text>
      <biological_entity id="o4143" name="style" name_original="style" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" modifier="usually" name="fragility_or_size" src="d0_s17" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="rarely" name="size" src="d0_s17" value="slender" value_original="slender" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma 2-lobed, lobes as long as wide.</text>
      <biological_entity id="o4144" name="stigma" name_original="stigma" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o4145" name="lobe" name_original="lobes" src="d0_s18" type="structure">
        <character is_modifier="false" name="width" src="d0_s18" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds oblong, 1.5–4 × 1–2 mm;</text>
    </statement>
    <statement id="d0_s20">
      <text>winged apically or not winged.</text>
    </statement>
    <statement id="d0_s21">
      <text>2n = 36.</text>
      <biological_entity id="o4146" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s19" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s19" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="apically" name="architecture" src="d0_s20" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="not; not" name="architecture" src="d0_s20" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4147" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon; Alaska, Ariz., Ark., Calif., Colo., Idaho, Ill., Ind., Iowa, Mo., Mont., N.Mex., Nev., Ohio, Okla., Oreg., Tenn., Tex., Utah, Wash., Wyo.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Erysimum capitatum is extremely widespread, ecologically diverse, morphologically highly variable, and nomenclaturally complex. It has been divided into infraspecific taxa; G. B. Rossbach (1958) recognized three varieties, R. A. Price (1987) eight subspecies, R. C. Rollins (1993) five varieties, Price (1993) four subspecies, and N. H. Holmgren (2005b) three varieties. The majority of these taxa were based on highly variable characters with considerable overlap, including width, margin, shape, and apex of basal leaves, fruit orientation, and development of elongated versus short caudices. Furthermore, there is a substantial degree of disagreement among these authors as to the limits of a given infraspecific taxon, its distribution, and synonymies involved. Except for two distinctive species that were previously treated as infraspecific taxa (arenicola, perenne) the vast majority of the remaining variation in E. capitatum is divided herein into two varieties.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Seeds winged (at least distally, rarely wing rudimentary), 2-4 × (0.8-)1-2 mm; fruits latiseptate, rarely 4-angled; petals usually orange, sometimes orange-yellow or yellow; adaxial surfaces of basal and proximalmost cauline leaf blades with mostly 3(-7)-rayed trichomes.</description>
      <determination>4a Erysimum capitatum var. capitatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Seeds not winged, 1.5-2(-2.4) × 0.7-1.2 mm; fruits 4-angled, rarely latiseptate; petals yellow, rarely lavender or purplish; adaxial surfaces of basal and proximalmost cauline leaf blades with mostly 2- or 3-rayed trichomes.</description>
      <determination>4b Erysimum capitatum var. purshii</determination>
    </key_statement>
  </key>
</bio:treatment>