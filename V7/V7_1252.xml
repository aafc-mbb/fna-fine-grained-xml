<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">703</other_info_on_meta>
    <other_info_on_meta type="treatment_page">719</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="Rollins" date="1946" rank="species">oliganthus</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Dudley Herb.</publication_title>
      <place_in_publication>3: 372. 1946</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species oliganthus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250095006</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Streptanthus</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">cordatus</taxon_name>
    <taxon_name authority="Jepson" date="unknown" rank="variety">exiguus</taxon_name>
    <taxon_hierarchy>genus Streptanthus;species cordatus;variety exiguus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(caudex simple or few-branched);</text>
    </statement>
    <statement id="d0_s2">
      <text>(glaucous), usually glabrous, (petioles of basal leaves pubescent).</text>
      <biological_entity id="o1863" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems usually unbranched, rarely branched, 1.5–4 (–5) dm.</text>
      <biological_entity id="o1864" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="5" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s3" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves rosulate;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate (petioles usually not or rarely narrowly winged, setose-ciliate);</text>
      <biological_entity constraint="basal" id="o1865" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade narrowly oblanceolate to lanceolate, 4–10 cm, margins entire.</text>
      <biological_entity id="o1866" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s6" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1867" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves: blade oblong-lanceolate, 2.5–8 cm × 5–16 (–25) mm, (smaller distally), base auriculate, margins entire, (apex obtuse to acute).</text>
      <biological_entity constraint="cauline" id="o1868" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o1869" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s7" to="8" to_unit="cm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1870" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o1871" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes ebracteate, (lax).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate-ascending, (straight), 3–10 mm.</text>
      <biological_entity id="o1872" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1873" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o1872" id="r173" name="fruiting" negation="false" src="d0_s9" to="o1873" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx campanulate;</text>
      <biological_entity id="o1874" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1875" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals purple, (broadly oblong), 5–8 mm, not keeled;</text>
      <biological_entity id="o1876" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1877" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals maroon-purple (claw purplish), 9–12 mm, blade 2–4 × 0.7–1 mm, margins not crisped, claw 6–8 mm, wider than blade;</text>
      <biological_entity id="o1878" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1879" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s12" value="maroon-purple" value_original="maroon-purple" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1880" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1881" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o1882" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character constraint="than blade" constraintid="o1883" is_modifier="false" name="width" src="d0_s12" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o1883" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens in 3 unequal pairs;</text>
      <biological_entity id="o1884" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o1885" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o1886" name="pair" name_original="pairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="3" value_original="3" />
        <character is_modifier="true" name="size" src="d0_s13" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o1885" id="r174" name="in" negation="false" src="d0_s13" to="o1886" />
    </statement>
    <statement id="d0_s14">
      <text>filaments (distinct): abaxial pair 5–7 mm, lateral pair 3–5 mm, adaxial pair 7–9 mm;</text>
      <biological_entity id="o1887" name="filament" name_original="filaments" src="d0_s14" type="structure" />
      <biological_entity constraint="abaxial" id="o1888" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1889" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers (all) fertile, 2.5–4 mm;</text>
      <biological_entity id="o1890" name="filament" name_original="filaments" src="d0_s15" type="structure" />
      <biological_entity id="o1891" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>gynophore 0.5–1 mm.</text>
      <biological_entity id="o1892" name="filament" name_original="filaments" src="d0_s16" type="structure" />
      <biological_entity id="o1893" name="gynophore" name_original="gynophore" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits ascending to divaricate-ascending, smooth, straight, flattened, 4.5–9.7 (–10.5) cm × 2–2.7 (–3) mm;</text>
      <biological_entity id="o1894" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s17" to="divaricate-ascending" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="9.7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s17" to="10.5" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s17" to="9.7" to_unit="cm" />
        <character char_type="range_value" from="2.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s17" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s17" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each with prominent midvein;</text>
      <biological_entity id="o1895" name="valve" name_original="valves" src="d0_s18" type="structure" />
      <biological_entity id="o1896" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o1895" id="r175" name="with" negation="false" src="d0_s18" to="o1896" />
    </statement>
    <statement id="d0_s19">
      <text>replum straight;</text>
      <biological_entity id="o1897" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules (42–) 48–60 per ovary;</text>
      <biological_entity id="o1898" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" from="42" name="atypical_quantity" src="d0_s20" to="48" to_inclusive="false" />
        <character char_type="range_value" constraint="per ovary" constraintid="o1899" from="48" name="quantity" src="d0_s20" to="60" />
      </biological_entity>
      <biological_entity id="o1899" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 0.1–1 mm;</text>
      <biological_entity id="o1900" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s21" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma entire.</text>
      <biological_entity id="o1901" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds oblong, 2–2.5 (–2.7) × 1.5–2 mm;</text>
      <biological_entity id="o1902" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s23" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s23" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s23" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>wing 0.2–0.4 mm wide, continuous.</text>
      <biological_entity id="o1903" name="wing" name_original="wing" src="d0_s24" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s24" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="continuous" value_original="continuous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry open pinyon woodland, pine forest, rocky subalpine forests, sagebrush</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry open pinyon" />
        <character name="habitat" value="pine forest" />
        <character name="habitat" value="rocky subalpine forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2000-3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <discussion>Streptanthus oliganthus is known in California from Mono County and in Nevada from Esmeralda, Lyon, and Mineral counties. N. H. Holmgren (2005b) reported it as disjunct in Nye County (Nevada).</discussion>
  <discussion>Streptanthus oliganthus is related to S. cordatus, from which it is readily distinguished by having basal leaves with margins entire versus dentate, petioles usually not winged, rarely narrowly so, versus broadly winged, stigmas entire versus slightly to strongly 2-lobed, ovules (41–)48–60 versus 20–38(–44) per ovary, fruits 2–2.7(–3) versus (2.5–)3–6(–7) mm wide, and seeds narrower (2–2.5(–2.7) × 1.5–2 versus 2.5–5 × 2.2–5 mm).</discussion>
  
</bio:treatment>