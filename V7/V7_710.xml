<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">467</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cardamine</taxon_name>
    <taxon_name authority="Hooker" date="1829" rank="species">angulata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 44. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus cardamine;species angulata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094653</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="Rafinesque" date="unknown" rank="species">angulata</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="variety">alba</taxon_name>
    <taxon_hierarchy>genus Cardamine;species angulata;variety alba;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">angulata</taxon_name>
    <taxon_name authority="O. E. Schulz" date="unknown" rank="variety">hirsuta</taxon_name>
    <taxon_hierarchy>genus Cardamine;species angulata;variety hirsuta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">angulata</taxon_name>
    <taxon_name authority="O. E. Schulz" date="unknown" rank="variety">pentaphylla</taxon_name>
    <taxon_hierarchy>genus Cardamine;species angulata;variety pentaphylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dentaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">grandiflora</taxon_name>
    <taxon_hierarchy>genus Dentaria;species grandiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>glabrous or sparsely pubescent.</text>
      <biological_entity id="o881" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rhizomes cylindrical, slender, to 2 mm diam.</text>
      <biological_entity id="o882" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, unbranched, (1.5–) 2.5–8.5 (–10) dm, sparsely to densely hirsute at base.</text>
      <biological_entity id="o883" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="2.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="10" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s3" to="8.5" to_unit="dm" />
        <character constraint="at base" constraintid="o884" is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o884" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Rhizomal leaves 3 (or 5) -foliolate, (4–) 7–20 (–22) cm, leaflets petiolulate or subsessile;</text>
      <biological_entity constraint="rhizomal" id="o885" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="3-foliolate" value_original="3-foliolate" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="22" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s4" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o886" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole (2–) 4–12 (–14) cm;</text>
      <biological_entity id="o887" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="14" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s5" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral leaflets subsessile, blade similar to terminal, larger or smaller in size;</text>
      <biological_entity constraint="lateral" id="o888" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o889" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="size" src="d0_s6" value="larger" value_original="larger" />
        <character is_modifier="false" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>terminal leaflet petiolulate [(0.3–) 0.5–1.5 cm], blade ovate to broadly lanceolate, 1.5–7 (–9) cm, base usually cuneate, rarely subreniform or obtuse, margins 3–5 (–7) -lobed or toothed, surfaces puberulent.</text>
      <biological_entity constraint="terminal" id="o890" name="leaflet" name_original="leaflet" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolulate" value_original="petiolulate" />
      </biological_entity>
      <biological_entity id="o891" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="broadly lanceolate" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="9" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s7" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o892" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="subreniform" value_original="subreniform" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o893" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="3-5(-7)-lobed" value_original="3-5(-7)-lobed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o894" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cauline leaves (3 or) 4–8, 3 (or 5) -foliolate, petiolate, leaflets petiolulate or sessile;</text>
      <biological_entity constraint="cauline" id="o895" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s8" to="8" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="3-foliolate" value_original="3-foliolate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o896" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petiole 1–4 cm, base not auriculate;</text>
      <biological_entity id="o897" name="petiole" name_original="petiole" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s9" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o898" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflets sessile, blade similar to terminal, smaller, margins usually dentate, rarely entire;</text>
      <biological_entity constraint="lateral" id="o899" name="leaflet" name_original="leaflets" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o900" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" notes="" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o901" name="margin" name_original="margins" src="d0_s10" type="structure" constraint="terminal">
        <character is_modifier="true" name="size" src="d0_s10" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s10" value="dentate" value_original="dentate" />
      </biological_entity>
      <relation from="o900" id="r68" name="to" negation="false" src="d0_s10" to="o901" />
    </statement>
    <statement id="d0_s11">
      <text>terminal leaflet sessile or petiolulate, blade broadly ovate to narrowly lanceolate, 2–7 cm × 6–40 mm, margins minutely pubescent.</text>
      <biological_entity constraint="terminal" id="o902" name="leaflet" name_original="leaflet" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="petiolulate" value_original="petiolulate" />
      </biological_entity>
      <biological_entity id="o903" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s11" to="narrowly lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s11" to="7" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s11" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o904" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Racemes ebracteate.</text>
    </statement>
    <statement id="d0_s13">
      <text>Fruiting pedicels ascending to divaricate, (9–) 12–25 mm.</text>
      <biological_entity id="o905" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s13" value="divaricate" value_original="divaricate" />
        <character char_type="range_value" from="9" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s13" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o906" name="pedicel" name_original="pedicels" src="d0_s13" type="structure" />
      <relation from="o905" id="r69" name="fruiting" negation="false" src="d0_s13" to="o906" />
    </statement>
    <statement id="d0_s14">
      <text>Flowers: sepals oblong, 2.5–4 × 1.3–2 mm, lateral pair saccate basally;</text>
      <biological_entity id="o907" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o908" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s14" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals usually white, rarely pinkish, obovate, 8–15 × 4–8 mm (clawed, apex rounded or emarginate);</text>
      <biological_entity id="o909" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o910" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s15" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s15" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s15" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments: median pairs 3.5–6 mm, lateral pair 2–3.5 mm;</text>
      <biological_entity id="o911" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="median" value_original="median" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers oblong, 0.8–1.2 mm.</text>
      <biological_entity id="o912" name="filament" name_original="filaments" src="d0_s17" type="structure" />
      <biological_entity id="o913" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s17" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits linear, 1.5–3.2 cm × 1.4–2 mm;</text>
      <biological_entity id="o914" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s18" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s18" to="3.2" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s18" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 10–16 per ovary;</text>
      <biological_entity id="o915" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o916" from="10" name="quantity" src="d0_s19" to="16" />
      </biological_entity>
      <biological_entity id="o916" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style (0.5–) 1–4 mm.</text>
      <biological_entity id="o917" name="style" name_original="style" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s20" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s20" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds dark-brown, oblong, 1.8–2.3 × 1–1.2 mm. 2n = 40.</text>
      <biological_entity id="o918" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s21" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s21" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s21" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o919" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist ground, stream banks, swampy or damp woods, thickets, wet meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream banks" modifier="moist ground" />
        <character name="habitat" value="swampy" />
        <character name="habitat" value="damp woods" />
        <character name="habitat" value="thickets" />
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>