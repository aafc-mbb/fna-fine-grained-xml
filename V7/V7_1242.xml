<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">702</other_info_on_meta>
    <other_info_on_meta type="mention_page">716</other_info_on_meta>
    <other_info_on_meta type="treatment_page">715</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="A. Gray" date="1864" rank="species">hispidus</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci.</publication_title>
      <place_in_publication>3: 101. 1864</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species hispidus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250095007</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euklisia</taxon_name>
    <taxon_name authority="(A. Gray) Greene" date="unknown" rank="species">hispida</taxon_name>
    <taxon_hierarchy>genus Euklisia;species hispida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely hirsute-hispid throughout, (trichomes to 1.4 mm).</text>
      <biological_entity id="o1455" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely; throughout" name="pubescence" src="d0_s1" value="hirsute-hispid" value_original="hirsute-hispid" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unbranched or branched basally, 0.3–3 dm.</text>
      <biological_entity id="o1456" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s2" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves not rosulate;</text>
    </statement>
    <statement id="d0_s4">
      <text>shortly petiolate;</text>
      <biological_entity constraint="basal" id="o1457" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade obovate, 1–5 cm, margins coarsely to shallowly dentate.</text>
      <biological_entity id="o1458" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1459" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="coarsely to shallowly" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: blade obovate to oblong, 0.7–6 cm × 2–25 mm, base cuneate or truncate, not auriculate or (distally) minutely ariculate, margins coarsely dentate.</text>
      <biological_entity constraint="cauline" id="o1460" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o1461" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s6" to="oblong" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s6" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1462" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="auriculate" value_original="auriculate" />
        <character name="shape" src="d0_s6" value="minutely" value_original="minutely" />
      </biological_entity>
      <biological_entity id="o1463" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes ebracteate, (not secund, with a terminal cluster of sterile flowers).</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels divaricate-ascending, (straight), 2–5 mm.</text>
      <biological_entity id="o1464" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1465" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o1464" id="r109" name="fruiting" negation="false" src="d0_s8" to="o1465" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx subcampanulate;</text>
      <biological_entity id="o1466" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o1467" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="subcampanulate" value_original="subcampanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals pale green to purplish, ovate, 4–6 mm, not keeled;</text>
      <biological_entity id="o1468" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o1469" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s10" to="purplish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals light purple (with white margins), 6–9 mm, blade 2–3 × 1–1.5 mm, margins crisped, claw 5–6 mm, wider than blade;</text>
      <biological_entity id="o1470" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o1471" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="light purple" value_original="light purple" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1472" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1473" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o1474" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character constraint="than blade" constraintid="o1475" is_modifier="false" name="width" src="d0_s11" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o1475" name="blade" name_original="blade" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stamens in 3 unequal pairs;</text>
      <biological_entity id="o1476" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o1477" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o1478" name="pair" name_original="pairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="true" name="size" src="d0_s12" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o1477" id="r110" name="in" negation="false" src="d0_s12" to="o1478" />
    </statement>
    <statement id="d0_s13">
      <text>filaments: abaxial pair (connate ca. 1/2 their length), 4–5 mm, lateral pair 3–4 mm, adaxial pair (exserted, connate to near apex), 5–6 mm;</text>
      <biological_entity id="o1479" name="filament" name_original="filaments" src="d0_s13" type="structure" />
      <biological_entity constraint="abaxial" id="o1480" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s13" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1481" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers: abaxial and adaxial pairs fertile, 1.5–1.8mm, adaxial pair sterile, 0.3–0.5 mm;</text>
      <biological_entity id="o1482" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="abaxial" value_original="abaxial" />
        <character is_modifier="false" name="position" src="d0_s14" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1483" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>gynophore 0.1–0.3 mm.</text>
      <biological_entity id="o1484" name="anther" name_original="anthers" src="d0_s15" type="structure" />
      <biological_entity id="o1485" name="gynophore" name_original="gynophore" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s15" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits divaricate-ascending to suberect, straight, flattened, 4–8.5 cm × 2–2.5 mm;</text>
      <biological_entity id="o1486" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="divaricate-ascending" name="orientation" src="d0_s16" to="suberect" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s16" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves each with prominent midvein;</text>
      <biological_entity id="o1487" name="valve" name_original="valves" src="d0_s17" type="structure" />
      <biological_entity id="o1488" name="midvein" name_original="midvein" src="d0_s17" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s17" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o1487" id="r111" name="with" negation="false" src="d0_s17" to="o1488" />
    </statement>
    <statement id="d0_s18">
      <text>replum straight;</text>
      <biological_entity id="o1489" name="replum" name_original="replum" src="d0_s18" type="structure">
        <character is_modifier="false" name="course" src="d0_s18" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 34–66 per ovary;</text>
      <biological_entity id="o1490" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o1491" from="34" name="quantity" src="d0_s19" to="66" />
      </biological_entity>
      <biological_entity id="o1491" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style 0.4–1 mm;</text>
      <biological_entity id="o1492" name="style" name_original="style" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s20" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigma slightly 2-lobed.</text>
      <biological_entity id="o1493" name="stigma" name_original="stigma" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s21" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds ovoid to suborbicular, 1.6–2 × 1.2–1.8 mm;</text>
      <biological_entity id="o1494" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s22" to="suborbicular" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s22" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s22" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>wing 0.2–0.35 mm wide, continuous.</text>
    </statement>
    <statement id="d0_s24">
      <text>2n = 28.</text>
      <biological_entity id="o1495" name="wing" name_original="wing" src="d0_s23" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s23" to="0.35" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s23" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1496" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Talus or rocky outcrops (Franciscan formation, largely on chert) and sparsely vegetated openings in grassland or chaparral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="talus" />
        <character name="habitat" value="rocky outcrops" />
        <character name="habitat" value="franciscan formation" />
        <character name="habitat" value="chert" modifier="largely" />
        <character name="habitat" value="vegetated openings" modifier="sparsely" constraint="in grassland or chaparral" />
        <character name="habitat" value="grassland" />
        <character name="habitat" value="chaparral" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Streptanthus hispidus is known from Mt. Diablo in Contra Costa County.</discussion>
  
</bio:treatment>