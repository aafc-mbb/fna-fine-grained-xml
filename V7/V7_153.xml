<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">46</other_info_on_meta>
    <other_info_on_meta type="mention_page">99</other_info_on_meta>
    <other_info_on_meta type="treatment_page">141</other_info_on_meta>
    <other_info_on_meta type="illustration_page">137</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="Seringe" date="1824" rank="section">arbuscella</taxon_name>
    <taxon_name authority="Andersson" date="1867" rank="species">arbusculoides</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. Salicum,</publication_title>
      <place_in_publication>147, plate 8, fig. 81. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section arbuscella;species arbusculoides</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445637</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: branches gray-brown to redbrown, not glaucous, glabrous;</text>
      <biological_entity id="o6957" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o6958" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character char_type="range_value" from="gray-brown" name="coloration" src="d0_s0" to="redbrown" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branchlets redbrown, glabrous or puberulent.</text>
      <biological_entity id="o6959" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o6960" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules rudimentary on early ones, apex acute;</text>
      <biological_entity id="o6961" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o6962" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character constraint="on ones" constraintid="o6963" is_modifier="false" name="prominence" src="d0_s2" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity id="o6963" name="one" name_original="ones" src="d0_s2" type="structure" />
      <biological_entity id="o6964" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole shallowly grooved adaxially, 3–11 mm, puberulent to glabrescent adaxially;</text>
      <biological_entity id="o6965" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6966" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="shallowly; adaxially" name="architecture" src="d0_s3" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="11" to_unit="mm" />
        <character char_type="range_value" from="puberulent" modifier="adaxially" name="pubescence" src="d0_s3" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>largest medial blade very narrowly elliptic to elliptic, 38–78 × 7–18 mm, base cuneate or convex, margins slightly revolute, serrulate, apex acuminate, acute, or convex, abaxial surface glaucous (sometimes obscured by hairs), sparsely to densely long-silky, hairs (white, sometimes also ferruginous), straight, adaxial highly or slightly glossy, glabrous;</text>
      <biological_entity id="o6967" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="largest medial" id="o6968" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character constraint="elliptic to elliptic base, margins, apex, surface, hairs" constraintid="o6969, o6970, o6971, o6972, o6973" is_modifier="false" modifier="very narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="course" notes="" src="d0_s4" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o6969" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s4" value="elliptic to elliptic" value_original="elliptic to elliptic" />
        <character char_type="range_value" from="38" from_unit="mm" is_modifier="true" name="length" src="d0_s4" to="78" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" is_modifier="true" name="width" src="d0_s4" to="18" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character char_type="range_value" from="glaucous" name="pubescence" src="d0_s4" to="sparsely densely long-silky" />
      </biological_entity>
      <biological_entity id="o6970" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s4" value="elliptic to elliptic" value_original="elliptic to elliptic" />
        <character char_type="range_value" from="38" from_unit="mm" is_modifier="true" name="length" src="d0_s4" to="78" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" is_modifier="true" name="width" src="d0_s4" to="18" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character char_type="range_value" from="glaucous" name="pubescence" src="d0_s4" to="sparsely densely long-silky" />
      </biological_entity>
      <biological_entity id="o6971" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s4" value="elliptic to elliptic" value_original="elliptic to elliptic" />
        <character char_type="range_value" from="38" from_unit="mm" is_modifier="true" name="length" src="d0_s4" to="78" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" is_modifier="true" name="width" src="d0_s4" to="18" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character char_type="range_value" from="glaucous" name="pubescence" src="d0_s4" to="sparsely densely long-silky" />
      </biological_entity>
      <biological_entity id="o6972" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s4" value="elliptic to elliptic" value_original="elliptic to elliptic" />
        <character char_type="range_value" from="38" from_unit="mm" is_modifier="true" name="length" src="d0_s4" to="78" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" is_modifier="true" name="width" src="d0_s4" to="18" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character char_type="range_value" from="glaucous" name="pubescence" src="d0_s4" to="sparsely densely long-silky" />
      </biological_entity>
      <biological_entity id="o6973" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s4" value="elliptic to elliptic" value_original="elliptic to elliptic" />
        <character char_type="range_value" from="38" from_unit="mm" is_modifier="true" name="length" src="d0_s4" to="78" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" is_modifier="true" name="width" src="d0_s4" to="18" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s4" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="convex" value_original="convex" />
        <character char_type="range_value" from="glaucous" name="pubescence" src="d0_s4" to="sparsely densely long-silky" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6974" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o6975" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s4" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal blade margins entire;</text>
      <biological_entity id="o6976" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="blade" id="o6977" name="margin" name_original="margins" src="d0_s5" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>juvenile blade yellowish green, very densely long-silky abaxially, hairs white, sometimes also ferruginous.</text>
      <biological_entity id="o6978" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o6979" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="very densely; abaxially" name="pubescence" src="d0_s6" value="long-silky" value_original="long-silky" />
      </biological_entity>
      <biological_entity id="o6980" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="ferruginous" value_original="ferruginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Catkins flowering as or just before leaves emerge;</text>
      <biological_entity id="o6982" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>staminate stout or slender, 17–43 × 5–10 mm, flowering branchlet 0–2.5 mm;</text>
      <biological_entity id="o6981" name="catkin" name_original="catkins" src="d0_s7" type="structure">
        <character constraint="before leaves" constraintid="o6982" is_modifier="false" name="life_cycle" src="d0_s7" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="size" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pistillate densely to loosely flowered, stout to slender, 20–46 × 6–15 mm, flowering branchlet 0–6 mm;</text>
      <biological_entity id="o6983" name="branchlet" name_original="branchlet" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="stout" value_original="stout" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="pistillate" name="architecture" src="d0_s9" to="densely loosely flowered" />
        <character char_type="range_value" from="stout" name="size" src="d0_s9" to="slender" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s9" to="46" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6984" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>floral bract tawny or brown, 0.8–1.2 mm, apex convex to rounded, abaxially hairy, hairs straight or wavy.</text>
      <biological_entity constraint="floral" id="o6985" name="bract" name_original="bract" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6986" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s10" to="rounded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o6987" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s10" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Staminate flowers: adaxial nectary oblong, 0.6–0.9 mm;</text>
      <biological_entity id="o6988" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6989" name="nectary" name_original="nectary" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments distinct;</text>
      <biological_entity id="o6990" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6991" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers purple turning yellow, ellipsoid to globose, 0.3–0.6 mm.</text>
      <biological_entity id="o6992" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o6993" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple turning yellow" value_original="purple turning yellow" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s13" to="globose" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s13" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pistillate flowers: adaxial nectary oblong or ovate, 0.6–1 mm;</text>
      <biological_entity id="o6994" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6995" name="nectary" name_original="nectary" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovary pyriform, beak gradually tapering to styles;</text>
      <biological_entity id="o6996" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o6997" name="ovary" name_original="ovary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="pyriform" value_original="pyriform" />
      </biological_entity>
      <biological_entity id="o6998" name="beak" name_original="beak" src="d0_s15" type="structure">
        <character constraint="to styles" constraintid="o6999" is_modifier="false" modifier="gradually" name="shape" src="d0_s15" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o6999" name="style" name_original="styles" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>ovules 16–18 per ovary;</text>
      <biological_entity id="o7000" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7001" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o7002" from="16" name="quantity" src="d0_s16" to="18" />
      </biological_entity>
      <biological_entity id="o7002" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>styles 0.3–0.5 mm.</text>
      <biological_entity id="o7003" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o7004" name="style" name_original="styles" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsules 4–6 mm. 2n = 38.</text>
      <biological_entity id="o7005" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s18" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7006" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid May-early Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jul" from="mid May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream margins, lakeshores, openings in white spruce forests, treed bogs, sedge fens, edges of alpine and arctic tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream margins" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="openings" constraint="in white" />
        <character name="habitat" value="forests" modifier="white spruce" />
        <character name="habitat" value="treed bogs" />
        <character name="habitat" value="sedge fens" />
        <character name="habitat" value="edges" constraint="of alpine and arctic tundra" />
        <character name="habitat" value="alpine" />
        <character name="habitat" value="arctic tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>90.</number>
  <other_name type="common_name">Little-tree willow</other_name>
  <discussion>Glands on leaf teeth of Salix arbusculoides are sometimes covered with fine crystals of sulphur, calcium, potassium, and silicon (R. Cooper, pers. comm.), indicating that they can function as hydathodes as well as resin glands.</discussion>
  
</bio:treatment>