<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">280</other_info_on_meta>
    <other_info_on_meta type="mention_page">308</other_info_on_meta>
    <other_info_on_meta type="treatment_page">298</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="(C. L. Hitchcock) Windham &amp; Beilstein" date="2004" rank="species">burkei</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>50: 221. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species burkei</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094749</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="C. L. Hitchcock" date="unknown" rank="species">maguirei</taxon_name>
    <taxon_name authority="C. L. Hitchcock" date="unknown" rank="variety">burkei</taxon_name>
    <place_of_publication>
      <publication_title>Revis. Drabas W. N. Amer.,</publication_title>
      <place_in_publication>72, plate 5, fig. 37c. 1941</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Draba;species maguirei;variety burkei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(cespitose, forming loose mats);</text>
      <biological_entity id="o3407" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (with persistent leaves, branches sometimes terminating in sterile rosettes);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o3408" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, 0.3–0.6 (–0.9) dm, glabrous.</text>
      <biological_entity id="o3409" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.9" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s4" to="0.6" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves subrosulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>subsessile;</text>
      <biological_entity constraint="basal" id="o3410" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="subrosulate" value_original="subrosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade oblanceolate to obovate, 0.3–0.8 (–1.3) cm × 1–2.5 mm, margins entire, (ciliate, trichomes simple, subsetiform, 0.25–0.8 mm), surfaces glabrous.</text>
      <biological_entity id="o3411" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s7" to="obovate" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="1.3" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s7" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3412" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o3413" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cauline leaves 0.</text>
      <biological_entity constraint="cauline" id="o3414" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes 4–10-flowered, ebracteate, elongated in fruit;</text>
      <biological_entity id="o3415" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="4-10-flowered" value_original="4-10-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o3416" is_modifier="false" name="length" src="d0_s9" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o3416" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>rachis not flexuous, glabrous.</text>
    </statement>
    <statement id="d0_s11">
      <text>Fruiting pedicels divaricate-ascending to ascending, straight, 4–9 (–15) mm, glabrous.</text>
      <biological_entity id="o3417" name="rachis" name_original="rachis" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s10" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="divaricate-ascending" name="orientation" src="d0_s11" to="ascending" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="15" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3418" name="pedicel" name_original="pedicels" src="d0_s11" type="structure" />
      <relation from="o3417" id="r260" name="fruiting" negation="false" src="d0_s11" to="o3418" />
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals broadly ovate, 2–3.5 mm, glabrous or sparsely pubescent, (trichomes simple, 0.07–0.35 mm);</text>
      <biological_entity id="o3419" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o3420" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals yellow, oblanceolate, 4–6 × 1.5–2 mm;</text>
      <biological_entity id="o3421" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o3422" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ovate, 0.4–0.5 mm.</text>
      <biological_entity id="o3423" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o3424" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits ovate, plane, flattened, 3–5.5 × 2–3.2 mm;</text>
      <biological_entity id="o3425" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s15" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s15" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s15" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>valves glabrous or puberulent, trichomes simple, 0.02–0.08 mm;</text>
      <biological_entity id="o3426" name="valve" name_original="valves" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o3427" name="trichome" name_original="trichomes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.02" from_unit="mm" name="some_measurement" src="d0_s16" to="0.08" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 4–10 per ovary;</text>
      <biological_entity id="o3428" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o3429" from="4" name="quantity" src="d0_s17" to="10" />
      </biological_entity>
      <biological_entity id="o3429" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 0.5–1.7 mm.</text>
      <biological_entity id="o3430" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s18" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds ovoid, 1–1.4 × 0.7–1 mm. 2n = 20.</text>
      <biological_entity id="o3431" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s19" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3432" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky ridges, steep talus slopes, rock outcrops and crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky ridges" />
        <character name="habitat" value="steep talus slopes" />
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Draba burkei was treated by C. L. Hitchcock (1941) and R. C. Rollins (1993) as a variety of D. maguirei. Chromosome numbers (2n = 20 versus 2n = 32), plant morphology, and molecular data support the recognition of these taxa as independent species (M. D. Windham 2004). Draba burkei is easily distinguished from D. maguirei by having exclusively simple trichomes confined to leaf blade margins (versus mostly branched trichomes on margins and surfaces) and smaller seeds (1–1.4 × 0.7–1 versus 1.6–2 × 1–1.3 mm). It is known from Box Elder, Cache, Morgan, and Weber counties, where it approaches but does not overlap the range of D. maguirei. Draba burkei (as D. maguirei var. burkei) is in the Center for Plant Conservation’s National Collection of Endangered Plants and is listed in NatureServe as a plant of conservation concern.</discussion>
  
</bio:treatment>