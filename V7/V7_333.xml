<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">254</other_info_on_meta>
    <other_info_on_meta type="illustration_page">255</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">anchonieae</taxon_name>
    <taxon_name authority="W. T. Aiton in W. Aiton and W. T. Aiton" date="unknown" rank="genus">matthiola</taxon_name>
    <taxon_name authority="(Ventenat) de Candolle" date="1821" rank="species">longipetala</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>2: 174. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe anchonieae;genus matthiola;species longipetala</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094796</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheiranthus</taxon_name>
    <taxon_name authority="Ventenat" date="unknown" rank="species">longipetalus</taxon_name>
    <place_of_publication>
      <publication_title>Descr. Pl. Nouv., plate</publication_title>
      <place_in_publication>93. 1802</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cheiranthus;species longipetalus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheiranthus</taxon_name>
    <taxon_name authority="Sibthorp &amp; Smith" date="unknown" rank="species">bicornis</taxon_name>
    <taxon_hierarchy>genus Cheiranthus;species bicornis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Matthiola</taxon_name>
    <taxon_name authority="(Sibthorp &amp; Smith) de Candolle" date="unknown" rank="species">bicornis</taxon_name>
    <taxon_hierarchy>genus Matthiola;species bicornis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Matthiola</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">longipetala</taxon_name>
    <taxon_name authority="(Sibthorp &amp; Smith) P. W. Ball" date="unknown" rank="subspecies">bicornis</taxon_name>
    <taxon_hierarchy>genus Matthiola;species longipetala;subspecies bicornis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>sparsely to moderately pubescent, (glandular papillae present or not).</text>
      <biological_entity id="o3661" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or ascending to decumbent, (1–) 1.5–5 (–6) dm, pubescent, (glandular or not).</text>
      <biological_entity id="o3662" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s2" to="decumbent" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="6" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s2" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves not forming vegetative rosettes.</text>
      <biological_entity constraint="basal" id="o3663" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o3664" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <relation from="o3663" id="r269" name="forming" negation="false" src="d0_s3" to="o3664" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves: petiole to 2 cm or (distal) sessile;</text>
      <biological_entity constraint="cauline" id="o3665" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o3666" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear-lanceolate to lanceolate or oblanceolate, (2–) 3.5–8 (–11) cm × 2–10 (–20) mm (smaller distally), base attenuate to cuneate, margins usually pinnatisect to sinuate or dentate, rarely entire or subentire.</text>
      <biological_entity constraint="cauline" id="o3667" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o3668" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s5" to="lanceolate or oblanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s5" to="3.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="11" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3669" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels divaricate to divaricate-ascending, straight, (0.5–) 1–2 (–3) mm, nearly as thick as fruit.</text>
      <biological_entity id="o3670" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="usually pinnatisect" name="shape" src="d0_s5" to="sinuate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subentire" value_original="subentire" />
      </biological_entity>
      <biological_entity id="o3671" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o3672" name="fruit" name_original="fruit" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o3670" id="r270" name="fruiting" negation="false" src="d0_s6" to="o3671" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals narrowly oblong, (7–) 8–11 (–12.5) × 1–2 mm;</text>
      <biological_entity id="o3673" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o3674" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s7" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s7" to="12.5" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="11" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals usually purple, pink, yellow, or brown, rarely white, oblong to linear-lanceolate, (15–) 18–23 (–27) × 2–4 (–5) mm, claw 7–13 mm, (margin crisped), apex subacute to obtuse;</text>
      <biological_entity id="o3675" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3676" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="linear-lanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="atypical_length" src="d0_s8" to="18" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="23" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="27" to_unit="mm" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s8" to="23" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3677" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3678" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="subacute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments 4–6 mm;</text>
      <biological_entity id="o3679" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3680" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 2.5–3.5 mm.</text>
      <biological_entity id="o3681" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3682" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits ascending to divaricate or, rarely, descending, straight, terete, (2.5–) 4–8.5 (–10) cm × 1–2 mm;</text>
      <biological_entity id="o3683" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="divaricate" value_original="divaricate" />
        <character name="arrangement" src="d0_s11" value="," value_original="," />
        <character is_modifier="false" name="orientation" src="d0_s11" value="descending" value_original="descending" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_length" src="d0_s11" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s11" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s11" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves pubescent, (often glandular);</text>
      <biological_entity id="o3684" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style obsolete to 3 mm;</text>
      <biological_entity id="o3685" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="obsolete" value_original="obsolete" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma horns 2, straight or curved upward, sometimes reflexed, 2–8 (–12) mm.</text>
      <biological_entity constraint="stigma" id="o3686" name="horn" name_original="horns" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds suborbicular to oblong or ovate, 1–2 × 0.8–1.2 mm;</text>
      <biological_entity id="o3687" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="suborbicular" name="shape" src="d0_s15" to="oblong or ovate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s15" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s15" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>wing 0.1–0.2 mm. 2n = 14.</text>
      <biological_entity id="o3688" name="wing" name_original="wing" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s16" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3689" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, disturbed areas, waste grounds, fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="waste grounds" />
        <character name="habitat" value="fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., Ont., Sask.; Ariz., Calif., Tex.; Europe; c, w Asia; n Africa; introduced also in Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="w Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Matthiola longipetala is sporadically naturalized in North America and Australia.</discussion>
  
</bio:treatment>