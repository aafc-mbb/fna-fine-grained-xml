<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">284</other_info_on_meta>
    <other_info_on_meta type="mention_page">347</other_info_on_meta>
    <other_info_on_meta type="treatment_page">293</other_info_on_meta>
    <other_info_on_meta type="illustration_page">289</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Greene" date="1883" rank="species">asprella</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>10: 125. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species asprella</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094715</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(cespitose);</text>
      <biological_entity id="o20532" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (with some persistent leaf-bases);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o20533" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, (0.4–) 0.6–1.5 (–2.7) dm, pubescent throughout, trichomes simple, 0.5–1.5 mm, and 2–4-rayed ones, 0.05–0.3 mm, (sometimes 2–4-rayed ones distally).</text>
      <biological_entity id="o20534" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.6" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="2.7" to_unit="dm" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="some_measurement" src="d0_s4" to="1.5" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20535" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves rosulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>subsessile or shortly petiolate;</text>
      <biological_entity constraint="basal" id="o20536" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole ciliate throughout;</text>
      <biological_entity id="o20537" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="throughout" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblanceolate to spatulate or obovate, (0.6–) 0.8–4.5 (–6) cm × 2–12 (–16) mm, margins usually entire, rarely obscurely dentate, (not ciliate), surfaces abaxially pubescent with stalked, (2–) 4-rayed trichomes, 0.2–1 mm, adaxially with stalked, 2–4-rayed trichomes, 0.3–0.8 mm, and simple ones, 0.5–1.9 mm.</text>
      <biological_entity id="o20538" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="spatulate or obovate" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_length" src="d0_s8" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s8" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="16" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20539" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely obscurely" name="architecture_or_shape" src="d0_s8" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o20540" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character constraint="with trichomes" constraintid="o20541" is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20541" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o20542" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
      <relation from="o20540" id="r1417" modifier="adaxially" name="with" negation="false" src="d0_s8" to="o20542" />
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 0.</text>
      <biological_entity constraint="cauline" id="o20543" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Racemes (15–) 30–75-flowered, ebracteate, elongated in fruit;</text>
      <biological_entity id="o20544" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="(15-)30-75-flowered" value_original="(15-)30-75-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o20545" is_modifier="false" name="length" src="d0_s10" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o20545" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>rachis not flexuous, pubescent, trichomes 2–4-rayed, sometimes with simple ones.</text>
      <biological_entity id="o20546" name="rachis" name_original="rachis" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s11" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20548" name="one" name_original="ones" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o20547" id="r1418" modifier="sometimes" name="with" negation="false" src="d0_s11" to="o20548" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting pedicels horizontal to divaricate-ascending, straight, 3–11 (–16) mm, pubescent, trichomes 2–4-rayed, sometimes with simple ones.</text>
      <biological_entity id="o20547" name="trichome" name_original="trichomes" src="d0_s11" type="structure" />
      <biological_entity id="o20549" name="pedicel" name_original="pedicels" src="d0_s12" type="structure" />
      <biological_entity id="o20550" name="whole-organism" name_original="" src="d0_s12" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s12" to="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="16" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="11" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20551" name="trichome" name_original="trichomes" src="d0_s12" type="structure" />
      <biological_entity id="o20552" name="one" name_original="ones" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o20547" id="r1419" name="fruiting" negation="false" src="d0_s12" to="o20549" />
      <relation from="o20551" id="r1420" modifier="sometimes" name="with" negation="false" src="d0_s12" to="o20552" />
    </statement>
    <statement id="d0_s13">
      <text>Flowers: sepals broadly ovate, 1.8–3.5 mm, pubescent, (trichomes simple and short-stalked, 2–4-rayed);</text>
      <biological_entity id="o20553" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o20554" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals pale-yellow, oblanceolate, 4–7 × 1.5–2.5 mm;</text>
      <biological_entity id="o20555" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o20556" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s14" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate to oblong, 0.4–0.7 mm.</text>
      <biological_entity id="o20557" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o20558" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s15" to="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits ovoid-ellipsoid to oblong, plane, inflated basally, 3–8 × 1.8–3.5 mm;</text>
      <biological_entity id="o20559" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="ovoid-ellipsoid" name="shape" src="d0_s16" to="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s16" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s16" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves hirsute, trichomes simple and spurred, (0.2–) 0.3–1 mm, or puberulent, trichomes short-stalked, 2–4-rayed, 0.1–0.2 mm;</text>
      <biological_entity id="o20560" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o20561" name="trichome" name_original="trichomes" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="spurred" value_original="spurred" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="0.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o20562" name="trichome" name_original="trichomes" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s17" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules 8–12 (–18) per ovary;</text>
      <biological_entity id="o20563" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="18" />
        <character char_type="range_value" constraint="per ovary" constraintid="o20564" from="8" name="quantity" src="d0_s18" to="12" />
      </biological_entity>
      <biological_entity id="o20564" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style 0.6–2 (–2.5) mm.</text>
      <biological_entity id="o20565" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s19" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds oblong, 0.9–1.2 × 0.6–0.8 mm. 2n = 30.</text>
      <biological_entity id="o20566" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s20" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s20" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20567" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Both R. C. Rollins (1993) and N. H. Holmgren (2005b) indicated that the chromosome number of Draba asprella is 2n = 32, but repeated counts by one of us (M. D. Windham 2000, unpubl.) show that the species consistently has 2n = 30. Rollins divided D. asprella into four varieties encompassing tremendous variation in trichome morphology. One of these (var. zionensis) is more closely related to D. sobolifera (Windham and L. Allphin, unpubl.) and is treated herein at species rank. Within D. asprella, in the strict sense, we recognize two varieties separated primarily by the type of trichomes found on the fruits. The distinctions are not absolute and there appear to be forms connecting the two, especially in Coconino County.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits hirsute, trichomes simple and unequally 2-rayed, (0.2-)0.3-1 mm.</description>
      <determination>11a Draba asprella var. asprella</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits puberulent, trichomes 2-4-rayed, 0.1-0.2 mm.</description>
      <determination>11b Draba asprella var. stelligera</determination>
    </key_statement>
  </key>
</bio:treatment>