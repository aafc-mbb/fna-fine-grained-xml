<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">546</other_info_on_meta>
    <other_info_on_meta type="mention_page">551</other_info_on_meta>
    <other_info_on_meta type="treatment_page">550</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">euclidieae</taxon_name>
    <taxon_name authority="Sternberg &amp; Hoppe" date="1815" rank="genus">braya</taxon_name>
    <taxon_name authority="Rouy" date="1899" rank="species">linearis</taxon_name>
    <place_of_publication>
      <publication_title>Ill. Pl. Eur.</publication_title>
      <place_in_publication>11: 84. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe euclidieae;genus braya;species linearis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095027</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not scapose;</text>
    </statement>
    <statement id="d0_s1">
      <text>sparsely to moderately pubescent throughout, trichomes subsessile or sessile, submalpighiaceous, 2-forked, and simple.</text>
      <biological_entity id="o32794" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="sparsely to moderately; throughout" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o32795" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-forked" value_original="2-forked" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple or few to several from base, erect, (usually unbranched), (0.4–) 0.7–1.4 (–1.8) dm.</text>
      <biological_entity id="o32796" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s2" value="few to several" value_original="few to several" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.7" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="1.8" to_unit="dm" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s2" to="1.4" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o32797" name="base" name_original="base" src="d0_s2" type="structure" />
      <relation from="o32796" id="r2205" name="from" negation="false" src="d0_s2" to="o32797" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (without distinct petiole);</text>
      <biological_entity constraint="basal" id="o32798" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade linear to narrowly spatulate, 0.5–3 cm × 0.5–2 (–3) mm, base attenuate or cuneate, margins dentate (with 1 or 2 teeth per side) or entire, apex obtuse, (surfaces glabrous or sparsely pubescent).</text>
      <biological_entity id="o32799" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="narrowly spatulate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32800" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o32801" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o32802" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves: 1–4 (each stem);</text>
      <biological_entity constraint="cauline" id="o32803" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>(subsessile);</text>
      <biological_entity constraint="cauline" id="o32804" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>blade similar to basal, smaller distally.</text>
      <biological_entity constraint="cauline" id="o32805" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o32807" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o32806" id="r2206" name="to" negation="false" src="d0_s7" to="o32807" />
    </statement>
    <statement id="d0_s8">
      <text>(Racemes not elongated in fruit.) Fruiting pedicels erect to divaricate, 1–4.5 mm.</text>
      <biological_entity id="o32806" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distally" name="size" notes="" src="d0_s7" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divaricate" value_original="divaricate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32808" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o32806" id="r2207" name="fruiting" negation="false" src="d0_s8" to="o32808" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 1.8–2.8 × 1–1.4 mm (sometimes slightly saccate basally);</text>
      <biological_entity id="o32809" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o32810" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s9" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white or purplish (especially claw), (broadly obovate or spatulate), 2.5–3.5 (–4) × 1.3–2 mm;</text>
      <biological_entity id="o32811" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o32812" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s10" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 1.5–2 mm;</text>
      <biological_entity id="o32813" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o32814" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers oblong, 0.4–0.6 mm.</text>
      <biological_entity id="o32815" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o32816" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits linear, more or less torulose, (straight or somewhat curved), (0.5–) 0.9–1.2 (–1.4) cm × 0.9–1.3 mm (uniform in width);</text>
      <biological_entity id="o32817" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s13" to="0.9" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s13" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="length" src="d0_s13" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s13" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>valves glabrous or sparsely pubescent, trichomes simple and submalpighiaceous;</text>
      <biological_entity id="o32818" name="valve" name_original="valves" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o32819" name="trichome" name_original="trichomes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>septum margin not expanded, or not basally;</text>
      <biological_entity constraint="septum" id="o32820" name="margin" name_original="margin" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="size" src="d0_s15" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 20–28 per ovary;</text>
      <biological_entity id="o32821" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o32822" from="20" name="quantity" src="d0_s16" to="28" />
      </biological_entity>
      <biological_entity id="o32822" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.3–0.5 mm;</text>
      <biological_entity id="o32823" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma weakly 2-lobed to entire.</text>
      <biological_entity id="o32824" name="stigma" name_original="stigma" src="d0_s18" type="structure">
        <character char_type="range_value" from="weakly 2-lobed" name="shape" src="d0_s18" to="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds uniseriate to weakly biseriate, oblong, 0.8–1 × 0.3–0.6 mm. 2n = 42.</text>
      <biological_entity id="o32825" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="uniseriate" name="arrangement" src="d0_s19" to="weakly biseriate" />
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s19" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s19" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o32826" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry or moist calcareous soils and alkaline clays and sands at the margins of evaporation pools on stream bank terraces and moraines</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry" />
        <character name="habitat" value="moist calcareous soils" />
        <character name="habitat" value="alkaline clays" constraint="at the margins of evaporation pools on stream bank terraces and moraines" />
        <character name="habitat" value="sands" constraint="at the margins of evaporation pools on stream bank terraces and moraines" />
        <character name="habitat" value="the margins" constraint="of evaporation pools on stream bank terraces and moraines" />
        <character name="habitat" value="evaporation pools" constraint="on stream bank terraces and moraines" />
        <character name="habitat" value="stream bank terraces" />
        <character name="habitat" value="moraines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Europe (Scandinavia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" value="Europe (Scandinavia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Braya linearis appears to be related to the European and Asian species of the genus. Hybridization studies with B. alpina from the European Alps (T. W. Böcher 1973) produced fertile F1 hybrids. DNA sequence evidence (S. I. Warwick et al. 2004) confirmed the close relationship between B. linearis and B. alpina, but suggested that B. linearis is even more closely related to B. humilis and the Asian B. siliquosa Bunge.</discussion>
  
</bio:treatment>