<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">29</other_info_on_meta>
    <other_info_on_meta type="mention_page">39</other_info_on_meta>
    <other_info_on_meta type="mention_page">51</other_info_on_meta>
    <other_info_on_meta type="treatment_page">49</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">salix</taxon_name>
    <taxon_name authority="Dumortier" date="1826" rank="section">Triandrae</taxon_name>
    <place_of_publication>
      <publication_title>Bijdr. Natuurk. Wetensch.</publication_title>
      <place_in_publication>1: 58. 1826</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus salix;section Triandrae</taxon_hierarchy>
    <other_info_on_name type="fna_id">317499</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="W. D. J. Koch" date="unknown" rank="section">Amygdalinae</taxon_name>
    <taxon_hierarchy>genus Salix;section Amygdalinae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 2–7 (–10) m.</text>
      <biological_entity id="o32235" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="m" name="atypical_some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: (bark flaking in plates), branches ± brittle at base.</text>
      <biological_entity id="o32237" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o32238" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character constraint="at base" constraintid="o32239" is_modifier="false" modifier="more or less" name="fragility" src="d0_s1" value="brittle" value_original="brittle" />
      </biological_entity>
      <biological_entity id="o32239" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules on late ones and vigorous shoots foliaceous, apex acute to acuminate;</text>
      <biological_entity id="o32240" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o32241" name="stipule" name_original="stipules" src="d0_s2" type="structure" />
      <biological_entity id="o32242" name="one" name_original="ones" src="d0_s2" type="structure" />
      <biological_entity id="o32243" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s2" value="vigorous" value_original="vigorous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o32244" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s2" to="acuminate" />
      </biological_entity>
      <relation from="o32241" id="r2153" name="on" negation="false" src="d0_s2" to="o32242" />
    </statement>
    <statement id="d0_s3">
      <text>petiole with paired, clustered, or stalked spherical glands or foliaceous glands distally;</text>
      <biological_entity id="o32245" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o32246" name="petiole" name_original="petiole" src="d0_s3" type="structure" />
      <biological_entity id="o32247" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s3" value="paired" value_original="paired" />
        <character is_modifier="true" name="arrangement_or_growth_form" src="d0_s3" value="clustered" value_original="clustered" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="stalked" value_original="stalked" />
        <character is_modifier="true" name="shape" src="d0_s3" value="spherical" value_original="spherical" />
      </biological_entity>
      <biological_entity id="o32248" name="gland" name_original="glands" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <relation from="o32246" id="r2154" name="with" negation="false" src="d0_s3" to="o32247" />
      <relation from="o32246" id="r2155" name="with" negation="false" src="d0_s3" to="o32248" />
    </statement>
    <statement id="d0_s4">
      <text>largest medial blade hypostomatous, abaxial surface glaucous or not;</text>
      <biological_entity id="o32249" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="largest medial" id="o32250" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="hypostomatous" value_original="hypostomatous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o32251" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
        <character name="pubescence" src="d0_s4" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>juvenile blade puberulent or pubescent abaxially, hairs white and ferruginous.</text>
      <biological_entity id="o32252" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o32253" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o32254" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white and ferruginous" value_original="white and ferruginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pistillate floral bracts persistent or deciduous after flowering.</text>
      <biological_entity constraint="floral" id="o32255" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character constraint="after flowering" is_modifier="false" name="duration" src="d0_s6" value="deciduous" value_original="deciduous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers: stamens 3 or, rarely, 2;</text>
      <biological_entity id="o32256" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o32257" name="stamen" name_original="stamens" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character name="quantity" src="d0_s7" value="," value_original="," />
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers (dry), 0.4–0.6 mm.</text>
      <biological_entity id="o32258" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32259" name="anther" name_original="anthers" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Pistillate flowers: adaxial nectary shorter than stipe;</text>
      <biological_entity id="o32260" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o32261" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character constraint="than stipe" constraintid="o32262" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o32262" name="stipe" name_original="stipe" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stipe 1–2 mm;</text>
      <biological_entity id="o32263" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o32264" name="stipe" name_original="stipe" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary glabrous.</text>
      <biological_entity id="o32265" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o32266" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>2b5.</number>
  <discussion>Species 5 (1 in the flora).</discussion>
  <discussion>A study of genetic relationships in Salix using AFLP (S. Trybush et al. 2008) showed that the genetic similarity of S. triandra to subg. Salix and subg. Vetrix was similar and greater than the genetic similarity between these subgenera. Further study may support treating sect. Triandrae as a distinct subgenus.</discussion>
  
</bio:treatment>