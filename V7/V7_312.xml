<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="treatment_page">221</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">ARIVELA</taxon_name>
    <place_of_publication>
      <publication_title>Sylva Tellur.,</publication_title>
      <place_in_publication>110. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus ARIVELA</taxon_hierarchy>
    <other_info_on_name type="etymology">Origin obscure</other_info_on_name>
    <other_info_on_name type="fna_id">102610</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o20332" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely branched [unbranched];</text>
    </statement>
    <statement id="d0_s2">
      <text>glandular-hirsute [glabrous].</text>
      <biological_entity id="o20333" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-hirsute" value_original="glandular-hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules absent;</text>
      <biological_entity id="o20334" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o20335" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole with pulvinus basally or distally (petiolule base adnate, forming pulvinar disc);</text>
      <biological_entity id="o20336" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o20337" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o20338" name="pulvinu" name_original="pulvinus" src="d0_s4" type="structure" />
      <relation from="o20337" id="r1405" modifier="distally" name="with" negation="false" src="d0_s4" to="o20338" />
    </statement>
    <statement id="d0_s5">
      <text>leaflets 3 or 5.</text>
      <biological_entity id="o20339" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o20340" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" unit="or" value="3" value_original="3" />
        <character name="quantity" src="d0_s5" unit="or" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary (from distal leaves), racemes (flat-topped or elongated);</text>
      <biological_entity id="o20341" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o20342" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present [absent].</text>
      <biological_entity id="o20343" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers: zygomorphic;</text>
      <biological_entity id="o20344" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="zygomorphic" value_original="zygomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals persistent, distinct, equal;</text>
      <biological_entity id="o20345" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o20346" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals equal;</text>
      <biological_entity id="o20347" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o20348" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 14–25 [–35];</text>
      <biological_entity id="o20349" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o20350" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="25" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="35" />
        <character char_type="range_value" from="14" name="quantity" src="d0_s11" to="25" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments inserted on a discoid or conical androgynophore, glandular-hirsute;</text>
      <biological_entity id="o20351" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o20352" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glandular-hirsute" value_original="glandular-hirsute" />
      </biological_entity>
      <biological_entity id="o20353" name="androgynophore" name_original="androgynophore" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="discoid" value_original="discoid" />
        <character is_modifier="true" name="shape" src="d0_s12" value="conical" value_original="conical" />
      </biological_entity>
      <relation from="o20352" id="r1406" name="inserted on a" negation="false" src="d0_s12" to="o20353" />
    </statement>
    <statement id="d0_s13">
      <text>anthers (oblong), coiling as pollen released;</text>
      <biological_entity id="o20354" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character constraint="as pollen" constraintid="o20356" is_modifier="false" name="shape" notes="" src="d0_s13" value="coiling" value_original="coiling" />
      </biological_entity>
      <biological_entity id="o20355" name="anther" name_original="anthers" src="d0_s13" type="structure" />
      <biological_entity id="o20356" name="pollen" name_original="pollen" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>gynophore obsolete.</text>
      <biological_entity id="o20357" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o20358" name="gynophore" name_original="gynophore" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s14" value="obsolete" value_original="obsolete" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsules, partly dehiscent, oblong, (glandular).</text>
      <biological_entity constraint="fruits" id="o20359" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="partly" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds [10–] 25–40 [–100], spheroidal, not arillate, (cleft fused between ends).</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 10.</text>
      <biological_entity id="o20360" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" name="atypical_quantity" src="d0_s16" to="25" to_inclusive="false" />
        <character char_type="range_value" from="40" from_inclusive="false" name="atypical_quantity" src="d0_s16" to="100" />
        <character char_type="range_value" from="25" name="quantity" src="d0_s16" to="40" />
        <character is_modifier="false" name="shape" src="d0_s16" value="spheroidal" value_original="spheroidal" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity constraint="x" id="o20361" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Asia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <other_name type="common_name">Spiderflower</other_name>
  <discussion>Species ca. 10 (1 in the flora).</discussion>
  
</bio:treatment>