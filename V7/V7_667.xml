<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">443</other_info_on_meta>
    <other_info_on_meta type="mention_page">494</other_info_on_meta>
    <other_info_on_meta type="treatment_page">442</other_info_on_meta>
    <other_info_on_meta type="illustration_page">440</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">sinapis</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">alba</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 668. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus sinapis;species alba</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200009674</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brassica</taxon_name>
    <taxon_name authority="(Linnaeus) Rabenhorst" date="unknown" rank="species">alba</taxon_name>
    <taxon_hierarchy>genus Brassica;species alba;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brassica</taxon_name>
    <taxon_name authority="Moench" date="unknown" rank="species">hirta</taxon_name>
    <taxon_hierarchy>genus Brassica;species hirta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rorippa</taxon_name>
    <taxon_name authority="Stuckey" date="unknown" rank="species">coloradensis</taxon_name>
    <taxon_hierarchy>genus Rorippa;species coloradensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually hispid, rarely glabrous.</text>
      <biological_entity id="o10122" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems often branched distally, (0.15–) 0.25–1 (–2.2) dm.</text>
      <biological_entity id="o10123" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often; distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.15" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="0.25" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2.2" to_unit="dm" />
        <character char_type="range_value" from="0.25" from_unit="dm" name="some_measurement" src="d0_s1" to="1" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: petiole 1–3 (–6) cm;</text>
      <biological_entity constraint="basal" id="o10124" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o10125" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblong, ovate, or lanceolate (in outline), (3.5–) 5–14 (–20) cm × 20–60 (–80) mm, margins lyrate, pinnatifid, pinnatisect;</text>
      <biological_entity constraint="basal" id="o10126" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10127" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="atypical_length" src="d0_s3" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="20" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="14" to_unit="cm" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="80" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s3" to="60" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10128" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lyrate" value_original="lyrate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnatisect" value_original="pinnatisect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lobes 1–3 each side, (oblong, ovate, or lanceolate, 1.5–2.5 cm), margins usually dentate or repand, rarely pinnatifid.</text>
      <biological_entity constraint="basal" id="o10129" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o10130" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity id="o10131" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o10132" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves (distal) shortly petiolate;</text>
      <biological_entity constraint="cauline" id="o10133" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade (ovate or oblong-ovate, 2–4.5 cm), margins coarsely dentate, rarely subentire.</text>
      <biological_entity id="o10134" name="blade" name_original="blade" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting pedicels divaricate, (3–) 6–12 (–17) mm.</text>
      <biological_entity id="o10135" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="subentire" value_original="subentire" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="divaricate" value_original="divaricate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="17" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10136" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <relation from="o10135" id="r705" name="fruiting" negation="false" src="d0_s7" to="o10136" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals (3.8–) 4–7 (–8) × 1–1.8 mm;</text>
      <biological_entity id="o10137" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o10138" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="atypical_length" src="d0_s8" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s8" to="7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals pale-yellow, (7–) 8–12 (–14) × (3–) 4–6 (–7) mm;</text>
      <biological_entity id="o10139" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10140" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_length" src="d0_s9" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="14" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments (3–) 4–7 (–8) mm;</text>
      <biological_entity id="o10141" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o10142" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1.2–1.5 mm.</text>
      <biological_entity id="o10143" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o10144" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits lanceolate, (1.5–) 2–4.2 (–5) cm × (2–) 3–5.5 (–6.5) mm;</text>
      <biological_entity id="o10145" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s12" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4.2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s12" to="5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s12" to="4.2" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s12" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s12" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>valvular segment terete or slightly flattened, (0.5–) 0.7–1.7 (–2) cm, 2–5-seeded per locule;</text>
      <biological_entity constraint="valvular" id="o10146" name="segment" name_original="segment" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s13" to="0.7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s13" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s13" to="1.7" to_unit="cm" />
        <character constraint="per locule" constraintid="o10147" is_modifier="false" name="architecture" src="d0_s13" value="2-5-seeded" value_original="2-5-seeded" />
      </biological_entity>
      <biological_entity id="o10147" name="locule" name_original="locule" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>terminal segment ensiform, flattened, (1–) 1.5–2.5 (–3) cm, equal to or longer than valves, seedless;</text>
      <biological_entity constraint="terminal" id="o10148" name="segment" name_original="segment" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ensiform" value_original="ensiform" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s14" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s14" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s14" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="variability" src="d0_s14" value="equal" value_original="equal" />
        <character constraint="than valves" constraintid="o10149" is_modifier="false" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="seedless" value_original="seedless" />
      </biological_entity>
      <biological_entity id="o10149" name="valve" name_original="valves" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>valves hispid, trichomes of 2 types (subsetiform mixed with shorter, slender ones).</text>
      <biological_entity id="o10150" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o10151" name="trichome" name_original="trichomes" src="d0_s15" type="structure" />
      <biological_entity id="o10152" name="type" name_original="types" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <relation from="o10151" id="r706" name="consist_of" negation="false" src="d0_s15" to="o10152" />
    </statement>
    <statement id="d0_s16">
      <text>Seeds pale-yellow to pale-brown, (1.7–) 2–3 (–3.5) mm diam. 2n = 24.</text>
      <biological_entity id="o10153" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="pale-yellow" name="coloration" src="d0_s16" to="pale-brown" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="diameter" src="d0_s16" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s16" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="diameter" src="d0_s16" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10154" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Escape from cultivation, roadsides, waste places, disturbed areas, grain fields, cultivated areas, gardens, orchards</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cultivation" modifier="escape from" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="grain fields" />
        <character name="habitat" value="cultivated areas" />
        <character name="habitat" value="gardens" />
        <character name="habitat" value="orchards" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Greenland; Alta., B.C., Man., N.B., N.S., Ont., P.E.I., Que., Sask., Yukon; Ariz., Calif., Colo., Conn., Del., D.C., Ill., Ind., Iowa, Maine, Mass., Mich., Minn., Mo., Mont., Nebr., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Wash., W.Va., Wis.; Eurasia; introduced also in Mexico, Central America, South America (Argentina), Europe, sw Asia, n Africa, Atlantic Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="sw Asia" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">White mustard</other_name>
  <discussion>Of the three subspecies of Sinapis alba recognized in European and North African floras, only subsp. alba is naturalized in the New World. This taxon is an important crop plant, and is occasionally reported as a weedy escape from cultivation. Its seeds are used for the manufacture of condiment mustard (see also 17. Brassica), pickling spice, and production of oils for soap and mayonnaise, lubrication, and cooking (I. A. Al-Shehbaz 1985).</discussion>
  
</bio:treatment>