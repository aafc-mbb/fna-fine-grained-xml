<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">284</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="mention_page">305</other_info_on_meta>
    <other_info_on_meta type="mention_page">313</other_info_on_meta>
    <other_info_on_meta type="mention_page">318</other_info_on_meta>
    <other_info_on_meta type="treatment_page">316</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="S. Watson in W. H. Brewer et al." date="unknown" rank="species">lemmonii</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>2: 430. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species lemmonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094707</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(cespitose, not pulvinate);</text>
      <biological_entity id="o34960" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (covered with persistent petiole remains, branches sometimes terminating in sterile rosettes);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o34961" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, 0.3–1 (–1.5) dm, hirsute throughout, trichomes simple, 0.2–0.7 mm, with short-stalked, 2-rayed ones, (smaller).</text>
      <biological_entity id="o34962" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="1.5" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s4" to="1" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o34963" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34964" name="one" name_original="ones" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
      </biological_entity>
      <relation from="o34963" id="r2342" name="with" negation="false" src="d0_s4" to="o34964" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves rosulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>shortly petiolate;</text>
      <biological_entity constraint="basal" id="o34965" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole base and margin ciliate, (trichomes simple, 0.5–1 mm, midvein obscure);</text>
      <biological_entity constraint="petiole" id="o34966" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o34967" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade (somewhat fleshy), oblanceolate to obovate, 0.4–1 (–1.8) cm × 1.5–4 (–6) mm, margins entire, surfaces hirsute, abaxially mostly with stalked, 2-rayed trichomes, 0.1–0.8 mm, rarely with fewer, simple ones, adaxially with mostly simple ones, 0.5–1 mm.</text>
      <biological_entity id="o34968" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="1.8" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s8" to="1" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34969" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o34970" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34971" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o34972" name="one" name_original="ones" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="fewer" value_original="fewer" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o34973" name="one" name_original="ones" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="mostly" name="architecture" src="d0_s8" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o34970" id="r2343" modifier="abaxially mostly; mostly" name="with" negation="false" src="d0_s8" to="o34971" />
      <relation from="o34970" id="r2344" modifier="rarely" name="with" negation="false" src="d0_s8" to="o34972" />
      <relation from="o34970" id="r2345" modifier="adaxially" name="with" negation="false" src="d0_s8" to="o34973" />
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 0.</text>
      <biological_entity constraint="cauline" id="o34974" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Racemes 4–15 (–21) -flowered, ebracteate, elongated in fruit;</text>
      <biological_entity id="o34975" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="4-15(-21)-flowered" value_original="4-15(-21)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o34976" is_modifier="false" name="length" src="d0_s10" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o34976" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>rachis not flexuous, hirsute as stem.</text>
      <biological_entity id="o34978" name="stem" name_original="stem" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting pedicels horizontal to divaricate-ascending (often somewhat decurrent basally), straight or curved upward, 4–10 (–14) mm, hirsute as stem.</text>
      <biological_entity id="o34977" name="rachis" name_original="rachis" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s11" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o34978" is_modifier="false" name="pubescence" src="d0_s11" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o34979" name="pedicel" name_original="pedicels" src="d0_s12" type="structure" />
      <biological_entity id="o34980" name="stem" name_original="stem" src="d0_s12" type="structure">
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s12" to="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="14" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <relation from="o34977" id="r2346" name="fruiting" negation="false" src="d0_s12" to="o34979" />
    </statement>
    <statement id="d0_s13">
      <text>Flowers: sepals ovate, 2–2.7 mm, pubescent, (trichomes simple, short-stalked, 2-rayed);</text>
      <biological_entity id="o34981" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o34982" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals yellow, spatulate, 4–6 × 1.5–2.5 mm;</text>
      <biological_entity id="o34983" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o34984" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s14" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s14" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers oblong, 0.4–0.6 mm.</text>
      <biological_entity id="o34985" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o34986" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits ovate to ovatelanceolate, slightly twisted, flattened, 4–9 × 3.5–5 mm;</text>
      <biological_entity id="o34987" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s16" to="ovatelanceolate" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s16" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s16" to="9" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves pubescent, trichomes usually simple, 0.1–0.45 mm, rarely with fewer, short-stalked, 2-rayed ones;</text>
      <biological_entity id="o34988" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o34989" name="trichome" name_original="trichomes" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s17" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s17" to="0.45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34990" name="one" name_original="ones" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="fewer" value_original="fewer" />
        <character is_modifier="true" name="architecture" src="d0_s17" value="short-stalked" value_original="short-stalked" />
      </biological_entity>
      <relation from="o34989" id="r2347" modifier="rarely" name="with" negation="false" src="d0_s17" to="o34990" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 10–14 (–16) per ovary;</text>
      <biological_entity id="o34991" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" from="14" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="16" />
        <character char_type="range_value" constraint="per ovary" constraintid="o34992" from="10" name="quantity" src="d0_s18" to="14" />
      </biological_entity>
      <biological_entity id="o34992" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style 0.1–0.6 (–0.8) mm.</text>
      <biological_entity id="o34993" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s19" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds ovoid, 1–1.4 × 0.6–1 mm. 2n = 50.</text>
      <biological_entity id="o34994" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s20" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s20" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o34995" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="50" value_original="50" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Granitic rock outcrops, boulder slopes, alpine fellfields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="granitic rock outcrops" />
        <character name="habitat" value="boulder slopes" />
        <character name="habitat" value="alpine fellfields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3000-4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="3000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>56.</number>
  <other_name type="past_name">lemmoni</other_name>
  <discussion>Draba lemmonii was so broadly circumscribed by C. L. Hitchcock (1941) and R. C. Rollins (1993) that it included plants here assigned to three different species. For a list of features distinguishing D. lemmonii from the recently-segregated D. longisquamosa and D. cyclomorpha, see I. A. Al-Shehbaz and M. D. Windham (2007). Draba lemmonii is apparently restricted to alpine areas of the Sierra Nevada in Fresno, Inyo, Madera, Mono, and Tuolumne counties.</discussion>
  
</bio:treatment>