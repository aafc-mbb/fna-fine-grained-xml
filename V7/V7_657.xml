<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Suzanne I. Warwick</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">234</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="treatment_page">436</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="Moench" date="1794" rank="genus">HIRSCHFELDIA</taxon_name>
    <place_of_publication>
      <publication_title>Methodus,</publication_title>
      <place_in_publication>264. 1794</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus HIRSCHFELDIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Christian Cajus Lorenz Hirschfeldt, 1742–1792, Austrian botanist/horticulturist</other_info_on_name>
    <other_info_on_name type="fna_id">115526</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annual or biennials;</text>
      <biological_entity id="o15896" name="whole-organism" name_original="" src="d0_s0" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s2">
      <text>pubescent.</text>
      <biological_entity id="o15897" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems (simple or several from base), erect, branched basally and distally.</text>
      <biological_entity id="o15898" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="basally; distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate or subsessile;</text>
      <biological_entity id="o15899" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal rosulate, petiolate, blade lyrate to pinnatifid, margins crenate-dentate;</text>
      <biological_entity constraint="basal" id="o15900" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o15901" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="lyrate" name="shape" src="d0_s6" to="pinnatifid" />
      </biological_entity>
      <biological_entity id="o15902" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="crenate-dentate" value_original="crenate-dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline subsessile or petiolate, blade (base not auriculate), margins dentate or pinnatifid.</text>
      <biological_entity constraint="cauline" id="o15903" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o15904" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o15905" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (corymbose, several-flowered), considerably elongated in fruit.</text>
      <biological_entity id="o15907" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels erect, stout.</text>
      <biological_entity id="o15906" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o15907" is_modifier="false" modifier="considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o15908" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o15906" id="r1106" name="fruiting" negation="false" src="d0_s9" to="o15908" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals widely spreading or reflexed, oblong, lateral pair not saccate basally;</text>
      <biological_entity id="o15909" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o15910" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals yellow, obovate to spatulate, claw differentiated from blade, (apex obtuse);</text>
      <biological_entity id="o15911" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o15912" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s11" to="spatulate" />
      </biological_entity>
      <biological_entity id="o15913" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character constraint="from blade" constraintid="o15914" is_modifier="false" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o15914" name="blade" name_original="blade" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o15915" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o15916" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments not dilated basally;</text>
      <biological_entity id="o15917" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o15918" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers oblong or ovate, (apex obtuse);</text>
      <biological_entity id="o15919" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o15920" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectar glands not confluent, median glands present.</text>
      <biological_entity id="o15921" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity constraint="nectar" id="o15922" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s15" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity constraint="median" id="o15923" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits siliques, dehiscent, sessile, segments 2, linear, slightly torulose, terete or slightly 4-angled;</text>
      <biological_entity constraint="fruits" id="o15924" name="silique" name_original="siliques" src="d0_s16" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s16" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>(proximal segment not torulose, somewhat corky at maturity, 8–20-seeded; terminal segment indehiscent, 1-seeded or 2-seeded, slightly swollen apically);</text>
      <biological_entity id="o15925" name="segment" name_original="segments" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s16" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s16" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s16" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s16" value="4-angled" value_original="4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves 3 (–7) -veined, usually glabrous, rarely sparsely pubescent;</text>
      <biological_entity id="o15926" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="3(-7)-veined" value_original="3(-7)-veined" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>replum rounded;</text>
      <biological_entity id="o15927" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum complete;</text>
      <biological_entity id="o15928" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 10–22 per ovary;</text>
      <biological_entity id="o15930" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>(style present);</text>
      <biological_entity id="o15929" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o15930" from="10" name="quantity" src="d0_s21" to="22" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>stigma capitate, entire.</text>
      <biological_entity id="o15931" name="stigma" name_original="stigma" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Seeds uniseriate, plump, not winged, globose;</text>
      <biological_entity id="o15932" name="seed" name_original="seeds" src="d0_s24" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s24" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="size" src="d0_s24" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s24" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s24" value="globose" value_original="globose" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>seed-coat (smooth to finely reticulate), mucilaginous when wetted;</text>
      <biological_entity id="o15933" name="seed-coat" name_original="seed-coat" src="d0_s25" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s25" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>cotyledons conduplicate.</text>
    </statement>
    <statement id="d0_s27">
      <text>x = 7.</text>
      <biological_entity id="o15934" name="cotyledon" name_original="cotyledons" src="d0_s26" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s26" value="conduplicate" value_original="conduplicate" />
      </biological_entity>
      <biological_entity constraint="x" id="o15935" name="chromosome" name_original="" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Eurasia, nw Africa; introduced also in South America, s Africa, Atlantic Islands, Pacific Islands (Hawaii), Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Eurasia" establishment_means="introduced" />
        <character name="distribution" value="nw Africa" establishment_means="introduced" />
        <character name="distribution" value="also in South America" establishment_means="introduced" />
        <character name="distribution" value="s Africa" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands" establishment_means="introduced" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <discussion>Species 1.</discussion>
  <discussion>It is with some hesitation that I recognize this genus; it should perhaps be united with Erucastrum, as recently proposed for conservation by I. A. Al-Shehbaz (2005b). As clearly shown by S. I. Warwick and L. D. Black (1993), Brassica, Diplotaxis, and Erucastrum are artificially delimited genera, and a substantial revision of their boundaries is needed.</discussion>
  
</bio:treatment>