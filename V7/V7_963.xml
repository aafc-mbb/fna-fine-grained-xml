<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">573</other_info_on_meta>
    <other_info_on_meta type="treatment_page">593</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">lepidieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lepidium</taxon_name>
    <taxon_name authority="(S. Watson) Rattan ex B. L. Robinson in A. Gray et al." date="1895" rank="species">strictum</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(1,1): 129. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe lepidieae;genus lepidium;species strictum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250095140</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lepidium</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="species">oxycarpum</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="variety">strictum</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>1: 46. 1876</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lepidium;species oxycarpum;variety strictum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lepidium</taxon_name>
    <taxon_name authority="Howell" date="unknown" rank="species">reticulatum</taxon_name>
    <taxon_hierarchy>genus Lepidium;species reticulatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>hirsute.</text>
      <biological_entity id="o20679" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems often several from base, usually ascending or decumbent to prostrate, rarely erect, branched distally, (0.4–) 0.7–1.7 (–2) dm.</text>
      <biological_entity id="o20680" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o20681" is_modifier="false" modifier="often" name="quantity" src="d0_s2" value="several" value_original="several" />
        <character is_modifier="false" modifier="usually" name="orientation" notes="" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation_or_growth_form" src="d0_s2" value="decumbent to prostrate" value_original="decumbent to prostrate" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.7" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="2" to_unit="dm" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s2" to="1.7" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o20681" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves not rosulate;</text>
      <biological_entity constraint="basal" id="o20682" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–3 cm;</text>
      <biological_entity id="o20683" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade 2-pinnatifid (lobes lanceolate to oblong), 1.5–5.6 cm, margins (of lobes) entire.</text>
      <biological_entity id="o20684" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="2-pinnatifid" value_original="2-pinnatifid" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="5.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o20685" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves shortly petiolate;</text>
      <biological_entity constraint="cauline" id="o20686" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade pinnatifid, 0.8–3 cm × 0.3–8 mm, base cuneate to attenuate, not auriculate, margins (of lobes) entire.</text>
      <biological_entity id="o20687" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="pinnatifid" value_original="pinnatifid" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20688" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s7" to="attenuate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o20689" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes elongated, (dense) in fruit;</text>
      <biological_entity id="o20690" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="length" src="d0_s8" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o20691" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <relation from="o20690" id="r1428" name="in" negation="false" src="d0_s8" to="o20691" />
    </statement>
    <statement id="d0_s9">
      <text>rachis puberulent, trichomes straight, cylindrical.</text>
      <biological_entity id="o20692" name="rachis" name_original="rachis" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels suberect and subappressed at base, recurved and becoming divaricate distally, strongly curved, (often flattened and narrowly winged), (1–) 1.4–2.5 (–3) × 0.2–0.4 mm, puberulent adaxially.</text>
      <biological_entity id="o20693" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" modifier="becoming; distally" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="strongly" name="course" src="d0_s10" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o20694" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <biological_entity id="o20695" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="suberect" value_original="suberect" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="subappressed" value_original="subappressed" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="recurved" value_original="recurved" />
      </biological_entity>
      <relation from="o20693" id="r1429" name="fruiting" negation="false" src="d0_s10" to="o20694" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals (persistent), oblong, 0.7–1 (–1.2) × 0.3–0.4 mm;</text>
      <biological_entity id="o20696" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s11" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20697" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals (rudimentary), white, linear, 0.2–0.5 × 0.05 mm, claw absent;</text>
      <biological_entity id="o20698" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="length" src="d0_s12" to="0.5" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="0.05" value_original="0.05" />
      </biological_entity>
      <biological_entity id="o20699" name="petal" name_original="petals" src="d0_s12" type="structure" />
      <biological_entity id="o20700" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 2, median;</text>
      <biological_entity id="o20701" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o20702" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s13" value="median" value_original="median" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments 0.5–0.8 mm;</text>
      <biological_entity id="o20703" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o20704" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 0.1–0.15 mm.</text>
      <biological_entity id="o20705" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o20706" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s15" to="0.15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits ovate-orbicular to ovate, 2.5–3.3 × 2–3 mm,apically winged, apical notch 0.3–0.6 mm deep;</text>
      <biological_entity id="o20707" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="ovate-orbicular" name="shape" src="d0_s16" to="ovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s16" to="3.3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s16" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="apically" name="architecture" src="d0_s16" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="apical" id="o20708" name="notch" name_original="notch" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s16" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves (enclosing seeds), thin, smooth, reticulate-veined, glabrous or puberulent on margin;</text>
      <biological_entity id="o20709" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="reticulate-veined" value_original="reticulate-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character constraint="on margin" constraintid="o20710" is_modifier="false" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o20710" name="margin" name_original="margin" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style obsolete or to 0.1 mm, included in apical notch.</text>
      <biological_entity id="o20711" name="style" name_original="style" src="d0_s18" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s18" value="obsolete" value_original="obsolete" />
        <character name="prominence" src="d0_s18" value="0-0.1 mm" value_original="0-0.1 mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o20712" name="notch" name_original="notch" src="d0_s18" type="structure" />
      <relation from="o20711" id="r1430" name="included in" negation="false" src="d0_s18" to="o20712" />
    </statement>
    <statement id="d0_s19">
      <text>Seeds oblong, 1.2–1.6 × 0.7–0.8 mm. 2n = 32.</text>
      <biological_entity id="o20713" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s19" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s19" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20714" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste grounds, woodlands, hillsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste grounds" />
        <character name="habitat" value="woodlands" />
        <character name="habitat" value="hillsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Oreg.; South America (Chile).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" value="South America (Chile)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>39.</number>
  <discussion>Lepidium strictum was reported from Utah (C. L. Hitchcock 1936) and Colorado (W. A. Weber 1989), but we have been unable to verify those records. The species is easily distinguished by a combination of reticulate-veined fruits, persistent sepals, flattened and narrowly winged fruiting pedicels, and filiform nectaries.</discussion>
  
</bio:treatment>