<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">572</other_info_on_meta>
    <other_info_on_meta type="mention_page">573</other_info_on_meta>
    <other_info_on_meta type="mention_page">587</other_info_on_meta>
    <other_info_on_meta type="treatment_page">583</other_info_on_meta>
    <other_info_on_meta type="illustration_page">578</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">lepidieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lepidium</taxon_name>
    <taxon_name authority="Nuttall in J. Torrey and A. Gray" date="1838" rank="species">integrifolium</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 116. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe lepidieae;genus lepidium;species integrifolium</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095097</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lepidium</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">montanum</taxon_name>
    <taxon_name authority="(Nuttall) C. L. Hitchcock" date="unknown" rank="subspecies">integrifolium</taxon_name>
    <taxon_hierarchy>genus Lepidium;species montanum;subspecies integrifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lepidium</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="species">montanum</taxon_name>
    <taxon_name authority="(Nuttall) C. L. Hitchcock" date="unknown" rank="variety">integrifolium</taxon_name>
    <taxon_hierarchy>genus Lepidium;species montanum;variety integrifolium;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lepidium</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">utahense</taxon_name>
    <taxon_hierarchy>genus Lepidium;species utahense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lepidium</taxon_name>
    <taxon_name authority="(Nuttall) Kuntze" date="unknown" rank="species">zionis</taxon_name>
    <taxon_hierarchy>genus Lepidium;species zionis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Nasturtium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">integrifolium</taxon_name>
    <taxon_hierarchy>genus Nasturtium;species integrifolium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(caudex often thick, not aboveground, covered with persistent petiolar remains);</text>
    </statement>
    <statement id="d0_s2">
      <text>puberulent.</text>
      <biological_entity id="o8689" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems several from base (caudex), ascending, branched distally, (1–) 1.5–3.5 (–4) dm.</text>
      <biological_entity id="o8690" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character constraint="from base" constraintid="o8691" is_modifier="false" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="4" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s3" to="3.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o8691" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves rosulate;</text>
      <biological_entity constraint="basal" id="o8692" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole (0.5–) 1.5–6 (–7.5) cm;</text>
      <biological_entity id="o8693" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblanceolate to obovate, (1.5–) 2.5–7 (–9) cm × (10–) 15–25 mm, margins usually entire, rarely denticulate subapically.</text>
      <biological_entity id="o8694" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s6" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="9" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_width" src="d0_s6" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s6" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8695" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely; subapically" name="shape" src="d0_s6" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves shortly petiolate or sessile;</text>
      <biological_entity constraint="cauline" id="o8696" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade narrowly lanceolate to broadly oblanceolate, 1–5 cm × 2–9 (–12) mm, base cuneate, not auriculate, margins usually entire, rarely denticulate subapically.</text>
      <biological_entity id="o8697" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s8" to="broadly oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s8" to="5" to_unit="cm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8698" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o8699" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely; subapically" name="shape" src="d0_s8" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes elongated in fruit;</text>
      <biological_entity id="o8700" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o8701" is_modifier="false" name="length" src="d0_s9" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o8701" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>rachis puberulent, trichomes straight, sometimes clavate.</text>
      <biological_entity id="o8702" name="rachis" name_original="rachis" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruiting pedicels divaricate-ascending to horizontal, straight, (not winged), (4–) 5–10 × 0.3–0.5 mm, puberulent adaxially.</text>
      <biological_entity id="o8703" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="divaricate-ascending" name="orientation" src="d0_s11" to="horizontal" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s11" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o8704" name="pedicel" name_original="pedicels" src="d0_s11" type="structure" />
      <relation from="o8703" id="r637" name="fruiting" negation="false" src="d0_s11" to="o8704" />
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals oblong-obovate, (1.5–) 1.8–2.5 × 0.8–1.3 mm;</text>
      <biological_entity id="o8705" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8706" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong-obovate" value_original="oblong-obovate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_length" src="d0_s12" to="1.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals white, obovate, (2.3) 2.5–3.6 (–4) × 1.5–2.2 mm, claw 0.5–1 mm;</text>
      <biological_entity id="o8707" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o8708" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="3.6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="4" to_unit="mm" />
        <character name="atypical_length" src="d0_s13" unit="mm" value="2.3" value_original="2.3" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s13" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s13" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8709" name="claw" name_original="claw" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens (2 or) 4 (or 6), median and lateral when 4, (erect);</text>
      <biological_entity id="o8710" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o8711" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="4" value_original="4" />
        <character is_modifier="false" name="position" src="d0_s14" value="median" value_original="median" />
        <character is_modifier="false" modifier="when 4" name="position" src="d0_s14" value="lateral" value_original="lateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments 1.7–2.5 mm, (glabrous);</text>
      <biological_entity id="o8712" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o8713" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 0.5–0.8 mm.</text>
      <biological_entity id="o8714" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o8715" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits ovate, (3–) 3.2–4 (–4.4) × 2–3.5 mm, apically winged, apical notch 0.1–0.3 mm deep;</text>
      <biological_entity id="o8716" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s17" to="3.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s17" to="4.4" to_unit="mm" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s17" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s17" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="apically" name="architecture" src="d0_s17" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="apical" id="o8717" name="notch" name_original="notch" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s17" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s17" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves thin, smooth, not veined, glabrous;</text>
      <biological_entity id="o8718" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="width" src="d0_s18" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s18" value="veined" value_original="veined" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>style 0.5–0.8 (–1) mm, exserted beyond apical notch.</text>
      <biological_entity id="o8719" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s19" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o8720" name="notch" name_original="notch" src="d0_s19" type="structure" />
      <relation from="o8719" id="r638" name="exserted beyond" negation="false" src="d0_s19" to="o8720" />
    </statement>
    <statement id="d0_s20">
      <text>Seeds ovate, 1.8–2 × 0.9–1.1 mm.</text>
      <biological_entity id="o8721" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s20" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s20" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline and saline meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="saline" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>The circumscription of Lepidium integrifolium is somewhat controversial. C. L. Hitchcock (1936) treated it as two varieties or (Hitchcock 1950) two subspecies of L. montanum, whereas R. C. Rollins (1993) treated it as a distinct species with two varieties. Rollins indicated that the species has two stamens, but such occurrence is rather rare. Most commonly, it has four stamens and is readily distinguished from related species by having four nectar glands and sepals sparsely pubescent subapically with crisped trichomes. Nothing is known about the populational variation of stamen number in the species and whether one or more taxa are involved.</discussion>
  
</bio:treatment>