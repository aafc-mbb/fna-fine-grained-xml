<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">237</other_info_on_meta>
    <other_info_on_meta type="mention_page">241</other_info_on_meta>
    <other_info_on_meta type="treatment_page">665</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="A. Gray" date="1849" rank="genus">SYNTHLIPSIS</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 116. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus SYNTHLIPSIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek synthlipsis, compression, alluding to flattened fruits</other_info_on_name>
    <other_info_on_name type="fna_id">132129</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials;</text>
      <biological_entity id="o38402" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>(short-lived, sometimes cespitose);</text>
    </statement>
    <statement id="d0_s2">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s3">
      <text>pubescent throughout, trichomes dendritic, base of plant sometimes mixed with fewer, simple or forked, stalked ones.</text>
      <biological_entity id="o38403" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o38405" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="dendritic" value_original="dendritic" />
      </biological_entity>
      <biological_entity id="o38406" name="base" name_original="base" src="d0_s3" type="structure">
        <character constraint="with fewer , simple or forked , stalked ones" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s3" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o38407" name="plant" name_original="plant" src="d0_s3" type="structure" />
      <relation from="o38406" id="r2586" name="part_of" negation="false" src="d0_s3" to="o38407" />
    </statement>
    <statement id="d0_s4">
      <text>Stems ascending to decumbent, unbranched or branched distally.</text>
      <biological_entity id="o38408" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s4" to="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate or sessile;</text>
      <biological_entity id="o38409" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal rosulate, petiolate, blade margins usually sinuately lobed to dentate, rarely repand;</text>
      <biological_entity constraint="basal" id="o38410" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o38411" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="usually sinuately lobed" name="shape" src="d0_s7" to="dentate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s7" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cauline petiolate or sessile, blade similar to basal.</text>
      <biological_entity constraint="cauline" id="o38412" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o38413" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity constraint="basal" id="o38414" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <relation from="o38413" id="r2587" name="to" negation="false" src="d0_s8" to="o38414" />
    </statement>
    <statement id="d0_s9">
      <text>Racemes (corymbose, several-flowered), slightly or considerably elongated in fruit.</text>
      <biological_entity id="o38416" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels divaricate or divaricate-ascending, slender.</text>
      <biological_entity id="o38415" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o38416" is_modifier="false" modifier="slightly; considerably" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o38417" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o38415" id="r2588" name="fruiting" negation="false" src="d0_s10" to="o38417" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals spreading, narrowly oblong, (equal), lateral pair not saccate basally;</text>
      <biological_entity id="o38418" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o38419" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s11" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals white to violet [purple], broadly obovate, (much longer than sepals), claw differentiated from blade, (much shorter, apex rounded);</text>
      <biological_entity id="o38420" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o38421" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="violet" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o38422" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character constraint="from blade" constraintid="o38423" is_modifier="false" name="variability" src="d0_s12" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o38423" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o38424" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o38425" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments not dilated basally, (somewhat spreading);</text>
      <biological_entity id="o38426" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o38427" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers linear [narrowly oblong];</text>
      <biological_entity id="o38428" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o38429" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands confluent, subtending bases of stamens.</text>
      <biological_entity id="o38430" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity constraint="nectar" id="o38431" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o38432" name="base" name_original="bases" src="d0_s16" type="structure" />
      <biological_entity id="o38433" name="stamen" name_original="stamens" src="d0_s16" type="structure" />
      <relation from="o38431" id="r2589" name="subtending" negation="false" src="d0_s16" to="o38432" />
      <relation from="o38431" id="r2590" name="part_of" negation="false" src="d0_s16" to="o38433" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits silicles, sessile, broadly oblong [broadly elliptic], smooth, angustiseptate;</text>
      <biological_entity constraint="fruits" id="o38434" name="silicle" name_original="silicles" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s17" value="angustiseptate" value_original="angustiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves carinate;</text>
      <biological_entity id="o38435" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="carinate" value_original="carinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>replum rounded;</text>
      <biological_entity id="o38436" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum complete;</text>
      <biological_entity id="o38437" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 10–50 per ovary;</text>
      <biological_entity id="o38438" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o38439" from="10" name="quantity" src="d0_s21" to="50" />
      </biological_entity>
      <biological_entity id="o38439" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>style distinct;</text>
      <biological_entity id="o38440" name="style" name_original="style" src="d0_s22" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s22" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>stigma broadly capitate, entire.</text>
      <biological_entity id="o38441" name="stigma" name_original="stigma" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="broadly" name="architecture_or_shape" src="d0_s23" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Seeds ± biseriate, flattened, not winged, narrowly margined, broadly ovate;</text>
      <biological_entity id="o38442" name="seed" name_original="seeds" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="more or less" name="arrangement" src="d0_s24" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" name="shape" src="d0_s24" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s24" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s24" value="margined" value_original="margined" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s24" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>seed-coat (nearly smooth), copiously mucilaginous when wetted;</text>
      <biological_entity id="o38443" name="seed-coat" name_original="seed-coat" src="d0_s25" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s25" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>cotyledons accumbent.</text>
    </statement>
    <statement id="d0_s27">
      <text>x = 10.</text>
      <biological_entity id="o38444" name="cotyledon" name_original="cotyledons" src="d0_s26" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s26" value="accumbent" value_original="accumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o38445" name="chromosome" name_original="" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex., n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>79.</number>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>Synthlipsis densiflora Rollins is known from Mexico (Coahuila).</discussion>
  <references>
    <reference>Rollins, R. C. 1959. The genus Synthlipsis (Cruciferae). Rhodora 61: 253–264.</reference>
  </references>
  
</bio:treatment>