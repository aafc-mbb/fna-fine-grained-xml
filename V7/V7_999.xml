<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">610</other_info_on_meta>
    <other_info_on_meta type="illustration_page">611</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="Greene" date="1900" rank="genus">nerisyrenia</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1900" rank="species">linearifolia</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>4: 225. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus nerisyrenia;species linearifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250095150</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Greggia</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">linearifolia</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>18: 191. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Greggia;species linearifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Greggia</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">camporum</taxon_name>
    <taxon_name authority="J. M. Coulter" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>genus Greggia;species camporum;variety angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Greggia</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="unknown" rank="species">camporum</taxon_name>
    <taxon_name authority="(S. Watson) M. E. Jones" date="unknown" rank="variety">linearifolia</taxon_name>
    <taxon_hierarchy>genus Greggia;species camporum;variety linearifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Parrasia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">linearifolia</taxon_name>
    <taxon_hierarchy>genus Parrasia;species linearifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants with woody caudex;</text>
      <biological_entity id="o29737" name="caudex" name_original="caudex" src="d0_s0" type="structure">
        <character is_modifier="true" name="texture" src="d0_s0" value="woody" value_original="woody" />
      </biological_entity>
      <relation from="o29736" id="r2000" name="with" negation="false" src="d0_s0" to="o29737" />
    </statement>
    <statement id="d0_s1">
      <text>moderately to densely pubescent.</text>
      <biological_entity id="o29736" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems (0.5–) 1–4 dm, woody proximally, densely to moderately pubescent.</text>
      <biological_entity id="o29738" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="4" to_unit="dm" />
        <character is_modifier="false" modifier="proximally" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character is_modifier="false" modifier="densely to moderately" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves sessile or nearly so;</text>
      <biological_entity constraint="cauline" id="o29739" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character name="architecture" src="d0_s3" value="nearly" value_original="nearly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear to linear-oblanceolate, 1.5–7 cm × 1–4.5 mm, (fleshy), base attenuate, margins usually entire, rarely dentate, apex acute to obtuse.</text>
      <biological_entity id="o29740" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s4" to="linear-oblanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29741" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o29742" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o29743" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes to 3.5 dm in fruit.</text>
      <biological_entity id="o29745" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels 0.6–1.4 cm, densely pubescent.</text>
      <biological_entity id="o29744" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in fruit" constraintid="o29745" from="0" from_unit="dm" name="some_measurement" src="d0_s5" to="3.5" to_unit="dm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s6" to="1.4" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o29746" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <relation from="o29744" id="r2001" name="fruiting" negation="false" src="d0_s6" to="o29746" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals broadly lanceolate to ovate, 4.8–7.5 × 1–2 mm;</text>
      <biological_entity id="o29747" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o29748" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="broadly lanceolate" name="shape" src="d0_s7" to="ovate" />
        <character char_type="range_value" from="4.8" from_unit="mm" name="length" src="d0_s7" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals obovate to spatulate, 8–12 (–13) × 5–8.5 mm, (often flattened), claw to 2 mm, (margin dentate);</text>
      <biological_entity id="o29749" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o29750" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="spatulate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="13" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s8" to="12" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29751" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments 4–6 mm;</text>
      <biological_entity id="o29752" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o29753" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 2.5–3.5 mm.</text>
      <biological_entity id="o29754" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o29755" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits terete to slightly angustiseptate, 0.9–3 cm × 1–2.2 mm;</text>
      <biological_entity id="o29756" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" from="terete" name="shape" src="d0_s11" to="slightly angustiseptate" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="length" src="d0_s11" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 30–80 per ovary;</text>
      <biological_entity id="o29757" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o29758" from="30" name="quantity" src="d0_s12" to="80" />
      </biological_entity>
      <biological_entity id="o29758" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>style 0.9–4 mm.</text>
      <biological_entity id="o29759" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 0.5–0.7 × 0.4–0.5 mm. 2n = 18, 19, 20, 34, 36.</text>
      <biological_entity id="o29760" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s14" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29761" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
        <character name="quantity" src="d0_s14" value="19" value_original="19" />
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
        <character name="quantity" src="d0_s14" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Dec.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Dec" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gypsum soils in knolls, bluffs, open flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gypsum soils" constraint="in knolls , bluffs ," />
        <character name="habitat" value="knolls" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="open flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1000-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="1000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico (Coahuila, Nuevo León, San Luis Potosí, Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
        <character name="distribution" value="Mexico (San Luis Potosí)" establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Both J. D. Bacon (1978) and R. C. Rollins (1993) recognized two weakly defined varieties of Nerisyrenia linearifolia distinguished primarily on the position of the widest portion of the fruit. Of those, var. mexicana Bacon (Coahuila, Mexico) has fruits widest near the base instead of the middle.</discussion>
  
</bio:treatment>