<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">687</other_info_on_meta>
    <other_info_on_meta type="treatment_page">688</other_info_on_meta>
    <other_info_on_meta type="illustration_page">686</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="genus">dryopetalon</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="species">runcinatum</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 12, plate 11. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus dryopetalon;species runcinatum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250094983</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Coelophragmus</taxon_name>
    <taxon_name authority="(B. L. Robinson) O. E. Schulz" date="unknown" rank="species">umbrosus</taxon_name>
    <taxon_hierarchy>genus Coelophragmus;species umbrosus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryopetalon</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="species">runcinatum</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="variety">laxiflorum</taxon_name>
    <taxon_hierarchy>genus Dryopetalon;species runcinatum;variety laxiflorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisymbrium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">umbrosum</taxon_name>
    <taxon_hierarchy>genus Sisymbrium;species umbrosum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually sparsely to densely hirsute proximally, rarely glabrous throughout.</text>
      <biological_entity id="o31697" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sparsely to densely; proximally" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="rarely; throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="sparsely to densely; proximally" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, unbranched or branched basally, branched distally, 2–8 dm.</text>
      <biological_entity id="o31699" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="8" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: petiole (1–) 2–6 (–8) cm;</text>
      <biological_entity constraint="basal" id="o31700" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o31701" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade spatulate to obovate in outline, lyrate, pinnatifid, or bipinnately lobed, (2–) 4–20 (–25) cm × 30–80 mm, surfaces glabrous or hirsute.</text>
      <biological_entity constraint="basal" id="o31702" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o31703" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="in outline" constraintid="o31704" from="spatulate" name="shape" src="d0_s4" to="obovate" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="lyrate" value_original="lyrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="bipinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="bipinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s4" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="20" to_unit="cm" />
        <character char_type="range_value" from="30" from_unit="mm" name="width" src="d0_s4" to="80" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31704" name="outline" name_original="outline" src="d0_s4" type="structure" />
      <biological_entity id="o31705" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves shortly petiolate or sessile;</text>
      <biological_entity constraint="cauline" id="o31706" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade base not auriculate, similar to basal, smaller distally.</text>
      <biological_entity constraint="blade" id="o31707" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="position" src="d0_s6" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes lax or dense.</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels divaricate-ascending, 5–15 (–20) mm, glabrous or pubescent.</text>
      <biological_entity id="o31708" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o31709" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o31708" id="r2119" name="fruiting" negation="false" src="d0_s8" to="o31709" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals (2.5–) 3.5–4.5 (–5) × 1.5–2 mm;</text>
      <biological_entity id="o31710" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o31711" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" src="d0_s9" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white to purplish, spatulate, 6–9 (–11) × 2.5–3.5 (–4.5) mm, claw 2.5–4 (–5) mm, margins pinnatifid, 5–7 (–11) -lobed, papillate basally;</text>
      <biological_entity id="o31712" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o31713" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s10" to="purplish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="11" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="9" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s10" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31714" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31715" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s10" value="5-7(-11)-lobed" value_original="5-7(-11)-lobed" />
        <character is_modifier="false" modifier="basally" name="relief" src="d0_s10" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments papillate basally, median pairs 3–5 mm, lateral pair 2.5–4 mm;</text>
      <biological_entity id="o31716" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o31717" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="basally" name="relief" src="d0_s11" value="papillate" value_original="papillate" />
        <character is_modifier="false" name="position" src="d0_s11" value="median" value_original="median" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers oblong, 0.7–1 mm.</text>
      <biological_entity id="o31718" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o31719" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits linear, (straight or arcuate), 2–6 cm × 0.5–1.2 mm;</text>
      <biological_entity id="o31720" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s13" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovules 60–110 per ovary;</text>
      <biological_entity id="o31721" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o31722" from="60" name="quantity" src="d0_s14" to="110" />
      </biological_entity>
      <biological_entity id="o31722" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style 0.1–0.7 (–1) mm.</text>
      <biological_entity id="o31723" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s15" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 0.6–0.8 × 0.4–0.6 mm, not winged.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 24.</text>
      <biological_entity id="o31724" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="length" src="d0_s16" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s16" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31725" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jan–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Jan" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ledges, shade of boulders and cliffs, foothills, canyons, scrub woodlands, streambeds, rocky basalt, crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ledges" />
        <character name="habitat" value="shade" constraint="of boulders and cliffs" />
        <character name="habitat" value="boulders" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="foothills" />
        <character name="habitat" value="canyons" />
        <character name="habitat" value="scrub woodlands" />
        <character name="habitat" value="streambeds" />
        <character name="habitat" value="rocky basalt" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico (Chihuahua, Sinaloa, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sinaloa)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Dryopetalon runcinatum is easily distinguished from other mustards in the flora area by having divided petals. In the United States, it is restricted to Cochise, Pima, Pinal, and Santa Cruz counties in Arizona, Catron, Dona Ana, Hidalgo, and Luna counties in New Mexico, and El Paso County in Texas.</discussion>
  
</bio:treatment>