<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="mention_page">482</other_info_on_meta>
    <other_info_on_meta type="treatment_page">481</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cardamine</taxon_name>
    <taxon_name authority="O. E. Schulz" date="1903" rank="species">penduliflora</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Jahrb. Syst.</publication_title>
      <place_in_publication>32: 538. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus cardamine;species penduliflora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094615</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="M. Peck" date="unknown" rank="species">rariflora</taxon_name>
    <taxon_hierarchy>genus Cardamine;species rariflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>glabrous throughout.</text>
      <biological_entity id="o24790" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rhizomes (tuberiform, fragile), 4–9 (–11) mm diam., (fleshy).</text>
      <biological_entity id="o24791" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s2" to="11" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s2" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect or decumbent at base, unbranched, 2–6 (–7.5) dm.</text>
      <biological_entity id="o24792" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o24793" is_modifier="false" name="orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="7.5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s3" to="6" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o24793" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Rhizomal leaves 5–13-foliolate, (4–) 10–18 (–25) cm, leaflets petiolulate or subsessile;</text>
      <biological_entity constraint="rhizomal" id="o24794" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-13-foliolate" value_original="5-13-foliolate" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="25" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s4" to="18" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24795" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole (3–) 5–12 (–17) cm;</text>
      <biological_entity id="o24796" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="17" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s5" to="12" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral leaflets subsessile, blade similar to terminal, sometimes smaller;</text>
      <biological_entity constraint="lateral" id="o24797" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o24798" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>terminal leaflet (petiolule 0.4–0.7 cm), blade oblong to elliptic or ovate, (0.4–) 0.7–1.7 (–2) cm, base cuneate or obtuse, margins entire or obscurely 3-lobed.</text>
      <biological_entity constraint="terminal" id="o24799" name="leaflet" name_original="leaflet" src="d0_s7" type="structure" />
      <biological_entity id="o24800" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="elliptic or ovate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="0.7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s7" to="1.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24801" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o24802" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s7" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cauline leaves 2–6, 5–11-foliolate, petiolate, leaflets petiolulate or sessile;</text>
      <biological_entity constraint="cauline" id="o24803" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s8" to="6" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-11-foliolate" value_original="5-11-foliolate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o24804" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petiole 3–10 cm, base not auriculate;</text>
      <biological_entity id="o24805" name="petiole" name_original="petiole" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s9" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24806" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflets sessile, blade similar to terminal, smaller;</text>
      <biological_entity constraint="lateral" id="o24807" name="leaflet" name_original="leaflets" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o24808" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="size" src="d0_s10" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>terminal leaflet (petiolule 0.5–1.5 cm), blade narrowly ovate or oblong to oblanceolate, 1.5–3.5 cm × 2–15 mm, margins entire or toothed to 3-lobed.</text>
      <biological_entity constraint="terminal" id="o24809" name="leaflet" name_original="leaflet" src="d0_s11" type="structure" />
      <biological_entity id="o24810" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="oblanceolate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s11" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24811" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character char_type="range_value" from="toothed" name="shape" src="d0_s11" to="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Racemes ebracteate.</text>
    </statement>
    <statement id="d0_s13">
      <text>Fruiting pedicels ascending to divaricate, (10–) 20–40 (–60) mm.</text>
      <biological_entity id="o24812" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s13" value="divaricate" value_original="divaricate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="60" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24813" name="pedicel" name_original="pedicels" src="d0_s13" type="structure" />
      <relation from="o24812" id="r1685" name="fruiting" negation="false" src="d0_s13" to="o24813" />
    </statement>
    <statement id="d0_s14">
      <text>Flowers: sepals oblong to ovate, 3.5–5 × 1.8–2.5 mm, lateral pair saccate basally;</text>
      <biological_entity id="o24814" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o24815" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s14" to="ovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s14" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s14" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals white, obovate, 12–16 × 6–8 mm, (not clawed, apex rounded or subemarginate);</text>
      <biological_entity id="o24816" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o24817" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s15" to="16" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s15" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments: median pairs 6–7 mm, lateral pair 4–5 mm;</text>
      <biological_entity id="o24818" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="median" value_original="median" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers oblong, 1.5–1.8 mm.</text>
      <biological_entity id="o24819" name="filament" name_original="filaments" src="d0_s17" type="structure" />
      <biological_entity id="o24820" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits linear, 2.5–4.5 cm × 1.4–2 mm;</text>
      <biological_entity id="o24821" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s18" value="linear" value_original="linear" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s18" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s18" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 12–24 per ovary;</text>
      <biological_entity id="o24822" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o24823" from="12" name="quantity" src="d0_s19" to="24" />
      </biological_entity>
      <biological_entity id="o24823" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style 4–6 mm.</text>
      <biological_entity id="o24824" name="style" name_original="style" src="d0_s20" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s20" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds brown, oblong, 1.8–2 × 1–1.5 mm.</text>
      <biological_entity id="o24825" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s21" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s21" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s21" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shallow pools, wet grounds, marshes, meadows, creeks, channels, swampy woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shallow pools" />
        <character name="habitat" value="wet grounds" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="creeks" />
        <character name="habitat" value="channels" />
        <character name="habitat" value="swampy woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-150 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="150" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  <discussion>Cardamine penduliflora is known from Douglas County north into Benton, Lane, Marion, Polk, and Yamhill counties.</discussion>
  
</bio:treatment>