<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">724</other_info_on_meta>
    <other_info_on_meta type="treatment_page">727</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Rydberg" date="1907" rank="genus">thelypodiopsis</taxon_name>
    <taxon_name authority="(M. C. Johnston) Rollins" date="1976" rank="species">shinnersii</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>206: 13. 1976</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus thelypodiopsis;species shinnersii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094930</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisymbrium</taxon_name>
    <taxon_name authority="M. C. Johnston" date="unknown" rank="species">shinnersii</taxon_name>
    <place_of_publication>
      <publication_title>SouthW. Naturalist</publication_title>
      <place_in_publication>2: 129. 1957,</place_in_publication>
      <other_info_on_pub>based on Thelypodium vaseyi J. M. Coulter, Contr. U.S. Natl. Herb. 1: 30. 1890, not S. vaseyi S. Watson ex B. L. Robinson 1895</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Sisymbrium;species shinnersii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>(often glaucous), glabrous throughout.</text>
      <biological_entity id="o392" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unbranched or branched distally, 3.5–8 dm.</text>
      <biological_entity id="o393" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="3.5" from_unit="dm" name="some_measurement" src="d0_s2" to="8" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (soon withered);</text>
    </statement>
    <statement id="d0_s4">
      <text>not rosulate;</text>
      <biological_entity constraint="basal" id="o394" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole (winged), 0.5–3.5 cm;</text>
      <biological_entity id="o395" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade pandurate to broadly obovate, 2–10 cm × 10–50 mm, margins entire or repand.</text>
      <biological_entity id="o396" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="pandurate" name="shape" src="d0_s6" to="broadly obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="50" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o397" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves (proximalmost) petiolate or (distal) sessile;</text>
      <biological_entity constraint="cauline" id="o398" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade (proximalmost) pandurate to broadly obovate or (distal) obovate, base auriculate, margins entire or repand.</text>
      <biological_entity id="o399" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="pandurate" name="shape" src="d0_s8" to="broadly obovate or obovate" />
      </biological_entity>
      <biological_entity id="o400" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o401" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s8" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes lax.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels often divaricate, sometimes ascending, straight, 4–13 mm.</text>
      <biological_entity id="o402" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o403" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o402" id="r26" name="fruiting" negation="false" src="d0_s10" to="o403" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals erect, purplish, 2.5–3.5 × 0.7–1 mm;</text>
      <biological_entity id="o404" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o405" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals white, narrowly oblanceolate, 3.5–4.5 × 1–1.2 mm, claw not developed;</text>
      <biological_entity id="o406" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o407" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s12" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o408" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="development" src="d0_s12" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>median filament pairs 1.5–2.5 mm;</text>
      <biological_entity id="o409" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity constraint="median" id="o410" name="filament" name_original="filament" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ovate-oblong, 0.7–1 mm;</text>
      <biological_entity id="o411" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o412" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate-oblong" value_original="ovate-oblong" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>gynophore 0.2–0.4 mm.</text>
      <biological_entity id="o413" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o414" name="gynophore" name_original="gynophore" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s15" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits divaricate to ascending, straight or curved, obscurely torulose, 3.2–7.5 cm × 0.9–1.2 mm;</text>
      <biological_entity id="o415" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s16" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s16" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="3.2" from_unit="cm" name="length" src="d0_s16" to="7.5" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s16" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 60–102 per ovary;</text>
      <biological_entity id="o416" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o417" from="60" name="quantity" src="d0_s17" to="102" />
      </biological_entity>
      <biological_entity id="o417" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style subclavate, 0.4–2.5 mm;</text>
      <biological_entity id="o418" name="style" name_original="style" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="subclavate" value_original="subclavate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s18" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stigma entire.</text>
      <biological_entity id="o419" name="stigma" name_original="stigma" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s19" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 0.9–1.1 × 0.5–0.7 mm.</text>
      <biological_entity id="o420" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="length" src="d0_s20" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s20" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Canyon sides, rocky arroyo floors, chaparral thickets, scrubs, dry banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="canyon sides" />
        <character name="habitat" value="rocky arroyo floors" />
        <character name="habitat" value="chaparral thickets" />
        <character name="habitat" value="scrubs" />
        <character name="habitat" value="dry banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Tamaulipas).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <discussion>Thelypodiopsis shinnersii is known from Cameron County.</discussion>
  
</bio:treatment>