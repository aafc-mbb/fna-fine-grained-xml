<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">733</other_info_on_meta>
    <other_info_on_meta type="treatment_page">734</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Endlicher" date="1839" rank="genus">thelypodium</taxon_name>
    <taxon_name authority="(Nuttall) Endlicher in W. G. Walpers" date="1842" rank="species">integrifolium</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="1973" rank="subspecies">complanatum</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>204. 105, plate 20, fig. 1. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus thelypodium;species integrifolium;subspecies complanatum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250095226</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelypodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">integrifolium</taxon_name>
    <taxon_name authority="(Al-Shehbaz) S. L. Welsh &amp; Reveal" date="unknown" rank="variety">complanatum</taxon_name>
    <taxon_hierarchy>genus Thelypodium;species integrifolium;variety complanatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Racemes (often strongly congested), not elongated in fruit;</text>
      <biological_entity id="o24117" name="raceme" name_original="racemes" src="d0_s0" type="structure">
        <character constraint="in fruit" constraintid="o24118" is_modifier="false" modifier="not" name="length" src="d0_s0" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o24118" name="fruit" name_original="fruit" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>central rachis (0.6–) 1.2–7 (–9) cm.</text>
    </statement>
    <statement id="d0_s2">
      <text>Fruiting pedicels horizontal, straight, not whitish, stout, 3–5 (–6) mm, strongly flattened at base.</text>
      <biological_entity constraint="central" id="o24119" name="rachis" name_original="rachis" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="1.2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="9" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s1" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o24120" name="pedicel" name_original="pedicels" src="d0_s2" type="structure" />
      <biological_entity id="o24121" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s2" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s2" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o24119" id="r1656" name="fruiting" negation="false" src="d0_s2" to="o24120" />
    </statement>
    <statement id="d0_s3">
      <text>Flowers: petals usually lavender, rarely white, (5–) 6–8.5 (–9.5) mm;</text>
      <biological_entity id="o24122" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o24123" name="petal" name_original="petals" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration_or_odor" src="d0_s3" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s3" value="white" value_original="white" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s3" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s3" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>gynophore stout, 0.5–1 (–3) mm.</text>
      <biological_entity id="o24124" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o24125" name="gynophore" name_original="gynophore" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruits horizontal or divaricate-ascending to ascending, straight or incurved, (0.9–) 1.4–2.7 (–3.4) cm.</text>
      <biological_entity id="o24126" name="fruit" name_original="fruits" src="d0_s5" type="structure">
        <character char_type="range_value" from="divaricate-ascending" name="orientation" src="d0_s5" to="ascending" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="0.9" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="1.4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="3.4" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="some_measurement" src="d0_s5" to="2.7" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alkaline areas, desert shrub communities, canyons</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alkaline areas" />
        <character name="habitat" value="desert shrub communities" />
        <character name="habitat" value="canyons" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800-2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6c.</number>
  
</bio:treatment>