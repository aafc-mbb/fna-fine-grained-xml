<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">486</other_info_on_meta>
    <other_info_on_meta type="treatment_page">487</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Torrey" date="1837" rank="genus">leavenworthia</taxon_name>
    <taxon_name authority="Rollins" date="1963" rank="species">crassa</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>192: 62. 1963</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus leavenworthia;species crassa</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094622</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leavenworthia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">crassa</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="variety">elongata</taxon_name>
    <taxon_hierarchy>genus Leavenworthia;species crassa;variety elongata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems (when present) 1–4 dm.</text>
      <biological_entity id="o431" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves: petiole (0.9–) 1.5–3 cm;</text>
      <biological_entity constraint="basal" id="o432" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o433" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="0.9" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s1" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blade 2.8–6.5 (–8) cm, lobes 1–8 on each side, margins entire or shallowly dentate, terminal lobe orbicular to broadly ovate, 0.5–1.2 (–2) cm × 5–11 (–18) mm, considerably larger than lateral lobes, margins entire or shallowly dentate.</text>
      <biological_entity constraint="basal" id="o434" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o435" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="2.8" from_unit="cm" name="some_measurement" src="d0_s2" to="6.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o436" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o437" from="1" name="quantity" src="d0_s2" to="8" />
      </biological_entity>
      <biological_entity id="o437" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o438" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o439" name="lobe" name_original="lobe" src="d0_s2" type="structure">
        <character char_type="range_value" from="orbicular" name="shape" src="d0_s2" to="broadly ovate" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s2" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s2" to="18" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s2" to="11" to_unit="mm" />
        <character constraint="than lateral lobes" constraintid="o440" is_modifier="false" name="size" src="d0_s2" value="considerably larger" value_original="considerably larger" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o440" name="lobe" name_original="lobes" src="d0_s2" type="structure" />
      <biological_entity id="o441" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Fruiting pedicels: solitary flowers 40–80 mm;</text>
      <biological_entity id="o443" name="pedicel" name_original="pedicels" src="d0_s3" type="structure" />
      <biological_entity id="o444" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s3" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="40" from_unit="mm" name="some_measurement" src="d0_s3" to="80" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>racemes 35–70 mm.</text>
      <biological_entity id="o446" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <biological_entity id="o447" name="raceme" name_original="racemes" src="d0_s4" type="structure">
        <character char_type="range_value" from="35" from_unit="mm" name="some_measurement" src="d0_s4" to="70" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals widely spreading, oblong-linear, 4–5.7 × 1–2.1 mm;</text>
      <biological_entity id="o448" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o449" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="oblong-linear" value_original="oblong-linear" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s5" to="5.7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals spreading, usually yellow, sometimes white, broadly spatulate to obovate, 9.5–14 × (2.5–) 3–6.7 (–8) mm, claw yellow to orange, 2.5–4 mm, apex deeply emarginate, apical notch 0.5–1 mm deep;</text>
      <biological_entity id="o450" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o451" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character char_type="range_value" from="broadly spatulate" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="9.5" from_unit="mm" name="length" src="d0_s6" to="14" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_width" src="d0_s6" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="6.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o452" name="claw" name_original="claw" src="d0_s6" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s6" to="orange" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o453" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="deeply" name="architecture_or_shape" src="d0_s6" value="emarginate" value_original="emarginate" />
      </biological_entity>
      <biological_entity constraint="apical" id="o454" name="notch" name_original="notch" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s6" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments: median 4–6 mm, lateral 1.6–3 mm;</text>
      <biological_entity id="o455" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="median" value_original="median" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 0.9–1.3 mm.</text>
      <biological_entity id="o456" name="filament" name_original="filaments" src="d0_s8" type="structure" />
      <biological_entity id="o457" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s8" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits subglobose to oblong, (0.6–) 0.8–1.2 (–1.4) cm × (3.5–) 4–5 (–6) mm, smooth, subterete;</text>
      <biological_entity id="o458" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s9" to="oblong" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_length" src="d0_s9" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s9" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_width" src="d0_s9" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s9" value="subterete" value_original="subterete" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>valves thick;</text>
      <biological_entity id="o459" name="valve" name_original="valves" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovules 4–6 (–8) per ovary;</text>
      <biological_entity id="o460" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" name="atypical_quantity" src="d0_s11" to="8" />
        <character char_type="range_value" constraint="per ovary" constraintid="o461" from="4" name="quantity" src="d0_s11" to="6" />
      </biological_entity>
      <biological_entity id="o461" name="ovary" name_original="ovary" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>style 2.2–5 (–6) mm.</text>
      <biological_entity id="o462" name="style" name_original="style" src="d0_s12" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1.7–3.4 mm diam.;</text>
      <biological_entity id="o463" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="diameter" src="d0_s13" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>wing 0.2–0.4 mm wide;</text>
      <biological_entity id="o464" name="wing" name_original="wing" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s14" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>embryo straight or nearly so.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 22.</text>
      <biological_entity id="o465" name="embryo" name_original="embryo" src="d0_s15" type="structure">
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character name="course" src="d0_s15" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity constraint="2n" id="o466" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone cedar glades, pastures, fields, roadsides, near limestone sinks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone cedar glades" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" constraint="near limestone sinks" />
        <character name="habitat" value="limestone sinks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Leavenworthia crassa is known only from Lawrence and Morgan counties.</discussion>
  <references>
    <reference>Lloyd, D. G. 1967. The genetics of self-incompatibility in Leavenworthia crassa Rollins (Cruciferae). Genetica 38: 227–242.</reference>
    <reference>Lyons, E. E. and J. Antonovics. 1991. Breeding system evolution in Leavenworthia: Breeding system variation and reproductive success in natural populations of Leavenworthia crassa (Cruciferae). Amer. J. Bot. 78: 270–287.</reference>
  </references>
  
</bio:treatment>