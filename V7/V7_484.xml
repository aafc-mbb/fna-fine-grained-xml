<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">279</other_info_on_meta>
    <other_info_on_meta type="mention_page">330</other_info_on_meta>
    <other_info_on_meta type="treatment_page">345</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="A. Heller" date="1901" rank="species">viridis</taxon_name>
    <place_of_publication>
      <publication_title>Muhlenbergia</publication_title>
      <place_in_publication>1: 27. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species viridis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094770</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">petrophila</taxon_name>
    <taxon_name authority="(A. Heller) C. L. Hitchcock" date="unknown" rank="variety">viridis</taxon_name>
    <taxon_hierarchy>genus Draba;species petrophila;variety viridis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(long-lived, cespitose);</text>
      <biological_entity id="o39867" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex simple or branched (densely covered with persistent petioles);</text>
    </statement>
    <statement id="d0_s3">
      <text>not scapose.</text>
      <biological_entity id="o39868" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched or branched, 0.35–3 dm, pubescent throughout, trichomes simple, 0.2–0.8 mm, with short-stalked to subsessile, cruciform ones, 0.05–0.2 mm.</text>
      <biological_entity id="o39869" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.35" from_unit="dm" name="some_measurement" src="d0_s4" to="3" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o39870" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39871" name="one" name_original="ones" src="d0_s4" type="structure">
        <character char_type="range_value" from="short-stalked" is_modifier="true" name="architecture" src="d0_s4" to="subsessile" />
        <character is_modifier="true" name="shape" src="d0_s4" value="cruciform" value_original="cruciform" />
      </biological_entity>
      <relation from="o39870" id="r2698" name="with" negation="false" src="d0_s4" to="o39871" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves rosulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o39872" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole ciliate, (trichomes simple, 0.4–1 mm);</text>
      <biological_entity id="o39873" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblanceolate, 1–6 cm × 2–6 mm, margins usually entire, rarely denticulate, surfaces pubescent with stalked, cruciform trichomes, 0.1–0.5 mm.</text>
      <biological_entity id="o39874" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s8" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39875" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o39876" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character constraint="with trichomes" constraintid="o39877" is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39877" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
        <character is_modifier="true" name="shape" src="d0_s8" value="cruciform" value_original="cruciform" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 5–16;</text>
    </statement>
    <statement id="d0_s10">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o39878" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="16" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>blade ovate to lanceolate or oblong, margins entire or denticulate, surfaces pubescent as basal.</text>
      <biological_entity id="o39879" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s11" to="lanceolate or oblong" />
      </biological_entity>
      <biological_entity id="o39880" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o39881" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character constraint="as basal" constraintid="o39882" is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o39882" name="basal" name_original="basal" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Racemes 11–28-flowered, ebracteate, elongated in fruit;</text>
      <biological_entity id="o39883" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="11-28-flowered" value_original="11-28-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o39884" is_modifier="false" name="length" src="d0_s12" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o39884" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>rachis not flexuous, pubescent as stem.</text>
      <biological_entity id="o39886" name="stem" name_original="stem" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruiting pedicels divaricate, straight, 4–7 mm, pubescent throughout, trichomes simple (0.1–0.4 mm) and subsessile, 2–4-rayed (0.03–0.25 mm).</text>
      <biological_entity id="o39885" name="rachis" name_original="rachis" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o39886" is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o39887" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <biological_entity id="o39888" name="whole-organism" name_original="" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o39889" name="trichome" name_original="trichomes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <relation from="o39885" id="r2699" name="fruiting" negation="false" src="d0_s14" to="o39887" />
    </statement>
    <statement id="d0_s15">
      <text>Flowers: sepals oblong, 2–3 mm, pubescent, (trichomes simple and 2-rayed);</text>
      <biological_entity id="o39890" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o39891" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals yellow, oblanceolate, 3.5–5 × 1–1.5 mm;</text>
      <biological_entity id="o39892" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o39893" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s16" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers ovate, 0.5–0.6 mm.</text>
      <biological_entity id="o39894" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o39895" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits lanceolate to ovate, often plane, flattened, 3.5–8 × 2–3 mm;</text>
      <biological_entity id="o39896" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s18" to="ovate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s18" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s18" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s18" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>valves pubescent, trichomes (vertical), simple and 2–4-rayed, (0.1–) 0.3–0.8 mm;</text>
      <biological_entity id="o39897" name="valve" name_original="valves" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o39898" name="trichome" name_original="trichomes" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="0.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s19" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 12–20 per ovary;</text>
      <biological_entity id="o39899" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o39900" from="12" name="quantity" src="d0_s20" to="20" />
      </biological_entity>
      <biological_entity id="o39900" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style (1.5–) 2–3 mm.</text>
      <biological_entity id="o39901" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_some_measurement" src="d0_s21" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s21" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds ovoid, 1.2–1.5 × 0.7–0.9 mm.</text>
      <biological_entity id="o39902" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s22" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s22" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Aug–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Aug" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open stands of pines</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open stands" constraint="of pines" />
        <character name="habitat" value="pines" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="2600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.; Mexico (Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>118.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Draba viridis has been reported from the Huachuca and Santa Catalina mountains in southern Arizona, and the adjacent San Jose Mountains in Sonora, Mexico. It was reduced to a variety of D. petrophila by Hitchcock and a form of D. helleriana by O. E. Schulz, but is easily separated from both (I. A. Al-Shehbaz and M. D. Windham 2007).</discussion>
  
</bio:treatment>