<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">277</other_info_on_meta>
    <other_info_on_meta type="mention_page">333</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="treatment_page">332</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Rollins" date="1984" rank="species">ramulosa</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>214: 6. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species ramulosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094800</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(loosely matted, grayish);</text>
      <biological_entity id="o15064" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (with persistent leaf-bases, branches creeping, sometimes terminating in sterile rosettes);</text>
    </statement>
    <statement id="d0_s3">
      <text>not scapose.</text>
      <biological_entity id="o15065" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, 0.4–0.6 dm, densely pubescent throughout, trichomes dendritic, 3–6-rayed, (often crisped), 0.1–0.4 mm.</text>
      <biological_entity id="o15066" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s4" to="0.6" to_unit="dm" />
        <character is_modifier="false" modifier="densely; throughout" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15067" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="dendritic" value_original="dendritic" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves (imbricate);</text>
    </statement>
    <statement id="d0_s6">
      <text>not rosulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>sessile;</text>
      <biological_entity constraint="basal" id="o15068" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade obovate to oblanceolate, 0.4–1.1 cm × 2–3.2 mm, margins entire, (base and margins not ciliate), surfaces pubescent with stalked, 4–8-rayed trichomes, 0.1–0.5 mm, (sometimes 1 or more rays spurred), adaxially sometimes trichomes simple.</text>
      <biological_entity id="o15069" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="oblanceolate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s8" to="1.1" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15070" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15071" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character constraint="with trichomes" constraintid="o15072" is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15072" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o15073" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 2 or 3 (sometimes basal leaves spaced, flowering-stem appearing to 8-leaved);</text>
    </statement>
    <statement id="d0_s10">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o15074" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s9" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>blade oblong to ovate, margins entire, surfaces pubescent as basal.</text>
      <biological_entity id="o15075" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="ovate" />
      </biological_entity>
      <biological_entity id="o15076" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o15077" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character constraint="as basal" constraintid="o15078" is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15078" name="basal" name_original="basal" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Racemes 4–15-flowered, ebracteate or proximalmost 1 or 2 flowers bracteate, slightly elongated in fruit;</text>
      <biological_entity id="o15079" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="4-15-flowered" value_original="4-15-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="position" src="d0_s12" value="proximalmost" value_original="proximalmost" />
        <character name="quantity" src="d0_s12" unit="or flowers" value="1" value_original="1" />
        <character name="quantity" src="d0_s12" unit="or flowers" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="bracteate" value_original="bracteate" />
        <character constraint="in fruit" constraintid="o15080" is_modifier="false" modifier="slightly" name="length" src="d0_s12" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o15080" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>rachis not flexuous, pubescent as stem.</text>
      <biological_entity id="o15082" name="stem" name_original="stem" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruiting pedicels divaricate-ascending, usually straight, rarely curved upward, 3–6 (–10) mm, pubescent, trichomes 3–6-rayed, (crisped, 0.1–0.4 mm), and, sometimes, simple.</text>
      <biological_entity id="o15081" name="rachis" name_original="rachis" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o15082" is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15083" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <biological_entity id="o15084" name="whole-organism" name_original="" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15085" name="trichome" name_original="trichomes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s14" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o15081" id="r1052" name="fruiting" negation="false" src="d0_s14" to="o15083" />
    </statement>
    <statement id="d0_s15">
      <text>Flowers: sepals broadly ovate, 1.7–2.4 mm, pubescent, (trichomes short-stalked, 2–5-rayed);</text>
      <biological_entity id="o15086" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o15087" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s15" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals yellow, obovate to oblanceolate, 3–4.5 × 1.5–2.5 mm;</text>
      <biological_entity id="o15088" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o15089" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s16" to="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s16" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers ovate, 0.4–0.5 mm.</text>
      <biological_entity id="o15090" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o15091" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits (not appressed to rachis), ovate to elliptic, plane, flattened, 4–6.5 × 2.5–4 mm;</text>
      <biological_entity id="o15092" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s18" to="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s18" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s18" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s18" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>valves densely pubescent, trichomes simple and short-stalked, 2–5-rayed, 0.08–0.35 mm;</text>
      <biological_entity id="o15093" name="valve" name_original="valves" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15094" name="trichome" name_original="trichomes" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.08" from_unit="mm" name="some_measurement" src="d0_s19" to="0.35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 6–12 per ovary;</text>
      <biological_entity id="o15095" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o15096" from="6" name="quantity" src="d0_s20" to="12" />
      </biological_entity>
      <biological_entity id="o15096" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style (0.1–) 0.3–0.7 mm.</text>
      <biological_entity id="o15097" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="atypical_some_measurement" src="d0_s21" to="0.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s21" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds oblong, 1.4–1.8 × 0.8–1.2 mm.</text>
      <biological_entity id="o15098" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s22" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s22" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops, talus, gravelly soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="gravelly soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>3300-3600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3600" to_unit="m" from="3300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>90.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Molecular and chromosomal data (M. D. Windham, unpubl.) strongly suggest that Draba ramulosa is an allopolyploid species. It is thought to have originated through hybridization between D. sobolifera and a member of the white-flowered, euploid lineage of M. A. Beilstein and M. D. Windham (2003). It is easily distinguished from D. sobolifera by having pale yellow to whitish (versus bright yellow) petals, grayish (versus green) foliage, non-ciliate (versus ciliate) basal leaves pubescent with 4–8-rayed (versus 2–4-rayed) trichomes, often proximally bracteate (versus ebracteate) racemes, and flattened (versus inflated basally) fruits. Draba ramulosa is known from the Tushar Mountains in south-central Utah (Beaver and Piute counties).</discussion>
  
</bio:treatment>