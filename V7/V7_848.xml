<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">535</other_info_on_meta>
    <other_info_on_meta type="treatment_page">537</other_info_on_meta>
    <other_info_on_meta type="illustration_page">532</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">erysimeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erysimum</taxon_name>
    <taxon_name authority="(Nuttall) de Candolle" date="1821" rank="species">asperum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>2: 505. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe erysimeae;genus erysimum;species asperum</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250095073</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheiranthus</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">asper</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>2: 69. 1818</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cheiranthus;species asper;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheirinia</taxon_name>
    <taxon_name authority="(Nuttall) Rydberg" date="unknown" rank="species">aspera</taxon_name>
    <taxon_hierarchy>genus Cheirinia;species aspera;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erysimum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">asperum</taxon_name>
    <taxon_name authority="O. E. Schulz" date="unknown" rank="variety">dolichocarpum</taxon_name>
    <taxon_hierarchy>genus Erysimum;species asperum;variety dolichocarpum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials.</text>
      <biological_entity id="o38980" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Trichomes of leaves 2-rayed or 3-rayed.</text>
      <biological_entity id="o38981" name="trichome" name_original="trichomes" src="d0_s1" type="structure" />
      <biological_entity id="o38982" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <relation from="o38981" id="r2630" name="part_of" negation="false" src="d0_s1" to="o38982" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, unbranched or branched distally, (0.6–) 1.2–6.5 (–8) dm.</text>
      <biological_entity id="o38983" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="1.2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="8" to_unit="dm" />
        <character char_type="range_value" from="1.2" from_unit="dm" name="some_measurement" src="d0_s2" to="6.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (often withered by fruiting);</text>
      <biological_entity constraint="basal" id="o38984" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate, 2–10 cm × (0.2–) 0.5–1.5 (–2.4) mm, base attenuate, margins dentate, apex acute.</text>
      <biological_entity id="o38985" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="atypical_width" src="d0_s4" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38986" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o38987" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o38988" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves (distal) sessile;</text>
      <biological_entity constraint="cauline" id="o38989" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade margins entire or denticulate.</text>
      <biological_entity constraint="blade" id="o38990" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="denticulate" value_original="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes considerably elongated in fruit.</text>
      <biological_entity id="o38992" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels horizontal to divaricate, slender, narrower than fruit, 5–16 (–25) mm.</text>
      <biological_entity id="o38991" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o38992" is_modifier="false" modifier="considerably" name="length" src="d0_s7" value="elongated" value_original="elongated" />
        <character constraint="than fruit" constraintid="o38994" is_modifier="false" name="width" src="d0_s8" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o38993" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <biological_entity id="o38994" name="fruit" name_original="fruit" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="25" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="16" to_unit="mm" />
      </biological_entity>
      <relation from="o38991" id="r2631" name="fruiting" negation="false" src="d0_s8" to="o38993" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals oblong to linear-oblong, 8–12 mm, lateral pair slightly saccate basally;</text>
      <biological_entity id="o38995" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o38996" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s9" to="linear-oblong" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="slightly; basally" name="architecture_or_shape" src="d0_s9" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals yellow, obovate to suborbicular, 13–22 × 4–9 mm, claw 8–15 mm, apex rounded;</text>
      <biological_entity id="o38997" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o38998" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s10" to="suborbicular" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s10" to="22" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o38999" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39000" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>median filaments 8–14 mm;</text>
      <biological_entity id="o39001" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="median" id="o39002" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers linear, 2.5–4 mm.</text>
      <biological_entity id="o39003" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o39004" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits widely spreading or divaricate, narrowly linear, usually straight, rarely curved upward, not torulose, (3–) 5–12 (–14) cm × 1.2–2.7 mm, 4-angled, strongly (longitudinally) 4-striped;</text>
      <biological_entity id="o39005" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="widely" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement" src="d0_s13" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s13" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_length" src="d0_s13" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s13" to="14" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s13" to="12" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s13" to="2.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" modifier="strongly" name="coloration" src="d0_s13" value="4-striped" value_original="4-striped" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>valves with prominent midvein and replum, densely pubescent outside, trichomes 2-rayed between midvein and replum, glabrous inside;</text>
      <biological_entity id="o39006" name="valve" name_original="valves" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o39007" name="midvein" name_original="midvein" src="d0_s14" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s14" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o39008" name="replum" name_original="replum" src="d0_s14" type="structure" />
      <biological_entity constraint="between replum midvein and" constraintid="o39010-o39011" id="o39009" name="trichome" name_original="trichomes" src="d0_s14" type="structure" constraint_original="between  replum midvein and, ">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o39010" name="midvein" name_original="midvein" src="d0_s14" type="structure" />
      <biological_entity id="o39011" name="replum" name_original="replum" src="d0_s14" type="structure" />
      <relation from="o39006" id="r2632" name="with" negation="false" src="d0_s14" to="o39007" />
      <relation from="o39006" id="r2633" name="with" negation="false" src="d0_s14" to="o39008" />
    </statement>
    <statement id="d0_s15">
      <text>ovules 72–120 per ovary;</text>
      <biological_entity id="o39012" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o39013" from="72" name="quantity" src="d0_s15" to="120" />
      </biological_entity>
      <biological_entity id="o39013" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style cylindrical, slender, 1–4 mm, sparsely pubescent;</text>
      <biological_entity id="o39014" name="style" name_original="style" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="size" src="d0_s16" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigma slightly 2-lobed, lobes as long as wide.</text>
      <biological_entity id="o39015" name="stigma" name_original="stigma" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o39016" name="lobe" name_original="lobes" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds ovoid, (1–) 1.3–2.3 × 0.7–1.2 mm;</text>
    </statement>
    <statement id="d0_s19">
      <text>usually not winged, rarely winged distally.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 36.</text>
      <biological_entity id="o39017" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_length" src="d0_s18" to="1.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s18" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s18" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s19" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="rarely; distally" name="architecture" src="d0_s19" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="2n" id="o39018" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun(-Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
        <character name="flowering time" char_type="atypical_range" to="Aug" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies, sand dunes, roadsides, bluffs, sandhills along stream banks, knolls, open plains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="sandhills" constraint="along stream banks , knolls" />
        <character name="habitat" value="stream banks" />
        <character name="habitat" value="knolls" />
        <character name="habitat" value="open plains" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., Man., Sask.; Ark., Colo., Ill., Kans., Minn., Mont., Nebr., N.Mex., N.Dak., Okla., S.Dak., Tex., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>