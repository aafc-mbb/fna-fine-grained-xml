<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">465</other_info_on_meta>
    <other_info_on_meta type="treatment_page">476</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cardamine</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">impatiens</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 655. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus cardamine;species impatiens</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200009318</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">impatiens</taxon_name>
    <taxon_name authority="O. E. Schulz" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>genus Cardamine;species impatiens;variety angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or, rarely, annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually glabrous, rarely sparsely pubescent basally.</text>
      <biological_entity id="o4523" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely; basally" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rhizomes absent.</text>
      <biological_entity id="o4525" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, (angled, sometimes flexuous), unbranched basally, usually branched distally, (1.2–) 2–6.5 (–9) dm.</text>
      <biological_entity id="o4526" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="1.2" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="9" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s3" to="6.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves (often withered by flowering), rosulate, similar to cauline, with fewer distal leaflets.</text>
      <biological_entity constraint="basal" id="o4527" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o4528" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="distal" id="o4529" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="fewer" value_original="fewer" />
      </biological_entity>
      <relation from="o4527" id="r334" name="with" negation="false" src="d0_s4" to="o4529" />
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves 9–24, (9–) 13–25-foliolate, petiolate, leaflets petiolulate;</text>
      <biological_entity constraint="cauline" id="o4530" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="9" name="quantity" src="d0_s5" to="24" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="(9-)13-25-foliolate" value_original="(9-)13-25-foliolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o4531" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolulate" value_original="petiolulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petiole 2–6 cm, base auriculate (auricles to 10 × 2.2 mm);</text>
      <biological_entity id="o4532" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4533" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral leaflets similar to terminal, blade often smaller;</text>
      <biological_entity constraint="lateral" id="o4534" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o4535" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="size" src="d0_s7" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>terminal leaflet (petiolule to 0.5 cm), blade orbicular, obovate, ovate, or lanceolate, 1–4 (–5) cm × 5–17 mm, margins entire or 3–5 (–9) -toothed or lobed.</text>
      <biological_entity constraint="terminal" id="o4536" name="leaflet" name_original="leaflet" src="d0_s8" type="structure" />
      <biological_entity id="o4537" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s8" to="4" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s8" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4538" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s8" value="3-5(-9)-toothed" value_original="3-5(-9)-toothed" />
        <character is_modifier="false" name="shape" src="d0_s8" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes ebracteate.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels divaricate or ascending, 3.5–12 (–15) mm.</text>
      <biological_entity id="o4539" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4540" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o4539" id="r335" name="fruiting" negation="false" src="d0_s10" to="o4540" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals oblong, 1.2–2 (–2.5) × 0.7–1 (–1.2) mm, lateral pair not saccate basally;</text>
      <biological_entity id="o4541" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4542" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s11" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="1" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s11" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals (rarely absent), white, oblanceolate, 1.5–4 (–5) × 0.6–1.2 mm;</text>
      <biological_entity id="o4543" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4544" name="petal" name_original="petals" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>filaments 2–3 (–4) mm;</text>
      <biological_entity id="o4545" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o4546" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers ovate, 0.3–0.5 mm.</text>
      <biological_entity id="o4547" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o4548" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits linear, (torulose), (1–) 1.6–3 (–3.5) cm × 0.9–1.5 mm;</text>
    </statement>
    <statement id="d0_s16">
      <text>(valves glabrous or, rarely, pilose);</text>
      <biological_entity id="o4549" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s15" to="1.6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s15" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="cm" name="length" src="d0_s15" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 10–30 per ovary;</text>
      <biological_entity id="o4550" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o4551" from="10" name="quantity" src="d0_s17" to="30" />
      </biological_entity>
      <biological_entity id="o4551" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 0.6–1.6 (–2) mm.</text>
      <biological_entity id="o4552" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s18" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s18" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds brown, oblong, 1.1–1.5 × 0.8–1 mm, (compressed, sometimes narrowly winged apically).</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 16.</text>
      <biological_entity id="o4553" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s19" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4554" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Streamsides, slopes, roadsides, fields, disturbed areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="streamsides" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="disturbed areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont.; Conn., Ky., Mich., Minn., N.H., Ohio, Pa., Va., W.Va.; Eurasia; introduced also in South Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="also in South Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>20.</number>
  
</bio:treatment>