<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">535</other_info_on_meta>
    <other_info_on_meta type="treatment_page">539</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">erysimeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">erysimum</taxon_name>
    <taxon_name authority="(Linnaeus) Crantz" date="1769" rank="species">cheiri</taxon_name>
    <place_of_publication>
      <publication_title>Cl. Crucif. Emend.,</publication_title>
      <place_in_publication>116. 1769</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe erysimeae;genus erysimum;species cheiri</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250009726</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheiranthus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">cheiri</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 661. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cheiranthus;species cheiri;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or subshrubs.</text>
      <biological_entity id="o4415" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
      <biological_entity id="o4416" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Trichomes of leaves 2-rayed, rarely mixed with fewer 3-rayed ones apically.</text>
      <biological_entity id="o4417" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character constraint="with ones" constraintid="o4419" is_modifier="false" modifier="rarely" name="arrangement" src="d0_s1" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o4418" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o4419" name="one" name_original="ones" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="fewer" value_original="fewer" />
      </biological_entity>
      <relation from="o4417" id="r324" name="part_of" negation="false" src="d0_s1" to="o4418" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, unbranched or branched distally, (woody at base when subshrubs), 1.5–8 dm.</text>
      <biological_entity id="o4420" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s2" to="8" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (rosulate when biennial, often withered by fruiting), similar to cauline.</text>
      <biological_entity constraint="basal" id="o4421" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="cauline" id="o4422" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o4421" id="r325" name="to" negation="false" src="d0_s3" to="o4422" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves petiolate;</text>
      <biological_entity constraint="cauline" id="o4423" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade (obovate to oblanceolate, 4–22 cm × 3–12 mm, base cuneate to attenuate), margins entire to repand.</text>
      <biological_entity id="o4424" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <biological_entity id="o4425" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s5" to="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes considerably elongated in fruit.</text>
      <biological_entity id="o4427" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting pedicels divaricate-ascending to ascending, slender, narrower than fruit, 7–13 mm.</text>
      <biological_entity id="o4426" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o4427" is_modifier="false" modifier="considerably" name="length" src="d0_s6" value="elongated" value_original="elongated" />
        <character constraint="than fruit" constraintid="o4429" is_modifier="false" name="width" src="d0_s7" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o4428" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <biological_entity id="o4429" name="fruit" name_original="fruit" src="d0_s7" type="structure">
        <character char_type="range_value" from="divaricate-ascending" name="orientation" src="d0_s7" to="ascending" />
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="13" to_unit="mm" />
      </biological_entity>
      <relation from="o4426" id="r326" name="fruiting" negation="false" src="d0_s7" to="o4428" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals oblong, 6–10 mm, lateral pair not or slightly saccate basally;</text>
      <biological_entity id="o4430" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4431" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s8" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="slightly; slightly; basally" name="architecture_or_shape" src="d0_s8" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals orange, yellow, brown, red, purple, violet, or white, broadly obovate to suborbicular, 20–35× 5–10 mm, claw 7–12 mm, apex rounded;</text>
      <biological_entity id="o4432" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o4433" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="violet" value_original="violet" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="violet" value_original="violet" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="broadly obovate" name="shape" src="d0_s9" to="suborbicular" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s9" to="35" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4434" name="claw" name_original="claw" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4435" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>median filaments 7–9 mm;</text>
      <biological_entity id="o4436" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity constraint="median" id="o4437" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers linear, 2.5–3.5 mm.</text>
      <biological_entity id="o4438" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o4439" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits ascending, narrowly linear, straight, not torulose, 3–10 cm × 2–7 mm, latiseptate to terete, not striped;</text>
      <biological_entity id="o4440" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s12" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="latiseptate" value_original="latiseptate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s12" value="striped" value_original="striped" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>valves with prominent midvein, pubescent outside, trichomes 2-rayed, glabrous inside;</text>
      <biological_entity id="o4441" name="valve" name_original="valves" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o4442" name="midvein" name_original="midvein" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o4443" name="trichome" name_original="trichomes" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o4441" id="r327" name="with" negation="false" src="d0_s13" to="o4442" />
    </statement>
    <statement id="d0_s14">
      <text>ovules 32–44 per ovary;</text>
      <biological_entity id="o4444" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o4445" from="32" name="quantity" src="d0_s14" to="44" />
      </biological_entity>
      <biological_entity id="o4445" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style cylindrical or subconical, slender, 0.5–4 mm, pubescent;</text>
      <biological_entity id="o4446" name="style" name_original="style" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s15" value="subconical" value_original="subconical" />
        <character is_modifier="false" name="size" src="d0_s15" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stigma strongly 2-lobed, lobes much longer than wide.</text>
      <biological_entity id="o4447" name="stigma" name_original="stigma" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s16" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o4448" name="lobe" name_original="lobes" src="d0_s16" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s16" value="much longer than wide" value_original="much longer than wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds ovate, 2–4 × 1.5–3 mm;</text>
      <biological_entity id="o4449" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s17" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>wing continuous or distal.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 12.</text>
      <biological_entity constraint="distal" id="o4451" name="wing" name_original="wing" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4452" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Disturbed sites, lawns, abandoned gardens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="lawns" />
        <character name="habitat" value="gardens" modifier="abandoned" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., Que., Yukon; Calif.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  <other_name type="common_name">Wallflower</other_name>
  <discussion>Erysimum cheiri is a widely cultivated ornamental of European origin.</discussion>
  
</bio:treatment>