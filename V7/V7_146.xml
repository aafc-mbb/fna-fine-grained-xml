<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">96</other_info_on_meta>
    <other_info_on_meta type="mention_page">97</other_info_on_meta>
    <other_info_on_meta type="mention_page">98</other_info_on_meta>
    <other_info_on_meta type="treatment_page">135</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="(Fries) Andersson in A. P. de Candolle and A. L. P. P. de Candolle" date="1868" rank="section">Phylicifoliae</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>16(2): 240. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section Phylicifoliae</taxon_hierarchy>
    <other_info_on_name type="fna_id">317515</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="family">Salix</taxon_name>
    <taxon_name authority="Fries" date="unknown" rank="tribe">Phylicifoliae</taxon_name>
    <place_of_publication>
      <publication_title>in C. F. Hornschuch, Syll. Pl. Nov.</publication_title>
      <place_in_publication>2: 36. 1828</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family Salix;tribe Phylicifoliae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="A. K. Skvortsov" date="unknown" rank="subsection">Bicolores</taxon_name>
    <taxon_hierarchy>genus Salix;subsection Bicolores;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 0.1–9 m.</text>
      <biological_entity id="o33921" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="9" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="9" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Buds arctica or caprea-type, or intermediate.</text>
      <biological_entity constraint="buds" id="o33923" name="arctica" name_original="arctica" src="d0_s1" type="taxon_name">
        <character is_modifier="false" name="architecture_ref_taxa" src="d0_s1" value="caprea-type" value_original="caprea-type" />
        <character is_modifier="false" name="size" src="d0_s1" value="intermediate" value_original="intermediate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules on late ones absent, rudimentary, or foliaceous;</text>
      <biological_entity id="o33924" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o33925" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" notes="" src="d0_s2" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o33926" name="one" name_original="ones" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o33925" id="r2278" name="on" negation="false" src="d0_s2" to="o33926" />
    </statement>
    <statement id="d0_s3">
      <text>largest medial blade (sometimes amphistomatous), 1.5–11 times as long as wide, adaxial surface not glaucous.</text>
      <biological_entity id="o33927" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="l_w_ratio" notes="" src="d0_s3" value="1.5-11" value_original="1.5-11" />
      </biological_entity>
      <biological_entity constraint="largest medial" id="o33928" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity constraint="adaxial" id="o33929" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Staminate flowers: filaments glabrous or hairy basally.</text>
      <biological_entity id="o33930" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o33931" name="filament" name_original="filaments" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pistillate flowers: adaxial nectary shorter than, equal to, or longer than stipe;</text>
      <biological_entity id="o33932" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o33933" name="nectary" name_original="nectary" src="d0_s5" type="structure">
        <character is_modifier="false" name="variability" src="d0_s5" value="equal" value_original="equal" />
        <character constraint="than stipe" constraintid="o33934" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o33934" name="stipe" name_original="stipe" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>stipe 0.2–2 mm;</text>
      <biological_entity id="o33935" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o33936" name="stipe" name_original="stipe" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ovary sparsely to very densely silky;</text>
      <biological_entity id="o33937" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o33938" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely to very densely" name="pubescence" src="d0_s7" value="silky" value_original="silky" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigmas with flat, non-papillate abaxial surface, or stigmas cylindrical, 0.3–1.1 mm.</text>
      <biological_entity id="o33939" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o33940" name="stigma" name_original="stigmas" src="d0_s8" type="structure" />
      <biological_entity constraint="abaxial" id="o33941" name="surface" name_original="surface" src="d0_s8" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="true" name="relief" src="d0_s8" value="non-papillate" value_original="non-papillate" />
      </biological_entity>
      <biological_entity id="o33942" name="stigma" name_original="stigmas" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="1.1" to_unit="mm" />
      </biological_entity>
      <relation from="o33940" id="r2279" name="with" negation="false" src="d0_s8" to="o33941" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2e6.</number>
  <discussion>Species 11 (5 in the flora).</discussion>
  
</bio:treatment>