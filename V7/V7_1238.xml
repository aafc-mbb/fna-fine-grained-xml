<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">712</other_info_on_meta>
    <other_info_on_meta type="treatment_page">714</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="Hooker" date="1836" rank="species">glandulosus</taxon_name>
    <taxon_name authority="(Greene) Kruckeberg" date="1958" rank="subspecies">secundus</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>14: 223. 1958</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species glandulosus;subspecies secundus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095250</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Streptanthus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">secundus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Francisc.,</publication_title>
      <place_in_publication>261. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Streptanthus;species secundus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euklisia</taxon_name>
    <taxon_name authority="(Greene) Greene" date="unknown" rank="species">secunda</taxon_name>
    <taxon_hierarchy>genus Euklisia;species secunda;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1.5–9.3 dm, densely hirsute proximally, sparsely pubescent distally.</text>
      <biological_entity id="o27711" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s0" to="9.3" to_unit="dm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="sparsely; distally" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves: blade flat distally, margins dentate, surfaces densely to moderately hirsute.</text>
      <biological_entity constraint="cauline" id="o27712" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o27713" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o27714" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o27715" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="densely to moderately" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Racemes secund;</text>
      <biological_entity id="o27716" name="raceme" name_original="racemes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="secund" value_original="secund" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>rachis straight.</text>
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels 7–10 mm, sparsely pubescent or glabrous.</text>
      <biological_entity id="o27717" name="rachis" name_original="rachis" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27718" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o27717" id="r1861" name="fruiting" negation="false" src="d0_s4" to="o27718" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals greenish white (base lavender or purplish), 6–7 mm, glabrous;</text>
      <biological_entity id="o27719" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o27720" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="greenish white" value_original="greenish white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals white (with purple veins), 12–16 mm;</text>
      <biological_entity id="o27721" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o27722" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s6" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>adaxial filaments 7–9 mm.</text>
      <biological_entity id="o27723" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="adaxial" id="o27724" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits spreading to reflexed, arcuate;</text>
      <biological_entity id="o27725" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s8" to="reflexed" />
        <character is_modifier="false" name="course_or_shape" src="d0_s8" value="arcuate" value_original="arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>valves glabrous.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 28.</text>
      <biological_entity id="o27726" name="valve" name_original="valves" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27727" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky, often barren, open slopes, talus, forest openings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open slopes" modifier="rocky often barren" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="forest openings" />
        <character name="habitat" value="rocky" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17g.</number>
  
</bio:treatment>