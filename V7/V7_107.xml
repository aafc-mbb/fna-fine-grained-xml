<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">93</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="mention_page">101</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="mention_page">151</other_info_on_meta>
    <other_info_on_meta type="treatment_page">107</other_info_on_meta>
    <other_info_on_meta type="illustration_page">105</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="(Fries) A. Kerner" date="1860" rank="section">hastatae</taxon_name>
    <taxon_name authority="Bebb in J. T. Rothrock" date="1879" rank="species">wolfii</taxon_name>
    <place_of_publication>
      <publication_title>in J. T. Rothrock, Rep. U. S. Geogr. Surv., Wheeler,</publication_title>
      <place_in_publication>241. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section hastatae;species wolfii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445895</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.1–2 m.</text>
      <biological_entity id="o24430" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches redbrown, violet, yellow-gray, or yellowbrown, pubescent or pilose to glabrescent;</text>
      <biological_entity id="o24431" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o24432" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="violet" value_original="violet" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow-gray" value_original="yellow-gray" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow-gray" value_original="yellow-gray" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowbrown" value_original="yellowbrown" />
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s1" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branchlets yellowish, yellowbrown, redbrown, or yellow-green, sparsely or moderately densely pubescent, or densely long-silky, (inner membranaceous bud-scale layer free, separating from outer layer).</text>
      <biological_entity id="o24433" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o24434" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish" value_original="yellowish" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" modifier="sparsely; moderately densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" modifier="moderately densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="long-silky" value_original="long-silky" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules rudimentary or foliaceous on early ones, foliaceous on late ones, apex rounded, acuminate, or acute;</text>
      <biological_entity id="o24435" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o24436" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="rudimentary" value_original="rudimentary" />
        <character constraint="on ones" constraintid="o24437" is_modifier="false" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
        <character constraint="on ones" constraintid="o24438" is_modifier="false" name="architecture" notes="" src="d0_s3" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o24437" name="one" name_original="ones" src="d0_s3" type="structure" />
      <biological_entity id="o24438" name="one" name_original="ones" src="d0_s3" type="structure" />
      <biological_entity id="o24439" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole convex to flat, or shallowly grooved adaxially, 3–12 mm, pubescent, long-silky, or villous adaxially;</text>
      <biological_entity id="o24440" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o24441" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s4" to="flat" />
        <character is_modifier="false" modifier="shallowly; adaxially" name="architecture" src="d0_s4" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>largest medial blade narrowly oblong, narrowly elliptic, elliptic, or oblanceolate, 26–56 × 8–16.5 mm, 2.5–3.7–5.6 times as long as wide, base cuneate, convex, or rounded, margins flat, entire, apex acute, acuminate, or convex, abaxial surface not glaucous, pubescent, short-silky, or villous, hairs appressed or spreading, straight or wavy, adaxial dull, sparsely to densely silky or villous;</text>
      <biological_entity id="o24442" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="largest medial" id="o24443" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="26" from_unit="mm" name="length" src="d0_s5" to="56" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="16.5" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="2.5-3.7-5.6" value_original="2.5-3.7-5.6" />
      </biological_entity>
      <biological_entity id="o24444" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o24445" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o24446" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24447" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="short-silky" value_original="short-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="short-silky" value_original="short-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o24448" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24449" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="sparsely to densely; densely" name="pubescence" src="d0_s5" value="silky" value_original="silky" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal blade margins entire;</text>
      <biological_entity id="o24450" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="blade" id="o24451" name="margin" name_original="margins" src="d0_s6" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>juvenile blade yellowish green, densely short or long-silky or villous abaxially, hairs white.</text>
      <biological_entity id="o24452" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o24453" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="densely" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character name="height_or_length_or_size" src="d0_s7" value="long-silky or villous" value_original="long-silky or villous" />
      </biological_entity>
      <biological_entity id="o24454" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Catkins flowering as leaves emerge;</text>
      <biological_entity id="o24456" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>staminate stout or subglobose, 9.5–16 × 6–12 mm, flowering branchlet 1–5.5 mm;</text>
      <biological_entity id="o24455" name="catkin" name_original="catkins" src="d0_s8" type="structure">
        <character constraint="as leaves" constraintid="o24456" is_modifier="false" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate moderately or very densely flowered, stout, subglobose or globose, 8.5–38 × 5–12 mm, flowering branchlet 1–11 mm;</text>
      <biological_entity id="o24457" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s9" value="subglobose" value_original="subglobose" />
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="5.5" to_unit="mm" />
        <character is_modifier="false" modifier="moderately" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24458" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="very densely; very densely" name="architecture" src="d0_s10" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s10" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="globose" value_original="globose" />
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract brown, black, or bicolor, 0.8–2 mm, apex rounded or acute, abaxially hairy, hairs wavy, straight, or curly.</text>
      <biological_entity constraint="floral" id="o24459" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="bicolor" value_original="bicolor" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="bicolor" value_original="bicolor" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24460" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o24461" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s11" value="curly" value_original="curly" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: (abaxial nectary 0–0.2 mm), adaxial nectary oblong, 0.4–1.1 mm, (nectaries distinct);</text>
      <biological_entity id="o24462" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24463" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, glabrous;</text>
      <biological_entity id="o24464" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24465" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers yellow, 0.3–0.5 mm.</text>
      <biological_entity id="o24466" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24467" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: adaxial nectary oblong, ovate, or flask-shaped, 0.4–1.1 mm, shorter to longer than stipe;</text>
      <biological_entity id="o24468" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24469" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="flask--shaped" value_original="flask--shaped" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="flask--shaped" value_original="flask--shaped" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="1.1" to_unit="mm" />
        <character constraint="than stipe" constraintid="o24470" is_modifier="false" name="size_or_length" src="d0_s15" value="shorter to longer" value_original="shorter to longer" />
      </biological_entity>
      <biological_entity id="o24470" name="stipe" name_original="stipe" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>stipe 0.2–0.9 mm;</text>
      <biological_entity id="o24471" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24472" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary pyriform, glabrous or hairy, beak gradually tapering to or slightly bulged below styles;</text>
      <biological_entity id="o24473" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24474" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o24475" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="gradually; slightly" name="shape" src="d0_s17" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o24476" name="style" name_original="styles" src="d0_s17" type="structure" />
      <relation from="o24475" id="r1671" name="bulged" negation="false" src="d0_s17" to="o24476" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 8–16 per ovary;</text>
      <biological_entity id="o24477" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24478" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o24479" from="8" name="quantity" src="d0_s18" to="16" />
      </biological_entity>
      <biological_entity id="o24479" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles 0.3–1 mm;</text>
      <biological_entity id="o24480" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24481" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas flat, abaxially non-papillate with rounded or pointed tip, or slenderly or broadly cylindrical, 0.24–0.3–0.4 mm.</text>
      <biological_entity id="o24482" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24483" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o24484" is_modifier="false" modifier="abaxially" name="relief" src="d0_s20" value="non-papillate" value_original="non-papillate" />
        <character is_modifier="false" modifier="slenderly; broadly" name="shape" notes="" src="d0_s20" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.24" from_unit="mm" name="some_measurement" src="d0_s20" to="0.3-0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24484" name="tip" name_original="tip" src="d0_s20" type="structure">
        <character is_modifier="true" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s20" value="pointed" value_original="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 3–5 mm.</text>
      <biological_entity id="o24485" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s21" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Idaho, Mont., Nev., Oreg., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54.</number>
  <other_name type="common_name">Idaho willow</other_name>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>The two varieties of Salix wolfii are distinguished mainly by ovary hairiness; other characters in the key overlap. Ovaries of the typical var. wolfii are glabrous and those of var. idahoensis are hairy. In the latter variety, ovaries are sometimes hairy throughout, but most have hairs in streaks or in a patch at the base of the ovary and on the stipes. These plants usually do not set seed and may be infertile hybrids. Occasional occurrence of staminate flowers with abaxial nectaries suggests that this variety may be a hybrid with S. glauca or S. brachycarpa, although it could also be with S. eastwoodiae, as suggested by S. J. Brunsfeld and F. D. Johnson (1985). The presence of both abaxial and adaxial nectaries in staminate flowers of S. wolfii (staminate plants cannot be identified to variety) is an unusual character in subg. Vetrix; it rarely occurs in S. argyrocarpa, S. breweri, and S. orestera, but is common in S. wolfii. Both hairy ovaries and abaxial nectaries could have been acquired through hybridization and introgression, or polyploidy, with S. glauca or S. brachycarpa. Cytological study of S. wolfii may help answer this question.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovaries glabrous; pistillate adaxial nectaries 0.4-0.8 mm; stipes 0.2-0.9 mm.</description>
      <determination>54a Salix wolfii var. wolfii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovaries pubescent or tomentose (hairs in streaks or patches); pistillate adaxial nectaries 0.4-1.1 mm; stipes 0-0.4 mm</description>
      <determination>54b Salix wolfii var. idahoensis</determination>
    </key_statement>
  </key>
</bio:treatment>