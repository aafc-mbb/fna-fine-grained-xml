<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="treatment_page">453</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">camelineae</taxon_name>
    <taxon_name authority="Crantz" date="1762" rank="genus">camelina</taxon_name>
    <taxon_name authority="(Miller) Thellung" date="1906" rank="species">alyssum</taxon_name>
    <place_of_publication>
      <publication_title>Index Seminum (Zürich)</publication_title>
      <place_in_publication>1906: 10. 1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe camelineae;genus camelina;species alyssum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250009787</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Myagrum</taxon_name>
    <taxon_name authority="Miller" date="unknown" rank="species">alyssum</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. ed.</publication_title>
      <place_in_publication>8, Myagrum no. 2. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Myagrum;species alyssum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials.</text>
      <biological_entity id="o4689" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched or branched distally, 2–7 (–10) dm, glabrous or pubescent basally, trichomes branched, minute.</text>
      <biological_entity id="o4691" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="7" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o4692" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="size" src="d0_s1" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves often withered by anthesis.</text>
      <biological_entity constraint="basal" id="o4693" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="by anthesis" is_modifier="false" modifier="often" name="life_cycle" src="d0_s2" value="withered" value_original="withered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves: blade lanceolate, narrowly oblong, or linear-lanceolate, pinnatifid or sinuate-dentate, (1.5–) 2.5–7 (–10) cm × 2–10 (–20) mm, base sagittate or strongly auriculate, margins usually coarsely dentate to lobed, rarely entire, apex acute, surfaces glabrescent or sparsely pubescent, trichomes primarily forked.</text>
      <biological_entity constraint="cauline" id="o4694" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4695" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s3" value="sinuate-dentate" value_original="sinuate-dentate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s3" value="sinuate-dentate" value_original="sinuate-dentate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s3" value="sinuate-dentate" value_original="sinuate-dentate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s3" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4696" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="sagittate" value_original="sagittate" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s3" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o4697" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="usually coarsely dentate" name="shape" src="d0_s3" to="lobed" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o4698" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o4699" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels ascending to divaricate, 15–30 (–40) mm.</text>
      <biological_entity id="o4700" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="primarily" name="shape" src="d0_s3" value="forked" value_original="forked" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="divaricate" value_original="divaricate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="40" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s4" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4701" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o4700" id="r356" name="fruiting" negation="false" src="d0_s4" to="o4701" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 2–4.2 × 0.7–1.5 mm;</text>
      <biological_entity id="o4702" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o4703" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals pale-yellow, (3.5–) 4–6.5 × 1.5–2 mm;</text>
      <biological_entity id="o4704" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o4705" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="atypical_length" src="d0_s6" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 2–3.5 mm;</text>
      <biological_entity id="o4706" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o4707" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers ca. 0.5 mm.</text>
      <biological_entity id="o4708" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o4709" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits depressed-globose, 7–11 × (5.5–) 6.5–8 (–9) mm (almost as long as wide, or longer), apex subtruncate or, rarely, rounded;</text>
      <biological_entity id="o4710" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="depressed-globose" value_original="depressed-globose" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="11" to_unit="mm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="atypical_width" src="d0_s9" to="6.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="9" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4711" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="subtruncate" value_original="subtruncate" />
        <character name="architecture_or_shape" src="d0_s9" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>valves each with prominent midvein, margin obscurely or narrowly winged;</text>
      <biological_entity id="o4712" name="valve" name_original="valves" src="d0_s10" type="structure" />
      <biological_entity id="o4713" name="midvein" name_original="midvein" src="d0_s10" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o4714" name="margin" name_original="margin" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s10" value="winged" value_original="winged" />
      </biological_entity>
      <relation from="o4712" id="r357" name="with" negation="false" src="d0_s10" to="o4713" />
    </statement>
    <statement id="d0_s11">
      <text>style 1.2–2.5 mm.</text>
      <biological_entity id="o4715" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds reddish or yellowish-brown, (1.8–) 2–3 × 0.7–1 mm. 2n = 40.</text>
      <biological_entity id="o4716" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="yellowish-brown" value_original="yellowish-brown" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="atypical_length" src="d0_s12" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4717" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–June.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="June" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fields, roadsides, prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., Man., Sask.; Calif., Colo., Mont., N.Dak., S.Dak., Wyo.; Europe; introduced also in Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="also in Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>As indicated by R. L. McGregor (1985), Camelina alyssum is known from the above states only from old collections, and, apparently, the species has not been collected again during the past five decades. Although we have not examined all of his cited specimens, those on which the Missouri and Nebraska records are based belong to C. sativa.</discussion>
  
</bio:treatment>