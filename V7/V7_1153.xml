<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">673</other_info_on_meta>
    <other_info_on_meta type="mention_page">674</other_info_on_meta>
    <other_info_on_meta type="treatment_page">672</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">smelowskieae</taxon_name>
    <taxon_name authority="C. A. Meyer in C. F. von Ledebour" date="unknown" rank="genus">smelowskia</taxon_name>
    <taxon_name authority="Rydberg" date="1902" rank="species">americana</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>29: 239. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe smelowskieae;genus smelowskia;species americana</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094840</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Hutchinsia</taxon_name>
    <taxon_name authority="(Stephan) Desvaux" date="unknown" rank="species">calycina</taxon_name>
    <taxon_name authority="Regel &amp; Herder" date="unknown" rank="variety">americana</taxon_name>
    <taxon_hierarchy>genus Hutchinsia;species calycina;variety americana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Smelowskia</taxon_name>
    <taxon_name authority="(Stephan) C. A. Meyer" date="unknown" rank="species">calycina</taxon_name>
    <taxon_name authority="(Regel &amp; Herder) W. H. Drury &amp; Rollins" date="unknown" rank="variety">americana</taxon_name>
    <taxon_hierarchy>genus Smelowskia;species calycina;variety americana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Smelowskia</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">lineariloba</taxon_name>
    <taxon_hierarchy>genus Smelowskia;species lineariloba;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Smelowskia</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">lobata</taxon_name>
    <taxon_hierarchy>genus Smelowskia;species lobata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants sometimes canescent basally;</text>
      <biological_entity id="o28874" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sometimes; basally" name="pubescence" src="d0_s0" value="canescent" value_original="canescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex branched.</text>
      <biological_entity id="o28875" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems: several from base, often unbranched, (0.4–) 0.6–2 (–2.7) dm, trichomes simple, 0.5–1.3 mm, mixed with smaller, dendritic ones.</text>
      <biological_entity id="o28876" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o28877" is_modifier="false" name="quantity" src="d0_s2" value="several" value_original="several" />
        <character is_modifier="false" modifier="often" name="architecture" notes="" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.6" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="2.7" to_unit="dm" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="some_measurement" src="d0_s2" to="2" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o28877" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o28878" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="1.3" to_unit="mm" />
        <character constraint="with ones" constraintid="o28879" is_modifier="false" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o28879" name="one" name_original="ones" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="smaller" value_original="smaller" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="dendritic" value_original="dendritic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: petiole 1–5 (–8) cm, ciliate, trichomes simple;</text>
      <biological_entity constraint="basal" id="o28880" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o28881" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o28882" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate to obovate, or ovate to oblong in outline, (terminal segments linear, oblong, or ovate), (0.5–) 1–3.5 (–5.2) cm × 4–17 mm, (terminal segments 0.2–1.4 cm × 0.5–4 mm), margins 1-pinnatifid or 2-pinnatifid or pinnatisect, apex obtuse or subacute.</text>
      <biological_entity constraint="basal" id="o28883" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o28884" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate or ovate" />
        <character char_type="range_value" constraint="in outline" constraintid="o28885" from="oblanceolate" name="shape" src="d0_s4" to="obovate or ovate" />
      </biological_entity>
      <biological_entity id="o28885" name="outline" name_original="outline" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" notes="alterIDs:o28885" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" notes="alterIDs:o28885" src="d0_s4" to="5.2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" notes="alterIDs:o28885" src="d0_s4" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" notes="alterIDs:o28885" src="d0_s4" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28886" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="1-pinnatifid" value_original="1-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s4" value="2-pinnatifid" value_original="2-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnatisect" value_original="pinnatisect" />
      </biological_entity>
      <biological_entity id="o28887" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves shortly petiolate or sessile;</text>
      <biological_entity constraint="cauline" id="o28888" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade similar to basal, smaller distally.</text>
      <biological_entity id="o28889" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes elongated in fruit.</text>
      <biological_entity id="o28891" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels suberect to ascending, (subappressed to rachis, often forming less than 40˚ angle), proximalmost bracteate, 4–10 (–14) mm, pubescent, trichomes simple (to 1.5 mm), mixed with smaller, dendritic ones.</text>
      <biological_entity id="o28890" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o28891" is_modifier="false" name="length" src="d0_s7" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o28892" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <biological_entity id="o28893" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character char_type="range_value" from="suberect" name="orientation" src="d0_s8" to="ascending" />
        <character is_modifier="false" name="position" src="d0_s8" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bracteate" value_original="bracteate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="14" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o28894" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character constraint="with ones" constraintid="o28895" is_modifier="false" name="arrangement" src="d0_s8" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o28895" name="one" name_original="ones" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="smaller" value_original="smaller" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="dendritic" value_original="dendritic" />
      </biological_entity>
      <relation from="o28890" id="r1948" name="fruiting" negation="false" src="d0_s8" to="o28892" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 2–3.5 mm;</text>
      <biological_entity id="o28896" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o28897" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals usually white, rarely pinkish or lavender, suborbicular to obovate, 3.5–6.5 × 1.5–3.5mm, narrowed to claw, 1.5–3 mm, apex rounded;</text>
      <biological_entity id="o28898" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o28899" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="lavender" value_original="lavender" />
        <character char_type="range_value" from="suborbicular" name="shape" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s10" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="3.5" to_unit="mm" />
        <character constraint="to claw" constraintid="o28900" is_modifier="false" name="shape" src="d0_s10" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28900" name="claw" name_original="claw" src="d0_s10" type="structure" />
      <biological_entity id="o28901" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers oblong, 0.5–0.7 mm.</text>
      <biological_entity id="o28902" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o28903" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits usually suberect, rarely ascending, ellipsoid or oblong to linear, 4-angled, 5–13 × 1.5–2 mm, base and apex cuneate;</text>
      <biological_entity id="o28904" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s12" value="suberect" value_original="suberect" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s12" to="linear" />
        <character is_modifier="false" name="shape" src="d0_s12" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="13" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28905" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o28906" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cuneate" value_original="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>valves each with prominent midvein;</text>
      <biological_entity id="o28907" name="valve" name_original="valves" src="d0_s13" type="structure" />
      <biological_entity id="o28908" name="midvein" name_original="midvein" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o28907" id="r1949" name="with" negation="false" src="d0_s13" to="o28908" />
    </statement>
    <statement id="d0_s14">
      <text>ovules 8–12 (–14) per ovary;</text>
      <biological_entity id="o28909" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" from_inclusive="false" name="atypical_quantity" src="d0_s14" to="14" />
        <character char_type="range_value" constraint="per ovary" constraintid="o28910" from="8" name="quantity" src="d0_s14" to="12" />
      </biological_entity>
      <biological_entity id="o28910" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style 0.2–0.8 mm.</text>
      <biological_entity id="o28911" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1.1–1.9 × 0.6–0.9 mm. 2n = 12, 22.</text>
      <biological_entity id="o28912" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s16" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s16" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28913" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
        <character name="quantity" src="d0_s16" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Talus and scree slopes, rock crevices, tundra, alpine meadows, fellfields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="talus" />
        <character name="habitat" value="scree slopes" />
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="tundra" />
        <character name="habitat" value="alpine meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2900-4000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="4000" to_unit="m" from="2900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Colo., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Both R. C. Rollins (1993) and N. H. Holmgren (2005b) listed 2n = 44 for Smelowskia americana (as S. calycina var. americana), but no such number is known for any species of the genus (S. I. Warwick and I. A. Al-Shehbaz 2006). It is most likely that the first two authors erred in reporting 2n = 22 for the species. The latter count is likely to represent a dysploid reduction of tetraploid populations based on x = 6.</discussion>
  <discussion>Previous North American authors (e.g., W. H. Drury Jr. and R. C. Rollins 1952; Rollins 1993; N. H. Holmgren 2005b) believed that the central Asian Smelowskia calycina and the North American plants also attributed to it are conspecific. S. I. Warwick et al. (2004b) clearly demonstrated that they are different species. The North American plants, S. americana, are easily distinguished from S. calycina by having readily caducous instead of persistent calyces. As recognized by Rollins (1993), the North American S. calycina represented three distinct taxa (S. americana, S. media, S. porsildii) none of which belongs to that species.</discussion>
  
</bio:treatment>