<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">258</other_info_on_meta>
    <other_info_on_meta type="mention_page">264</other_info_on_meta>
    <other_info_on_meta type="treatment_page">265</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">arabis</taxon_name>
    <taxon_name authority="Eastwood" date="1903" rank="species">mcdonaldiana</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>30: 488, unnumb. fig. (p. 489). 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus arabis;species mcdonaldiana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094727</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="Hooker &amp; Arnott" date="unknown" rank="species">blepharophylla</taxon_name>
    <taxon_name authority="(Eastwood) Jepson" date="unknown" rank="variety">mcdonaldiana</taxon_name>
    <taxon_hierarchy>genus Arabis;species blepharophylla;variety mcdonaldiana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="species">serpentinicola</taxon_name>
    <taxon_hierarchy>genus Arabis;species serpentinicola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(caudex simple or branched, covered with persistent petiolar remains);</text>
    </statement>
    <statement id="d0_s2">
      <text>usually glabrous, rarely sparsely pubescent, trichomes simple, (to 0.5 mm), not bulbous-based.</text>
      <biological_entity id="o17842" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o17843" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="bulbous-based" value_original="bulbous-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems simple or few from base (caudex), erect, unbranched, (0.6–) 1.5–3 (–4) dm, (glabrous).</text>
      <biological_entity id="o17844" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character constraint="from base" constraintid="o17845" is_modifier="false" name="quantity" src="d0_s3" value="few" value_original="few" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="1.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="4" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s3" to="3" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o17845" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: petiole 0.3–1.5 cm;</text>
      <biological_entity constraint="basal" id="o17846" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o17847" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s4" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblanceolate to obovate, (0.5–) 1–3 (–4) cm × (2–) 3–6 (–10) mm, margins entire, repand, or obtusely dentate, apex obtuse, surfaces sometimes with individual trichomes terminating some or all leaf teeth.</text>
      <biological_entity constraint="basal" id="o17848" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o17849" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s5" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17850" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o17851" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o17852" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity constraint="individual" id="o17853" name="trichome" name_original="trichomes" src="d0_s5" type="structure" />
      <biological_entity constraint="leaf" id="o17854" name="tooth" name_original="teeth" src="d0_s5" type="structure" />
      <relation from="o17852" id="r1217" name="with" negation="false" src="d0_s5" to="o17853" />
      <relation from="o17853" id="r1218" name="terminating some" negation="false" src="d0_s5" to="o17854" />
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (2 or) 3–5 (or 6);</text>
      <biological_entity constraint="cauline" id="o17855" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade oblong, 0.3–1 (–1.2) cm × 1–3 mm, base not auriculate, margins entire or repand, apex obtuse, surfaces glabrous.</text>
      <biological_entity id="o17856" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s7" to="1" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17857" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o17858" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="repand" value_original="repand" />
      </biological_entity>
      <biological_entity id="o17859" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o17860" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes simple, (dense).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels ascending to suberect, 3–10 (–13) mm.</text>
      <biological_entity id="o17861" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s9" to="suberect" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17862" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o17861" id="r1219" name="fruiting" negation="false" src="d0_s9" to="o17862" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals (purple), oblong, 4–8 × 1.5–2.5 mm, lateral pair saccate basally;</text>
      <biological_entity id="o17863" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
      <biological_entity id="o17864" name="sepal" name_original="sepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>petals purple, spatulate, 8–16 × 2–5 mm, apex obtuse;</text>
      <biological_entity id="o17865" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o17866" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s11" to="16" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17867" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments 4–8 mm;</text>
      <biological_entity id="o17868" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o17869" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers oblong, 1.5–2 mm.</text>
      <biological_entity id="o17870" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o17871" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits ascending to suberect, not torulose, sometimes slightly curved, 2–4 cm × 1.5–2 mm;</text>
      <biological_entity id="o17872" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s14" to="suberect" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="torulose" value_original="torulose" />
        <character is_modifier="false" modifier="sometimes slightly" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s14" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves each with prominent midvein extending full length;</text>
      <biological_entity id="o17873" name="valve" name_original="valves" src="d0_s15" type="structure" />
      <biological_entity id="o17874" name="midvein" name_original="midvein" src="d0_s15" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s15" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o17873" id="r1220" name="with" negation="false" src="d0_s15" to="o17874" />
    </statement>
    <statement id="d0_s16">
      <text>ovules 20–34 per ovary;</text>
      <biological_entity id="o17875" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o17876" from="20" name="quantity" src="d0_s16" to="34" />
      </biological_entity>
      <biological_entity id="o17876" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.3–1.5 mm.</text>
      <biological_entity id="o17877" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds narrowly winged distally or, rarely, not winged, oblong, 1.5–2.2 × 1–1.3 mm, wing 0.1–0.2 mm wide.</text>
      <biological_entity id="o17878" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="narrowly; distally; distally" name="architecture" src="d0_s18" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="rarely; not" name="architecture" src="d0_s18" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s18" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s18" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s18" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17879" name="wing" name_original="wing" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s18" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine scrap and slopes, red serpentinized soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine scrap" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="red serpentinized soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Arabis mcdonaldiana is known in California from Del Norte, Mendocino, and Siskiyou counties, and in Oregon from Curry and Jackson counties. It is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>