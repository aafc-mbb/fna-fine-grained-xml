<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="treatment_page">633</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(A. Gray) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">densiflora</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 322. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species densiflora</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095120</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vesicaria</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">densiflora</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6: 145. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Vesicaria;species densiflora;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="(A. Gray) S. Watson" date="unknown" rank="species">densiflora</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species densiflora;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials;</text>
      <biological_entity id="o9239" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex simple or branched, (relatively small, cespitose);</text>
    </statement>
    <statement id="d0_s2">
      <text>densely pubescent, trichomes (spreading, sessile or short-stalked), 5–7-rayed, rays distinct and simple, (tuberculate, finely tubercled with a U-shaped notch on one side).</text>
      <biological_entity id="o9241" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o9242" name="trichome" name_original="trichomes" src="d0_s2" type="structure" />
      <biological_entity id="o9243" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s2" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems simple or few to several from base, erect or decumbent, (rarely branched, usually leafy), to 4 dm.</text>
      <biological_entity id="o9244" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s3" value="few to several" value_original="few to several" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s3" to="4" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o9245" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o9244" id="r665" name="from" negation="false" src="d0_s3" to="o9245" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: blade lyrate-pinnatifid, 1–7 cm, margins entire or shallowly dentate.</text>
      <biological_entity constraint="basal" id="o9246" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9247" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="lyrate-pinnatifid" value_original="lyrate-pinnatifid" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9248" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="shallowly" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves (sessile or shortly petiolate);</text>
      <biological_entity constraint="cauline" id="o9249" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blade narrowly obovate to elliptic, 0.5–6 cm, margins entire, repand, or shallowly dentate.</text>
      <biological_entity id="o9250" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly obovate" name="shape" src="d0_s6" to="elliptic" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9251" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes dense, (elongated in fruit, often subtended by distal cauline leaves).</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels (usually divaricate-spreading, straight or slightly curved, delicate, sometimes drooping, especially on herbarium specimens), 7–10 mm, (somewhat rigid).</text>
      <biological_entity id="o9252" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9253" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o9252" id="r666" name="fruiting" negation="false" src="d0_s8" to="o9253" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals elliptic, 3.7–7.2 mm, (lateral pair somewhat cucullate, median pair thickened apically);</text>
      <biological_entity id="o9254" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o9255" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s9" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s9" to="7.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals (yellow to orange-yellow), obovate to obdeltate, (4.5–) 7–10 (–11) mm, (tapering to short claw, apex often emarginate).</text>
      <biological_entity id="o9256" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="obovate" name="shape" notes="" src="d0_s10" to="obdeltate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="11" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9257" name="petal" name_original="petals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Fruits (sessile or substipitate), globose or broadly obovate, not inflated, 4–6 mm, (smooth);</text>
      <biological_entity id="o9258" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves (not retaining seeds after dehiscence), glabrous;</text>
      <biological_entity id="o9259" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>replum as wide as or wider than fruit;</text>
      <biological_entity id="o9260" name="replum" name_original="replum" src="d0_s13" type="structure">
        <character constraint="than fruit" constraintid="o9261" is_modifier="false" name="width" src="d0_s13" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o9261" name="fruit" name_original="fruit" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>ovules 8–16 per ovary;</text>
      <biological_entity id="o9262" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o9263" from="8" name="quantity" src="d0_s14" to="16" />
      </biological_entity>
      <biological_entity id="o9263" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style 2–5 mm.</text>
      <biological_entity id="o9264" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds flattened.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 14.</text>
      <biological_entity id="o9265" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9266" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy, granitic, or calcareous soils, sandy ledges, limestone outcrops, rocky prairies, uplands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="calcareous soils" modifier="sandy granitic or" />
        <character name="habitat" value="sandy ledges" />
        <character name="habitat" value="limestone outcrops" />
        <character name="habitat" value="rocky prairies" />
        <character name="habitat" value="uplands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>30-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  <other_name type="common_name">Denseflower or low bladderpod</other_name>
  <discussion>Alyssum densiflorum (A. Gray) Kuntze (1891), not Desfontaines (1808) is an illegitimate name, sometimes found in synonymy with Physaria densiflora.</discussion>
  
</bio:treatment>