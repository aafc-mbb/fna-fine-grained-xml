<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">506</other_info_on_meta>
    <other_info_on_meta type="treatment_page">507</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">selenia</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray in War Department [U.S.]" date="1855" rank="species">dissecta</taxon_name>
    <place_of_publication>
      <publication_title>in War Department [U.S.], Pacif. Railr. Rep.</publication_title>
      <place_in_publication>2(2): 160. 1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus selenia;species dissecta</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095062</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants winter-annuals, (often nearly acaulescent).</text>
      <biological_entity constraint="plants" id="o28693" name="winter-annual" name_original="winter-annuals" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems (often inflated into 2.3 cm thick crown), usually ascending, rarely decumbent, 0.8–2.2 dm (when formed).</text>
      <biological_entity id="o28694" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="rarely" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="0.8" from_unit="dm" name="some_measurement" src="d0_s1" to="2.2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves rosulate;</text>
      <biological_entity constraint="basal" id="o28695" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.5–3 (–5) cm;</text>
      <biological_entity id="o28696" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins usually 2-, rarely 3-pinnatisect, (2–) 3–10 (–15) cm;</text>
      <biological_entity constraint="blade" id="o28697" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="2-pinnatisect" value_original="2-pinnatisect" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="3-pinnatisect" value_original="3-pinnatisect" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes 5–10 (–15) on each side, (smaller than terminal);</text>
      <biological_entity id="o28698" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="15" />
        <character char_type="range_value" constraint="on side" constraintid="o28699" from="5" name="quantity" src="d0_s5" to="10" />
      </biological_entity>
      <biological_entity id="o28699" name="side" name_original="side" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>apical segment linear to oblong or ovate, 1–8 (–12) × 0.5–1 (–2.5) mm, margins entire.</text>
      <biological_entity constraint="apical" id="o28700" name="segment" name_original="segment" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="oblong or ovate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28701" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves (and bracts, when present) similar to basal, smaller distally.</text>
      <biological_entity constraint="basal" id="o28703" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o28702" id="r1941" name="to" negation="false" src="d0_s7" to="o28703" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels usually from basal leaf-axil, (20–) 30–80 (–100) mm.</text>
      <biological_entity constraint="cauline" id="o28702" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distally" name="size" notes="" src="d0_s7" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o28704" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <biological_entity constraint="basal" id="o28705" name="axil-leaf" name_original="leaf-axil" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="mm" name="atypical_distance" src="d0_s8" to="30" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="80" from_inclusive="false" from_unit="mm" name="atypical_distance" src="d0_s8" to="100" to_unit="mm" />
        <character char_type="range_value" from="30" from_unit="mm" name="distance" src="d0_s8" to="80" to_unit="mm" />
      </biological_entity>
      <relation from="o28702" id="r1942" modifier="usually" name="fruiting" negation="false" src="d0_s8" to="o28704" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals (caducous or tardily so), spreading, oblong, (6–) 7–12 (–14) × 2–3.5 mm, apex appendage well-developed, (1–) 1.5–3 mm;</text>
      <biological_entity id="o28706" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s9" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="14" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28707" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity constraint="apex" id="o28708" name="appendage" name_original="appendage" src="d0_s9" type="structure">
        <character is_modifier="false" name="development" src="d0_s9" value="well-developed" value_original="well-developed" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals broadly spatulate to obovate, (12–) 15–20 × (5–) 6–9 mm, apex rounded;</text>
      <biological_entity id="o28709" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o28710" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="broadly spatulate" name="shape" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="12" from_unit="mm" name="atypical_length" src="d0_s10" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s10" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s10" to="6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s10" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28711" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>median filament pairs 6–10 mm, not dilated basally;</text>
      <biological_entity id="o28712" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="median" id="o28713" name="filament" name_original="filament" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s11" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers linear, 2–3 mm;</text>
      <biological_entity id="o28714" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o28715" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>gynophore (1–) 1.5–3 (–4) mm.</text>
      <biological_entity id="o28716" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o28717" name="gynophore" name_original="gynophore" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits oblong to elliptical, latiseptate, 1.4–3.5 (–4) cm × (8–) 10–17 mm, (slightly fleshy when green, thick, papery), base and apex acute;</text>
      <biological_entity id="o28718" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s14" to="elliptical" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="latiseptate" value_original="latiseptate" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s14" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.4" from_unit="cm" name="length" src="d0_s14" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_width" src="d0_s14" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s14" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28719" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o28720" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves prominently reticulate-veined;</text>
      <biological_entity id="o28721" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s15" value="reticulate-veined" value_original="reticulate-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>replum strongly flattened;</text>
      <biological_entity id="o28722" name="replum" name_original="replum" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>septum complete;</text>
      <biological_entity id="o28723" name="septum" name_original="septum" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules 28–40 per ovary;</text>
      <biological_entity id="o28724" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o28725" from="28" name="quantity" src="d0_s18" to="40" />
      </biological_entity>
      <biological_entity id="o28725" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style (2–) 3.5–6 (–7) mm, strongly flattened basally.</text>
      <biological_entity id="o28726" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="7" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s19" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="strongly; basally" name="shape" src="d0_s19" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 5–7 mm diam.;</text>
      <biological_entity id="o28727" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s20" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>wing 1–2 mm. 2n = 14.</text>
      <biological_entity id="o28728" name="wing" name_original="wing" src="d0_s21" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s21" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28729" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Feb–Apr.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Apr" from="Feb" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy banks, pastures, salt draws, gypseous llano, roadsides, sandy alluvium, limestone or sandy areas, creosote bush scrubland, open flats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy banks" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="salt" />
        <character name="habitat" value="gypseous llano" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="sandy alluvium" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="creosote bush" />
        <character name="habitat" value="open flats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Nuevo León).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Chihuahua)" establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2.</number>
  <discussion>Selenia dissecta rarely produces racemes, and most flowers originate from the axils of basal leaves that cover an inflated stem reduced to a crown.</discussion>
  
</bio:treatment>