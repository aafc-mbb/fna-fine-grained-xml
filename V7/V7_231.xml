<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="mention_page">175</other_info_on_meta>
    <other_info_on_meta type="mention_page">182</other_info_on_meta>
    <other_info_on_meta type="treatment_page">183</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">limnanthaceae</taxon_name>
    <taxon_name authority="R. Brown" date="unknown" rank="genus">limnanthes</taxon_name>
    <taxon_name authority="C. T. Mason" date="1952" rank="section">Inflexae</taxon_name>
    <taxon_name authority="Howell" date="1897" rank="species">floccosa</taxon_name>
    <taxon_name authority="Arroyo" date="1973" rank="subspecies">grandiflora</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>25: 188. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family limnanthaceae;genus limnanthes;section inflexae;species floccosa;subspecies grandiflora;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250095172</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbage sparsely hairy.</text>
      <biological_entity id="o31851" name="herbage" name_original="herbage" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending.</text>
      <biological_entity id="o31852" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Flowers cupshaped;</text>
      <biological_entity id="o31853" name="flower" name_original="flowers" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sepals accrescent, oblong-lanceolate to lanceolate-ovate, 8.5–9 mm, abaxially glabrous or sparsely hairy, adaxially densely hairy;</text>
      <biological_entity id="o31854" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="accrescent" value_original="accrescent" />
        <character char_type="range_value" from="oblong-lanceolate" name="shape" src="d0_s3" to="lanceolate-ovate" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="adaxially densely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petals obovate, 4.5–8.5 mm, apex obtuse, not emarginate, sometimes erose;</text>
      <biological_entity id="o31855" name="petal" name_original="petals" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s4" to="8.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31856" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s4" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_relief" src="d0_s4" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>filaments 4.5–5 mm;</text>
      <biological_entity id="o31857" name="filament" name_original="filaments" src="d0_s5" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>anthers ca. 0.8 mm, usually dehiscing extrorsely;</text>
      <biological_entity id="o31858" name="anther" name_original="anthers" src="d0_s6" type="structure">
        <character name="some_measurement" src="d0_s6" unit="mm" value="0.8" value_original="0.8" />
        <character is_modifier="false" modifier="usually; extrorsely" name="dehiscence" src="d0_s6" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>style 3.2–4 mm.</text>
      <biological_entity id="o31859" name="style" name_original="style" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Nutlets 3–5, tubercles broad-based, platelike cones.</text>
      <biological_entity id="o31860" name="nutlet" name_original="nutlets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <biological_entity id="o31861" name="tubercle" name_original="tubercles" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="broad-based" value_original="broad-based" />
      </biological_entity>
      <biological_entity id="o31862" name="cone" name_original="cones" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="plate-like" value_original="platelike" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, inner edges of vernal pools</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="inner edges" modifier="wet" constraint="of vernal pools" />
        <character name="habitat" value="vernal pools" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7d.</number>
  <other_name type="common_name">Large-flowered dwarf meadowfoam</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies grandiflora occurs primarily in the Agate Desert of Jackson County.</discussion>
  
</bio:treatment>