<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">88</other_info_on_meta>
    <other_info_on_meta type="mention_page">116</other_info_on_meta>
    <other_info_on_meta type="mention_page">122</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="treatment_page">87</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="(Fries) Andersson in A. P. de Candolle and A. L. P. P. de Candolle" date="1868" rank="section">glaucae</taxon_name>
    <taxon_name authority="Nuttall" date="1842" rank="species">brachycarpa</taxon_name>
    <taxon_name authority="Raup" date="1936" rank="variety">psammophila</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>17: 230, plate 191. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section glaucae;species brachycarpa;variety psammophila</taxon_hierarchy>
    <other_info_on_name type="fna_id">242445668</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems: branches densely villous;</text>
      <biological_entity id="o23813" name="stem" name_original="stems" src="d0_s0" type="structure" />
      <biological_entity id="o23814" name="branch" name_original="branches" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>branchlets very densely woolly, villous, or long-silky.</text>
      <biological_entity id="o23815" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o23816" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="very densely" name="pubescence" src="d0_s1" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="long-silky" value_original="long-silky" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: petiole 0.8–4 mm, (densely villous to long-silky adaxially);</text>
      <biological_entity id="o23817" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o23818" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s2" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>largest medial blade oblong, elliptic to ovate, 15–34 × 7.5–19 mm, 1.4–2.6 times as long as wide, base subcordate, cordate, or convex, apex rounded or convex, abaxial surface very densely woolly, villous, or long-silky, adaxial moderately densely villous or long-silky.</text>
      <biological_entity id="o23819" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="largest medial" id="o23820" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="ovate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s3" to="34" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="width" src="d0_s3" to="19" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="1.4-2.6" value_original="1.4-2.6" />
      </biological_entity>
      <biological_entity id="o23821" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s3" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o23822" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s3" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23823" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="very densely" name="pubescence" src="d0_s3" value="woolly" value_original="woolly" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="long-silky" value_original="long-silky" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o23824" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="moderately densely" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="long-silky" value_original="long-silky" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Catkins: staminate 9.5–24 × 6–10 mm, flowering branchlet 2–24 (–43 in later-flowering plants) mm;</text>
      <biological_entity id="o23825" name="catkin" name_original="catkins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="9.5" from_unit="mm" name="length" src="d0_s4" to="24" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23826" name="branchlet" name_original="branchlet" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>pistillate stout or subglobose, 18–28.5 × 7–14 mm, flowering branchlet 4–8 mm;</text>
      <biological_entity id="o23827" name="catkin" name_original="catkins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s5" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" src="d0_s5" to="28.5" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s5" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23828" name="branchlet" name_original="branchlet" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>floral bract tawny or greenish, 1.2–2.4 mm.</text>
      <biological_entity id="o23829" name="catkin" name_original="catkins" src="d0_s6" type="structure" />
      <biological_entity constraint="floral" id="o23830" name="bract" name_original="bract" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s6" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Staminate flowers: abaxial nectary (0–) 0.9–1.5 mm, adaxial nectary 0.5–2 mm;</text>
      <biological_entity id="o23831" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23832" name="nectary" name_original="nectary" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="0.9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o23833" name="nectary" name_original="nectary" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments distinct, hairy throughout or on proximal 1/2;</text>
      <biological_entity id="o23834" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o23835" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="on proximal 1/2" value_original="on proximal 1/2" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o23836" name="1/2" name_original="1/2" src="d0_s8" type="structure" />
      <relation from="o23835" id="r1626" name="on" negation="false" src="d0_s8" to="o23836" />
    </statement>
    <statement id="d0_s9">
      <text>anthers (yellow), ellipsoid or shortly cylindrical, 0.5–0.8 mm.</text>
      <biological_entity id="o23837" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s9" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23838" name="anther" name_original="anthers" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Pistillate flowers: abaxial nectary 0.3–0.6 mm, adaxial nectary narrowly oblong or ovate (with slender tips), 0.8–1.8 mm;</text>
      <biological_entity id="o23839" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o23840" name="nectary" name_original="nectary" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o23841" name="nectary" name_original="nectary" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stipe 0–0.2 mm;</text>
      <biological_entity id="o23842" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23843" name="stipe" name_original="stipe" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 8–10 per ovary;</text>
      <biological_entity id="o23844" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23845" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o23846" from="8" name="quantity" src="d0_s12" to="10" />
      </biological_entity>
      <biological_entity id="o23846" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>styles 0.4–1.2 mm.</text>
      <biological_entity id="o23847" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23848" name="style" name_original="styles" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules 4.5–6.5 mm.</text>
      <biological_entity id="o23849" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Jun-early Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Aug" from="late Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open sand dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open sand dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Sask.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>48b.</number>
  <other_name type="common_name">Small-fruit sand dune willow</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Previously, I (G. W. Argus 1965) did not recognize var. psammophila, but field experience showed that this sand dune endemic deserves formal taxonomic status. It is mainly characterized by very dense hairiness and slightly broader leaf blades. Salix brachycarpa var. psammophila is found only on Lake Athabasca sand dunes.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Variety psammophila forms natural hybrids with Salix pyrifolia, S. silicicola, and S. turnorii. All of these taxa occur together in the sand dunes at Lake Athabasca in northwestern Saskatchewan.</discussion>
  <discussion>Variety psammophila × Salix pyrifolia resembles S. pyrifolia in its reddish petioles and midribs, margins somewhat toothed, styles 0.4 mm, and stipes 0.4–1.1 mm; and S. brachycarpa var. psammophila in its sparsely hairy branches, branchlets, and ovaries.</discussion>
  <discussion>Variety psammophila × Salix turnorii (S. ×brachypurpurea B. Boivin) resembles var. psammophila in floral bracts tawny, petioles relatively very short, and branchlets and juvenile leaves very densely long-silky; and S. turnorii in leaves amphistomatous, leaf margins serrulate, and branches yellow-brown. It is intermediate in ovaries varying from moderately densely hairy to glabrescent. The hybrids appear to be infertile (G. W. Argus 1965).</discussion>
  
</bio:treatment>