<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">77</other_info_on_meta>
    <other_info_on_meta type="illustration_page">76</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="(Rydberg) C. K. Schneider in C. S. Sargent" date="1916" rank="section">ovalifoliae</taxon_name>
    <taxon_name authority="Trautvetter" date="1832" rank="species">ovalifolia</taxon_name>
    <place_of_publication>
      <publication_title>Nouv. Mém. Soc. Imp. Naturalistes Moscou</publication_title>
      <place_in_publication>2: 306, plate 13. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section ovalifoliae;species ovalifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242445807</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.02–0.05 m, not clonal or forming clones by layering.</text>
      <biological_entity id="o29499" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.02" from_unit="m" name="some_measurement" src="d0_s0" to="0.05" to_unit="m" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="clonal" value_original="clonal" />
        <character name="growth_form" src="d0_s0" value="forming" value_original="forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems trailing;</text>
      <biological_entity id="o29500" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="trailing" value_original="trailing" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches yellowbrown, gray-brown, or redbrown, glabrous or hairy;</text>
      <biological_entity id="o29501" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets yellow-green, yellowbrown, or redbrown, glabrous or pilose.</text>
      <biological_entity id="o29502" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules usually absent or rudimentary, rarely foliaceous;</text>
      <biological_entity id="o29503" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o29504" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole (deeply to shallowly grooved adaxially), 1.1–16 mm, (glabrous);</text>
      <biological_entity id="o29505" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29506" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>largest medial blade hypostomatous, narrowly to broadly elliptic, circular, subcircular, or obovate, 13–46 × 7–20 mm, 1–3.4 times as long as wide, base subcordate, cordate, rounded, or convex, margins slightly revolute or flat, entire, sometimes ciliate, apex convex, rounded, acuminate, acute, or retuse, abaxial surface glabrous, villous, long-silky, pubescent, or pilose, hairs wavy or straight, adaxial highly glossy, usually glabrous;</text>
      <biological_entity id="o29507" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="largest medial" id="o29508" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="hypostomatous" value_original="hypostomatous" />
        <character is_modifier="false" modifier="narrowly to broadly" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="circular" value_original="circular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcircular" value_original="subcircular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcircular" value_original="subcircular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s6" to="46" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1-3.4" value_original="1-3.4" />
      </biological_entity>
      <biological_entity id="o29509" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o29510" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o29511" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29512" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o29513" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o29514" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="highly" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal blade margins entire;</text>
      <biological_entity id="o29515" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o29516" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>juvenile blade (reddish or yellowish green), pilose, villous, or long-silky abaxially.</text>
      <biological_entity id="o29517" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s8" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="long-silky" value_original="long-silky" />
      </biological_entity>
      <biological_entity id="o29518" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="juvenile" value_original="juvenile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Catkins: staminate 4.8–46 × 5–11 mm, flowering branchlet 1.5–24 mm;</text>
      <biological_entity id="o29519" name="catkin" name_original="catkins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="4.8" from_unit="mm" name="length" src="d0_s9" to="46" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29520" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate moderately densely flowered, stout, subglobose, globose, or slender, 6.3–50 × 5–28 mm, flowering branchlet 2.5–22 mm;</text>
      <biological_entity id="o29521" name="catkin" name_original="catkins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="moderately densely" name="architecture" src="d0_s10" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s10" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s10" value="globose" value_original="globose" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character char_type="range_value" from="6.3" from_unit="mm" name="length" src="d0_s10" to="50" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="28" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29522" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract brown, greenish, or bicolor, 1.2–2 mm, apex rounded, entire or, sometimes, 2-fid, abaxially hairy, hairs straight or wavy.</text>
      <biological_entity id="o29523" name="catkin" name_original="catkins" src="d0_s11" type="structure" />
      <biological_entity constraint="floral" id="o29524" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="bicolor" value_original="bicolor" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="bicolor" value_original="bicolor" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29525" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s11" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s11" value="2-fid" value_original="2-fid" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o29526" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s11" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: abaxial nectary 0.6–1 mm, adaxial nectary oblong or ovate, 0.6–1.6 mm, nectaries distinct or connate and cupshaped;</text>
      <biological_entity id="o29527" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29528" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o29529" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29530" name="nectary" name_original="nectaries" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct or connate less than 1/2 their lengths (glabrous);</text>
      <biological_entity id="o29531" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o29532" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s13" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers elliptic, short-cylindrical, or globose, 0.3–0.5 (–0.6) mm.</text>
      <biological_entity id="o29533" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o29534" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s14" value="short-cylindrical" value_original="short-cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s14" value="short-cylindrical" value_original="short-cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s14" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s14" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: abaxial nectary (0–) 0.4–0.6 mm, adaxial nectary longer than stipe;</text>
      <biological_entity id="o29535" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29536" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s15" to="0.4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o29537" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character constraint="than stipe" constraintid="o29538" is_modifier="false" name="length_or_size" src="d0_s15" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o29538" name="stipe" name_original="stipe" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>stipe 0.2–1.4 mm;</text>
      <biological_entity id="o29539" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o29540" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary obclavate or pyriform, glaucous or not, usually glabrous or tomentose, sometimes pubescent or villous, beak abruptly tapering to styles;</text>
      <biological_entity id="o29541" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o29542" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="obclavate" value_original="obclavate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glaucous" value_original="glaucous" />
        <character name="pubescence" src="d0_s17" value="not" value_original="not" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o29543" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character constraint="to styles" constraintid="o29544" is_modifier="false" modifier="abruptly" name="shape" src="d0_s17" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o29544" name="style" name_original="styles" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 10–15 per ovary;</text>
      <biological_entity id="o29545" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o29546" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o29547" from="10" name="quantity" src="d0_s18" to="15" />
      </biological_entity>
      <biological_entity id="o29547" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles 0.2–0.8 mm;</text>
      <biological_entity id="o29548" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o29549" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s19" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas flat, abaxially non-papillate with pointed tip, or slenderly cylindrical, 0.32–0.41–0.64 mm.</text>
      <biological_entity id="o29550" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o29551" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o29552" is_modifier="false" modifier="abaxially" name="relief" src="d0_s20" value="non-papillate" value_original="non-papillate" />
        <character is_modifier="false" modifier="slenderly" name="shape" notes="" src="d0_s20" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.32" from_unit="mm" name="some_measurement" src="d0_s20" to="0.41-0.64" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29552" name="tip" name_original="tip" src="d0_s20" type="structure">
        <character is_modifier="true" name="shape" src="d0_s20" value="pointed" value_original="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 5.2–9.6 mm. 2n = 38.</text>
      <biological_entity id="o29553" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="5.2" from_unit="mm" name="some_measurement" src="d0_s21" to="9.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29554" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Yukon; Alaska; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>38.</number>
  <other_name type="common_name">Arctic seashore or oval-leaf willow</other_name>
  <discussion>Varieties 4 (4 in the flora).</discussion>
  <discussion>The varieties of Salix ovalifolia are relatively minor variants; their ranges overlap and their differences in leaf shape and ovary indumentum intergrade (G. W. Argus 1969, 1973). The only one with a more or less distinctive geographical distribution is var. cyclophylla; where its range overlaps with var. ovalifolia there is intergradation. Variety arctolitoralis, which is characterized by larger leaves and catkins, may be an ecotype. Variety glacialis is known only from near Point Barrow, Alaska. E. Hultén (1968) suggested, probably based on its often tomentose ovaries, that it is S. arctica × S. ovalifolia. All varieties of the species have some plants with hairy ovaries, but the suggestion that this character is an indication of hybridization deserves study.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Salix ovalifolia forms natural hybrids with S. arctica and S. fuscescens.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovaries usually tomentose, sometimes glabrous; largest medial blades 8.5-14 mm; endemic to Point Barrow, Alaska.</description>
      <determination>38d Salix ovalifolia var. glacialis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Ovaries usually glabrous, sometimes pubescent or villous; largest medial blades 13-46 mm; not endemic to Point Barrow, Alaska</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Largest medial blades subcircular to circular, 1-1.5 times as long as wide.</description>
      <determination>38b Salix ovalifolia var. cyclophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Largest medial blades narrowly to broadly elliptic, obovate, or subcircular, 1.1-3.4 times as long as wide</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Largest medial blades elliptic, broadly elliptic, or subcircular, 13-28 mm.</description>
      <determination>38a Salix ovalifolia var. ovalifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Largest medial blades narrowly to broadly elliptic or obovate, 25-46 mm.</description>
      <determination>38c Salix ovalifolia var. arctolitoralis</determination>
    </key_statement>
  </key>
</bio:treatment>