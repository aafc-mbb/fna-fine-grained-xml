<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">712</other_info_on_meta>
    <other_info_on_meta type="mention_page">714</other_info_on_meta>
    <other_info_on_meta type="treatment_page">713</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="Hooker" date="1836" rank="species">glandulosus</taxon_name>
    <taxon_name authority="Al-Shehbaz &amp; M. S. Mayer" date="2008" rank="subspecies">josephinensis</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>18: 280. 2008</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species glandulosus;subspecies josephinensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095237</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems moderately hirsute proximally, glabrous distally, 1.7–3.8 dm.</text>
      <biological_entity id="o19417" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="moderately; proximally" name="pubescence" src="d0_s0" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="1.7" from_unit="dm" name="some_measurement" src="d0_s0" to="3.8" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves: blade flat distally, margins dentate, surfaces sparsely hirsute.</text>
      <biological_entity constraint="cauline" id="o19418" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o19419" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o19420" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o19421" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Racemes secund;</text>
      <biological_entity id="o19422" name="raceme" name_original="racemes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="secund" value_original="secund" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>rachis straight.</text>
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels 5–10 mm, glabrous or sparsely pubescent.</text>
      <biological_entity id="o19423" name="rachis" name_original="rachis" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o19424" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o19423" id="r1328" name="fruiting" negation="false" src="d0_s4" to="o19424" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals white or cream, 3–5 mm, glabrous or sparsely pubescent;</text>
      <biological_entity id="o19425" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o19426" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="cream" value_original="cream" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals white (with purple veins), 7–8 mm;</text>
      <biological_entity id="o19427" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o19428" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>adaxial filaments 5–6 mm.</text>
      <biological_entity id="o19429" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="adaxial" id="o19430" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits divaricate-ascending, straight or curved inwards;</text>
      <biological_entity id="o19431" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s8" value="curved" value_original="curved" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="inward" value_original="inward" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>valves glabrous or sparsely pubescent.</text>
      <biological_entity id="o19432" name="valve" name_original="valves" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17d.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies josephinensis, known from Josephine County, is disjunct from the white-flowered Californian subspecies. R. C. Rollins (1993) and R. E. Buck et al. (1993) attributed the Oregonian plants to subspp. glandulosus and secundus, respectively.</discussion>
  
</bio:treatment>