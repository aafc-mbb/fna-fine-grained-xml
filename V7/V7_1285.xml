<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">729</other_info_on_meta>
    <other_info_on_meta type="treatment_page">735</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Endlicher" date="1839" rank="genus">thelypodium</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="1973" rank="species">laxiflorum</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>204: 129, plate 22A. 1973</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus thelypodium;species laxiflorum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094962</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stanleyella</taxon_name>
    <taxon_name authority="(A. Gray) Rydberg" date="unknown" rank="species">wrightii</taxon_name>
    <taxon_name authority="(M. E. Jones) Payson" date="unknown" rank="variety">tenella</taxon_name>
    <taxon_hierarchy>genus Stanleyella;species wrightii;variety tenella;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelypodium</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">wrightii</taxon_name>
    <taxon_name authority="M. E. Jones" date="unknown" rank="variety">tenellum</taxon_name>
    <taxon_hierarchy>genus Thelypodium;species wrightii;variety tenellum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>somewhat glaucous, glabrous throughout or pubescent basally.</text>
      <biological_entity id="o5540" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="somewhat" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems branched distally, (1.5–) 3–14 (–23.5) dm, (glabrous throughout or sparsely to densely hirsute proximally).</text>
      <biological_entity id="o5541" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="23.5" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s2" to="14" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (and proximal cauline): petiole (1–) 1.7–8 (–11) cm, pubescent or glabrous;</text>
      <biological_entity constraint="basal" id="o5542" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o5543" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="1.7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="11" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_unit="cm" name="some_measurement" src="d0_s3" to="8" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate in outline (lateral lobes usually oblong to linear, rarely deltate), (4–) 7.2–20.5 (–30) cm × (6–) 10–40 (–100) mm, margins pinnately lobed or lyrate (lobes entire or dentate).</text>
      <biological_entity constraint="basal" id="o5544" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o5545" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character constraint="in outline" constraintid="o5546" is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o5546" name="outline" name_original="outline" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_length" notes="alterIDs:o5546" src="d0_s4" to="7.2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20.5" from_inclusive="false" from_unit="cm" name="atypical_length" notes="alterIDs:o5546" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="7.2" from_unit="cm" name="length" notes="alterIDs:o5546" src="d0_s4" to="20.5" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_width" notes="alterIDs:o5546" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="mm" name="atypical_width" notes="alterIDs:o5546" src="d0_s4" to="100" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" notes="alterIDs:o5546" src="d0_s4" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5547" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lyrate" value_original="lyrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves petiolate;</text>
      <biological_entity constraint="cauline" id="o5548" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade usually lanceolate to linear, sometimes oblanceolate to oblong, 1.5–7 (–10) cm × 3–12 (–20) mm, margins usually entire or repand, rarely lobed.</text>
      <biological_entity id="o5549" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="usually lanceolate" name="shape" src="d0_s6" to="linear" />
        <character char_type="range_value" from="oblanceolate" modifier="sometimes" name="shape" src="d0_s6" to="oblong" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="20" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5550" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes corymbose, somewhat lax, considerably elongated in fruit.</text>
      <biological_entity id="o5552" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels often divaricate, sometimes reflexed, rarely horizontal or divaricate-ascending, straight, slender, (4–) 5–13 (–19) mm, somewhat flattened at base.</text>
      <biological_entity id="o5551" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="corymbose" value_original="corymbose" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
        <character constraint="in fruit" constraintid="o5552" is_modifier="false" modifier="considerably" name="length" src="d0_s7" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o5553" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <biological_entity id="o5554" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s8" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s8" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="19" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="13" to_unit="mm" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o5551" id="r414" name="fruiting" negation="false" src="d0_s8" to="o5553" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals (equal), ascending, oblong, (2.5–) 3–5 (–5.5) × 1–1.7 (–3) mm, (lateral pair not saccate);</text>
      <biological_entity id="o5555" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" src="d0_s9" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5556" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>petals usually white, rarely lavender, usually spatulate, rarely obovate, 5–7.5 (–9.5) × 1.5–2.5 (–3.5) mm, margins not crisped, claw obscurely differentiated from blade (widest at base);</text>
      <biological_entity id="o5557" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o5558" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration_or_odor" src="d0_s10" value="lavender" value_original="lavender" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="7.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s10" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5559" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o5560" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character constraint="from blade" constraintid="o5561" is_modifier="false" modifier="obscurely" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o5561" name="blade" name_original="blade" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>nectar glands continuous, flat, subtending bases of stamens;</text>
      <biological_entity id="o5562" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="nectar" id="o5563" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="continuous" value_original="continuous" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s11" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o5564" name="base" name_original="bases" src="d0_s11" type="structure" />
      <biological_entity id="o5565" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <relation from="o5563" id="r415" name="subtending" negation="false" src="d0_s11" to="o5564" />
      <relation from="o5563" id="r416" name="part_of" negation="false" src="d0_s11" to="o5565" />
    </statement>
    <statement id="d0_s12">
      <text>filaments tetradynamous, median pairs 3–4.5 (–5.5) mm, lateral pair 2.5–3.5 (–4.5) mm;</text>
      <biological_entity id="o5566" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o5567" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
        <character is_modifier="false" name="position" src="d0_s12" value="median" value_original="median" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers included, oblong, (1–) 1.5–2.2 (–2.5) mm, not circinately coiled;</text>
      <biological_entity id="o5568" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o5569" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="included" value_original="included" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.2" to_unit="mm" />
        <character is_modifier="false" modifier="not circinately" name="architecture" src="d0_s13" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>gynophore 0.5–0.8 (–1.5) mm.</text>
      <biological_entity id="o5570" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o5571" name="gynophore" name_original="gynophore" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits divaricate to reflexed, submoniliform to strongly torulose, straight or curved, terete, (2–) 3–6.6 (–7.4) cm × 0.7–1 (–1.5) mm, (replum constricted between seeds);</text>
      <biological_entity id="o5572" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="submoniliform" name="shape" src="d0_s15" to="strongly torulose" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s15" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s15" value="terete" value_original="terete" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s15" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="6.6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s15" to="7.4" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s15" to="6.6" to_unit="cm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s15" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s15" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 26–62 per ovary;</text>
      <biological_entity id="o5573" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o5574" from="26" name="quantity" src="d0_s16" to="62" />
      </biological_entity>
      <biological_entity id="o5574" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style usually clavate to subclavate, rarely cylindrical, (0.5–) 0.8–2 (–5) mm.</text>
      <biological_entity id="o5575" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="usually clavate" name="shape" src="d0_s17" to="subclavate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s17" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="0.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s17" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds (oblong), (1–) 1.3–1.8 (–2) × 0.5–1 mm.</text>
      <biological_entity id="o5576" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_length" src="d0_s18" to="1.3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s18" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s18" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s18" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Talus, rocky slopes, cliffs, pinyon-juniper-brush communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="talus" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="pinyon-juniper-brush communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  
</bio:treatment>