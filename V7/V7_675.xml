<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">446</other_info_on_meta>
    <other_info_on_meta type="illustration_page">445</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Horaninov" date="1847" rank="tribe">calepineae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">calepina</taxon_name>
    <taxon_name authority="(Asso) Thellung in H. Schinz and R. Keller" date="1905" rank="species">irregularis</taxon_name>
    <place_of_publication>
      <publication_title>in H. Schinz and R. Keller, Fl. Schweiz ed.</publication_title>
      <place_in_publication>2, 1: 218. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe calepineae;genus calepina;species irregularis</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250094667</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Myagrum</taxon_name>
    <taxon_name authority="Asso" date="unknown" rank="species">irregulare</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Stirp. Aragon.,</publication_title>
      <place_in_publication>82. 1779</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Myagrum;species irregulare;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Calepina</taxon_name>
    <taxon_name authority="(Allioni) Desvaux" date="unknown" rank="species">corvini</taxon_name>
    <taxon_hierarchy>genus Calepina;species corvini;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Crambe</taxon_name>
    <taxon_name authority="Allioni" date="unknown" rank="species">corvini</taxon_name>
    <taxon_hierarchy>genus Crambe;species corvini;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems several from base, (1–) 2–8 dm, unbranched or branched distally.</text>
      <biological_entity id="o17813" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character constraint="from base" constraintid="o17814" is_modifier="false" name="quantity" src="d0_s0" value="several" value_original="several" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s0" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" notes="" src="d0_s0" to="8" to_unit="dm" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s0" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o17814" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Basal leaves early rosulate;</text>
      <biological_entity constraint="basal" id="o17815" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole (0.2–) 1–3.5 (–6) cm;</text>
      <biological_entity id="o17816" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade obovate, spatulate, or oblanceolate, (0.8–) 2–5 (–9) cm × (3–) 10–30 mm, base attenuate.</text>
      <biological_entity id="o17817" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_length" src="d0_s3" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="9" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s3" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="30" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17818" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves: blade oblong to lanceolate, (1–) 2–7 (–8) cm × (4–) 10–20 (–30) mm, base sagittate or amplexicaul, apex obtuse to acute.</text>
      <biological_entity constraint="cauline" id="o17819" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o17820" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17821" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="sagittate" value_original="sagittate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="amplexicaul" value_original="amplexicaul" />
      </biological_entity>
      <biological_entity id="o17822" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s4" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes with straight rachis.</text>
      <biological_entity id="o17824" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="true" name="course" src="d0_s5" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o17823" id="r1215" name="with" negation="false" src="d0_s5" to="o17824" />
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels straight or curved upward, (3–) 5–10 (–13) mm.</text>
      <biological_entity id="o17823" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="13" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17825" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <relation from="o17823" id="r1216" name="fruiting" negation="false" src="d0_s6" to="o17825" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals oblong or ovate, 1.2–2 × 0.5–1 mm;</text>
      <biological_entity id="o17826" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o17827" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals (1.5–) 2–3 × 0.5–1.5 mm, unequal, abaxial pair larger than adaxial (longer than sepals);</text>
      <biological_entity id="o17828" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o17829" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_length" src="d0_s8" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17830" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character constraint="than adaxial petals" constraintid="o17831" is_modifier="false" name="size" src="d0_s8" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17831" name="petal" name_original="petals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>filaments 1–1.5 mm;</text>
      <biological_entity id="o17832" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o17833" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers ca. 0.3 mm;</text>
      <biological_entity id="o17834" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o17835" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>gynophore 0.1–0.2 mm.</text>
      <biological_entity id="o17836" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o17837" name="gynophore" name_original="gynophore" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits 2.5–3.5 (–4) × 2–3 mm, abruptly tapering to a conical, beaklike apex, 0.5–0.8 mm.</text>
      <biological_entity id="o17838" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s12" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="3" to_unit="mm" />
        <character constraint="to apex" constraintid="o17839" is_modifier="false" modifier="abruptly" name="shape" src="d0_s12" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17839" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="conical" value_original="conical" />
        <character is_modifier="true" name="shape" src="d0_s12" value="beaklike" value_original="beaklike" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds brown, 1.3–1.6 mm. 2n = 14, 28.</text>
      <biological_entity id="o17840" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s13" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17841" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" value_original="14" />
        <character name="quantity" src="d0_s13" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Fields, waste places, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="fields" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Md., N.C., Va.; Europe; c, sw Asia; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>The fruits of Calepina irregularis and Neslia apiculata are remarkably similar; the two can easily be separated by the presence of white instead of yellow petals and by the absence of indumentum in Calepina.</discussion>
  
</bio:treatment>