<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="mention_page">438</other_info_on_meta>
    <other_info_on_meta type="treatment_page">437</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="Bunge" date="1833" rank="genus">ORYCHOPHRAGMUS</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl. China Bor.,</publication_title>
      <place_in_publication>7. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus ORYCHOPHRAGMUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek oryche, pit, and phragmos, partition, alluding to fruit septum</other_info_on_name>
    <other_info_on_name type="fna_id">123307</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials [perennials];</text>
    </statement>
    <statement id="d0_s1">
      <text>(sometimes rhizomatous);</text>
    </statement>
    <statement id="d0_s2">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s3">
      <text>pubescent or glabrous.</text>
      <biological_entity id="o29992" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems erect or ascending, unbranched or branched distally.</text>
      <biological_entity id="o29994" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s4" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate or sessile;</text>
      <biological_entity id="o29995" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s5" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal not rosulate, petiolate or sessile, blade (simple or pinnatisect with 1–6 leafletlike lobes on each side), margins crenate;</text>
      <biological_entity constraint="basal" id="o29996" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s7" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o29997" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o29998" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>cauline petiolate or sessile, blade (base sometimes auriculate or amplexicaul), margins entire, dentate, or, sometimes, with 1–4 lateral lobes [crenate or serrate].</text>
      <biological_entity constraint="cauline" id="o29999" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o30000" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity id="o30001" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o30002" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <relation from="o30001" id="r2011" modifier="sometimes" name="with" negation="false" src="d0_s8" to="o30002" />
    </statement>
    <statement id="d0_s9">
      <text>Racemes (corymbose, several-flowered).</text>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels divaricate-ascending [divaricate, recurved], slender or stout.</text>
      <biological_entity id="o30003" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s10" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o30004" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o30003" id="r2012" name="fruiting" negation="false" src="d0_s10" to="o30004" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals erect [ascending], linear [oblong], lateral pair strongly [slightly] saccate basally;</text>
      <biological_entity id="o30005" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o30006" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s11" value="linear" value_original="linear" />
        <character is_modifier="false" name="position" src="d0_s11" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="strongly; basally" name="architecture_or_shape" src="d0_s11" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals purple, lavender, or white, broadly obovate [narrowly obcordate], claw obscurely to considerably differentiated from blade ([shorter than] as long as sepals, apex rounded [emarginate]);</text>
      <biological_entity id="o30007" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o30008" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o30009" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character constraint="from blade" constraintid="o30010" is_modifier="false" modifier="obscurely to considerably" name="variability" src="d0_s12" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o30010" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o30011" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o30012" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments dilated basally;</text>
      <biological_entity id="o30013" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o30014" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers linear [oblong], (apiculate);</text>
      <biological_entity id="o30015" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o30016" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands (2), lateral, semiannular or annular.</text>
      <biological_entity id="o30017" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity constraint="nectar" id="o30018" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character name="atypical_quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s16" value="semiannular" value_original="semiannular" />
        <character is_modifier="false" name="shape" src="d0_s16" value="annular" value_original="annular" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits siliques, dehiscent, sessile or shortly stipitate, linear, torulose, terete or somewhat 4-angled;</text>
      <biological_entity constraint="fruits" id="o30019" name="silique" name_original="siliques" src="d0_s17" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s17" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s17" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s17" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s17" value="4-angled" value_original="4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves (leathery), each with prominent midvein, usually glabrous, rarely densely hirsute;</text>
      <biological_entity id="o30020" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely densely" name="pubescence" src="d0_s18" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o30021" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o30020" id="r2013" name="with" negation="false" src="d0_s18" to="o30021" />
    </statement>
    <statement id="d0_s19">
      <text>replum rounded;</text>
      <biological_entity id="o30022" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum complete;</text>
      <biological_entity id="o30023" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules [20–] 40–70 per ovary;</text>
      <biological_entity id="o30024" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character char_type="range_value" from="20" name="atypical_quantity" src="d0_s21" to="40" to_inclusive="false" />
        <character char_type="range_value" constraint="per ovary" constraintid="o30025" from="40" name="quantity" src="d0_s21" to="70" />
      </biological_entity>
      <biological_entity id="o30025" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>stigma capitate, 2-lobed (lobes distinct, decurrent).</text>
      <biological_entity id="o30026" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s22" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds uniseriate, plump, not winged, oblong;</text>
      <biological_entity id="o30027" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s23" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="size" src="d0_s23" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s23" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s23" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>seed-coat (alveolate-reticulate), not mucilaginous when wetted;</text>
      <biological_entity id="o30028" name="seed-coat" name_original="seed-coat" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s24" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>cotyledons conduplicate.</text>
    </statement>
    <statement id="d0_s26">
      <text>x = 12.</text>
      <biological_entity id="o30029" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s25" value="conduplicate" value_original="conduplicate" />
      </biological_entity>
      <biological_entity constraint="x" id="o30030" name="chromosome" name_original="" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced, Va.; Asia (China, Japan, Korea).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" value="Asia (China)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan)" establishment_means="native" />
        <character name="distribution" value="Asia (Korea)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <discussion>Species 2 (1 in the flora).</discussion>
  <discussion>Orychophragmus is fairly similar to, and closely related to, Moricandia, and they can easily be misidentified. The former has petiolate, pinnately divided, broadly auriculate proximalmost leaves, non-cucullate median sepals, apiculate anthers, and non-connivent stigma lobes. By contrast, Moricandia has sessile, entire, non-auriculate proximalmost leaves, cucullate median sepals, non-apiculate anthers, and connivent stigma lobes.</discussion>
  <discussion>Orychophragmus limprichtianus (Pax) Al-Shehbaz &amp; G. Yang is cultivated as an ornamental, rarely escaping.</discussion>
  <references>
    <reference>Al-Shehbaz, I. A. and Yang G. 2000. A revision of the Chinese endemic Orychophragmus (Brassicaceae). Novon 10: 349–353.</reference>
  </references>
  
</bio:treatment>