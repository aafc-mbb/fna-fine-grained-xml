<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">465</other_info_on_meta>
    <other_info_on_meta type="treatment_page">476</other_info_on_meta>
    <other_info_on_meta type="illustration_page">473</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cardamine</taxon_name>
    <taxon_name authority="Fernald" date="1917" rank="species">longii</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>19: 91. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus cardamine;species longii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094597</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>glabrous throughout.</text>
      <biological_entity id="o18052" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rhizomes cylindrical, slender, 0.8–1.5 mm diam.</text>
      <biological_entity id="o18053" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="diameter" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems prostrate to decumbent or erect, unbranched or branched, 0.5–4 (–6) dm.</text>
      <biological_entity id="o18054" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s3" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s3" to="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Rhizomal leaves absent.</text>
      <biological_entity constraint="rhizomal" id="o18055" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves 3–10, usually simple, rarely 3 or 5-foliolate, petiolate;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolulate or sessile;</text>
      <biological_entity constraint="cauline" id="o18056" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s5" to="10" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s5" value="5-foliolate" value_original="5-foliolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole 0.4–2.5 cm, base not auriculate;</text>
      <biological_entity id="o18057" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18058" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lateral leaflets (when present) sessile or petiolulate (to 0.2 cm), blade similar to terminal, considerably smaller;</text>
      <biological_entity constraint="lateral" id="o18059" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolulate" value_original="petiolulate" />
      </biological_entity>
      <biological_entity id="o18060" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="considerably" name="size" src="d0_s8" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>terminal leaflet blade orbicular to reniform or ovate to oblong, 0.4–3 cm × 3–20 mm, (somewhat fleshy), base subcordate to rounded or subtruncate, margins entire or repand, rarely undulate, (apex rounded).</text>
      <biological_entity constraint="leaflet" id="o18061" name="blade" name_original="blade" src="d0_s9" type="structure" constraint_original="terminal leaflet">
        <character char_type="range_value" from="orbicular" name="shape" src="d0_s9" to="reniform or ovate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s9" to="3" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18062" name="base" name_original="base" src="d0_s9" type="structure">
        <character char_type="range_value" from="subcordate" name="shape" src="d0_s9" to="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="subtruncate" value_original="subtruncate" />
      </biological_entity>
      <biological_entity id="o18063" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s9" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Racemes ebracteate.</text>
    </statement>
    <statement id="d0_s11">
      <text>Fruiting pedicels divaricate-ascending to spreading, 0.5–2 (–4) mm.</text>
      <biological_entity id="o18064" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="ebracteate" value_original="ebracteate" />
        <character char_type="range_value" from="divaricate-ascending" name="orientation" src="d0_s11" to="spreading" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18065" name="pedicel" name_original="pedicels" src="d0_s11" type="structure" />
      <relation from="o18064" id="r1250" name="fruiting" negation="false" src="d0_s11" to="o18065" />
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals ovate, 0.7–1.2 × 0.5–0.7 mm, lateral pair not saccate basally;</text>
      <biological_entity id="o18066" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o18067" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s12" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s12" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals absent or rudimentary, to 0.7 mm;</text>
      <biological_entity id="o18068" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o18069" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s13" value="rudimentary" value_original="rudimentary" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments 0.5–0.8 mm;</text>
      <biological_entity id="o18070" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o18071" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate, 0.1–0.2 mm.</text>
      <biological_entity id="o18072" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o18073" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s15" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits narrowly oblong to linear, (3–) 5–10 (–15) × 0.8–1.2 mm;</text>
      <biological_entity id="o18074" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s16" to="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_length" src="d0_s16" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s16" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s16" to="10" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s16" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 6–22 per ovary;</text>
      <biological_entity id="o18075" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o18076" from="6" name="quantity" src="d0_s17" to="22" />
      </biological_entity>
      <biological_entity id="o18076" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 0.2–0.5 (–1) mm.</text>
      <biological_entity id="o18077" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s18" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s18" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds light or yellowish-brown, oblong or ovoid, 1–1.4 × 0.7–1 mm.</text>
      <biological_entity id="o18078" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="light" value_original="light" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s19" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tidal marshes, mud flats, tidal shores of rivers, shallow water, swampy areas, shady rocky crevices covered at high tide</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tidal marshes" />
        <character name="habitat" value="mud flats" />
        <character name="habitat" value="tidal shores" constraint="of rivers , shallow water , swampy areas , shady rocky crevices covered at high tide" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="shallow water" />
        <character name="habitat" value="swampy areas" />
        <character name="habitat" value="shady rocky crevices" />
        <character name="habitat" value="high tide" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-10 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="10" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Maine, Md., Mass., N.J., N.Y., N.C., S.C., Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21.</number>
  
</bio:treatment>