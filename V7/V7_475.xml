<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="treatment_page">341</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Trautvetter" date="1879" rank="species">stenopetala</taxon_name>
    <place_of_publication>
      <publication_title>Trudy Imp. S.-Petersburgsk. Bot. Sada</publication_title>
      <place_in_publication>6: 11. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species stenopetala</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094824</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">stenopetala</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="variety">purpurea</taxon_name>
    <taxon_hierarchy>genus Draba;species stenopetala;variety purpurea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(pulvinate);</text>
      <biological_entity id="o30371" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (covered with persistent leaves, branches sometimes terminating in sterile rosettes);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o30372" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, (0.02–) 0.07–0.2 (–0.3) dm, pubescent throughout, trichomes simple and short-stalked, 2–5-rayed, 0.1–0.5 mm, (sometimes simple ones very sparse).</text>
      <biological_entity id="o30373" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.02" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.07" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.3" to_unit="dm" />
        <character char_type="range_value" from="0.07" from_unit="dm" name="some_measurement" src="d0_s4" to="0.2" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o30374" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves (densely imbricate);</text>
    </statement>
    <statement id="d0_s6">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o30375" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petiole base and margin ciliate, (trichomes simple and 2-rayed, 0.2–0.5 mm);</text>
      <biological_entity constraint="petiole" id="o30376" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o30377" name="margin" name_original="margin" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade obovate to oblong-spatulate, 0.2–0.6 (–0.8) cm × 1–2 (–3) mm, margins entire, surfaces often pubescent with simple and stalked, 2–5-rayed trichomes, 0.2–0.9 mm, sometimes glabrous and trichomes on margins only.</text>
      <biological_entity id="o30378" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s9" to="oblong-spatulate" />
        <character char_type="range_value" from="0.6" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="length" src="d0_s9" to="0.6" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30379" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o30380" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character constraint="with trichomes" constraintid="o30381" is_modifier="false" modifier="often" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="0.9" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o30381" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="simple" value_original="simple" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o30382" name="trichome" name_original="trichomes" src="d0_s9" type="structure" />
      <biological_entity id="o30383" name="margin" name_original="margins" src="d0_s9" type="structure" />
      <relation from="o30382" id="r2042" name="on" negation="false" src="d0_s9" to="o30383" />
    </statement>
    <statement id="d0_s10">
      <text>Cauline leaves 0.</text>
      <biological_entity constraint="cauline" id="o30384" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Racemes 2–5-flowered, ebracteate, not or slightly elongated in fruit;</text>
      <biological_entity id="o30385" name="raceme" name_original="racemes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-5-flowered" value_original="2-5-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o30386" is_modifier="false" modifier="not; slightly" name="length" src="d0_s11" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o30386" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>rachis slightly flexuous, pubescent as stem.</text>
      <biological_entity id="o30388" name="stem" name_original="stem" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Fruiting pedicels divaricate, straight, 2–5 (–7) mm, pubescent as stem.</text>
      <biological_entity id="o30387" name="rachis" name_original="rachis" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s12" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o30388" is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o30389" name="pedicel" name_original="pedicels" src="d0_s13" type="structure" />
      <biological_entity id="o30390" name="stem" name_original="stem" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s13" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o30387" id="r2043" name="fruiting" negation="false" src="d0_s13" to="o30389" />
    </statement>
    <statement id="d0_s14">
      <text>Flowers: sepals (spreading or reflexed), oblong, 2–3.5 mm, pubescent, (trichomes simple and 2-rayed);</text>
      <biological_entity id="o30391" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o30392" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>petals yellow or purple, linear, 2.5–5 × 0.3–0.7 mm;</text>
      <biological_entity id="o30393" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o30394" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple" value_original="purple" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s15" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s15" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers ovate, 0.3–0.4 mm.</text>
      <biological_entity id="o30395" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o30396" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits suborbicular, plane, inflated basally, flattened distally, 3–5 × 3–4 mm;</text>
      <biological_entity id="o30397" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" name="shape" src="d0_s17" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s17" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s17" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s17" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves glabrous or puberulent, trichomes simple, 0.02–0.3 mm;</text>
      <biological_entity id="o30398" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o30399" name="trichome" name_original="trichomes" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.02" from_unit="mm" name="some_measurement" src="d0_s18" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 4–10 per ovary;</text>
      <biological_entity id="o30400" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o30401" from="4" name="quantity" src="d0_s19" to="10" />
      </biological_entity>
      <biological_entity id="o30401" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style 0.2–0.6 mm.</text>
      <biological_entity id="o30402" name="style" name_original="style" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s20" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds ovoid, 1–1.3 × 0.8–1 mm. 2n = 24, 64.</text>
      <biological_entity id="o30403" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s21" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s21" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30404" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="24" value_original="24" />
        <character name="quantity" src="d0_s21" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops, talus, rocky ridges, alpine tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="talus" />
        <character name="habitat" value="rocky ridges" />
        <character name="habitat" value="tundra" modifier="alpine" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Yukon; Alaska; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>109.</number>
  <discussion>Different chromosome counts for Draba stenopetala were reported as 2n = 24 from North America and 2n = 64 from the Russian Far East (S. I. Warwick and I. A. Al-Shehbaz 2006). It is unlikely that a single species is involved, and further work is needed to verify the counts from North America.</discussion>
  
</bio:treatment>