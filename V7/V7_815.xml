<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">517</other_info_on_meta>
    <other_info_on_meta type="illustration_page">513</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="D. A. German &amp; Al-Shehbaz" date="unknown" rank="tribe">conringieae</taxon_name>
    <taxon_name authority="Heister ex Fabricius" date="1759" rank="genus">conringia</taxon_name>
    <taxon_name authority="(Linnaeus) Dumortier" date="1827" rank="species">orientalis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Belg.,</publication_title>
      <place_in_publication>123. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe conringieae;genus conringia;species orientalis</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416304</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brassica</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">orientalis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 666. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Brassica;species orientalis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erysimum</taxon_name>
    <taxon_name authority="(Linnaeus) Crantz" date="unknown" rank="species">orientale</taxon_name>
    <place_of_publication>
      <place_in_publication>1769</place_in_publication>
      <other_info_on_pub>not Miller 1768</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Erysimum;species orientale;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants sometimes winter-annuals.</text>
      <biological_entity id="o2418" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2419" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="winter-annual" value_original="winter-annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems mostly simple, (1–) 3–7 dm.</text>
      <biological_entity id="o2420" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s1" to="7" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: blade (slightly fleshy), pale green, oblanceolate to obovate, 5–9 cm, margins ± entire.</text>
      <biological_entity constraint="basal" id="o2421" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s2" value="pale green" value_original="pale green" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s2" to="obovate" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2422" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity id="o2423" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves: blade oblong to elliptic or lanceolate, (1–) 3–10 (–15) cm × (5–) 20–25 (–50) mm, base deeply cordate-amplexicaul, apex rounded.</text>
      <biological_entity constraint="cauline" id="o2424" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o2425" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s3" to="elliptic or lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s3" to="20" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="50" to_unit="mm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s3" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2426" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="deeply" name="architecture" src="d0_s3" value="cordate-amplexicaul" value_original="cordate-amplexicaul" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels ascending, straight or curved-ascending, (8–) 10–15 (–20) mm.</text>
      <biological_entity id="o2427" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s4" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="curved-ascending" value_original="curved-ascending" />
        <character char_type="range_value" from="8" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2428" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o2427" id="r209" name="fruiting" negation="false" src="d0_s4" to="o2428" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 6–8 × 1–1.5 mm, median pair narrower than lateral, apex acute;</text>
      <biological_entity id="o2429" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o2430" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s5" value="median" value_original="median" />
        <character constraint="than lateral" constraintid="o2431" is_modifier="false" name="width" src="d0_s5" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o2431" name="lateral" name_original="lateral" src="d0_s5" type="structure" />
      <biological_entity id="o2432" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals 7–12 × 2–3 mm, base attenuate, claw usually as long as sepal;</text>
      <biological_entity id="o2433" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o2434" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2435" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o2436" name="claw" name_original="claw" src="d0_s6" type="structure" />
      <biological_entity id="o2437" name="sepal" name_original="sepal" src="d0_s6" type="structure" />
      <relation from="o2436" id="r210" name="as long as" negation="false" src="d0_s6" to="o2437" />
    </statement>
    <statement id="d0_s7">
      <text>filaments 5–7 mm;</text>
      <biological_entity id="o2438" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o2439" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 1.5–2 mm.</text>
      <biological_entity id="o2440" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o2441" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits ± torulose, strongly 4-angled to ± cylindrical, 1-nerved, keeled, (5–) 8–14 cm × 2–2.5 mm;</text>
      <biological_entity id="o2442" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="strongly 4-angled" name="shape" src="d0_s9" to="more or less cylindrical" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-nerved" value_original="1-nerved" />
        <character is_modifier="false" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_length" src="d0_s9" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s9" to="14" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style cylindrical, 0.5–4 mm.</text>
      <biological_entity id="o2443" name="style" name_original="style" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds brown, 2–2.9 × 1.2–1.5 mm. 2n = 14.</text>
      <biological_entity id="o2444" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2445" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (Mar in Texas) May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="in Texas" to="Mar" from="Mar" />
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cultivated lands, grain fields, disturbed areas, waste places, roadsides, gardens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cultivated lands" />
        <character name="habitat" value="grain fields" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="gardens" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-3500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que., Sask.; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; Asia; introduced also in Mexico, nw Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="nw Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Rabbit’s-ear</other_name>
  <other_name type="common_name">rabbit-ears</other_name>
  <other_name type="common_name">treacle mustard</other_name>
  <other_name type="common_name">slinkweed</other_name>
  <discussion>Conringia orientalis was collected on ballast in New York as early as 1879. It is most abundant in the plains and prairies of both the United States and Canada (I. A. Al-Shehbaz 1985; R. C. Rollins and Al-Shehbaz 1986). In disturbed places, it has penetrated into the native vegetation over a wide area.</discussion>
  
</bio:treatment>