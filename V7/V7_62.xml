<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">72</other_info_on_meta>
    <other_info_on_meta type="mention_page">73</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="treatment_page">67</other_info_on_meta>
    <other_info_on_meta type="illustration_page">68</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="Seringe" date="1824" rank="section">herbella</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">herbacea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1018. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section herbella;species herbacea</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242445736</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.005–0.05 m, (dwarf), forming clonal mats by rhizomes.</text>
      <biological_entity id="o9951" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.005" from_unit="m" name="some_measurement" src="d0_s0" to="0.05" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o9952" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form" src="d0_s0" value="clonal" value_original="clonal" />
      </biological_entity>
      <biological_entity id="o9953" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o9951" id="r698" name="forming" negation="false" src="d0_s0" to="o9952" />
      <relation from="o9951" id="r699" name="by" negation="false" src="d0_s0" to="o9953" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect;</text>
      <biological_entity id="o9954" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches redbrown to violet, (sometimes weakly glaucous), glabrous;</text>
      <biological_entity id="o9955" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="redbrown" name="coloration" src="d0_s2" to="violet" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets yellowbrown or redbrown, glabrous.</text>
      <biological_entity id="o9956" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules absent;</text>
      <biological_entity id="o9957" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9958" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole (convex or flat to deeply grooved adaxially), 1.5–6 (–7) mm;</text>
      <biological_entity id="o9959" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s5" to="7" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9960" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>largest medial blade (2 pairs of secondary-veins arising at or close to base, arcing) circular, subcircular or broadly elliptic, 6–21 (–34) × 6–17 (–31) mm, 0.9–1.4 times as long as wide, base usually subcordate or cordate, sometimes convex or rounded, margins flat, crenulate or crenate, apex rounded, convex, retuse, or toothed, abaxial surface (not glaucous), glabrous, adaxial slightly glossy to almost dull, glabrous;</text>
      <biological_entity id="o9961" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="largest medial" id="o9962" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="circular" value_original="circular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcircular" value_original="subcircular" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="21" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="34" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s6" to="21" to_unit="mm" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="31" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s6" to="17" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="0.9-1.4" value_original="0.9-1.4" />
      </biological_entity>
      <biological_entity id="o9963" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o9964" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o9965" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9966" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9967" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="reflectance" src="d0_s6" value="glossy to almost" value_original="glossy to almost" />
        <character is_modifier="false" modifier="almost" name="reflectance" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal blade margins crenulate;</text>
      <biological_entity id="o9968" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o9969" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="shape" src="d0_s7" value="crenulate" value_original="crenulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>juvenile blade glabrous.</text>
      <biological_entity id="o9970" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o9971" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Catkins: from subterminal buds;</text>
      <biological_entity id="o9972" name="catkin" name_original="catkins" src="d0_s9" type="structure" />
      <biological_entity constraint="subterminal" id="o9973" name="bud" name_original="buds" src="d0_s9" type="structure" />
      <relation from="o9972" id="r700" name="from" negation="false" src="d0_s9" to="o9973" />
    </statement>
    <statement id="d0_s10">
      <text>staminate 3–7.5 × 1.5–5 mm, flowering branchlet 0.3–2 mm;</text>
      <biological_entity id="o9974" name="catkin" name_original="catkins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9975" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillate loosely flowered (2–11 flowers), stout to globose, 3.3–13 × 2–10 mm, flowering branchlet 0.8–3.5 mm;</text>
      <biological_entity id="o9976" name="catkin" name_original="catkins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="loosely" name="architecture" src="d0_s11" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s11" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s11" value="globose" value_original="globose" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="length" src="d0_s11" to="13" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9977" name="branchlet" name_original="branchlet" src="d0_s11" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s11" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>floral bract tawny, light rose, or brown, 0.5–1.5 mm, margins ciliate, apex rounded, retuse, or truncate, entire, abaxially glabrous.</text>
      <biological_entity id="o9978" name="catkin" name_original="catkins" src="d0_s12" type="structure" />
      <biological_entity constraint="floral" id="o9979" name="bract" name_original="bract" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="light rose" value_original="light rose" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="light rose" value_original="light rose" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9980" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o9981" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s12" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Staminate flowers: abaxial nectary 0.5–0.8 mm, adaxial nectary oblong or ovate, 0.6–1.1 mm, nectaries distinct, or connate and shallowly cupshaped;</text>
      <biological_entity id="o9982" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9983" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9984" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s13" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9985" name="nectary" name_original="nectaries" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s13" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments distinct, glabrous, or hairy on proximal 1/2;</text>
      <biological_entity id="o9986" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o9987" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character constraint="on proximal 1/2" constraintid="o9988" is_modifier="false" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o9988" name="1/2" name_original="1/2" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers shortly cylindrical or globose, 0.3–0.6 mm.</text>
      <biological_entity id="o9989" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o9990" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s15" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s15" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: abaxial nectary (0–) 0.2–0.3 mm, adaxial nectary narrowly oblong or oblong, 0.3–1.1 mm, longer or shorter than stipe, nectaries distinct or connate and shallowly cupshaped;</text>
      <biological_entity id="o9991" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9992" name="nectary" name_original="nectary" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="0.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9993" name="nectary" name_original="nectary" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
        <character constraint="than stipe" constraintid="o9994" is_modifier="false" name="height_or_length_or_size" src="d0_s16" value="longer or shorter" value_original="longer or shorter" />
      </biological_entity>
      <biological_entity id="o9994" name="stipe" name_original="stipe" src="d0_s16" type="structure" />
      <biological_entity id="o9995" name="nectary" name_original="nectaries" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s16" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stipe 0.3–1.1 mm;</text>
      <biological_entity id="o9996" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9997" name="stipe" name_original="stipe" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovary pyriform or ovoid, glabrous, beak abruptly tapering to styles;</text>
      <biological_entity id="o9998" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9999" name="ovary" name_original="ovary" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10000" name="beak" name_original="beak" src="d0_s18" type="structure">
        <character constraint="to styles" constraintid="o10001" is_modifier="false" modifier="abruptly" name="shape" src="d0_s18" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o10001" name="style" name_original="styles" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>ovules 11–18 per ovary;</text>
      <biological_entity id="o10002" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10003" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o10004" from="11" name="quantity" src="d0_s19" to="18" />
      </biological_entity>
      <biological_entity id="o10004" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>styles connate to distinct, 0.2–0.4 mm;</text>
      <biological_entity id="o10005" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10006" name="style" name_original="styles" src="d0_s20" type="structure">
        <character char_type="range_value" from="connate" name="fusion" src="d0_s20" to="distinct" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s20" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigmas broadly cylindrical or 2 plump lobes, 0.08–0.24–0.32 mm.</text>
      <biological_entity id="o10007" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o10008" name="stigma" name_original="stigmas" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s21" value="cylindrical" value_original="cylindrical" />
        <character name="quantity" src="d0_s21" value="2" value_original="2" />
        <character char_type="range_value" from="0.08" from_unit="mm" name="some_measurement" notes="" src="d0_s21" to="0.24-0.32" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10009" name="lobe" name_original="lobes" src="d0_s21" type="structure">
        <character is_modifier="true" name="size" src="d0_s21" value="plump" value_original="plump" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Capsules 2.2–7.5 mm. 2n = 38.</text>
      <biological_entity id="o10010" name="capsule" name_original="capsules" src="d0_s22" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s22" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10011" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Jun-mid Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Aug" from="late Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Snowbeds and places with good snow protection, well-drained riverbanks, sandy beaches, granite boulder ridges, steep bouldery slopes, or in marshes, usually on non-calcareous substrates, places exposed to sea-spray</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="snowbeds" constraint="with good snow protection" />
        <character name="habitat" value="places" constraint="with good snow protection" />
        <character name="habitat" value="good snow protection" />
        <character name="habitat" value="well-drained riverbanks" />
        <character name="habitat" value="sandy beaches" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="boulder ridges" />
        <character name="habitat" value="steep bouldery slopes" />
        <character name="habitat" value="marshes" modifier="or in" />
        <character name="habitat" value="non-calcareous substrates" modifier="usually on" />
        <character name="habitat" value="places exposed to sea-spray" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Man., Nfld. and Labr., N.W.T., Nunavut, Que.; Maine, N.H.; Europe (British Isles, Russia, Scandinavia, Spitzbergen); Atlantic Islands (Iceland).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" value="Europe (British Isles)" establishment_means="native" />
        <character name="distribution" value="Europe (Russia)" establishment_means="native" />
        <character name="distribution" value="Europe (Scandinavia)" establishment_means="native" />
        <character name="distribution" value="Europe (Spitzbergen)" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Iceland)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <other_name type="common_name">Snowbed willow</other_name>
  <discussion>Salix herbacea is the only willow with an amphi-Atlantic distribution. Disjunct populations occur as far west as Great Bear and Great Slave lakes, Northwest Territories. Macrofossils show that, during the late-Wisconsinan period, it occurred in North America along the glacial margin between Minnesota (R. G. Baker et al. 1999) and Cambridge, Massachusetts (G. W. Argus and M. B. Davis 1962). D. J. Beerling (1998) provided a comprehensive review of its biology and ecology.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Salix herbacea forms natural hybrids with S. arctica, S. argyrocarpa, S. fuscescens, and S. uva-ursi.</discussion>
  <discussion>Salix herbacea × S. uva-ursi (S. ×peasei Fernald) was described from Mt. Washington, New Hampshire, but occurs also in northern Quebec (G. W. Argus, unpubl.). It is morphologically intermediate between the parents. Its distinctly crenulate, broadly obovate leaves are similar to those of S. herbacea, its catkins are smaller and have fewer flowers than those of S. uva-ursi but more flowers than those of S. herbacea, its leaves are sparsely glaucous abaxially, and it has stems stouter than those of S. uva-ursi.</discussion>
  
</bio:treatment>