<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="mention_page">485</other_info_on_meta>
    <other_info_on_meta type="treatment_page">484</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="(Torrey &amp; A. Gray) Steudel" date="1840" rank="genus">IODANTHUS</taxon_name>
    <place_of_publication>
      <publication_title>Nomencl. Bot. ed.</publication_title>
      <place_in_publication>2, 1: 812. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus IODANTHUS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek iodes, violet-colored, and anthos, flower</other_info_on_name>
    <other_info_on_name type="fna_id">116450</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Cheiranthus</taxon_name>
    <taxon_name authority="Torrey &amp; A. Gray" date="unknown" rank="unranked">Iodanthus</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 72. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cheiranthus;unranked Iodanthus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous or sparsely pubescent.</text>
      <biological_entity id="o31936" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, branched distally.</text>
      <biological_entity id="o31937" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate or sessile;</text>
      <biological_entity id="o31938" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal (withered by flowering), not rosulate, petiolate, blade margins lobed or not;</text>
      <biological_entity constraint="basal" id="o31939" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o31940" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="lobed" value_original="lobed" />
        <character name="shape" src="d0_s6" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline petiolate (petioles winged) or sessile, blade (base auriculate), margins dentate, entire, or lyrately lobed.</text>
      <biological_entity constraint="cauline" id="o31941" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o31942" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o31943" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="lyrately" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="lyrately" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (lax), considerably elongated in fruit.</text>
      <biological_entity id="o31945" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate to ascending, slender or stout.</text>
      <biological_entity id="o31944" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o31945" is_modifier="false" modifier="considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s9" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o31946" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o31944" id="r2140" name="fruiting" negation="false" src="d0_s9" to="o31946" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals erect to ascending, oblong, lateral pair not or slightly saccate basally;</text>
      <biological_entity id="o31947" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o31948" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s10" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="slightly; slightly; basally" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals purple, pink, or white, spatulate, (longer than sepals), claw differentiated from blade;</text>
      <biological_entity id="o31949" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o31950" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o31951" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character constraint="from blade" constraintid="o31952" is_modifier="false" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o31952" name="blade" name_original="blade" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o31953" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o31954" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments not dilated basally;</text>
      <biological_entity id="o31955" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o31956" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers linear to narrowly oblong, (apiculate);</text>
      <biological_entity id="o31957" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o31958" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s14" to="narrowly oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectar glands: lateral annular, median glands confluent with lateral.</text>
      <biological_entity constraint="nectar" id="o31959" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s15" value="annular" value_original="annular" />
      </biological_entity>
      <biological_entity constraint="median" id="o31960" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character constraint="with lateral" constraintid="o31961" is_modifier="false" name="arrangement" src="d0_s15" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o31961" name="lateral" name_original="lateral" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits siliques, sessile or shortly stipitate, linear, smooth, terete;</text>
      <biological_entity constraint="fruits" id="o31962" name="silique" name_original="siliques" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s16" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s16" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves each with distinct midvein, glabrous;</text>
      <biological_entity id="o31963" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o31964" name="midvein" name_original="midvein" src="d0_s17" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o31963" id="r2141" name="with" negation="false" src="d0_s17" to="o31964" />
    </statement>
    <statement id="d0_s18">
      <text>replum rounded;</text>
      <biological_entity id="o31965" name="replum" name_original="replum" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>septum complete;</text>
      <biological_entity id="o31966" name="septum" name_original="septum" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 22–36 per ovary;</text>
      <biological_entity id="o31967" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o31968" from="22" name="quantity" src="d0_s20" to="36" />
      </biological_entity>
      <biological_entity id="o31968" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style distinct;</text>
      <biological_entity id="o31969" name="style" name_original="style" src="d0_s21" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s21" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma capitate.</text>
      <biological_entity id="o31970" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds uniseriate, plump, not winged, oblong;</text>
      <biological_entity id="o31971" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s23" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="size" src="d0_s23" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s23" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s23" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>seed-coat (minutely reticulate), not mucilaginous when wetted;</text>
      <biological_entity id="o31972" name="seed-coat" name_original="seed-coat" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s24" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>cotyledons accumbent.</text>
      <biological_entity id="o31973" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s25" value="accumbent" value_original="accumbent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>c, e United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="native" />
        <character name="distribution" value="e United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>41.</number>
  <discussion>Species 1.</discussion>
  <discussion>R. C. Rollins (1993) recognized four species in Iodanthus, of which three are endemic to Mexico. As summarized by R. A. Price and I. A. Al-Shehbaz (2001), molecular data clearly support treatment of the genus as monospecific and closely allied to Cardamine, as well as recognition of the three Mexican species as members of Chaunanthus, a genus unrelated to Iodanthus and members of the Thelypodieae.</discussion>
  <references>
    <reference>Rollins, R. C. 1942. A systematic study of Iodanthus. Contr. Dudley Herb. 3: 209–215.</reference>
  </references>
  
</bio:treatment>