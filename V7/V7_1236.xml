<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">712</other_info_on_meta>
    <other_info_on_meta type="treatment_page">714</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="Hooker" date="1836" rank="species">glandulosus</taxon_name>
    <taxon_name authority="(Greene) Al-Shehbaz" date="2008" rank="subspecies">niger</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>18: 280. 2008</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species glandulosus;subspecies niger</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095221</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Streptanthus</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">niger</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>13: 141. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Streptanthus;species niger;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Euklisia</taxon_name>
    <taxon_name authority="(Greene) Greene" date="unknown" rank="species">nigra</taxon_name>
    <taxon_hierarchy>genus Euklisia;species nigra;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Streptanthus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">glandulosus</taxon_name>
    <taxon_name authority="(Greene) Munz" date="unknown" rank="variety">niger</taxon_name>
    <taxon_hierarchy>genus Streptanthus;species glandulosus;variety niger;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 2–11 dm, glabrous throughout.</text>
      <biological_entity id="o41771" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s0" to="11" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Cauline leaves: blade conduplicate distally, margins entire, surfaces glabrous.</text>
      <biological_entity constraint="cauline" id="o41772" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o41773" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="arrangement_or_vernation" src="d0_s1" value="conduplicate" value_original="conduplicate" />
      </biological_entity>
      <biological_entity id="o41774" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o41775" name="surface" name_original="surfaces" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Racemes not secund;</text>
      <biological_entity id="o41776" name="raceme" name_original="racemes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="secund" value_original="secund" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>rachis flexuous.</text>
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels 10–32 mm, glabrous.</text>
      <biological_entity id="o41777" name="rachis" name_original="rachis" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="flexuous" value_original="flexuous" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s4" to="32" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o41778" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o41777" id="r2817" name="fruiting" negation="false" src="d0_s4" to="o41778" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals dark purple-black, 7–8 mm, glabrous;</text>
      <biological_entity id="o41779" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o41780" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="dark purple-black" value_original="dark purple-black" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals dark purple, 10–12 mm;</text>
      <biological_entity id="o41781" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o41782" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark purple" value_original="dark purple" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>adaxial filaments 8–10 mm.</text>
      <biological_entity id="o41783" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity constraint="adaxial" id="o41784" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits divaricate-ascending, straight;</text>
      <biological_entity id="o41785" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>valves glabrous.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 28.</text>
      <biological_entity id="o41786" name="valve" name_original="valves" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o41787" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine grasslands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine grasslands" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17e.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Subspecies niger is known from the Tiburon Peninsula, Marin County.</discussion>
  
</bio:treatment>