<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">254</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">anchonieae</taxon_name>
    <taxon_name authority="W. T. Aiton in W. Aiton and W. T. Aiton" date="unknown" rank="genus">matthiola</taxon_name>
    <taxon_name authority="(Linnaeus) W. T. Aiton in W. Aiton and W. T. Aiton" date="unknown" rank="species">incana</taxon_name>
    <place_of_publication>
      <publication_title>in W. Aiton and W. T. Aiton, Hortus Kew.</publication_title>
      <place_in_publication>4: 119. 1812</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe anchonieae;genus matthiola;species incana</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200009612</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheiranthus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">incanus</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 662. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cheiranthus;species incanus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Biennials or perennials, rarely annuals;</text>
      <biological_entity id="o39839" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>usually densely tomentose.</text>
      <biological_entity id="o39837" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="false" modifier="usually densely" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, (1–) 2.5–6 (–9) dm, (unbranched or branched distally), often tomentose.</text>
      <biological_entity id="o39840" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="1" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="2.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="9" to_unit="dm" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s2" to="6" to_unit="dm" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves often in vegetative rosettes.</text>
      <biological_entity constraint="basal" id="o39841" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o39842" name="rosette" name_original="rosettes" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="vegetative" value_original="vegetative" />
      </biological_entity>
      <relation from="o39841" id="r2695" name="in" negation="false" src="d0_s3" to="o39842" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves shortly petiolate or sessile;</text>
      <biological_entity constraint="cauline" id="o39843" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear-oblanceolate, narrowly oblong, or lanceolate, (2.5–) 4–16 (–22) cm × (5–) 8–18 (–25) mm (smaller distally), base attenuate to cuneate, margins usually entire or repand, rarely sinuate.</text>
      <biological_entity id="o39844" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_length" src="d0_s5" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="22" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="16" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s5" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="25" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39845" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="attenuate" name="shape" src="d0_s5" to="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Fruiting pedicels ascending, straight or slightly curved, (6–) 10–20 (–25) mm, thinner than fruit.</text>
      <biological_entity id="o39846" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="sinuate" value_original="sinuate" />
        <character constraint="than fruit" constraintid="o39848" is_modifier="false" name="width" src="d0_s6" value="thinner" value_original="thinner" />
      </biological_entity>
      <biological_entity id="o39847" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o39848" name="fruit" name_original="fruit" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
      </biological_entity>
      <relation from="o39846" id="r2696" name="fruiting" negation="false" src="d0_s6" to="o39847" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals linear-lanceolate to narrowly oblong, 10–15 × 2–3 mm;</text>
      <biological_entity id="o39849" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o39850" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s7" to="narrowly oblong" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals purple, violet, pink, or white, obovate to ovate, 20–30 × 7–15 mm, claw 10–17 mm (margin not crisped), apex rounded or emarginate;</text>
      <biological_entity id="o39851" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o39852" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="violet" value_original="violet" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s8" to="ovate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s8" to="30" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39853" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39854" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments 5–8 mm;</text>
      <biological_entity id="o39855" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o39856" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 3–4 mm.</text>
      <biological_entity id="o39857" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o39858" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits divaricate-ascending to suberect, latiseptate, (4–) 6–12 (–15) cm × (3–) 4–6 mm;</text>
      <biological_entity id="o39859" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" from="divaricate-ascending" name="orientation" src="d0_s11" to="suberect" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="latiseptate" value_original="latiseptate" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_length" src="d0_s11" to="6" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s11" to="15" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s11" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s11" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves densely pubescent;</text>
      <biological_entity id="o39860" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 1–5 mm;</text>
      <biological_entity id="o39861" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma without horns.</text>
      <biological_entity id="o39862" name="stigma" name_original="stigma" src="d0_s14" type="structure" />
      <biological_entity id="o39863" name="horn" name_original="horns" src="d0_s14" type="structure" />
      <relation from="o39862" id="r2697" name="without" negation="false" src="d0_s14" to="o39863" />
    </statement>
    <statement id="d0_s15">
      <text>Seeds orbicular or nearly so, 2.5–3.2 mm diam.;</text>
      <biological_entity id="o39864" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="orbicular" value_original="orbicular" />
        <character name="shape" src="d0_s15" value="nearly" value_original="nearly" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="diameter" src="d0_s15" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>wing 0.2–0.5 mm. 2n = 14.</text>
      <biological_entity id="o39865" name="wing" name_original="wing" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o39866" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ocean cliffs and bluffs, sandy areas near beaches, roadsides, abandoned gardens</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ocean cliffs" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="sandy areas" constraint="near beaches , roadsides" />
        <character name="habitat" value="beaches" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="gardens" modifier="abandoned" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif., Tex.; Europe; introduced also elsewhere in the New World, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="also elsewhere in the New World" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="past_name">Mathiola</other_name>
  <discussion>Matthiola incana is widely cultivated worldwide for its attractive, highly scented flowers.</discussion>
  
</bio:treatment>