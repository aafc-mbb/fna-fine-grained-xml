<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="treatment_page">562</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">hesperideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">HESPERIS</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 663. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 297. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe hesperideae;genus HESPERIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hesperos, evening, alluding to time when flowers of some species are most fragrant</other_info_on_name>
    <other_info_on_name type="fna_id">115229</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants with caudex;</text>
      <biological_entity id="o22655" name="caudex" name_original="caudex" src="d0_s0" type="structure" />
      <relation from="o22654" id="r1565" name="with" negation="false" src="d0_s0" to="o22655" />
    </statement>
    <statement id="d0_s1">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s2">
      <text>pubescent or glabrous, trichomes simple and/or forked, often mixed with unicellular glands on uniseriate stalks.</text>
      <biological_entity id="o22654" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22656" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s2" value="forked" value_original="forked" />
        <character constraint="with glands" constraintid="o22657" is_modifier="false" modifier="often" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o22657" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="unicellular" value_original="unicellular" />
      </biological_entity>
      <biological_entity id="o22658" name="stalk" name_original="stalks" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s2" value="uniseriate" value_original="uniseriate" />
      </biological_entity>
      <relation from="o22657" id="r1566" name="on" negation="false" src="d0_s2" to="o22658" />
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, unbranched or branched.</text>
      <biological_entity id="o22659" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate [sessile];</text>
      <biological_entity id="o22660" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal rosulate [not rosulate], blade margins entire, dentate, or pinnatifid;</text>
      <biological_entity constraint="basal" id="o22661" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o22662" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline similar to basal.</text>
      <biological_entity constraint="cauline" id="o22663" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o22664" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o22663" id="r1567" name="to" negation="false" src="d0_s7" to="o22664" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes (corymbose), considerably elongated in fruit.</text>
      <biological_entity id="o22666" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate or ascending [reflexed], slender or stout.</text>
      <biological_entity id="o22665" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o22666" is_modifier="false" modifier="considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s9" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o22667" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o22665" id="r1568" name="fruiting" negation="false" src="d0_s9" to="o22667" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals oblong [linear], (sometimes connivent), lateral pair strongly saccate basally, (pubescent or glabrous);</text>
      <biological_entity id="o22668" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o22669" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="strongly; basally" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals obovate [oblong], (much longer than sepals), claw distinctly differentiated from blade, (apex rounded [obtuse]);</text>
      <biological_entity id="o22670" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o22671" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o22672" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character constraint="from blade" constraintid="o22673" is_modifier="false" modifier="distinctly" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o22673" name="blade" name_original="blade" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>stamens strongly tetradynamous;</text>
      <biological_entity id="o22674" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o22675" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments (erect), slender or dilated basally;</text>
      <biological_entity id="o22676" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s13" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
      </biological_entity>
      <biological_entity id="o22677" name="filament" name_original="filaments" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>anthers linear [oblong], (apex obtuse);</text>
      <biological_entity id="o22678" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o22679" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectar glands (2), lateral, annular or lunar.</text>
      <biological_entity id="o22680" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity constraint="nectar" id="o22681" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character name="atypical_quantity" src="d0_s15" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s15" value="annular" value_original="annular" />
        <character name="shape" src="d0_s15" value="lunar" value_original="lunar" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits tardily dehiscent, sessile, linear, torulose;</text>
      <biological_entity id="o22682" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="tardily" name="dehiscence" src="d0_s16" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s16" value="torulose" value_original="torulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves each with prominent midvein, glabrous;</text>
      <biological_entity id="o22683" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22684" name="midvein" name_original="midvein" src="d0_s17" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s17" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o22683" id="r1569" name="with" negation="false" src="d0_s17" to="o22684" />
    </statement>
    <statement id="d0_s18">
      <text>replum rounded;</text>
      <biological_entity id="o22685" name="replum" name_original="replum" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>septum complete;</text>
      <biological_entity id="o22686" name="septum" name_original="septum" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 4–40 per ovary;</text>
      <biological_entity id="o22687" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o22688" from="4" name="quantity" src="d0_s20" to="40" />
      </biological_entity>
      <biological_entity id="o22688" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style obsolete or distinct (relatively short);</text>
      <biological_entity id="o22689" name="style" name_original="style" src="d0_s21" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s21" value="obsolete" value_original="obsolete" />
        <character is_modifier="false" name="fusion" src="d0_s21" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma conical (lobes prominent, connivent or distinct, decurrent).</text>
      <biological_entity id="o22690" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="conical" value_original="conical" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds plump, not winged, oblong;</text>
      <biological_entity id="o22691" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="size" src="d0_s23" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s23" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s23" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>seed-coat (reticulate), not mucilaginous when wetted;</text>
      <biological_entity id="o22692" name="seed-coat" name_original="seed-coat" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s24" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>cotyledons incumbent.</text>
      <biological_entity id="o22693" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s25" value="incumbent" value_original="incumbent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; se Europe, c, sw Asia, n Africa; introduced also in South America (Argentina, Chile).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se Europe" establishment_means="introduced" />
        <character name="distribution" value="c" establishment_means="introduced" />
        <character name="distribution" value="sw Asia" establishment_means="introduced" />
        <character name="distribution" value="n Africa" establishment_means="introduced" />
        <character name="distribution" value=" also in South America (Argentina)" establishment_means="introduced" />
        <character name="distribution" value=" also in South America (Chile)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>62.</number>
  <discussion>Species 25 (1 in the flora).</discussion>
  <references>
    <reference>Dvoák, F. 1966. A contribution to the study of the evolution of Hesperis series Matronales Cvl. emend. Dvoák. Feddes Repert. 73: 94–99.</reference>
    <reference>Dvoák, F. 1973. Infrageneric classification of Hesperis. Feddes Repert. 84: 259–272.</reference>
  </references>
  
</bio:treatment>