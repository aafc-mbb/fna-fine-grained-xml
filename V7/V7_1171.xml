<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">678</other_info_on_meta>
    <other_info_on_meta type="treatment_page">682</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="S. Watson" date="1871" rank="genus">caulanthus</taxon_name>
    <taxon_name authority="Payson" date="1923" rank="species">hallii</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>9: 290. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus caulanthus;species hallii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094866</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Streptanthus</taxon_name>
    <taxon_name authority="(Payson) Jepson" date="unknown" rank="species">hallii</taxon_name>
    <taxon_hierarchy>genus Streptanthus;species hallii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>sparsely to densely hispid or subglabrate.</text>
      <biological_entity id="o34085" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="subglabrate" value_original="subglabrate" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or ascending, unbranched or branched distally, (hollow, sometimes slightly inflated), 2–12 dm.</text>
      <biological_entity id="o34086" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="12" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves rosulate;</text>
      <biological_entity constraint="basal" id="o34087" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.5–3.5 cm;</text>
      <biological_entity id="o34088" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s4" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblanceolate to oblong (in outline), 1.5–11.5 cm × 5–55 mm, margins pinnately lobed (lobes dentate).</text>
      <biological_entity id="o34089" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="oblong" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s5" to="11.5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="55" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34090" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (distalmost) sessile;</text>
      <biological_entity constraint="cauline" id="o34091" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade lanceolate-linear, margins entire, (surfaces sparsely hispid).</text>
      <biological_entity id="o34092" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate-linear" value_original="lanceolate-linear" />
      </biological_entity>
      <biological_entity id="o34093" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (somewhat lax), without a terminal cluster of sterile flowers.</text>
      <biological_entity constraint="terminal" id="o34095" name="cluster" name_original="cluster" src="d0_s8" type="structure" />
      <biological_entity id="o34096" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o34094" id="r2285" name="without" negation="false" src="d0_s8" to="o34095" />
      <relation from="o34095" id="r2286" name="part_of" negation="false" src="d0_s8" to="o34096" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels ascending, 9–25 mm, hispid or subglabrate.</text>
      <biological_entity id="o34094" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="subglabrate" value_original="subglabrate" />
      </biological_entity>
      <biological_entity id="o34097" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o34094" id="r2287" name="fruiting" negation="false" src="d0_s9" to="o34097" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals erect, (creamy white), lanceolate to ovate, 3–6.5 × 1.8–2.5 mm (equal);</text>
      <biological_entity id="o34098" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o34099" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals creamy white, 6–10.5 mm, blade 3–4 × 1.5–2 mm, not crisped, claw narrowly oblanceolate or oblong, 3–6 × 2–3 mm;</text>
      <biological_entity id="o34100" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o34101" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="creamy white" value_original="creamy white" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o34102" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o34103" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments in 3 unequal pairs, abaxial pair 2.5–6 mm, lateral pair 1.5–4.5, adaxial pair 4.5–8 mm;</text>
      <biological_entity id="o34104" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o34105" name="filament" name_original="filaments" src="d0_s12" type="structure" />
      <biological_entity id="o34106" name="pair" name_original="pairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="3" value_original="3" />
        <character is_modifier="true" name="size" src="d0_s12" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o34107" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="1.5" name="quantity" src="d0_s12" to="4.5" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o34108" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
      <relation from="o34105" id="r2288" name="in" negation="false" src="d0_s12" to="o34106" />
    </statement>
    <statement id="d0_s13">
      <text>anthers narrowly oblong, unequal, 2–3 mm (adaxial pair slightly smaller).</text>
      <biological_entity id="o34109" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o34110" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits divaricate to ascending (sometimes curved), terete, 6.5–12.5 cm × 1.8–2.2 mm;</text>
      <biological_entity id="o34111" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s14" value="terete" value_original="terete" />
        <character char_type="range_value" from="6.5" from_unit="cm" name="length" src="d0_s14" to="12.5" to_unit="cm" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="width" src="d0_s14" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves each with prominent midvein;</text>
      <biological_entity id="o34112" name="valve" name_original="valves" src="d0_s15" type="structure" />
      <biological_entity id="o34113" name="midvein" name_original="midvein" src="d0_s15" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s15" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o34112" id="r2289" name="with" negation="false" src="d0_s15" to="o34113" />
    </statement>
    <statement id="d0_s16">
      <text>ovules 78–96per ovary;</text>
      <biological_entity id="o34114" name="ovule" name_original="ovules" src="d0_s16" type="structure" />
      <biological_entity id="o34115" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style to 2 mm;</text>
      <biological_entity id="o34116" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma slightly 2-lobed.</text>
      <biological_entity id="o34117" name="stigma" name_original="stigma" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s18" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 1–1.6 × 0.7–1 mm.</text>
      <biological_entity id="o34118" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s19" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky areas, chaparral, scrub</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky areas" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Caulanthus hallii is known from Riverside and San Diego counties.</discussion>
  
</bio:treatment>