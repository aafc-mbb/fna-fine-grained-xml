<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">612</other_info_on_meta>
    <other_info_on_meta type="mention_page">616</other_info_on_meta>
    <other_info_on_meta type="treatment_page">615</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="O’Kane &amp; Al-Shehbaz" date="2002" rank="genus">paysonia</taxon_name>
    <taxon_name authority="(Rollins) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">stonensis</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 381. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus paysonia;species stonensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095100</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="species">stonensis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>57: 255, plate 1210. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lesquerella;species stonensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely hirsute, trichomes simple proximally, densely pubescent distally, trichomes simple, or mixed simple, forked, and slightly branched.</text>
      <biological_entity id="o17056" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o17057" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o17058" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="mixed" value_original="mixed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="mixed" value_original="mixed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s1" value="forked" value_original="forked" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, outer ones usually decumbent at base, 2–4 dm.</text>
      <biological_entity id="o17059" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="position" src="d0_s2" value="outer" value_original="outer" />
        <character constraint="at base" constraintid="o17060" is_modifier="false" modifier="usually" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" notes="" src="d0_s2" to="4" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o17060" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: blade 3–6 cm × 8–15 mm, margins lyrately lobed to pinnatifid, (lobes) entire or sinuate-dentate (lateral lobes decurrent on leaf-rachis, triangular to broadly oblong, terminal lobes ovate to nearly orbicular, relatively large, apex obtuse), surfaces densely hirsute (abaxial with simple, relatively long trichomes, mixed with forked or branched, shorter ones, adaxial with simple trichomes).</text>
      <biological_entity constraint="basal" id="o17061" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17062" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s3" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17063" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="lyrately lobed" name="shape" src="d0_s3" to="pinnatifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="sinuate-dentate" value_original="sinuate-dentate" />
      </biological_entity>
      <biological_entity id="o17064" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves: blade broadly oblong to ovate, 1–5 cm × 5–15 mm, base auriculate, clasping, margins dentate, (proximal surfaces densely hirsute, abaxial with simple, forked, and branched trichomes, adaxial with predominantly simple ones, distal surfaces densely pubescent, trichomes predominantly forked or branched).</text>
      <biological_entity constraint="cauline" id="o17065" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o17066" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="broadly oblong" name="shape" src="d0_s4" to="ovate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17067" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="architecture_or_fixation" src="d0_s4" value="clasping" value_original="clasping" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Fruiting pedicels divaricate-ascending, straight, 10–25 mm, densely pubescent.</text>
      <biological_entity id="o17068" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s5" to="25" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o17069" name="pedicel" name_original="pedicels" src="d0_s5" type="structure" />
      <relation from="o17068" id="r1166" name="fruiting" negation="false" src="d0_s5" to="o17069" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 4.5–5.5 × 1.5–2 mm, (outer pair slightly saccate, inner pair narrower, not saccate), pubescent;</text>
      <biological_entity id="o17070" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o17071" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s6" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white, (claw pale-yellow, short), 7–9 × 5–6 mm, apex rounded to emarginate;</text>
      <biological_entity id="o17072" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o17073" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="9" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17074" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments dilated basally, (glandular tissue continuous, surrounding insertion point of single stamens, subtending that of paired stamens).</text>
      <biological_entity id="o17075" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o17076" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s8" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits subsessile, subglobose, slightly didymous, 3–5 × 4–5 mm;</text>
      <biological_entity id="o17077" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="shape" src="d0_s9" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_arrangement" src="d0_s9" value="didymous" value_original="didymous" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>valves densely hirsute, trichomes simple;</text>
      <biological_entity id="o17078" name="valve" name_original="valves" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o17079" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>replum orbicular or suborbicular (slightly wider than long, rounded at apex);</text>
      <biological_entity id="o17080" name="replum" name_original="replum" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s11" value="suborbicular" value_original="suborbicular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>septum usually perforate, sometimes complete;</text>
      <biological_entity id="o17081" name="septum" name_original="septum" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s12" value="perforate" value_original="perforate" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s12" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules 8–12 per ovary;</text>
      <biological_entity id="o17082" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o17083" from="8" name="quantity" src="d0_s13" to="12" />
      </biological_entity>
      <biological_entity id="o17083" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>style ca. 2 mm, hirsute at least proximally;</text>
      <biological_entity id="o17084" name="style" name_original="style" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" modifier="at-least proximally; proximally" name="pubescence" src="d0_s14" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigma expanded.</text>
      <biological_entity id="o17085" name="stigma" name_original="stigma" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds oval, 1.8–2 × 1.5 mm. 2n = 16.</text>
      <biological_entity id="o17086" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oval" value_original="oval" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s16" to="2" to_unit="mm" />
        <character name="width" src="d0_s16" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17087" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Flood plains, knoll tops, pastures, fields, roadsides, stream banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="knoll tops" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="stream banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tenn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8.</number>
  <other_name type="common_name">Stones River bladderpod</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Paysonia stonensis has a very limited distribution in the watershed of the East Fork of the Stones River, where it is known to form hybrids with P. densipila. The hybrid has been named Lesquerella ×maxima Rollins.</discussion>
  <discussion>Paysonia stonensis is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  
</bio:treatment>