<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">224</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">238</other_info_on_meta>
    <other_info_on_meta type="mention_page">563</other_info_on_meta>
    <other_info_on_meta type="mention_page">565</other_info_on_meta>
    <other_info_on_meta type="treatment_page">564</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Webb &amp; Berthelot" date="unknown" rank="tribe">iberideae</taxon_name>
    <taxon_name authority="W. T. Aiton in W. Aiton and W. T. Aiton" date="1812" rank="genus">TEESDALIA</taxon_name>
    <place_of_publication>
      <publication_title>in W. Aiton and W. T. Aiton, Hortus Kew.</publication_title>
      <place_in_publication>4: 83. 1812</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe iberideae;genus TEESDALIA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Robert Teesdale, 1740–1804, British botanist and gardener at Yorkshire</other_info_on_name>
    <other_info_on_name type="fna_id">202774</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals [perennials];</text>
    </statement>
    <statement id="d0_s1">
      <text>scapose;</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous or sparsely pubescent.</text>
      <biological_entity id="o30499" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems (simple or few to several from base), erect or ascending, unbranched.</text>
      <biological_entity id="o30500" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate or sessile;</text>
      <biological_entity id="o30501" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal (persistent), rosulate, petiolate, blade margins usually lyrate-pinnatifid or pinnatisect, rarely entire or dentate;</text>
      <biological_entity constraint="basal" id="o30502" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o30503" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="lyrate-pinnatifid" value_original="lyrate-pinnatifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnatisect" value_original="pinnatisect" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline (0–4), sessile, blade margins entire or dentate.</text>
      <biological_entity constraint="cauline" id="o30504" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" name="atypical_quantity" src="d0_s7" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="blade" id="o30505" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (corymbose, several-flowered).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate, slender.</text>
      <biological_entity id="o30506" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o30507" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o30506" id="r2053" name="fruiting" negation="false" src="d0_s9" to="o30507" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers (actinomorphic or zygomorphic);</text>
      <biological_entity id="o30508" name="flower" name_original="flowers" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>sepals ascending to spreading, ovate, glabrous;</text>
      <biological_entity id="o30509" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s11" to="spreading" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals white, oblong or obovate, (equal to or longer than sepals, or lateral pair much larger), claw undifferentiated from blade, (apex obtuse);</text>
      <biological_entity id="o30510" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o30511" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character constraint="from blade" constraintid="o30512" is_modifier="false" name="prominence" src="d0_s12" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
      <biological_entity id="o30512" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens (6) tetradynamous, or (4) equal;</text>
      <biological_entity id="o30513" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="atypical_quantity" src="d0_s13" value="6" value_original="6" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="tetradynamous" value_original="tetradynamous" />
        <character name="atypical_quantity" src="d0_s13" value="4" value_original="4" />
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments dilated basally (appendaged);</text>
      <biological_entity id="o30514" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s14" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate;</text>
      <biological_entity id="o30515" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>nectar glands lateral, 1 on each side of lateral stamen, median glands absent.</text>
      <biological_entity constraint="nectar" id="o30516" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character constraint="on side" constraintid="o30517" name="quantity" src="d0_s16" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o30517" name="side" name_original="side" src="d0_s16" type="structure" />
      <biological_entity constraint="lateral" id="o30518" name="stamen" name_original="stamen" src="d0_s16" type="structure" />
      <biological_entity constraint="median" id="o30519" name="gland" name_original="glands" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o30517" id="r2054" name="part_of" negation="false" src="d0_s16" to="o30518" />
    </statement>
    <statement id="d0_s17">
      <text>Fruits (often divaricate), sessile, broadly obcordate to suborbicular, slightly keeled, angustiseptate (strongly compressed), (apex notched);</text>
      <biological_entity id="o30520" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="broadly obcordate" name="shape" src="d0_s17" to="suborbicular" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s17" value="angustiseptate" value_original="angustiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves prominently veined, (apex narrowly winged), glabrous;</text>
      <biological_entity id="o30521" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s18" value="veined" value_original="veined" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>replum rounded;</text>
      <biological_entity id="o30522" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>septum complete;</text>
      <biological_entity id="o30523" name="septum" name_original="septum" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>ovules 4 per ovary;</text>
      <biological_entity id="o30524" name="ovule" name_original="ovules" src="d0_s21" type="structure">
        <character constraint="per ovary" constraintid="o30525" name="quantity" src="d0_s21" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o30525" name="ovary" name_original="ovary" src="d0_s21" type="structure" />
    </statement>
    <statement id="d0_s22">
      <text>style absent or not;</text>
      <biological_entity id="o30526" name="style" name_original="style" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s22" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>stigma capitate, entire.</text>
      <biological_entity id="o30527" name="stigma" name_original="stigma" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s23" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>Seeds uniseriate, slightly compressed, not winged, broadly ovate;</text>
      <biological_entity id="o30528" name="seed" name_original="seeds" src="d0_s24" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s24" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s24" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s24" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s24" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>seed-coat (rugulose), copiously mucilaginous when wetted;</text>
      <biological_entity id="o30529" name="seed-coat" name_original="seed-coat" src="d0_s25" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s25" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>cotyledons accumbent.</text>
    </statement>
    <statement id="d0_s27">
      <text>x = 9.</text>
      <biological_entity id="o30530" name="cotyledon" name_original="cotyledons" src="d0_s26" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s26" value="accumbent" value_original="accumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o30531" name="chromosome" name_original="" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, sw Asia (Middle East), Africa; introduced also in South America (Chile), n, c Europe, nw Africa, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="sw Asia (Middle East)" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
        <character name="distribution" value="also in South America (Chile)" establishment_means="introduced" />
        <character name="distribution" value="n" establishment_means="introduced" />
        <character name="distribution" value="c Europe" establishment_means="introduced" />
        <character name="distribution" value="nw Africa" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>64.</number>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>Teesdalia conferta (Lagasca) O. Appel is known from Europe (Portugal, Spain). For a summary of the nomenclature of Teesdalia and references on its species introduced in North America, see I. A. Al-Shehbaz (1986).</discussion>
  <references>
    <reference>Appel, O. 1998. The status of Teesdaliopsis and Teesdalia (Brassicaceae). Novon 8: 218–219.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers zygomorphic; stamens 6, tetradynamous; basal leaf blades: lateral lobes obtuse; styles ca. 0.1 mm.</description>
      <determination>1 Teesdalia nudicaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers actinomorphic; stamens 4, equal; basal leaf blades: lateral lobes acute; styles 0 mm.</description>
      <determination>2 Teesdalia coronopifolia</determination>
    </key_statement>
  </key>
</bio:treatment>