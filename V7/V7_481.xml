<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">279</other_info_on_meta>
    <other_info_on_meta type="mention_page">287</other_info_on_meta>
    <other_info_on_meta type="treatment_page">344</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Rollins" date="1984" rank="species">trichocarpa</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>214: 4. 1984</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species trichocarpa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094804</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(cespitose, densely pulvinate);</text>
      <biological_entity id="o29666" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (covered with persistent leaves, branches sometimes terminating in sterile rosettes);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o29667" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, 0.07–0.35 dm, pubescent throughout, trichomes (soft), stalked, subdendritic, (somewhat crisped), 0.1–0.5 mm, (simple ones absent).</text>
      <biological_entity id="o29668" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.07" from_unit="dm" name="some_measurement" src="d0_s4" to="0.35" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o29669" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="stalked" value_original="stalked" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subdendritic" value_original="subdendritic" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves (densely imbricate);</text>
    </statement>
    <statement id="d0_s6">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>sessile;</text>
      <biological_entity constraint="basal" id="o29670" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblong to obovate, 0.2–0.4 cm × 0.5–1.5 mm, margins entire, (ciliate, trichomes simple and branched, subdendritic, or spurred, 0.3–0.8 mm), surfaces sparsely pubescent, abaxially with stalked, 4–6-rayed stellate trichomes, 0.1–0.4 mm, adaxially with simple and 4–6-rayed trichomes, mainly on distal 1/2.</text>
      <biological_entity id="o29671" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s8" to="obovate" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="length" src="d0_s8" to="0.4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29672" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o29673" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29674" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s8" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity id="o29675" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity constraint="distal" id="o29676" name="1/2" name_original="1/2" src="d0_s8" type="structure" />
      <relation from="o29673" id="r1995" modifier="abaxially" name="with" negation="false" src="d0_s8" to="o29674" />
      <relation from="o29673" id="r1996" modifier="adaxially" name="with" negation="false" src="d0_s8" to="o29675" />
      <relation from="o29673" id="r1997" modifier="mainly" name="on" negation="false" src="d0_s8" to="o29676" />
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 0 (or 1);</text>
    </statement>
    <statement id="d0_s10">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o29677" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>blade similar to basal.</text>
      <biological_entity id="o29678" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Racemes 2–9-flowered, ebracteate, slightly elongated in fruit;</text>
      <biological_entity id="o29679" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-9-flowered" value_original="2-9-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o29680" is_modifier="false" modifier="slightly" name="length" src="d0_s12" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o29680" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>rachis not flexuous, pubescent as stem.</text>
      <biological_entity id="o29682" name="stem" name_original="stem" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruiting pedicels divaricate-ascending to ascending, straight, 1–4.5 mm, pubescent as stem.</text>
      <biological_entity id="o29681" name="rachis" name_original="rachis" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o29682" is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o29683" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <biological_entity id="o29684" name="stem" name_original="stem" src="d0_s14" type="structure">
        <character char_type="range_value" from="divaricate-ascending" name="orientation" src="d0_s14" to="ascending" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o29681" id="r1998" name="fruiting" negation="false" src="d0_s14" to="o29683" />
    </statement>
    <statement id="d0_s15">
      <text>Flowers: sepals ovate, 2–3 mm, pubescent, (trichomes short-stalked);</text>
      <biological_entity id="o29685" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o29686" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petal color unknown, broadly obovate, 2–4 × 2–2.5 mm;</text>
      <biological_entity id="o29687" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o29688" name="petal" name_original="petal" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="broadly" name="coloration" src="d0_s16" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s16" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers oblong, 0.5–0.6 mm.</text>
      <biological_entity id="o29689" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o29690" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits ovoid, plane, slightly inflated basally, flattened distally, 2–6 × 2–3.5 mm;</text>
      <biological_entity id="o29691" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly; basally" name="shape" src="d0_s18" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s18" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s18" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>valves densely pubescent, trichomes 4-rayed, 0.1–0.4 mm, (often some rays spurred or branched);</text>
      <biological_entity id="o29692" name="valve" name_original="valves" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o29693" name="trichome" name_original="trichomes" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s19" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 4–10 per ovary;</text>
      <biological_entity id="o29694" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o29695" from="4" name="quantity" src="d0_s20" to="10" />
      </biological_entity>
      <biological_entity id="o29695" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 0.3–0.7 mm.</text>
      <biological_entity id="o29696" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s21" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds oblong, 1.4–2 × 0.8–1.2 mm.</text>
      <biological_entity id="o29697" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s22" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s22" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly metamorphic soil at ecotone between sagebrush steppe and open conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="metamorphic soil" modifier="gravelly" constraint="at ecotone between sagebrush steppe and open conifer forests" />
        <character name="habitat" value="ecotone" modifier="at" constraint="between sagebrush steppe and open conifer forests" />
        <character name="habitat" value="sagebrush steppe" />
        <character name="habitat" value="open conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="2000" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>115.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Draba trichocarpa is an apomictic polyploid that appears to be closely related to D. novolympica. It is readily distinguished from that species by the primarily dendritic trichomes (and absence of simple trichomes) on the stems, pedicels, and fruits. Draba trichocarpa is known from the Stanley Basin of central Idaho (Custer County).</discussion>
  
</bio:treatment>