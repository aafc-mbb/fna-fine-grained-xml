<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">432</other_info_on_meta>
    <other_info_on_meta type="mention_page">435</other_info_on_meta>
    <other_info_on_meta type="treatment_page">433</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="de Candolle" date="1821" rank="genus">diplotaxis</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle" date="1821" rank="species">erucoides</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>2: 631. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus diplotaxis;species erucoides</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250009781</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sinapis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">erucoides</taxon_name>
    <place_of_publication>
      <publication_title>Cent. Pl. II,</publication_title>
      <place_in_publication>24. 1756</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sinapis;species erucoides;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials, not scented.</text>
      <biological_entity id="o42408" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s0" value="scented" value_original="scented" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="odor" src="d0_s0" value="scented" value_original="scented" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, 1–4 (–8) dm, densely pubescent throughout, (trichomes retrorse, appressed).</text>
      <biological_entity id="o42410" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="8" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="4" to_unit="dm" />
        <character is_modifier="false" modifier="densely; throughout" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: blades elliptic to obovate, 2.5–8 cm × 10–40 mm, margins sinuate to pinnatifid or lyrate, (2–5 lobes each side), (surfaces pubescent throughout, trichomes antrorse).</text>
      <biological_entity constraint="basal" id="o42411" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o42412" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s2" to="obovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s2" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o42413" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="sinuate" name="shape" src="d0_s2" to="pinnatifid or lyrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves (distal) sessile;</text>
      <biological_entity constraint="cauline" id="o42414" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade (base cuneate to broad, truncate), margins similar to basal, (distally reduced, subtending proximal flowers).</text>
      <biological_entity id="o42415" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Fruiting pedicels 3–10 (–22) mm.</text>
      <biological_entity id="o42416" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s5" to="22" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o42417" name="pedicel" name_original="pedicels" src="d0_s5" type="structure" />
      <relation from="o42416" id="r2852" name="fruiting" negation="false" src="d0_s5" to="o42417" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 4–5.5 mm, pubescent, trichomes ± flexuous;</text>
      <biological_entity id="o42418" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o42419" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o42420" name="trichome" name_original="trichomes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="course" src="d0_s6" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white (turning purple when dried), 7–10 × 4–5 mm;</text>
      <biological_entity id="o42421" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o42422" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s7" to="10" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>filaments 4–6.5 mm;</text>
      <biological_entity id="o42423" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o42424" name="filament" name_original="filaments" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 1.5–2 mm;</text>
      <biological_entity id="o42425" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o42426" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>gynophore obsolete or to 0.5 mm.</text>
      <biological_entity id="o42427" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o42428" name="gynophore" name_original="gynophore" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="obsolete" value_original="obsolete" />
        <character name="prominence" src="d0_s10" value="0-0.5 mm" value_original="0-0.5 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits erect-patent, 2–3.5 (–4) cm × 1.5–2 (–2.5) mm;</text>
      <biological_entity id="o42429" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect-patent" value_original="erect-patent" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s11" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s11" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s11" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>terminal segment beaklike, 2–5 mm, 1-seeded or 2-seeded.</text>
      <biological_entity constraint="terminal" id="o42430" name="segment" name_original="segment" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="beaklike" value_original="beaklike" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-seeded" value_original="1-seeded" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-seeded" value_original="2-seeded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds 1–1.2 × 0.5–0.8 mm. 2n = 14.</text>
      <biological_entity id="o42431" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s13" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o42432" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ballast and waste places</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ballast" />
        <character name="habitat" value="waste" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Que.; Calif., Mass., N.J.; Eurasia; Africa; introduced also in South America (Argentina).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="also in South America (Argentina)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Diplotaxis erucoides was introduced from Europe as a ballast plant in the last century and may have failed to persist in some of the recorded provinces and states.</discussion>
  
</bio:treatment>