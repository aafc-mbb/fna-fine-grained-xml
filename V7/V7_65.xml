<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">70</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="mention_page">81</other_info_on_meta>
    <other_info_on_meta type="treatment_page">69</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="Seringe" date="1824" rank="section">herbella</taxon_name>
    <taxon_name authority="Trautvetter" date="1832" rank="species">rotundifolia</taxon_name>
    <place_of_publication>
      <publication_title>Nouv. Mém. Soc. Imp. Naturalistes Moscou</publication_title>
      <place_in_publication>2: 304, plate 11. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section herbella;species rotundifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">200006005</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.005–0.05 m, (dwarf), forming clones by rhizomes.</text>
      <biological_entity id="o11197" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.005" from_unit="m" name="some_measurement" src="d0_s0" to="0.05" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11198" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure" />
      <relation from="o11197" id="r783" name="forming clones" negation="false" src="d0_s0" to="o11198" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect;</text>
      <biological_entity id="o11199" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches yellow-green, yellowbrown, or gray-brown, glabrous;</text>
      <biological_entity id="o11200" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets yellowbrown or redbrown, glabrous;</text>
      <biological_entity id="o11201" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches and branchlets sometimes weakly glaucous.</text>
      <biological_entity id="o11202" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes weakly" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o11203" name="branchlet" name_original="branchlets" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes weakly" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves (marcescent but not skeletonized), stipules usually absent or rudimentary, rarely present on late ones;</text>
      <biological_entity id="o11204" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o11205" name="stipule" name_original="stipules" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s5" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="rudimentary" value_original="rudimentary" />
        <character constraint="on ones" constraintid="o11206" is_modifier="false" modifier="rarely" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11206" name="one" name_original="ones" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>petiole (convex, or shallowly to deeply grooved, flat), 0.4–4.6 (–5.5) mm, (glabrous adaxially);</text>
      <biological_entity id="o11207" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="4.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s6" to="4.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>largest medial blade (2 pairs of secondary-veins arising at or close to base, arcing toward apex) broadly elliptic, subcircular, or circular, 1.9–16.3 × 3–10.5 mm, 0.84–1.17 (–2.53) times as long as wide, base rounded or convex, margins flat, entire, ciliate, apex retuse, rounded, convex, or acute, abaxial surface glabrous, adaxial highly glossy, glabrous;</text>
      <biological_entity constraint="largest medial" id="o11208" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="subcircular" value_original="subcircular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="circular" value_original="circular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="subcircular" value_original="subcircular" />
        <character is_modifier="false" name="shape" src="d0_s7" value="circular" value_original="circular" />
        <character char_type="range_value" from="1.9" from_unit="mm" name="length" src="d0_s7" to="16.3" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="10.5" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s7" value="0.84-1.17(-2.53)" value_original="0.84-1.17(-2.53)" />
      </biological_entity>
      <biological_entity id="o11209" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o11210" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o11211" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11212" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11213" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="highly" name="reflectance" src="d0_s7" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>proximal blade margins entire;</text>
      <biological_entity constraint="blade" id="o11214" name="margin" name_original="margins" src="d0_s8" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>juvenile blade glabrous or puberulent.</text>
      <biological_entity id="o11215" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Catkins from subterminal buds;</text>
      <biological_entity constraint="subterminal" id="o11217" name="bud" name_original="buds" src="d0_s10" type="structure" />
      <relation from="o11216" id="r784" name="from" negation="false" src="d0_s10" to="o11217" />
    </statement>
    <statement id="d0_s11">
      <text>staminate subglobose, stout, or indeterminate, 3.3–18.5 × 2.5–12 mm, flowering branchlet 0.5–9 mm;</text>
      <biological_entity id="o11216" name="catkin" name_original="catkins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pistillate moderately densely to loosely flowered (2–15 flowers), stout, subglobose, globose, or indeterminate, 4.5–35 × 2–17 mm, flowering branchlet 0.5–22 mm;</text>
      <biological_entity id="o11218" name="branchlet" name_original="branchlet" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s11" value="stout" value_original="stout" />
        <character is_modifier="false" name="development" src="d0_s11" value="indeterminate" value_original="indeterminate" />
        <character is_modifier="true" name="life_cycle" src="d0_s11" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="moderately densely" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11219" name="branchlet" name_original="branchlet" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="densely to loosely" name="architecture" src="d0_s12" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s12" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character is_modifier="false" name="development" src="d0_s12" value="indeterminate" value_original="indeterminate" />
        <character is_modifier="true" name="life_cycle" src="d0_s12" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>floral bract brown, 1.6–2.8 mm, apex rounded or retuse, entire, abaxially sparsely hairy or ciliate, hairs usually wavy, crinkled or curly, rarely straight.</text>
      <biological_entity constraint="floral" id="o11220" name="bract" name_original="bract" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s13" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11221" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s13" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially sparsely" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s13" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o11222" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s13" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="shape" src="d0_s13" value="crinkled" value_original="crinkled" />
        <character is_modifier="false" name="shape" src="d0_s13" value="curly" value_original="curly" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s13" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Staminate flowers: abaxial nectary 0.5–1 mm, adaxial nectary narrowly oblong or oblong, 0.8–1.4 mm, nectaries distinct;</text>
      <biological_entity id="o11223" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11224" name="nectary" name_original="nectary" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11225" name="nectary" name_original="nectary" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11226" name="nectary" name_original="nectaries" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments distinct or connate less than 1/2 their lengths, glabrous;</text>
      <biological_entity id="o11227" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o11228" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s15" to="1/2" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers ellipsoid or globose, 0.4–0.6 mm.</text>
      <biological_entity id="o11229" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o11230" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s16" value="globose" value_original="globose" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s16" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Pistillate flowers: abaxial nectary present or absent, adaxial nectary usually narrowly oblong or oblong, sometimes flask-shaped, 0.8–2 mm, longer than stipe;</text>
      <biological_entity id="o11231" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11232" name="nectary" name_original="nectary" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11233" name="nectary" name_original="nectary" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="usually narrowly" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s17" value="flask--shaped" value_original="flask--shaped" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
        <character constraint="than stipe" constraintid="o11234" is_modifier="false" name="length_or_size" src="d0_s17" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o11234" name="stipe" name_original="stipe" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>stipe 0.4–0.8 mm;</text>
      <biological_entity id="o11235" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11236" name="stipe" name_original="stipe" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s18" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovary pyriform, glabrous or puberulent, (hairs in patches, especially on beak), beak slightly bulged below styles;</text>
      <biological_entity id="o11237" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11238" name="ovary" name_original="ovary" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o11239" name="beak" name_original="beak" src="d0_s19" type="structure" />
      <biological_entity id="o11240" name="style" name_original="styles" src="d0_s19" type="structure" />
      <relation from="o11239" id="r785" modifier="below" name="bulged" negation="false" src="d0_s19" to="o11240" />
    </statement>
    <statement id="d0_s20">
      <text>ovules 7–17 per ovary;</text>
      <biological_entity id="o11241" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11242" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o11243" from="7" name="quantity" src="d0_s20" to="17" />
      </biological_entity>
      <biological_entity id="o11243" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>styles connate or slightly distinct distally, 0.5–1 mm;</text>
      <biological_entity id="o11244" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11245" name="style" name_original="styles" src="d0_s21" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s21" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="slightly; distally" name="fusion" src="d0_s21" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s21" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigmas flat, abaxially non-papillate with pointed tip, or slenderly or broadly cylindrical, 0.28–0.6 mm.</text>
      <biological_entity id="o11246" name="flower" name_original="flowers" src="d0_s22" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s22" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o11247" name="stigma" name_original="stigmas" src="d0_s22" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s22" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o11248" is_modifier="false" modifier="abaxially" name="relief" src="d0_s22" value="non-papillate" value_original="non-papillate" />
        <character is_modifier="false" modifier="slenderly; broadly" name="shape" notes="" src="d0_s22" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.28" from_unit="mm" name="some_measurement" src="d0_s22" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11248" name="tip" name_original="tip" src="d0_s22" type="structure">
        <character is_modifier="true" name="shape" src="d0_s22" value="pointed" value_original="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Capsules 3.8–8.3 mm.</text>
      <biological_entity id="o11249" name="capsule" name_original="capsules" src="d0_s23" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="some_measurement" src="d0_s23" to="8.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Yukon; Alaska, Mont., Wyo.; e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Salix rotundifolia is closely related to S. polaris, from which it can be separated by its glabrous ovaries and fewer-flowered catkins. They also differ somewhat in leaf venation: S. rotundifolia typically having three main veins arising from the leaf base, often only one or two pair of secondary veins, and no or indistinct tertiary veins; S. polaris typically having pinnate venation, multiple secondary veins, and distinct tertiary veins. Salix rotundifolia consists of two varieties, the diploid var. dodgeana and the hexaploid var. rotundifolia. In general, var. dodgeana is a high alpine species in the southern cordillera of Wyoming and Montana, the St. Elias Mountains in Alaska and Yukon, the Mackenzie Mountains, Northwest Territories, and the Richardson Mountains, Yukon Territory. A diploid specimen of S. rotundifolia in the Cherski Mountains, Yakutia, Russia (B. A. Jurtzev and P. G. Zhukova 1982), which fits var. dodgeana in its 2–3-flowered catkins, relatively small leaves (3.5 × 3.9 mm), and small stomata (490 µm2), may represent an ancestral population. Variety rotundifolia usually occurs at lower elevations in Alaska and in easternmost Chukotka and Wrangel Island, Russia, but elevation separation is not distinct. There is a general correlation between stomatal size and ploidal level (W. Buechler, pers. comm.), but relatively large stomata in some diploid specimens of S. rotundifolia indicates a need for further cytological study. For the present, it is best to recognize the two cytotypes as varieties.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Salix rotundifolia forms natural hybrids with S. arctica, S. phlebophylla, and S. polaris.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistillate catkins: (3-)4-7-15 flowers; largest medial blades 4.5-8-16.3 mm, 0.92-1.23-2.27 times as long as wide; petioles 0.5-2-4.6 (-5.5) mm; floral bracts: hairs usually wavy, some straight, curly, or crinkled, exceeding bract by 0.32-0.71-1.25(-2.4) mm; pistillate flowers: abaxial nectaries present or absent; 2n = 114.</description>
      <determination>31a Salix rotundifolia var. rotundifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Pistillate catkins: 2-4-9 flowers; largest medial blades 2.9-6.1-7.4 mm, 0.84-1.5-2.2 times as long as wide; petioles 0.4-1.1-2.8 mm; floral bracts: hairs usually wavy, crinkled, or curly, rarely straight, exceeding bract by 0.1-0.37-0.75 mm; pistillate flowers: abaxial nectaries absent; 2n = 38.</description>
      <determination>31b Salix rotundifolia var. dodgeana</determination>
    </key_statement>
  </key>
</bio:treatment>