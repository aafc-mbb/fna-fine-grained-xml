<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">420</other_info_on_meta>
    <other_info_on_meta type="mention_page">423</other_info_on_meta>
    <other_info_on_meta type="mention_page">437</other_info_on_meta>
    <other_info_on_meta type="mention_page">442</other_info_on_meta>
    <other_info_on_meta type="treatment_page">422</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">brassica</taxon_name>
    <taxon_name authority="(Linnaeus) W. D. J. Koch in J. C. Röhling" date="1833" rank="species">nigra</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Röhling, Deutschl. Fl. ed.</publication_title>
      <place_in_publication>3, 4: 713. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus brassica;species nigra</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200009265</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sinapis</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">nigra</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 668. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sinapis;species nigra;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>sparsely to densely hirsute-hispid (at least basally, proximally rarely subglabrate).</text>
      <biological_entity id="o30621" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s1" value="hirsute-hispid" value_original="hirsute-hispid" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems usually branched distally, (widely spreading), 3–20 dm.</text>
      <biological_entity id="o30622" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s2" to="20" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: petiole to 10 cm;</text>
      <biological_entity constraint="basal" id="o30623" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o30624" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade lyrate-pinnatifid to sinuate-lobed, 6–30 cm × 10–100 mm, lobes 1–3 each side, (smaller than terminal, terminal lobe ovate, obtuse).</text>
      <biological_entity constraint="basal" id="o30625" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o30626" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lyrate-pinnatifid" name="shape" src="d0_s4" to="sinuate-lobed" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="100" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30627" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity id="o30628" name="side" name_original="side" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves sessile or subsessile;</text>
      <biological_entity constraint="cauline" id="o30629" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade (ovate-elliptic to lanceolate, similar to basal, reduced distally and less divided), base tapered, not auriculate or amplexicaul, (margins entire to sinuate-serrate).</text>
      <biological_entity id="o30630" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <biological_entity id="o30631" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="amplexicaul" value_original="amplexicaul" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes not paniculately branched.</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels erect (straight), (2–) 3–5 (–6) mm.</text>
      <biological_entity id="o30632" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not paniculately" name="architecture" src="d0_s7" value="branched" value_original="branched" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30633" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o30632" id="r2058" name="fruiting" negation="false" src="d0_s8" to="o30633" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 4–6 (–7) × 1–1.5 mm;</text>
      <biological_entity id="o30634" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o30635" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s9" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals yellow, ovate, 7–11 (–13) × (2.5–) 3–4.5 (–5.5) mm, claw 3–6 mm, apex rounded;</text>
      <biological_entity id="o30636" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o30637" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="13" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s10" to="11" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_width" src="d0_s10" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30638" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30639" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 3.5–5 mm;</text>
      <biological_entity id="o30640" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o30641" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 1–1.5 mm.</text>
      <biological_entity id="o30642" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o30643" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits erect-ascending (± appressed to rachis), smooth, ± 4-angled, 1–2.5 (–2.7) cm × (1.5–) 2–3 (–4) mm;</text>
      <biological_entity id="o30644" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s13" value="erect-ascending" value_original="erect-ascending" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s13" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s13" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s13" to="2.7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s13" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s13" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s13" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>valvular segment 2–5 (–8) -seeded per locule, (0.4–) 0.8–2 (–2.5) cm, terminal segment seedless (linear, narrow), (1–) 2–5 (–6) mm.</text>
      <biological_entity constraint="valvular" id="o30645" name="segment" name_original="segment" src="d0_s14" type="structure">
        <character constraint="per locule" constraintid="o30646" is_modifier="false" name="architecture" src="d0_s14" value="2-5(-8)-seeded" value_original="2-5(-8)-seeded" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s14" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s14" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" notes="" src="d0_s14" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30646" name="locule" name_original="locule" src="d0_s14" type="structure" />
      <biological_entity constraint="terminal" id="o30647" name="segment" name_original="segment" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="seedless" value_original="seedless" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds brown to black, 1.2–1.5 (–2) mm diam.;</text>
      <biological_entity id="o30648" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s15" to="black" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="diameter" src="d0_s15" to="2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="diameter" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>seed-coat coarsely reticulate, minutely alveolate, not mucilaginous when wetted.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 16.</text>
      <biological_entity id="o30649" name="seed-coat" name_original="seed-coat" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="coarsely" name="architecture_or_coloration_or_relief" src="d0_s16" value="reticulate" value_original="reticulate" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s16" value="alveolate" value_original="alveolate" />
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s16" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30650" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, disturbed areas, waste places, fields, orchards</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="disturbed areas" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="orchards" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., N.B., Nfld. and Labr., N.S., Ont., P.E.I., Que., Sask.; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; Asia; Africa; introduced also in Mexico, Central America, South America, Atlantic Islands, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="also in Mexico" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America" establishment_means="introduced" />
        <character name="distribution" value="Atlantic Islands" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>5.</number>
  <other_name type="common_name">Black mustard</other_name>
  <discussion>Brassica nigra is widely cultivated as a condiment mustard. It is also a cosmopolitan weed especially common in the valleys of California (R. C. Rollins 1993). It occurs only sporadically in southern Canada but most frequently in Ontario and along the St. Lawrence River. Specimens from Alberta, Arkansas, Delaware, and South Carolina have not been observed.</discussion>
  
</bio:treatment>