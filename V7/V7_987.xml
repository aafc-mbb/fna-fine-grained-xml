<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">604</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">Physarieae</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(1,1): 100. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe Physarieae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20870</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, perennials, or subshrubs;</text>
      <biological_entity id="o31332" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>eglandular.</text>
      <biological_entity id="o31333" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o31335" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="eglandular" value_original="eglandular" />
        <character name="growth_form" value="subshrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Trichomes usually short-stalked, subsessile, or sessile, sometimes long-stalked, stellate, scalelike, subdendritic, or forked, sometimes mixed with simple ones.</text>
      <biological_entity id="o31336" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="long-stalked" value_original="long-stalked" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="stellate" value_original="stellate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="subdendritic" value_original="subdendritic" />
        <character is_modifier="false" name="shape" src="d0_s2" value="forked" value_original="forked" />
        <character constraint="with ones" constraintid="o31337" is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o31337" name="one" name_original="ones" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves petiolate, sessile, or subsessile;</text>
      <biological_entity constraint="cauline" id="o31338" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade base usually not auriculate (except Paysonia), margins entire, dentate, or sinuate.</text>
      <biological_entity constraint="blade" id="o31339" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s4" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o31340" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sinuate" value_original="sinuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes ebracteate, often elongated in fruit.</text>
      <biological_entity id="o31341" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o31342" is_modifier="false" modifier="often" name="length" src="d0_s5" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o31342" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers actinomorphic;</text>
      <biological_entity id="o31343" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="actinomorphic" value_original="actinomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals erect, spreading, ascending, or reflexed, lateral pair seldom saccate basally;</text>
      <biological_entity id="o31344" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="seldom; basally" name="architecture_or_shape" src="d0_s7" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals white, yellow, lavender, purple, violet, orange, or brown [pink], claw present, often distinct;</text>
      <biological_entity id="o31345" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="lavender" value_original="lavender" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="violet" value_original="violet" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o31346" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments unappendaged, not winged;</text>
      <biological_entity id="o31347" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="unappendaged" value_original="unappendaged" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pollen (3 or) 4–11-colpate.</text>
      <biological_entity id="o31348" name="pollen" name_original="pollen" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="4-11-colpate" value_original="4-11-colpate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits silicles or siliques, dehiscent, unsegmented, terete, latiseptate, or angustiseptate;</text>
      <biological_entity constraint="fruits" id="o31349" name="silicle" name_original="silicles" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="latiseptate" value_original="latiseptate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="angustiseptate" value_original="angustiseptate" />
      </biological_entity>
      <biological_entity constraint="fruits" id="o31350" name="silique" name_original="siliques" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="latiseptate" value_original="latiseptate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="angustiseptate" value_original="angustiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 2–100 per ovary;</text>
      <biological_entity id="o31351" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o31352" from="2" name="quantity" src="d0_s12" to="100" />
      </biological_entity>
      <biological_entity id="o31352" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>style usually distinct;</text>
      <biological_entity id="o31353" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma entire or strongly 2-lobed.</text>
      <biological_entity id="o31354" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s14" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds biseriate, uniseriate, or aseriate;</text>
      <biological_entity id="o31355" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="biseriate" value_original="biseriate" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="aseriate" value_original="aseriate" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="arrangement" src="d0_s15" value="aseriate" value_original="aseriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>cotyledons accumbent or incumbent.</text>
      <biological_entity id="o31356" name="cotyledon" name_original="cotyledons" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="accumbent" value_original="accumbent" />
        <character is_modifier="false" name="arrangement" src="d0_s16" value="incumbent" value_original="incumbent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, South America, Asia (ne Russia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Asia (ne Russia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>aa.</number>
  <discussion>Genera 7, species ca. 130 (7 genera, 105 species in the flora).</discussion>
  
</bio:treatment>