<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">136</other_info_on_meta>
    <other_info_on_meta type="mention_page">137</other_info_on_meta>
    <other_info_on_meta type="mention_page">139</other_info_on_meta>
    <other_info_on_meta type="mention_page">144</other_info_on_meta>
    <other_info_on_meta type="mention_page">147</other_info_on_meta>
    <other_info_on_meta type="mention_page">148</other_info_on_meta>
    <other_info_on_meta type="treatment_page">146</other_info_on_meta>
    <other_info_on_meta type="illustration_page">145</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="(Andersson) Rouy" date="1910" rank="section">villosae</taxon_name>
    <taxon_name authority="(Andersson) Coville" date="1900" rank="species">alaxensis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">alaxensis</taxon_name>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section villosae;species alaxensis;variety alaxensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242445631</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 1–7 m.</text>
      <biological_entity id="o32374" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches redbrown, densely villous;</text>
      <biological_entity id="o32376" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o32377" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branchlets not noticeably glaucous, very densely villous, hairs white or yellowish.</text>
      <biological_entity id="o32378" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o32379" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not noticeably" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="very densely" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o32380" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Largest medial leaves: midrib evident, moderately densely tomentose to sparsely pubescent, abaxial surface not noticeably glaucous.</text>
      <biological_entity constraint="largest medial" id="o32381" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o32382" name="midrib" name_original="midrib" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="evident" value_original="evident" />
        <character char_type="range_value" from="moderately densely tomentose" name="pubescence" src="d0_s3" to="sparsely pubescent" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o32383" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not noticeably" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Catkins: pistillate 33–85 (–90 in fruit) × 10–22 mm, flowering branchlet 0–2 mm. 2n = 38.</text>
      <biological_entity id="o32384" name="catkin" name_original="catkins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="33" from_unit="mm" name="length" src="d0_s4" to="85" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s4" to="22" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o32385" name="branchlet" name_original="branchlet" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o32386" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid Apr-mid Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Jul" from="mid Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Stream and lake shores, terraces on coarse, calcareous gravel, well-watered scree slopes, well-drained to wet sand plains and dune remnants on deltas, wet alpine and subalpine meadows and thickets</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="stream" />
        <character name="habitat" value="lake shores" />
        <character name="habitat" value="terraces" constraint="on coarse , calcareous gravel , well-watered scree slopes" />
        <character name="habitat" value="coarse" />
        <character name="habitat" value="calcareous gravel" />
        <character name="habitat" value="well-watered scree slopes" />
        <character name="habitat" value="well-drained to wet sand plains" />
        <character name="habitat" value="dune remnants" />
        <character name="habitat" value="deltas" modifier="on" />
        <character name="habitat" value="wet alpine" />
        <character name="habitat" value="subalpine meadows" />
        <character name="habitat" value="thickets" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.W.T., Nunavut, Que., Yukon; Alaska; Asia (n, e Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (n)" establishment_means="native" />
        <character name="distribution" value="Asia (e Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>95a.</number>
  <discussion>Variety alaxensis is one of the tallest “trees” in the Canadian Arctic. An extensive stand of willows, some of which reach tree-size, occurs in a valley on deep marine, lake, or river gravels and sands southeast of Deception Bay in northern Ungava, Quebec (P. F. Maycock and J. B. Matthews 1966). The largest of the dominant willows of tree stature, var. alaxensis and Salix planifolia were ca. 5 m tall and 20 cm in diameter. The maximum age for a stem 10 cm in diameter was 60 years, but at this age there was heartwood decay. An outlier stand of tree-sized willows on Victoria Island, Northwest Territories, reached 6–8 m, to 81 years of age (S. A. Edlund and P. A. Egginton 1984).</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Variety alaxensis forms natural hybrids with Salix calcicola, S. drummondiana, S. pellita, and S. planifolia.</discussion>
  <discussion>Variety alaxensis × Salix calcicola was discovered by M. Blondeau at Kangigsujuak, Quebec. The plants resemble S. calcicola in having broad leaves and stipules, and reddish styles, and var. alaxensis in having densely villous leaves and branchlets, and relatively short pistillate flowering branchlets. The ovaries have hairy beaks or are sparsely hairy throughout.</discussion>
  <discussion>Variety alaxensis × Salix drummondiana: In the northern Rocky Mountains, plants resembling S. drummondiana but with stipules prominent, linear or lanceolate, and foliaceous, and leaves abaxially densely woolly may be this hybrid.</discussion>
  <discussion>Variety alaxensis × Salix pellita occurs in the Churchill, Manitoba, region where it grows with var. alaxensis, S. pellita, and S. planifolia. The plants resemble var. alaxensis in having very densely villous branchlets, and leaves with short wavy hairs on abaxial surfaces. The leaf indumentum is sparse, and ferruginous hairs often occur on juvenile and late leaves. Strongly revolute margins suggest S. pellita as the second parent.</discussion>
  <discussion>Variety alaxensis × Salix planifolia occurs in the Churchill, Manitoba, region where it grows with the two parental species. It resembles var. alaxensis in its very densely villous branchlets and leaves with short wavy hairs on abaxial surfaces. Leaf indumentum is sparse, and ferruginous hairs often occur on juvenile and late leaves.</discussion>
  
</bio:treatment>