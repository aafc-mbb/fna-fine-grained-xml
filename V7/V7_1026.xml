<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">621</other_info_on_meta>
    <other_info_on_meta type="treatment_page">629</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(Rollins) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">calcicola</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 322. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species calcicola</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095092</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="species">calcicola</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Bot.</publication_title>
      <place_in_publication>26: 419, fig. 1A,B. 1939</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lesquerella;species calcicola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(compact);</text>
      <biological_entity id="o6864" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched;</text>
    </statement>
    <statement id="d0_s3">
      <text>densely (silvery) pubescent, trichomes (sessile or short-stalked), 5–8-rayed, rays distinct, furcate or bifurcate, (umbonate, tuberculate and the center less so).</text>
      <biological_entity id="o6865" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o6866" name="trichome" name_original="trichomes" src="d0_s3" type="structure" />
      <biological_entity id="o6867" name="ray" name_original="rays" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s3" value="furcate" value_original="furcate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="bifurcate" value_original="bifurcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems several from base, erect or outer ones decumbent, (unbranched, stout, usually sparsely leaved), 1–3 dm.</text>
      <biological_entity id="o6868" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character constraint="from base" constraintid="o6869" is_modifier="false" name="quantity" src="d0_s4" value="several" value_original="several" />
      </biological_entity>
      <biological_entity id="o6869" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o6870" name="whole-organism" name_original="" src="d0_s4" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s4" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s4" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade linear, 2–7 (–10) cm, margins entire, repand, or shallowly dentate.</text>
      <biological_entity constraint="basal" id="o6871" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o6872" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6873" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (sessile);</text>
      <biological_entity constraint="cauline" id="o6874" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>blade (erect), spatulate to linear, (1–) 2–3 (–4.5) cm, margins entire, sometimes involute, (apex acute or subacute).</text>
      <biological_entity id="o6875" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s7" to="linear" />
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s7" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6876" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="sometimes" name="shape_or_vernation" src="d0_s7" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes dense, (exceeding basal leaves).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels (spreading, sharply sigmoid), 8–15 mm.</text>
      <biological_entity id="o6877" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="density" src="d0_s8" value="dense" value_original="dense" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6878" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o6877" id="r501" name="fruiting" negation="false" src="d0_s9" to="o6878" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals ovate or oblong, (4.5–) 5–6 (–7) mm, (lateral pair subsaccate, cucullate, median pair thickened, cucullate apically);</text>
      <biological_entity id="o6879" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6880" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals spatulate, 7–9 (–11) mm (widened at base, slightly retuse).</text>
      <biological_entity id="o6881" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6882" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="11" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits (sessile or substipitate), ovate to oblong, not compressed at distal margins or apex, 5–9 mm;</text>
      <biological_entity id="o6883" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="oblong" />
        <character constraint="at apex" constraintid="o6885" is_modifier="false" modifier="not" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6884" name="margin" name_original="margins" src="d0_s12" type="structure" />
      <biological_entity id="o6885" name="apex" name_original="apex" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>valves sparsely pubescent, trichomes appressed;</text>
      <biological_entity id="o6886" name="valve" name_original="valves" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o6887" name="trichome" name_original="trichomes" src="d0_s13" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s13" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovules 4–8 per ovary;</text>
      <biological_entity id="o6888" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o6889" from="4" name="quantity" src="d0_s14" to="8" />
      </biological_entity>
      <biological_entity id="o6889" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style 3–5 mm.</text>
      <biological_entity id="o6890" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds flattened.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 16, ca. 20.</text>
      <biological_entity id="o6891" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6892" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="16" value_original="16" />
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shale bluffs, limestone hillsides, gypseous knolls and ravines, calcareous substrates, grasslands and pinyon-juniper communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shale bluffs" />
        <character name="habitat" value="limestone hillsides" />
        <character name="habitat" value="knolls" modifier="gypseous" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="calcareous substrates" />
        <character name="habitat" value="grasslands" />
        <character name="habitat" value="pinyon-juniper communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12.</number>
  <other_name type="common_name">Rocky Mountain bladderpod</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>