<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">704</other_info_on_meta>
    <other_info_on_meta type="treatment_page">715</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="Jepson" date="1893" rank="species">hesperidis</taxon_name>
    <place_of_publication>
      <publication_title>Erythea</publication_title>
      <place_in_publication>1: 14. 1893</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species hesperidis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094996</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pleiocardia</taxon_name>
    <taxon_name authority="(Jepson) Greene" date="unknown" rank="species">hesperidis</taxon_name>
    <taxon_hierarchy>genus Pleiocardia;species hesperidis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Streptanthus</taxon_name>
    <taxon_name authority="A. Gray" date="unknown" rank="species">breweri</taxon_name>
    <taxon_name authority="(Jepson) Jepson" date="unknown" rank="variety">hesperidis</taxon_name>
    <taxon_hierarchy>genus Streptanthus;species breweri;variety hesperidis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>glabrous.</text>
      <biological_entity id="o39516" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems unbranched or branched basally, 1–3 dm.</text>
      <biological_entity id="o39517" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s2" to="3" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (soon withered);</text>
    </statement>
    <statement id="d0_s4">
      <text>not rosulate;</text>
    </statement>
    <statement id="d0_s5">
      <text>shortly petiolate;</text>
      <biological_entity constraint="basal" id="o39518" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade broadly obovate, 2–3 cm, margins coarsely and bluntly dentate distally.</text>
      <biological_entity id="o39519" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o39520" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="bluntly; distally" name="architecture_or_shape" src="d0_s6" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves: blade (mostly yellowish), ovate to lanceolate, 1–4 cm × 5–15 mm, (smaller distally), base amplexicaul, margins often entire.</text>
      <biological_entity constraint="cauline" id="o39521" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" notes="" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="4" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39522" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o39523" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="amplexicaul" value_original="amplexicaul" />
      </biological_entity>
      <biological_entity id="o39524" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes ebracteate, (secund, rachis flexuous).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate-ascending, (straight), 1–2 mm.</text>
      <biological_entity id="o39525" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39526" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o39525" id="r2671" name="fruiting" negation="false" src="d0_s9" to="o39526" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx urceolate;</text>
      <biological_entity id="o39527" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o39528" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="urceolate" value_original="urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals yellow-green, (ovatelanceolate), 5–8 mm, keeled, (apex recurved or flaring);</text>
      <biological_entity id="o39529" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o39530" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals whitish (with purplish veins), 6–8 mm, blade 2–3 × 1–1.3 mm, margins not crisped, claw 4–5 mm, about as wide as blade;</text>
      <biological_entity id="o39531" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o39532" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="whitish" value_original="whitish" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39533" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39534" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o39535" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o39536" name="blade" name_original="blade" src="d0_s12" type="structure" />
      <relation from="o39535" id="r2672" name="as wide as" negation="false" src="d0_s12" to="o39536" />
    </statement>
    <statement id="d0_s13">
      <text>stamens in 3 unequal pairs;</text>
      <biological_entity id="o39537" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o39538" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o39539" name="pair" name_original="pairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="3" value_original="3" />
        <character is_modifier="true" name="size" src="d0_s13" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o39538" id="r2673" name="in" negation="false" src="d0_s13" to="o39539" />
    </statement>
    <statement id="d0_s14">
      <text>filaments: abaxial pair (connate 1/2 their length), 3–5 mm, lateral pair 2–3.5 mm, adaxial pair (connate entire length, recurved), 6–9 mm;</text>
      <biological_entity id="o39540" name="filament" name_original="filaments" src="d0_s14" type="structure" />
      <biological_entity constraint="abaxial" id="o39541" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o39542" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers: abaxial and lateral pairs fertile, 1.7–2.5 mm, adaxial pair sterile, 0.7–1.2 mm;</text>
      <biological_entity id="o39543" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="abaxial" value_original="abaxial" />
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o39544" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s15" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>gynophore 0.2–0.5 mm.</text>
      <biological_entity id="o39545" name="anther" name_original="anthers" src="d0_s16" type="structure" />
      <biological_entity id="o39546" name="gynophore" name_original="gynophore" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits divaricate-ascending, somewhat torulose, nearly straight to, rarely, arcuate, flattened, 3–6 cm × 0.9–1.1 mm;</text>
      <biological_entity id="o39547" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s17" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="rarely" name="course_or_shape" src="d0_s17" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s17" to="6" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s17" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each with obscure midvein;</text>
      <biological_entity id="o39548" name="valve" name_original="valves" src="d0_s18" type="structure" />
      <biological_entity id="o39549" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="obscure" value_original="obscure" />
      </biological_entity>
      <relation from="o39548" id="r2674" name="with" negation="false" src="d0_s18" to="o39549" />
    </statement>
    <statement id="d0_s19">
      <text>replum straight;</text>
      <biological_entity id="o39550" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 26–38 per ovary;</text>
      <biological_entity id="o39551" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o39552" from="26" name="quantity" src="d0_s20" to="38" />
      </biological_entity>
      <biological_entity id="o39552" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 0.1–0.3 mm;</text>
      <biological_entity id="o39553" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s21" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma entire.</text>
      <biological_entity id="o39554" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds oblong, 1.2–1.5 × 0.6–0.8 mm;</text>
      <biological_entity id="o39555" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s23" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s23" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>wing (0–) 0.05–0.1 mm wide, distal.</text>
      <biological_entity id="o39556" name="wing" name_original="wing" src="d0_s24" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s24" to="0.05" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s24" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>2n = 28.</text>
      <biological_entity constraint="distal" id="o39557" name="wing" name_original="wing" src="d0_s24" type="structure" />
      <biological_entity constraint="2n" id="o39558" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine barrens and associated openings in chaparral-oak woodland and cypress woodland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine barrens" />
        <character name="habitat" value="associated openings" constraint="in chaparral-oak woodland and cypress" />
        <character name="habitat" value="chaparral-oak woodland" />
        <character name="habitat" value="cypress" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Streptanthus hesperidis is known from Lake and Napa counties. R. E. Buck et al. (1993) reduced it to a variety of S. breweri, but the two are sufficiently distinct to be recognized as independent species. It differs from S. breweri by having flexuous (versus straight) raceme rachises, yellowish (versus glaucous green) foliage, smaller petals (6–8 versus 8–12 mm), and often straight (versus often arcuate) fruits.</discussion>
  
</bio:treatment>