<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="mention_page">358</other_info_on_meta>
    <other_info_on_meta type="treatment_page">406</other_info_on_meta>
    <other_info_on_meta type="illustration_page">405</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="Windham &amp; Al-Shehbaz" date="2006" rank="species">serpenticola</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>11: 82. 2006</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species serpenticola</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094531</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>(cespitose);</text>
    </statement>
    <statement id="d0_s3">
      <text>sexual;</text>
      <biological_entity id="o17534" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="sexual" value_original="sexual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>caudex woody.</text>
      <biological_entity id="o17535" name="caudex" name_original="caudex" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Stems usually 1 per caudex branch, arising from center of rosette near ground surface, 0.5–1.8 dm, densely pubescent proximally, trichomes short-stalked, 5–8-rayed, 0.1–0.2, sparsely to moderately pubescent distally.</text>
      <biological_entity id="o17536" name="stem" name_original="stems" src="d0_s5" type="structure">
        <character constraint="per caudex branch" constraintid="o17537" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character constraint="from center" constraintid="o17538" is_modifier="false" name="orientation" notes="" src="d0_s5" value="arising" value_original="arising" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" notes="" src="d0_s5" to="1.8" to_unit="dm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o17537" name="branch" name_original="branch" src="d0_s5" type="structure" />
      <biological_entity id="o17538" name="center" name_original="center" src="d0_s5" type="structure" />
      <biological_entity id="o17539" name="rosette" name_original="rosette" src="d0_s5" type="structure" />
      <biological_entity id="o17540" name="ground" name_original="ground" src="d0_s5" type="structure" />
      <biological_entity id="o17541" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity id="o17542" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" name="quantity" src="d0_s5" to="0.2" />
        <character is_modifier="false" modifier="sparsely to moderately; distally" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o17538" id="r1195" name="part_of" negation="false" src="d0_s5" to="o17539" />
      <relation from="o17538" id="r1196" name="near" negation="false" src="d0_s5" to="o17540" />
      <relation from="o17538" id="r1197" name="near" negation="false" src="d0_s5" to="o17541" />
    </statement>
    <statement id="d0_s6">
      <text>Basal leaves: blade narrowly oblanceolate, 1–2 mm wide, margins entire, ciliate along petiole, trichomes (branched), to 0.5 mm, surfaces densely pubescent, trichomes short-stalked, 5–8-rayed, 0.1–0.2 mm.</text>
      <biological_entity constraint="basal" id="o17543" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o17544" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17545" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character constraint="along petiole" constraintid="o17546" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o17546" name="petiole" name_original="petiole" src="d0_s6" type="structure" />
      <biological_entity id="o17547" name="trichome" name_original="trichomes" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17548" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o17549" name="trichome" name_original="trichomes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s6" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves: 4–14, rarely concealing stem proximally;</text>
      <biological_entity constraint="cauline" id="o17550" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="14" />
      </biological_entity>
      <biological_entity id="o17551" name="stem" name_original="stem" src="d0_s7" type="structure" />
      <relation from="o17550" id="r1198" modifier="proximally" name="rarely concealing" negation="false" src="d0_s7" to="o17551" />
    </statement>
    <statement id="d0_s8">
      <text>blade auricles 0.1–0.5 mm, surfaces of distalmost leaves densely pubescent.</text>
      <biological_entity constraint="cauline" id="o17552" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="blade" id="o17553" name="auricle" name_original="auricles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17554" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o17555" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <relation from="o17554" id="r1199" name="part_of" negation="false" src="d0_s8" to="o17555" />
    </statement>
    <statement id="d0_s9">
      <text>Racemes 10–20-flowered, unbranched.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels divaricate-descending or horizontal, nearly straight, 3–7 mm, pubescent, trichomes appressed, branched.</text>
      <biological_entity id="o17556" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="10-20-flowered" value_original="10-20-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o17557" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <biological_entity id="o17558" name="whole-organism" name_original="" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="divaricate-descending" value_original="divaricate-descending" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" modifier="nearly" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o17559" name="trichome" name_original="trichomes" src="d0_s10" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o17556" id="r1200" name="fruiting" negation="false" src="d0_s10" to="o17557" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers ascending at anthesis;</text>
      <biological_entity id="o17560" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals pubescent;</text>
      <biological_entity id="o17561" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals purple, 6–8 × 1.5–2.5 mm, glabrous;</text>
      <biological_entity id="o17562" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s13" value="purple" value_original="purple" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s13" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s13" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pollen ellipsoid.</text>
      <biological_entity id="o17563" name="pollen" name_original="pollen" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits usually divaricate-descending or horizontal, rarely widely pendent, not appressed to rachis, not secund, straight or slightly curved, edges parallel, 3–4.5 cm × 2.5–3 mm;</text>
      <biological_entity id="o17564" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s15" value="divaricate-descending" value_original="divaricate-descending" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" modifier="rarely widely" name="orientation" src="d0_s15" value="pendent" value_original="pendent" />
        <character constraint="to rachis" constraintid="o17565" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s15" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s15" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s15" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o17565" name="rachis" name_original="rachis" src="d0_s15" type="structure" />
      <biological_entity id="o17566" name="edge" name_original="edges" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s15" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>valves sparsely pubescent to glabrate;</text>
      <biological_entity id="o17567" name="valve" name_original="valves" src="d0_s16" type="structure">
        <character char_type="range_value" from="sparsely pubescent" name="pubescence" src="d0_s16" to="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 20–24 per ovary;</text>
      <biological_entity id="o17568" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o17569" from="20" name="quantity" src="d0_s17" to="24" />
      </biological_entity>
      <biological_entity id="o17569" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 1.5–2 mm.</text>
      <biological_entity id="o17570" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s18" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds uniseriate, 2–2.5 × 1.5–1.8 mm;</text>
      <biological_entity id="o17571" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s19" value="uniseriate" value_original="uniseriate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s19" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s19" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>wing continuous, to 0.2 mm wide.</text>
      <biological_entity id="o17572" name="wing" name_original="wing" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="continuous" value_original="continuous" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s20" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Serpentine ridges and talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="serpentine ridges" />
        <character name="habitat" value="talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 1100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1100" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>94.</number>
  <discussion>Boechera serpenticola has been confused with B. subpinnatifida but is easily distinguished by its entire (versus subpinnatifid to dentate) leaves, shorter (6–8 versus 9–14 mm) petals, and longer (1.5–2 versus 0.5–1 mm) styles. It is known only from Shasta and Trinity counties in north-central California.</discussion>
  
</bio:treatment>