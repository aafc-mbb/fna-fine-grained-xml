<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">19</other_info_on_meta>
    <other_info_on_meta type="treatment_page">18</other_info_on_meta>
    <other_info_on_meta type="illustration_page">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">populus</taxon_name>
    <taxon_name authority="W. Bartram ex Marshall" date="unknown" rank="species">deltoides</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">deltoides</taxon_name>
    <taxon_hierarchy>family salicaceae;genus populus;species deltoides;subspecies deltoides</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095166</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Populus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">deltoides</taxon_name>
    <taxon_name authority="(A. Henry) A. Henry" date="unknown" rank="variety">missouriensis</taxon_name>
    <taxon_hierarchy>genus Populus;species deltoides;variety missouriensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants to 55 m.</text>
      <biological_entity id="o18497" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="55" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Winter buds usually glabrous.</text>
      <biological_entity id="o18498" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="true" name="season" src="d0_s1" value="winter" value_original="winter" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: blade base usually with 3–6 tubular basilaminar glands, apex short-acuminate, abaxial surface pilose at emergence;</text>
      <biological_entity id="o18499" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="blade" id="o18500" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="basilaminar" id="o18501" name="gland" name_original="glands" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s2" to="6" />
        <character is_modifier="true" name="shape" src="d0_s2" value="tubular" value_original="tubular" />
      </biological_entity>
      <biological_entity id="o18502" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="short-acuminate" value_original="short-acuminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o18503" name="surface" name_original="surface" src="d0_s2" type="structure">
        <character constraint="at emergence" constraintid="o18504" is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o18504" name="emergence" name_original="emergence" src="d0_s2" type="structure" />
      <relation from="o18500" id="r1267" name="with" negation="false" src="d0_s2" to="o18501" />
    </statement>
    <statement id="d0_s3">
      <text>preformed blade with (6–) 12–20 (–30) teeth on each side;</text>
      <biological_entity id="o18505" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o18506" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o18507" name="tooth" name_original="teeth" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="atypical_quantity" src="d0_s3" to="12" to_inclusive="false" />
        <character char_type="range_value" from="20" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s3" to="30" />
        <character char_type="range_value" from="12" is_modifier="true" name="quantity" src="d0_s3" to="20" />
      </biological_entity>
      <biological_entity id="o18508" name="side" name_original="side" src="d0_s3" type="structure" />
      <relation from="o18505" id="r1268" name="preformed" negation="false" src="d0_s3" to="o18506" />
      <relation from="o18505" id="r1269" name="with" negation="false" src="d0_s3" to="o18507" />
      <relation from="o18507" id="r1270" name="on" negation="false" src="d0_s3" to="o18508" />
    </statement>
    <statement id="d0_s4">
      <text>neoformed blade lengths usually distinctly greater than widths.</text>
      <biological_entity id="o18509" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o18510" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="neoformed" value_original="neoformed" />
        <character is_modifier="false" name="lengths" src="d0_s4" value="usually distinctly greater than widths" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pedicels: lengths graded, shorter from base to apex, 1–13 (–17 in fruit) mm.</text>
      <biological_entity id="o18511" name="pedicel" name_original="pedicels" src="d0_s5" type="structure">
        <character is_modifier="false" name="lengths" src="d0_s5" value="graded" value_original="graded" />
        <character constraint="from base" constraintid="o18512" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" notes="" src="d0_s5" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18512" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o18513" name="apex" name_original="apex" src="d0_s5" type="structure" />
      <relation from="o18512" id="r1271" name="to" negation="false" src="d0_s5" to="o18513" />
    </statement>
    <statement id="d0_s6">
      <text>Capsules with thin, flexible valves.</text>
      <biological_entity id="o18514" name="capsule" name_original="capsules" src="d0_s6" type="structure" />
      <biological_entity id="o18515" name="valve" name_original="valves" src="d0_s6" type="structure">
        <character is_modifier="true" name="width" src="d0_s6" value="thin" value_original="thin" />
        <character is_modifier="true" name="fragility" src="d0_s6" value="pliable" value_original="flexible" />
      </biological_entity>
      <relation from="o18514" id="r1272" name="with" negation="false" src="d0_s6" to="o18515" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Apr (fruiting Apr–Jul).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="fruiting" to="Apr" from="Mar" />
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
        <character name="fruiting time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Floodplains, low wet areas, secondary woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="floodplains" />
        <character name="habitat" value="low wet areas" />
        <character name="habitat" value="secondary woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Ky., La., Md., Mass., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5a.</number>
  <other_name type="common_name">Southern cottonwood</other_name>
  <discussion>Southern cottonwoods are nearly ubiquitous, although varying greatly in abundance, throughout the southeastern United States. They are among the fastest growing and largest trees in the region and are the basis for local poplar plantation forestry rather than hybrids used in other regions. Subspecies deltoides intergrades rather freely with subsp. monilifera up the Mississippi River drainage system, and traces of its morphological influence may be found as far north as Minnesota and Wisconsin (E. Marcet 1962).</discussion>
  
</bio:treatment>