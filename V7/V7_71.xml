<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">63</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="mention_page">90</other_info_on_meta>
    <other_info_on_meta type="mention_page">91</other_info_on_meta>
    <other_info_on_meta type="mention_page">92</other_info_on_meta>
    <other_info_on_meta type="mention_page">152</other_info_on_meta>
    <other_info_on_meta type="treatment_page">73</other_info_on_meta>
    <other_info_on_meta type="illustration_page">72</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="A. Kerner" date="1860" rank="section">myrtosalix</taxon_name>
    <taxon_name authority="Cockerell ex A. Heller" date="1910" rank="species">arctophila</taxon_name>
    <place_of_publication>
      <publication_title>Cat. N. Amer. Pl. ed.</publication_title>
      <place_in_publication>3, 89. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section myrtosalix;species arctophila</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445639</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Lundström" date="unknown" rank="species">arctophila</taxon_name>
    <taxon_name authority="(Lange) C. K. Schneider" date="unknown" rank="variety">lejocarpa</taxon_name>
    <taxon_hierarchy>genus Salix;species arctophila;variety lejocarpa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">groenlandica</taxon_name>
    <taxon_name authority="Lange" date="unknown" rank="variety">lejocarpa</taxon_name>
    <taxon_hierarchy>genus Salix;species groenlandica;variety lejocarpa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.03–0.15 m, (dwarf), forming clones by layering.</text>
      <biological_entity id="o27553" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.03" from_unit="m" name="some_measurement" src="d0_s0" to="0.15" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems prostrate, long-trailing;</text>
      <biological_entity id="o27554" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="long-trailing" value_original="long-trailing" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches yellowbrown, redbrown, or green-brown, glabrous;</text>
      <biological_entity id="o27555" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green-brown" value_original="green-brown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="green-brown" value_original="green-brown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets yellow-green or yellowbrown to redbrown, (sometimes weakly glaucous), glabrous, (inner membranaceous bud-scale layer free, not separating from outer layer).</text>
      <biological_entity id="o27556" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character char_type="range_value" from="yellowbrown" name="coloration" src="d0_s3" to="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules rudimentary, absent, or foliaceous on early ones, foliaceous or rudimentary on late ones;</text>
      <biological_entity id="o27557" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o27558" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character constraint="on early ones" is_modifier="false" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="foliaceous" value_original="foliaceous" />
        <character constraint="on ones" constraintid="o27559" is_modifier="false" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity id="o27559" name="one" name_original="ones" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petiole 3–7.8–15 mm;</text>
      <biological_entity id="o27560" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o27561" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="7.8-15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>largest medial blade hypostomatous or hemiamphistomatous, elliptic, obovate, broadly elliptic, broadly obovate, subcircular, or oblanceolate, 15–31–60 × 6.5–16–35 mm, 1.2–3–4.3 times as long as wide, base cuneate, convex, or rounded, margins slightly revolute, inconspicuously crenulate or entire, apex usually acute or convex, sometimes rounded, abaxial surface glaucous, glabrous, adaxial slightly or highly glossy, glabrous;</text>
      <biological_entity id="o27562" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="largest medial" id="o27563" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="hypostomatous" value_original="hypostomatous" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="hemiamphistomatous" value_original="hemiamphistomatous" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcircular" value_original="subcircular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcircular" value_original="subcircular" />
        <character is_modifier="false" name="shape" src="d0_s6" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s6" to="31-60" to_unit="mm" />
        <character char_type="range_value" from="6.5" from_unit="mm" name="width" src="d0_s6" to="16-35" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1.2-3-4.3" value_original="1.2-3-4.3" />
      </biological_entity>
      <biological_entity id="o27564" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o27565" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" modifier="inconspicuously" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o27566" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o27567" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27568" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="highly" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal blade margins entire or serrulate;</text>
      <biological_entity id="o27569" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o27570" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>juvenile blade glabrous.</text>
      <biological_entity id="o27571" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o27572" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Catkins: staminate 19–54 × 7–16 mm, flowering branchlet 4–20 mm;</text>
      <biological_entity id="o27573" name="catkin" name_original="catkins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="19" from_unit="mm" name="length" src="d0_s9" to="54" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s9" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27574" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate densely to moderately densely flowered, slender to subglobose, 30–79 (–130 in fruit) × 10–20 mm, flowering branchlet 8–57 mm;</text>
      <biological_entity id="o27575" name="catkin" name_original="catkins" src="d0_s10" type="structure">
        <character char_type="range_value" from="pistillate" name="architecture" src="d0_s10" to="densely moderately densely flowered" />
        <character is_modifier="false" name="size" src="d0_s10" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="30" from_unit="mm" name="length" src="d0_s10" to="79" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s10" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27576" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="57" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract brown, black, or bicolor, 0.8–2.4 mm, apex rounded or acute, entire, abaxially densely hairy, hairs straight.</text>
      <biological_entity id="o27577" name="catkin" name_original="catkins" src="d0_s11" type="structure" />
      <biological_entity constraint="floral" id="o27578" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="bicolor" value_original="bicolor" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="bicolor" value_original="bicolor" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27579" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially densely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o27580" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: abaxial nectary absent, adaxial nectary oblong, square, narrowly oblong, or ovate, 0.4–1 mm;</text>
      <biological_entity id="o27581" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o27582" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27583" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="square" value_original="square" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct or connate less than 1/2 their lengths, glabrous, or hairy on proximal 1/2;</text>
      <biological_entity id="o27584" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o27585" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s13" to="1/2" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character constraint="on proximal 1/2" constraintid="o27586" is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o27586" name="1/2" name_original="1/2" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>anthers ellipsoid or long-cylindrical, 0.5–0.7 mm.</text>
      <biological_entity id="o27587" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o27588" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s14" value="long-cylindrical" value_original="long-cylindrical" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: abaxial nectary absent, adaxial nectary oblong or narrowly oblong, 0.5–0.9 mm, shorter than stipe;</text>
      <biological_entity id="o27589" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o27590" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27591" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="0.9" to_unit="mm" />
        <character constraint="than stipe" constraintid="o27592" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o27592" name="stipe" name_original="stipe" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>stipe 0.8–1.4 mm;</text>
      <biological_entity id="o27593" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27594" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s16" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary pyriform or obclavate, pubescent or short-silky, (refractive), hairs (white, grayish, or ferruginous), crinkled, often refractive, ribbonlike, beak gradually tapering to styles;</text>
      <biological_entity id="o27595" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27596" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obclavate" value_original="obclavate" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="short-silky" value_original="short-silky" />
      </biological_entity>
      <biological_entity id="o27597" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="crinkled" value_original="crinkled" />
        <character is_modifier="false" modifier="often" name="reflectance" src="d0_s17" value="refractive" value_original="refractive" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="ribbonlike" value_original="ribbonlike" />
      </biological_entity>
      <biological_entity id="o27598" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character constraint="to styles" constraintid="o27599" is_modifier="false" modifier="gradually" name="shape" src="d0_s17" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o27599" name="style" name_original="styles" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 8–16 per ovary;</text>
      <biological_entity id="o27600" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27601" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o27602" from="8" name="quantity" src="d0_s18" to="16" />
      </biological_entity>
      <biological_entity id="o27602" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles connate or distinct 1/2 their lengths, 0.6–1.4 mm;</text>
      <biological_entity id="o27603" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27604" name="style" name_original="styles" src="d0_s19" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s19" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s19" value="distinct" value_original="distinct" />
        <character name="lengths" src="d0_s19" value="1/2" value_original="1/2" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s19" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas slenderly or broadly cylindrical, 0.24–0.47–0.72 mm.</text>
      <biological_entity id="o27605" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o27606" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s20" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.24" from_unit="mm" name="some_measurement" src="d0_s20" to="0.47-0.72" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 5–9 mm. 2n = 76.</text>
      <biological_entity id="o27607" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s21" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27608" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="76" value_original="76" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late May-late Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late Jul" from="late May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arctic-alpine, subarctic, hummocks in wet, mossy, grass or sedge meadows, margins of streams or ponds, among granite boulders, on alluvial plains, sometimes in snowbeds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="arctic-alpine" />
        <character name="habitat" value="wet" modifier="subarctic hummocks in" />
        <character name="habitat" value="mossy" />
        <character name="habitat" value="grass" />
        <character name="habitat" value="sedge meadows" />
        <character name="habitat" value="margins" constraint="of streams or ponds" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="granite boulders" modifier="among" />
        <character name="habitat" value="alluvial plains" modifier="on" />
        <character name="habitat" value="snowbeds" modifier="sometimes in" />
        <character name="habitat" value="hummocks" modifier="sometimes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>40-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="40" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Man., Nfld. and Labr., N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska, Maine.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>34.</number>
  <other_name type="common_name">Northern willow</other_name>
  <discussion>Salix arctophila occurs in western Greenland.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Salix arctophila forms natural hybrids with S. arctica, S. glauca var. cordifolia, and S. uva-ursi.</discussion>
  <discussion>Salix arctophila × S. glauca var. cordifolia: Plants with villous leaves and moderately densely hairy branchlets and branches suggest this hybrid. Putative hybrids are rare but have been seen from Kuujjuaq and Ivujivik, Quebec (G. W. Argus, unpubl.), and are reported to be common in West Greenland (T. W. Böcher 1952).</discussion>
  <discussion>Salix arctophila × S. uva-ursi is a rare hybrid. The plants often have ovaries with patches of hairs, some of which are ribbonlike, as in S. arctophila, but their habit is compact, as in S. uva-ursi, rather than long-trailing as in S. arctophila. Some specimens are infertile and are evidently hybrids, but there is little to confirm S. uva-ursi as the second parent. N. Polunin (1940b) also expressed some uncertainty about plants intermediate between S. arctophila and S. uva-ursi, and A. K. Skvortsov (1971) discounted this hybrid but noted that there were a few somewhat doubtful specimens.</discussion>
  
</bio:treatment>