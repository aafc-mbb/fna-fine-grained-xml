<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">464</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="treatment_page">478</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cardamine</taxon_name>
    <taxon_name authority="Gandoger" date="unknown" rank="species">nymanii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Soc. Bot. France</publication_title>
      <place_in_publication>72: 1043. 1925</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus cardamine;species nymanii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094591</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">pratensis</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="variety">angustifolia</taxon_name>
    <taxon_hierarchy>genus Cardamine;species pratensis;variety angustifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(cespitose);</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous.</text>
      <biological_entity id="o10824" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Rhizomes absent.</text>
      <biological_entity id="o10825" name="rhizome" name_original="rhizomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems erect, unbranched or, rarely, branched, 0.5–1.6 (–3.5) dm.</text>
      <biological_entity id="o10826" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character name="architecture" src="d0_s4" value="," value_original="," />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="3.5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s4" to="1.6" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves (7 or) 9–21-foliolate, (thick, veins impressed);</text>
      <biological_entity constraint="basal" id="o10827" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="9-21-foliolate" value_original="9-21-foliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leaflets petiolulate or sessile;</text>
      <biological_entity id="o10828" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lateral lobes or leaflets similar to terminal;</text>
      <biological_entity constraint="lateral" id="o10829" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o10830" name="leaflet" name_original="leaflets" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>terminal lobe or leaflet blade orbicular, broadly ovate to lanceolate, base rounded to cuneate, margins usually entire.</text>
      <biological_entity constraint="terminal" id="o10831" name="lobe" name_original="lobe" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="orbicular" value_original="orbicular" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s8" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="leaflet" id="o10832" name="blade" name_original="blade" src="d0_s8" type="structure" constraint_original="terminal leaflet">
        <character is_modifier="false" name="shape" src="d0_s8" value="orbicular" value_original="orbicular" />
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s8" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o10833" name="base" name_original="base" src="d0_s8" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s8" to="cuneate" />
      </biological_entity>
      <biological_entity id="o10834" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 2–4 (–7), pinnatisect or pinnately compound, (7 or) 9–21-foliolate, (thick, veins impressed), petiolate, leaflets petiolulate or sessile;</text>
      <biological_entity constraint="cauline" id="o10835" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s9" to="7" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s9" to="4" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pinnatisect" value_original="pinnatisect" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s9" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="9-21-foliolate" value_original="9-21-foliolate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o10836" name="leaflet" name_original="leaflets" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petiole base not auriculate;</text>
      <biological_entity constraint="petiole" id="o10837" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lobes or leaflets (of proximal leaves) (4–) 7–10 each side of rachis, fewer distally, distal leaves with 4 or 5 lobes or leaflets each side of rachis;</text>
      <biological_entity id="o10838" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <biological_entity id="o10839" name="leaflet" name_original="leaflets" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s11" to="7" to_inclusive="false" />
        <character char_type="range_value" from="7" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
      <biological_entity id="o10840" name="side" name_original="side" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="distally" name="quantity" src="d0_s11" value="fewer" value_original="fewer" />
      </biological_entity>
      <biological_entity id="o10841" name="rachis" name_original="rachis" src="d0_s11" type="structure" />
      <biological_entity constraint="distal" id="o10842" name="leaf" name_original="leaves" src="d0_s11" type="structure" />
      <biological_entity id="o10843" name="leaflet" name_original="leaflets" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o10844" name="side" name_original="side" src="d0_s11" type="structure" />
      <biological_entity id="o10845" name="rachis" name_original="rachis" src="d0_s11" type="structure" />
      <relation from="o10840" id="r760" name="part_of" negation="false" src="d0_s11" to="o10841" />
      <relation from="o10842" id="r761" name="with" negation="false" src="d0_s11" to="o10843" />
      <relation from="o10844" id="r762" name="part_of" negation="false" src="d0_s11" to="o10845" />
    </statement>
    <statement id="d0_s12">
      <text>terminal leaflet petiolulate or sessile, blade (or lobe) narrowly lanceolate to lanceolate, base cuneate, margins entire.</text>
      <biological_entity constraint="terminal" id="o10846" name="leaflet" name_original="leaflet" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o10847" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s12" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o10848" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o10849" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Racemes ebracteate.</text>
    </statement>
    <statement id="d0_s14">
      <text>Fruiting pedicels erect-ascending, 5–15 mm.</text>
      <biological_entity id="o10850" name="raceme" name_original="racemes" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect-ascending" value_original="erect-ascending" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10851" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <relation from="o10850" id="r763" name="fruiting" negation="false" src="d0_s14" to="o10851" />
    </statement>
    <statement id="d0_s15">
      <text>Flowers: sepals oblong or ovate, 3.6–4.4 mm, lateral pair saccate basally, (green with hyaline margins);</text>
      <biological_entity id="o10852" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o10853" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3.6" from_unit="mm" name="some_measurement" src="d0_s15" to="4.4" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s15" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals white-lilac, 9–12.3 × 4.8–6.8 mm, (clawed, apex rounded or emarginate);</text>
      <biological_entity id="o10854" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o10855" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white-lilac" value_original="white-lilac" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s16" to="12.3" to_unit="mm" />
        <character char_type="range_value" from="4.8" from_unit="mm" name="width" src="d0_s16" to="6.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>filaments: median pairs 3.5–4.5 mm, lateral pair 2–3 mm;</text>
      <biological_entity id="o10856" name="filament" name_original="filaments" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="median" value_original="median" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s17" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s17" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers narrowly oblong, 0.9–1.4 mm.</text>
      <biological_entity id="o10857" name="filament" name_original="filaments" src="d0_s18" type="structure" />
      <biological_entity id="o10858" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s18" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s18" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Fruits linear, 1–1.8 cm × ca. 1.5 mm;</text>
      <biological_entity id="o10859" name="fruit" name_original="fruits" src="d0_s19" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s19" value="linear" value_original="linear" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s19" to="1.8" to_unit="cm" />
        <character name="width" src="d0_s19" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules ca. 16 per ovary;</text>
      <biological_entity id="o10860" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character constraint="per ovary" constraintid="o10861" name="quantity" src="d0_s20" value="16" value_original="16" />
      </biological_entity>
      <biological_entity id="o10861" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style ca. 1 mm, (stout).</text>
      <biological_entity id="o10862" name="style" name_original="style" src="d0_s21" type="structure">
        <character name="some_measurement" src="d0_s21" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds brown, oblong, ca. 1.5 mm. 2n = 56, 60, 64, 80–100.</text>
      <biological_entity id="o10863" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s22" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
        <character name="some_measurement" src="d0_s22" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10864" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="56" value_original="56" />
        <character name="quantity" src="d0_s22" value="60" value_original="60" />
        <character name="quantity" src="d0_s22" value="64" value_original="64" />
        <character char_type="range_value" from="80" name="quantity" src="d0_s22" to="100" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet meadows, marshes, margins of ponds, along streams, seacoasts, swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet meadows" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="margins" constraint="of ponds , along streams , seacoasts , swamps" />
        <character name="habitat" value="ponds" constraint="along streams , seacoasts , swamps" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="seacoasts" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Man., Nfld. and Labr., N.W.T., Nunavut, Que., Yukon; Alaska; n Eurasia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="n Eurasia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>27.</number>
  <other_name type="past_name">nymani</other_name>
  
</bio:treatment>