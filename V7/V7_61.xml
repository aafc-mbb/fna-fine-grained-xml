<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">29</other_info_on_meta>
    <other_info_on_meta type="mention_page">69</other_info_on_meta>
    <other_info_on_meta type="treatment_page">67</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="Seringe" date="1824" rank="section">Herbella</taxon_name>
    <place_of_publication>
      <publication_title>Exempl. Rév. Salix,</publication_title>
      <place_in_publication>14. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section Herbella</taxon_hierarchy>
    <other_info_on_name type="fna_id">317504</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="A. Kerner" date="unknown" rank="section">Retusae</taxon_name>
    <taxon_hierarchy>genus Salix;section Retusae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.005–0.09 m, clonal by layering or rhizomes.</text>
      <biological_entity id="o39275" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.005" from_unit="m" name="some_measurement" src="d0_s0" to="0.09" to_unit="m" />
        <character constraint="by rhizomes" constraintid="o39276" is_modifier="false" name="growth_form" src="d0_s0" value="clonal" value_original="clonal" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o39276" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s0" value="layering" value_original="layering" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Largest medial blades amphistomatous, abaxial surface not glaucous.</text>
      <biological_entity constraint="largest medial" id="o39277" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="amphistomatous" value_original="amphistomatous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o39278" name="surface" name_original="surface" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Catkins from subterminal or lateral buds.</text>
      <biological_entity id="o39279" name="catkin" name_original="catkins" src="d0_s2" type="structure" />
      <biological_entity id="o39280" name="bud" name_original="buds" src="d0_s2" type="structure" />
      <relation from="o39279" id="r2655" name="from" negation="false" src="d0_s2" to="o39280" />
    </statement>
    <statement id="d0_s3">
      <text>Staminate flowers: filaments glabrous or hairy.</text>
      <biological_entity id="o39281" name="flower" name_original="flowers" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o39282" name="filament" name_original="filaments" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pistillate flowers: abaxial nectary sometimes present, then distinct or connate to adaxial and forming a cup;</text>
      <biological_entity id="o39283" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o39284" name="nectary" name_original="nectary" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="fusion" src="d0_s4" value="distinct" value_original="distinct" />
        <character constraint="to adaxial nectary" constraintid="o39285" is_modifier="false" name="fusion" src="d0_s4" value="connate" value_original="connate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o39285" name="nectary" name_original="nectary" src="d0_s4" type="structure" />
      <biological_entity id="o39286" name="cup" name_original="cup" src="d0_s4" type="structure" />
      <relation from="o39284" id="r2656" name="forming a" negation="false" src="d0_s4" to="o39286" />
    </statement>
    <statement id="d0_s5">
      <text>ovary not glaucous, glabrous, puberulent, or villous to pilose.</text>
      <biological_entity id="o39287" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o39288" name="ovary" name_original="ovary" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="villous" name="pubescence" src="d0_s5" to="pilose" />
        <character char_type="range_value" from="villous" name="pubescence" src="d0_s5" to="pilose" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Eurasia, Atlantic Islands.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2d3.</number>
  <discussion>Species 7 (4 in the flora).</discussion>
  
</bio:treatment>