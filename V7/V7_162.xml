<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">149</other_info_on_meta>
    <other_info_on_meta type="treatment_page">146</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="(Andersson) Rouy" date="1910" rank="section">Villosae</taxon_name>
    <place_of_publication>
      <publication_title>Fl. France</publication_title>
      <place_in_publication>12: 200. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section Villosae</taxon_hierarchy>
    <other_info_on_name type="fna_id">317519</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Andersson" date="unknown" rank="unranked">Villosae</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>16(2): 275. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Salix;unranked Villosae;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs or trees, 0.3–7 m.</text>
      <biological_entity id="o35379" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character name="growth_form" value="shrub" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Buds caprea-type.</text>
      <biological_entity id="o35381" name="bud" name_original="buds" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_ref_taxa" src="d0_s1" value="caprea-type" value_original="caprea-type" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules on late ones foliaceous;</text>
      <biological_entity id="o35382" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o35383" name="stipule" name_original="stipules" src="d0_s2" type="structure" />
      <biological_entity id="o35384" name="one" name_original="ones" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <relation from="o35383" id="r2371" name="on" negation="false" src="d0_s2" to="o35384" />
    </statement>
    <statement id="d0_s3">
      <text>largest medial blade 2–5 times as long as wide, adaxial surface not glaucous.</text>
      <biological_entity id="o35385" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="largest medial" id="o35386" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="2-5" value_original="2-5" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o35387" name="surface" name_original="surface" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Staminate flowers: filaments glabrous.</text>
      <biological_entity id="o35388" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o35389" name="filament" name_original="filaments" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pistillate flowers: adaxial nectary longer than stipe;</text>
      <biological_entity id="o35390" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o35391" name="nectary" name_original="nectary" src="d0_s5" type="structure">
        <character constraint="than stipe" constraintid="o35392" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o35392" name="stipe" name_original="stipe" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>stipe 0–0.6 mm;</text>
      <biological_entity id="o35393" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o35394" name="stipe" name_original="stipe" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ovary sparsely to very densely villous;</text>
      <biological_entity id="o35395" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o35396" name="ovary" name_original="ovary" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely to very densely" name="pubescence" src="d0_s7" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigmas slenderly cylindrical, 0.3–1.3 mm.</text>
      <biological_entity id="o35397" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o35398" name="stigma" name_original="stigmas" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slenderly" name="shape" src="d0_s8" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2e10.</number>
  <discussion>Species 6 (3 in the flora).</discussion>
  
</bio:treatment>