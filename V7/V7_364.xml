<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">270</other_info_on_meta>
    <other_info_on_meta type="mention_page">284</other_info_on_meta>
    <other_info_on_meta type="mention_page">290</other_info_on_meta>
    <other_info_on_meta type="treatment_page">289</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="Ekman" date="1936" rank="species">aleutica</taxon_name>
    <place_of_publication>
      <publication_title>Svensk Bot. Tidskr.</publication_title>
      <place_in_publication>30: 522, figs. 3c,d. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species aleutica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250094721</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Tolmatchew" date="unknown" rank="species">behringii</taxon_name>
    <taxon_hierarchy>genus Draba;species behringii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(densely matted);</text>
      <biological_entity id="o13154" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (with persistent leaf remains, some branches terminating in sterile rosettes);</text>
    </statement>
    <statement id="d0_s3">
      <text>scapose.</text>
      <biological_entity id="o13155" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, 0.04–0.4 dm, usually pubescent throughout, sometimes glabrous basally, trichomes simple, 0.1–1 mm, with 2-rayed ones.</text>
      <biological_entity id="o13156" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.04" from_unit="dm" name="some_measurement" src="d0_s4" to="0.4" to_unit="dm" />
        <character is_modifier="false" modifier="usually; throughout" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes; basally" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13157" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13158" name="one" name_original="ones" src="d0_s4" type="structure" />
      <relation from="o13157" id="r918" name="with" negation="false" src="d0_s4" to="o13158" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves (densely imbricate);</text>
    </statement>
    <statement id="d0_s6">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o13159" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petiole base and margin ciliate, (trichomes simple, to 1.4 mm);</text>
      <biological_entity constraint="petiole" id="o13160" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o13161" name="margin" name_original="margin" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade oblanceolate to spatulate or obovate, 0.4–0.8 (–1) cm × 2–3.5 (–4.5) mm, margins entire, surfaces usually sparsely to densely pilose with simple trichomes, to 1.4 mm, with much fewer, stalked, 2-rayed ones, (sometimes surfaces glabrous, or adaxially only, midvein obscure abaxially).</text>
      <biological_entity id="o13162" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s9" to="spatulate or obovate" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s9" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s9" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13163" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o13164" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character constraint="with trichomes" constraintid="o13165" is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13165" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Cauline leaves 0.</text>
      <biological_entity constraint="cauline" id="o13166" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Racemes (1 or) 2–5 (–9) -flowered, ebracteate, not or slightly elongated in fruit;</text>
      <biological_entity id="o13167" name="raceme" name_original="racemes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="2-5(-9)-flowered" value_original="2-5(-9)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o13168" is_modifier="false" modifier="not; slightly" name="length" src="d0_s11" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o13168" name="fruit" name_original="fruit" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>rachis not flexuous, pubescent as stem.</text>
      <biological_entity id="o13170" name="stem" name_original="stem" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Fruiting pedicels divaricate, straight, 1.5–4 (–6) mm, pubescent, trichomes simple with fewer, 2-rayed, stalked ones.</text>
      <biological_entity id="o13169" name="rachis" name_original="rachis" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s12" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o13170" is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13171" name="pedicel" name_original="pedicels" src="d0_s13" type="structure" />
      <biological_entity id="o13172" name="whole-organism" name_original="" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s13" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13173" name="trichome" name_original="trichomes" src="d0_s13" type="structure">
        <character constraint="with ones" constraintid="o13174" is_modifier="false" name="architecture" src="d0_s13" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o13174" name="one" name_original="ones" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="fewer" value_original="fewer" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="stalked" value_original="stalked" />
      </biological_entity>
      <relation from="o13169" id="r919" name="fruiting" negation="false" src="d0_s13" to="o13171" />
    </statement>
    <statement id="d0_s14">
      <text>Flowers: sepals oblong, 2–3 mm, glabrous or sparsely pubescent, (trichomes simple);</text>
      <biological_entity id="o13175" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o13176" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals yellowish green to pale-yellow, linear-oblanceolate, 3–4 × 0.5–0.8 mm;</text>
      <biological_entity id="o13177" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o13178" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character char_type="range_value" from="yellowish green" name="coloration" src="d0_s15" to="pale-yellow" />
        <character is_modifier="false" name="shape" src="d0_s15" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s15" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s15" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers oblong, 0.4–0.5 mm.</text>
      <biological_entity id="o13179" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o13180" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits broadly obovoid to subglobose, plane, slightly flattened, 3–5 × 3–4.5 mm;</text>
      <biological_entity id="o13181" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character char_type="range_value" from="broadly obovoid" name="shape" src="d0_s17" to="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s17" to="5" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s17" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves glabrous or pubescent, trichomes simple, 0.1–0.3 mm;</text>
      <biological_entity id="o13182" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o13183" name="trichome" name_original="trichomes" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s18" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 4–8 per ovary;</text>
      <biological_entity id="o13184" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o13185" from="4" name="quantity" src="d0_s19" to="8" />
      </biological_entity>
      <biological_entity id="o13185" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style 0.1–0.4 mm.</text>
      <biological_entity id="o13186" name="style" name_original="style" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s20" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds oblong, (slightly flattened), 1.4–1.8 × 0.9–1.1 mm.</text>
      <biological_entity id="o13187" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s21" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s21" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly soil, retreating snow banks, fellfields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly soil" />
        <character name="habitat" value="snow banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; e Asia (Russian Far East).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Draba aleutica is known in the flora area from the Aleutian Islands (e.g., Atka, Attu) and on the Pribilov Islands (St. Paul), as well as in the Alaskan Peninsula near Ugashik.</discussion>
  
</bio:treatment>