<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="treatment_page">175</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">limnanthaceae</taxon_name>
    <taxon_name authority="R. Brown" date="unknown" rank="genus">limnanthes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Limnanthes</taxon_name>
    <taxon_name authority="J. T. Howell" date="1943" rank="species">bakeri</taxon_name>
    <place_of_publication>
      <publication_title>Leafl. W. Bot.</publication_title>
      <place_in_publication>3: 206. 1943</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family limnanthaceae;genus limnanthes;section limnanthes;species bakeri;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250095072</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 10–40 cm.</text>
      <biological_entity id="o6544" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="40" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending.</text>
      <biological_entity id="o6545" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3–10 cm;</text>
      <biological_entity id="o6546" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 3–9, blade elliptic to ovate, margins usually entire (rarely 2-lobed or 3-lobed).</text>
      <biological_entity id="o6547" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="9" />
      </biological_entity>
      <biological_entity id="o6548" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s3" to="ovate" />
      </biological_entity>
      <biological_entity id="o6549" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers funnel to bell-shaped;</text>
      <biological_entity id="o6550" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="funnel" name="shape" src="d0_s4" to="bell-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals lanceolate, 5–7 mm;</text>
      <biological_entity id="o6551" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals pale-yellow with white-tips, cuneate, 7–9 mm, apex truncate, erose;</text>
      <biological_entity id="o6552" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character constraint="with white-tips" constraintid="o6553" is_modifier="false" name="coloration" src="d0_s6" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6553" name="white-tip" name_original="white-tips" src="d0_s6" type="structure" />
      <biological_entity id="o6554" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 2.5–4 mm;</text>
      <biological_entity id="o6555" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers cream, 0.5 mm;</text>
      <biological_entity id="o6556" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="cream" value_original="cream" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 2–3 mm.</text>
      <biological_entity id="o6557" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Nutlets dark-brown, 3–3.5 mm, tuberculate, tubercles light-brown or pinkish, rounded.</text>
      <biological_entity id="o6558" name="nutlet" name_original="nutlets" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s10" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 10.</text>
      <biological_entity id="o6559" name="tubercle" name_original="tubercles" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6560" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Vernal pools, marshy margins of pools</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="vernal pools" />
        <character name="habitat" value="marshy margins" constraint="of pools" />
        <character name="habitat" value="pools" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Baker’s meadowfoam</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Limnanthes bakeri is known from the Inner Coast Ranges of Mendocino County. It is easily recognized by having relatively few leaves with broad, mostly entire leaflets. The stamens and styles are about equal in length and are pressed together by a funnel-shaped corolla, facilitating self-pollination (R. V. Kesseli and S. K. Jain 1984b). Combined ITS, trnL, and morphological analyses placed L. bakeri in a basal position with respect to the Limnanthes (as sect. Reflexae) clade, and most closely allied with L. vinculans (M. S. Plotkin 1998). W. H. Parker and B. A. Bohm (1979) suggested that Floerkea proserpinacoides and L. bakeri separated from the family line long before the separation of other species.</discussion>
  
</bio:treatment>