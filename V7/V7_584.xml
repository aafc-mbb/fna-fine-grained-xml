<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">357</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">404</other_info_on_meta>
    <other_info_on_meta type="treatment_page">403</other_info_on_meta>
    <other_info_on_meta type="illustration_page">399</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="Windham &amp; Al-Shehbaz" date="2006" rank="species">rollinsiorum</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>11: 81. 2006</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species rollinsiorum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094536</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>sexual;</text>
      <biological_entity id="o12763" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="sexual" value_original="sexual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>caudex woody.</text>
      <biological_entity id="o12764" name="caudex" name_original="caudex" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems usually 3–5 per caudex branch, arising from margin of rosette, elevated above ground surface on woody base, 1–2.5 dm, densely pubescent proximally, trichomes short-stalked, 2–4 (or 5) -rayed, 0.2–0.4 mm, moderately to sparsely pubescent distally.</text>
      <biological_entity id="o12765" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="per caudex branch" constraintid="o12766" from="3" name="quantity" src="d0_s4" to="5" />
        <character constraint="from margin" constraintid="o12767" is_modifier="false" name="orientation" notes="" src="d0_s4" value="arising" value_original="arising" />
        <character constraint="above surface" constraintid="o12769" is_modifier="false" name="prominence" notes="" src="d0_s4" value="elevated" value_original="elevated" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="2.5" to_unit="dm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o12766" name="branch" name_original="branch" src="d0_s4" type="structure" />
      <biological_entity id="o12767" name="margin" name_original="margin" src="d0_s4" type="structure" />
      <biological_entity id="o12768" name="rosette" name_original="rosette" src="d0_s4" type="structure" />
      <biological_entity id="o12769" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o12770" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o12771" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
        <character is_modifier="false" modifier="moderately to sparsely; distally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o12767" id="r881" name="part_of" negation="false" src="d0_s4" to="o12768" />
      <relation from="o12769" id="r882" name="on" negation="false" src="d0_s4" to="o12770" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade oblanceolate, 3–5 mm wide, margins entire, ciliate proximally, trichomes (simple), to 0.6 mm, surfaces densely pubescent, trichomes short-stalked, 2–4 (or 5) -rayed, 0.15–0.4 mm.</text>
      <biological_entity constraint="basal" id="o12772" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o12773" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12774" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o12775" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12776" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o12777" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.15" from_unit="mm" name="some_measurement" src="d0_s5" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: 5–9, not concealing stem;</text>
      <biological_entity constraint="cauline" id="o12778" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="9" />
      </biological_entity>
      <biological_entity id="o12779" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o12778" id="r883" name="concealing" negation="true" src="d0_s6" to="o12779" />
    </statement>
    <statement id="d0_s7">
      <text>blade auricles 0.5–1 mm, surfaces of distalmost leaves sparsely to moderately pubescent.</text>
      <biological_entity constraint="cauline" id="o12780" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o12781" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12782" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o12783" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o12782" id="r884" name="part_of" negation="false" src="d0_s7" to="o12783" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes 5–11-flowered, usually unbranched.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels descending, straight to slightly recurved, 4–6 mm, pubescent, trichomes appressed, branched.</text>
      <biological_entity id="o12784" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-11-flowered" value_original="5-11-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o12785" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o12786" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="descending" value_original="descending" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o12787" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o12784" id="r885" name="fruiting" negation="false" src="d0_s9" to="o12785" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers divaricate-ascending at anthesis;</text>
      <biological_entity id="o12788" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s10" value="divaricate-ascending" value_original="divaricate-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals pubescent;</text>
      <biological_entity id="o12789" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals pale-purple, 6–8 × 1.5–2 mm, glabrous;</text>
      <biological_entity id="o12790" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale-purple" value_original="pale-purple" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s12" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollen ellipsoid.</text>
      <biological_entity id="o12791" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits (immature) pendent, not appressed to rachis, not secund, straight, edges parallel;</text>
      <biological_entity id="o12792" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="pendent" value_original="pendent" />
        <character constraint="to rachis" constraintid="o12793" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s14" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o12793" name="rachis" name_original="rachis" src="d0_s14" type="structure" />
      <biological_entity id="o12794" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="parallel" value_original="parallel" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves glabrous.</text>
      <biological_entity id="o12795" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds not seen.</text>
      <biological_entity id="o12796" name="seed" name_original="seeds" src="d0_s16" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Metamorphosed igneous gravel on steep slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="metamorphosed igneous" />
        <character name="habitat" value="steep slopes" modifier="gravel on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>89.</number>
  <discussion>Boechera rollinsiorum shows superficial similarities to both B. glareosa and B. lasiocarpa (see M. D. Windham and I. A. Al-Shehbaz 2006 for detailed comparison). It is known thus far only from the type collection below Galena Summit in central Idaho.</discussion>
  
</bio:treatment>