<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">703</other_info_on_meta>
    <other_info_on_meta type="mention_page">718</other_info_on_meta>
    <other_info_on_meta type="treatment_page">717</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">streptanthus</taxon_name>
    <taxon_name authority="G. L. Clifton &amp; R. E. Buck" date="unknown" rank="species">longisiliquus</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>54: 94, fig. 1. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus streptanthus;species longisiliquus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094990</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(short-lived, caudex simple or few-branched);</text>
    </statement>
    <statement id="d0_s2">
      <text>(glaucous), usually glabrous throughout, (except sepals pubescent, sometimes also petioles).</text>
      <biological_entity id="o26056" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually; throughout" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems branched, 2.2–12 (–15) dm.</text>
      <biological_entity id="o26057" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="15" to_unit="dm" />
        <character char_type="range_value" from="2.2" from_unit="dm" name="some_measurement" src="d0_s3" to="12" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves rosulate (in juvenile plants);</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate (petioles usually glabrous, rarely ciliate);</text>
      <biological_entity constraint="basal" id="o26058" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade obovate to spatulate, 3.5–10 cm, margins entire.</text>
      <biological_entity id="o26059" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s6" to="spatulate" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="some_measurement" src="d0_s6" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o26060" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves: blade broadly oblong to ovate or suborbicular, 2.5–10 cm × 10–35 mm, (smaller distally), base amplexicaul, margins entire.</text>
      <biological_entity constraint="cauline" id="o26061" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o26062" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="broadly oblong" name="shape" src="d0_s7" to="ovate or suborbicular" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s7" to="35" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26063" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="amplexicaul" value_original="amplexicaul" />
      </biological_entity>
      <biological_entity id="o26064" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes ebracteate, (lax).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels divaricate-ascending, (straight), 5–10 mm.</text>
      <biological_entity id="o26065" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26066" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o26065" id="r1767" name="fruiting" negation="false" src="d0_s9" to="o26066" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx subcampanulate;</text>
      <biological_entity id="o26067" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o26068" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subcampanulate" value_original="subcampanulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals yellow-greenish proximally, purple distally, oblong, 6–8 mm, not keeled, (with subapical tuft of hairs);</text>
      <biological_entity id="o26069" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o26070" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s11" value="yellow-greenish" value_original="yellow-greenish" />
        <character is_modifier="false" modifier="distally" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals purple or brownish (claw yellow-green), 8–12 mm, blade 1–3 × 0.5–0.8 mm, margins not crisped, claw 6–10 mm, wider than blade;</text>
      <biological_entity id="o26071" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o26072" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26073" name="blade" name_original="blade" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o26074" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o26075" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character constraint="than blade" constraintid="o26076" is_modifier="false" name="width" src="d0_s12" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o26076" name="blade" name_original="blade" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stamens in 3 unequal pairs;</text>
      <biological_entity id="o26077" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o26078" name="stamen" name_original="stamens" src="d0_s13" type="structure" />
      <biological_entity id="o26079" name="pair" name_original="pairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="3" value_original="3" />
        <character is_modifier="true" name="size" src="d0_s13" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o26078" id="r1768" name="in" negation="false" src="d0_s13" to="o26079" />
    </statement>
    <statement id="d0_s14">
      <text>filaments (distinct): abaxial pair 6–8 mm, lateral pair 4–6 mm, adaxial pair 7–10 mm;</text>
      <biological_entity id="o26080" name="filament" name_original="filaments" src="d0_s14" type="structure" />
      <biological_entity constraint="abaxial" id="o26081" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o26082" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers (all) fertile, 3.5–5 mm;</text>
      <biological_entity id="o26083" name="filament" name_original="filaments" src="d0_s15" type="structure" />
      <biological_entity id="o26084" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>gynophore 0.3–1 mm.</text>
      <biological_entity id="o26085" name="filament" name_original="filaments" src="d0_s16" type="structure" />
      <biological_entity id="o26086" name="gynophore" name_original="gynophore" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Fruits descending, smooth or slightly torulose, arcuate, flattened, 5–13 (–15) cm × 2–2.5 mm;</text>
      <biological_entity id="o26087" name="fruit" name_original="fruits" src="d0_s17" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s17" value="descending" value_original="descending" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="course_or_shape" src="d0_s17" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s17" to="15" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s17" to="13" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s17" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>valves each with prominent midvein;</text>
      <biological_entity id="o26088" name="valve" name_original="valves" src="d0_s18" type="structure" />
      <biological_entity id="o26089" name="midvein" name_original="midvein" src="d0_s18" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s18" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o26088" id="r1769" name="with" negation="false" src="d0_s18" to="o26089" />
    </statement>
    <statement id="d0_s19">
      <text>replum straight;</text>
      <biological_entity id="o26090" name="replum" name_original="replum" src="d0_s19" type="structure">
        <character is_modifier="false" name="course" src="d0_s19" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 50–82 per ovary;</text>
      <biological_entity id="o26091" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o26092" from="50" name="quantity" src="d0_s20" to="82" />
      </biological_entity>
      <biological_entity id="o26092" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 1.5–3.5 mm;</text>
      <biological_entity id="o26093" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s21" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma entire.</text>
      <biological_entity id="o26094" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds oblong, 2.2–3 × 1.4–1.8 mm;</text>
      <biological_entity id="o26095" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="shape" src="d0_s23" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="length" src="d0_s23" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s23" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>wing 0.1–0.4 mm wide, continuous.</text>
      <biological_entity id="o26096" name="wing" name_original="wing" src="d0_s24" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s24" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="continuous" value_original="continuous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Openings in pine forests, oak woodland</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="openings" constraint="in pine forests , oak" />
        <character name="habitat" value="pine forests" />
        <character name="habitat" value="oak" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <other_name type="past_name">longisiliqus</other_name>
  <discussion>Streptanthus longisiliquus is known from Butte, Shasta, and Tehama counties.</discussion>
  
</bio:treatment>