<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">573</other_info_on_meta>
    <other_info_on_meta type="treatment_page">594</other_info_on_meta>
    <other_info_on_meta type="illustration_page">593</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">lepidieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lepidium</taxon_name>
    <taxon_name authority="(Rollins) Al-Shehbaz" date="2002" rank="species">tiehmii</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 9. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe lepidieae;genus lepidium;species tiehmii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095139</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stroganowia</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="species">tiehmii</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>7: 215, fig. 1. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Stroganowia;species tiehmii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(caudex woody, to 1 cm diam., covered with persistent petiolar remains);</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous throughout.</text>
      <biological_entity id="o7957" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems simple from base (caudex branch), erect, branched distally, 1–7.5 dm.</text>
      <biological_entity id="o7958" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character constraint="from base" constraintid="o7959" is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="7.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o7959" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves rosulate;</text>
      <biological_entity constraint="basal" id="o7960" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole (1.5–) 2.5–10 (–13) cm;</text>
      <biological_entity id="o7961" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="2.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="13" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade oblong to lanceolate, (2.5–) 4.5–9 (–14) cm × 15–40 mm, margins entire.</text>
      <biological_entity id="o7962" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="lanceolate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="atypical_length" src="d0_s6" to="4.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="14" to_unit="cm" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s6" to="9" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s6" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7963" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves shortly petiolate;</text>
      <biological_entity constraint="cauline" id="o7964" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblanceolate, (much smaller than basal), base attenuate-cuneate, not auriculate, margins entire.</text>
      <biological_entity id="o7965" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o7966" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="attenuate-cuneate" value_original="attenuate-cuneate" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o7967" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Racemes (paniculate), considerably elongated in fruit.</text>
      <biological_entity id="o7969" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels divaricate-ascending, straight, (terete), 8–15 × 0.4–0.5 mm.</text>
      <biological_entity id="o7968" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character constraint="in fruit" constraintid="o7969" is_modifier="false" modifier="considerably" name="length" src="d0_s9" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o7970" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o7968" id="r581" name="fruiting" negation="false" src="d0_s10" to="o7970" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals oblong, 2.5–3 (–4) × 1.2–1.8 mm;</text>
      <biological_entity id="o7971" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o7972" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s11" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals creamy white to pale-yellow, obovate to oblanceolate, 4–5.5 (–6.5) × 2–3.3 mm, claw 1.5–2.2 mm;</text>
      <biological_entity id="o7973" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o7974" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="creamy white" name="coloration" src="d0_s12" to="pale-yellow" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s12" to="oblanceolate" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="6.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="3.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7975" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 6;</text>
      <biological_entity id="o7976" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o7977" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments (median pairs) 3.5–4.5 mm;</text>
      <biological_entity id="o7978" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o7979" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1–1.2 mm.</text>
      <biological_entity id="o7980" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o7981" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits obovate to somewhat rhomboid, 7–11 × 5–6.5 mm, apically not winged, apical notch absent;</text>
      <biological_entity id="o7982" name="fruit" name_original="fruits" src="d0_s16" type="structure" />
      <biological_entity constraint="apical" id="o7983" name="notch" name_original="notch" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="obovate to somewhat" value_original="obovate to somewhat" />
        <character is_modifier="true" modifier="somewhat" name="shape" src="d0_s16" value="rhomboid" value_original="rhomboid" />
        <character char_type="range_value" from="7" from_unit="mm" is_modifier="true" name="length" src="d0_s16" to="11" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" is_modifier="true" name="width" src="d0_s16" to="6.5" to_unit="mm" />
        <character is_modifier="true" modifier="apically not" name="architecture" src="d0_s16" value="winged" value_original="winged" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o7982" id="r582" name="obovate to somewhat" negation="false" src="d0_s16" to="o7983" />
    </statement>
    <statement id="d0_s17">
      <text>valves thin, smooth, obscurely veined;</text>
      <biological_entity id="o7984" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="obscurely" name="architecture" src="d0_s17" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style 0.2–0.6 mm.</text>
      <biological_entity id="o7985" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s18" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds oblong, 3.7–4.5 × 1.6–2 mm.</text>
      <biological_entity id="o7986" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="length" src="d0_s19" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s19" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky crevices and slopes in sagebrush communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky crevices" constraint="in sagebrush communities" />
        <character name="habitat" value="slopes" constraint="in sagebrush communities" />
        <character name="habitat" value="sagebrush communities" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1400-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>41.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Lepidium tiehmii is known from mountain ranges in Douglas and Lyon counties. It was described and has been maintained (N. H. Holmgren 2005b) in Stroganowia, a genus now united with Lepidium (I. A. Al-Shehbaz et al. 2002) that otherwise is disjunct and restricted to the central Asian states of the Former Soviet Union and adjacent western China. In our opinion, the similarity of this species to those Asian ones formerly placed in Stroganowia is superficial and is the result of convergence rather than descent.</discussion>
  <discussion>The cotyledonary type was erroneously reported as conduplicate (R. C. Rollins 1993; N. H. Holmgren 2005b). In the several seeds that we dissected it was always incumbent.</discussion>
  
</bio:treatment>