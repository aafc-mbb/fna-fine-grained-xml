<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="mention_page">69</other_info_on_meta>
    <other_info_on_meta type="treatment_page">68</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="Seringe" date="1824" rank="section">herbella</taxon_name>
    <taxon_name authority="Andersson in A. P. de Candolle and A. L. P. P. de Candolle" date="1868" rank="species">nummularia</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>16(2): 298. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section herbella;species nummularia</taxon_hierarchy>
    <other_info_on_name type="fna_id">210001958</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nummularia</taxon_name>
    <taxon_name authority="(Schljakov) Á. Löve &amp; D. Löve" date="unknown" rank="subspecies">tundricola</taxon_name>
    <taxon_hierarchy>genus Salix;species nummularia;subspecies tundricola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.01–0.03 m, (dwarf), forming clones by layering.</text>
      <biological_entity id="o30651" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.01" from_unit="m" name="some_measurement" src="d0_s0" to="0.03" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems trailing;</text>
      <biological_entity id="o30652" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="trailing" value_original="trailing" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branches yellowbrown or redbrown, glabrous;</text>
      <biological_entity id="o30653" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branchlets yellowbrown or redbrown, pubescent, pilose, or glabrescent.</text>
      <biological_entity id="o30654" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves: stipules absent or rudimentary;</text>
      <biological_entity id="o30655" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o30656" name="stipule" name_original="stipules" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 1.5–2 mm (glabrous or pubescent adaxially);</text>
      <biological_entity id="o30657" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o30658" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>largest medial blade (2 pairs of secondary-veins arising at or close to base, arcing toward apex) broadly elliptic, subcircular, broadly ovate, or elliptic, 9–22 (–30) × 7.5–14 (–19) mm, 1.2–2 times as long as wide, base rounded, subcordate, cordate, or convex, margins flat or slightly revolute, entire or serrulate, apex convex, rounded, or retuse, abaxial surface glabrous, adaxial highly glossy, glabrous;</text>
      <biological_entity id="o30659" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="largest medial" id="o30660" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcircular" value_original="subcircular" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="22" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="30" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s6" to="22" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="19" to_unit="mm" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="width" src="d0_s6" to="14" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s6" value="1.2-2" value_original="1.2-2" />
      </biological_entity>
      <biological_entity id="o30661" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o30662" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s6" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o30663" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s6" value="retuse" value_original="retuse" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o30664" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o30665" name="surface" name_original="surface" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="highly" name="reflectance" src="d0_s6" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal blade margins entire or serrulate;</text>
      <biological_entity id="o30666" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o30667" name="margin" name_original="margins" src="d0_s7" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>juvenile blade pilose or puberulent abaxially.</text>
      <biological_entity id="o30668" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity id="o30669" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Catkins: from lateral buds;</text>
      <biological_entity id="o30670" name="catkin" name_original="catkins" src="d0_s9" type="structure" />
      <biological_entity constraint="lateral" id="o30671" name="bud" name_original="buds" src="d0_s9" type="structure" />
      <relation from="o30670" id="r2059" name="from" negation="false" src="d0_s9" to="o30671" />
    </statement>
    <statement id="d0_s10">
      <text>staminate (3–8 flowers), 3.2–6.6 × 2–5.2 mm, flowering branchlet 0.8–4.6 mm;</text>
      <biological_entity id="o30672" name="catkin" name_original="catkins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s10" to="6.6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="5.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30673" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="4.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pistillate loosely flowered (3–5 flowers), shape indeterminate, 7.5–13 × 3–10 mm, flowering branchlet 0.5–10 mm;</text>
      <biological_entity id="o30674" name="catkin" name_original="catkins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="loosely" name="architecture" src="d0_s11" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="shape" src="d0_s11" value="indeterminate" value_original="indeterminate" />
        <character char_type="range_value" from="7.5" from_unit="mm" name="length" src="d0_s11" to="13" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30675" name="branchlet" name_original="branchlet" src="d0_s11" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s11" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>floral bract tawny, 0.6–1.4 mm, apex rounded or truncate, entire, abaxially glabrous or sparsely hairy.</text>
      <biological_entity id="o30676" name="catkin" name_original="catkins" src="d0_s12" type="structure" />
      <biological_entity constraint="floral" id="o30677" name="bract" name_original="bract" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="tawny" value_original="tawny" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s12" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30678" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Staminate flowers: abaxial nectary (0.3–) 0.5–0.7 mm, adaxial nectary narrowly oblong or oblong, 0.7–1.1 mm, nectaries distinct, or connate and cupshaped;</text>
      <biological_entity id="o30679" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o30680" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="0.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o30681" name="nectary" name_original="nectary" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30682" name="nectary" name_original="nectaries" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments distinct or connate less than 1/2 their lengths, glabrous;</text>
      <biological_entity id="o30683" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o30684" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s14" to="1/2" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ellipsoid, 0.4–0.5 mm.</text>
      <biological_entity id="o30685" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o30686" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pistillate flowers: abaxial nectary (0–) 0.6–0.9 mm, adaxial nectary narrowly oblong or oblong, 0.6–1.4 mm, longer than stipe, nectaries connate and shallowly cupshaped;</text>
      <biological_entity id="o30687" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o30688" name="nectary" name_original="nectary" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="atypical_some_measurement" src="d0_s16" to="0.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s16" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o30689" name="nectary" name_original="nectary" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s16" to="1.4" to_unit="mm" />
        <character constraint="than stipe" constraintid="o30690" is_modifier="false" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o30690" name="stipe" name_original="stipe" src="d0_s16" type="structure" />
      <biological_entity id="o30691" name="nectary" name_original="nectaries" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="connate" value_original="connate" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s16" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stipe 0–0.7 mm;</text>
      <biological_entity id="o30692" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o30693" name="stipe" name_original="stipe" src="d0_s17" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s17" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovary pyriform, glabrous, beak slightly bulged below styles;</text>
      <biological_entity id="o30694" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o30695" name="ovary" name_original="ovary" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o30696" name="beak" name_original="beak" src="d0_s18" type="structure" />
      <biological_entity id="o30697" name="style" name_original="styles" src="d0_s18" type="structure" />
      <relation from="o30696" id="r2060" name="bulged" negation="false" src="d0_s18" to="o30697" />
    </statement>
    <statement id="d0_s19">
      <text>ovules 8–10 per ovary;</text>
      <biological_entity id="o30698" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o30699" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o30700" from="8" name="quantity" src="d0_s19" to="10" />
      </biological_entity>
      <biological_entity id="o30700" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>styles 0.2–1 mm;</text>
      <biological_entity id="o30701" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o30702" name="style" name_original="styles" src="d0_s20" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s20" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigmas flat, abaxially non-papillate with rounded tip, or broadly cylindrical, 0.2–0.27–0.32 mm.</text>
      <biological_entity id="o30703" name="flower" name_original="flowers" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o30704" name="stigma" name_original="stigmas" src="d0_s21" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s21" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o30705" is_modifier="false" modifier="abaxially" name="relief" src="d0_s21" value="non-papillate" value_original="non-papillate" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s21" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s21" to="0.27-0.32" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30705" name="tip" name_original="tip" src="d0_s21" type="structure">
        <character is_modifier="true" name="shape" src="d0_s21" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Capsules 3.5–7.5 mm. 2n = 38.</text>
      <biological_entity id="o30706" name="capsule" name_original="capsules" src="d0_s22" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s22" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30707" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late Jun-early Aug (based on Russian collections).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="based on Russian collections" to="early Aug" from="late Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Exposed, relatively dry, stony, moss-lichen, and moss tundra, polygonal tundra, outcrops, marine sediments, sand dunes, restricted to snow-free areas, usually on acidic substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="exposed" />
        <character name="habitat" value="relatively dry" />
        <character name="habitat" value="stony" />
        <character name="habitat" value="moss-lichen" />
        <character name="habitat" value="moss tundra" modifier="and" />
        <character name="habitat" value="polygonal tundra" />
        <character name="habitat" value="outcrops" />
        <character name="habitat" value="marine sediments" />
        <character name="habitat" value="sand dunes" />
        <character name="habitat" value="restricted to snow-free areas" />
        <character name="habitat" value="acidic substrates" modifier="usually on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; Asia (China [Jilin], Chukotka, Japan [Hokkaido], North Korea, Russian Far East, arctic, e Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia (China [Jilin])" establishment_means="native" />
        <character name="distribution" value="Asia (Chukotka)" establishment_means="native" />
        <character name="distribution" value="Asia (Japan [Hokkaido])" establishment_means="native" />
        <character name="distribution" value="Asia (North Korea)" establishment_means="native" />
        <character name="distribution" value="Asia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="Asia (arctic)" establishment_means="native" />
        <character name="distribution" value="Asia (e Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>29.</number>
  <other_name type="common_name">Coin-leaf willow</other_name>
  <discussion>Salix nummularia occurs in Alaska on St. Paul Island.</discussion>
  
</bio:treatment>