<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="mention_page">245</other_info_on_meta>
    <other_info_on_meta type="mention_page">246</other_info_on_meta>
    <other_info_on_meta type="mention_page">687</other_info_on_meta>
    <other_info_on_meta type="mention_page">693</other_info_on_meta>
    <other_info_on_meta type="mention_page">724</other_info_on_meta>
    <other_info_on_meta type="treatment_page">688</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="A. Gray" date="1853" rank="genus">DRYOPETALON</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 11, plate 11. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus DRYOPETALON</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek drys, oak, and petalon, leaf, alluding to resemblance of petal shape to leaves of some oaks</other_info_on_name>
    <other_info_on_name type="fna_id">110990</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="genus">Rollinsia</taxon_name>
    <taxon_hierarchy>genus Rollinsia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s2">
      <text>usually pubescent or hirsute, rarely glabrous.</text>
      <biological_entity id="o38236" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect or ascending, unbranched or branched basally or distally.</text>
      <biological_entity id="o38238" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="basally; basally; distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate or sessile;</text>
      <biological_entity id="o38239" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>basal rosulate or not, petiolate, blade margins entire, dentate to runcinate, or pinnatifid;</text>
      <biological_entity constraint="basal" id="o38240" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character name="arrangement" src="d0_s6" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o38241" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character char_type="range_value" from="dentate" name="shape" src="d0_s6" to="runcinate or pinnatifid" />
        <character char_type="range_value" from="dentate" name="shape" src="d0_s6" to="runcinate or pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>cauline petiolate or [sub] sessile, blade (base not auriculate, or auriculate to amplexicaul), margins entire or dentate to pinnatifid.</text>
      <biological_entity constraint="cauline" id="o38242" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o38243" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <biological_entity id="o38244" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s7" value="dentate to pinnatifid" value_original="dentate to pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (corymbose, initially congested), considerably elongated in fruit.</text>
      <biological_entity id="o38246" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels ascending, divaricate, or horizontal, slender.</text>
      <biological_entity id="o38245" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o38246" is_modifier="false" modifier="considerably" name="length" src="d0_s8" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o38247" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o38245" id="r2579" name="fruiting" negation="false" src="d0_s9" to="o38247" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals erect to ascending, oblong [ovate], lateral pair slightly saccate or not basally, (glabrous or pubescent);</text>
      <biological_entity id="o38248" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o38249" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s10" to="ascending" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="position" src="d0_s10" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s10" value="saccate" value_original="saccate" />
        <character name="architecture_or_shape" src="d0_s10" value="not basally" value_original="not basally" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals white or purplish, spatulate or obovate (longer than sepals), claw gradually narrowed from blade to base, (margins sometimes pinnatifid, or deeply 2-lobed);</text>
      <biological_entity id="o38250" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o38251" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="shape" src="d0_s11" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity id="o38252" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character constraint="from blade" constraintid="o38253" is_modifier="false" modifier="gradually" name="shape" src="d0_s11" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o38253" name="blade" name_original="blade" src="d0_s11" type="structure" />
      <biological_entity id="o38254" name="base" name_original="base" src="d0_s11" type="structure" />
      <relation from="o38253" id="r2580" name="to" negation="false" src="d0_s11" to="o38254" />
    </statement>
    <statement id="d0_s12">
      <text>stamens tetradynamous;</text>
      <biological_entity id="o38255" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o38256" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments not dilated basally, (glabrous or papillate basally);</text>
      <biological_entity id="o38257" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o38258" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s13" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers oblong;</text>
      <biological_entity id="o38259" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o38260" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>nectar glands: lateral annular, median glands confluent with lateral.</text>
      <biological_entity constraint="nectar" id="o38261" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="shape" src="d0_s15" value="annular" value_original="annular" />
      </biological_entity>
      <biological_entity constraint="median" id="o38262" name="gland" name_original="glands" src="d0_s15" type="structure">
        <character constraint="with lateral" constraintid="o38263" is_modifier="false" name="arrangement" src="d0_s15" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o38263" name="lateral" name_original="lateral" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits sessile or shortly stipitate, usually linear, rarely linear-oblong, torulose or smooth, terete or flattened (latiseptate);</text>
      <biological_entity id="o38264" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s16" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="usually" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s16" value="linear-oblong" value_original="linear-oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="torulose" value_original="torulose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s16" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s16" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves each with a distinct midvein, glabrous;</text>
      <biological_entity id="o38265" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o38266" name="midvein" name_original="midvein" src="d0_s17" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o38265" id="r2581" name="with" negation="false" src="d0_s17" to="o38266" />
    </statement>
    <statement id="d0_s18">
      <text>replum rounded;</text>
      <biological_entity id="o38267" name="replum" name_original="replum" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>septum complete, not veined;</text>
      <biological_entity id="o38268" name="septum" name_original="septum" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="complete" value_original="complete" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s19" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 10–110 per ovary;</text>
      <biological_entity id="o38269" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o38270" from="10" name="quantity" src="d0_s20" to="110" />
      </biological_entity>
      <biological_entity id="o38270" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style distinct;</text>
      <biological_entity id="o38271" name="style" name_original="style" src="d0_s21" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s21" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>stigma capitate, usually entire, rarely slightly 2-lobed.</text>
      <biological_entity id="o38272" name="stigma" name_original="stigma" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s22" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s22" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely slightly" name="shape" src="d0_s22" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Seeds uniseriate, plump, usually not winged, rarely narrowly so, ovate [oblong];</text>
      <biological_entity id="o38273" name="seed" name_original="seeds" src="d0_s23" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s23" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="size" src="d0_s23" value="plump" value_original="plump" />
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s23" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="rarely narrowly; narrowly" name="shape" src="d0_s23" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>seed-coat not mucilaginous when wetted;</text>
      <biological_entity id="o38274" name="seed-coat" name_original="seed-coat" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s24" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>cotyledons accumbent [incumbent].</text>
    </statement>
    <statement id="d0_s26">
      <text>x = [10] 12, 14.</text>
      <biological_entity id="o38275" name="cotyledon" name_original="cotyledons" src="d0_s25" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s25" value="accumbent" value_original="accumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o38276" name="chromosome" name_original="" src="d0_s26" type="structure">
        <character name="atypical_quantity" src="d0_s26" value="10" value_original="10" />
        <character name="quantity" src="d0_s26" value="12" value_original="12" />
        <character name="quantity" src="d0_s26" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>sw United States, n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="sw United States" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>85.</number>
  <discussion>Species 8 (2 in the flora).</discussion>
  <references>
    <reference>Al-Shehbaz, I. A. 2007. Generic limits of Dryopetalon, Rollinsia, Sibara, and Thelypodiopsis (Brassicaceae) and a synopsis of Dryopetalon. Novon 17: 397–402.</reference>
    <reference>Rollins, R. C. 1941c. The cruciferous genus Dryopetalon. Contr. Dudley Herb. 3: 199–207.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Cauline leaves shortly petiolate or sessile, blade bases not auriculate; fruits 0.5-1.2 mm wide; ovules 60-110 per ovary; seeds not winged; petal margins pinnatifid.</description>
      <determination>1 Dryopetalon runcinatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Cauline leaves sessile, blade bases auriculate; fruits 2-3 mm wide; ovules 10-28 per ovary; seeds winged (narrowly); petal margins usually entire, sometimes repand.</description>
      <determination>2 Dryopetalon viereckii</determination>
    </key_statement>
  </key>
</bio:treatment>