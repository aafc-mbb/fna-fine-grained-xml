<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">464</other_info_on_meta>
    <other_info_on_meta type="mention_page">465</other_info_on_meta>
    <other_info_on_meta type="treatment_page">468</other_info_on_meta>
    <other_info_on_meta type="illustration_page">463</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cardamine</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">bellidifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 654. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus cardamine;species bellidifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250094651</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bellidifolia</taxon_name>
    <taxon_name authority="A. E. Porsild" date="unknown" rank="variety">beringensis</taxon_name>
    <taxon_hierarchy>genus Cardamine;species bellidifolia;variety beringensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bellidifolia</taxon_name>
    <taxon_name authority="Lange" date="unknown" rank="variety">laxa</taxon_name>
    <taxon_hierarchy>genus Cardamine;species bellidifolia;variety laxa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bellidifolia</taxon_name>
    <taxon_name authority="Coville &amp; Leiberg" date="unknown" rank="variety">pachyphylla</taxon_name>
    <taxon_hierarchy>genus Cardamine;species bellidifolia;variety pachyphylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bellidifolia</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="variety">pinnatifida</taxon_name>
    <taxon_hierarchy>genus Cardamine;species bellidifolia;variety pinnatifida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials (somewhat cespitose, subscapose);</text>
    </statement>
    <statement id="d0_s1">
      <text>glabrous throughout.</text>
      <biological_entity id="o10366" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rhizomes absent (caudex usually with slender, rarely stout, rhizomelike branches).</text>
      <biological_entity id="o10367" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect to ascending, branched (several), 0.1–0.8 (–1.4) dm, glabrous.</text>
      <biological_entity id="o10368" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="1.4" to_unit="dm" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s3" to="0.8" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves rosulate, simple, (0.6–) 1.2–5 (–7) cm;</text>
      <biological_entity constraint="basal" id="o10369" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="1.2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="7" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s4" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole (3–) 1–3.5 (–5.5) cm;</text>
      <biological_entity id="o10370" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="average_some_measurement" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s5" to="3.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade usually ovate to oblong, rarely oblanceolate to obovate, (0.4–) 0.8–1.7 (–2.5) cm × (2–) 5–10 (–16) mm, base cuneate to obtuse, margins usually entire, rarely repand or obtusely small-toothed (apex obtuse).</text>
      <biological_entity id="o10371" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="usually ovate" name="shape" src="d0_s6" to="oblong rarely oblanceolate" />
        <character char_type="range_value" from="usually ovate" name="shape" src="d0_s6" to="oblong rarely oblanceolate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="atypical_length" src="d0_s6" to="0.8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s6" to="1.7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s6" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="16" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10372" name="base" name_original="base" src="d0_s6" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
      <biological_entity id="o10373" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s6" value="repand" value_original="repand" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s6" value="small-toothed" value_original="small-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves 0 or 1 (or 2), simple, shortly petiolate;</text>
      <biological_entity constraint="cauline" id="o10374" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character name="presence" src="d0_s7" unit="or" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s7" unit="or" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s7" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petiole base not auriculate;</text>
      <biological_entity constraint="petiole" id="o10375" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade similar to basal leaves, much smaller.</text>
      <biological_entity id="o10376" name="blade" name_original="blade" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="much" name="size" notes="" src="d0_s9" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10377" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <relation from="o10376" id="r727" name="to" negation="false" src="d0_s9" to="o10377" />
    </statement>
    <statement id="d0_s10">
      <text>Racemes ebracteate.</text>
    </statement>
    <statement id="d0_s11">
      <text>Fruiting pedicels ascending to erect, 3–6 (–8) mm.</text>
      <biological_entity id="o10378" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="ebracteate" value_original="ebracteate" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s11" to="erect" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10379" name="pedicel" name_original="pedicels" src="d0_s11" type="structure" />
      <relation from="o10378" id="r728" name="fruiting" negation="false" src="d0_s11" to="o10379" />
    </statement>
    <statement id="d0_s12">
      <text>Flowers: sepals oblong, 2–3 (–4) × 0.8–1.5 (–2) mm, lateral pair not saccate basally;</text>
      <biological_entity id="o10380" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o10381" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s12" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals white, oblanceolate, 4–5.5 (–7) × 1.3–2 (–2.5) mm (not clawed, apex rounded or emarginate);</text>
      <biological_entity id="o10382" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o10383" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s13" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s13" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s13" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments: median pairs 2.5–4 mm, lateral pair 2–3 mm;</text>
      <biological_entity id="o10384" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="median" value_original="median" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers oblong, 0.5–0.9 mm.</text>
      <biological_entity id="o10385" name="filament" name_original="filaments" src="d0_s15" type="structure" />
      <biological_entity id="o10386" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits linear, (0.8–) 1.3–2.8 (–3.7) cm × 1.3–2 mm;</text>
      <biological_entity id="o10387" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s16" value="linear" value_original="linear" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_length" src="d0_s16" to="1.3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.8" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s16" to="3.7" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="length" src="d0_s16" to="2.8" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s16" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 8–18 per ovary;</text>
      <biological_entity id="o10388" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o10389" from="8" name="quantity" src="d0_s17" to="18" />
      </biological_entity>
      <biological_entity id="o10389" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 0.5–3 mm.</text>
      <biological_entity id="o10390" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s18" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds brown, oblong, 1.5–2 × 0.9–1.2 mm. 2n = 16.</text>
      <biological_entity id="o10391" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s19" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s19" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10392" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Sep.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Sep" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mossy areas, tundra, marshes at stream headwaters, cliffs, talus slopes, barren chert slopes, moist rock crevices, rocky slopes, alpine streams, sandy beaches, moist rocky streambeds</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mossy areas" />
        <character name="habitat" value="tundra" />
        <character name="habitat" value="marshes" constraint="at stream headwaters , cliffs , talus slopes , barren chert slopes , moist rock crevices , rocky slopes , alpine streams , sandy beaches , moist rocky streambeds" />
        <character name="habitat" value="stream headwaters" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="barren chert slopes" />
        <character name="habitat" value="moist rock crevices" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="alpine streams" />
        <character name="habitat" value="sandy beaches" />
        <character name="habitat" value="moist rocky streambeds" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; Alta., B.C., Nfld. and Labr. (Labr.), N.W.T., Nunavut, Que., Yukon; Alaska, Calif., Maine, N.H., Oreg., Wash.; n Europe; Asia (Russian Far East, Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="n Europe" establishment_means="native" />
        <character name="distribution" value="Asia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="Asia (Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>