<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Steve Boyd</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">244</other_info_on_meta>
    <other_info_on_meta type="mention_page">676</other_info_on_meta>
    <other_info_on_meta type="treatment_page">694</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="S. Boyd &amp; T. S. Ross" date="1997" rank="genus">SIBAROPSIS</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>44: 30, figs. 2–4. 1997</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus SIBAROPSIS</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Sibara and Greek opsis, appearance</other_info_on_name>
    <other_info_on_name type="fna_id">316470</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s2">
      <text>(often glaucous), usually glabrous, sometimes glabrate, trichomes minute, (proximalmost leaves with evanescent cilia).</text>
      <biological_entity id="o37115" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glabrate" value_original="glabrate" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
      <biological_entity id="o37116" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, often branched basally.</text>
      <biological_entity id="o37117" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often; basally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s5">
      <text>sessile;</text>
      <biological_entity id="o37118" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade (base not auriculate), not rosulate, margins entire.</text>
      <biological_entity id="o37119" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
      </biological_entity>
      <biological_entity id="o37120" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes (corymbose, several-flowered, lax), considerably elongated in fruit.</text>
      <biological_entity id="o37122" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels usually ascending, rarely straight, slender.</text>
      <biological_entity id="o37121" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o37122" is_modifier="false" modifier="considerably" name="length" src="d0_s7" value="elongated" value_original="elongated" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o37123" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o37121" id="r2501" name="fruiting" negation="false" src="d0_s8" to="o37123" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals erect, lanceolate to ovate-oblong, (subequal), lateral pair obscurely subsaccate basally;</text>
      <biological_entity id="o37124" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o37125" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="ovate-oblong" />
        <character is_modifier="false" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="obscurely; basally" name="shape" src="d0_s9" value="subsaccate" value_original="subsaccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals light purple or pink-lavender (with darker purplish veins, adaxial pair slightly larger), spatulate, claw well-differentiated from blade, (apex slightly emarginate to obcordate);</text>
      <biological_entity id="o37126" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o37127" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="light purple" value_original="light purple" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pink-lavender" value_original="pink-lavender" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o37128" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character constraint="from blade" constraintid="o37129" is_modifier="false" name="variability" src="d0_s10" value="well-differentiated" value_original="well-differentiated" />
      </biological_entity>
      <biological_entity id="o37129" name="blade" name_original="blade" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens in 3 unequal pairs, (adaxial pair sterile);</text>
      <biological_entity id="o37130" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o37131" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
      <biological_entity id="o37132" name="pair" name_original="pairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="true" name="size" src="d0_s11" value="unequal" value_original="unequal" />
      </biological_entity>
      <relation from="o37131" id="r2502" name="in" negation="false" src="d0_s11" to="o37132" />
    </statement>
    <statement id="d0_s12">
      <text>filaments not dilated basally, (adaxial pair ± connate);</text>
      <biological_entity id="o37133" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o37134" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s12" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers ovate-oblong, (not apiculate);</text>
      <biological_entity id="o37135" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o37136" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate-oblong" value_original="ovate-oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>nectar glands lateral, (minute), median glands absent.</text>
      <biological_entity id="o37137" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity constraint="nectar" id="o37138" name="gland" name_original="glands" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
      </biological_entity>
      <biological_entity constraint="median" id="o37139" name="gland" name_original="glands" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits tardily dehiscent, sessile, linear, smooth, slightly latiseptate;</text>
      <biological_entity id="o37140" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="tardily" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s15" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>valves each with obscure midvein, glabrate, (margins minutely scabrous);</text>
      <biological_entity id="o37141" name="valve" name_original="valves" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s16" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o37142" name="midvein" name_original="midvein" src="d0_s16" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s16" value="obscure" value_original="obscure" />
      </biological_entity>
      <relation from="o37141" id="r2503" name="with" negation="false" src="d0_s16" to="o37142" />
    </statement>
    <statement id="d0_s17">
      <text>replum rounded;</text>
      <biological_entity id="o37143" name="replum" name_original="replum" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>septum complete;</text>
      <biological_entity id="o37144" name="septum" name_original="septum" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 24–44 per ovary;</text>
      <biological_entity id="o37145" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o37146" from="24" name="quantity" src="d0_s19" to="44" />
      </biological_entity>
      <biological_entity id="o37146" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style distinct;</text>
      <biological_entity id="o37147" name="style" name_original="style" src="d0_s20" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s20" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigma subentire.</text>
      <biological_entity id="o37148" name="stigma" name_original="stigma" src="d0_s21" type="structure">
        <character is_modifier="false" name="shape" src="d0_s21" value="subentire" value_original="subentire" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds uniseriate, flattened, obscurely winged distally, oblong;</text>
      <biological_entity id="o37149" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s22" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="shape" src="d0_s22" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="obscurely; distally" name="architecture" src="d0_s22" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>seed-coat not mucilaginous when wetted;</text>
      <biological_entity id="o37150" name="seed-coat" name_original="seed-coat" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s23" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>cotyledons incumbent, (linear).</text>
      <biological_entity id="o37151" name="cotyledon" name_original="cotyledons" src="d0_s24" type="structure">
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s24" value="incumbent" value_original="incumbent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>88.</number>
  <discussion>Species 1.</discussion>
  <discussion>Sibaropsis is unusual in Brassicaceae in that the inflorescence axis disarticulates distal to each pedicel and subtending axis internode, thus fruits are dispersed as individual units, except that the proximalmost fruits remain persistent.</discussion>
  
</bio:treatment>