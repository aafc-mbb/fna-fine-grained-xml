<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">425</other_info_on_meta>
    <other_info_on_meta type="treatment_page">426</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">cakile</taxon_name>
    <taxon_name authority="(B. L. Robinson) Millspaugh" date="1900" rank="species">geniculata</taxon_name>
    <place_of_publication>
      <publication_title>Publ. Field Columbian Mus., Bot. Ser.</publication_title>
      <place_in_publication>2: 126. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus cakile;species geniculata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094634</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cakile</taxon_name>
    <taxon_name authority="(Willdenow) O. E. Schulz" date="unknown" rank="species">maritima</taxon_name>
    <taxon_name authority="B. L. Robinson" date="unknown" rank="variety">geniculata</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(1,1): 132. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cakile;species maritima;variety geniculata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cakile</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lanceolata</taxon_name>
    <taxon_name authority="(B. L. Robinson) Shinners" date="unknown" rank="variety">geniculata</taxon_name>
    <taxon_hierarchy>genus Cakile;species lanceolata;variety geniculata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o41479" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, (sometimes suffrutescent, much-branched), to 10 dm.</text>
      <biological_entity id="o41480" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s1" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Cauline leaves: blade broadly ovate or spatulate, margins sinuate, dentate, or pinnately lobed.</text>
      <biological_entity constraint="cauline" id="o41481" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o41482" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o41483" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Racemes: 1–2 dm;</text>
      <biological_entity id="o41484" name="raceme" name_original="racemes" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="2" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>rachis geniculate.</text>
      <biological_entity id="o41485" name="raceme" name_original="racemes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Fruiting pedicels (rachis of equal width), 2–5 mm, (widely spaced).</text>
      <biological_entity id="o41486" name="rachis" name_original="rachis" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="geniculate" value_original="geniculate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41487" name="pedicel" name_original="pedicels" src="d0_s5" type="structure" />
      <relation from="o41486" id="r2802" name="fruiting" negation="false" src="d0_s5" to="o41487" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers: sepals 3–4 mm, lateral pair not saccate basally;</text>
      <biological_entity id="o41488" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o41489" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s6" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s6" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petals white to pale lavender, 4–6.1 × 1.2–1.9 mm, claw distinct.</text>
      <biological_entity id="o41490" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o41491" name="petal" name_original="petals" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="pale lavender" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6.1" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s7" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o41492" name="claw" name_original="claw" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits (8-ribbed), lanceoloid, 20–27 × 3–5 mm;</text>
      <biological_entity id="o41493" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceoloid" value_original="lanceoloid" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s8" to="27" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>proximal segment terete, (6–11 mm);</text>
      <biological_entity constraint="proximal" id="o41494" name="segment" name_original="segment" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>terminal segment slenderly conical, (13–18 mm), apex acute to blunt, (often slightly curved).</text>
      <biological_entity constraint="terminal" id="o41495" name="segment" name_original="segment" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slenderly" name="shape" src="d0_s10" value="conical" value_original="conical" />
      </biological_entity>
      <biological_entity id="o41496" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds: cotyledons accumbent to obliquely incumbent.</text>
      <biological_entity id="o41497" name="seed" name_original="seeds" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 18.</text>
      <biological_entity id="o41498" name="cotyledon" name_original="cotyledons" src="d0_s11" type="structure">
        <character char_type="range_value" from="accumbent" name="arrangement" src="d0_s11" to="obliquely incumbent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o41499" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round (peak Mar–Aug).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="peak" to="" from="" constraint=" year round" />
        <character name="flowering time" char_type="range_value" to="Aug" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy beaches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy beaches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>La., Tex.; Mexico (Tamaulipas, Veracruz).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Tamaulipas)" establishment_means="native" />
        <character name="distribution" value="Mexico (Veracruz)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  
</bio:treatment>