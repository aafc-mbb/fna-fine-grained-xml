<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">467</other_info_on_meta>
    <other_info_on_meta type="mention_page">484</other_info_on_meta>
    <other_info_on_meta type="treatment_page">483</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cardamine</taxon_name>
    <taxon_name authority="(O. E. Schulz) C. L. Hitchcock in C. L. Hitchcock et al." date="1964" rank="species">rupicola</taxon_name>
    <place_of_publication>
      <publication_title>in C. L. Hitchcock et al., Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>2: 474. 1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus cardamine;species rupicola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094625</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="(Nuttall) Greene" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="O. E. Schulz" date="unknown" rank="variety">rupicola</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Jahrb. Syst.</publication_title>
      <place_in_publication>32: 388. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cardamine;species californica;variety rupicola;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dentaria</taxon_name>
    <taxon_name authority="(O. E. Schulz) Rydberg" date="unknown" rank="species">rupicola</taxon_name>
    <taxon_hierarchy>genus Dentaria;species rupicola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>glabrous throughout.</text>
      <biological_entity id="o10490" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rhizomes cylindrical, slender, 1–2 mm diam.</text>
      <biological_entity id="o10491" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect or decumbent at base, unbranched, 0.6–2 dm.</text>
      <biological_entity id="o10492" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o10493" is_modifier="false" name="orientation" src="d0_s3" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.6" from_unit="dm" name="some_measurement" src="d0_s3" to="2" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o10493" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Rhizomal leaves palmately or subpalmately compound, 3 or 5 (or 7) -foliolate, 5.5–17 (–22) cm, (fleshy), petiolate, leaflets petiolulate or subsessile;</text>
      <biological_entity constraint="rhizomal" id="o10494" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="subpalmately" name="architecture" src="d0_s4" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-foliolate" value_original="5-foliolate" />
        <character char_type="range_value" from="17" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="22" to_unit="cm" />
        <character char_type="range_value" from="5.5" from_unit="cm" name="some_measurement" src="d0_s4" to="17" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o10495" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 4–14 (–17) cm;</text>
      <biological_entity id="o10496" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="17" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s5" to="14" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral leaflets subsessile, blade similar to terminal, sometimes smaller;</text>
      <biological_entity constraint="lateral" id="o10497" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o10498" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>terminal leaflet (petiolule 0.1–0.5 cm), blade ovate to lanceolate or elliptic-oblong, 1–3 cm × 6–17 mm, base cuneate or obtuse, margins entire, (apiculate).</text>
      <biological_entity constraint="terminal" id="o10499" name="leaflet" name_original="leaflet" src="d0_s7" type="structure" />
      <biological_entity id="o10500" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s7" to="lanceolate or elliptic-oblong" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10501" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o10502" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cauline leaves 2 or 3, 3 or 5-foliolate, petiolate, leaflets petiolulate or sessile;</text>
      <biological_entity constraint="cauline" id="o10503" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s8" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="5-foliolate" value_original="5-foliolate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o10504" name="leaflet" name_original="leaflets" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petiole 0.7–4 (–8) cm, base not auriculate;</text>
      <biological_entity id="o10505" name="petiole" name_original="petiole" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s9" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10506" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflets sessile, blade similar to terminal, smaller;</text>
      <biological_entity constraint="lateral" id="o10507" name="leaflet" name_original="leaflets" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o10508" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="size" src="d0_s10" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>terminal leaflet petiolulate (0.1–0.5 cm), blade elliptic to oblong, or ovate, 1.2–3.5 cm × 4–25 mm, margins entire.</text>
      <biological_entity constraint="terminal" id="o10509" name="leaflet" name_original="leaflet" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="petiolulate" value_original="petiolulate" />
      </biological_entity>
      <biological_entity id="o10510" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="oblong or ovate" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="oblong or ovate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s11" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s11" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10511" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Racemes ebracteate.</text>
    </statement>
    <statement id="d0_s13">
      <text>Fruiting pedicels ascending to divaricate, 6–18 mm.</text>
      <biological_entity id="o10512" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s13" value="divaricate" value_original="divaricate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10513" name="pedicel" name_original="pedicels" src="d0_s13" type="structure" />
      <relation from="o10512" id="r735" name="fruiting" negation="false" src="d0_s13" to="o10513" />
    </statement>
    <statement id="d0_s14">
      <text>Flowers: sepals oblong, 3–5 × 1.5–2 mm, lateral pair saccate basally;</text>
      <biological_entity id="o10514" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o10515" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s14" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="basally" name="architecture_or_shape" src="d0_s14" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals white, obovate, 8–14 × 4–7 mm, (short-clawed, apex rounded or subemarginate);</text>
      <biological_entity id="o10516" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o10517" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s15" to="14" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s15" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments: median pairs 4–5 mm, lateral pair 2.5–3.5 mm;</text>
      <biological_entity id="o10518" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="median" value_original="median" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s16" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers oblong, 1–1.2 mm.</text>
      <biological_entity id="o10519" name="filament" name_original="filaments" src="d0_s17" type="structure" />
      <biological_entity id="o10520" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits linear, 2–4 cm × 1.5–2.2 mm;</text>
      <biological_entity id="o10521" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s18" value="linear" value_original="linear" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s18" to="4" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s18" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 10–14 per ovary;</text>
      <biological_entity id="o10522" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o10523" from="10" name="quantity" src="d0_s19" to="14" />
      </biological_entity>
      <biological_entity id="o10523" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style 1–5 mm.</text>
      <biological_entity id="o10524" name="style" name_original="style" src="d0_s20" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s20" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Seeds brown, oblong, 1.8–2.2 × 1.2–1.5 mm.</text>
      <biological_entity id="o10525" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s21" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s21" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s21" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s21" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Limestone talus slopes, loose limey shale, moist banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="limestone talus slopes" />
        <character name="habitat" value="loose limey shale" />
        <character name="habitat" value="moist banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200-2700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2700" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>38.</number>
  <discussion>Cardamine rupicola is known from Flathead, Lewis and Clarke, and Missoula counties.</discussion>
  
</bio:treatment>