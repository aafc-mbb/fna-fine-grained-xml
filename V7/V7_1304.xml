<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">229</other_info_on_meta>
    <other_info_on_meta type="mention_page">232</other_info_on_meta>
    <other_info_on_meta type="mention_page">245</other_info_on_meta>
    <other_info_on_meta type="treatment_page">742</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="Nuttall" date="1834" rank="genus">WAREA</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>7: 83, plate 10. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus WAREA</taxon_hierarchy>
    <other_info_on_name type="etymology">For Nathaniel A. Ware, 1789–1853, teacher in South Carolina and plant collector, especially in Florida</other_info_on_name>
    <other_info_on_name type="fna_id">134766</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>not scapose;</text>
    </statement>
    <statement id="d0_s2">
      <text>(usually somewhat glaucous), glabrous throughout (rarely petal claws pubescent).</text>
      <biological_entity id="o10595" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="scapose" value_original="scapose" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems erect, often branched distally, rarely unbranched, (usually slender, rarely stout).</text>
      <biological_entity id="o10596" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often; distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline (basal not seen, soon withered, not rosulate);</text>
    </statement>
    <statement id="d0_s5">
      <text>petiolate or sessile;</text>
      <biological_entity id="o10597" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade (base cuneate, auriculate, or amplexicaul), margins entire.</text>
      <biological_entity id="o10598" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <biological_entity id="o10599" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes (corymbose, several-flowered, floral buds clavate), not or slightly elongated in fruit.</text>
      <biological_entity id="o10601" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels (often deciduous at maturity, leaving elevated discoid scars on rachis), divaricate, slender, (sometimes filiform, straight, with 2 lateral glands basally).</text>
      <biological_entity id="o10600" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o10601" is_modifier="false" modifier="not; slightly" name="length" src="d0_s7" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o10602" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o10600" id="r739" name="fruiting" negation="false" src="d0_s8" to="o10602" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals spreading or reflexed, linear-oblanceolate, lateral pair not saccate basally;</text>
      <biological_entity id="o10603" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o10604" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character is_modifier="false" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s9" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals (spreading), white or pink to deep purple, obovate, orbicular, or spatulate, (margins entire or crisped), claw strongly differentiated from blade (slender, often dilated basally, usually minutely to coarsely papillate or pubescent, rarely nearly smooth, apex rounded);</text>
      <biological_entity id="o10605" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character char_type="range_value" from="pink" name="coloration" notes="" src="d0_s10" to="deep purple" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o10606" name="petal" name_original="petals" src="d0_s10" type="structure" />
      <biological_entity id="o10607" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character constraint="from blade" constraintid="o10608" is_modifier="false" modifier="strongly" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o10608" name="blade" name_original="blade" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens (strongly exserted, spreading), subequal;</text>
      <biological_entity id="o10609" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s11" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o10610" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>filaments not dilated basally;</text>
      <biological_entity id="o10611" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o10612" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s12" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers linear, (coiled after dehiscence);</text>
      <biological_entity id="o10613" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o10614" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s13" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>nectar glands confluent, subtending bases of stamens, (often 6 teeth alternating with filaments), median glands present.</text>
      <biological_entity id="o10615" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity constraint="nectar" id="o10616" name="gland" name_original="glands" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o10617" name="base" name_original="bases" src="d0_s14" type="structure" />
      <biological_entity id="o10618" name="stamen" name_original="stamens" src="d0_s14" type="structure" />
      <biological_entity constraint="median" id="o10619" name="gland" name_original="glands" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o10616" id="r740" name="subtending" negation="false" src="d0_s14" to="o10617" />
      <relation from="o10616" id="r741" name="part_of" negation="false" src="d0_s14" to="o10618" />
    </statement>
    <statement id="d0_s15">
      <text>Fruits stipitate, narrowly linear, smooth, (recurved), latiseptate;</text>
      <biological_entity id="o10620" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="latiseptate" value_original="latiseptate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>valves each with prominent midvein throughout;</text>
      <biological_entity id="o10621" name="valve" name_original="valves" src="d0_s16" type="structure" />
      <biological_entity id="o10622" name="midvein" name_original="midvein" src="d0_s16" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s16" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o10621" id="r742" name="with" negation="false" src="d0_s16" to="o10622" />
    </statement>
    <statement id="d0_s17">
      <text>replum rounded;</text>
      <biological_entity id="o10623" name="replum" name_original="replum" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>septum complete;</text>
      <biological_entity id="o10624" name="septum" name_original="septum" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules 20–60 per ovary;</text>
      <biological_entity id="o10625" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o10626" from="20" name="quantity" src="d0_s19" to="60" />
      </biological_entity>
      <biological_entity id="o10626" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>style usually obsolete, rarely distinct;</text>
      <biological_entity id="o10627" name="style" name_original="style" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s20" value="obsolete" value_original="obsolete" />
        <character is_modifier="false" modifier="rarely" name="fusion" src="d0_s20" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigma entire.</text>
      <biological_entity id="o10628" name="stigma" name_original="stigma" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s21" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds uniseriate, not winged, oblong;</text>
      <biological_entity id="o10629" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s22" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s22" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>seed-coat (concentrically striate), not mucilaginous when wetted;</text>
      <biological_entity id="o10630" name="seed-coat" name_original="seed-coat" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s23" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>cotyledons accumbent.</text>
    </statement>
    <statement id="d0_s25">
      <text>x = 12.</text>
      <biological_entity id="o10631" name="cotyledon" name_original="cotyledons" src="d0_s24" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s24" value="accumbent" value_original="accumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o10632" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>se United States.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="se United States" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>95.</number>
  <discussion>Species 4 (4 in the flora).</discussion>
  <references>
    <reference>Channell, R. B. and C. W. James. 1964. Nomenclatural and taxonomic corrections in Warea (Cruciferae). Rhodora 66: 18–26.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Cauline leaves petiolate or obsolete, blades linear-oblanceolate, oblancolate, or narrowly oblong</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Cauline leaves sessile, blades oblong, ovate, or lanceolate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petal claws nearly smooth or obscurely papillate, margins entire; gynophores (5-)7-11 mm.</description>
      <determination>1 Warea cuneifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petal claws coarsely papillate to pubescent, margins crisped; gynophores 3-6(-7) mm.</description>
      <determination>2 Warea carteri</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade base not clasping stem, obtuse to minutely auriculate.</description>
      <determination>3 Warea sessilifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade base clasping stem, amplexicaul to strongly auriculate.</description>
      <determination>4 Warea amplexifolia</determination>
    </key_statement>
  </key>
</bio:treatment>