<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">274</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="mention_page">292</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="treatment_page">323</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="R. Brown ex de Candolle" date="1821" rank="species">oblongata</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>2: 342. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species oblongata</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094693</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="J. Vahl" date="unknown" rank="species">arctica</taxon_name>
    <taxon_name authority="(E. Ekman) Böcher" date="unknown" rank="subspecies">groenlandica</taxon_name>
    <taxon_hierarchy>genus Draba;species arctica;subspecies groenlandica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Adams" date="unknown" rank="species">cinerea</taxon_name>
    <taxon_name authority="(E. Ekman) Böcher" date="unknown" rank="subspecies">groenlandica</taxon_name>
    <taxon_hierarchy>genus Draba;species cinerea;subspecies groenlandica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="E. Ekman" date="unknown" rank="species">groenlandica</taxon_name>
    <taxon_hierarchy>genus Draba;species groenlandica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(cespitose);</text>
      <biological_entity id="o20259" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched (with persistent leaf remains);</text>
    </statement>
    <statement id="d0_s3">
      <text>sometimes scapose.</text>
      <biological_entity id="o20260" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched, (0.2–) 0.4–1.5 (–1.9) dm, pubescent throughout, trichomes simple, 0.4–0.8 mm, and short-stalked, 4–10-rayed, 0.05–0.3 mm, (mostly branched distally).</text>
      <biological_entity id="o20261" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="0.4" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="1.9" to_unit="dm" />
        <character char_type="range_value" from="0.4" from_unit="dm" name="some_measurement" src="d0_s4" to="1.5" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20262" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s4" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves rosulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o20263" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole base and margin ciliate, (trichomes simple, 0.3–1 mm);</text>
      <biological_entity constraint="petiole" id="o20264" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o20265" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblanceolate to lanceolate, 0.4–1.5 cm × 1.5–5 mm, margins entire, surfaces pubescent with simple and stalked, 2-rayed or 3-rayed trichomes, 0.4–1 mm, with short-stalked, 8–12-rayed, stellate ones, 0.1–0.2 mm.</text>
      <biological_entity id="o20266" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="lanceolate" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s8" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20267" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o20268" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character constraint="with simple" is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o20269" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20270" name="one" name_original="ones" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s8" value="stellate" value_original="stellate" />
      </biological_entity>
      <relation from="o20269" id="r1400" name="with" negation="false" src="d0_s8" to="o20270" />
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 0–2;</text>
    </statement>
    <statement id="d0_s10">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o20271" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" name="quantity" src="d0_s9" to="2" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>blade oblong to broadly ovate, margins entire, surfaces pubescent as basal.</text>
      <biological_entity id="o20272" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="broadly ovate" />
      </biological_entity>
      <biological_entity id="o20273" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o20274" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character constraint="as basal" constraintid="o20275" is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20275" name="basal" name_original="basal" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Racemes 5–13 (–18) -flowered, ebracteate, elongated in fruit;</text>
      <biological_entity id="o20276" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-13(-18)-flowered" value_original="5-13(-18)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o20277" is_modifier="false" name="length" src="d0_s12" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o20277" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>rachis not flexuous, pubescent as stem distally.</text>
      <biological_entity id="o20279" name="stem" name_original="stem" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruiting pedicels divaricate to ascending, straight, 2–6 (–8) mm, pubescent as rachis.</text>
      <biological_entity id="o20278" name="rachis" name_original="rachis" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o20279" is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20280" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <biological_entity id="o20281" name="rachis" name_original="rachis" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o20278" id="r1401" name="fruiting" negation="false" src="d0_s14" to="o20280" />
    </statement>
    <statement id="d0_s15">
      <text>Flowers: sepals ovate, 2–3 mm, pubescent, (trichomes simple and short-stalked, mostly 2-rayed);</text>
      <biological_entity id="o20282" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o20283" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals white, spatulate to obovate, 3.5–5 × 1.5–2.5 mm;</text>
      <biological_entity id="o20284" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o20285" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character char_type="range_value" from="spatulate" name="shape" src="d0_s16" to="obovate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s16" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s16" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers ovate, 0.2–0.3 mm.</text>
      <biological_entity id="o20286" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o20287" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s17" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits elliptic to oblong, plane, slightly flattened, 4–8 (–9.5) × 2–3 (–4) mm;</text>
      <biological_entity id="o20288" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s18" to="oblong" />
        <character is_modifier="false" name="shape" src="d0_s18" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s18" to="9.5" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s18" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s18" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s18" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>valves pubescent, trichomes short-stalked, (2–) 5–12-rayed, 0.05–3 mm;</text>
      <biological_entity id="o20289" name="valve" name_original="valves" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o20290" name="trichome" name_original="trichomes" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s19" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 24–36 per ovary;</text>
      <biological_entity id="o20291" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o20292" from="24" name="quantity" src="d0_s20" to="36" />
      </biological_entity>
      <biological_entity id="o20292" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 0.2–0.8 mm (stigma considerably wider than style, often appearing 2-lobed).</text>
      <biological_entity id="o20293" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s21" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds ovate, 0.7–1 × 0.5–0.6 mm. 2n = 64.</text>
      <biological_entity id="o20294" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s22" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s22" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20295" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Ridges and hillsides, sand and gravel flood plains, swales, tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ridges" />
        <character name="habitat" value="hillsides" />
        <character name="habitat" value="sand" />
        <character name="habitat" value="gravel flood plains" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; N.W.T., Nunavut; Alaska; e Asia (Russian Far East, Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="e Asia (Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>71.</number>
  <discussion>Plants of Draba oblongata are occasionally misidentified as decaploid (2n = 80) D. arctica, but the former is readily separated by a preponderance of simple (versus stellate) trichomes on both leaf blade surfaces. Draba oblongata is sometimes confused with D. micropetala, but the latter has pale yellow (versus white) flowers, fruits pubescent with simple and spurred (versus (2–)5–12-rayed) trichomes, and stigmas as wide as (versus much wider than) the style.</discussion>
  
</bio:treatment>