<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="mention_page">378</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="treatment_page">408</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="(Rydberg) Windham &amp; Al-Shehbaz" date="2006" rank="species">spatifolia</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>11: 84. 2006</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species spatifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094527</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="Rydberg" date="unknown" rank="species">spatifolia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Rocky Mts.,</publication_title>
      <place_in_publication>361, 1062. 1917</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arabis;species spatifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="unknown" rank="species">fendleri</taxon_name>
    <taxon_name authority="(Rydberg) Rollins" date="unknown" rank="variety">spatifolia</taxon_name>
    <taxon_hierarchy>genus Arabis;species fendleri;variety spatifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Boechera</taxon_name>
    <taxon_name authority="(S. Watson) W. A. Weber" date="unknown" rank="species">fendleri</taxon_name>
    <taxon_name authority="(Rydberg) W. A. Weber" date="unknown" rank="subspecies">spatifolia</taxon_name>
    <taxon_hierarchy>genus Boechera;species fendleri;subspecies spatifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Boechera</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fendleri</taxon_name>
    <taxon_name authority="(Rydberg) Dorn" date="unknown" rank="variety">spatifolia</taxon_name>
    <taxon_hierarchy>genus Boechera;species fendleri;variety spatifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>short to long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>sexual;</text>
      <biological_entity id="o4296" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="sexual" value_original="sexual" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>caudex usually not woody (rarely with persistent, crowded leaf-bases).</text>
      <biological_entity id="o4297" name="caudex" name_original="caudex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually not" name="texture" src="d0_s3" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems simple or few to several per caudex branch, arising from center of rosette near ground surface, 1.5–3.5 (–5) dm, densely pubescent proximally, trichomes simple mixed with short-stalked ones, 2-rayed, 0.3–0.7 mm.</text>
      <biological_entity id="o4298" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s4" value="few to several" value_original="few to several" />
        <character constraint="from center" constraintid="o4300" is_modifier="false" name="orientation" notes="" src="d0_s4" value="arising" value_original="arising" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s4" to="5" to_unit="dm" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="3.5" to_unit="dm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o4299" name="branch" name_original="branch" src="d0_s4" type="structure" />
      <biological_entity id="o4300" name="center" name_original="center" src="d0_s4" type="structure" />
      <biological_entity id="o4301" name="rosette" name_original="rosette" src="d0_s4" type="structure" />
      <biological_entity id="o4302" name="ground" name_original="ground" src="d0_s4" type="structure" />
      <biological_entity id="o4303" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o4304" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character constraint="with ones" constraintid="o4305" is_modifier="false" name="arrangement" src="d0_s4" value="mixed" value_original="mixed" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4305" name="one" name_original="ones" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
      </biological_entity>
      <relation from="o4298" id="r314" name="per" negation="false" src="d0_s4" to="o4299" />
      <relation from="o4300" id="r315" name="part_of" negation="false" src="d0_s4" to="o4301" />
      <relation from="o4300" id="r316" name="near" negation="false" src="d0_s4" to="o4302" />
      <relation from="o4300" id="r317" name="near" negation="false" src="d0_s4" to="o4303" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade narrowly oblanceolate, 1.5–3 (–4) mm wide, margins entire, strongly ciliate at least along petiole, trichomes (simple), to 1 mm, surfaces glabrous or sparsely pubescent, trichomes simple and short-stalked, 2-rayed, 0.3–0.7 mm.</text>
      <biological_entity constraint="basal" id="o4306" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o4307" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4308" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character constraint="along petiole" constraintid="o4309" is_modifier="false" modifier="strongly" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o4309" name="petiole" name_original="petiole" src="d0_s5" type="structure" />
      <biological_entity id="o4310" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4311" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o4312" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: 5–15 (–20), often concealing stem proximally;</text>
      <biological_entity constraint="cauline" id="o4313" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="20" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="15" />
      </biological_entity>
      <biological_entity id="o4314" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o4313" id="r318" modifier="proximally" name="often concealing" negation="false" src="d0_s6" to="o4314" />
    </statement>
    <statement id="d0_s7">
      <text>blade auricles 0.5–1.5 mm, surfaces of distalmost leaves usually glabrous.</text>
      <biological_entity constraint="cauline" id="o4315" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o4316" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4317" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o4318" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o4317" id="r319" name="part_of" negation="false" src="d0_s7" to="o4318" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes 10–30-flowered, usually unbranched.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels horizontal or slightly descending, curved or angled downward, 6–10 (–15) mm, usually glabrous, rarely sparsely pubescent, trichomes spreading, simple.</text>
      <biological_entity id="o4319" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="10-30-flowered" value_original="10-30-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o4320" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o4321" name="whole-organism" name_original="" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s9" value="descending" value_original="descending" />
        <character is_modifier="false" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s9" value="angled" value_original="angled" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="downward" value_original="downward" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o4322" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o4319" id="r320" name="fruiting" negation="false" src="d0_s9" to="o4320" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers divaricate-ascending at anthesis;</text>
      <biological_entity id="o4323" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s10" value="divaricate-ascending" value_original="divaricate-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals pubescent;</text>
      <biological_entity id="o4324" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals usually white, rarely pale lavender, 3–3.7 (–4) × 0.5–0.8 mm, glabrous;</text>
      <biological_entity id="o4325" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s12" value="white" value_original="white" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s12" value="pale lavender" value_original="pale lavender" />
        <character char_type="range_value" from="3.7" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s12" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollen ellipsoid.</text>
      <biological_entity id="o4326" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits pendent, not appressed to rachis, rarely slightly secund, straight or gently curved, edges parallel, 3.3–5.7 cm × 1.2–1.8 mm;</text>
      <biological_entity id="o4327" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="pendent" value_original="pendent" />
        <character constraint="to rachis" constraintid="o4328" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="rarely slightly" name="architecture" notes="" src="d0_s14" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="gently" name="course" src="d0_s14" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o4328" name="rachis" name_original="rachis" src="d0_s14" type="structure" />
      <biological_entity id="o4329" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="3.3" from_unit="cm" name="length" src="d0_s14" to="5.7" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s14" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves glabrous;</text>
      <biological_entity id="o4330" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 90–126 per ovary;</text>
      <biological_entity id="o4331" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o4332" from="90" name="quantity" src="d0_s16" to="126" />
      </biological_entity>
      <biological_entity id="o4332" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.1–0.4 mm.</text>
      <biological_entity id="o4333" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s17" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds biseriate, 0.7–0.9 × 0.5–0.6 mm;</text>
      <biological_entity id="o4334" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s18" value="biseriate" value_original="biseriate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="length" src="d0_s18" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s18" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>wing distal or, sometimes, absent, 0.05–0.1 mm wide.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 14.</text>
      <biological_entity id="o4335" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="position_or_shape" src="d0_s19" value="distal" value_original="distal" />
        <character name="position_or_shape" src="d0_s19" value="," value_original="," />
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s19" to="0.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4336" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="14" value_original="14" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and gravelly soil in sagebrush, pinyon-juniper woodlands, open conifer forests and subalpine meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="gravelly soil" constraint="in sagebrush" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="pinyon-juniper woodlands" />
        <character name="habitat" value="open conifer forests" />
        <character name="habitat" value="subalpine meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1800-2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="1800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo., N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>98.</number>
  <discussion>Boechera spatifolia is a sexual diploid that usually has been treated as a variety of Arabis (Boechera) fendleri but appears to be sufficiently distinct to warrant recognition at species level (see M. D. Windham and I. A. Al-Shehbaz 2006 for detailed comparison). There is little geographic overlap between the two, with B. spatifolia confined to the mountains of central Colorado and north-central New Mexico and B. fendleri ranging from western New Mexico and the Four Corners region through northern Arizona to southern Nevada.</discussion>
  
</bio:treatment>