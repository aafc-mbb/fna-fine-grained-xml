<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">248</other_info_on_meta>
    <other_info_on_meta type="treatment_page">250</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">alysseae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">alyssum</taxon_name>
    <taxon_name authority="Waldstein &amp; Kitaibel" date="1799" rank="species">murale</taxon_name>
    <place_of_publication>
      <publication_title>Descr. Icon. Pl. Hung.</publication_title>
      <place_in_publication>1: 5. 1799</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe alysseae;genus alyssum;species murale</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">250094818</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(cespitose, sometimes caudex woody);</text>
    </statement>
    <statement id="d0_s2">
      <text>canescent or not, trichomes stellate, 5–20-rayed.</text>
      <biological_entity id="o16361" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="canescent" value_original="canescent" />
        <character name="pubescence" src="d0_s2" value="not" value_original="not" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
      <biological_entity id="o16362" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s2" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems often several from caudex (sterile shoots absent or few), usually erect or ascending, (2.5–) 3–6 (–7) dm.</text>
      <biological_entity id="o16363" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character constraint="from caudex" constraintid="o16364" is_modifier="false" modifier="often" name="quantity" src="d0_s3" value="several" value_original="several" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="3" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="7" to_unit="dm" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s3" to="6" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o16364" name="caudex" name_original="caudex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves subsessile;</text>
      <biological_entity constraint="cauline" id="o16365" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade narrowly oblanceolate to linear, (0.5–) 0.7–2.5 (–3) cm × 1–5 (–8) mm (gradually smaller distally), base cuneate to attenuate, apex obtuse to subacute, (surfaces often more grayish abaxially than adaxially).</text>
      <biological_entity id="o16366" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s5" to="linear" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s5" to="0.7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16367" name="base" name_original="base" src="d0_s5" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s5" to="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>(Racemes corymbose, in panicles terminating each stem.) Fruiting pedicels divaricate-ascending, straight or slightly curved distally, slender, (2–) 2.5–6 (–8) mm, trichomes uniformly stellate.</text>
      <biological_entity id="o16368" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s5" to="subacute" />
      </biological_entity>
      <biological_entity id="o16369" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity id="o16370" name="whole-organism" name_original="" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly; distally" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character is_modifier="false" name="size" src="d0_s6" value="slender" value_original="slender" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16371" name="trichome" name_original="trichomes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="uniformly" name="arrangement_or_shape" src="d0_s6" value="stellate" value_original="stellate" />
      </biological_entity>
      <relation from="o16368" id="r1129" name="fruiting" negation="false" src="d0_s6" to="o16369" />
    </statement>
    <statement id="d0_s7">
      <text>Flowers: sepals oblong, 1.2–2 × 0.4–0.7 mm, stellate-pubescent;</text>
      <biological_entity id="o16372" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o16373" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s7" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals yellow, spatulate, 2.5–3.5 × 0.5–1 mm, base attenuate, apex often obtuse or rounded, glabrous abaxially;</text>
      <biological_entity id="o16374" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o16375" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s8" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s8" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16376" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o16377" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments: median pairs broadly winged, apically 1-toothed, lateral pair with lanceolate or narrowly oblong basal appendage, apically subacute, 1.5–2.5 mm;</text>
      <biological_entity id="o16378" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="median" value_original="median" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s9" value="1-toothed" value_original="1-toothed" />
        <character constraint="with basal appendage" constraintid="o16379" is_modifier="false" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="apically" name="shape" notes="" src="d0_s9" value="subacute" value_original="subacute" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o16379" name="appendage" name_original="appendage" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers oblong, 0.2–0.3 mm.</text>
      <biological_entity id="o16380" name="filament" name_original="filaments" src="d0_s10" type="structure" />
      <biological_entity id="o16381" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits broadly elliptic to orbicular, 3.5–5 × 2.5–5 mm, apex obtuse to rounded;</text>
      <biological_entity id="o16382" name="fruit" name_original="fruits" src="d0_s11" type="structure">
        <character char_type="range_value" from="broadly elliptic" name="shape" src="d0_s11" to="orbicular" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16383" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s11" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>valves not inflated, flattened and slightly undulate, stellate-pubescent;</text>
      <biological_entity id="o16384" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="shape" src="d0_s12" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="stellate-pubescent" value_original="stellate-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>ovules 1 per ovary;</text>
      <biological_entity id="o16385" name="ovule" name_original="ovules" src="d0_s13" type="structure">
        <character constraint="per ovary" constraintid="o16386" name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o16386" name="ovary" name_original="ovary" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>style (slender) 1–2 mm, sparsely pubescent.</text>
      <biological_entity id="o16387" name="style" name_original="style" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds suborbicular to broadly ovoid, compressed, 3–3.8 × 2–3.2 mm, margins winged, wing 0.5–0.9 mm wide.</text>
      <biological_entity id="o16388" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character char_type="range_value" from="suborbicular" name="shape" src="d0_s15" to="broadly ovoid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s15" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s15" to="3.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16389" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>2n = 16, 32.</text>
      <biological_entity id="o16390" name="wing" name_original="wing" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s15" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16391" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
        <character name="quantity" src="d0_s16" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste areas, roadsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste areas" />
        <character name="habitat" value="roadsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Ont., Que.; Colo., Oreg., Utah; Europe; sw Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6.</number>
  
</bio:treatment>