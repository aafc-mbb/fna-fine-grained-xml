<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">227</other_info_on_meta>
    <other_info_on_meta type="treatment_page">443</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">Buniadeae</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Mus. Hist. Nat.</publication_title>
      <place_in_publication>7: 245. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe Buniadeae</taxon_hierarchy>
    <other_info_on_name type="fna_id">20857</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals, biennials, or perennials;</text>
      <biological_entity id="o13999" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>glandular (glands multicellular on multiseriate stalks).</text>
      <biological_entity id="o14000" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character name="duration" value="biennial" value_original="biennial" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Trichomes stalked, forked, or simple.</text>
      <biological_entity id="o14002" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="stalked" value_original="stalked" />
        <character is_modifier="false" name="shape" src="d0_s2" value="forked" value_original="forked" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves sessile or subsessile [petiolate];</text>
      <biological_entity constraint="cauline" id="o14003" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade base not auriculate, margins dentate or entire.</text>
      <biological_entity constraint="blade" id="o14004" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s4" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o14005" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Racemes ebracteate, often elongated in fruit.</text>
      <biological_entity id="o14006" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o14007" is_modifier="false" modifier="often" name="length" src="d0_s5" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o14007" name="fruit" name_original="fruit" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Flowers actinomorphic;</text>
      <biological_entity id="o14008" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="actinomorphic" value_original="actinomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sepals spreading or ascending [erect], lateral pair not saccate basally;</text>
      <biological_entity id="o14009" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s7" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>petals yellow, claw usually present [absent], often distinct;</text>
      <biological_entity id="o14010" name="petal" name_original="petals" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o14011" name="claw" name_original="claw" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="often" name="fusion" src="d0_s8" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>filaments unappendaged [winged];</text>
      <biological_entity id="o14012" name="filament" name_original="filaments" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="unappendaged" value_original="unappendaged" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pollen 3-colpate.</text>
      <biological_entity id="o14013" name="pollen" name_original="pollen" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-colpate" value_original="3-colpate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Fruits silicles [siliques], indehiscent [dehiscent], unsegmented, terete or 4-angled [latiseptate];</text>
      <biological_entity constraint="fruits" id="o14014" name="silicle" name_original="silicles" src="d0_s11" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s11" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="unsegmented" value_original="unsegmented" />
        <character is_modifier="false" name="shape" src="d0_s11" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s11" value="4-angled" value_original="4-angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 2–4 [–numerous] per ovary;</text>
      <biological_entity id="o14015" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o14016" from="2" name="quantity" src="d0_s12" to="4" />
      </biological_entity>
      <biological_entity id="o14016" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>style obsolete or distinct;</text>
      <biological_entity id="o14017" name="style" name_original="style" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s13" value="obsolete" value_original="obsolete" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stigma entire [2-lobed].</text>
      <biological_entity id="o14018" name="stigma" name_original="stigma" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds aseriate [biseriate];</text>
      <biological_entity id="o14019" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="aseriate" value_original="aseriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>cotyledons spirolobal.</text>
      <biological_entity id="o14020" name="cotyledon" name_original="cotyledons" src="d0_s16" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s16" value="spirolobal" value_original="spirolobal" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Europe, Asia, Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Europe" establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="introduced" />
        <character name="distribution" value="Africa" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>g.</number>
  <discussion>Genus 1, species 2 (2 in the flora).</discussion>
  
</bio:treatment>