<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">174</other_info_on_meta>
    <other_info_on_meta type="mention_page">175</other_info_on_meta>
    <other_info_on_meta type="treatment_page">176</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">limnanthaceae</taxon_name>
    <taxon_name authority="R. Brown" date="unknown" rank="genus">limnanthes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Limnanthes</taxon_name>
    <taxon_name authority="Trelease" date="1888" rank="species">macounii</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Boston Soc. Nat. Hist.</publication_title>
      <place_in_publication>4: 85. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family limnanthaceae;genus limnanthes;section limnanthes;species macounii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250095081</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 2–7 (–15) cm.</text>
      <biological_entity id="o22596" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="7" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent (sometimes upcurved apically).</text>
      <biological_entity id="o22597" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 1–7 cm;</text>
      <biological_entity id="o22598" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 3–15, blade ovate, margins irregularly toothed to 5-lobed.</text>
      <biological_entity id="o22599" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="15" />
      </biological_entity>
      <biological_entity id="o22600" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o22601" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="irregularly toothed" name="shape" src="d0_s3" to="5-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers bowl to bell-shaped;</text>
      <biological_entity id="o22602" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character char_type="range_value" from="bowl" name="shape" src="d0_s4" to="bell-shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals (4) ovate, 3–4 mm;</text>
      <biological_entity id="o22603" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character name="atypical_quantity" src="d0_s5" value="4" value_original="4" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals (4) white, cuneate to obovate, 4–5 mm, apex emarginate (reflexed in fruit);</text>
      <biological_entity id="o22604" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character name="atypical_quantity" src="d0_s6" value="4" value_original="4" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s6" to="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22605" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 2–2.5 mm;</text>
      <biological_entity id="o22606" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers cream, 0.3 mm;</text>
      <biological_entity id="o22607" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="cream" value_original="cream" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 3 mm.</text>
      <biological_entity id="o22608" name="style" name_original="style" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Nutlets light-brown, 3 mm, tuberculate, tubercles light-brown, broadly conical.</text>
      <biological_entity id="o22609" name="nutlet" name_original="nutlets" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="light-brown" value_original="light-brown" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="relief" src="d0_s10" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n = 10.</text>
      <biological_entity id="o22610" name="tubercle" name_original="tubercles" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="conical" value_original="conical" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22611" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar-early May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Depressions in shallow soil on rocks, seepage areas, rocky coastal areas, open forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="depressions" constraint="in shallow soil on rocks , seepage areas , rocky coastal areas , open forests" />
        <character name="habitat" value="shallow soil" constraint="on rocks , seepage areas , rocky coastal areas , open forests" />
        <character name="habitat" value="rocks" />
        <character name="habitat" value="seepage areas" />
        <character name="habitat" value="rocky coastal areas" />
        <character name="habitat" value="open forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <other_name type="common_name">Macoun’s meadowfoam</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Limnanthes macounii is native on Vancouver Island and adjacent islands in British Columbia from East Sooke Park to Victoria, Inskip, Chatham, and Trial islands, to Yellow Point, Saltspring, Gabriola, and Hornby islands. It grows in seasonally moist depressions (including edges of vernal pools) in acidic soils that often have a high nutrient content. The population in California is in an agricultural field in San Mateo County; first discovered there in 1998 (E. G. Buxton 1998), it appears to be persisting. Phylogenetic analyses suggest this may be an aberrant population of L. douglasii (S. Meyers, pers. comm.)</discussion>
  <discussion>The angular and comparatively massive tubercles of the nutlets of Limnanthes macounii, with the base equaling 1/4–1/3 the width of the nutlet, are unique in the genus (H. H. Hauptli et al. 1978). Allozyme studies by R. V. Kesseli and S. K. Jain (1984b) showed that L. macounii had alleles found in no other taxa at three loci. M. S. Plotkin (1998) concluded that L. macounii should be included in L. douglasii based on ITS analysis. Because of its unique characteristics and highly disjunct distribution, L. macounii is maintained as a species here pending further study.</discussion>
  <references>
    <reference>Committee on the Status of Endangered Wildlife in Canada. 2004. COSEWIC assessment and update status report on the Macoun’s meadowfoam Limnanthes macounii in Canada. Ottawa.</reference>
  </references>
  
</bio:treatment>