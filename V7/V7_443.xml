<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">285</other_info_on_meta>
    <other_info_on_meta type="mention_page">320</other_info_on_meta>
    <other_info_on_meta type="treatment_page">326</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="R. Brown" date="1823" rank="species">pauciflora</taxon_name>
    <place_of_publication>
      <publication_title>Chlor. Melvill.,</publication_title>
      <place_in_publication>266. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species pauciflora</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094813</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="Ledebour" date="unknown" rank="species">adamsii</taxon_name>
    <taxon_hierarchy>genus Draba;species adamsii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o8001" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex branched (with persistent, thickened leaf midveins);</text>
    </statement>
    <statement id="d0_s2">
      <text>scapose.</text>
      <biological_entity id="o8002" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems unbranched, (0.05–) 0.1–0.8 dm, pubescent throughout, sometimes sparsely so distally, trichomes simple, 0.3–0.7 mm, and 2–4-rayed, 0.05–0.2 mm.</text>
      <biological_entity id="o8003" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.05" from_unit="dm" name="atypical_some_measurement" src="d0_s3" to="0.1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s3" to="0.8" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8004" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely; sparsely; distally" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s3" to="0.7" to_unit="mm" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s3" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves (not imbricate);</text>
    </statement>
    <statement id="d0_s5">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s6">
      <text>petiolate;</text>
      <biological_entity constraint="basal" id="o8005" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>petiole ciliate, (trichomes simple and 2-rayed, 0.4–1.3 mm);</text>
      <biological_entity id="o8006" name="petiole" name_original="petiole" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblanceolate, 0.5–1.1 cm × 1.5–4 mm, margins entire, (pubescent as petiole, apex acute to subacute, trichomes simple and/or branched), surfaces pubescent, abaxially with simple trichomes, to 1 mm, and stalked, 2–4-rayed ones, 0.1–0.5 mm, adaxially with simple trichomes, 0.4–1 mm, (sometimes glabrous).</text>
      <biological_entity id="o8007" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s8" to="1.1" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8008" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o8009" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8010" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o8011" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o8009" id="r585" modifier="abaxially" name="with" negation="false" src="d0_s8" to="o8010" />
      <relation from="o8009" id="r586" modifier="adaxially" name="with" negation="false" src="d0_s8" to="o8011" />
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 0.</text>
      <biological_entity constraint="cauline" id="o8012" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Racemes 2–8-flowered (congested), ebracteate, slightly elongated in fruit;</text>
      <biological_entity id="o8013" name="raceme" name_original="racemes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="2-8-flowered" value_original="2-8-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="ebracteate" value_original="ebracteate" />
        <character constraint="in fruit" constraintid="o8014" is_modifier="false" modifier="slightly" name="length" src="d0_s10" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o8014" name="fruit" name_original="fruit" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>rachis not flexuous, pubescent as stem.</text>
      <biological_entity id="o8016" name="stem" name_original="stem" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Fruiting pedicels divaricate to ascending, straight or slightly curved upward, 1.5–4 mm, pubescent as stem or trichomes branched.</text>
      <biological_entity id="o8015" name="rachis" name_original="rachis" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s11" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o8016" is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8017" name="pedicel" name_original="pedicels" src="d0_s12" type="structure" />
      <biological_entity id="o8018" name="stem" name_original="stem" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s12" value="curved" value_original="curved" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character constraint="as trichomes" constraintid="o8019" is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8019" name="trichome" name_original="trichomes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o8015" id="r587" name="fruiting" negation="false" src="d0_s12" to="o8017" />
    </statement>
    <statement id="d0_s13">
      <text>Flowers: sepals ovate, 1.8–2.3 mm, pubescent, (trichomes simple with fewer, short-stalked ones);</text>
      <biological_entity id="o8020" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o8021" name="sepal" name_original="sepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s13" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>petals pale-yellow, narrowly spatulate to oblanceolate, 2.5–3 × 0.8–1.5 mm;</text>
      <biological_entity id="o8022" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o8023" name="petal" name_original="petals" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="narrowly spatulate" name="shape" src="d0_s14" to="oblanceolate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s14" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers ovate, 0.2–0.3 mm.</text>
      <biological_entity id="o8024" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o8025" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s15" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits often obovate, plane, slightly flattened, 5–10 × (3–) 3.5–5 mm;</text>
      <biological_entity id="o8026" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s16" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="plane" value_original="plane" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s16" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s16" to="10" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s16" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves glabrate or sparsely pubescent, 0.05–0.2 mm;</text>
      <biological_entity id="o8027" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrate" value_original="glabrate" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s17" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules 8–16 (–20) per ovary;</text>
      <biological_entity id="o8028" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" from="16" from_inclusive="false" name="atypical_quantity" src="d0_s18" to="20" />
        <character char_type="range_value" constraint="per ovary" constraintid="o8029" from="8" name="quantity" src="d0_s18" to="16" />
      </biological_entity>
      <biological_entity id="o8029" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style 0.05–0.15 mm.</text>
      <biological_entity id="o8030" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s19" to="0.15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds ovoid, 1.1–1.6 × 0.7–1 mm. 2n = 32.</text>
      <biological_entity id="o8031" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s20" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s20" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8032" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp rocky slopes, tundra, swales, dry silt plains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" modifier="damp" />
        <character name="habitat" value="tundra" />
        <character name="habitat" value="swales" />
        <character name="habitat" value="dry silt plains" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; N.W.T., Nunavut; Alaska; Europe (Norway [Svalbard], Russia); e Asia (Russian Far East, n Siberia).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Europe (Norway [Svalbard])" establishment_means="native" />
        <character name="distribution" value="Europe (Russia)" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="e Asia (n Siberia)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>77.</number>
  <discussion>O. E. Schulz (1927) recognized five varieties within Draba pauciflora, of which four were listed from North America. Schulz’s concept of D. pauciflora encompassed multiple taxa that we recognize as separate species, including D. micropetala and D. subcapitata. C. L. Hitchcock (1941) and R. C. Rollins (1993) did not mention D. pauciflora; the latter (and G. A. Mulligan 1974b) referred the North American material to D. adamsii.</discussion>
  
</bio:treatment>