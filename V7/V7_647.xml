<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">431</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">crambe</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">maritima</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 671. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus crambe;species maritima</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250094636</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants succulent;</text>
      <biological_entity id="o40575" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="texture" src="d0_s0" value="succulent" value_original="succulent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots thick.</text>
      <biological_entity id="o40576" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems 2–5 (–7.5) dm, stout.</text>
      <biological_entity id="o40577" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="7.5" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="5" to_unit="dm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s2" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves (cabbagelike);</text>
      <biological_entity constraint="basal" id="o40578" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>petiole 4–16 cm;</text>
      <biological_entity id="o40579" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="16" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblong, or elliptic-ovate to ovate, 10–40 cm × 80–300 mm.</text>
      <biological_entity id="o40580" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic-ovate" name="shape" src="d0_s5" to="ovate" />
        <character char_type="range_value" from="elliptic-ovate" name="shape" src="d0_s5" to="ovate" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="40" to_unit="cm" />
        <character char_type="range_value" from="80" from_unit="mm" name="width" src="d0_s5" to="300" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves similar to basal, (proximal) blade margins irregularly pinnate or sinuate-dentate.</text>
      <biological_entity constraint="cauline" id="o40581" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="basal" id="o40582" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o40581" id="r2752" name="to" negation="false" src="d0_s6" to="o40582" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting pedicels (10–) 15–30 (–37) mm, stout.</text>
      <biological_entity constraint="blade" id="o40583" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="sinuate-dentate" value_original="sinuate-dentate" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="37" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s7" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o40584" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <relation from="o40583" id="r2753" name="fruiting" negation="false" src="d0_s7" to="o40584" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals 3–4 × 2–3.5 mm;</text>
      <biological_entity id="o40585" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o40586" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s8" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals (6–) 8–12 (–15) × (4–) 5–7 mm;</text>
      <biological_entity id="o40587" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o40588" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s9" to="8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="atypical_width" src="d0_s9" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s9" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 3–4 mm;</text>
      <biological_entity id="o40589" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o40590" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1–1.5 mm.</text>
      <biological_entity id="o40591" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o40592" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits: proximal segment 0.1–0.4 cm, slightly thicker than pedicel;</text>
      <biological_entity id="o40593" name="fruit" name_original="fruits" src="d0_s12" type="structure" />
      <biological_entity constraint="proximal" id="o40594" name="segment" name_original="segment" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s12" to="0.4" to_unit="cm" />
        <character constraint="than pedicel" constraintid="o40595" is_modifier="false" name="width" src="d0_s12" value="slightly thicker" value_original="slightly thicker" />
      </biological_entity>
      <biological_entity id="o40595" name="pedicel" name_original="pedicel" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>terminal segment 1-seeded, subglobose to ovoid, 0.7–1.2 (–1.4) cm × 6–8 mm, thick.</text>
      <biological_entity id="o40596" name="fruit" name_original="fruits" src="d0_s13" type="structure" />
      <biological_entity constraint="terminal" id="o40597" name="segment" name_original="segment" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-seeded" value_original="1-seeded" />
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s13" to="ovoid" />
        <character char_type="range_value" from="1.2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s13" to="1.4" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s13" to="1.2" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s13" to="8" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s13" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 4–5 (–6) mm.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 30, 60.</text>
      <biological_entity id="o40598" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s14" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o40599" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="30" value_original="30" />
        <character name="quantity" src="d0_s15" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal beaches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal beaches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Oreg.; Europe; n Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Sea-kale</other_name>
  <discussion>In the flora area, Crambe maritima has been reported only from Yaquina Head in Lincoln County; it was not treated by R. C. Rollins (1993).</discussion>
  
</bio:treatment>