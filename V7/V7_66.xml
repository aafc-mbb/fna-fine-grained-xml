<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">70</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Nasarow in V. L. Komarov et al." date="1936" rank="subgenus">chamaetia</taxon_name>
    <taxon_name authority="Seringe" date="1824" rank="section">herbella</taxon_name>
    <taxon_name authority="Trautvetter" date="1832" rank="species">rotundifolia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">rotundifolia</taxon_name>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus chamaetia;section herbella;species rotundifolia;variety rotundifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095322</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="(Chamisso) Coville" date="unknown" rank="species">leiocarpa</taxon_name>
    <taxon_hierarchy>genus Salix;species leiocarpa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Wahlenberg" date="unknown" rank="species">polaris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">leiocarpa</taxon_name>
    <taxon_hierarchy>genus Salix;species polaris;variety leiocarpa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.01–0.05 m.</text>
      <biological_entity id="o19614" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.01" from_unit="m" name="some_measurement" src="d0_s0" to="0.05" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves: petiole 0.5–2–4.6 (–5.5) mm;</text>
      <biological_entity id="o19615" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o19616" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="2-4.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s1" to="5.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s1" to="2-4.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>largest medial blade 4.5–8–16.3 × 3–5.9–10.5 mm, 0.92–1.23–2.27 (–2.53) times as long as wide.</text>
      <biological_entity id="o19617" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="largest medial" id="o19618" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s2" to="8-16.3" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="5.9-10.5" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="0.92-1.23-2.27(-2.53)" value_original="0.92-1.23-2.27(-2.53)" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Catkins: staminate subglobose or stout, 9.5–18.5 × 5–12 mm, flowering branchlet 1–9 mm;</text>
      <biological_entity id="o19619" name="catkin" name_original="catkins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s3" value="stout" value_original="stout" />
        <character char_type="range_value" from="9.5" from_unit="mm" name="length" src="d0_s3" to="18.5" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19620" name="branchlet" name_original="branchlet" src="d0_s3" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s3" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pistillate: (3–) 4–7–15 flowers, stout, subglobose, or globose, 13–35 × 6–17 mm, flowering branchlet 2–22 mm;</text>
      <biological_entity id="o19621" name="whole-organism" name_original="" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s4" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="7-15" />
      </biological_entity>
      <biological_entity id="o19622" name="flower" name_original="flowers" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s4" value="globose" value_original="globose" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" src="d0_s4" to="35" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s4" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19623" name="branchlet" name_original="branchlet" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="22" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>floral bract hairs usually wavy, some straight, curly, or crinkled, exceeding bract by 0.32–0.71–1.25 (–2.4) mm.</text>
      <biological_entity id="o19624" name="whole-organism" name_original="" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="bract" id="o19625" name="hair" name_original="hairs" src="d0_s5" type="structure" constraint_original="floral bract">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s5" value="curly" value_original="curly" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crinkled" value_original="crinkled" />
        <character is_modifier="false" name="shape" src="d0_s5" value="curly" value_original="curly" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crinkled" value_original="crinkled" />
      </biological_entity>
      <biological_entity id="o19626" name="bract" name_original="bract" src="d0_s5" type="structure" />
      <relation from="o19625" id="r1336" name="exceeding" negation="false" src="d0_s5" to="o19626" />
    </statement>
    <statement id="d0_s6">
      <text>Staminate flowers: abaxial nectary 0.5–0.6 mm.</text>
      <biological_entity id="o19627" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19628" name="nectary" name_original="nectary" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pistillate flowers: abaxial nectary present or absent, oblong or narrowly oblong, 0.9–2 mm;</text>
      <biological_entity id="o19629" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o19630" name="nectary" name_original="nectary" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>stigmas slenderly or broadly cylindrical, 0.36–0.47–0.6 mm.</text>
      <biological_entity id="o19631" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o19632" name="stigma" name_original="stigmas" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.36" from_unit="mm" name="some_measurement" src="d0_s8" to="0.47-0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Capsules 4–8.3 mm. 2n = 114.</text>
      <biological_entity id="o19633" name="capsule" name_original="capsules" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="8.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19634" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="114" value_original="114" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul-early Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Arctic-alpine, Dryas tundra and willow thickets along streams, sedge-grass tundra, sandy, saline coastal flats, beach cobbles, Empetrum-lichen heath on wet slopes, upland shrubby tundra, polygonal tundra and raised center polygons, snowbeds, scree and colluvial slopes, substrates from clay to coarse rubble, both calcareous limestone and acidic</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="arctic-alpine" />
        <character name="habitat" value="dryas tundra" />
        <character name="habitat" value="willow" />
        <character name="habitat" value="streams" modifier="thickets along" />
        <character name="habitat" value="sedge-grass tundra" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="coastal flats" modifier="saline" />
        <character name="habitat" value="saline" />
        <character name="habitat" value="beach cobbles" />
        <character name="habitat" value="empetrum-lichen heath" constraint="on wet slopes" />
        <character name="habitat" value="wet slopes" />
        <character name="habitat" value="upland shrubby tundra" />
        <character name="habitat" value="polygonal tundra" />
        <character name="habitat" value="raised center polygons" />
        <character name="habitat" value="snowbeds" />
        <character name="habitat" value="scree and colluvial slopes" />
        <character name="habitat" value="clay" modifier="substrates from" />
        <character name="habitat" value="coarse rubble" />
        <character name="habitat" value="calcareous" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Yukon; Alaska; e Asia (Chukotka, Russian Far East, Wrangel Island).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="e Asia (Chukotka)" establishment_means="native" />
        <character name="distribution" value="e Asia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="e Asia (Wrangel Island)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>31a.</number>
  <other_name type="common_name">Round-leaf or least willow</other_name>
  
</bio:treatment>