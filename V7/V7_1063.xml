<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="treatment_page">641</other_info_on_meta>
    <other_info_on_meta type="illustration_page">640</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(Hooker) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">gracilis</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 323. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species gracilis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094899</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Vesicaria</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="species">gracilis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Mag.</publication_title>
      <place_in_publication>63: plate 3533. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Vesicaria;species gracilis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alyssum</taxon_name>
    <taxon_name authority="(Hooker) Kuntze" date="unknown" rank="species">gracile</taxon_name>
    <taxon_hierarchy>genus Alyssum;species gracile;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="(Hooker) S. Watson" date="unknown" rank="species">gracilis</taxon_name>
    <taxon_hierarchy>genus Lesquerella;species gracilis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals or biennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(delicate, wiry);</text>
    </statement>
    <statement id="d0_s2">
      <text>with a fine taproot;</text>
      <biological_entity id="o3975" name="taproot" name_original="taproot" src="d0_s2" type="structure">
        <character is_modifier="true" name="width" src="d0_s2" value="fine" value_original="fine" />
      </biological_entity>
      <relation from="o3973" id="r291" name="with" negation="false" src="d0_s2" to="o3975" />
      <relation from="o3973" id="r292" name="with" negation="false" src="d0_s2" to="o3975" />
    </statement>
    <statement id="d0_s3">
      <text>pubescent, trichomes (sessile or subsessile), 4–7-rayed, rays distinct, usually furcate, occasionally bifurcate, (smooth to somewhat tuberculate).</text>
      <biological_entity id="o3973" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character name="duration" value="biennial" value_original="biennial" />
      </biological_entity>
      <biological_entity id="o3976" name="trichome" name_original="trichomes" src="d0_s3" type="structure" />
      <biological_entity id="o3977" name="ray" name_original="rays" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="furcate" value_original="furcate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s3" value="bifurcate" value_original="bifurcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems simple to several from base, erect, often outer decumbent, (unbranched or branched distally), 1–7 dm.</text>
      <biological_entity id="o3978" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character constraint="from base" constraintid="o3979" is_modifier="false" name="quantity" src="d0_s4" value="several" value_original="several" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often" name="position" src="d0_s4" value="outer" value_original="outer" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s4" value="decumbent" value_original="decumbent" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s4" to="7" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o3979" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade oblanceolate to elliptic, 1.5–8 (–11.5) cm, margins lyrate-pinnatifid to dentate or repand, (abaxial surface densely pubescent, adaxial sparsely pubescent).</text>
      <biological_entity constraint="basal" id="o3980" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o3981" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="elliptic" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s5" to="11.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3982" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="lyrate-pinnatifid" value_original="lyrate-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: (proximal petiolate, distal sessile);</text>
      <biological_entity constraint="cauline" id="o3983" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>blade oblanceolate to oblong, 1–7 cm, margins dentate to repand.</text>
      <biological_entity constraint="cauline" id="o3984" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o3985" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s7" to="oblong" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3986" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s7" to="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes loose, (elongated).</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels (usually divaricate-spreading, sometimes horizontal or shallowly recurved, straight or slightly curved), (7–) 10–20 (–25) mm, (slender or stout).</text>
      <biological_entity id="o3987" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s8" value="loose" value_original="loose" />
        <character char_type="range_value" from="7" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="25" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3988" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o3987" id="r293" name="fruiting" negation="false" src="d0_s9" to="o3988" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals elliptic or broadly ovate, 3–6.5 (–8) mm, (median pair slightly thickened apically, cucullate);</text>
      <biological_entity id="o3989" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3990" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals (yellow to orange), broadly obovate, 6–11 mm, (narrowing gradually to short claw).</text>
      <biological_entity id="o3991" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s11" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3992" name="petal" name_original="petals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits (stipitate or subsessile, gynophore 1–2 mm), globose, subglobose, obpyriform, or obovoid, not or slightly inflated, 3–9 mm;</text>
      <biological_entity id="o3993" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obpyriform" value_original="obpyriform" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obpyriform" value_original="obpyriform" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="not; slightly" name="shape" src="d0_s12" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>valves (not retaining seeds after dehiscence), glabrous throughout or sparsely pubescent inside;</text>
      <biological_entity id="o3994" name="valve" name_original="valves" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; sparsely" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>replum as wide as or wider than fruit;</text>
      <biological_entity id="o3995" name="replum" name_original="replum" src="d0_s14" type="structure">
        <character constraint="than fruit" constraintid="o3996" is_modifier="false" name="width" src="d0_s14" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o3996" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>ovules 8–20 (–28) per ovary;</text>
      <biological_entity id="o3997" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s15" to="28" />
        <character char_type="range_value" constraint="per ovary" constraintid="o3998" from="8" name="quantity" src="d0_s15" to="20" />
      </biological_entity>
      <biological_entity id="o3998" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style 2–4.5 mm.</text>
      <biological_entity id="o3999" name="style" name_original="style" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds slightly flattened.</text>
      <biological_entity id="o4000" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ill., Iowa, Kans., La., Miss., Mo., Okla., Tenn., Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>37.</number>
  <other_name type="common_name">Spreading bladderpod</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems to 7 dm; cauline leaves: blade margins usually deeply dentate, rarely repand; fruits ± sessile, globose or subglobose, 3-6 mm, bases rounded.</description>
      <determination>37a Physaria gracilis subsp. gracilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems usually less than 3 dm; cauline leaves: blade margins frequently repand, occasionally dentate; fruits stipitate (gynophore slender), obpyriform to narrowly obovoid, (5-)6-9 mm, bases truncate.</description>
      <determination>37b Physaria gracilis subsp. nuttallii</determination>
    </key_statement>
  </key>
</bio:treatment>