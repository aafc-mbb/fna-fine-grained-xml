<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">607</other_info_on_meta>
    <other_info_on_meta type="illustration_page">606</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="Harvey" date="1845" rank="genus">dithyrea</taxon_name>
    <taxon_name authority="Harvey" date="1845" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>London J. Bot.</publication_title>
      <place_in_publication>4: 77, plate 5. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus dithyrea;species californica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250095157</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Biscutella</taxon_name>
    <taxon_name authority="(Harvey) Bentham &amp; Hooker f. ex W. H. Brewer &amp; S. Watson" date="unknown" rank="species">californica</taxon_name>
    <taxon_hierarchy>genus Biscutella;species californica;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dithyrea</taxon_name>
    <taxon_name authority="J. F. Macbride &amp; Payson" date="unknown" rank="species">californica</taxon_name>
    <taxon_name authority="(J. F. Macbride &amp; Payson) Wiggins" date="unknown" rank="variety">clinata</taxon_name>
    <taxon_hierarchy>genus Dithyrea;species californica;variety clinata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dithyrea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">clinata</taxon_name>
    <taxon_hierarchy>genus Dithyrea;species clinata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o24939" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems few to several from base, erect, branched distally, (0.7–) 1–6 (–7) dm.</text>
      <biological_entity id="o24940" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="from base" constraintid="o24941" from="few" name="quantity" src="d0_s1" to="several" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="7" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s1" to="6" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o24941" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: petiole (0.5–) 1–4 (–5) cm;</text>
      <biological_entity constraint="basal" id="o24942" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o24943" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade lanceolate, oblanceolate, or ovate, (1.2–) 2–5 (–6) cm × (5–) 10–20 (–30) mm, base cuneate, margins usually dentate, sinuate, rarely pinnatifid.</text>
      <biological_entity constraint="basal" id="o24944" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o24945" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="atypical_length" src="d0_s3" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="5" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s3" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s3" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24946" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o24947" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves (distal) shortly petiolate or sessile;</text>
      <biological_entity constraint="cauline" id="o24948" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade ovate, lanceolate, or oblong, 1–4 (–5) cm × (3–) 10–20 (–30) mm, base cuneate, margins entire, dentate, or repand.</text>
      <biological_entity id="o24949" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_width" src="d0_s5" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24950" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o24951" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
        <character is_modifier="false" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Racemes considerably elongated in fruit.</text>
      <biological_entity id="o24953" name="fruit" name_original="fruit" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Fruiting pedicels 1.5–2.5 mm.</text>
      <biological_entity id="o24952" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character constraint="in fruit" constraintid="o24953" is_modifier="false" modifier="considerably" name="length" src="d0_s6" value="elongated" value_original="elongated" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24954" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <relation from="o24952" id="r1694" name="fruiting" negation="false" src="d0_s7" to="o24954" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals purple to lavender, linear, (6–) 7–9 (–10) × 0.8–1.2 mm;</text>
      <biological_entity id="o24955" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o24956" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character char_type="range_value" from="purple" name="coloration" src="d0_s8" to="lavender" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s8" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="10" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s8" to="9" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals white to pale lavender, (10–) 12–15 × (1.5–) 2–3 mm;</text>
      <biological_entity id="o24957" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o24958" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="pale lavender" />
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s9" to="12" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="atypical_width" src="d0_s9" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>filaments 6–7 mm;</text>
      <biological_entity id="o24959" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o24960" name="filament" name_original="filaments" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1.5–2 (–2.5) mm.</text>
      <biological_entity id="o24961" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o24962" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits: valves transversely ovate to suborbicular, 3.5–5 (–6) × 4–6 (–7) mm, base and apex rounded, pubescent, trichomes clavate, simple, papillate (branched trichomes rarely present on fruit margin);</text>
      <biological_entity id="o24963" name="fruit" name_original="fruits" src="d0_s12" type="structure" />
      <biological_entity id="o24964" name="valve" name_original="valves" src="d0_s12" type="structure">
        <character char_type="range_value" from="transversely ovate" name="shape" src="d0_s12" to="suborbicular" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24965" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24966" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24967" name="trichome" name_original="trichomes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="simple" value_original="simple" />
        <character is_modifier="false" name="relief" src="d0_s12" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>style 0.1–0.5 (–1) mm.</text>
      <biological_entity id="o24968" name="fruit" name_original="fruits" src="d0_s13" type="structure" />
      <biological_entity id="o24969" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds 3–4 × 1.5–2.5 mm. 2n = 20.</text>
      <biological_entity id="o24970" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24971" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering (depending on elevation) Mar–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="depending on elevation" to="Oct" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy deserts, sand dunes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy deserts" />
        <character name="habitat" value="sand dunes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Nev.; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Dithyrea californica is known in Arizona from Maricopa, Mohave, and Yuma counties and in Nevada from Clark and Lincoln counties. In material with immature fruits and lacking the proximal portion of stems, the species is readily distinguished from D. maritima by the presence of clavate trichomes, quite visible even on the ovaries of developing fruits. By contrast, D. maritima has branched instead of clavate trichomes.</discussion>
  
</bio:treatment>