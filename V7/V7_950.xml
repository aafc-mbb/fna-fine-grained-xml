<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">572</other_info_on_meta>
    <other_info_on_meta type="mention_page">588</other_info_on_meta>
    <other_info_on_meta type="treatment_page">587</other_info_on_meta>
    <other_info_on_meta type="illustration_page">586</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">lepidieae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">lepidium</taxon_name>
    <taxon_name authority="S. Watson" date="1871" rank="species">nanum</taxon_name>
    <place_of_publication>
      <publication_title>Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>30, plate 4, figs. 5–7. 1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe lepidieae;genus lepidium;species nanum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095160</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(forming pincushion-like, pulvinate mounds, caudex woody, to 1.5 cm diam., buried, much-branched, covered with persistent leaves);</text>
    </statement>
    <statement id="d0_s2">
      <text>puberulent.</text>
      <biological_entity id="o30245" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems simple from base (caudex branches), erect to ascending, unbranched distally, 0.05–0.2 dm.</text>
      <biological_entity id="o30246" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character constraint="from base" constraintid="o30247" is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="erect" name="orientation" notes="" src="d0_s3" to="ascending" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.05" from_unit="dm" name="some_measurement" src="d0_s3" to="0.2" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o30247" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves rosulate;</text>
      <biological_entity constraint="basal" id="o30248" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole undifferentiated;</text>
      <biological_entity id="o30249" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="undifferentiated" value_original="undifferentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade obovate, 2.5–5 cm × 15–25 (–35) mm, margins entire, (ciliolate), apex deeply 3-lobed (lobes ovate to suborbicular, margins entire).</text>
      <biological_entity id="o30250" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="25" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="35" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s6" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30251" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o30252" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s6" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves absent.</text>
      <biological_entity constraint="cauline" id="o30253" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes slightly elongated in fruit, (2–7-fruited);</text>
      <biological_entity id="o30254" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character constraint="in fruit" constraintid="o30255" is_modifier="false" modifier="slightly" name="length" src="d0_s8" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o30255" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>rachis puberulent, trichomes straight, cylindrical.</text>
      <biological_entity id="o30256" name="rachis" name_original="rachis" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels suberect to ascending, often straight, (terete), 2–4.5 × 0.2–0.3 mm, puberulent throughout.</text>
      <biological_entity id="o30257" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="suberect" name="orientation" src="d0_s10" to="ascending" />
        <character is_modifier="false" modifier="often" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s10" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o30258" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o30257" id="r2028" name="fruiting" negation="false" src="d0_s10" to="o30258" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers: sepals (tardily deciduous), obovate, 1.3–4 × 0.8–1.1 mm;</text>
      <biological_entity id="o30259" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s11" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30260" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>petals pale-yellow or creamy white, spatulate, 1.8–2.9 × 0.8–1.2 mm, claw 0.8–1.1 mm;</text>
      <biological_entity id="o30261" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o30262" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="creamy white" value_original="creamy white" />
        <character is_modifier="false" name="shape" src="d0_s12" value="spatulate" value_original="spatulate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s12" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30263" name="claw" name_original="claw" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s12" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>stamens 6;</text>
      <biological_entity id="o30264" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o30265" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>filaments 1.4–2 mm, (glabrous);</text>
      <biological_entity id="o30266" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o30267" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1.4–2 mm.</text>
      <biological_entity id="o30268" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o30269" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits ovate, 2–4.2 × 1.5–3 mm, often apically winged, apical notch 0.1–0.2 mm deep;</text>
      <biological_entity id="o30270" name="fruit" name_original="fruits" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s16" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s16" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="often apically" name="architecture" src="d0_s16" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity constraint="apical" id="o30271" name="notch" name_original="notch" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s16" to="0.2" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s16" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>valves thin, smooth, not veined, glabrous;</text>
      <biological_entity id="o30272" name="valve" name_original="valves" src="d0_s17" type="structure">
        <character is_modifier="false" name="width" src="d0_s17" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s17" value="veined" value_original="veined" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>style (0.4–) 0.6–1 (–1.2) mm, exserted beyond apical notch.</text>
      <biological_entity id="o30273" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="atypical_some_measurement" src="d0_s18" to="0.6" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s18" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s18" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o30274" name="notch" name_original="notch" src="d0_s18" type="structure" />
      <relation from="o30273" id="r2029" name="exserted beyond" negation="false" src="d0_s18" to="o30274" />
    </statement>
    <statement id="d0_s19">
      <text>Seeds oblong, 1–2 × 0.8–1 mm.</text>
      <biological_entity id="o30275" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s19" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gypsum knolls, tufa mounds around hotsprings, quartzite gravel, barren areas with shale and chalky soil, gravelly hillsides, white calcareous soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gypsum knolls" />
        <character name="habitat" value="tufa mounds" constraint="around hotsprings" />
        <character name="habitat" value="hotsprings" />
        <character name="habitat" value="gravel" modifier="quartzite" constraint="with shale and chalky soil , gravelly hillsides , white calcareous soils" />
        <character name="habitat" value="barren areas" constraint="with shale and chalky soil , gravelly hillsides , white calcareous soils" />
        <character name="habitat" value="shale" />
        <character name="habitat" value="chalky soil" />
        <character name="habitat" value="gravelly hillsides" />
        <character name="habitat" value="white calcareous soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1500-2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="1500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>26.</number>
  <discussion>Lepidium nanum is most common in Nevada and is known in Utah from collections in Tooele County.</discussion>
  
</bio:treatment>