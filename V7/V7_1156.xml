<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">672</other_info_on_meta>
    <other_info_on_meta type="mention_page">673</other_info_on_meta>
    <other_info_on_meta type="mention_page">675</other_info_on_meta>
    <other_info_on_meta type="treatment_page">674</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">smelowskieae</taxon_name>
    <taxon_name authority="C. A. Meyer in C. F. von Ledebour" date="unknown" rank="genus">smelowskia</taxon_name>
    <taxon_name authority="(W. H. Drury &amp; Rollins) Velichkin" date="1979" rank="species">media</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Zhurn. (Moscow &amp; Leningrad)</publication_title>
      <place_in_publication>64: 167. 1979</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe smelowskieae;genus smelowskia;species media</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094849</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Smelowskia</taxon_name>
    <taxon_name authority="(Stephan) C. A. Meyer" date="unknown" rank="species">calycina</taxon_name>
    <taxon_name authority="W. H. Drury &amp; Rollins" date="unknown" rank="variety">media</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>54: 100. 1952</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Smelowskia;species calycina;variety media;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants sometimes canescent basally;</text>
      <biological_entity id="o2744" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sometimes; basally" name="pubescence" src="d0_s0" value="canescent" value_original="canescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex branched.</text>
      <biological_entity id="o2745" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems: several from base, unbranched, (0.2–) 0.5–1.4 (–1.8) dm, trichomes simple, 0.5–1.2 mm, mixed with smaller, dendritic ones.</text>
      <biological_entity id="o2746" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o2747" is_modifier="false" name="quantity" src="d0_s2" value="several" value_original="several" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.5" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="1.8" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s2" to="1.4" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o2747" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o2748" name="trichome" name_original="trichomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="1.2" to_unit="mm" />
        <character constraint="with ones" constraintid="o2749" is_modifier="false" name="arrangement" src="d0_s2" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o2749" name="one" name_original="ones" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="smaller" value_original="smaller" />
        <character is_modifier="true" name="architecture" src="d0_s2" value="dendritic" value_original="dendritic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: petiole 0.7–3.5 cm, ciliate, trichomes simple;</text>
      <biological_entity constraint="basal" id="o2750" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o2751" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s3" to="3.5" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o2752" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade oblanceolate to obovate, or ovate to oblong in outline, (terminal segments linear, oblong, or ovate), 0.5–3 cm × 4–12 mm, (terminal segments 0.2–1.4 cm × 0.5–4 mm), margins usually pinnatifid, rarely apically 3 or 5-lobed, apex obtuse or subacute.</text>
      <biological_entity constraint="basal" id="o2753" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o2754" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s4" to="obovate or ovate" />
        <character char_type="range_value" constraint="in outline" constraintid="o2755" from="oblanceolate" name="shape" src="d0_s4" to="obovate or ovate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" notes="" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" notes="" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2755" name="outline" name_original="outline" src="d0_s4" type="structure" />
      <biological_entity id="o2756" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="rarely apically" name="shape" src="d0_s4" value="5-lobed" value_original="5-lobed" />
      </biological_entity>
      <biological_entity id="o2757" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s4" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves shortly petiolate or sessile;</text>
      <biological_entity constraint="cauline" id="o2758" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade similar to basal, smaller distally.</text>
      <biological_entity id="o2759" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes elongated in fruit.</text>
      <biological_entity id="o2761" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels usually spreading to divaricate, rarely divaricate-ascending, (often forming greater than 40˚ angle, straight or upcurved), proximalmost bracteate, 4–12 mm, pubescent, trichomes simple mixed with smaller, dendritic ones.</text>
      <biological_entity id="o2760" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o2761" is_modifier="false" name="length" src="d0_s7" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o2762" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <biological_entity id="o2763" name="whole-organism" name_original="" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s8" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character is_modifier="false" name="position" src="d0_s8" value="proximalmost" value_original="proximalmost" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="bracteate" value_original="bracteate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2764" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character constraint="with ones" constraintid="o2765" is_modifier="false" name="arrangement" src="d0_s8" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o2765" name="one" name_original="ones" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="smaller" value_original="smaller" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="dendritic" value_original="dendritic" />
      </biological_entity>
      <relation from="o2760" id="r222" name="fruiting" negation="false" src="d0_s8" to="o2762" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 2–3 mm;</text>
      <biological_entity id="o2766" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2767" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals white, suborbicular to obovate, 4–5 × 2–3 mm, narrowed to claw, 1.5–2 mm, apex rounded;</text>
      <biological_entity id="o2768" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2769" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character char_type="range_value" from="suborbicular" name="shape" src="d0_s10" to="obovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s10" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="3" to_unit="mm" />
        <character constraint="to claw" constraintid="o2770" is_modifier="false" name="shape" src="d0_s10" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2770" name="claw" name_original="claw" src="d0_s10" type="structure" />
      <biological_entity id="o2771" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers oblong, 0.5–0.6 mm.</text>
      <biological_entity id="o2772" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2773" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Fruits divaricate-ascending, ellipsoid to oblong, 4-angled, 5–11 × 2–3 mm, base and apex cuneate;</text>
      <biological_entity id="o2774" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s12" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s12" to="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="11" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2775" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o2776" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="cuneate" value_original="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>valves each with prominent midvein;</text>
      <biological_entity id="o2777" name="valve" name_original="valves" src="d0_s13" type="structure" />
      <biological_entity id="o2778" name="midvein" name_original="midvein" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o2777" id="r223" name="with" negation="false" src="d0_s13" to="o2778" />
    </statement>
    <statement id="d0_s14">
      <text>ovules 8–12 per ovary;</text>
      <biological_entity id="o2779" name="ovule" name_original="ovules" src="d0_s14" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o2780" from="8" name="quantity" src="d0_s14" to="12" />
      </biological_entity>
      <biological_entity id="o2780" name="ovary" name_original="ovary" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>style 0.1–0.5 mm.</text>
      <biological_entity id="o2781" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1.7–2.2 × 0.9–1.1 mm. 2n = 12.</text>
      <biological_entity id="o2782" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s16" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s16" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2783" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Talus scree slopes, dry calcareous gravel flats, limestone scree, tundra turf, seepages, bluffs, stony slopes, grassy quartzite slopes, gravelly lake shores</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="talus scree slopes" />
        <character name="habitat" value="dry calcareous" />
        <character name="habitat" value="gravel flats" />
        <character name="habitat" value="limestone scree" />
        <character name="habitat" value="tundra turf" />
        <character name="habitat" value="seepages" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="stony slopes" />
        <character name="habitat" value="grassy quartzite slopes" />
        <character name="habitat" value="gravelly lake shores" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>400-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="400" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Yukon; Alaska.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <discussion>Smelowskia media resembles S. americana in aspects of habit, flowers, and fruits. It differs by having non-appressed (versus appressed) fruits, fruiting pedicels held at wider angles (greater than 40˚ versus less), and somewhat larger seeds (1.7–2.3 × 0.9–1.2 mm versus 1.1–1.9 × 0.6–0.9 mm).</discussion>
  
</bio:treatment>