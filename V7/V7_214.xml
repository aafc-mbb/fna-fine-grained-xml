<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">175</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="mention_page">179</other_info_on_meta>
    <other_info_on_meta type="treatment_page">176</other_info_on_meta>
    <other_info_on_meta type="illustration_page">177</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="R. Brown" date="unknown" rank="family">limnanthaceae</taxon_name>
    <taxon_name authority="R. Brown" date="unknown" rank="genus">limnanthes</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Limnanthes</taxon_name>
    <taxon_name authority="R. Brown" date="unknown" rank="species">douglasii</taxon_name>
    <place_of_publication>
      <publication_title>London Edinburgh Philos. Mag. &amp; J. Sci.</publication_title>
      <place_in_publication>3: 71. 1833</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family limnanthaceae;genus limnanthes;section limnanthes;species douglasii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250095082</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–35 (–100) cm.</text>
      <biological_entity id="o29160" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending.</text>
      <biological_entity id="o29161" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 3–7 (–25 cm);</text>
      <biological_entity id="o29162" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaflets 5–13, blade linear, ovate, or wide-ovate, margins usually toothed or lobed, sometimes entire.</text>
      <biological_entity id="o29163" name="leaflet" name_original="leaflets" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s3" to="13" />
      </biological_entity>
      <biological_entity id="o29164" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="wide-ovate" value_original="wide-ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="wide-ovate" value_original="wide-ovate" />
      </biological_entity>
      <biological_entity id="o29165" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s3" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers cup, bell, or funnel-shaped;</text>
      <biological_entity constraint="flowers" id="o29166" name="cup" name_original="cup" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="bell" value_original="bell" />
        <character is_modifier="false" name="shape" src="d0_s4" value="funnel--shaped" value_original="funnel--shaped" />
        <character is_modifier="false" name="shape" src="d0_s4" value="bell" value_original="bell" />
        <character is_modifier="false" name="shape" src="d0_s4" value="funnel--shaped" value_original="funnel--shaped" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sepals narrowly lanceolate-ovate, lanceolate, or linear-lanceolate, 4–10 mm (glabrous or ciliate);</text>
      <biological_entity id="o29167" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="lanceolate-ovate" value_original="lanceolate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals white, yellow, or yellow with white-tips (sometimes greenish yellow basally), cuneate, obovate, spatulate, or obovate-spatulate, 7–18 mm, apex usually emarginate, sometimes truncate or obcordate;</text>
      <biological_entity id="o29168" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character constraint="with white-tips" constraintid="o29169" is_modifier="false" name="coloration" src="d0_s6" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" notes="" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate-spatulate" value_original="obovate-spatulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obovate-spatulate" value_original="obovate-spatulate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s6" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29169" name="white-tip" name_original="white-tips" src="d0_s6" type="structure" />
      <biological_entity id="o29170" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s6" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="obcordate" value_original="obcordate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 2–7 mm;</text>
      <biological_entity id="o29171" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers usually cream or yellow, sometimes dark-pink, orange-red, or nearly black, 0.8–1.5 mm;</text>
      <biological_entity id="o29172" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s8" value="cream" value_original="cream" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s8" value="dark-pink" value_original="dark-pink" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s8" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="orange-red" value_original="orange-red" />
        <character is_modifier="false" modifier="nearly" name="coloration" src="d0_s8" value="black" value_original="black" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 3–8 mm.</text>
      <biological_entity id="o29173" name="style" name_original="style" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Nutlets dark-brown, black, or gray, 2–5 mm, smooth, ridged, or tuberculate (at least apically), tubercles or ridges brown, pinkish, gray, or whitish, lamellar, planar, conical, rounded, or blunt.</text>
      <biological_entity id="o29174" name="nutlet" name_original="nutlets" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="black" value_original="black" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="gray" value_original="gray" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ridged" value_original="ridged" />
        <character is_modifier="false" name="relief" src="d0_s10" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity id="o29175" name="tubercle" name_original="tubercles" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s10" value="lamellar" value_original="lamellar" />
        <character is_modifier="false" name="shape" src="d0_s10" value="planar" value_original="planar" />
        <character is_modifier="false" name="shape" src="d0_s10" value="conical" value_original="conical" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity id="o29176" name="ridge" name_original="ridges" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pinkish" value_original="pinkish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="gray" value_original="gray" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s10" value="lamellar" value_original="lamellar" />
        <character is_modifier="false" name="shape" src="d0_s10" value="planar" value_original="planar" />
        <character is_modifier="false" name="shape" src="d0_s10" value="conical" value_original="conical" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s10" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="past_name">douglassii</other_name>
  <discussion>Subspecies 5 (5 in the flora).</discussion>
  <discussion>Limnanthes douglasii has five subspecies with core distributions as follows: subsp. sulphurea with two populations near the coast in Marin County and in southwestern San Mateo County; subsp. douglasii primarily coastal and Outer Coast Ranges with outlying populations in the northern Sierra Nevada foothills; subsp. nivea primarily Inner Coast Ranges and Bay Area with Central Valley and northern Sierra Nevada foothills populations; subsp. rosea in the Inner Coast Ranges, Central Valley, and Sierra Nevada foothills; and subsp. striata in the central Sierra Nevada foothills with outlying populations in Trinity County.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers funnel-shaped; petals white with greenish yellow bases (veins usually dark); anthers usually cream, rarely dark.</description>
      <determination>4d Limnanthes douglasii subsp. striata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers cup- or bell-shaped; petals yellow and/or white (veins rose, white, yellow, or purplish, not dark); anthers cream, yellow, dark pink, orange-red, reddish brown, or nearly black</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals yellow or yellow with white tips</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals white (sometimes aging or drying pale pink or yellowish)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals yellow with white tips; anthers cream, yellow, or reddish brown.</description>
      <determination>4a Limnanthes douglasii subsp. douglasii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals yellow; anthers yellow.</description>
      <determination>4b Limnanthes douglasii subsp. sulphurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals 7-13 mm (veins white or purplish); anthers cream to yellow; sepals 4-7 mm.</description>
      <determination>4c Limnanthes douglasii subsp. nivea</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petals 12-18 mm (veins rose); anthers cream, yellow, dark pink, orange-red, or nearly black; sepals 7-8 mm.</description>
      <determination>4e Limnanthes douglasii subsp. rosea</determination>
    </key_statement>
  </key>
</bio:treatment>