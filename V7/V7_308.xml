<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker,Hugh H. Iltis</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="treatment_page">220</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1838" rank="genus">HEMISCOLA</taxon_name>
    <place_of_publication>
      <publication_title>Sylva Tellur.,</publication_title>
      <place_in_publication>111. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus HEMISCOLA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek, hemi- , half, and skolios, curved, alluding to seed shape</other_info_on_name>
    <other_info_on_name type="fna_id">115062</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual [perennial].</text>
      <biological_entity id="o24486" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems (sometimes ± procumbent, weak), sparsely to moderately (or delicately) branched;</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous or glandular-pubescent.</text>
      <biological_entity id="o24487" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="sparsely to moderately" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipular spines present (recurved);</text>
      <biological_entity id="o24488" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="stipular" id="o24489" name="spine" name_original="spines" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole with pulvinus basally or distally;</text>
      <biological_entity id="o24490" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o24491" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o24492" name="pulvinu" name_original="pulvinus" src="d0_s4" type="structure" />
      <relation from="o24491" id="r1672" modifier="distally" name="with" negation="false" src="d0_s4" to="o24492" />
    </statement>
    <statement id="d0_s5">
      <text>leaflets (1 or) 3.</text>
      <biological_entity id="o24493" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o24494" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary (from distal leaves), racemes (flat-topped or elongated);</text>
      <biological_entity id="o24495" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o24496" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present.</text>
      <biological_entity id="o24497" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers (often appearing unisexual due to incomplete development), zygomorphic;</text>
      <biological_entity id="o24498" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="zygomorphic" value_original="zygomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sepals persistent or deciduous, distinct, equal (each often subtending a nectary);</text>
      <biological_entity id="o24499" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="duration" src="d0_s9" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s9" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="fusion" src="d0_s9" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals oblong to ovate, equal;</text>
      <biological_entity id="o24500" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="ovate" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens 6;</text>
      <biological_entity id="o24501" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="6" value_original="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments inserted on a discoid or conical gynophore, glabrous;</text>
      <biological_entity id="o24502" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24503" name="gynophore" name_original="gynophore" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="discoid" value_original="discoid" />
        <character is_modifier="true" name="shape" src="d0_s12" value="conical" value_original="conical" />
      </biological_entity>
      <relation from="o24502" id="r1673" name="inserted on a" negation="false" src="d0_s12" to="o24503" />
    </statement>
    <statement id="d0_s13">
      <text>anthers (ellipsoid to linear), coiling as pollen released;</text>
      <biological_entity id="o24504" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character constraint="as pollen" constraintid="o24505" is_modifier="false" name="shape" src="d0_s13" value="coiling" value_original="coiling" />
      </biological_entity>
      <biological_entity id="o24505" name="pollen" name_original="pollen" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>gynophore reflexed in fruit.</text>
      <biological_entity id="o24506" name="gynophore" name_original="gynophore" src="d0_s14" type="structure">
        <character constraint="in fruit" constraintid="o24507" is_modifier="false" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o24507" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Fruits capsules, dehiscent, fusiform to linear-cylindric.</text>
      <biological_entity constraint="fruits" id="o24508" name="capsule" name_original="capsules" src="d0_s15" type="structure">
        <character is_modifier="false" name="dehiscence" src="d0_s15" value="dehiscent" value_original="dehiscent" />
        <character char_type="range_value" from="fusiform" name="shape" src="d0_s15" to="linear-cylindric" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 10–20, oblong or obovoid, prominently arillate, (cleft fused between ends).</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 10.</text>
      <biological_entity id="o24509" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s16" to="20" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s16" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity constraint="x" id="o24510" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mexico, Central America, South America (south to Argentina).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mexico" establishment_means="introduced" />
        <character name="distribution" value="Central America" establishment_means="introduced" />
        <character name="distribution" value="South America (south to Argentina)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <other_name type="common_name">Spiderflower</other_name>
  <discussion>species 6 (2 in the flora)</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet blade lanceolate-elliptic to ovate or rhombic; sepals lanceolate; anthers 2.5-4 mm; capsules (15-)25-40(-65) mm.</description>
      <determination>1 Hemiscola aculeata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet blade obovate; sepals ovate; anthers 0.3-0.5 mm; capsules 15-20 mm.</description>
      <determination>2 Hemiscola diffusa</determination>
    </key_statement>
  </key>
</bio:treatment>