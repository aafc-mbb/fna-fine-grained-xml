<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker,Staria S. Vanderpool</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">195</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="mention_page">205</other_info_on_meta>
    <other_info_on_meta type="mention_page">222</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="treatment_page">199</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">CLEOMACEAE</taxon_name>
    <taxon_hierarchy>family CLEOMACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10199</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs or shrubs, annual or perennial (usually deciduous, evergreen in Peritoma arborea);</text>
      <biological_entity id="o7324" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>spines usually absent (present in Hemiscola and Tarenaya);</text>
    </statement>
    <statement id="d0_s2">
      <text>glabrous or glandular-pubescent, hairs stalked or sessile (producing glucosinolates).</text>
      <biological_entity id="o7326" name="spine" name_original="spines" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s1" value="absent" value_original="absent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o7327" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="stalked" value_original="stalked" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems usually erect, sometimes spreading or procumbent;</text>
    </statement>
    <statement id="d0_s4">
      <text>branched or unbranched.</text>
      <biological_entity id="o7328" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="growth_form" src="d0_s3" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves alternate, spirally arranged (usually palmately compound, sometimes simple);</text>
    </statement>
    <statement id="d0_s6">
      <text>venation pinnate;</text>
      <biological_entity id="o7329" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>stipules usually present (usually caducous, sometimes deciduous, 3–8-palmatifid, linear, threadlike, minute, scalelike, or absent, nodal (stipular) spines present in Tarenaya and Hemiscola);</text>
      <biological_entity id="o7330" name="stipule" name_original="stipules" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s7" value="caducous" value_original="caducous" />
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s7" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="shape" src="d0_s7" value="3-8-palmatifid" value_original="3-8-palmatifid" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s7" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s7" value="thread-like" value_original="threadlike" />
        <character is_modifier="false" name="size" src="d0_s7" value="minute" value_original="minute" />
        <character is_modifier="false" name="shape" src="d0_s7" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="nodal" id="o7331" name="spine" name_original="spines" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>petiole present (pulvinus usually present, nectaries absent, petiolar spines sometimes present, petiolules present);</text>
      <biological_entity id="o7332" name="petiole" name_original="petiole" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blade margins entire, serrate, or serrulate.</text>
      <biological_entity constraint="blade" id="o7333" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Inflorescences terminal or axillary, usually racemose, sometimes flat-topped, or flowers solitary (usually elongated in fruit);</text>
      <biological_entity id="o7334" name="inflorescence" name_original="inflorescences" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s10" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s10" value="racemose" value_original="racemose" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s10" value="flat-topped" value_original="flat-topped" />
      </biological_entity>
      <biological_entity id="o7335" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bud-scales absent;</text>
      <biological_entity id="o7336" name="bud-scale" name_original="bud-scales" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bracts present or absent (unifoliate, often trifoliate proximally, bracteoles absent).</text>
      <biological_entity id="o7337" name="bract" name_original="bracts" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pedicels present.</text>
      <biological_entity id="o7338" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Flowers usually bisexual (developmentally unisexual within sections of racemes), actinomorphic or slightly zygomorphic, rotate to crateriform, campanulate, or urceolate;</text>
      <biological_entity id="o7339" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="actinomorphic" value_original="actinomorphic" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s14" value="zygomorphic" value_original="zygomorphic" />
        <character char_type="range_value" from="rotate" name="shape" src="d0_s14" to="crateriform campanulate or urceolate" />
        <character char_type="range_value" from="rotate" name="shape" src="d0_s14" to="crateriform campanulate or urceolate" />
        <character char_type="range_value" from="rotate" name="shape" src="d0_s14" to="crateriform campanulate or urceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>perianth and androecium hypogynous;</text>
      <biological_entity id="o7340" name="perianth" name_original="perianth" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
      <biological_entity id="o7341" name="androecium" name_original="androecium" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="hypogynous" value_original="hypogynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>sepals persistent or deciduous, 4, distinct or connate basally;</text>
      <biological_entity id="o7342" name="sepal" name_original="sepals" src="d0_s16" type="structure">
        <character is_modifier="false" name="duration" src="d0_s16" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s16" value="deciduous" value_original="deciduous" />
        <character name="quantity" src="d0_s16" value="4" value_original="4" />
        <character is_modifier="false" name="fusion" src="d0_s16" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s16" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>petals 4, attached directly to receptacle, imbricate, distinct, equal or unequal;</text>
      <biological_entity id="o7343" name="petal" name_original="petals" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="4" value_original="4" />
        <character constraint="to receptacle" constraintid="o7344" is_modifier="false" name="fixation" src="d0_s17" value="attached" value_original="attached" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s17" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="fusion" src="d0_s17" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s17" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s17" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity id="o7344" name="receptacle" name_original="receptacle" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>intrastaminal nectary-discs, scales, or glands present or absent;</text>
      <biological_entity constraint="intrastaminal" id="o7345" name="nectary-disc" name_original="nectary-discs" src="d0_s18" type="structure" />
      <biological_entity id="o7346" name="scale" name_original="scales" src="d0_s18" type="structure" />
      <biological_entity id="o7347" name="gland" name_original="glands" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>stamens [4–] 6–27 [–35];</text>
      <biological_entity id="o7348" name="stamen" name_original="stamens" src="d0_s19" type="structure">
        <character char_type="range_value" from="4" name="atypical_quantity" src="d0_s19" to="6" to_inclusive="false" />
        <character char_type="range_value" from="27" from_inclusive="false" name="atypical_quantity" src="d0_s19" to="35" />
        <character char_type="range_value" from="6" name="quantity" src="d0_s19" to="27" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>filaments free or basally adnate to gynophore (or along proximal 1/3–1/2 in Gynandropsis) or androgynophore, glabrous or pubescent;</text>
      <biological_entity id="o7349" name="filament" name_original="filaments" src="d0_s20" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s20" value="free" value_original="free" />
        <character constraint="to androgynophore" constraintid="o7351" is_modifier="false" modifier="basally" name="fusion" src="d0_s20" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o7350" name="gynophore" name_original="gynophore" src="d0_s20" type="structure" />
      <biological_entity id="o7351" name="androgynophore" name_original="androgynophore" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>anthers dehiscing by longitudinal slits, pollen shed in single grains, binucleate, commonly tricolporate;</text>
      <biological_entity id="o7352" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character constraint="by slits" constraintid="o7353" is_modifier="false" name="dehiscence" src="d0_s21" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o7353" name="slit" name_original="slits" src="d0_s21" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s21" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o7354" name="pollen" name_original="pollen" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s21" value="binucleate" value_original="binucleate" />
        <character is_modifier="false" modifier="commonly" name="architecture" src="d0_s21" value="tricolporate" value_original="tricolporate" />
      </biological_entity>
      <biological_entity id="o7355" name="grain" name_original="grains" src="d0_s21" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s21" value="single" value_original="single" />
      </biological_entity>
      <relation from="o7354" id="r530" name="shed in" negation="false" src="d0_s21" to="o7355" />
    </statement>
    <statement id="d0_s22">
      <text>gynophore present or absent;</text>
      <biological_entity id="o7356" name="gynophore" name_original="gynophore" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>pistil 1;</text>
      <biological_entity id="o7357" name="pistil" name_original="pistil" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>ovary 1-carpellate (except 2 in Oxystylis), 2-locular;</text>
    </statement>
    <statement id="d0_s25">
      <text>placentation parietal;</text>
      <biological_entity id="o7358" name="ovary" name_original="ovary" src="d0_s24" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s24" value="1-carpellate" value_original="1-carpellate" />
        <character is_modifier="false" name="architecture_or_structure_in_adjective_form" src="d0_s24" value="2-locular" value_original="2-locular" />
        <character is_modifier="false" name="placentation" src="d0_s25" value="parietal" value_original="parietal" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>ovules 1–18 (–26+) per locule, anatropous, bitegmic;</text>
      <biological_entity id="o7359" name="ovule" name_original="ovules" src="d0_s26" type="structure">
        <character char_type="range_value" from="18" from_inclusive="false" name="atypical_quantity" src="d0_s26" to="26" upper_restricted="false" />
        <character char_type="range_value" constraint="per locule" constraintid="o7360" from="1" name="quantity" src="d0_s26" to="18" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s26" value="anatropous" value_original="anatropous" />
        <character is_modifier="false" name="architecture" src="d0_s26" value="bitegmic" value_original="bitegmic" />
      </biological_entity>
      <biological_entity id="o7360" name="locule" name_original="locule" src="d0_s26" type="structure" />
    </statement>
    <statement id="d0_s27">
      <text>style 1 (straight, relatively short, thick, not spinelike in fruit, except in Oxystylis, sometimes in Wislizenia);</text>
      <biological_entity id="o7361" name="style" name_original="style" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s28">
      <text>stigma 1, capitate, unlobed.</text>
      <biological_entity id="o7362" name="stigma" name_original="stigma" src="d0_s28" type="structure">
        <character name="quantity" src="d0_s28" value="1" value_original="1" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s28" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="shape" src="d0_s28" value="unlobed" value_original="unlobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s29">
      <text>Fruits capsular or nutlets (usually stipitate from elongation of gynophore, erect to divergent, usually not inflated), valvate, elongate (± dehiscent by 2 lateral valves, except in Polanisia), or schizocarps (inflated in Peritoma arborea), indehiscent or dehiscent.</text>
      <biological_entity id="o7363" name="fruit" name_original="fruits" src="d0_s29" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s29" value="capsular" value_original="capsular" />
        <character is_modifier="false" name="arrangement_or_dehiscence" notes="" src="d0_s29" value="valvate" value_original="valvate" />
        <character is_modifier="false" name="shape" src="d0_s29" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="dehiscence" src="d0_s29" value="indehiscent" value_original="indehiscent" />
        <character is_modifier="false" name="dehiscence" src="d0_s29" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
      <biological_entity id="o7364" name="nutlet" name_original="nutlets" src="d0_s29" type="structure" />
      <biological_entity id="o7365" name="schizocarp" name_original="schizocarps" src="d0_s29" type="structure" />
    </statement>
    <statement id="d0_s30">
      <text>Seeds 1–65 [–200], tan, yellowish-brown, light-brown, pale green, brown, reddish-brown, silver-gray, or gray to black (papillose or tuberculate);</text>
    </statement>
    <statement id="d0_s31">
      <text>arillate or not;</text>
      <biological_entity id="o7366" name="seed" name_original="seeds" src="d0_s30" type="structure">
        <character char_type="range_value" from="65" from_inclusive="false" name="atypical_quantity" src="d0_s30" to="200" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s30" to="65" />
        <character is_modifier="false" name="coloration" src="d0_s30" value="tan" value_original="tan" />
        <character is_modifier="false" name="coloration" src="d0_s30" value="yellowish-brown" value_original="yellowish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s30" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s30" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="coloration" src="d0_s30" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s30" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s30" to="black" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s30" to="black" />
        <character is_modifier="false" name="architecture" src="d0_s31" value="arillate" value_original="arillate" />
        <character name="architecture" src="d0_s31" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s32">
      <text>endosperm scanty or absent, persistent perisperm sometimes present;</text>
      <biological_entity id="o7367" name="endosperm" name_original="endosperm" src="d0_s32" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s32" value="scanty" value_original="scanty" />
        <character is_modifier="false" name="presence" src="d0_s32" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o7368" name="perisperm" name_original="perisperm" src="d0_s32" type="structure">
        <character is_modifier="true" name="duration" src="d0_s32" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s32" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s33">
      <text>cotyledons incumbent, (radicle-hypocotyl elongated).</text>
      <biological_entity id="o7369" name="cotyledon" name_original="cotyledons" src="d0_s33" type="structure">
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s33" value="incumbent" value_original="incumbent" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide; tropical and temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
        <character name="distribution" value="tropical and temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>0</number>
  <other_name type="common_name">Spiderflower Family</other_name>
  <discussion>Genera 17, species ca. 150 (12 genera, 34 species in the flora).</discussion>
  <discussion>A discussion of the status of Cleomaceae and its segregation from Capparaceae (in the narrow sense) appears under the latter. Throughout this treatment, style length indicates the length in fruit; in some species, the style elongates after anthesis. The key to genera, in some cases, include characteristics of species, in addition to those of genera. This circumscription of Cleomaceae includes Oxystylidaceae Hutchinson.</discussion>
  <references>
    <reference>Ernst, W. R. 1963b. The genera of Capparidaceae and Moringaceae in the southeastern United States. J. Arnold Arbor. 44: 81–95.</reference>
    <reference>Hall, J. C., H. H. Iltis, and K. J. Sytsma. 2004. Molecular phylogenetics of core Brassicales, placement of orphan genera Emblingia, Forchhammeria, Tirania, and character evolution. Syst. Bot. 29: 654–669.</reference>
    <reference>Hall, J. C., K. J. Sytsma, and H. H. Iltis. 2002. Phylogeny of Capparaceae and Brassicaceae based on chloroplast sequence data. Amer. J. Bot. 89: 1826–1842.</reference>
    <reference>Holmgren, P. K. and A. Cronquist. 2005. Cleomaceae. In: A. Cronquist et al. 1972+. Intermountain Flora. Vascular Plants of the Intermountain West, U.S.A. 6+ vols. in 7+. New York and London. Vol. 2, part B, pp. 160–174.</reference>
    <reference>Iltis, H. H. 1952. A Revision of the Genus Cleome in the New World. Ph.D. dissertation. Washington University.</reference>
    <reference>Iltis, H. H. 1960. Studies in the Capparidaceae. VII. Old World cleomes adventive in the New World. Brittonia 12: 279–294.</reference>
    <reference>Kers, L. E. 2003. Capparaceae. In: K. Kubitzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 9+ vols. Berlin etc. Vol. 5, pp. 36–56.</reference>
    <reference>Vanderpool, S. S., W. J. Elisens, and J. R. Estes. 1991. Pattern, tempo, and mode of evolutionary and biogeographic divergence in Oxystylis and Wislizenia (Capparaceae). Amer. J. Bot. 78: 925–937.</reference>
    <reference>Woodson, R. E. Jr. 1948. Gynandropsis, Cleome, and Podandrogyne. Ann. Missouri Bot. Gard. 35: 139–148.</reference>
    <reference>Iltis, H. H. 1957. Studies in the Capparidaceae. III. Evolution and phylogeny of the western North American Cleomoideae. Ann. Missouri Bot. Gard. 44: 77–119.</reference>
    <reference>Acebo, L. 2005. A phylogenetic study of the New World Cleome (Brassicaceae, Cleomoideae). Ann. Missouri Bot. Gard. 92: 179–201.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs (evergreen); leaflets 3; fruits inflated or not.</description>
      <determination>2 Peritoma</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs; leaflets 1-9; fruits usually not inflated</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences axillary, racemes (flat-topped); style spinelike in fruit.</description>
      <determination>6 Oxystylis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences terminal or axillary (from distal leaves), racemes or corymbs, or flowers solitary; style not spinelike in fruit (except sometimes in Wislizenia)</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stamens 8-32; gynophore usually 0-14 mm in fruit; fruits dehiscent in distal 1/2.</description>
      <determination>1 Polanisia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stamens 6 (except Arivela with 14-25); gynophore usually 0.5-85 mm in fruit (in Arivela viscosa fruits sessile or absent); fruits dehiscent ± entire lengths or indehiscent</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Fruits schizocarps; seeds 2(-4), 0.5 mm.</description>
      <determination>5 Wislizenia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Fruits capsules; seeds 1-40, 1.2-3.5 mm</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Fruits 2-8 mm, as long as or shorter than wide.</description>
      <determination>4 Cleomella</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Fruits (2-)12-150 mm, much longer than wide</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants with stipular spines (sometimes petioles with spines)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Plants without stipular spines (petioles without spines)</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Petals 11-30(-45) mm; gynophore 45-80 mm in fruit.</description>
      <determination>9 Tarenaya</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Petals 5-10 mm; gynophore 1-4 mm in fruit.</description>
      <determination>10 Hemiscola</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Filaments adnate basally to gynophore (scars evident in fruiting specimens near midpoint of gynophore).</description>
      <determination>12 Gynandropsis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Filaments free from gynophore (or gynophore obsolete) or inserted on androgynophore</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Bracts 1-18 mm, unifoliate.</description>
      <determination>8 Cleoserrata</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Bracts 1-25 mm, unifoliate or trifoliate (sometimes expanded)</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Gynophore obsolete; stems, leaflet surfaces, and fruitsglandular-hirsute.</description>
      <determination>11 Arivela</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Gynophore 1-25 mm (in fruit); stems, leaflet surfaces, and fruits usually not glandular (sometimes pubescent)</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Sepals distinct; gynophore 2-5 mm in fruit; anthers 3-5 mm.</description>
      <determination>3 Carsonia</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Sepals partly connate or distinct; gynophore 1-25 mm; anthers 0.6-2.6 mm</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Filaments inserted on cylindric androgynophore (usually expanded adaxially into gibbous or flattened appendage); leaflets conduplicate and flat.</description>
      <determination>2 Peritoma</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Filaments inserted on discoid or conical androgynophore; leaflets flat.</description>
      <determination>7 Cleome</determination>
    </key_statement>
  </key>
</bio:treatment>