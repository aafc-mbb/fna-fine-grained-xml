<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">269</other_info_on_meta>
    <other_info_on_meta type="illustration_page">266</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">aubrieta</taxon_name>
    <taxon_name authority="(Linnaeus) de Candolle" date="1821" rank="species">deltoidea</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>2: 294. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus aubrieta;species deltoidea</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416161</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Alyssum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">deltoideum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl. ed.</publication_title>
      <place_in_publication>2, 2: 908. 1763</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Alyssum;species deltoideum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming mats or cushions;</text>
      <biological_entity id="o14270" name="mat" name_original="mats" src="d0_s0" type="structure" />
      <biological_entity id="o14271" name="cushion" name_original="cushions" src="d0_s0" type="structure" />
      <relation from="o14269" id="r989" name="forming" negation="false" src="d0_s0" to="o14270" />
      <relation from="o14269" id="r990" name="forming" negation="false" src="d0_s0" to="o14271" />
    </statement>
    <statement id="d0_s1">
      <text>densely pubescent, trichomes stellate, mixed with fewer, setiform and forked ones.</text>
      <biological_entity id="o14269" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o14272" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s1" value="stellate" value_original="stellate" />
        <character constraint="with fewer , setiform and forked ones" is_modifier="false" name="arrangement" src="d0_s1" value="mixed" value_original="mixed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems several from base (caudex), ascending to procumbent, 0.7–3 (–5) dm, pubescent.</text>
      <biological_entity id="o14273" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character constraint="from base" constraintid="o14274" is_modifier="false" name="quantity" src="d0_s2" value="several" value_original="several" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="procumbent" value_original="procumbent" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="5" to_unit="dm" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s2" to="3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o14274" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves: petiole 0.1–1 cm;</text>
      <biological_entity constraint="basal" id="o14275" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o14276" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade obovate, oblanceolate, or rhombic, (0.5–) 1–3 (–4.5) cm × (2–) 4–13 (–20) mm, base cuneate to attenuate, margins entire or 1–3 teeth on each side, surfaces densely pubescent.</text>
      <biological_entity constraint="basal" id="o14277" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o14278" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rhombic" value_original="rhombic" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rhombic" value_original="rhombic" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s4" to="4" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="13" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s4" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14279" name="base" name_original="base" src="d0_s4" type="structure">
        <character char_type="range_value" from="cuneate" name="shape" src="d0_s4" to="attenuate" />
      </biological_entity>
      <biological_entity id="o14280" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s4" to="3" />
      </biological_entity>
      <biological_entity id="o14281" name="tooth" name_original="teeth" src="d0_s4" type="structure" />
      <biological_entity id="o14282" name="side" name_original="side" src="d0_s4" type="structure" />
      <biological_entity id="o14283" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o14281" id="r991" name="on" negation="false" src="d0_s4" to="o14282" />
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves: petiolate or (distalmost) sessile;</text>
      <biological_entity constraint="cauline" id="o14284" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blade similar to basal.</text>
      <biological_entity constraint="cauline" id="o14285" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o14286" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <biological_entity constraint="basal" id="o14287" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o14286" id="r992" name="to" negation="false" src="d0_s6" to="o14287" />
    </statement>
    <statement id="d0_s7">
      <text>Racemes 1–13-flowered, (lax).</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels erect to ascending, 5–12 (–16) mm.</text>
      <biological_entity id="o14288" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="1-13-flowered" value_original="1-13-flowered" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="ascending" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="16" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14289" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o14288" id="r993" name="fruiting" negation="false" src="d0_s8" to="o14289" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals 6–10 × 1–1.5 mm;</text>
      <biological_entity id="o14290" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o14291" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals (10–) 15–28 × 4–7 (–8) mm, (attenuate to claw, 5–12 mm);</text>
      <biological_entity id="o14292" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o14293" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="atypical_length" src="d0_s10" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s10" to="28" to_unit="mm" />
        <character char_type="range_value" from="7" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>filaments 5–10 mm;</text>
      <biological_entity id="o14294" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o14295" name="filament" name_original="filaments" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 1.2–1.6 mm.</text>
      <biological_entity id="o14296" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o14297" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Fruits terete or slightly flattened, 0.7–2 (–2.8) cm × 2–4 (–4.8) mm;</text>
      <biological_entity id="o14298" name="fruit" name_original="fruits" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s13" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s13" to="2.8" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s13" to="2" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s13" to="4.8" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s13" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>valves: trichomes long-setiform and forked, mixed with smaller, stellate ones;</text>
      <biological_entity id="o14299" name="valve" name_original="valves" src="d0_s14" type="structure" />
      <biological_entity id="o14300" name="trichome" name_original="trichomes" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="long-setiform" value_original="long-setiform" />
        <character is_modifier="false" name="shape" src="d0_s14" value="forked" value_original="forked" />
        <character constraint="with ones" constraintid="o14301" is_modifier="false" name="arrangement" src="d0_s14" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o14301" name="one" name_original="ones" src="d0_s14" type="structure">
        <character is_modifier="true" name="size" src="d0_s14" value="smaller" value_original="smaller" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s14" value="stellate" value_original="stellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>style 4–12 mm.</text>
      <biological_entity id="o14302" name="valve" name_original="valves" src="d0_s15" type="structure" />
      <biological_entity id="o14303" name="style" name_original="style" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds 1.2–16 × 0.7–1 mm. 2n = 16.</text>
      <biological_entity id="o14304" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s16" to="16" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14305" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock crevices</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock crevices" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Calif.; s Europe (Mediterranean region); sw Asia; nw Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" value="s Europe (Mediterranean region)" establishment_means="native" />
        <character name="distribution" value="sw Asia" establishment_means="native" />
        <character name="distribution" value="nw Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <discussion>Aubrieta deltoidea is known as an escape from the Mt. Hull area at the Mendocino-Lake counties boundary. It is highly variable in its native range, and several infraspecific taxa have been recognized.</discussion>
  
</bio:treatment>