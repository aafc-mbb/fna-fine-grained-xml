<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="treatment_page">108</other_info_on_meta>
    <other_info_on_meta type="illustration_page">105</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="(Fries) A. Kerner" date="1860" rank="section">hastatae</taxon_name>
    <taxon_name authority="Bebb in J. T. Rothrock" date="1879" rank="species">wolfii</taxon_name>
    <taxon_name authority="C. R. Ball" date="1905" rank="variety">idahoensis</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>40: 378. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section hastatae;species wolfii;variety idahoensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445896</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="(C. R. Ball) Rydberg" date="unknown" rank="species">idahoensis</taxon_name>
    <taxon_hierarchy>genus Salix;species idahoensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.1–2 m.</text>
      <biological_entity id="o17127" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.1" from_unit="m" name="some_measurement" src="d0_s0" to="2" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches yellow-gray or yellowbrown, pubescent or pilose;</text>
      <biological_entity id="o17128" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o17129" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow-gray" value_original="yellow-gray" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branchlets yellow-green or redbrown (darker in age), sparsely to moderately densely pubescent, hairs wavy or geniculate.</text>
      <biological_entity id="o17130" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o17131" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="sparsely to moderately densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o17132" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: petiole shallowly grooved adaxially, 3–10 mm, pubescent or villous adaxially;</text>
      <biological_entity id="o17133" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o17134" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="shallowly; adaxially" name="architecture" src="d0_s3" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>largest medial blade very narrowly elliptic to elliptic, or narrowly oblanceolate, apex acute to acuminate, abaxial surface villous, adaxial densely silky or villous;</text>
      <biological_entity id="o17135" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="largest medial" id="o17136" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character constraint="to apex, surface, surface" constraintid="o17137, o17139" is_modifier="false" modifier="very narrowly" name="arrangement_or_shape" src="d0_s4" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o17137" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="silky" value_original="silky" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o17139" name="surface" name_original="surface" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="silky" value_original="silky" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17140" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o17141" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>juvenile blade long-silky abaxially.</text>
      <biological_entity id="o17142" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o17143" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="long-silky" value_original="long-silky" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Catkins: pistillate very densely flowered, stout or subglobose, 8.5–38 × 5–12 mm, flowering branchlet 1–11 mm;</text>
      <biological_entity id="o17144" name="catkin" name_original="catkins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" modifier="very densely" name="architecture" src="d0_s6" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s6" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subglobose" value_original="subglobose" />
        <character char_type="range_value" from="8.5" from_unit="mm" name="length" src="d0_s6" to="38" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s6" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17145" name="branchlet" name_original="branchlet" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>floral bract 1–2 mm.</text>
      <biological_entity id="o17146" name="catkin" name_original="catkins" src="d0_s7" type="structure" />
      <biological_entity constraint="floral" id="o17147" name="bract" name_original="bract" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Staminate flowers: abaxial nectary 0–0.2 mm, adaxial nectary 0.6–1.1 mm.</text>
      <biological_entity id="o17148" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17149" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17150" name="nectary" name_original="nectary" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pistillate flowers: adaxial nectary oblong, ovate, or flask-shaped, 0.4–1.1 mm, longer than or, rarely, equal to stipe;</text>
      <biological_entity id="o17151" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17152" name="nectary" name_original="nectary" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="flask--shaped" value_original="flask--shaped" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="flask--shaped" value_original="flask--shaped" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s9" to="1.1" to_unit="mm" />
        <character constraint="than or" constraintid="" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
        <character constraint="to stipe" constraintid="o17153" is_modifier="false" modifier="rarely" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o17153" name="stipe" name_original="stipe" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>stipe 0–0.4 mm;</text>
      <biological_entity id="o17154" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17155" name="stipe" name_original="stipe" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>ovary pubescent or tomentose, hairs in streaks or patches;</text>
      <biological_entity id="o17156" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17157" name="ovary" name_original="ovary" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o17158" name="hair" name_original="hairs" src="d0_s11" type="structure" />
      <biological_entity id="o17159" name="streak" name_original="streaks" src="d0_s11" type="structure" />
      <biological_entity id="o17160" name="patch" name_original="patches" src="d0_s11" type="structure" />
      <relation from="o17158" id="r1173" name="in" negation="false" src="d0_s11" to="o17159" />
      <relation from="o17158" id="r1174" name="in" negation="false" src="d0_s11" to="o17160" />
    </statement>
    <statement id="d0_s12">
      <text>ovules 8–16 per ovary;</text>
      <biological_entity id="o17161" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17162" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o17163" from="8" name="quantity" src="d0_s12" to="16" />
      </biological_entity>
      <biological_entity id="o17163" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>stigmas flat, abaxially non-papillate with rounded or pointed tip, or slenderly or broadly cylindrical.</text>
      <biological_entity id="o17164" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o17166" name="tip" name_original="tip" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s13" value="pointed" value_original="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = unknown.</text>
      <biological_entity id="o17165" name="stigma" name_original="stigmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s13" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o17166" is_modifier="false" modifier="abaxially" name="relief" src="d0_s13" value="non-papillate" value_original="non-papillate" />
        <character is_modifier="false" modifier="slenderly; broadly" name="shape" notes="" src="d0_s13" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17167" name="chromosome" name_original="" src="d0_s14" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early-mid Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="mid Jun" from="early" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sedge meadows along stream and lake margins, drainageways, around springs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sedge meadows" constraint="along stream and lake margins , drainageways ," />
        <character name="habitat" value="stream" />
        <character name="habitat" value="lake margins" />
        <character name="habitat" value="drainageways" />
        <character name="habitat" value="springs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2100-3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="2100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Idaho, Mont., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>54b.</number>
  
</bio:treatment>