<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">678</other_info_on_meta>
    <other_info_on_meta type="treatment_page">682</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Prantl in H. G. A. Engler and K. Prantl" date="unknown" rank="tribe">thelypodieae</taxon_name>
    <taxon_name authority="S. Watson" date="1871" rank="genus">caulanthus</taxon_name>
    <taxon_name authority="(Nuttall) Payson" date="1923" rank="species">heterophyllus</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>9: 298. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe thelypodieae;genus caulanthus;species heterophyllus</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094865</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Streptanthus</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">heterophyllus</taxon_name>
    <place_of_publication>
      <publication_title>in J. Torrey and A. Gray, Fl. N. Amer.</publication_title>
      <place_in_publication>1: 77. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Streptanthus;species heterophyllus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Caulanthus</taxon_name>
    <taxon_name authority="Payson" date="unknown" rank="species">stenocarpus</taxon_name>
    <taxon_hierarchy>genus Caulanthus;species stenocarpus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Guillenia</taxon_name>
    <taxon_name authority="(Nuttall) O. E. Schulz" date="unknown" rank="species">heterophylla</taxon_name>
    <taxon_hierarchy>genus Guillenia;species heterophylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Streptanthus</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">repandus</taxon_name>
    <taxon_hierarchy>genus Streptanthus;species repandus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals;</text>
    </statement>
    <statement id="d0_s1">
      <text>hispid basally, glabrous or subglabrate distally.</text>
      <biological_entity id="o8493" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s1" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s1" value="subglabrate" value_original="subglabrate" />
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, usually branched distally, 2.5–12 dm, hispid.</text>
      <biological_entity id="o8494" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="2.5" from_unit="dm" name="some_measurement" src="d0_s2" to="12" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Basal leaves weakly rosulate;</text>
      <biological_entity constraint="basal" id="o8495" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="weakly" name="arrangement" src="d0_s3" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 0.3–3 cm;</text>
      <biological_entity id="o8496" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear-oblanceolate to linear-oblong, 0.7–7 cm × 2–18 mm, margins coarsely dentate or pinnately lobed.</text>
      <biological_entity id="o8497" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear-oblanceolate" name="shape" src="d0_s5" to="linear-oblong" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s5" to="7" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8498" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s5" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves (median) sessile;</text>
      <biological_entity constraint="cauline" id="o8499" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blade linear-lanceolate, 5–16 cm × 5–40 mm, (smaller distally, base amplexicaul to sagittate), margins dentate or (distalmost) entire.</text>
      <biological_entity id="o8500" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="16" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="40" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8501" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Racemes (densely flowered), without a terminal cluster of sterile flowers.</text>
      <biological_entity constraint="terminal" id="o8503" name="cluster" name_original="cluster" src="d0_s8" type="structure" />
      <biological_entity id="o8504" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o8502" id="r615" name="without" negation="false" src="d0_s8" to="o8503" />
      <relation from="o8503" id="r616" name="part_of" negation="false" src="d0_s8" to="o8504" />
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels reflexed, 2–8 mm, glabrous or hispid.</text>
      <biological_entity id="o8502" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o8505" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o8502" id="r617" name="fruiting" negation="false" src="d0_s9" to="o8505" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals erect (purple or yellow to creamy white), lanceolate, 3–8 × 1–1.8 mm (equal);</text>
      <biological_entity id="o8506" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8507" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals purple or yellowish (often with darker purple veins), 5–15 mm, blade 2–6 × 1–1.5 mm, not crisped, claw narrowly oblanceolate or oblong, 3–9 × 1–1.5 mm;</text>
      <biological_entity id="o8508" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o8509" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellowish" value_original="yellowish" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8510" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s11" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o8511" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="9" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments tetradynamous, median pairs 3–6 mm, lateral pair 2–5 mm;</text>
      <biological_entity id="o8512" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o8513" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="tetradynamous" value_original="tetradynamous" />
        <character is_modifier="false" name="position" src="d0_s12" value="median" value_original="median" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s12" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers oblong, equal, 1–3 mm.</text>
      <biological_entity id="o8514" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o8515" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits reflexed (often straight, rarely curved), latiseptate or 4-angled, 4.5–10 cm × 1–1.5 mm;</text>
      <biological_entity id="o8516" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="latiseptate" value_original="latiseptate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="4-angled" value_original="4-angled" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s14" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves each with prominent midvein;</text>
      <biological_entity id="o8517" name="valve" name_original="valves" src="d0_s15" type="structure" />
      <biological_entity id="o8518" name="midvein" name_original="midvein" src="d0_s15" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s15" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o8517" id="r618" name="with" negation="false" src="d0_s15" to="o8518" />
    </statement>
    <statement id="d0_s16">
      <text>ovules 56–82 per ovary;</text>
      <biological_entity id="o8519" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o8520" from="56" name="quantity" src="d0_s16" to="82" />
      </biological_entity>
      <biological_entity id="o8520" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.5–3.5mm;</text>
      <biological_entity id="o8521" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s17" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>stigma slightly 2-lobed.</text>
      <biological_entity id="o8522" name="stigma" name_original="stigma" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s18" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds 1.2–2 × 0.9–1.4 mm. 2n = 28.</text>
      <biological_entity id="o8523" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s19" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s19" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8524" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="28" value_original="28" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal scrub, chaparral, rocky areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal scrub" />
        <character name="habitat" value="chaparral" />
        <character name="habitat" value="rocky areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11.</number>
  <discussion>Caulanthus heterophyllus is a common species distributed from Santa Barbara County southward into northwestern Baja California, Mexico.</discussion>
  <discussion>R. E. Buck (1993) divided the species into two varieties, including one invalidly published, based on flower color, but these are treated here as mere color variants.</discussion>
  
</bio:treatment>