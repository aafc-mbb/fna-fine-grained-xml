<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">364</other_info_on_meta>
    <other_info_on_meta type="mention_page">370</other_info_on_meta>
    <other_info_on_meta type="mention_page">390</other_info_on_meta>
    <other_info_on_meta type="treatment_page">369</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="Windham &amp; Al-Shehbaz" date="2007" rank="species">cascadensis</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>11: 260. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species cascadensis</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094789</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="Nuttall" date="unknown" rank="species">microphylla</taxon_name>
    <taxon_name authority="Rollins" date="unknown" rank="variety">thompsonii</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>43: 429. 1941,</place_in_publication>
      <other_info_on_pub>not Boechera thompsonii (S. L. Welsh) N. H. Holmgren 2005</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Arabis;species microphylla;variety thompsonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>long-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>(cespitose);</text>
    </statement>
    <statement id="d0_s3">
      <text>apomictic;</text>
      <biological_entity id="o40238" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="long-lived" value_original="long-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s3" value="apomictic" value_original="apomictic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>caudex somewhat woody.</text>
      <biological_entity id="o40239" name="caudex" name_original="caudex" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="somewhat" name="texture" src="d0_s4" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Stems usually 1 per caudex branch, arising from center of rosette near ground surface, 0.5–2.2 dm, sparsely pubescent proximally, trichomes simple and short-stalked, 2-rayed or 3-rayed, 0.1–0.2 mm, glabrous distally.</text>
      <biological_entity id="o40240" name="stem" name_original="stems" src="d0_s5" type="structure">
        <character constraint="per caudex branch" constraintid="o40241" name="quantity" src="d0_s5" value="1" value_original="1" />
        <character constraint="from center" constraintid="o40242" is_modifier="false" name="orientation" notes="" src="d0_s5" value="arising" value_original="arising" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" notes="" src="d0_s5" to="2.2" to_unit="dm" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o40241" name="branch" name_original="branch" src="d0_s5" type="structure" />
      <biological_entity id="o40242" name="center" name_original="center" src="d0_s5" type="structure" />
      <biological_entity id="o40243" name="rosette" name_original="rosette" src="d0_s5" type="structure" />
      <biological_entity id="o40244" name="ground" name_original="ground" src="d0_s5" type="structure" />
      <biological_entity id="o40245" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity id="o40246" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="0.2" to_unit="mm" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o40242" id="r2722" name="part_of" negation="false" src="d0_s5" to="o40243" />
      <relation from="o40242" id="r2723" name="near" negation="false" src="d0_s5" to="o40244" />
      <relation from="o40242" id="r2724" name="near" negation="false" src="d0_s5" to="o40245" />
    </statement>
    <statement id="d0_s6">
      <text>Basal leaves: blade linear-oblanceolate, 0.7–2 mm wide, margins entire, ciliate proximally, trichomes to 0.4 mm, surfaces densely pubescent, trichomes short-stalked, (2-rayed or) 3–6-rayed, 0.05–0.2 (–0.3) mm.</text>
      <biological_entity constraint="basal" id="o40247" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o40248" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="linear-oblanceolate" value_original="linear-oblanceolate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40249" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o40250" name="trichome" name_original="trichomes" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="0.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40251" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o40252" name="trichome" name_original="trichomes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="0.3" to_unit="mm" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s6" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves: 4–6, not concealing stem;</text>
      <biological_entity constraint="cauline" id="o40253" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="6" />
      </biological_entity>
      <biological_entity id="o40254" name="stem" name_original="stem" src="d0_s7" type="structure" />
      <relation from="o40253" id="r2725" name="concealing" negation="true" src="d0_s7" to="o40254" />
    </statement>
    <statement id="d0_s8">
      <text>blade auricles 0.5–1 mm, surfaces of distalmost leaves glabrous.</text>
      <biological_entity constraint="cauline" id="o40255" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <biological_entity constraint="blade" id="o40256" name="auricle" name_original="auricles" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o40257" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o40258" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
      <relation from="o40257" id="r2726" name="part_of" negation="false" src="d0_s8" to="o40258" />
    </statement>
    <statement id="d0_s9">
      <text>Racemes 3–11-flowered, unbranched.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fruiting pedicels ascending to divaricate-ascending, straight, 3–8 mm, glabrous.</text>
      <biological_entity id="o40259" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-11-flowered" value_original="3-11-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s10" to="divaricate-ascending" />
        <character is_modifier="false" name="course" src="d0_s10" value="straight" value_original="straight" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o40260" name="pedicel" name_original="pedicels" src="d0_s10" type="structure" />
      <relation from="o40259" id="r2727" name="fruiting" negation="false" src="d0_s10" to="o40260" />
    </statement>
    <statement id="d0_s11">
      <text>Flowers ascending at anthesis;</text>
      <biological_entity id="o40261" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sepals glabrous or sparsely pubescent;</text>
      <biological_entity id="o40262" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>petals lavender, 5–6 × 1–1.7 mm, glabrous;</text>
      <biological_entity id="o40263" name="petal" name_original="petals" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration_or_odor" src="d0_s13" value="lavender" value_original="lavender" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pollen spheroid.</text>
      <biological_entity id="o40264" name="pollen" name_original="pollen" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="spheroid" value_original="spheroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Fruits ascending to divaricate-ascending, not appressed to rachis, not secund, straight, edges parallel, 3.5–6.2 cm × 1.2–1.5 mm;</text>
      <biological_entity id="o40265" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s15" to="divaricate-ascending" />
        <character constraint="to rachis" constraintid="o40266" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s15" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s15" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o40266" name="rachis" name_original="rachis" src="d0_s15" type="structure" />
      <biological_entity id="o40267" name="edge" name_original="edges" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="3.5" from_unit="cm" name="length" src="d0_s15" to="6.2" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>valves glabrous;</text>
      <biological_entity id="o40268" name="valve" name_original="valves" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovules 58–80 per ovary;</text>
      <biological_entity id="o40269" name="ovule" name_original="ovules" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o40270" from="58" name="quantity" src="d0_s17" to="80" />
      </biological_entity>
      <biological_entity id="o40270" name="ovary" name_original="ovary" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>style 0.8–1.5 mm.</text>
      <biological_entity id="o40271" name="style" name_original="style" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Seeds uniseriate, 1.1–1.3 × 0.9–1 mm;</text>
      <biological_entity id="o40272" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s19" value="uniseriate" value_original="uniseriate" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s19" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s19" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>wing continuous, 0.05–0.1 mm wide.</text>
      <biological_entity id="o40273" name="wing" name_original="wing" src="d0_s20" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s20" value="continuous" value_original="continuous" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s20" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jun" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Basaltic cliffs and rocky slopes in subalpine areas</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="basaltic cliffs" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="subalpine areas" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 1900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1900" to_unit="m" from="1900" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Morphological evidence suggests that Boechera cascadensis is an apomictic species that arose through hybridization between B. microphylla and B. paupercula (see M. D. Windham and I. A. Al-Shehbaz 2007 for detailed comparison). It is known from two collections: the type specimens from Kittitas County, Washington, and a more recent collection from Baker County, Oregon.</discussion>
  
</bio:treatment>