<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Gordon C. Tucker</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">200</other_info_on_meta>
    <other_info_on_meta type="treatment_page">201</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Berchtold &amp; J. Presl" date="unknown" rank="family">cleomaceae</taxon_name>
    <taxon_name authority="Rafinesque" date="1819" rank="genus">POLANISIA</taxon_name>
    <place_of_publication>
      <publication_title>Amer. J. Sci.</publication_title>
      <place_in_publication>1: 3781819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cleomaceae;genus POLANISIA</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek polys, many, and anisos, unequal, alluding to stamens</other_info_on_name>
    <other_info_on_name type="fna_id">126304</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Nuttall" date="unknown" rank="genus">Cristatella</taxon_name>
    <taxon_hierarchy>genus Cristatella;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual or perennial (unscented).</text>
      <biological_entity id="o10732" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems sparsely or profusely branched;</text>
    </statement>
    <statement id="d0_s2">
      <text>glandular-pubescent, viscid.</text>
      <biological_entity id="o10733" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="profusely" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" name="coating" src="d0_s2" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules absent or minute;</text>
      <biological_entity id="o10734" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o10735" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s3" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole without pulvinus distally;</text>
      <biological_entity id="o10736" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o10737" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o10738" name="pulvinu" name_original="pulvinus" src="d0_s4" type="structure" />
      <relation from="o10737" id="r752" name="without" negation="false" src="d0_s4" to="o10738" />
    </statement>
    <statement id="d0_s5">
      <text>leaflets 3.</text>
      <biological_entity id="o10739" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o10740" name="leaflet" name_original="leaflets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal or axillary (from distal leaves), racemes (flat-topped);</text>
      <biological_entity id="o10741" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o10742" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>bracts present (pedicels green to purple, glandular-pubescent).</text>
      <biological_entity id="o10743" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers slightly zygomorphic;</text>
    </statement>
    <statement id="d0_s9">
      <text>(receptacle prolonged into gland adaxially);</text>
      <biological_entity id="o10744" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s8" value="zygomorphic" value_original="zygomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sepals deciduous, distinct, equal;</text>
      <biological_entity id="o10745" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="duration" src="d0_s10" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals unequal (adaxial pair larger);</text>
      <biological_entity id="o10746" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 8–32;</text>
      <biological_entity id="o10747" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" name="quantity" src="d0_s12" to="32" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments inserted on gynophore, (unequal), glabrous;</text>
      <biological_entity id="o10748" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10749" name="gynophore" name_original="gynophore" src="d0_s13" type="structure" />
      <relation from="o10748" id="r753" name="inserted on" negation="false" src="d0_s13" to="o10749" />
    </statement>
    <statement id="d0_s14">
      <text>anthers (ellipsoid), not coiling as pollen released;</text>
      <biological_entity id="o10750" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character constraint="as pollen" constraintid="o10751" is_modifier="false" modifier="not" name="shape" src="d0_s14" value="coiling" value_original="coiling" />
      </biological_entity>
      <biological_entity id="o10751" name="pollen" name_original="pollen" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>gynophore ascending in fruit.</text>
      <biological_entity id="o10752" name="gynophore" name_original="gynophore" src="d0_s15" type="structure">
        <character constraint="in fruit" constraintid="o10753" is_modifier="false" name="orientation" src="d0_s15" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o10753" name="fruit" name_original="fruit" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Fruits capsules, dehiscent in distal 1/2, cylindric, oblong.</text>
      <biological_entity constraint="fruits" id="o10754" name="capsule" name_original="capsules" src="d0_s16" type="structure">
        <character constraint="in distal 1/2" constraintid="o10755" is_modifier="false" name="dehiscence" src="d0_s16" value="dehiscent" value_original="dehiscent" />
        <character is_modifier="false" name="shape" notes="" src="d0_s16" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10755" name="1/2" name_original="1/2" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>Seeds 12–65, globose or subglobose, not arillate, (cleft fused between ends).</text>
    </statement>
    <statement id="d0_s18">
      <text>x = 10.</text>
      <biological_entity id="o10756" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" from="12" name="quantity" src="d0_s17" to="65" />
        <character is_modifier="false" name="shape" src="d0_s17" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s17" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s17" value="arillate" value_original="arillate" />
      </biological_entity>
      <biological_entity constraint="x" id="o10757" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Clammyweed</other_name>
  <discussion>Species 5 (5 in the flora).</discussion>
  <references>
    <reference>Iltis, H. H. 1958. Studies in the Capparidaceae. IV. Polanisia Raf. Brittonia 10: 33–58.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet blades obovate to oblanceolate or broadly elliptic, 0.5-2(-3) cm wide; styles deciduous in fruit, 5-40 mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet blades linear or narrowly elliptic, 0.05-0.2 cm wide; styles persistent in fruit, 2.5-4.5 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants usually annual, rarely perennial; seeds roughened or tuberculate-rugose, 2-2.3 mm; stamens 10-20; styles 5-17 mm.</description>
      <determination>1 Polanisia dodecandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants perennial; seeds smooth, 0.7-2 mm; stamens 20-27; styles 20-40 mm (New Mexico, Texas).</description>
      <determination>5 Polanisia uniglandulosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Adaxial petals 6-11 mm; nectary glands 1-5.5 mm; gynophore 3-14 mm in fruit.</description>
      <determination>3 Polanisia erosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Adaxial petals 3-5 mm; nectary glands 0.5-1 mm; gynophore 1-4 mm in fruit</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsules 8-30 mm; seeds 1.8-2 mm; Great Plains, midwestern states.</description>
      <determination>2 Polanisia jamesii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsules 40-60 mm; seeds 0.7-0.9 mm; southeastern states.</description>
      <determination>4 Polanisia tenuifolia</determination>
    </key_statement>
  </key>
</bio:treatment>