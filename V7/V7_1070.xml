<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="mention_page">664</other_info_on_meta>
    <other_info_on_meta type="treatment_page">643</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(Munz) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">hitchcockii</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 324. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species hitchcockii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094894</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="Munz" date="unknown" rank="species">hitchcockii</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>56: 163. 1929</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lesquerella;species hitchcockii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(forming loose mats);</text>
      <biological_entity id="o8213" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex (buried), branched;</text>
    </statement>
    <statement id="d0_s3">
      <text>densely pubescent, trichomes (short-stalked), 4–6-rayed, rays distinct, bifurcate, (rough-tuberculate).</text>
      <biological_entity id="o8214" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8215" name="trichome" name_original="trichomes" src="d0_s3" type="structure" />
      <biological_entity id="o8216" name="ray" name_original="rays" src="d0_s3" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s3" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s3" value="bifurcate" value_original="bifurcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems few to several from base, prostrate to erect or spreading, 0.05–0.5 (–1.2) dm.</text>
      <biological_entity id="o8217" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="from base" constraintid="o8218" from="few" name="quantity" src="d0_s4" to="several" />
        <character char_type="range_value" from="prostrate" name="orientation" notes="" src="d0_s4" to="erect or spreading" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s4" to="1.2" to_unit="dm" />
        <character char_type="range_value" from="0.05" from_unit="dm" name="some_measurement" src="d0_s4" to="0.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o8218" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: (petiole and blade differentiated or not);</text>
      <biological_entity constraint="basal" id="o8219" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>blade spatulate to elliptic or linear or narrowly oblanceolate, 0.5–1.5 (–2.5) cm, margins entire.</text>
      <biological_entity constraint="basal" id="o8220" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o8221" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="spatulate to elliptic" value_original="spatulate to elliptic" />
        <character name="shape" src="d0_s6" value="linear or narrowly oblanceolate" value_original="linear or narrowly oblanceolate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8222" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves similar to basal, smaller.</text>
      <biological_entity constraint="cauline" id="o8223" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" notes="" src="d0_s7" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8224" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o8223" id="r593" name="to" negation="false" src="d0_s7" to="o8224" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes dense.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels (ascending, straight or slightly curved), 2–6 mm.</text>
      <biological_entity id="o8225" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="density" src="d0_s8" value="dense" value_original="dense" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8226" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <relation from="o8225" id="r594" name="fruiting" negation="false" src="d0_s9" to="o8226" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals narrowly lanceolate to lanceolate, 2.8–6 mm;</text>
      <biological_entity id="o8227" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o8228" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" src="d0_s10" to="lanceolate" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals (pale to deep yellow), narrowly lanceolate to oblanceolate, 5–9 mm, (claw undifferentiated from blade).</text>
      <biological_entity id="o8229" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="narrowly lanceolate" name="shape" notes="" src="d0_s11" to="oblanceolate" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8230" name="petal" name_original="petals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Fruits (sessile or substipitate), globose or subglobose to obovoid, not or slightly inflated, 3–6 mm, (firm, apex acute);</text>
      <biological_entity id="o8231" name="fruit" name_original="fruits" src="d0_s12" type="structure">
        <character char_type="range_value" from="subglobose" name="shape" src="d0_s12" to="obovoid" />
        <character is_modifier="false" modifier="not; slightly" name="shape" src="d0_s12" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>valves (reddish in age, not retaining seeds after dehiscence), glabrous throughout;</text>
      <biological_entity id="o8232" name="valve" name_original="valves" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>replum as wide as or wider than fruit;</text>
      <biological_entity id="o8233" name="replum" name_original="replum" src="d0_s14" type="structure">
        <character constraint="than fruit" constraintid="o8234" is_modifier="false" name="width" src="d0_s14" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity id="o8234" name="fruit" name_original="fruit" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>ovules 4–8 per ovary;</text>
      <biological_entity id="o8235" name="ovule" name_original="ovules" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o8236" from="4" name="quantity" src="d0_s15" to="8" />
      </biological_entity>
      <biological_entity id="o8236" name="ovary" name_original="ovary" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>style 1.7–6 mm.</text>
      <biological_entity id="o8237" name="style" name_original="style" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s16" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds flattened.</text>
      <biological_entity id="o8238" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nev., Utah</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>40.</number>
  <other_name type="common_name">Hitchcock’s bladderpod</other_name>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <discussion>The taxonomic treatment of Physaria hitchcockii has varied widely over the years. Molecular study (pers. obs.) has shown no direct relationship to P. tumulosa; morphologically, though, P. navajoensis and P. tumulosa appear closely related. Infraspecific taxonomy is based on the presence of a discernable petiole and whether or not the caudex is elastically elongated. The subspecies recognized here are usually geographically coherent, except that collections from the Table Cliff Plateau are more similar to subsp. hitchcockii, disjunct in Nevada, than they are to the very nearly sympatric subsp. rubicundula.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Caudices elongated and elastic; basal leaves: petiole not differentiated from blade, blades linear-oblanceolate; Aquarius, Markagunt, and Paunsaugunt plateaus, Utah (limited to the pink member of the limestone Wasatch (Claron) Formation).</description>
      <determination>40c Physaria hitchcockii subsp. rubicundula</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Caudices elongated or not, elastic or not; basal leaves: petiole differentiated (sometimes weakly) from blade, blades oblanceolate to obovate; Nevada, Utah</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants forming tufts; caudices not elongated, not elastic; fruits 2.6-3.8 mm wide; Table Cliff Plateau, Utah (limited to the white member of the limestone Wasatch (Claron) Formation) and limestones of the Sheep Range and Spring Mountains, Nevada.</description>
      <determination>40a Physaria hitchcockii subsp. hitchcockii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants forming soft mats; caudices elongated, elastic (creeping); fruits 1.7-3 mm wide; Grant, Quinn Canyon, and Schell Creek ranges, Nevada.</description>
      <determination>40b Physaria hitchcockii subsp. confluens</determination>
    </key_statement>
  </key>
</bio:treatment>