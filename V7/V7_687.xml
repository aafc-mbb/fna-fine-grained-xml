<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="treatment_page">452</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">camelineae</taxon_name>
    <taxon_name authority="Crantz" date="1762" rank="genus">camelina</taxon_name>
    <taxon_name authority="de Candolle" date="1821" rank="species">microcarpa</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>2: 517. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe camelineae;genus camelina;species microcarpa</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200009286</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Camelina</taxon_name>
    <taxon_name authority="(Linnaeus) Crantz" date="unknown" rank="species">sativa</taxon_name>
    <taxon_name authority="(de Candolle) E. Schmid" date="unknown" rank="subspecies">microcarpa</taxon_name>
    <taxon_hierarchy>genus Camelina;species sativa;subspecies microcarpa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Annuals.</text>
      <biological_entity id="o11283" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems unbranched or branched distally, (0.8–) 2–8 (–10) dm, densely to moderately hirsute basally, trichomes simple, to 2.5 mm, often mixed branched ones, (glabrescent distally).</text>
      <biological_entity id="o11284" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.8" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="10" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="8" to_unit="dm" />
        <character is_modifier="false" modifier="densely to moderately; basally" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o11285" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s1" value="mixed" value_original="mixed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves withered by anthesis.</text>
      <biological_entity constraint="basal" id="o11286" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="by anthesis" is_modifier="false" name="life_cycle" src="d0_s2" value="withered" value_original="withered" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves: blade lanceolate, narrowly oblong, or linear-lanceolate, (0.8–) 1.5–5.5 (–7) cm × 1–10 (–20) mm, base sagittate or minutely auriculate, margins entire or, rarely, remotely denticulate, (often subciliate), apex acute, surfaces pubescent, trichomes primarily simple.</text>
      <biological_entity constraint="cauline" id="o11287" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o11288" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_length" src="d0_s3" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s3" to="5.5" to_unit="cm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11289" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="sagittate" value_original="sagittate" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s3" value="auriculate" value_original="auriculate" />
      </biological_entity>
      <biological_entity id="o11290" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s3" value="," value_original="," />
        <character is_modifier="false" modifier="remotely" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o11291" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o11292" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels ascending, 4–14 (–17) mm.</text>
      <biological_entity id="o11293" name="trichome" name_original="trichomes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="primarily" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="17" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11294" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o11293" id="r787" name="fruiting" negation="false" src="d0_s4" to="o11294" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 2–3.5 × 0.5–1 mm;</text>
      <biological_entity id="o11295" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o11296" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s5" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals pale-yellow, (2.5–) 3–4 (–6) × 1–2 mm;</text>
      <biological_entity id="o11297" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o11298" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale-yellow" value_original="pale-yellow" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" src="d0_s6" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="6" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 1.5–3 mm;</text>
      <biological_entity id="o11299" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o11300" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers ca. 0.5 mm.</text>
      <biological_entity id="o11301" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o11302" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Fruits pyriform to narrowly so, 3.5–5 (–7) × 2–4 (–5) mm, apex acute;</text>
      <biological_entity id="o11303" name="fruit" name_original="fruits" src="d0_s9" type="structure">
        <character constraint="to apex" constraintid="o11304" is_modifier="false" name="shape" src="d0_s9" value="pyriform" value_original="pyriform" />
      </biological_entity>
      <biological_entity id="o11304" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_length" src="d0_s9" to="7" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" is_modifier="true" name="length" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" is_modifier="true" name="atypical_width" src="d0_s9" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" name="width" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>valves each often with obscure midvein, margin narrowly winged;</text>
      <biological_entity id="o11305" name="valve" name_original="valves" src="d0_s10" type="structure" />
      <biological_entity id="o11306" name="midvein" name_original="midvein" src="d0_s10" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s10" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o11307" name="margin" name_original="margin" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s10" value="winged" value_original="winged" />
      </biological_entity>
      <relation from="o11305" id="r788" modifier="often" name="with" negation="false" src="d0_s10" to="o11306" />
    </statement>
    <statement id="d0_s11">
      <text>style 1–3.5 mm.</text>
      <biological_entity id="o11308" name="style" name_original="style" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds reddish-brown or brown, 0.8–1.4 (–1.5) × 0.5–0.6 mm. 2n = 40.</text>
      <biological_entity id="o11309" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="brown" value_original="brown" />
        <character char_type="range_value" from="1.4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s12" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s12" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11310" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Farms, fields, meadows, prairies, roadsides, forest margins, open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="farms" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="prairies" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="forest margins" />
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Ala., Ariz., Calif., Colo., Conn., Del., D.C., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.Dak., Tenn., Tex., Utah, Vt., Va., Wash., W.Va., Wis., Wyo.; Europe; Asia; n Africa; introduced also in South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="also in South America" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  
</bio:treatment>