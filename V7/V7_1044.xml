<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">623</other_info_on_meta>
    <other_info_on_meta type="treatment_page">634</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="B. L. Robinson in A. Gray et al." date="unknown" rank="tribe">physarieae</taxon_name>
    <taxon_name authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="1848" rank="genus">physaria</taxon_name>
    <taxon_name authority="(S. Watson) O’Kane &amp; Al-Shehbaz" date="2002" rank="species">douglasii</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>12: 322. 2002</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe physarieae;genus physaria;species douglasii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250095104</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lesquerella</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">douglasii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>23: 255. 1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lesquerella;species douglasii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
      <biological_entity id="o23313" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex simple;</text>
    </statement>
    <statement id="d0_s2">
      <text>densely pubescent, trichomes (sessile or nearly so), 4–6 (–10) -rayed, rays usually furcate near base, rarely bifurcate, (umbonate, tuberculate throughout).</text>
      <biological_entity id="o23314" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o23315" name="trichome" name_original="trichomes" src="d0_s2" type="structure" />
      <biological_entity id="o23316" name="ray" name_original="rays" src="d0_s2" type="structure">
        <character constraint="near base" constraintid="o23317" is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="furcate" value_original="furcate" />
        <character is_modifier="false" modifier="rarely" name="shape" notes="" src="d0_s2" value="bifurcate" value_original="bifurcate" />
      </biological_entity>
      <biological_entity id="o23317" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Stems simple from base, erect, (usually unbranched), to 4.5 dm.</text>
      <biological_entity id="o23318" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character constraint="from base" constraintid="o23319" is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s3" value="erect" value_original="erect" />
        <character char_type="range_value" from="0" from_unit="dm" name="some_measurement" src="d0_s3" to="4.5" to_unit="dm" />
      </biological_entity>
      <biological_entity id="o23319" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves: blade suborbicular to elliptic, 2–9.5 (–11.5) cm, margins entire, sinuate, coarsely dentate, or almost lyrate-pinnatifid.</text>
      <biological_entity constraint="basal" id="o23320" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o23321" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="suborbicular" name="shape" src="d0_s4" to="elliptic" />
        <character char_type="range_value" from="9.5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="11.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="9.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23322" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s4" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" modifier="coarsely" name="architecture_or_shape" src="d0_s4" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="almost" name="architecture_or_shape" src="d0_s4" value="lyrate-pinnatifid" value_original="lyrate-pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cauline leaves similar to basal, blade narrowly linear or, sometimes, orbicular.</text>
      <biological_entity constraint="cauline" id="o23323" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o23324" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o23325" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character name="arrangement_or_course_or_shape" src="d0_s5" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s5" value="orbicular" value_original="orbicular" />
      </biological_entity>
      <relation from="o23323" id="r1601" name="to" negation="false" src="d0_s5" to="o23324" />
    </statement>
    <statement id="d0_s6">
      <text>Racemes loose (lax).</text>
    </statement>
    <statement id="d0_s7">
      <text>Fruiting pedicels (recurved, straight, curved, or sigmoid), 6–20 mm.</text>
      <biological_entity id="o23326" name="raceme" name_original="racemes" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23327" name="pedicel" name_original="pedicels" src="d0_s7" type="structure" />
      <relation from="o23326" id="r1602" name="fruiting" negation="false" src="d0_s7" to="o23327" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: sepals elliptic or ovate, (2–) 3.5–7.5 mm, (cucullate);</text>
      <biological_entity id="o23328" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o23329" name="sepal" name_original="sepals" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="3.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petals 6–11 mm.</text>
      <biological_entity id="o23330" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o23331" name="petal" name_original="petals" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s9" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Fruits obovoid to subglobose, not inflated (not angustiseptate), 3–6 mm;</text>
      <biological_entity id="o23332" name="fruit" name_original="fruits" src="d0_s10" type="structure">
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s10" to="subglobose" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s10" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>valves sparsely pubescent, sometimes glabrous inside, trichomes sessile or stalked;</text>
      <biological_entity id="o23333" name="valve" name_original="valves" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23334" name="trichome" name_original="trichomes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="stalked" value_original="stalked" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovules 4 (–8) per ovary;</text>
      <biological_entity id="o23335" name="ovule" name_original="ovules" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="8" />
        <character constraint="per ovary" constraintid="o23336" name="quantity" src="d0_s12" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o23336" name="ovary" name_original="ovary" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>style (1.6–) 3–6 mm.</text>
      <biological_entity id="o23337" name="style" name_original="style" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="atypical_some_measurement" src="d0_s13" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds flattened.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 10, 30.</text>
      <biological_entity id="o23338" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23339" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
        <character name="quantity" src="d0_s15" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <other_name type="common_name">Douglas’s bladderpod</other_name>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruit valves: trichomes sessile; cauline leaves loosely arranged, blades narrowly linear; British Columbia, Idaho, Montana, Oregon, Washington.</description>
      <determination>24a Physaria douglasii subsp. douglasii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruit valves: trichomes stalked; cauline leaves imbricate, blades sometimes orbicular; White Bluffs adjacent to Columbia River of Washington.</description>
      <determination>24b Physaria douglasii subsp. tuplashensis</determination>
    </key_statement>
  </key>
</bio:treatment>