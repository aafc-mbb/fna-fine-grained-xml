<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">506</other_info_on_meta>
    <other_info_on_meta type="mention_page">507</other_info_on_meta>
    <other_info_on_meta type="treatment_page">508</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Nuttall" date="1825" rank="genus">selenia</taxon_name>
    <taxon_name authority="R. F. Martin" date="1938" rank="species">grandis</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>40: 183. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus selenia;species grandis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250095061</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Selenia</taxon_name>
    <taxon_name authority="Steyermark" date="unknown" rank="species">oinosepala</taxon_name>
    <taxon_hierarchy>genus Selenia;species oinosepala;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants winter-annuals.</text>
      <biological_entity constraint="plants" id="o31091" name="winter-annual" name_original="winter-annuals" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to ascending, (slender or stout), 1.5–6.5 dm.</text>
      <biological_entity id="o31092" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="ascending" />
        <character char_type="range_value" from="1.5" from_unit="dm" name="some_measurement" src="d0_s1" to="6.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves (soon withered), early rosulate;</text>
      <biological_entity constraint="basal" id="o31093" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="rosulate" value_original="rosulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 1–4 cm;</text>
      <biological_entity id="o31094" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade margins 2-pinnatisect or 3-pinnatisect, 4–15 cm;</text>
      <biological_entity constraint="blade" id="o31095" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="2-pinnatisect" value_original="2-pinnatisect" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="3-pinnatisect" value_original="3-pinnatisect" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lobes 8–16 on each side;</text>
      <biological_entity id="o31096" name="lobe" name_original="lobes" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="on side" constraintid="o31097" from="8" name="quantity" src="d0_s5" to="16" />
      </biological_entity>
      <biological_entity id="o31097" name="side" name_original="side" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>apical segment oblong to ovate, 1–10 (–12) × 0.5–2 (–3.5) mm, margins entire.</text>
      <biological_entity constraint="apical" id="o31098" name="segment" name_original="segment" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="ovate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s6" to="10" to_unit="mm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31099" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves (and bracts) similar to basal, smaller distally (lobes fewer).</text>
      <biological_entity constraint="cauline" id="o31100" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="distally" name="size" notes="" src="d0_s7" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity constraint="basal" id="o31101" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o31100" id="r2079" name="to" negation="false" src="d0_s7" to="o31101" />
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels: some from basal rosette (straight or slightly recurved), (30–) 50–100 (–180) mm.</text>
      <biological_entity id="o31103" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s8" to="50" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" notes="" src="d0_s8" to="180" to_unit="mm" />
        <character char_type="range_value" from="50" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="100" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o31104" name="rosette" name_original="rosette" src="d0_s8" type="structure" />
      <relation from="o31103" id="r2080" name="from" negation="false" src="d0_s8" to="o31104" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals (persistent), erect, oblong-lanceolate, 9–12 (–15) × 2–3.5 mm, apex appendage well-developed, (1–) 2–4 mm;</text>
      <biological_entity id="o31105" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s9" to="15" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s9" to="12" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31106" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity constraint="apex" id="o31107" name="appendage" name_original="appendage" src="d0_s9" type="structure">
        <character is_modifier="false" name="development" src="d0_s9" value="well-developed" value_original="well-developed" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s9" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals broadly obovate, 12–15 (–20) × (5–) 7–11 mm, apex rounded;</text>
      <biological_entity id="o31108" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o31109" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="20" to_unit="mm" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_width" src="d0_s10" to="7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s10" to="11" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31110" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>median filament pairs 4–6 mm, slightly dilated basally;</text>
      <biological_entity id="o31111" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity constraint="median" id="o31112" name="filament" name_original="filament" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="slightly; basally" name="shape" src="d0_s11" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers linear, 3–4 mm;</text>
      <biological_entity id="o31113" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o31114" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s12" value="linear" value_original="linear" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>gynophore usually obsolete, rarely to 2 mm.</text>
      <biological_entity id="o31115" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o31116" name="gynophore" name_original="gynophore" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s13" value="obsolete" value_original="obsolete" />
        <character char_type="range_value" from="0" from_unit="mm" modifier="rarely" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits oblong, somewhat inflated, (0.8–) 1–2 (–2.5) cm × 5.5–7.5 mm, (fleshy green, thick, leathery), base and apex obtuse to subacute;</text>
      <biological_entity id="o31117" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s14" value="inflated" value_original="inflated" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_length" src="d0_s14" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s14" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s14" to="2" to_unit="cm" />
        <character char_type="range_value" from="5.5" from_unit="mm" name="width" src="d0_s14" to="7.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o31118" name="base" name_original="base" src="d0_s14" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s14" to="subacute" />
      </biological_entity>
      <biological_entity id="o31119" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s14" to="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves (covered with well-developed vesicles), not veined;</text>
      <biological_entity id="o31120" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="veined" value_original="veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>replum rounded;</text>
      <biological_entity id="o31121" name="replum" name_original="replum" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>septum complete;</text>
      <biological_entity id="o31122" name="septum" name_original="septum" src="d0_s17" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s17" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovules 16–44 per ovary;</text>
      <biological_entity id="o31123" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o31124" from="16" name="quantity" src="d0_s18" to="44" />
      </biological_entity>
      <biological_entity id="o31124" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>style 2–5 (–7) mm, not flattened basally.</text>
      <biological_entity id="o31125" name="style" name_original="style" src="d0_s19" type="structure">
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s19" to="7" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s19" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s19" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 4–5 mm diam.;</text>
      <biological_entity id="o31126" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s20" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>wing 1–1.5 mm. 2n = 24.</text>
      <biological_entity id="o31127" name="wing" name_original="wing" src="d0_s21" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s21" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o31128" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Dec–Mar.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Mar" from="Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open grounds, fields, flood plains, roadsides, slightly saline alluvial silt, ditch banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open grounds" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="flood plains" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="saline alluvial silt" modifier="slightly" />
        <character name="habitat" value="ditch banks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3.</number>
  <discussion>Selenia grandis, which is restricted to the lower valley of the Rio Grande, is easily distinguished from other species of the genus by the presence of vesicles on fruits and by the persistent sepals.</discussion>
  
</bio:treatment>