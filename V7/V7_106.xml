<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">48</other_info_on_meta>
    <other_info_on_meta type="mention_page">90</other_info_on_meta>
    <other_info_on_meta type="mention_page">100</other_info_on_meta>
    <other_info_on_meta type="mention_page">107</other_info_on_meta>
    <other_info_on_meta type="mention_page">109</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="treatment_page">106</other_info_on_meta>
    <other_info_on_meta type="illustration_page">105</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="(Fries) A. Kerner" date="1860" rank="section">hastatae</taxon_name>
    <taxon_name authority="Cockerell ex A. Heller" date="unknown" rank="species">eastwoodiae</taxon_name>
    <place_of_publication>
      <publication_title>Cat. N. Amer. Pl. ed.</publication_title>
      <place_in_publication>3, 89. 1910</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section hastatae;species eastwoodiae</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445698</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Bebb" date="unknown" rank="species">californica</taxon_name>
    <place_of_publication>
      <publication_title>Willows Calif.,</publication_title>
      <place_in_publication>89. 1879,</place_in_publication>
      <other_info_on_pub>not Lesquereux 1878</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Salix;species californica;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 0.6–4 m.</text>
      <biological_entity id="o9148" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0.6" from_unit="m" name="some_measurement" src="d0_s0" to="4" to_unit="m" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches yellow, red, or violet, not to strongly glaucous (slightly glossy), pilose;</text>
      <biological_entity id="o9149" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o9150" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="violet" value_original="violet" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="violet" value_original="violet" />
        <character is_modifier="false" modifier="not to strongly" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branchlets yellow-green or redbrown, pilose to villous (inner membranaceous bud-scale layer free, separating from outer layer).</text>
      <biological_entity id="o9151" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o9152" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s2" to="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules foliaceous, apex acute;</text>
      <biological_entity id="o9153" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9154" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o9155" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole convex to flat, or shallowly grooved adaxially, 3–8–17 mm, pilose or villous adaxially;</text>
      <biological_entity id="o9156" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9157" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s4" to="flat" />
        <character is_modifier="false" modifier="shallowly; adaxially" name="architecture" src="d0_s4" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="8-17" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>largest medial blade narrowly oblong, oblong, or elliptic, 21–57–99 × 6–20–37 mm, 1.9–2.9–5 times as long as wide, base rounded, convex, subcordate, or cordate, margins flat or slightly revolute, entire or serrulate (with relatively short, slender teeth), apex acuminate, acute, or convex, abaxial surface not glaucous, pilose, short-silky, or densely woolly-tomentose to glabrescent, hairs wavy, adaxial dull or slightly glossy, sparsely to densely silky-tomentose, midrib remaining hairy;</text>
      <biological_entity id="o9158" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="largest medial" id="o9159" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="21" from_unit="mm" name="length" src="d0_s5" to="57-99" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s5" to="20-37" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="1.9-2.9-5" value_original="1.9-2.9-5" />
      </biological_entity>
      <biological_entity id="o9160" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o9161" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o9162" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o9163" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character char_type="range_value" from="densely woolly-tomentose" name="pubescence" src="d0_s5" to="glabrescent" />
        <character char_type="range_value" from="densely woolly-tomentose" name="pubescence" src="d0_s5" to="glabrescent" />
      </biological_entity>
      <biological_entity id="o9164" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9165" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s5" value="glossy" value_original="glossy" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s5" value="silky-tomentose" value_original="silky-tomentose" />
      </biological_entity>
      <biological_entity id="o9166" name="midrib" name_original="midrib" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal blade margins entire or serrulate;</text>
      <biological_entity id="o9167" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="blade" id="o9168" name="margin" name_original="margins" src="d0_s6" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrulate" value_original="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>juvenile blade yellowish green, very densely long-silky or woolly abaxially, hairs white (sometimes yellowish).</text>
      <biological_entity id="o9169" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o9170" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish green" value_original="yellowish green" />
        <character is_modifier="false" modifier="very densely" name="pubescence" src="d0_s7" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="woolly" value_original="woolly" />
      </biological_entity>
      <biological_entity id="o9171" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Catkins flowering as leaves emerge;</text>
      <biological_entity id="o9173" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>staminate stout or subglobose, 9.5–36.5 × 7–15 mm, flowering branchlet 1.5–7 mm;</text>
      <biological_entity id="o9172" name="catkin" name_original="catkins" src="d0_s8" type="structure">
        <character constraint="as leaves" constraintid="o9173" is_modifier="false" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate densely or moderately densely flowered, stout or subglobose, 11–51 × 8–16 mm, flowering branchlet 2–12 mm;</text>
      <biological_entity id="o9174" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s9" value="subglobose" value_original="subglobose" />
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9175" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s10" value="or" value_original="or" />
        <character is_modifier="false" modifier="moderately densely" name="architecture" src="d0_s10" value="flowered" value_original="flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s10" value="stout" value_original="stout" />
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract brown or black, 1.4–2.8 mm, apex rounded or acute, abaxially hairy, hairs straight or wavy.</text>
      <biological_entity constraint="floral" id="o9176" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="black" value_original="black" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s11" to="2.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9177" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o9178" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s11" value="wavy" value_original="wavy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: adaxial nectary narrowly oblong to oblong, 0.5–1.1 mm;</text>
      <biological_entity id="o9179" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9180" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s12" to="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, glabrous or hairy basally;</text>
      <biological_entity id="o9181" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o9182" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers yellow or purple turning yellow, 0.5–0.9 mm.</text>
      <biological_entity id="o9183" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o9184" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purple turning yellow" value_original="purple turning yellow" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: adaxial nectary narrowly oblong to oblong, 0.5–1.1 mm, longer than or equal to stipe;</text>
      <biological_entity id="o9185" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9186" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character char_type="range_value" from="narrowly oblong" name="shape" src="d0_s15" to="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s15" to="1.1" to_unit="mm" />
        <character constraint="than or equal to stipe" constraintid="o9187" is_modifier="false" name="length_or_size" src="d0_s15" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o9187" name="stipe" name_original="stipe" src="d0_s15" type="structure">
        <character is_modifier="true" name="variability" src="d0_s15" value="equal" value_original="equal" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stipe 0.2–1.6 mm;</text>
      <biological_entity id="o9188" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9189" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary pyriform, short or long-silky to glabrescent, beak gradually tapering to or slightly bulged below styles;</text>
      <biological_entity id="o9190" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9191" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
        <character name="height_or_length_or_size" src="d0_s17" value="long-silky to glabrescent" value_original="long-silky to glabrescent" />
      </biological_entity>
      <biological_entity id="o9192" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="gradually; slightly" name="shape" src="d0_s17" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o9193" name="style" name_original="styles" src="d0_s17" type="structure" />
      <relation from="o9192" id="r661" name="bulged" negation="false" src="d0_s17" to="o9193" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 12–16 per ovary;</text>
      <biological_entity id="o9194" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9195" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o9196" from="12" name="quantity" src="d0_s18" to="16" />
      </biological_entity>
      <biological_entity id="o9196" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles 0.5–1.5 mm;</text>
      <biological_entity id="o9197" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9198" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s19" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas flat, abaxially non-papillate with rounded tip, slenderly or broadly cylindrical, or 2 plump lobes, 0.18–0.39–0.76 mm (evidentially two size classes).</text>
      <biological_entity id="o9199" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o9200" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o9201" is_modifier="false" modifier="abaxially" name="relief" src="d0_s20" value="non-papillate" value_original="non-papillate" />
        <character is_modifier="false" modifier="slenderly; broadly" name="shape" notes="" src="d0_s20" value="cylindrical" value_original="cylindrical" />
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
        <character char_type="range_value" from="0.18" from_unit="mm" name="some_measurement" notes="" src="d0_s20" to="0.39-0.76" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9201" name="tip" name_original="tip" src="d0_s20" type="structure">
        <character is_modifier="true" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o9202" name="lobe" name_original="lobes" src="d0_s20" type="structure">
        <character is_modifier="true" name="size" src="d0_s20" value="plump" value_original="plump" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 4–10 mm. 2n = 76.</text>
      <biological_entity id="o9203" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s21" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9204" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="76" value_original="76" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering mid May-late Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="late Jul" from="mid May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine and subalpine meadows, streams, lakeshores, talus slopes, granite substrate</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine" />
        <character name="habitat" value="subalpine meadows" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="lakeshores" />
        <character name="habitat" value="talus slopes" />
        <character name="habitat" value="granite substrate" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600-3800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3800" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Mont., Nev., Oreg., Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>53.</number>
  <other_name type="past_name">fastwoodiae</other_name>
  <other_name type="common_name">Sierra willow</other_name>
  <discussion>Salix eastwoodiae and S. commutata are distinct species with different ploidal levels, the former tetraploid and the latter diploid; where they come into contact in the Pacific Northwest, hybrids occur and vegetative plants are often difficult to separate. See comparison below. The most important difference is that ovaries of S. eastwoodiae usually are silky turning glabrescent in age and those of S. commutata are glabrous. Populations occur in Oregon with both glabrous and hairy ovaries without any other evident differences. There are also unusual specimens, which are often tentatively identified as S. eastwoodiae, that have glabrous ovaries and patches of hairs at the base and on the sutures. The possibility that they are hybrids between S. eastwoodiae and S. boothii, S. commutata, or S. lemmonii needs study.</discussion>
  <discussion>Salix commutata is distinguished from S. eastwoodiae by having leaf blades sometimes amphistomatous, 1.5–3.4 times as long as wide, teeth 0–19 per cm, adaxial surfaces glabrous or pilose to villous, floral bracts tawny to brown, staminate and pistillate adaxial nectaries oblong to square, and ovaries glabrous; S. eastwoodiae has leaf blades hypostomatous, 1.8–5 times as long as wide, teeth 0–10 per cm, adaxial surfaces tomentose or long-silky, floral bracts brown to black, staminate and pistillate adaxial nectaries narrowly oblong to oblong, and ovaries silky to glabrescent.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Salix eastwoodiae forms natural hybrids with S. arizonica, S. boothii, and S. commutata.</discussion>
  <discussion>Salix eastwoodiae × S. lasiandra was found in Sierra County, California, growing with both parents in a wetland along a disturbed roadside. It had leaf indumentum and hair color of S. eastwoodiae and leaf shape and margins of S. lasiandra. Catkins of this intersubgeneric hybrid were teratological and presumably infertile.</discussion>
  
</bio:treatment>