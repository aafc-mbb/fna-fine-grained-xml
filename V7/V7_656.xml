<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="treatment_page">435</other_info_on_meta>
    <other_info_on_meta type="illustration_page">436</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">brassiceae</taxon_name>
    <taxon_name authority="C. Presl" date="1826" rank="genus">erucastrum</taxon_name>
    <taxon_name authority="(Willdenow) O. E. Schulz" date="1916" rank="species">gallicum</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Jahrb. Syst.</publication_title>
      <place_in_publication>54(Beibl. 119): 56. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe brassiceae;genus erucastrum;species gallicum</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416515</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sisymbrium</taxon_name>
    <taxon_name authority="Willdenow" date="unknown" rank="species">gallicum</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Pl.,</publication_title>
      <place_in_publication>678. 1809</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Sisymbrium;species gallicum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Erucastrum</taxon_name>
    <taxon_name authority="Schimper &amp; Spenner" date="unknown" rank="species">pollichii</taxon_name>
    <taxon_hierarchy>genus Erucastrum;species pollichii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants sparsely to densely pubescent, trichomes stiff, recurved (or retrorsely appressed).</text>
      <biological_entity id="o0" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1" name="trichome" name_original="trichomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s0" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or ascending, unbranched or branched (few to several), 0.9–6.5 (–8) dm.</text>
      <biological_entity id="o2" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character char_type="range_value" from="6.5" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s1" to="8" to_unit="dm" />
        <character char_type="range_value" from="0.9" from_unit="dm" name="some_measurement" src="d0_s1" to="6.5" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Basal leaves: blade oblanceolate, 3–28 cm × 8–110 mm, margins dentate to deeply lobed or pinnatifid, lobes 3–10 each side, smaller than terminal, lobe margins crenate or dentate, surfaces sparsely pubescent.</text>
      <biological_entity constraint="basal" id="o3" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4" name="blade" name_original="blade" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s2" to="28" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s2" to="110" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="dentate" name="shape" src="d0_s2" to="deeply lobed or pinnatifid" />
      </biological_entity>
      <biological_entity id="o6" name="lobe" name_original="lobes" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s2" to="10" />
        <character constraint="than terminal" constraintid="o8" is_modifier="false" name="size" notes="" src="d0_s2" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o7" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity id="o8" name="terminal" name_original="terminal" src="d0_s2" type="structure" />
      <biological_entity constraint="lobe" id="o9" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o10" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Cauline leaves similar to basal, distal shortly petiolate or sessile, blade smaller (distalmost 1–2 cm, passing into bracts, leaflike, linear, margins entire).</text>
      <biological_entity constraint="cauline" id="o11" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o12" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="distal" id="o13" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o11" id="r0" name="to" negation="false" src="d0_s3" to="o12" />
    </statement>
    <statement id="d0_s4">
      <text>Fruiting pedicels (3–) 5–10 (–20) mm.</text>
      <biological_entity id="o14" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="size" src="d0_s3" value="smaller" value_original="smaller" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="20" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
      <relation from="o14" id="r1" name="fruiting" negation="false" src="d0_s4" to="o15" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers: sepals 3–5 × 1–2 mm, sparsely hispid apically;</text>
      <biological_entity id="o16" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o17" name="sepal" name_original="sepals" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely; apically" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>petals white to pale-yellow, 4–8 × 1.5–3 mm;</text>
      <biological_entity id="o18" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o19" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s6" to="pale-yellow" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s6" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>filaments 3.5–5.5 mm.</text>
      <biological_entity id="o20" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o21" name="filament" name_original="filaments" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fruits slightly torulose, 1–4.5 cm × 1–2 (–2.7) mm;</text>
      <biological_entity id="o22" name="fruit" name_original="fruits" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="torulose" value_original="torulose" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s8" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>terminal segment 1.5–4 mm;</text>
      <biological_entity constraint="terminal" id="o23" name="segment" name_original="segment" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>style 1–3 mm.</text>
      <biological_entity id="o24" name="style" name_original="style" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seeds reddish-brown, 1.1–1.5 × 0.7–0.8 mm, alveolate.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 30.</text>
      <biological_entity id="o25" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s11" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s11" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="alveolate" value_original="alveolate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Sep(-Dec in south, fruiting shortly after).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" modifier="in south" to="Sep" from="Mar" constraint=" (-Dec" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Roadsides, waste places, disturbed sites, along railroads, fields, gardens, orchards, beaches of Great Lakes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="disturbed sites" constraint="along railroads , fields , gardens , orchards , beaches of great lakes" />
        <character name="habitat" value="railroads" />
        <character name="habitat" value="fields" />
        <character name="habitat" value="gardens" />
        <character name="habitat" value="orchards" />
        <character name="habitat" value="beaches" constraint="of great lakes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Ont., P.E.I., Que., Sask.; Ala., Calif., Conn., Fla., Idaho, Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Mont., N.H., N.Y., N.Dak., Ohio, Oreg., Pa., S.Dak., Tex., Vt., Wash., W.Va., Wis., Wyo.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1.</number>
  <other_name type="common_name">Dog mustard</other_name>
  <other_name type="common_name">rocket-weed</other_name>
  <other_name type="common_name">bracted-rocket</other_name>
  <other_name type="common_name">French rocket</other_name>
  <discussion>A European native, Erucastrum gallicum was first recorded for North America from Massachusetts and Wisconsin (see J. O. Luken et al. 1993 for history of introduction and spread). It is naturalized in all the provinces of Canada and in parts of the United States, particularly the Midwest. It is an allopolyploid, with the n = 7 component from Diplotaxis erucoides/D. cossoniana and n = 8 from the E. nasturtiifolium complex (S. I. Warwick and L. D. Black 1993). I have not seen specimens from Maryland.</discussion>
  
</bio:treatment>