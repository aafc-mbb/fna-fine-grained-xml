<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="mention_page">104</other_info_on_meta>
    <other_info_on_meta type="mention_page">112</other_info_on_meta>
    <other_info_on_meta type="mention_page">113</other_info_on_meta>
    <other_info_on_meta type="treatment_page">123</other_info_on_meta>
    <other_info_on_meta type="illustration_page">124</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="Barratt ex Hooker" date="1838" rank="section">cordatae</taxon_name>
    <taxon_name authority="Nuttall" date="1842" rank="species">lutea</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Sylv.</publication_title>
      <place_in_publication>1: 63, plate 19. 1842</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section cordatae;species lutea</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242445781</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Muhlenberg" date="unknown" rank="species">cordata</taxon_name>
    <taxon_name authority="Bebb" date="unknown" rank="variety">watsonii</taxon_name>
    <taxon_hierarchy>genus Salix;species cordata;variety watsonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Michaux" date="unknown" rank="species">eriocephala</taxon_name>
    <taxon_name authority="(Bebb) Dorn" date="unknown" rank="variety">watsonii</taxon_name>
    <taxon_hierarchy>genus Salix;species eriocephala;variety watsonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="Muhlenberg" date="unknown" rank="species">lutea</taxon_name>
    <taxon_name authority="(Bebb) Jepson" date="unknown" rank="variety">watsonii</taxon_name>
    <taxon_hierarchy>genus Salix;species lutea;variety watsonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Salix</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rigida</taxon_name>
    <taxon_name authority="(Bebb) Cronquist" date="unknown" rank="variety">watsonii</taxon_name>
    <taxon_hierarchy>genus Salix;species rigida;variety watsonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 3–7 m, (sometimes forming clones by stem fragmentation).</text>
      <biological_entity id="o35181" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="m" name="some_measurement" src="d0_s0" to="7" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches (sometimes ± brittle at base) yellow-gray, yellowbrown, or gray-brown, (sometimes weakly glaucous, with sparkling wax crystals), glabrous;</text>
      <biological_entity id="o35182" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o35183" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow-gray" value_original="yellow-gray" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branchlets redbrown or brownish, glabrous or pilose, (inner membranaceous bud-scale layer free, separating from outer layer).</text>
      <biological_entity id="o35184" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o35185" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules rudimentary or foliaceous on early ones, foliaceous on late ones, apex acute or rounded;</text>
      <biological_entity id="o35186" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o35187" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s3" value="rudimentary" value_original="rudimentary" />
        <character constraint="on ones" constraintid="o35188" is_modifier="false" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
        <character constraint="on ones" constraintid="o35189" is_modifier="false" name="architecture" notes="" src="d0_s3" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o35188" name="one" name_original="ones" src="d0_s3" type="structure" />
      <biological_entity id="o35189" name="one" name_original="ones" src="d0_s3" type="structure" />
      <biological_entity id="o35190" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole convex to flat, or shallowly grooved adaxially, 4–19 mm, pilose, velvety, or pubescent to glabrescent adaxially;</text>
      <biological_entity id="o35191" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o35192" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="convex" name="shape" src="d0_s4" to="flat" />
        <character is_modifier="false" modifier="shallowly; adaxially" name="architecture" src="d0_s4" value="grooved" value_original="grooved" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="19" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s4" to="glabrescent" />
        <character char_type="range_value" from="pubescent" modifier="adaxially" name="pubescence" src="d0_s4" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>largest medial blade (sometimes amphistomatous), lorate, narrowly elliptic, elliptic, lanceolate, or narrowly oblanceolate, 42–90 × 8–32 mm, 2.8–3.9–5.6 times as long as wide, base rounded, convex, or subcordate, margins flat, entire, serrulate, crenulate, or sinuate, apex acuminate to acute, abaxial surface glaucous, glabrous, pilose, or sparsely long-silky, hairs straight, adaxial dull or slightly glossy, glabrous, pilose, sparsely long-silky, especially midrib;</text>
      <biological_entity id="o35193" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="lorate" value_original="lorate" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="42" from_unit="mm" name="length" src="d0_s5" to="90" to_unit="mm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="32" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="2.8-3.9-5.6" value_original="2.8-3.9-5.6" />
      </biological_entity>
      <biological_entity constraint="largest medial" id="o35194" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <biological_entity id="o35195" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o35196" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sinuate" value_original="sinuate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sinuate" value_original="sinuate" />
      </biological_entity>
      <biological_entity id="o35197" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s5" to="acute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o35198" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="long-silky" value_original="long-silky" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="long-silky" value_original="long-silky" />
      </biological_entity>
      <biological_entity id="o35199" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" name="position" src="d0_s5" value="adaxial" value_original="adaxial" />
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s5" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="long-silky" value_original="long-silky" />
      </biological_entity>
      <biological_entity id="o35200" name="midrib" name_original="midrib" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal blade margins entire, serrulate, or crenulate;</text>
      <biological_entity id="o35201" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="blade" id="o35202" name="margin" name_original="margins" src="d0_s6" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>juvenile blade reddish or yellowish green, glabrous or sparsely to moderately densely long-silky throughout, hairs white.</text>
      <biological_entity id="o35203" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o35204" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish green" value_original="yellowish green" />
        <character char_type="range_value" from="glabrous or" modifier="throughout" name="pubescence" src="d0_s7" to="sparsely moderately densely long-silky" />
      </biological_entity>
      <biological_entity id="o35205" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Catkins flowering as leaves emerge;</text>
      <biological_entity id="o35207" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>staminate stout, slender, or subglobose, 10–45 × 6–12 mm, flowering branchlet 0.5–2 mm;</text>
      <biological_entity id="o35206" name="catkin" name_original="catkins" src="d0_s8" type="structure">
        <character constraint="as leaves" constraintid="o35207" is_modifier="false" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate loosely to densely flowered, stout or subglobose, 13.5–38 × 7–15 mm, flowering branchlet 0.5–7 mm;</text>
      <biological_entity id="o35208" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s9" value="stout" value_original="stout" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="shape" src="d0_s9" value="subglobose" value_original="subglobose" />
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="pistillate" name="architecture" src="d0_s10" to="loosely densely flowered" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s10" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o35209" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subglobose" value_original="subglobose" />
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract brown, tawny, or bicolor, 0.6–1.2 mm, apex acute or rounded, abaxially glabrous or sparsely hairy, hairs curly.</text>
      <biological_entity constraint="floral" id="o35210" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="bicolor" value_original="bicolor" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="bicolor" value_original="bicolor" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35211" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o35212" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="curly" value_original="curly" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: adaxial nectary narrowly oblong, oblong, square, or flask-shaped, 0.4–0.9 mm;</text>
      <biological_entity id="o35213" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o35214" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s12" value="flask--shaped" value_original="flask--shaped" />
        <character is_modifier="false" name="shape" src="d0_s12" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s12" value="flask--shaped" value_original="flask--shaped" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct or connate less than 1/2 their lengths, glabrous;</text>
      <biological_entity id="o35215" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o35216" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="fusion" src="d0_s13" value="connate" value_original="connate" />
        <character char_type="range_value" from="0" name="lengths" src="d0_s13" to="1/2" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers yellow or purple turning yellow, (ellipsoid or globose), 0.4–0.8 mm.</text>
      <biological_entity id="o35217" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o35218" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purple turning yellow" value_original="purple turning yellow" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: adaxial nectary oblong, square, or ovate, 0.3–0.9 mm, shorter than stipe;</text>
      <biological_entity id="o35219" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o35220" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="square" value_original="square" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.9" to_unit="mm" />
        <character constraint="than stipe" constraintid="o35221" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o35221" name="stipe" name_original="stipe" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>stipe 0.9–3.8 mm;</text>
      <biological_entity id="o35222" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o35223" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s16" to="3.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary pyriform or ovoid, glabrous, beak gradually tapering to styles;</text>
      <biological_entity id="o35224" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o35225" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o35226" name="beak" name_original="beak" src="d0_s17" type="structure">
        <character constraint="to styles" constraintid="o35227" is_modifier="false" modifier="gradually" name="shape" src="d0_s17" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o35227" name="style" name_original="styles" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 12–24 per ovary;</text>
      <biological_entity id="o35228" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o35229" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o35230" from="12" name="quantity" src="d0_s18" to="24" />
      </biological_entity>
      <biological_entity id="o35230" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles 0.1–0.6 mm;</text>
      <biological_entity id="o35231" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o35232" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s19" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas flat, abaxially non-papillate with rounded tip, or 2 plump lobes, 0.14–0.2–0.3 mm.</text>
      <biological_entity id="o35233" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o35234" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s20" value="flat" value_original="flat" />
        <character constraint="with tip" constraintid="o35235" is_modifier="false" modifier="abaxially" name="relief" src="d0_s20" value="non-papillate" value_original="non-papillate" />
        <character char_type="range_value" from="0.14" from_unit="mm" name="some_measurement" notes="" src="d0_s20" to="0.2-0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o35235" name="tip" name_original="tip" src="d0_s20" type="structure">
        <character is_modifier="true" name="shape" src="d0_s20" value="rounded" value_original="rounded" />
        <character name="quantity" src="d0_s20" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o35236" name="lobe" name_original="lobes" src="d0_s20" type="structure">
        <character is_modifier="true" name="size" src="d0_s20" value="plump" value_original="plump" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 3–5 mm. 2n = 38.</text>
      <biological_entity id="o35237" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s21" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o35238" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Banks of streams, meadows, hillsides, gullies, sandy-clay, sandy or rocky substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="banks" constraint="of streams , meadows , hillsides , gullies , sandy-clay , sandy or rocky substrates" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="hillsides" />
        <character name="habitat" value="gullies" />
        <character name="habitat" value="sandy-clay" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="rocky substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>72.</number>
  <other_name type="common_name">Yellow willow</other_name>
  <discussion>The possible occurrence of Salix lutea in Ginkgo Petrified Forest Park, Washington, needs to be investigated.</discussion>
  <discussion>Hybrids:</discussion>
  <discussion>Salix lutea forms natural hybrids with S. arizonica.</discussion>
  
</bio:treatment>