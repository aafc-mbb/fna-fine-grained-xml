<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">278</other_info_on_meta>
    <other_info_on_meta type="treatment_page">295</other_info_on_meta>
    <other_info_on_meta type="illustration_page">289</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="de Candolle" date="unknown" rank="tribe">arabideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">draba</taxon_name>
    <taxon_name authority="S. Watson in W. H. Brewer et al." date="1880" rank="species">aureola</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Brewer et al., Bot. California</publication_title>
      <place_in_publication>2: 430. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe arabideae;genus draba;species aureola</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094720</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Draba</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">aureola</taxon_name>
    <taxon_name authority="L. F. Henderson" date="unknown" rank="variety">paniculata</taxon_name>
    <taxon_hierarchy>genus Draba;species aureola;variety paniculata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>(short-lived);</text>
      <biological_entity id="o10959" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex simple or branched (poorly developed, with persistent dry leaves);</text>
    </statement>
    <statement id="d0_s3">
      <text>not scapose.</text>
      <biological_entity id="o10960" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="scapose" value_original="scapose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems unbranched or branched, 0.3–1.5 dm, hirsute throughout, trichomes simple and long-stalked, 2–4-rayed, 0.2–1.5 mm.</text>
      <biological_entity id="o10961" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="some_measurement" src="d0_s4" to="1.5" to_unit="dm" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o10962" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="long-stalked" value_original="long-stalked" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves (forming dense clusters);</text>
    </statement>
    <statement id="d0_s6">
      <text>rosulate;</text>
    </statement>
    <statement id="d0_s7">
      <text>sessile;</text>
      <biological_entity constraint="basal" id="o10963" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blade oblanceolate to linear, (0.7–) 1–2.5 (–3.2) cm × 2–3.5 (–5) mm, margins entire, (ciliate, trichomes simple and 2-rayed, 0.5–1.5 mm), surfaces densely hirsute, abaxially with stalked, 3–5-rayed trichomes, 0.1–0.5 mm, adaxially with simple and long-stalked, 2-rayed trichomes, to 1 mm, with smaller, 3–5-rayed ones.</text>
      <biological_entity id="o10964" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s8" to="linear" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="atypical_length" src="d0_s8" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s8" to="3.2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s8" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10965" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10966" name="surface" name_original="surfaces" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s8" value="hirsute" value_original="hirsute" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10967" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o10968" name="trichome" name_original="trichomes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="long-stalked" value_original="long-stalked" />
      </biological_entity>
      <biological_entity id="o10969" name="one" name_original="ones" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="smaller" value_original="smaller" />
      </biological_entity>
      <relation from="o10966" id="r771" modifier="abaxially" name="with" negation="false" src="d0_s8" to="o10967" />
      <relation from="o10966" id="r772" modifier="adaxially" name="with" negation="false" src="d0_s8" to="o10968" />
      <relation from="o10966" id="r773" name="with" negation="false" src="d0_s8" to="o10969" />
    </statement>
    <statement id="d0_s9">
      <text>Cauline leaves 5–33;</text>
    </statement>
    <statement id="d0_s10">
      <text>sessile;</text>
      <biological_entity constraint="cauline" id="o10970" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s9" to="33" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>blade oblong to linear, margins entire, surfaces pubescent as basal.</text>
      <biological_entity id="o10971" name="blade" name_original="blade" src="d0_s11" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s11" to="linear" />
      </biological_entity>
      <biological_entity id="o10972" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o10973" name="surface" name_original="surfaces" src="d0_s11" type="structure">
        <character constraint="as basal" constraintid="o10974" is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o10974" name="basal" name_original="basal" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Racemes 12–83-flowered, ebracteate or proximalmost 1–9 flowers bracteate, slightly or not elongated in fruit;</text>
      <biological_entity id="o10975" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="12-83-flowered" value_original="12-83-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="position" src="d0_s12" value="proximalmost" value_original="proximalmost" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s12" to="9" />
      </biological_entity>
      <biological_entity id="o10976" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="bracteate" value_original="bracteate" />
        <character constraint="in fruit" constraintid="o10977" is_modifier="false" modifier="slightly; not" name="length" src="d0_s12" value="elongated" value_original="elongated" />
      </biological_entity>
      <biological_entity id="o10977" name="fruit" name_original="fruit" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>rachis not flexuous, hirsute as stem.</text>
      <biological_entity id="o10979" name="stem" name_original="stem" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Fruiting pedicels horizontal to divaricate, usually straight, rarely curved upward, (3–) 5–12 (–19) mm, hirsute as stem.</text>
      <biological_entity id="o10978" name="rachis" name_original="rachis" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
        <character constraint="as stem" constraintid="o10979" is_modifier="false" name="pubescence" src="d0_s13" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o10980" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <biological_entity id="o10981" name="stem" name_original="stem" src="d0_s14" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s14" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="arrangement" src="d0_s14" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="usually" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="rarely" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character char_type="range_value" from="3" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="12" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="19" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <relation from="o10978" id="r774" name="fruiting" negation="false" src="d0_s14" to="o10980" />
    </statement>
    <statement id="d0_s15">
      <text>Flowers: sepals ovate, 2.5–4 mm, glabrous;</text>
      <biological_entity id="o10982" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o10983" name="sepal" name_original="sepals" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>petals bright-yellow, linear to linear-oblanceolate, 4–6 × 0.5–1 (–1.2) mm;</text>
      <biological_entity id="o10984" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o10985" name="petal" name_original="petals" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="bright-yellow" value_original="bright-yellow" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s16" to="linear-oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s16" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s16" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers narrowly oblong, 0.8–1 mm.</text>
      <biological_entity id="o10986" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o10987" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s17" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s17" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits oblong or, rarely, oblong-ovate, plane, flattened, (6–) 9–14 (–16) × 3–5 mm;</text>
      <biological_entity id="o10988" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="oblong" value_original="oblong" />
        <character name="shape" src="d0_s18" value="," value_original="," />
        <character is_modifier="false" name="shape" src="d0_s18" value="oblong-ovate" value_original="oblong-ovate" />
        <character is_modifier="false" name="shape" src="d0_s18" value="plane" value_original="plane" />
        <character is_modifier="false" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character char_type="range_value" from="6" from_unit="mm" name="atypical_length" src="d0_s18" to="9" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s18" to="16" to_unit="mm" />
        <character char_type="range_value" from="9" from_unit="mm" name="length" src="d0_s18" to="14" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s18" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>valves pubescent, trichomes stalked, 2–4-rayed, 0.07–0.5 mm;</text>
      <biological_entity id="o10989" name="valve" name_original="valves" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o10990" name="trichome" name_original="trichomes" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="stalked" value_original="stalked" />
        <character char_type="range_value" from="0.07" from_unit="mm" name="some_measurement" src="d0_s19" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 10–20 per ovary;</text>
      <biological_entity id="o10991" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o10992" from="10" name="quantity" src="d0_s20" to="20" />
      </biological_entity>
      <biological_entity id="o10992" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style (0.6–) 1–1.6 (–2) mm.</text>
      <biological_entity id="o10993" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="atypical_some_measurement" src="d0_s21" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s21" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s21" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds oblong, 1.4–1.9 × 0.8–1.1 mm. 2n = 20.</text>
      <biological_entity id="o10994" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s22" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s22" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10995" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Jul–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="Jul" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open conifer forests, alpine meadows and moraines, talus slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open conifer forests" />
        <character name="habitat" value="alpine meadows" />
        <character name="habitat" value="moraines" />
        <character name="habitat" value="talus slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>2200-3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="2200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14.</number>
  <discussion>Draba aureola is known from Lassen, Shasta, and Siskiyou counties in California, Deschutes County in Oregon, and Lewis and Pierce counties in Washington.</discussion>
  
</bio:treatment>