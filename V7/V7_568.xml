<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">349</other_info_on_meta>
    <other_info_on_meta type="mention_page">350</other_info_on_meta>
    <other_info_on_meta type="mention_page">357</other_info_on_meta>
    <other_info_on_meta type="mention_page">359</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="mention_page">393</other_info_on_meta>
    <other_info_on_meta type="mention_page">410</other_info_on_meta>
    <other_info_on_meta type="treatment_page">397</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Al-Shehbaz" date="unknown" rank="tribe">boechereae</taxon_name>
    <taxon_name authority="Á. Löve &amp; D. Löve" date="1976" rank="genus">boechera</taxon_name>
    <taxon_name authority="(Tidestrom) Windham &amp; Al-Shehbaz" date="2007" rank="species">pinetorum</taxon_name>
    <place_of_publication>
      <publication_title>Harvard Pap. Bot.</publication_title>
      <place_in_publication>11: 271. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe boechereae;genus boechera;species pinetorum</taxon_hierarchy>
    <other_info_on_name type="fna_id">250094501</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="Tidestrom" date="unknown" rank="species">pinetorum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>36: 182. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arabis;species pinetorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">divaricarpa</taxon_name>
    <taxon_name authority="(Tidestrom) B. Boivin" date="unknown" rank="variety">pinetorum</taxon_name>
    <taxon_hierarchy>genus Arabis;species divaricarpa;variety pinetorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arabis</taxon_name>
    <taxon_name authority="Hornemann" date="unknown" rank="species">holboellii</taxon_name>
    <taxon_name authority="(Tidestrom) Rollins" date="unknown" rank="variety">pinetorum</taxon_name>
    <taxon_hierarchy>genus Arabis;species holboellii;variety pinetorum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Boechera</taxon_name>
    <taxon_name authority="(Hornemann) Á. Löve &amp; D. Löve" date="unknown" rank="species">holboellii</taxon_name>
    <taxon_name authority="(Tidestrom) Dorn" date="unknown" rank="variety">pinetorum</taxon_name>
    <taxon_hierarchy>genus Boechera;species holboellii;variety pinetorum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>short-lived;</text>
    </statement>
    <statement id="d0_s2">
      <text>apomictic;</text>
      <biological_entity id="o28588" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s1" value="short-lived" value_original="short-lived" />
        <character is_modifier="false" name="reproduction" src="d0_s2" value="apomictic" value_original="apomictic" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>caudex not woody.</text>
      <biological_entity id="o28589" name="caudex" name_original="caudex" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Stems usually 1 per caudex branch, arising from center of rosette near ground surface, 2–8 (–9.6) dm, densely pubescent proximally, trichomes simple and short-stalked, 2–4-rayed, to 1 mm, sparsely pubescent or glabrous distally.</text>
      <biological_entity id="o28590" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character constraint="per caudex branch" constraintid="o28591" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character constraint="from center" constraintid="o28592" is_modifier="false" name="orientation" notes="" src="d0_s4" value="arising" value_original="arising" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" notes="" src="d0_s4" to="9.6" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" notes="" src="d0_s4" to="8" to_unit="dm" />
        <character is_modifier="false" modifier="densely; proximally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="caudex" id="o28591" name="branch" name_original="branch" src="d0_s4" type="structure" />
      <biological_entity id="o28592" name="center" name_original="center" src="d0_s4" type="structure" />
      <biological_entity id="o28593" name="rosette" name_original="rosette" src="d0_s4" type="structure" />
      <biological_entity id="o28594" name="ground" name_original="ground" src="d0_s4" type="structure" />
      <biological_entity id="o28595" name="surface" name_original="surface" src="d0_s4" type="structure" />
      <biological_entity id="o28596" name="trichome" name_original="trichomes" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o28592" id="r1930" name="part_of" negation="false" src="d0_s4" to="o28593" />
      <relation from="o28592" id="r1931" name="near" negation="false" src="d0_s4" to="o28594" />
      <relation from="o28592" id="r1932" name="near" negation="false" src="d0_s4" to="o28595" />
    </statement>
    <statement id="d0_s5">
      <text>Basal leaves: blade oblanceolate, 2–8 (–11) mm wide, margins entire or denticulate, ciliate proximally, trichomes (simple), 0.5–1 mm, surfaces sparsely to densely pubescent, trichomes short-stalked, 2–4 (or 5) -rayed, 0.2–0.5 mm, these mixed with fewer simple ones.</text>
      <biological_entity constraint="basal" id="o28597" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o28598" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="mm" name="width" src="d0_s5" to="11" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28599" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o28600" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28601" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o28602" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="short-stalked" value_original="short-stalked" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
        <character constraint="with ones" constraintid="o28603" is_modifier="false" name="arrangement" src="d0_s5" value="mixed" value_original="mixed" />
      </biological_entity>
      <biological_entity id="o28603" name="one" name_original="ones" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="fewer" value_original="fewer" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves: 7–33, often concealing stem proximally;</text>
      <biological_entity constraint="cauline" id="o28604" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" name="quantity" src="d0_s6" to="33" />
      </biological_entity>
      <biological_entity id="o28605" name="stem" name_original="stem" src="d0_s6" type="structure" />
      <relation from="o28604" id="r1933" modifier="proximally" name="often concealing" negation="false" src="d0_s6" to="o28605" />
    </statement>
    <statement id="d0_s7">
      <text>blade auricles 1–3 mm, surfaces of distalmost leaves sparsely pubescent or glabrous.</text>
      <biological_entity constraint="cauline" id="o28606" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o28607" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28608" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distalmost" id="o28609" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o28608" id="r1934" name="part_of" negation="false" src="d0_s7" to="o28609" />
    </statement>
    <statement id="d0_s8">
      <text>Racemes 15–63-flowered, usually unbranched.</text>
    </statement>
    <statement id="d0_s9">
      <text>Fruiting pedicels reflexed, abruptly recurved near base, otherwise straight, 4–12 mm, sparsely pubescent or glabrous, trichomes spreading, simple and 2-rayed.</text>
      <biological_entity id="o28610" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="15-63-flowered" value_original="15-63-flowered" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o28611" name="pedicel" name_original="pedicels" src="d0_s9" type="structure" />
      <biological_entity id="o28612" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" modifier="abruptly" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
        <character is_modifier="false" modifier="otherwise" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28613" name="trichome" name_original="trichomes" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="simple" value_original="simple" />
      </biological_entity>
      <relation from="o28610" id="r1935" name="fruiting" negation="false" src="d0_s9" to="o28611" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers divaricate to pendent at anthesis;</text>
      <biological_entity id="o28614" name="flower" name_original="flowers" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divaricate" value_original="divaricate" />
        <character constraint="at anthesis" is_modifier="false" name="orientation" src="d0_s10" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sepals pubescent;</text>
      <biological_entity id="o28615" name="sepal" name_original="sepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>petals white to lavender, 5–6 × 1–1.5 mm, glabrous;</text>
      <biological_entity id="o28616" name="petal" name_original="petals" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="lavender" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s12" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s12" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pollen spheroid.</text>
      <biological_entity id="o28617" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="spheroid" value_original="spheroid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Fruits reflexed to closely pendent, not appressed to rachis, not secund, straight or curved, edges parallel, (4.5–) 5.5–8.5 cm × 1.5–2 mm;</text>
      <biological_entity id="o28618" name="fruit" name_original="fruits" src="d0_s14" type="structure">
        <character char_type="range_value" from="reflexed" name="orientation" src="d0_s14" to="closely pendent" />
        <character constraint="to rachis" constraintid="o28619" is_modifier="false" modifier="not" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s14" value="secund" value_original="secund" />
        <character is_modifier="false" name="course" src="d0_s14" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s14" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o28619" name="rachis" name_original="rachis" src="d0_s14" type="structure" />
      <biological_entity id="o28620" name="edge" name_original="edges" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s14" value="parallel" value_original="parallel" />
        <character char_type="range_value" from="4.5" from_unit="cm" name="atypical_length" src="d0_s14" to="5.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5.5" from_unit="cm" name="length" src="d0_s14" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>valves glabrous;</text>
      <biological_entity id="o28621" name="valve" name_original="valves" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>ovules 70–110 per ovary;</text>
      <biological_entity id="o28622" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o28623" from="70" name="quantity" src="d0_s16" to="110" />
      </biological_entity>
      <biological_entity id="o28623" name="ovary" name_original="ovary" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>style 0.05–0.4 mm.</text>
      <biological_entity id="o28624" name="style" name_original="style" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.05" from_unit="mm" name="some_measurement" src="d0_s17" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Seeds uniseriate, 1.5–1.8 × 1.2–1.5 mm;</text>
      <biological_entity id="o28625" name="seed" name_original="seeds" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s18" value="uniseriate" value_original="uniseriate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s18" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s18" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>wing continuous, 0.15–0.3 mm wide.</text>
      <biological_entity id="o28626" name="wing" name_original="wing" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="continuous" value_original="continuous" />
        <character char_type="range_value" from="0.15" from_unit="mm" name="width" src="d0_s19" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rock outcrops and gravelly soil in meadows and open conifer forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock outcrops" />
        <character name="habitat" value="gravelly soil" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="open conifer forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1100-3200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="1100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>73.</number>
  <discussion>Most authors (e.g., R. C. Rollins 1993; R. D. Dorn 2001; S. L. Welsh et al. 2003; N. H. Holmgren 2005b) have treated Boechera pinetorum as a variety of Arabis (Boechera) holboellii. Under this guise, the name has been applied to a vast array of plants collected throughout western North America. This includes a diversity of sexual diploids plus nearly every hybrid containing a genome from B. retrofracta. Based on re-examination of the type collection, we have adopted a much narrower concept of the species. Morphological evidence suggests that B. pinetorum is an apomictic triploid hybrid containing three different genomes, derived from B. rectissima, B. retrofracta, and B. sparsiflora. Plants closely resembling the type of B. pinetorum are confined to the northern Sierra Nevada and adjacent southern Cascades. The majority of collections previously associated with the epithet pinetorum represent B. pauciflora (see M. D. Windham and I. A. Al-Shehbaz 2007 for detailed comparison).</discussion>
  
</bio:treatment>