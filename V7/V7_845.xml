<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Ihsan A. Al-Shehbaz</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">235</other_info_on_meta>
    <other_info_on_meta type="mention_page">242</other_info_on_meta>
    <other_info_on_meta type="treatment_page">534</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">erysimeae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">ERYSIMUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 660. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 296. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe erysimeae;genus ERYSIMUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek eryso, to ward off or to cure, alluding to the supposed medicinal properties of some species</other_info_on_name>
    <other_info_on_name type="fna_id">112142</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Cheiranthus</taxon_name>
    <taxon_hierarchy>genus Cheiranthus;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Link" date="unknown" rank="genus">Cheirinia</taxon_name>
    <taxon_hierarchy>genus Cheirinia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="(de Candolle) Besser" date="unknown" rank="genus">Cuspidaria</taxon_name>
    <taxon_hierarchy>genus Cuspidaria;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Andrzejowski ex Besser" date="unknown" rank="genus">Syrenia</taxon_name>
    <taxon_hierarchy>genus Syrenia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not scapose;</text>
    </statement>
    <statement id="d0_s1">
      <text>pubescent, trichomes sessile, medifixed, appressed, 2-rayed (malpighiaceous) or 3–5 (–8) -rayed (stellate), rays (when 2) parallel to long axis of stems, leaves, sepals, and fruits.</text>
      <biological_entity id="o37857" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="scapose" value_original="scapose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o37858" name="trichome" name_original="trichomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="fixation" src="d0_s1" value="medifixed" value_original="medifixed" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o37859" name="ray" name_original="rays" src="d0_s1" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s1" value="parallel" value_original="parallel" />
      </biological_entity>
      <biological_entity id="o37860" name="axis" name_original="axis" src="d0_s1" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s1" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o37861" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o37862" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o37863" name="sepal" name_original="sepals" src="d0_s1" type="structure" />
      <biological_entity id="o37864" name="fruit" name_original="fruits" src="d0_s1" type="structure" />
      <relation from="o37860" id="r2550" name="part_of" negation="false" src="d0_s1" to="o37861" />
      <relation from="o37860" id="r2551" name="part_of" negation="false" src="d0_s1" to="o37862" />
      <relation from="o37860" id="r2552" name="part_of" negation="false" src="d0_s1" to="o37863" />
      <relation from="o37860" id="r2553" name="part_of" negation="false" src="d0_s1" to="o37864" />
    </statement>
    <statement id="d0_s2">
      <text>Stems erect or ascending [decumbent], unbranched or branched basally and/or distally.</text>
      <biological_entity id="o37865" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="basally; distally" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal and cauline;</text>
    </statement>
    <statement id="d0_s4">
      <text>petiolate or sessile;</text>
      <biological_entity id="o37866" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal rosulate or not, petiolate, blade margins usually entire, dentate, sinuate-dentate, or denticulate, rarely pinnatifid or pinnatisect;</text>
      <biological_entity constraint="basal" id="o37867" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s5" value="rosulate" value_original="rosulate" />
        <character name="arrangement" src="d0_s5" value="not" value_original="not" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity constraint="blade" id="o37868" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sinuate-dentate" value_original="sinuate-dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatisect" value_original="pinnatisect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sinuate-dentate" value_original="sinuate-dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatisect" value_original="pinnatisect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="sinuate-dentate" value_original="sinuate-dentate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatisect" value_original="pinnatisect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline petiolate or sessile, blade (base cuneate or attenuate [auriculate]), margins entire, dentate, denticulate, dentate-sinuate, or repand.</text>
      <biological_entity constraint="cauline" id="o37869" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o37870" name="blade" name_original="blade" src="d0_s6" type="structure" />
      <biological_entity id="o37871" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="dentate-sinuate" value_original="dentate-sinuate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="repand" value_original="repand" />
        <character is_modifier="false" name="shape" src="d0_s6" value="dentate-sinuate" value_original="dentate-sinuate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Racemes (densely flowered, E. pallasii bracteate basally).</text>
    </statement>
    <statement id="d0_s8">
      <text>Fruiting pedicels erect, ascending, divaricate, reflexed, horizontal, or spreading, slender or stout (nearly as wide as fruit).</text>
      <biological_entity id="o37872" name="raceme" name_original="racemes" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character is_modifier="false" name="size" src="d0_s8" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o37873" name="pedicel" name_original="pedicels" src="d0_s8" type="structure" />
      <relation from="o37872" id="r2554" name="fruiting" negation="false" src="d0_s8" to="o37873" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: sepals oblong or linear, lateral pair saccate or not basally (pubescent);</text>
      <biological_entity id="o37874" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o37875" name="sepal" name_original="sepals" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s9" value="linear" value_original="linear" />
        <character is_modifier="false" name="position" src="d0_s9" value="lateral" value_original="lateral" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="saccate" value_original="saccate" />
        <character name="architecture_or_shape" src="d0_s9" value="not basally" value_original="not basally" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>petals suborbicular, obovate, or spatulate, claw differentiated from blade (subequaling or longer than sepals, apex rounded [emarginate]);</text>
      <biological_entity id="o37876" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o37877" name="petal" name_original="petals" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o37878" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character constraint="from blade" constraintid="o37879" is_modifier="false" name="variability" src="d0_s10" value="differentiated" value_original="differentiated" />
      </biological_entity>
      <biological_entity id="o37879" name="blade" name_original="blade" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens (erect), tetradynamous;</text>
      <biological_entity id="o37880" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="tetradynamous" value_original="tetradynamous" />
      </biological_entity>
      <biological_entity id="o37881" name="stamen" name_original="stamens" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>filaments not dilated basally;</text>
      <biological_entity id="o37882" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o37883" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not; basally" name="shape" src="d0_s12" value="dilated" value_original="dilated" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers oblong or linear;</text>
      <biological_entity id="o37884" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o37885" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s13" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>nectar glands (1, 2, or 4), distinct or confluent, subtending bases of stamens, median glands present or absent.</text>
      <biological_entity id="o37886" name="flower" name_original="flowers" src="d0_s14" type="structure" constraint="stamen" constraint_original="stamen; stamen">
        <character is_modifier="false" name="fusion" notes="" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="arrangement" src="d0_s14" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity constraint="nectar" id="o37887" name="gland" name_original="glands" src="d0_s14" type="structure" />
      <biological_entity id="o37888" name="base" name_original="bases" src="d0_s14" type="structure" />
      <biological_entity id="o37889" name="stamen" name_original="stamens" src="d0_s14" type="structure" />
      <biological_entity constraint="median" id="o37890" name="gland" name_original="glands" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o37886" id="r2555" name="subtending" negation="false" src="d0_s14" to="o37888" />
      <relation from="o37886" id="r2556" name="part_of" negation="false" src="d0_s14" to="o37889" />
    </statement>
    <statement id="d0_s15">
      <text>Fruits usually sessile, rarely shortly stipitate (gynophore to 4 mm), usually linear or narrowly so [oblong], smooth or torulose, (keeled or not);</text>
      <biological_entity id="o37891" name="fruit" name_original="fruits" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="rarely shortly" name="architecture" src="d0_s15" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" modifier="usually" name="arrangement_or_course_or_shape" src="d0_s15" value="linear" value_original="linear" />
        <character name="arrangement_or_course_or_shape" src="d0_s15" value="narrowly" value_original="narrowly" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s15" value="torulose" value_original="torulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>valves each with obscure to prominent midvein, pubescent outside, usually glabrous inside;</text>
      <biological_entity id="o37892" name="valve" name_original="valves" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s16" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o37893" name="midvein" name_original="midvein" src="d0_s16" type="structure">
        <character char_type="range_value" from="obscure" is_modifier="true" name="prominence" src="d0_s16" to="prominent" />
      </biological_entity>
      <relation from="o37892" id="r2557" name="with" negation="false" src="d0_s16" to="o37893" />
    </statement>
    <statement id="d0_s17">
      <text>replum rounded;</text>
      <biological_entity id="o37894" name="replum" name_original="replum" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>septum complete, (not veined);</text>
      <biological_entity id="o37895" name="septum" name_original="septum" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="complete" value_original="complete" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>ovules [15–] 20–120 per ovary;</text>
      <biological_entity id="o37897" name="ovary" name_original="ovary" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>(style relatively short, rarely 1/2 as long as or subequaling fruit, often pubescent);</text>
      <biological_entity id="o37896" name="ovule" name_original="ovules" src="d0_s19" type="structure">
        <character char_type="range_value" from="15" name="atypical_quantity" src="d0_s19" to="20" to_inclusive="false" />
        <character char_type="range_value" constraint="per ovary" constraintid="o37897" from="20" name="quantity" src="d0_s19" to="120" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>stigma capitate.</text>
      <biological_entity id="o37898" name="stigma" name_original="stigma" src="d0_s21" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s21" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds plump or flattened, winged, margined, or not winged, oblong, ovoid, obovate, or suborbicular;</text>
      <biological_entity id="o37899" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="size" src="d0_s22" value="plump" value_original="plump" />
        <character is_modifier="false" name="shape" src="d0_s22" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="winged" value_original="winged" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="margined" value_original="margined" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s22" value="winged" value_original="winged" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="margined" value_original="margined" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s22" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s22" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s22" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s22" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s22" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" name="shape" src="d0_s22" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s22" value="suborbicular" value_original="suborbicular" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>seed-coat (minutely reticulate), mucilaginous when wetted;</text>
      <biological_entity id="o37900" name="seed-coat" name_original="seed-coat" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="when wetted" name="coating" src="d0_s23" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>cotyledons incumbent, rarely accumbent.</text>
    </statement>
    <statement id="d0_s25">
      <text>x = (6) 7, 8 (9–17).</text>
      <biological_entity id="o37901" name="cotyledon" name_original="cotyledons" src="d0_s24" type="structure">
        <character is_modifier="false" name="arrangement_or_orientation" src="d0_s24" value="incumbent" value_original="incumbent" />
        <character is_modifier="false" modifier="rarely" name="arrangement" src="d0_s24" value="accumbent" value_original="accumbent" />
      </biological_entity>
      <biological_entity constraint="x" id="o37902" name="chromosome" name_original="" src="d0_s25" type="structure">
        <character name="atypical_quantity" src="d0_s25" unit=",[9-1]" value="6" value_original="6" />
        <character name="quantity" src="d0_s25" unit=",[9-1]" value="7" value_original="7" />
        <character name="quantity" src="d0_s25" unit=",[9-1]" value="8" value_original="8" />
        <character char_type="range_value" from="9" name="atypical_quantity" src="d0_s25" to="17" unit=",[9-1]" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, n Mexico, Central America, Europe, Asia, n Africa, Atlantic Islands (Macaronesia); introduced in South America, Australia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="n Africa" establishment_means="native" />
        <character name="distribution" value="Atlantic Islands (Macaronesia)" establishment_means="native" />
        <character name="distribution" value="in South America" establishment_means="introduced" />
        <character name="distribution" value="Australia" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>55.</number>
  <other_name type="common_name">Wallflower</other_name>
  <discussion>Species ca. 150 (19 in the flora).</discussion>
  <discussion>Erysimum is found in the northern hemisphere, primarily Asia and Europe, with eight species in northern Africa and Macaronesia, and one each endemic to Baja California (E. moranii Rollins) and Costa Rica and Guatemala (E. ghiesbreghtii J. D. Smith). Of the 21 species found in North America, four are naturalized. Most of the native species have x = 9 and are believed to represent a monophyletic group (R. A. Price 1987).</discussion>
  <discussion>Erysimum is a taxonomically difficult genus much in need of comprehensive phylogenetic and systematic studies covering its entire range. The principal sources of difficulty are the inflation in the number of species described, the heavy reliance on vegetative morphological characters in the delimitation of species, and the inadequacy of most herbarium specimens. In order to reliably identify a given sample, one often needs a complete specimen that has basal leaves, flowers, mature fruits, and seeds. Unfortunately, plants of most species shed their basal leaves or have no flowers when at full fruit maturity. Another complicating factor in North America is that almost all of the native species readily hybridize in areas of overlap to produce wide arrays of intermediates that backcross with the parents and blur species boundaries.</discussion>
  <references>
    <reference>Price, R. A. 1987. Systematics of the Erysimum capitatum Alliance (Brassicaceae) in North America. Ph.D. dissertation. University of California, Berkeley.</reference>
    <reference>Rossbach, G. B. 1940. Erysimum in North America. Ph.D. dissertation. Stanford University.</reference>
    <reference>Rossbach, G. B. 1958. The genus Erysimum (Cruciferae) in North America north of Mexico—A key to the species and varieties. Madroño 14: 261–267.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 3-10(-15) × 1.5-3 mm; median filaments 2-7(-10) mm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals (10-)13-30(-35) × 3-10(-15) mm; median filaments (6-)7-15 mm</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruit valves densely pubescent inside; sepals 1.8-3.2 mm; petal claws 1.5-3.5 mm.</description>
      <determination>5 Erysimum cheiranthoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Fruit valves usually glabrous inside, rarely sparsely pubescent; sepals 4-7(-8) mm; petal claws 3-8 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Annuals; fruiting pedicels as wide as fruit.</description>
      <determination>17 Erysimum repandum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Biennials or perennials (short-lived); fruiting pedicels narrower than fruit</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stigmas entire; fruits appressed to rachises; leaf blade surfaces and fruit valves with 3- and 4-rayed trichomes.</description>
      <determination>10 Erysimum hieraciifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stigmas strongly 2-lobed; fruits not appressed or subappressed to rachises; leaf blade surfaces and fruit valves with 2- or 3-rayed trichomes</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Petals narrowly obovate to spatulate, (8-)10-15 × (2-)2.5-4 mm; fruits 1.8-2.5 mm wide; seeds 1.5-2 mm.</description>
      <determination>7 Erysimum coarctatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Petals oblanceolate, 6-9(-11) × 1-2 mm; fruits 1.2-1.7 mm wide; seeds 1.2-1.7 × 0.8-1 mm.</description>
      <determination>11 Erysimum inconspicuum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Subshrubs (stems woody at base)</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Biennials or perennials (stems not woody at base)</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Fruit valve trichomes 2-rayed; stigmas strongly 2-lobed, lobes much longer than wide; petals orange, yellow, brown, red, purple, violet, or white.</description>
      <determination>6 Erysimum cheiri</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Fruit valve trichomes 2-4-rayed; stigmas not strongly 2-lobed, lobes as long as wide; petals yellow or cream</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Fruits angustiseptate; leaf trichomes 2- or 3-rayed.</description>
      <determination>12 Erysimum insulare</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Fruits latispetate or 4-angled; leaf trichomes 2-5-rayed</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Distal cauline leaves petiolate; fruits latiseptate, not 4-angled; fruiting pedicels stout, 5-17(-22) mm.</description>
      <determination>9 Erysimum franciscanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Distal cauline leaves sessile; fruits slightly latiseptate or 4-angled; fruiting pedicels slender, (3-)5-10 mm.</description>
      <determination>18 Erysimum suffrutescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Petals purple or lilac; leaf trichomes 2-rayed; sepals 5-9 mm; anthers 1-1.5 mm.</description>
      <determination>15 Erysimum pallasii</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Petals usually yellow or orange, rarely lavender or purplish; leaf trichomes (at least some) 3-7-rayed; sepals 7-14 mm; anthers 2-4 mm</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Basal leaf blades filiform to narrowly linear, (somewhat revolute, appearing terete).</description>
      <determination>19 Erysimum teretifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Basal leaf blades not filiform or narrowly linear</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Fruits 4-angled, longitudinally 4-striped; valves densely pubescent between midvein and replum with 2-rayed trichomes; ovules 72-120 per ovary.</description>
      <determination>3 Erysimum asperum</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Fruits latiseptate, rarely 4-angled, not longitudinally striped; valves pubescent with 2-6-rayed trichomes; ovules 24-86 per ovary</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blades: surfaces with 2 (or 3)-rayed trichomes</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blades: surfaces with 2-5(-7)-rayed trichomes</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Fruits usually 4-angled, rarely latiseptate; ovules (40-)54-82 per ovary; seeds not winged, 1.5-2(-2.4) mm.</description>
      <determination>4 Erysimum capitatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Fruits strongly latiseptate; ovules 24-46 per ovary; seeds usually broadly winged all around or apically, rarely not winged, 2-3.5 mm</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Perennials; seeds not winged or winged distally; fruits 1.5-2.7 mm wide.</description>
      <determination>2 Erysimum arenicola</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Biennials; seeds winged all around; fruits (2-)2.4-3.7 mm wide.</description>
      <determination>14 Erysimum occidentale</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Fruits torulose; ovules 26-44 per ovary; petals 3.5-6 mm wide.</description>
      <determination>16 Erysimum perenne</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Fruits not torulose; ovules (32-)42-86 per ovary; petals (5-)6-16 mm wide</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Fruiting pedicels 2-4(-6) mm; seeds broadly obovate to suborbicular, 1.5-3 mm wide.</description>
      <determination>8 Erysimum concinnum</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Fruiting pedicels 4-17(-25) mm; seeds oblong, 1-2 mm wide</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Fruits divaricate, ascending, or erect; petals orange or orange-yellow to yellow; seeds winged distally; Midwestern, Mountain, Pacific states.</description>
      <determination>4 Erysimum capitatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Fruits spreading; petals yellow; seeds winged all around; California (Humboldt, Mendocino, Monterrey, Santa Cruz counties)</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Basal leaf blades linear-oblanceolate, 2-9 mm wide; stems 0.4-9(-13) dm; fruit valves each with prominent midvein; ovules 50-86 per ovary.</description>
      <determination>1 Erysimum ammophilum</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Basal leaf blades spatulate, 5-15 mm wide; stems 0.2-2.5(-3.5) dm; fruit valves each with obscure midvein; ovules 32-74 per ovary.</description>
      <determination>13 Erysimum menziesii</determination>
    </key_statement>
  </key>
</bio:treatment>