<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="treatment_page">479</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Burnett" date="unknown" rank="family">brassicaceae</taxon_name>
    <taxon_name authority="Dumortier" date="1827" rank="tribe">cardamineae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cardamine</taxon_name>
    <taxon_name authority="(S. Watson) Howell" date="1897" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N.W. Amer.,</publication_title>
      <place_in_publication>50. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family brassicaceae;tribe cardamineae;genus cardamine;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250094590</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">pratensis</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="variety">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>in A. Gray et al., Syn. Fl. N. Amer.</publication_title>
      <place_in_publication>1(1,1): 158. 1895</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cardamine;species pratensis;variety occidentalis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cardamine</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">neglecta</taxon_name>
    <taxon_hierarchy>genus Cardamine;species neglecta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Perennials;</text>
    </statement>
    <statement id="d0_s1">
      <text>glabrous or hirsute.</text>
      <biological_entity id="o23113" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="hirsute" value_original="hirsute" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rhizomes (tuberiform, fragile), ovoid or globose at base of stem, 3–10 mm diam., (fleshy).</text>
      <biological_entity id="o23114" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovoid" value_original="ovoid" />
        <character constraint="at base" constraintid="o23115" is_modifier="false" name="shape" src="d0_s2" value="globose" value_original="globose" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" notes="" src="d0_s2" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23115" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o23116" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o23115" id="r1592" name="part_of" negation="false" src="d0_s2" to="o23116" />
    </statement>
    <statement id="d0_s3">
      <text>Stems (simple from base), erect to ascending, (not flexuous), unbranched or branched distally, 1–5 dm, glabrous or pubescent proximally.</text>
      <biological_entity id="o23117" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Basal leaves not rosulate, pinnately compound, (3 or) 5 (or 7) -foliolate, 2–10 cm, leaflets petiolulate or subsessile;</text>
      <biological_entity constraint="basal" id="o23118" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s4" value="rosulate" value_original="rosulate" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s4" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="5-foliolate" value_original="5-foliolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23119" name="leaflet" name_original="leaflets" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="subsessile" value_original="subsessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>petiole 0.5–6.5 cm;</text>
      <biological_entity id="o23120" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="6.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lateral leaflets petiolulate or subsessile, blade similar to terminal, ovate, smaller, margins entire;</text>
      <biological_entity constraint="lateral" id="o23121" name="leaflet" name_original="leaflets" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="petiolulate" value_original="petiolulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <biological_entity id="o23122" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o23123" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>terminal leaflet (petiolule 0.03–0.18 cm), blade orbicular to broadly ovate or subcordate, 0.5–2 cm × 7–25 mm, base cordate to rounded, margins entire or repand, (surfaces glabrous).</text>
      <biological_entity constraint="terminal" id="o23124" name="leaflet" name_original="leaflet" src="d0_s7" type="structure" />
      <biological_entity id="o23125" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character char_type="range_value" from="orbicular" name="shape" src="d0_s7" to="broadly ovate or subcordate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s7" to="2" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s7" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23126" name="base" name_original="base" src="d0_s7" type="structure">
        <character char_type="range_value" from="cordate" name="shape" src="d0_s7" to="rounded" />
      </biological_entity>
      <biological_entity id="o23127" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s7" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Cauline leaves 3–7, (3 or) 5 or 7-foliolate (middle ones 5 or 7-foliolate, smaller distally, becoming 3-foliolate), petiolate;</text>
      <biological_entity constraint="cauline" id="o23128" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s8" to="7" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="7-foliolate" value_original="7-foliolate" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="petiolate" value_original="petiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>petiole 0.5–3 cm, base not auriculate;</text>
      <biological_entity id="o23129" name="petiole" name_original="petiole" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s9" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23130" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s9" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lateral leaflets similar to terminal, smaller;</text>
      <biological_entity constraint="lateral" id="o23131" name="leaflet" name_original="leaflets" src="d0_s10" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s10" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="size" src="d0_s10" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>terminal leaflet blade obovate to oblanceolate, 0.5–2.6 cm × 3–13 mm, margins shallowly toothed, entire, or repand.</text>
      <biological_entity constraint="leaflet" id="o23132" name="blade" name_original="blade" src="d0_s11" type="structure" constraint_original="terminal leaflet">
        <character char_type="range_value" from="obovate" name="shape" src="d0_s11" to="oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s11" to="2.6" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23133" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s11" value="toothed" value_original="toothed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="repand" value_original="repand" />
        <character is_modifier="false" name="shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s11" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Racemes ebracteate.</text>
    </statement>
    <statement id="d0_s13">
      <text>Fruiting pedicels divaricate-ascending, 7–18 mm.</text>
      <biological_entity id="o23134" name="raceme" name_original="racemes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="divaricate-ascending" value_original="divaricate-ascending" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23135" name="pedicel" name_original="pedicels" src="d0_s13" type="structure" />
      <relation from="o23134" id="r1593" name="fruiting" negation="false" src="d0_s13" to="o23135" />
    </statement>
    <statement id="d0_s14">
      <text>Flowers: sepals oblong, 1.7–2 × 1–1.2 mm, lateral pair not saccate basally;</text>
      <biological_entity id="o23136" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o23137" name="sepal" name_original="sepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s14" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s14" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s14" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="not; basally" name="architecture_or_shape" src="d0_s14" value="saccate" value_original="saccate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>petals white, oblanceolate, 4–6 × 1.5–2 mm, (not clawed);</text>
      <biological_entity id="o23138" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o23139" name="petal" name_original="petals" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s15" to="6" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments: median pairs 2–2.5 mm, lateral pair 1–1.5 mm;</text>
      <biological_entity id="o23140" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="median" value_original="median" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="position" src="d0_s16" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers ovate, 0.3–0.5 mm.</text>
      <biological_entity id="o23141" name="filament" name_original="filaments" src="d0_s17" type="structure" />
      <biological_entity id="o23142" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits linear, (torulose), 1.5–3.3 cm × 1.7–2.2 mm;</text>
    </statement>
    <statement id="d0_s19">
      <text>(valves glabrous or sparsely pubescent);</text>
      <biological_entity id="o23143" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s18" value="linear" value_original="linear" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s18" to="3.3" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s18" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>ovules 18–40 per ovary;</text>
      <biological_entity id="o23144" name="ovule" name_original="ovules" src="d0_s20" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o23145" from="18" name="quantity" src="d0_s20" to="40" />
      </biological_entity>
      <biological_entity id="o23145" name="ovary" name_original="ovary" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>style 0.5–1.5 mm.</text>
      <biological_entity id="o23146" name="style" name_original="style" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s21" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Seeds brown, ovoid, 1–1.6 × 1–1.2 mm. 2n = 64.</text>
      <biological_entity id="o23147" name="seed" name_original="seeds" src="d0_s22" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s22" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s22" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s22" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s22" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23148" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="64" value_original="64" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Apr–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Muddy grounds, lake margins, shallow streams, meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="muddy grounds" />
        <character name="habitat" value="lake margins" />
        <character name="habitat" value="shallow streams" />
        <character name="habitat" value="meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150-1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  
</bio:treatment>