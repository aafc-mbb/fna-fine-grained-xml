<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/10 21:46:11</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">7</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="treatment_page">133</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">salicaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">salix</taxon_name>
    <taxon_name authority="(Dumortier) Dumortier" date="1862" rank="subgenus">vetrix</taxon_name>
    <taxon_name authority="Seringe" date="1824" rank="section">cinerella</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">aurita</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1019. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family salicaceae;genus salix;subgenus vetrix;section cinerella;species aurita</taxon_hierarchy>
    <other_info_on_name type="fna_id">200005758</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs, 1–3 m.</text>
      <biological_entity id="o20902" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: branches brownish, not glaucous, pubescent to glabrescent, (peeled wood often with very dense striae, to 21 mm);</text>
      <biological_entity id="o20903" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o20904" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="brownish" value_original="brownish" />
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s1" to="glabrescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>branchlets redbrown or yellowbrown, (weakly glaucous), sparsely tomentose.</text>
      <biological_entity id="o20905" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o20906" name="branchlet" name_original="branchlets" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules foliaceous, apex acute or convex;</text>
      <biological_entity id="o20907" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o20908" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="foliaceous" value_original="foliaceous" />
      </biological_entity>
      <biological_entity id="o20909" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s3" value="convex" value_original="convex" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole convex to flat adaxially, 2–9 mm, velvety adaxially;</text>
      <biological_entity id="o20910" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o20911" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="convex" modifier="adaxially" name="shape" src="d0_s4" to="flat" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s4" value="velvety" value_original="velvety" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>largest medial blade obovate, broadly obovate, or elliptic, 27–85 × 14–35 mm, 1.5–2.8 times as long as wide, base convex or cuneate, margins slightly revolute, entire, remotely or irregularly serrate, or crenate, (glands submarginal), apex acuminate or convex, abaxial surface glaucous, pubescent or pilose, hairs (white, sometimes also ferruginous) spreading or erect, wavy or crinkled, adaxial dull or slightly glossy, pubescent or pilose to glabrescent, veins more hairy, (hairs white, sometimes also ferruginous);</text>
      <biological_entity id="o20912" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="largest medial" id="o20913" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="27" from_unit="mm" name="length" src="d0_s5" to="85" to_unit="mm" />
        <character char_type="range_value" from="14" from_unit="mm" name="width" src="d0_s5" to="35" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s5" value="1.5-2.8" value_original="1.5-2.8" />
      </biological_entity>
      <biological_entity id="o20914" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o20915" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape_or_vernation" src="d0_s5" value="revolute" value_original="revolute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="remotely; irregularly" name="shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o20916" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o20917" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o20918" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s5" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="shape" src="d0_s5" value="crinkled" value_original="crinkled" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20919" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="slightly" name="reflectance" src="d0_s5" value="glossy" value_original="glossy" />
        <character char_type="range_value" from="pilose" name="pubescence" src="d0_s5" to="glabrescent" />
      </biological_entity>
      <biological_entity id="o20920" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal blade margins entire;</text>
      <biological_entity id="o20921" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity constraint="blade" id="o20922" name="margin" name_original="margins" src="d0_s6" type="structure" constraint_original="proximal blade">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>juvenile blade reddish or yellowish green, densely tomentose to glabrescent abaxially, hairs white.</text>
      <biological_entity id="o20923" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o20924" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellowish green" value_original="yellowish green" />
        <character char_type="range_value" from="densely tomentose" modifier="abaxially" name="pubescence" src="d0_s7" to="glabrescent" />
      </biological_entity>
      <biological_entity id="o20925" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Catkins flowering before leaves emerge;</text>
      <biological_entity id="o20927" name="leaf" name_original="leaves" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>staminate subglobose or globose, 15.5–21.5 × 10–15 mm, flowering branchlet 0.5–4 mm;</text>
      <biological_entity id="o20926" name="catkin" name_original="catkins" src="d0_s8" type="structure">
        <character constraint="before leaves" constraintid="o20927" is_modifier="false" name="life_cycle" src="d0_s8" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="shape" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pistillate loosely to moderately densely flowered, 15–37 × 9–20, flowering branchlet 2.5–7 mm;</text>
      <biological_entity id="o20928" name="branchlet" name_original="branchlet" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="subglobose" value_original="subglobose" />
        <character is_modifier="false" name="shape" src="d0_s9" value="globose" value_original="globose" />
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o20929" name="branchlet" name_original="branchlet" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="loosely to moderately densely" name="architecture" src="d0_s10" value="flowered" value_original="flowered" />
        <character char_type="range_value" from="15" name="quantity" src="d0_s10" to="37" />
        <character char_type="range_value" from="9" name="quantity" src="d0_s10" to="20" />
        <character is_modifier="true" name="life_cycle" src="d0_s10" value="flowering" value_original="flowering" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>floral bract brown, tawny, or bicolor, 1–2.2 mm, apex acute or tapering and rounded, abaxially hairy, hairs straight.</text>
      <biological_entity constraint="floral" id="o20930" name="bract" name_original="bract" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="bicolor" value_original="bicolor" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="tawny" value_original="tawny" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="bicolor" value_original="bicolor" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o20931" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o20932" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Staminate flowers: adaxial nectary oblong or square, 0.3–0.7 mm;</text>
      <biological_entity id="o20933" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20934" name="nectary" name_original="nectary" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s12" value="square" value_original="square" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s12" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, glabrous or hairy on proximal 1/2 or basally;</text>
      <biological_entity id="o20935" name="flower" name_original="flowers" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o20936" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character constraint="on proximal 1/2" constraintid="o20937" is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o20937" name="1/2" name_original="1/2" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>anthers purple turning yellow, ellipsoid or shortly cylindrical, 0.5–0.8 mm.</text>
      <biological_entity id="o20938" name="flower" name_original="flowers" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o20939" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="purple turning yellow" value_original="purple turning yellow" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s14" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pistillate flowers: adaxial nectary oblong or square, 0.3–0.7 mm, shorter than stipe;</text>
      <biological_entity id="o20940" name="flower" name_original="flowers" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o20941" name="nectary" name_original="nectary" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="square" value_original="square" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s15" to="0.7" to_unit="mm" />
        <character constraint="than stipe" constraintid="o20942" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o20942" name="stipe" name_original="stipe" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>stipe 1.4–2.6 mm;</text>
      <biological_entity id="o20943" name="flower" name_original="flowers" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o20944" name="stipe" name_original="stipe" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s16" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>ovary pyriform, densely short-silky, hairs wavy or crinkled, beak sometimes slightly bulged below styles (long-beaked);</text>
      <biological_entity id="o20945" name="flower" name_original="flowers" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o20946" name="ovary" name_original="ovary" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="pyriform" value_original="pyriform" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s17" value="short-silky" value_original="short-silky" />
      </biological_entity>
      <biological_entity id="o20947" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="wavy" value_original="wavy" />
        <character is_modifier="false" name="shape" src="d0_s17" value="crinkled" value_original="crinkled" />
      </biological_entity>
      <biological_entity id="o20948" name="beak" name_original="beak" src="d0_s17" type="structure" />
      <biological_entity id="o20949" name="style" name_original="styles" src="d0_s17" type="structure" />
      <relation from="o20948" id="r1449" modifier="below" name="bulged" negation="false" src="d0_s17" to="o20949" />
    </statement>
    <statement id="d0_s18">
      <text>ovules 10–12 per ovary;</text>
      <biological_entity id="o20950" name="flower" name_original="flowers" src="d0_s18" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s18" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o20951" name="ovule" name_original="ovules" src="d0_s18" type="structure">
        <character char_type="range_value" constraint="per ovary" constraintid="o20952" from="10" name="quantity" src="d0_s18" to="12" />
      </biological_entity>
      <biological_entity id="o20952" name="ovary" name_original="ovary" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>styles 0–0.3 mm;</text>
      <biological_entity id="o20953" name="flower" name_original="flowers" src="d0_s19" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s19" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o20954" name="style" name_original="styles" src="d0_s19" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s19" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>stigmas broadly cylindrical, 0.25–0.37–0.5 mm.</text>
      <biological_entity id="o20955" name="flower" name_original="flowers" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o20956" name="stigma" name_original="stigmas" src="d0_s20" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s20" value="cylindrical" value_original="cylindrical" />
        <character char_type="range_value" from="0.25" from_unit="mm" name="some_measurement" src="d0_s20" to="0.37-0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Capsules 4–13 mm. 2n = 76, 38.</text>
      <biological_entity id="o20957" name="capsule" name_original="capsules" src="d0_s21" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s21" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20958" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="76" value_original="76" />
        <character name="quantity" src="d0_s21" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early Apr-early Jun.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early Jun" from="early Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet thickets, swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet thickets" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>10-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="10" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Mass., Pa.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>83.</number>
  <other_name type="common_name">Eared willow</other_name>
  
</bio:treatment>