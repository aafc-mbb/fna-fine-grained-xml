<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">121</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">SPOROBOLUS</taxon_name>
    <taxon_name authority="(L.) Kunth" date="unknown" rank="species">virginicus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus sporobolus;species virginicus</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Seashore dropseed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, stoloniferous.</text>
      <biological_entity id="o3135" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-65 cm, erect to decumbent.</text>
      <biological_entity id="o3136" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="65" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths overlapping, margins ciliate, apices with tufts of hairs, hairs to 2 mm;</text>
      <biological_entity id="o3137" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o3138" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o3139" name="apex" name_original="apices" src="d0_s3" type="structure" />
      <biological_entity id="o3140" name="tuft" name_original="tufts" src="d0_s3" type="structure" />
      <biological_entity id="o3141" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <biological_entity id="o3142" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o3139" id="r515" name="with" negation="false" src="d0_s3" to="o3140" />
      <relation from="o3140" id="r516" name="part_of" negation="false" src="d0_s3" to="o3141" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.1-0.4 mm;</text>
      <biological_entity id="o3143" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades usually conspicuously distichous, 4-16 cm long, 2-5 mm wide, flat to loosely involute, glabrous abaxially, scabridulous adaxially, margins scabridulous.</text>
      <biological_entity id="o3144" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually conspicuously" name="arrangement" src="d0_s5" value="distichous" value_original="distichous" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="16" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="loosely involute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o3145" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 3-10 cm long, 0.4-1.6 cm wide, contracted, spikelike, dense;</text>
      <biological_entity id="o3146" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s6" to="1.6" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" name="density" src="d0_s6" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>primary branches 0.5-2 cm, appressed, spikelet-bearing to the base;</text>
      <biological_entity constraint="primary" id="o3147" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="2" to_unit="cm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o3148" name="base" name_original="base" src="d0_s7" type="structure" />
      <relation from="o3147" id="r517" name="to" negation="false" src="d0_s7" to="o3148" />
    </statement>
    <statement id="d0_s8">
      <text>pedicels 0.2-1.4 mm, appressed.</text>
      <biological_entity id="o3149" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets (1.8) 2-3.2 mm, yellowish-white to purplish-tinged, sometimes grayish.</text>
      <biological_entity id="o3150" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="1.8" value_original="1.8" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="yellowish-white" name="coloration" src="d0_s9" to="purplish-tinged" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s9" value="grayish" value_original="grayish" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Glumes subequal, ovate-oblong, membranous;</text>
      <biological_entity id="o3151" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate-oblong" value_original="ovate-oblong" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 1.5-2.4 mm;</text>
      <biological_entity constraint="lower" id="o3152" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 1.8-3 (3.2) mm;</text>
      <biological_entity constraint="upper" id="o3153" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="3.2" value_original="3.2" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 2.1-3 mm, ovate to lanceolate, membranous, glabrous, acute;</text>
      <biological_entity id="o3154" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s13" to="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas 2.1-3 mm, ovate, membranous;</text>
      <biological_entity id="o3155" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s14" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 3, 1-1.7 mm, yellowish.</text>
      <biological_entity id="o3156" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Fruits not known.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 20, 30.</text>
      <biological_entity id="o3157" name="fruit" name_original="fruits" src="d0_s16" type="structure" />
      <biological_entity constraint="2n" id="o3158" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
        <character name="quantity" src="d0_s17" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sporobolus virginicus grows on sandy beaches, sand dunes, and in saline habitats, primarily along the south-eastern coast, occasionally inland. Its range extends through Mexico and Central America to Peru, Chile, and Brazil. No fruits of this species have been found despite examination of several natural populations and over 200 herbarium specimens.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Md.;Miss.;Tex.;La.;Virgin Islands;Ala.;N.C.;S.C.;Va.;Pacific Islands (Hawaii);Ga.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>