<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">72</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Wolf" date="unknown" rank="genus">ERAGROSTIS</taxon_name>
    <taxon_name authority="(L.) Wight &amp; Arn. ex Nees" date="unknown" rank="species">amabilis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eragrostis;species amabilis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eragrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tenella</taxon_name>
    <taxon_hierarchy>genus eragrostis;species tenella</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Japanese lovegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, without innovations, without glands.</text>
      <biological_entity id="o2917" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2918" name="innovation" name_original="innovations" src="d0_s1" type="structure" />
      <biological_entity id="o2919" name="gland" name_original="glands" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-40 cm, erect, glabrous, occasionally with oblong glandular areas below the nodes.</text>
      <biological_entity id="o2920" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o2921" name="area" name_original="areas" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o2922" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <relation from="o2920" id="r472" modifier="occasionally" name="with" negation="false" src="d0_s2" to="o2921" />
      <relation from="o2921" id="r473" name="below" negation="false" src="d0_s2" to="o2922" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths hairy on the distal margins and at the apices, hairs to 4 mm, stiff;</text>
      <biological_entity id="o2923" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="on the distal margins and at apices, hairs" constraintid="o2924, o2925" is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o2924" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o2925" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.2-0.3 mm;</text>
      <biological_entity id="o2926" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 2-8 cm long, 2-4 mm wide, flat to involute, abaxial surfaces smooth, adaxial surfaces scabridulous, bases occasionally with papillose-based hairs.</text>
      <biological_entity id="o2927" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o2928" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o2929" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o2930" name="base" name_original="bases" src="d0_s5" type="structure" />
      <biological_entity id="o2931" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o2930" id="r474" name="with" negation="false" src="d0_s5" to="o2931" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles 4-15 cm long, 1-7 cm wide, cylindrical to narrowly ovate, open, rachises sometimes glandular below the nodes;</text>
      <biological_entity id="o2932" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="cylindrical" name="shape" src="d0_s6" to="narrowly ovate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o2933" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character constraint="below nodes" constraintid="o2934" is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s6" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o2934" name="node" name_original="nodes" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>primary branches 0.5-4 cm, diverging 20-100° from the rachises;</text>
      <biological_entity constraint="primary" id="o2935" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s7" to="4" to_unit="cm" />
        <character constraint="from rachises" constraintid="o2936" is_modifier="false" modifier="20-100°" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o2936" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>pulvini sparsely pilose;</text>
      <biological_entity id="o2937" name="pulvinus" name_original="pulvini" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 1-4 (7) mm, as long as or longer than the spikelets, mostly pendent, lax, terete.</text>
      <biological_entity id="o2938" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="orientation" notes="" src="d0_s9" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o2940" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o2939" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <relation from="o2938" id="r475" name="as long as" negation="false" src="d0_s9" to="o2940" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets (1) 1.5-2.5 mm long, 0.9-1.4 mm wide, ovate to oblong, reddish-purple to greenish, with 4-8 florets;</text>
      <biological_entity id="o2942" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="8" />
      </biological_entity>
      <relation from="o2941" id="r476" name="with" negation="false" src="d0_s10" to="o2942" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation basipetal, glumes persistent.</text>
      <biological_entity id="o2941" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character name="length" src="d0_s10" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s10" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s10" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="oblong" />
        <character char_type="range_value" from="reddish-purple" name="coloration" src="d0_s10" to="greenish" />
        <character is_modifier="false" name="development" src="d0_s11" value="basipetal" value_original="basipetal" />
      </biological_entity>
      <biological_entity id="o2943" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes ovate, hyaline, keeled, veins commonly green;</text>
      <biological_entity id="o2944" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o2945" name="vein" name_original="veins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="commonly" name="coloration" src="d0_s12" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 0.4-0.7 mm;</text>
      <biological_entity constraint="lower" id="o2946" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s13" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 0.7-1 mm;</text>
      <biological_entity constraint="upper" id="o2947" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 0.7-1.1 mm, ovate to broadly oblong, membranous, lateral-veins usually greenish, apices truncate to obtuse;</text>
      <biological_entity id="o2948" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s15" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s15" to="broadly oblong" />
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o2949" name="lateral-vein" name_original="lateral-veins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s15" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o2950" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s15" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 0.6-1.1 mm, hyaline, keels ciliate, cilia 0.3-0.5 mm, apices obtuse to truncate;</text>
      <biological_entity id="o2951" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s16" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o2952" name="keel" name_original="keels" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s16" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o2953" name="cilium" name_original="cilia" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2954" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s16" to="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, about 0.2 mm, purplish.</text>
      <biological_entity id="o2955" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character name="some_measurement" src="d0_s17" unit="mm" value="0.2" value_original="0.2" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 0.3-0.5 mm, ellipsoid, translucent, light-brown.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 20.</text>
      <biological_entity id="o2956" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s18" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s18" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2957" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eragrostis amabilis is native to the Eastern Hemisphere. It is now naturalized in the southeastern United States, growing in open areas such as cultivated fields, forest margins, and roadsides at 0-200 m.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Ga.;Tex.;Virgin Islands;Pacific Islands (Hawaii);Miss.;S.C.;Ala.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>