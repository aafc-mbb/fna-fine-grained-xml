<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">609</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Trin." date="unknown" rank="genus">SPODIOPOGON</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus spodiopogon</taxon_hierarchy>
  </taxon_identification>
  <number>26.01</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>sometimes rhizomatous.</text>
      <biological_entity id="o16435" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 40-150 cm, erect, simple or branching.</text>
      <biological_entity id="o16436" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves not aromatic;</text>
      <biological_entity id="o16437" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s3" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous;</text>
      <biological_entity id="o16438" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades lanceolate to broadly linear, sometimes pseudopetiolate.</text>
      <biological_entity id="o16439" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="broadly linear" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="pseudopetiolate" value_original="pseudopetiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, open or contracted panicles, with evident rachiseswith numerous subverticellate branches that terminate in 1-3 short rames;</text>
      <biological_entity id="o16441" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="true" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o16442" name="subverticellate" name_original="subverticellate" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="evident" value_original="evident" />
        <character is_modifier="true" name="quantity" src="d0_s6" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o16443" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="evident" value_original="evident" />
        <character is_modifier="true" name="quantity" src="d0_s6" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o16444" name="rame" name_original="rames" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
      </biological_entity>
      <relation from="o16440" id="r2753" name="with" negation="false" src="d0_s6" to="o16442" />
      <relation from="o16440" id="r2754" name="with" negation="false" src="d0_s6" to="o16443" />
      <relation from="o16442" id="r2755" name="terminate in" negation="false" src="d0_s6" to="o16444" />
      <relation from="o16443" id="r2756" name="terminate in" negation="false" src="d0_s6" to="o16444" />
    </statement>
    <statement id="d0_s7">
      <text>rames with slender internodes and 2-5 sessile-pedicellate homogamous spikelet pairs;</text>
      <biological_entity constraint="slender" id="o16445" name="internode" name_original="internodes" src="d0_s7" type="structure" />
      <biological_entity constraint="slender" id="o16446" name="spikelet" name_original="spikelet" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="sessile-pedicellate" value_original="sessile-pedicellate" />
        <character is_modifier="true" name="reproduction" src="d0_s7" value="homogamous" value_original="homogamous" />
      </biological_entity>
      <relation from="o16440" id="r2757" name="rames with" negation="false" src="d0_s7" to="o16445" />
      <relation from="o16440" id="r2758" name="rames with" negation="false" src="d0_s7" to="o16446" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation in the rames, below the sessile spikelets.</text>
      <biological_entity id="o16440" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o16447" name="rame" name_original="rames" src="d0_s8" type="structure" />
      <biological_entity id="o16448" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o16440" id="r2759" name="in" negation="false" src="d0_s8" to="o16447" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets usually lanceolate.</text>
      <biological_entity id="o16449" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Glumes equal, chartaceous, often pilose, scarcely keeled, with several raised veins, acute;</text>
      <biological_entity id="o16450" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s10" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="scarcely" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o16451" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="several" value_original="several" />
        <character is_modifier="true" name="prominence" src="d0_s10" value="raised" value_original="raised" />
      </biological_entity>
      <relation from="o16450" id="r2760" name="with" negation="false" src="d0_s10" to="o16451" />
    </statement>
    <statement id="d0_s11">
      <text>calluses glabrous or densely hairy;</text>
      <biological_entity id="o16452" name="callus" name_original="calluses" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower florets usually staminate, unawned;</text>
      <biological_entity constraint="lower" id="o16453" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper florets bisexual;</text>
      <biological_entity constraint="upper" id="o16454" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper lemmas bilobed, with a geniculate awn;</text>
      <biological_entity constraint="upper" id="o16455" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="bilobed" value_original="bilobed" />
      </biological_entity>
      <biological_entity id="o16456" name="awn" name_original="awn" src="d0_s14" type="structure">
        <character is_modifier="true" name="shape" src="d0_s14" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <relation from="o16455" id="r2761" name="with" negation="false" src="d0_s14" to="o16456" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 3.</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 10.</text>
      <biological_entity id="o16457" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="x" id="o16458" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Pedicels slender, not fused to the rames axes.</text>
      <biological_entity id="o16459" name="pedicel" name_original="pedicels" src="d0_s17" type="structure">
        <character is_modifier="false" name="size" src="d0_s17" value="slender" value_original="slender" />
        <character constraint="to axes" constraintid="o16460" is_modifier="false" modifier="not" name="fusion" src="d0_s17" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o16460" name="axis" name_original="axes" src="d0_s17" type="structure" />
    </statement>
  </description>
  <discussion>Spodiopogon is a genus of 10-15 species, most of which grow in subtropical regions of the Eastern Hemisphere, although Spodiopogon sibiricus extends north to Irkutsk, Russia. One species is cultivated in the Flora region.</discussion>
  
</bio:treatment>