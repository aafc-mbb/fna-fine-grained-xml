<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">525</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Rich." date="unknown" rank="genus">PENNISETUM</taxon_name>
    <taxon_name authority="(Vahl) Wipff" date="unknown" rank="species">setigerum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus pennisetum;species setigerum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cenchrus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">setigerus</taxon_name>
    <taxon_hierarchy>genus cenchrus;species setigerus</taxon_hierarchy>
  </taxon_identification>
  <number>13</number>
  <other_name type="common_name">Birdwood grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose from a hard, knotty base, without rhizomes.</text>
      <biological_entity id="o28155" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character constraint="from base" constraintid="o28156" is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o28156" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="texture" src="d0_s1" value="hard" value_original="hard" />
        <character is_modifier="true" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
      </biological_entity>
      <biological_entity id="o28157" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-100 cm, erect, sometimes branching, mostly glabrous but sometimes scabrous beneath the panicle;</text>
      <biological_entity id="o28158" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="beneath panicle" constraintid="o28159" is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o28159" name="panicle" name_original="panicle" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o28160" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves green;</text>
      <biological_entity id="o28161" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths glabrous or pubescent, margins ciliate;</text>
      <biological_entity id="o28162" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o28163" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.6-1.2 mm, membranous, ciliate;</text>
      <biological_entity id="o28164" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s6" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 2-45 cm long, 2.5-7 mm wide, flat, glabrous or pubescent, margins ciliate or glabrous basally.</text>
      <biological_entity id="o28165" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s7" to="45" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o28166" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 2-13.8 cm long, 4-11 mm wide, erect, green or dark purple;</text>
      <biological_entity id="o28167" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s8" to="13.8" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="11" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachises terete, scabrous.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fascicles 11-24 per cm, disarticulating at maturity;</text>
      <biological_entity id="o28168" name="rachis" name_original="rachises" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="fascicles" value_original="fascicles" />
        <character char_type="range_value" from="11" name="quantity" src="d0_s10" to="24" />
        <character constraint="at maturity" is_modifier="false" modifier="per cm" name="architecture" src="d0_s10" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>fascicle axes 0.2-1.1 mm, with 1-12 spikelets;</text>
      <biological_entity constraint="fascicle" id="o28169" name="axis" name_original="axes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28170" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="12" />
      </biological_entity>
      <relation from="o28169" id="r4797" name="with" negation="false" src="d0_s11" to="o28170" />
    </statement>
    <statement id="d0_s12">
      <text>outer bristles 10-62, 0.1-1.8 mm, not exceeding the spikelets;</text>
      <biological_entity constraint="outer" id="o28171" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s12" to="62" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28172" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
      <relation from="o28171" id="r4798" name="exceeding the" negation="true" src="d0_s12" to="o28172" />
    </statement>
    <statement id="d0_s13">
      <text>inner bristles 6-32, 1.2-5 mm, ciliate, fused for 1/3 – 1/2 their length;</text>
    </statement>
    <statement id="d0_s14">
      <text>flattened, grooved;</text>
      <biological_entity constraint="inner" id="o28173" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s13" to="32" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s13" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="length" src="d0_s13" value="fused" value_original="fused" />
        <character is_modifier="false" name="shape" src="d0_s14" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="grooved" value_original="grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>primary bristles 2.9-6.5 mm, ciliate, not noticeably longer than the other bristles.</text>
      <biological_entity constraint="primary" id="o28174" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s15" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s15" value="ciliate" value_original="ciliate" />
        <character constraint="than the other bristles" constraintid="o28175" is_modifier="false" name="length_or_size" src="d0_s15" value="not noticeably longer" value_original="not noticeably longer" />
      </biological_entity>
      <biological_entity id="o28175" name="bristle" name_original="bristles" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Spikelets 3.1-5.3 mm, sessile, glabrous;</text>
      <biological_entity id="o28176" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character char_type="range_value" from="3.1" from_unit="mm" name="some_measurement" src="d0_s16" to="5.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower glumes 1-2.5 mm, 0-1-veined;</text>
      <biological_entity constraint="lower" id="o28177" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="0-1-veined" value_original="0-1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper glumes 1.5-3.4 mm, (0) 1-3-veined, about 1/2 as long as the spikelet;</text>
      <biological_entity constraint="upper" id="o28178" name="glume" name_original="glumes" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s18" to="3.4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="(0)1-3-veined" value_original="(0)1-3-veined" />
        <character constraint="as-long-as spikelet" constraintid="o28179" name="quantity" src="d0_s18" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o28179" name="spikelet" name_original="spikelet" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>lower florets staminate or sterile;</text>
      <biological_entity constraint="lower" id="o28180" name="floret" name_original="florets" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s19" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>upper florets not disarticulating at maturity;</text>
      <biological_entity constraint="upper" id="o28181" name="floret" name_original="florets" src="d0_s20" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="not" name="architecture" src="d0_s20" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>lower lemmas 2.7-5.3 mm, 3-7-veined;</text>
      <biological_entity constraint="lower" id="o28182" name="lemma" name_original="lemmas" src="d0_s21" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s21" to="5.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s21" value="3-7-veined" value_original="3-7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>lower paleas absent or 2.5-4.5 mm;</text>
      <biological_entity constraint="lower" id="o28183" name="palea" name_original="paleas" src="d0_s22" type="structure">
        <character is_modifier="false" name="presence" src="d0_s22" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s22" value="2.5-4.5 mm" value_original="2.5-4.5 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>anthers absent or 0.9-3 mm;</text>
      <biological_entity id="o28184" name="anther" name_original="anthers" src="d0_s23" type="structure">
        <character is_modifier="false" name="presence" src="d0_s23" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s23" value="0.9-3 mm" value_original="0.9-3 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s24">
      <text>upper lemmas 2.8-5 mm, 3-5-veined;</text>
      <biological_entity constraint="upper" id="o28185" name="lemma" name_original="lemmas" src="d0_s24" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s24" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s24" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s25">
      <text>anthers 3, 2-3.2 mm.</text>
      <biological_entity id="o28186" name="anther" name_original="anthers" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="3" value_original="3" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s25" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>Caryopses 1.2-1.8 mm long, 0.4-1 mm wide.</text>
    </statement>
    <statement id="d0_s27">
      <text>2n = 34, 36, 37, 54, 72.</text>
      <biological_entity id="o28187" name="caryopsis" name_original="caryopses" src="d0_s26" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s26" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s26" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28188" name="chromosome" name_original="" src="d0_s27" type="structure">
        <character name="quantity" src="d0_s27" value="34" value_original="34" />
        <character name="quantity" src="d0_s27" value="36" value_original="36" />
        <character name="quantity" src="d0_s27" value="37" value_original="37" />
        <character name="quantity" src="d0_s27" value="54" value_original="54" />
        <character name="quantity" src="d0_s27" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pennisetum setigerum is grown as a forage grass in the southern United States, but is not known to be established in the Flora region. It is native to Africa, Arabia, and India. It is sometimes included in Cenchrus, based solely on the fusion of its bristles.</discussion>
  
</bio:treatment>