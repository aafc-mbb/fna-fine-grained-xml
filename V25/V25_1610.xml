<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">669</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="genus">SCHIZACHYRIUM</taxon_name>
    <taxon_name authority="(Michx.) Nash" date="unknown" rank="species">scoparium</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus schizachyrium;species scoparium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Andropogon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">scoparius</taxon_name>
    <taxon_hierarchy>genus andropogon;species scoparius</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose or rhizomatous, green to purplish, sometimes glaucous.</text>
      <biological_entity id="o1944" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s0" to="purplish" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 7-210 cm tall, usually 1-3 mm thick, not rooting or branching at the lower nodes.</text>
      <biological_entity id="o1945" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="height" src="d0_s1" to="210" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="usually" name="thickness" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character constraint="at lower nodes" constraintid="o1946" is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="lower" id="o1946" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths rounded or keeled, glabrous or pubescent, sometimes glaucous;</text>
      <biological_entity id="o1947" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 0.5-2 mm, collars neither elongate nor narrowed;</text>
      <biological_entity id="o1948" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1949" name="collar" name_original="collars" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 7-105 cm long, 1.5-9 mm wide, without a longitudinal stripe of white, spongy tissue.</text>
      <biological_entity id="o1950" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s4" to="105" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1951" name="stripe" name_original="stripe" src="d0_s4" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s4" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o1952" name="tissue" name_original="tissue" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="true" name="texture" src="d0_s4" value="spongy" value_original="spongy" />
      </biological_entity>
      <relation from="o1950" id="r325" name="without" negation="false" src="d0_s4" to="o1951" />
      <relation from="o1951" id="r326" name="part_of" negation="false" src="d0_s4" to="o1952" />
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0.8-10 cm;</text>
    </statement>
    <statement id="d0_s6">
      <text>rames 2.5-8 cm, partially to completely exserted, usually somewhat open;</text>
      <biological_entity id="o1953" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s6" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="partially to completely" name="position" src="d0_s6" value="exserted" value_original="exserted" />
        <character is_modifier="false" modifier="usually somewhat" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>internodes 3-7 mm, usually arcuate at maturity, ciliate on at least the distal 1/2 (sometimes throughout), hairs 1.5-6 mm.</text>
      <biological_entity id="o1954" name="internode" name_original="internodes" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" modifier="usually" name="course_or_shape" src="d0_s7" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" modifier="at-least" name="position_or_shape" src="d0_s7" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s7" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o1955" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sessile spikelets 3-11 mm;</text>
      <biological_entity id="o1956" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>calluses 0.5-1 (2) mm, hairs 0.3-4 mm;</text>
      <biological_entity id="o1957" name="callus" name_original="calluses" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1958" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes glabrous;</text>
      <biological_entity constraint="lower" id="o1959" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper lemmas membranous throughout, cleft to 1/2 their length;</text>
      <biological_entity constraint="upper" id="o1960" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="throughout" name="texture" src="d0_s11" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="cleft" value_original="cleft" />
        <character char_type="range_value" from="0" name="length" src="d0_s11" to="1/2" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>awns 2.5-17 mm.</text>
      <biological_entity id="o1961" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s12" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pedicels 3-7.5 mm long, 0.1-0.2 mm wide at the base, flaring above midlength to 0.3-0.5 mm, straight or curving outwards.</text>
      <biological_entity id="o1962" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="7.5" to_unit="mm" />
        <character char_type="range_value" constraint="at base" constraintid="o1963" from="0.1" from_unit="mm" name="width" src="d0_s13" to="0.2" to_unit="mm" />
        <character constraint="above midlength 0.3-0.5 mm" is_modifier="false" name="shape" notes="" src="d0_s13" value="flaring" value_original="flaring" />
        <character is_modifier="false" name="course" src="d0_s13" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="outwards" name="course" src="d0_s13" value="curving" value_original="curving" />
      </biological_entity>
      <biological_entity id="o1963" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>Pedicellate spikelets 0.7-10 mm, sometimes shorter than the sessile spikelets, sterile or staminate, unawned or awned, awns to 4 mm, when sterile, the lemma usually absent.</text>
      <biological_entity id="o1964" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
        <character constraint="than the sessile spikelets" constraintid="o1965" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="sometimes shorter" value_original="sometimes shorter" />
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o1965" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o1966" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>2n = 40.</text>
      <biological_entity id="o1967" name="lemma" name_original="lemma" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="when sterile; usually" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1968" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Schizachyrium scoparium is a widespread grassland species extending from Canada to Mexico. It is one of the principal grasses in the tallgrass prairies that used to dominate the central plains of North America. It exhibits considerable variation, much of it clinal. The following varieties are recognized because they are morphologically, ecologically, and geographically distinctive.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Minn.;Conn.;N.J.;N.Y.;Fla.;Pa.;Wash.;Del.;D.C.;Wis.;W.Va.;Kans.;Mo.;N.Dak.;Nebr.;Okla.;S.Dak.;Pacific Islands (Hawaii);Mass.;Maine;N.H.;R.I.;Vt.;Wyo.;N.Mex.;Tex.;La.;Tenn.;N.C.;S.C.;Ill.;Ohio;Va.;Colo.;Calif.;Ala.;Ark.;Ga.;Ind.;Iowa;Ariz.;Idaho;Md.;Utah;Alta.;B.C.;Man.;N.B.;N.S.;Ont.;Que.;Sask.;Mich.;Mont.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants   not  cespitose,   strongly  rhizomatous; pedicellate spikelets sterile</description>
      <determination>Schizachyrium scoparium var. stoloniferum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants usually cespitose, not or shortly rhizomatous; pedicellate spikelets staminate or sterile.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Pedicellate spikelets of the proximal spikelet units on each rame staminate, 5-10 mm long, with a lemma, pedicellate spikelets of the distal units usually smaller (1-4 mm) and sterile; sheaths and blades densely tomentose to glabrate</description>
      <determination>Schizachyrium scoparium var. divergens</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Most pedicellate spikelets sterile, 1-6 mm long, without a lemma; sheaths and blades usually glabrous, occasionally pubescent</description>
      <determination>Schizachyrium scoparium var. scoparium</determination>
    </key_statement>
  </key>
</bio:treatment>