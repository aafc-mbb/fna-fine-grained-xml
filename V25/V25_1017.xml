<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">333</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Andy Sudkamp</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Caro" date="unknown" rank="subfamily">ARISTIDOIDEAE</taxon_name>
    <taxon_name authority="C.E. Hubb." date="unknown" rank="tribe">ARISTIDEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ARISTIDA</taxon_name>
    <taxon_name authority="Nutt." date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="(Nash) Allred" date="unknown" rank="variety">wrightii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily aristidoideae;tribe aristideae;genus aristida;species purpurea;variety wrightii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aristida</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">wrightii</taxon_name>
    <taxon_hierarchy>genus aristida;species wrightii</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Wright's threeawn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 45-100 cm.</text>
      <biological_entity id="o6517" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="45" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Blades 10-25 cm, involute or flat.</text>
      <biological_entity id="o6518" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="25" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s1" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s1" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Panicles (12) 14-30 cm;</text>
      <biological_entity id="o6519" name="panicle" name_original="panicles" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="12" value_original="12" />
        <character char_type="range_value" from="14" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>primary branches usually erect, without axillary pulvini, stiff, straight, lower nodes associated with 2-10 spikelets.</text>
      <biological_entity constraint="primary" id="o6520" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s3" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o6521" name="pulvinus" name_original="pulvini" src="d0_s3" type="structure" />
      <biological_entity constraint="lower" id="o6522" name="node" name_original="nodes" src="d0_s3" type="structure" />
      <biological_entity id="o6523" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
      <relation from="o6520" id="r1028" name="without" negation="false" src="d0_s3" to="o6521" />
      <relation from="o6522" id="r1029" name="associated with" negation="false" src="d0_s3" to="o6523" />
    </statement>
    <statement id="d0_s4">
      <text>Glumes tan to brown, fading to stramineous.</text>
      <biological_entity id="o6524" name="glume" name_original="glumes" src="d0_s4" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s4" to="brown fading" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s4" to="brown fading" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Lower glumes 5-10 mm;</text>
      <biological_entity constraint="lower" id="o6525" name="glume" name_original="glumes" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>upper glumes 9-16 mm;</text>
      <biological_entity constraint="upper" id="o6526" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s6" to="16" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lemmas 8-14 mm long, narrowing to 0.2-0.3 mm wide;</text>
      <biological_entity id="o6527" name="lemma" name_original="lemmas" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="14" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s7" value="narrowing" value_original="narrowing" />
        <character constraint="to 0.2-0.3 mm" is_modifier="false" name="width" src="d0_s7" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>awns (8) 20-35 mm long, 0.2-0.3 mm wide at the base, lateral awns usually subequal to the central awn, rarely 1-3 mm. 2n = 22, 44, 66.</text>
      <biological_entity id="o6528" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="mm" value="8" value_original="8" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s8" to="35" to_unit="mm" />
        <character char_type="range_value" constraint="at base" constraintid="o6529" from="0.2" from_unit="mm" name="width" src="d0_s8" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6529" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity constraint="lateral" id="o6530" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character constraint="to central awn" constraintid="o6531" is_modifier="false" modifier="usually" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="rarely" name="some_measurement" notes="" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="central" id="o6531" name="awn" name_original="awn" src="d0_s8" type="structure" />
      <biological_entity constraint="2n" id="o6532" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="22" value_original="22" />
        <character name="quantity" src="d0_s8" value="44" value_original="44" />
        <character name="quantity" src="d0_s8" value="66" value_original="66" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Aristida purpurea var. wrightii grows on sandy to gravelly hills and flats from the southwestern United States to southern Mexico. It is the most robust variety of A. purpurea, and has dark, stout awns and long panicles. It may be confused with var. nealleyi, which has narrower lemmas and awns and a light-colored panicle, but it also intergrades with var. purpurea and var. parishii. Aristida purpurea forma brownii (Warnock) Allred &amp; Valdes-Reyna refers to plants with short central awns and lateral awns that are only 1-3 mm long.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.;Tex.;Colo.;Utah;Calif.;Okla.;Ariz.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>