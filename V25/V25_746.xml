<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">175</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="Scribn. ex Vasey" date="unknown" rank="species">dumosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species dumosa</taxon_hierarchy>
  </taxon_identification>
  <number>33</number>
  <other_name type="common_name">Bamboo muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, loosely cespitose.</text>
      <biological_entity id="o6232" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 100-300 cm tall, 3-6 mm thick, erect or ascending, woody below, branching at the middle and upper nodes, branches numerous, fascicled, and spreading;</text>
      <biological_entity id="o6233" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="height" src="d0_s2" to="300" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="thickness" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="below" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character constraint="at middle" constraintid="o6234" is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o6234" name="middle" name_original="middle" src="d0_s2" type="structure" />
      <biological_entity constraint="upper" id="o6235" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o6236" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="fascicled" value_original="fascicled" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes glabrous for most of their length, puberulent or glaucous below the nodes.</text>
      <biological_entity id="o6237" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character constraint="for" is_modifier="false" name="length" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character constraint="below nodes" constraintid="o6238" is_modifier="false" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o6238" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous, enlarged and flattened basally, somewhat chartaceous;</text>
      <biological_entity id="o6239" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="size" src="d0_s4" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
        <character is_modifier="false" modifier="somewhat" name="pubescence_or_texture" src="d0_s4" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.2-0.6 mm, membranous, truncate;</text>
      <biological_entity id="o6240" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades of the branch leaves 1.2-8 (12) cm long, 0.7-2.2 mm wide, flat or involute, glabrous abaxially, hirtellous adaxially;</text>
      <biological_entity id="o6241" name="blade" name_original="blades" src="d0_s6" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character name="length" src="d0_s6" unit="cm" value="12" value_original="12" />
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s6" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity constraint="branch" id="o6242" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o6241" id="r989" name="part_of" negation="false" src="d0_s6" to="o6242" />
    </statement>
    <statement id="d0_s7">
      <text>blades of the cauline leaves, particularly the lower cauline leaves, absent or greatly reduced.</text>
      <biological_entity id="o6243" name="blade" name_original="blades" src="d0_s7" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character is_modifier="false" name="presence" notes="" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="greatly" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o6244" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity constraint="lower cauline" id="o6245" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o6243" id="r990" name="part_of" negation="false" src="d0_s7" to="o6244" />
    </statement>
    <statement id="d0_s8">
      <text>Panicles numerous, terminal on the main culms and the branches, 1-4 cm long, 0.3-1.4 cm wide, lax, inconspicuous;</text>
      <biological_entity id="o6246" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s8" value="numerous" value_original="numerous" />
        <character constraint="on main culms" constraintid="o6247" is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" notes="" src="d0_s8" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s8" to="1.4" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <biological_entity constraint="main" id="o6247" name="culm" name_original="culms" src="d0_s8" type="structure" />
      <biological_entity id="o6248" name="branch" name_original="branches" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>primary panicle branches appressed or loosely spreading up to 40° from the rachises;</text>
      <biological_entity constraint="panicle" id="o6249" name="branch" name_original="branches" src="d0_s9" type="structure" constraint_original="primary panicle">
        <character is_modifier="false" name="orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character constraint="from rachises" constraintid="o6250" is_modifier="false" modifier="0-40°" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o6250" name="rachis" name_original="rachises" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>pedicels 0.1-1.5 mm.</text>
      <biological_entity id="o6251" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.2-3.1 mm, green or purplish.</text>
      <biological_entity id="o6252" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s11" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes subequal, 1-1.7 mm, glabrous, 1-veined, acute to acuminate, occasionally mucronate, mucros to 0.5 mm;</text>
      <biological_entity id="o6253" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="acuminate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s12" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o6254" name="mucro" name_original="mucros" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 2.3-3.1 mm, lanceolate, appressed-pubescent on the calluses and lower portion of the margins, apices acuminate, awned, awns 1-5 mm, flexuous;</text>
      <biological_entity id="o6255" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s13" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character constraint="on calluses" constraintid="o6256" is_modifier="false" name="pubescence" src="d0_s13" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity id="o6256" name="callus" name_original="calluses" src="d0_s13" type="structure" />
      <biological_entity constraint="lower" id="o6257" name="portion" name_original="portion" src="d0_s13" type="structure" />
      <biological_entity id="o6258" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <biological_entity id="o6259" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o6260" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s13" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <relation from="o6257" id="r991" name="part_of" negation="false" src="d0_s13" to="o6258" />
    </statement>
    <statement id="d0_s14">
      <text>paleas 2.3-3.1 mm, narrowly lanceolate, appressed-pubescent basally, acuminate;</text>
      <biological_entity id="o6261" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s14" to="3.1" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s14" value="appressed-pubescent" value_original="appressed-pubescent" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1.5-2 mm, purplish.</text>
      <biological_entity id="o6262" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 1.2-1.6 mm, fusiform, reddish-brown.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 40.</text>
      <biological_entity id="o6263" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s16" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6264" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia dumosa grows on rocky slopes, canyon ledges, and cliffs, in areas protected from grazing animals in oak-pine and thorn-scrub forests and oak-gramma savannahs, at elevations of 600-1800 m, from Arizona to southern Mexico.</discussion>
  <discussion>The bladeless cauline leaves and abundant branching from the middle and upper nodes make 'Bamboo Muhly' a very apt English name. North American Indians used it, after boiling, for chest and bowel ailments. It is native from southern Arizona to southern Mexico, but is also grown as an ornamental in the southwestern United States.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>