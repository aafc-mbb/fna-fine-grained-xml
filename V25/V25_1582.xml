<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">655</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ANDROPOGON</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="section">Leptopogon</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">floridanus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus andropogon;section leptopogon;species floridanus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>7</number>
  <other_name type="common_name">Florida bluestem</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, usually densely obpyramidal to oblanceolate above.</text>
      <biological_entity id="o19674" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="usually densely obpyramidal" name="shape" src="d0_s0" to="oblanceolate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 70-210 cm;</text>
      <biological_entity id="o19675" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="70" from_unit="cm" name="some_measurement" src="d0_s1" to="210" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes occasionally somewhat glaucous just below the node;</text>
      <biological_entity id="o19676" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character constraint="just below node" constraintid="o19677" is_modifier="false" modifier="occasionally somewhat" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o19677" name="node" name_original="node" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>branches straight, mostly erect to ascending.</text>
      <biological_entity id="o19678" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
        <character char_type="range_value" from="mostly erect" name="orientation" src="d0_s3" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths often scabrous, somteimes smooth;</text>
      <biological_entity id="o19679" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.4-1.2 mm, ciliate, cilia 0.2-1.3 mm;</text>
      <biological_entity id="o19680" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s5" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o19681" name="cilium" name_original="cilia" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 32-61 cm long, 2.9-5 mm wide, glabrous, rarely sparsely pubescent.</text>
      <biological_entity id="o19682" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="32" from_unit="cm" name="length" src="d0_s6" to="61" to_unit="cm" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely sparsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescence units (9) 50-210 per culm;</text>
      <biological_entity id="o19684" name="culm" name_original="culm" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>subtending sheaths (3) 4-5.9 (7) cm long, (1.5) 2-2.7 (3.6) mm wide;</text>
      <biological_entity constraint="inflorescence" id="o19683" name="unit" name_original="units" src="d0_s7" type="structure">
        <character name="atypical_quantity" src="d0_s7" value="9" value_original="9" />
        <character char_type="range_value" constraint="per culm" constraintid="o19684" from="50" name="quantity" src="d0_s7" to="210" />
        <character name="length" src="d0_s8" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s8" to="5.9" to_unit="cm" />
        <character name="width" src="d0_s8" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19685" name="sheath" name_original="sheaths" src="d0_s8" type="structure" />
      <relation from="o19683" id="r3329" name="subtending" negation="false" src="d0_s8" to="o19685" />
    </statement>
    <statement id="d0_s9">
      <text>peduncles (10) 19-48 (93) mm, with 2 (4) rames;</text>
      <biological_entity id="o19687" name="rame" name_original="rames" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
      <relation from="o19686" id="r3330" name="with" negation="false" src="d0_s9" to="o19687" />
    </statement>
    <statement id="d0_s10">
      <text>rames (2) 2.5-3.7 (4.5) cm, usually exserted at maturity, internodes evenly pubescent.</text>
      <biological_entity id="o19686" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="19" from_unit="mm" name="some_measurement" src="d0_s9" to="48" to_unit="mm" />
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s10" to="3.7" to_unit="cm" />
        <character constraint="at maturity" is_modifier="false" modifier="usually" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o19688" name="internode" name_original="internodes" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="evenly" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sessile spikelets (3.8) 4.4-4.8 (5.5) mm;</text>
      <biological_entity id="o19689" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="3.8" value_original="3.8" />
        <character char_type="range_value" from="4.4" from_unit="mm" name="some_measurement" src="d0_s11" to="4.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>callus hairs 1-3 mm;</text>
      <biological_entity constraint="callus" id="o19690" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keels of lower glumes glabrous below midlength;</text>
      <biological_entity id="o19691" name="keel" name_original="keels" src="d0_s13" type="structure" constraint="glume" constraint_original="glume; glume">
        <character constraint="below midlength" is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o19692" name="glume" name_original="glumes" src="d0_s13" type="structure" />
      <relation from="o19691" id="r3331" name="part_of" negation="false" src="d0_s13" to="o19692" />
    </statement>
    <statement id="d0_s14">
      <text>awns 5-15 mm;</text>
      <biological_entity id="o19693" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1 (3), 1.3-2 mm, usually yellow (sometimes purple).</text>
      <biological_entity id="o19694" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character name="atypical_quantity" src="d0_s15" value="3" value_original="3" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pedicellate spikelets vestigial or absent.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 20.</text>
      <biological_entity id="o19695" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19696" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Andropogon floridanus grows on sandy soils in southeastern Georgia and Florida, being most abundant in Pinus clausa scrublands. It usually occurs in small stands, but stands of about a hundred individuals have been observed.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.;Ala.;Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>