<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles M. Allen;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">558</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="genus">PASPALIDIUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalidium</taxon_hierarchy>
  </taxon_identification>
  <number>25.21</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial.</text>
      <biological_entity id="o25556" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 100 cm, not branching above the base.</text>
      <biological_entity id="o25557" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character constraint="above base" constraintid="o25558" is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o25558" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Auricles absent;</text>
      <biological_entity id="o25559" name="auricle" name_original="auricles" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules membranous and ciliate or of hairs.</text>
      <biological_entity id="o25560" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="of hairs" value_original="of hairs" />
      </biological_entity>
      <biological_entity id="o25561" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <relation from="o25560" id="r4311" name="consists_of" negation="false" src="d0_s3" to="o25561" />
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences panicles of racemosely arranged, spikelike branches, branches sometimes highly reduced, each panicle appearing spikelike;</text>
      <biological_entity constraint="inflorescences" id="o25562" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o25564" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s4" value="arranged" value_original="arranged" />
        <character is_modifier="true" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" modifier="sometimes highly" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o25565" name="panicle" name_original="panicle" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s4" value="arranged" value_original="arranged" />
        <character is_modifier="true" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" modifier="sometimes highly" name="size" src="d0_s4" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o25562" id="r4312" name="part_of" negation="false" src="d0_s4" to="o25564" />
      <relation from="o25562" id="r4313" name="part_of" negation="false" src="d0_s4" to="o25565" />
    </statement>
    <statement id="d0_s5">
      <text>branches 1-sided, terminating in a more or less inconspicuous bristle, bristles 2.5-4 mm;</text>
      <biological_entity id="o25566" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <biological_entity id="o25567" name="bristle" name_original="bristle" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="more or less" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
      <relation from="o25566" id="r4314" name="terminating in" negation="false" src="d0_s5" to="o25567" />
    </statement>
    <statement id="d0_s6">
      <text>disarticulation beneath the spikelets.</text>
      <biological_entity id="o25568" name="bristle" name_original="bristles" src="d0_s5" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25569" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <relation from="o25568" id="r4315" name="beneath" negation="false" src="d0_s6" to="o25569" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets subsessile, in 2 rows on 1 side of the branches, lacking subtending bristles, dorsally compressed, with 2 florets, upper glumes and upper florets appressed to the branch axes.</text>
      <biological_entity id="o25570" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="quantity" notes="" src="d0_s7" value="lacking" value_original="lacking" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o25571" name="row" name_original="rows" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o25572" name="side" name_original="side" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o25573" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <biological_entity id="o25574" name="bristle" name_original="bristles" src="d0_s7" type="structure" />
      <biological_entity id="o25575" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="upper" id="o25576" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character constraint="to branch axes" constraintid="o25578" is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="upper upper" id="o25577" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character constraint="to branch axes" constraintid="o25578" is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="branch" id="o25578" name="axis" name_original="axes" src="d0_s7" type="structure" />
      <relation from="o25570" id="r4316" name="in" negation="false" src="d0_s7" to="o25571" />
      <relation from="o25571" id="r4317" name="on" negation="false" src="d0_s7" to="o25572" />
      <relation from="o25572" id="r4318" name="part_of" negation="false" src="d0_s7" to="o25573" />
      <relation from="o25570" id="r4319" name="subtending" negation="false" src="d0_s7" to="o25574" />
      <relation from="o25570" id="r4320" name="with" negation="false" src="d0_s7" to="o25575" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes membranous;</text>
      <biological_entity id="o25579" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower glumes much shorter than the spikelets;</text>
      <biological_entity constraint="lower" id="o25580" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character constraint="than the spikelets" constraintid="o25581" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="much shorter" value_original="much shorter" />
      </biological_entity>
      <biological_entity id="o25581" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>upper glumes subequal to the upper florets;</text>
      <biological_entity constraint="upper" id="o25582" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character constraint="to upper florets" constraintid="o25583" is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="upper" id="o25583" name="floret" name_original="florets" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lower florets sterile or staminate;</text>
      <biological_entity constraint="lower" id="o25584" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s11" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower lemmas similar to the upper glumes in size and texture;</text>
      <biological_entity constraint="lower" id="o25585" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
      <biological_entity constraint="upper" id="o25586" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <relation from="o25585" id="r4321" name="to" negation="false" src="d0_s12" to="o25586" />
    </statement>
    <statement id="d0_s13">
      <text>upper florets bisexual;</text>
      <biological_entity constraint="upper" id="o25587" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper lemmas indurate, rugose, unawned, yellow or brown;</text>
      <biological_entity constraint="upper" id="o25588" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="relief" src="d0_s14" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper paleas similar to their lemmas;</text>
      <biological_entity constraint="upper" id="o25589" name="palea" name_original="paleas" src="d0_s15" type="structure" />
      <biological_entity id="o25590" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
      <relation from="o25589" id="r4322" name="to" negation="false" src="d0_s15" to="o25590" />
    </statement>
    <statement id="d0_s16">
      <text>anthers 3.</text>
    </statement>
    <statement id="d0_s17">
      <text>x = 9.</text>
      <biological_entity id="o25591" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="x" id="o25592" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalidium is a genus of approximately 40 species, one of which is native to the Flora region. It grows in tropical regions throughout the world. Most of its species have an inflorescence of well-spaced, unilateral, spicate branches and the resemblance to Paspalum is evident, but species with closely crowded, highly reduced branches are easily mistaken for species of Setaria, the terminal bristle resembling a single, subtending bristle.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Okla.;Tex.;La.;Mo.;Virgin Islands;Ala.;Pacific Islands (Hawaii);Ga.;S.C.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>