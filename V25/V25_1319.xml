<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">497</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">UROCHLOA</taxon_name>
    <taxon_name authority="(L.) T.Q. Nguyen" date="unknown" rank="species">ramosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus urochloa;species ramosa</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ramosum</taxon_name>
    <taxon_hierarchy>genus panicum;species ramosum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Brachiaria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ramosa</taxon_name>
    <taxon_hierarchy>genus brachiaria;species ramosa</taxon_hierarchy>
  </taxon_identification>
  <number>7</number>
  <other_name type="common_name">Browntop millet</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted.</text>
      <biological_entity id="o27728" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-65 cm, decumbent, rooting or not at the lower nodes;</text>
      <biological_entity id="o27729" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="65" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character name="architecture" src="d0_s2" value="not" value_original="not" />
      </biological_entity>
      <biological_entity constraint="lower" id="o27730" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <relation from="o27729" id="r4728" name="at" negation="false" src="d0_s2" to="o27730" />
    </statement>
    <statement id="d0_s3">
      <text>nodes pubescent.</text>
      <biological_entity id="o27731" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths usually puberulous, sometimes glabrous or sparsely pilose, margins ciliate;</text>
      <biological_entity id="o27732" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s4" value="puberulous" value_original="puberulous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o27733" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.8-1.7 mm;</text>
      <biological_entity id="o27734" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s5" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 2-25 cm long, 4-14 mm wide, glabrous, margins scabrous.</text>
      <biological_entity id="o27735" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="25" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s6" to="14" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27736" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 3-13 cm, simple, with 3-15 spikelike primary branches;</text>
      <biological_entity id="o27737" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="13" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity constraint="primary" id="o27738" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="15" />
        <character is_modifier="true" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <relation from="o27737" id="r4729" name="with" negation="false" src="d0_s7" to="o27738" />
    </statement>
    <statement id="d0_s8">
      <text>primary branches 1-8 cm, divergent, axils glabrous, axes 0.4-0.6 mm wide, triquetrous, glabrous, scabrous, or pubescent, with or without some papillose-based hairs;</text>
      <biological_entity constraint="primary" id="o27739" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="8" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o27740" name="axil" name_original="axils" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27741" name="axis" name_original="axes" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s8" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="triquetrous" value_original="triquetrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o27742" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o27741" id="r4730" name="with or without" negation="false" src="d0_s8" to="o27742" />
    </statement>
    <statement id="d0_s9">
      <text>secondary branches, if present, confined to the lower branches;</text>
      <biological_entity constraint="secondary" id="o27743" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o27744" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <relation from="o27743" id="r4731" name="confined to the" negation="false" src="d0_s9" to="o27744" />
    </statement>
    <statement id="d0_s10">
      <text>pedicels shorter than the spikelets, scabrous or pubescent.</text>
      <biological_entity id="o27745" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character constraint="than the spikelets" constraintid="o27746" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o27746" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.5-3.4 mm long, 1.3-2 mm wide, ellipsoid, apices broadly acute to acute, paired, appressed to the branches.</text>
      <biological_entity id="o27747" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s11" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity id="o27748" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s11" to="acute" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="paired" value_original="paired" />
        <character constraint="to branches" constraintid="o27749" is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o27749" name="branch" name_original="branches" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes scarcely separated, rachilla internode between the glumes not pronounced;</text>
      <biological_entity id="o27750" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="scarcely" name="arrangement" src="d0_s12" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity constraint="between rachilla" constraintid="o27752" id="o27751" name="internode" name_original="internode" src="d0_s12" type="structure" constraint_original="between  rachilla, ">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="pronounced" value_original="pronounced" />
      </biological_entity>
      <biological_entity id="o27752" name="glume" name_original="glumes" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 1-1.5 mm, 1/3 – 1/2 as long as the spikelets, glabrous, 3-5-veined;</text>
      <biological_entity constraint="lower" id="o27753" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o27754" from="1/3" name="quantity" src="d0_s13" to="1/2" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o27754" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 2.5-3.4 mm, usually puberulent, sometimes glabrous, margins sometimes somewhat pubescent, 7-9-veined, without evident cross venation;</text>
      <biological_entity constraint="upper" id="o27755" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="3.4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27756" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="sometimes somewhat" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="7-9-veined" value_original="7-9-veined" />
      </biological_entity>
      <biological_entity id="o27757" name="vein" name_original="venation" src="d0_s14" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s14" value="evident" value_original="evident" />
        <character is_modifier="true" name="shape" src="d0_s14" value="cross" value_original="cross" />
      </biological_entity>
      <relation from="o27756" id="r4732" name="without" negation="false" src="d0_s14" to="o27757" />
    </statement>
    <statement id="d0_s15">
      <text>lower florets sterile, lower lemmas 2.4-3.3 mm, usually puberulent or occasionally glabrous, margins not ciliate, without cross venation, 5-veined;</text>
      <biological_entity constraint="lower" id="o27758" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity constraint="lower" id="o27759" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s15" to="3.3" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s15" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27760" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s15" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s15" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o27761" name="vein" name_original="venation" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="cross" value_original="cross" />
      </biological_entity>
      <relation from="o27760" id="r4733" name="without" negation="false" src="d0_s15" to="o27761" />
    </statement>
    <statement id="d0_s16">
      <text>upper lemmas 2.3-3.3 mm, acute, mucronate;</text>
      <biological_entity constraint="upper" id="o27762" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s16" to="3.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s16" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 0.7-1.2 mm.</text>
      <biological_entity id="o27763" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s17" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 1.2-2.3 mm;</text>
      <biological_entity id="o27764" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s18" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>hila punctiform.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 36 (usually);</text>
      <biological_entity constraint="2n" id="o27766" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="36" value_original="36" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>also 14, 28, 32, 42, 46, 72.</text>
      <biological_entity id="o27765" name="hilum" name_original="hila" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="punctiform" value_original="punctiform" />
        <character name="quantity" src="d0_s21" value="14" value_original="14" />
        <character name="quantity" src="d0_s21" value="28" value_original="28" />
        <character name="quantity" src="d0_s21" value="32" value_original="32" />
        <character name="quantity" src="d0_s21" value="42" value_original="42" />
        <character name="quantity" src="d0_s21" value="46" value_original="46" />
        <character name="quantity" src="d0_s21" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <discussion>A weedy species of tropical Africa and Asia, Urochloa ramosa has spread throughout the tropics and subtropics, including the southeastern United States. It is considered a weed in the Flora area, but it is is cultivated in India as a grain and forage crop; the grain is sometimes used for birdseed.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Miss.;Tex.;La.;N.C.;Ala.;Tenn.;S.C.;Va.;Ark.;Ga.;Ky.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>