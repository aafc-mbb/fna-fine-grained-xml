<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Stephen J. Darbyshire; Henry E. Connor;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">309</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="N.P. Barker &amp; H.P. Under" date="unknown" rank="subfamily">DANTHONIOIDEAE</taxon_name>
    <taxon_name authority="Zotov" date="unknown" rank="tribe">DANTHONIEAE</taxon_name>
    <taxon_name authority="Steud." date="unknown" rank="genus">RYTIDOSPERMA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily danthonioideae;tribe danthonieae;genus rytidosperma</taxon_hierarchy>
  </taxon_identification>
  <number>20.05</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose to somewhat spreading, sometimes shortly rhizomatous.</text>
      <biological_entity id="o17259" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="somewhat" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="sometimes shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (1.5) 30-90 (140) cm.</text>
      <biological_entity id="o17260" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="90" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open, glabrous or hairy, apices with tufts of hair, these sometimes extending across the collar;</text>
      <biological_entity id="o17261" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o17262" name="apex" name_original="apices" src="d0_s3" type="structure" />
      <biological_entity id="o17263" name="tuft" name_original="tufts" src="d0_s3" type="structure" />
      <biological_entity id="o17264" name="hair" name_original="hair" src="d0_s3" type="structure" />
      <biological_entity id="o17265" name="collar" name_original="collar" src="d0_s3" type="structure" />
      <relation from="o17262" id="r2899" name="with" negation="false" src="d0_s3" to="o17263" />
      <relation from="o17263" id="r2900" name="part_of" negation="false" src="d0_s3" to="o17264" />
      <relation from="o17262" id="r2901" modifier="sometimes" name="extending across the" negation="false" src="d0_s3" to="o17265" />
    </statement>
    <statement id="d0_s4">
      <text>ligules of hairs;</text>
      <biological_entity id="o17266" name="ligule" name_original="ligules" src="d0_s4" type="structure" />
      <biological_entity id="o17267" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <relation from="o17266" id="r2902" name="consists_of" negation="false" src="d0_s4" to="o17267" />
    </statement>
    <statement id="d0_s5">
      <text>blades persistent or disarticulating.</text>
      <biological_entity id="o17268" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, racemes or panicles.</text>
      <biological_entity id="o17269" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o17270" name="raceme" name_original="racemes" src="d0_s6" type="structure" />
      <biological_entity id="o17271" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets with 3-10 florets;</text>
      <biological_entity id="o17272" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o17273" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
      <relation from="o17272" id="r2903" name="with" negation="false" src="d0_s7" to="o17273" />
    </statement>
    <statement id="d0_s8">
      <text>florets bisexual, terminal florets reduced;</text>
      <biological_entity id="o17274" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>disarticulation above the glumes and between the florets.</text>
      <biological_entity constraint="terminal" id="o17275" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o17276" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <relation from="o17275" id="r2904" name="above the glumes and between" negation="false" src="d0_s9" to="o17276" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes (2) 8-20 mm, subequal or equal, usually exceeding the florets, stiffly membranous;</text>
      <biological_entity id="o17277" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s10" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="stiffly" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o17278" name="floret" name_original="florets" src="d0_s10" type="structure" />
      <relation from="o17277" id="r2905" modifier="usually" name="exceeding the" negation="false" src="d0_s10" to="o17278" />
    </statement>
    <statement id="d0_s11">
      <text>calluses with lateral tufts of stiff hairs;</text>
      <biological_entity id="o17279" name="callus" name_original="calluses" src="d0_s11" type="structure" />
      <biological_entity constraint="lateral" id="o17280" name="tuft" name_original="tufts" src="d0_s11" type="structure" />
      <biological_entity id="o17281" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s11" value="stiff" value_original="stiff" />
      </biological_entity>
      <relation from="o17279" id="r2906" name="with" negation="false" src="d0_s11" to="o17280" />
      <relation from="o17280" id="r2907" name="part_of" negation="false" src="d0_s11" to="o17281" />
    </statement>
    <statement id="d0_s12">
      <text>lemmas ovate to lanceolate, with 2 complete or incomplete transverse rows of tufts of hairs, sometimes reduced to marginal tufts, 5-9-veined, apices bilobed, lobes usually at least as long as the body, acute, acuminate, or aristate, awned from between the lobes, awns longer than the lobes, twisted, usually geniculate;</text>
      <biological_entity id="o17282" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="size" notes="" src="d0_s12" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="5-9-veined" value_original="5-9-veined" />
      </biological_entity>
      <biological_entity id="o17283" name="row" name_original="rows" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="architecture" src="d0_s12" value="complete" value_original="complete" />
        <character is_modifier="true" name="architecture" src="d0_s12" value="incomplete" value_original="incomplete" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s12" value="transverse" value_original="transverse" />
      </biological_entity>
      <biological_entity id="o17284" name="tuft" name_original="tufts" src="d0_s12" type="structure" />
      <biological_entity id="o17285" name="hair" name_original="hairs" src="d0_s12" type="structure" />
      <biological_entity constraint="marginal" id="o17286" name="tuft" name_original="tufts" src="d0_s12" type="structure" />
      <biological_entity id="o17287" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="bilobed" value_original="bilobed" />
      </biological_entity>
      <biological_entity id="o17288" name="lobe" name_original="lobes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="aristate" value_original="aristate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="aristate" value_original="aristate" />
        <character constraint="from " constraintid="o17290" is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o17289" name="body" name_original="body" src="d0_s12" type="structure" />
      <biological_entity id="o17290" name="lobe" name_original="lobes" src="d0_s12" type="structure" />
      <biological_entity id="o17291" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character constraint="than the lobes" constraintid="o17292" is_modifier="false" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="twisted" value_original="twisted" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s12" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity id="o17292" name="lobe" name_original="lobes" src="d0_s12" type="structure" />
      <relation from="o17282" id="r2908" name="with" negation="false" src="d0_s12" to="o17283" />
      <relation from="o17283" id="r2909" name="part_of" negation="false" src="d0_s12" to="o17284" />
      <relation from="o17283" id="r2910" name="part_of" negation="false" src="d0_s12" to="o17285" />
      <relation from="o17288" id="r2911" name="as long as" negation="false" src="d0_s12" to="o17289" />
    </statement>
    <statement id="d0_s13">
      <text>lodicules 2, fleshy, with hairs or glabrous.</text>
      <biological_entity id="o17293" name="lodicule" name_original="lodicules" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="texture" src="d0_s13" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17294" name="hair" name_original="hairs" src="d0_s13" type="structure" />
      <relation from="o17293" id="r2912" name="with" negation="false" src="d0_s13" to="o17294" />
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 1.2-3 mm, obovate to elliptic.</text>
      <biological_entity id="o17295" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s14" to="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Cleistogenes absent, x = 12.</text>
      <biological_entity id="o17296" name="cleistogene" name_original="cleistogenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o17297" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Rytidosperma, as interpreted here and by Edgar and Connor (2000), is a genus of about 45 species that are native to south and southeastern Asia, Australia, New Zealand, and South America. Linder and Verboom (1996) advocated a narrower interpretation of the genus than Edgar and Connor, but acknowledged that "there is an almost equally strong case for recognizing a single, large genus, Rytidosperma" (p. 607). According to their interpretation, all three species treated here would be placed in Austrodanthonia H.P. Linder (Linder 1997).</discussion>
  <discussion>Several species of Rytidosperma have been cultivated in research plots or forage trials in North America. The three species treated here have been tried in several states but have escaped cultivation and persisted only in California and Oregon (Weintraub 1953). They have been included in commercial seed mixtures for forage planting in Australia and New Zealand. Other species that have been grown experimentally in both the United States and Canada include R. caespitosum (Gaudich.) Connor &amp; Edgar, R. setaceum (R. Br.) Connor &amp; Edgar, and R. tenuius (Steud.) A. Hansen &amp; P. Sunding. They are not known to have escaped or persisted in North America.</discussion>
  <discussion>H.E. Connor identified two additional species of Rytidosperma among specimens that have been found as escapes in Alameda and San Mateo counties, California. They are Rytidosperma caespitosa (Gaudich.) Connor &amp; Edgar, and R. richardsonii (Cashmore) Connor &amp; Edgar. Both are native to Australia. Rytidosperma caespitosa differs from the three species included in volume 25 in having two rows of tufts of hair in which the upper row of hairs greatly exceeds the lemma body. Like R. biannulare, it has intravaginal branching. Rytidosperma richardsonii has lemma lobes that are shorter than the lemma body, and obovate paleas that are 2-2.5 mm wide.</discussion>
  <references>
    <reference>Blumler, M. 2001. Notes and comments. Fremontia 29:36</reference>
    <reference>Connor, H.E. and E. Edgar. 1979. Rytidosperma Steudel (Nothodantbonia Zotov) in New Zealand. New Zealand J. Bot. 17:311-337</reference>
    <reference>Edgar, E. and H.E. Connor. 2000. Flora of New Zealand, vol. 5. Manaaki Whenua Press, Lincoln, New Zealand. 650 pp.</reference>
    <reference>Linder, H.P. 1997. Nomenclatural corrections in the Rytidosperma complex (Danthonieae, Poaceae). Telopea 7:269-274</reference>
    <reference>Linder, H.P. and G.A. Verboom. 1996. Generic limits in the Rytidosperma (Danthonieae, Poaceae) complex. Telopea 6:597-627</reference>
    <reference>Murphy, A.H. and R.M. Love. 1950. Hairy oatgrass, Danthonia pilosa R. Br., as a weedy range grass. Bull. Calif. Dep. Agric. 39:118-124</reference>
    <reference>Myers, W.M. 1947. Cytology and genetics of forage grasses (concluded). Bot. Re.-. 7:369-419</reference>
    <reference>Vickery, J.W. 1956. A revision of the Australian species of Danthonia DC. Contr. New South Wales Natl. Herb. 2:249-325</reference>
    <reference>Weintraub, EC. 1953. Grasses Introduced into the United States. Agricultural Handbook No. 58. Forest Service, U.S. Department of Agriculture, Washington, D.C., U.S.A. 79 pp.</reference>
    <reference>Zotov, V.D. 1963. Synopsis of the grass subfamily Arundinoideae in New Zealand. New Zealand J. Bot. 1:78-136.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Pacific Islands (Hawaii);Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Upper row of lemma hairs in a more or less continuous row of tufts, the hairs much exceeding the base of the awn; shoots intravaginal, without scaly cataphylls</description>
      <determination>2 Rytidosperma biannulare</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Upper row of lemma hairs in isolated tufts or only at the margins, the hairs not or only just exceeding the base of the awn; some or most shoots extravaginal and with scaly cataphylls.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Callus hairs usually overlapping the lower row of lemma hairs; upper row of lemma hairs often reduced to marginal tufts, sometimes scanty medial tufts also present</description>
      <determination>1 Rytidosperma penicillatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Callus hairs short, rarely reaching the lower row of lemma hairs; upper row of lemma hairs usually with scanty medial tufts</description>
      <determination>3 Rytidosperma racemosum</determination>
    </key_statement>
  </key>
</bio:treatment>