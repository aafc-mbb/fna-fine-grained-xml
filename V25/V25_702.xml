<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">141</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Hack." date="unknown" rank="genus">CALAMOVILFA</taxon_name>
    <taxon_name authority="(Hook.) Scribn." date="unknown" rank="species">longifolia</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus calamovilfa;species longifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ammophila</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">longifolia</taxon_name>
    <taxon_hierarchy>genus ammophila;species longifolia</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Prairie sandreed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Rhizomes elongate, covered with shiny, coriaceous, scalelike leaves.</text>
      <biological_entity id="o28056" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o28057" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="true" name="reflectance" src="d0_s0" value="shiny" value_original="shiny" />
        <character is_modifier="true" name="texture" src="d0_s0" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="true" name="shape" src="d0_s0" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <relation from="o28056" id="r4778" name="covered with" negation="false" src="d0_s0" to="o28057" />
    </statement>
    <statement id="d0_s1">
      <text>Culms to 2.4 m.</text>
      <biological_entity id="o28058" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="2.4" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous to densely pubescent;</text>
      <biological_entity id="o28059" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s2" to="densely pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 0.7-2.5 mm;</text>
      <biological_entity id="o28060" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s3" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades to 64 cm long, about 12 mm wide.</text>
      <biological_entity id="o28061" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s4" to="64" to_unit="cm" />
        <character name="width" src="d0_s4" unit="mm" value="12" value_original="12" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 15-78 cm long, 1.7-26.4 cm wide;</text>
      <biological_entity id="o28062" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s5" to="78" to_unit="cm" />
        <character char_type="range_value" from="1.7" from_unit="cm" name="width" src="d0_s5" to="26.4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches to 33 cm, erect to strongly divergent, lowermost branches sometimes reflexed.</text>
      <biological_entity id="o28063" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="33" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s6" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity constraint="lowermost" id="o28064" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 5-8.5 mm.</text>
      <biological_entity id="o28065" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glumes straight;</text>
      <biological_entity id="o28066" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower glumes 3.5-6.5 mm;</text>
      <biological_entity constraint="lower" id="o28067" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="6.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 5-8.2 mm;</text>
      <biological_entity constraint="upper" id="o28068" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 4.5-7.1 mm, straight, glabrous;</text>
      <biological_entity id="o28069" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s11" to="7.1" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>paleas 4.4-6.9 mm, glabrous.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 40, ca. 60.</text>
      <biological_entity id="o28070" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.4" from_unit="mm" name="some_measurement" src="d0_s12" to="6.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28071" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="40" value_original="40" />
        <character name="quantity" src="d0_s13" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Calamovilfa longifolia usually grows in sand or sandy soils, but is occasionally found in clay soils or loess. Two geographically contiguous varieties exist. They differ as shown in the following key; the differences between the two are more striking in the field.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ill.;Ind.;Colo.;N.Mex.;Wash.;Mont.;Alta.;B.C.;Man.;Ont.;Sask.;Minn.;Mich.;Iowa;Idaho;Kans.;Mo.;N.Dak.;Nebr.;Pa.;S.Dak.;Wyo.;Wis.;N.Y.;Ohio</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Most spikelets overlapping no more than 1 other spikelet, usually with a brownish cast</description>
      <determination>Calamovilfa longifolia var. magna</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Most spikelets overlapping 2-3 other spikelets, usually without a brownish cast</description>
      <determination>Calamovilfa longifolia var. longifolia</determination>
    </key_statement>
  </key>
</bio:treatment>