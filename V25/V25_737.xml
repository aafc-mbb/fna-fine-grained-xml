<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">169</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="Trin." date="unknown" rank="species">spiciformis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species spiciformis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muhlenbergia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">parviglumis</taxon_name>
    <taxon_hierarchy>genus muhlenbergia;species parviglumis</taxon_hierarchy>
  </taxon_identification>
  <number>25</number>
  <other_name type="common_name">Longawn muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o11730" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 25-80 cm, erect, wiry;</text>
      <biological_entity id="o11731" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="wiry" value_original="wiry" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes mostly glabrous, strigose or glabrous below the nodes.</text>
      <biological_entity id="o11732" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
        <character constraint="below nodes" constraintid="o11733" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11733" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths scabridulous, not becoming spirally coiled when old;</text>
      <biological_entity id="o11734" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="when old" name="architecture" src="d0_s4" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-3 mm, membranous, acuminate, deeply lacerate;</text>
      <biological_entity id="o11735" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 2-12 cm long, 1-3 mm wide, flat to involute, scabridulous abaxially, hirtellous or scabrous adaxially.</text>
      <biological_entity id="o11736" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s6" to="involute" />
        <character is_modifier="false" modifier="abaxially" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hirtellous" value_original="hirtellous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-18 (20) cm long, 0.2-2.8 cm wide, contracted, not dense, sometimes interrupted below;</text>
      <biological_entity id="o11737" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="18" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s7" to="2.8" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="not" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="sometimes; below" name="architecture" src="d0_s7" value="interrupted" value_original="interrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>panicle branches 0.6-5 cm, appressed or diverging up to 30° from the rachises, spikelet-bearing to the base;</text>
      <biological_entity constraint="panicle" id="o11738" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character constraint="from rachises" constraintid="o11739" is_modifier="false" modifier="0-30°" name="orientation" src="d0_s8" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o11739" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
      <biological_entity id="o11740" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o11738" id="r1931" name="to" negation="false" src="d0_s8" to="o11740" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels 0.1-3 mm.</text>
      <biological_entity id="o11741" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s9" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3-4 mm.</text>
      <biological_entity id="o11742" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes unequal, 0.3-1 mm, shorter than the florets, 1-veined, unawned;</text>
      <biological_entity id="o11743" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
        <character constraint="than the florets" constraintid="o11744" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o11744" name="floret" name_original="florets" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lower glumes shorter than the upper glumes, obtuse to acute, sometimes erose;</text>
      <biological_entity constraint="lower" id="o11745" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character constraint="than the upper glumes" constraintid="o11746" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s12" to="acute" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_relief" src="d0_s12" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity constraint="upper" id="o11746" name="glume" name_original="glumes" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas 2.8-4 mm, narrowly lanceolate, purplish, scabrous, sparsely appressed-pubescent on the calluses and lower 1/4 of the midveins and margins, hairs shorter than 0.3 mm, apices acuminate, awned, awns (10) 20-40 mm;</text>
      <biological_entity id="o11747" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character constraint="on calluses" constraintid="o11748" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="appressed-pubescent" value_original="appressed-pubescent" />
        <character is_modifier="false" name="position" notes="" src="d0_s13" value="lower" value_original="lower" />
        <character constraint="of margins" constraintid="o11750" name="quantity" src="d0_s13" value="1/4" value_original="1/4" />
      </biological_entity>
      <biological_entity id="o11748" name="callus" name_original="calluses" src="d0_s13" type="structure" />
      <biological_entity id="o11749" name="midvein" name_original="midveins" src="d0_s13" type="structure" />
      <biological_entity id="o11750" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <biological_entity id="o11751" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s13" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <biological_entity id="o11752" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o11753" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="20" from_unit="mm" name="some_measurement" src="d0_s13" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas 2.6-3.9 mm, narrowly lanceolate, intercostal region sparsely pubescent on the basal 1/3, apices acuminate, scabrous;</text>
      <biological_entity id="o11754" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s14" to="3.9" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o11755" name="region" name_original="region" src="d0_s14" type="structure">
        <character constraint="on basal 1/3" constraintid="o11756" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o11756" name="1/3" name_original="1/3" src="d0_s14" type="structure" />
      <biological_entity id="o11757" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 0.9-1.6 mm, purplish.</text>
      <biological_entity id="o11758" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s15" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 2-2.6 mm, fusiform, brownish.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 40.</text>
      <biological_entity id="o11759" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11760" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia spiciformis grows on rocky slopes, cliffs, and calcareous rock outcrops, often in thorn-scrub and open woodland communities. Its elevational range is 450-2800 m; its geographic range extends from the southwestern United States to northern Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>