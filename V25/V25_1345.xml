<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">513</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="genus">ERIOCHLOA</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">aristata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus eriochloa;species aristata</taxon_hierarchy>
  </taxon_identification>
  <number>8</number>
  <other_name type="common_name">Bearded cupgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o18837" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 40-100 cm, erect or decumbent, sometimes rooting at the lower nodes;</text>
      <biological_entity id="o18838" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o18839" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18839" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>internodes glabrous;</text>
      <biological_entity id="o18840" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes 3-10, puberulent.</text>
      <biological_entity id="o18841" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" to="10" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o18842" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.5-0.8 (2) mm;</text>
      <biological_entity id="o18843" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 6-20 cm long, 6-20 mm wide, linear to lanceolate, flat or folded, straight or lax, glabrous (rarely sparsely pubescent) adaxially.</text>
      <biological_entity id="o18844" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="lanceolate flat or folded" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="lanceolate flat or folded" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 5-20 cm long, 1-3 cm wide, loosely contracted;</text>
      <biological_entity id="o18845" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s8" to="20" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s8" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachises hairy;</text>
      <biological_entity id="o18846" name="rachis" name_original="rachises" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>branches 16-30, 20-35 mm long, 0.3-0.5 mm wide, divergent to spreading, setose, not winged, with 20-35 spikelets in unequally pedicellate pairs;</text>
      <biological_entity id="o18847" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="16" name="quantity" src="d0_s10" to="30" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s10" to="35" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s10" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="setose" value_original="setose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o18848" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s10" to="35" />
      </biological_entity>
      <biological_entity id="o18849" name="pair" name_original="pairs" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="unequally" name="architecture" src="d0_s10" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o18847" id="r3182" name="with" negation="false" src="d0_s10" to="o18848" />
      <relation from="o18848" id="r3183" name="in" negation="false" src="d0_s10" to="o18849" />
    </statement>
    <statement id="d0_s11">
      <text>pedicels 0.5-3 mm, hairy, with some hairs 0.5-2.5 mm long, at least distally.</text>
      <biological_entity id="o18850" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o18851" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
      <relation from="o18850" id="r3184" modifier="at-least distally; distally" name="with" negation="false" src="d0_s11" to="o18851" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 4-8.8 mm long, 1.1-1.6 mm wide, lanceolate.</text>
      <biological_entity id="o18852" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="8.8" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s12" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Upper glumes 1-1.1 times as long as the lower lemmas, lanceolate, pilose or scabrous above, 5-veined, acuminate and awned, awns 0.5-3.5 mm;</text>
      <biological_entity constraint="upper" id="o18853" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character constraint="lemma" constraintid="o18854" is_modifier="false" name="length" src="d0_s13" value="1-1.1 times as long as the lower lemmas" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="5-veined" value_original="5-veined" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18854" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity id="o18855" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o18856" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower lemmas 4-8 mm long, 1.1-1.6 mm wide, lanceolate, setose, 3-7-veined, acuminate, mucronate, mucro less than 0.4 mm;</text>
      <biological_entity constraint="lower" id="o18857" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s15" to="8" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s15" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="setose" value_original="setose" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-7-veined" value_original="3-7-veined" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o18858" name="mucro" name_original="mucro" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower paleas absent;</text>
      <biological_entity constraint="lower" id="o18859" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers absent;</text>
      <biological_entity id="o18860" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper lemmas (2) 3-4 (6) mm, 0.4-0.6 times as long as the lower lemmas, indurate, elliptic, acute to rounded, 5-veined, awned, awns 0.2-0.8 mm;</text>
      <biological_entity constraint="upper" id="o18861" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character name="atypical_some_measurement" src="d0_s18" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s18" to="4" to_unit="mm" />
        <character constraint="lemma" constraintid="o18862" is_modifier="false" name="length" src="d0_s18" value="0.4-0.6 times as long as the lower lemmas" />
        <character is_modifier="false" name="texture" src="d0_s18" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="shape" src="d0_s18" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s18" to="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="5-veined" value_original="5-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18862" name="lemma" name_original="lemmas" src="d0_s18" type="structure" />
      <biological_entity id="o18863" name="awn" name_original="awns" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s18" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>upper paleas indurate, rugose.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 36.</text>
      <biological_entity constraint="upper" id="o18864" name="palea" name_original="paleas" src="d0_s19" type="structure">
        <character is_modifier="false" name="texture" src="d0_s19" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="relief" src="d0_s19" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18865" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eriochloa aristata is a weed of moist swales, roadsides, and irrigated fields of the southwestern United States. Its range extends through Mexico and Central America to Colombia. There are three specimens from Oktibbeha County, Mississippi, two made in 1890 and one made in 1960. The last collection was from a waste area, which suggests that the species is now established there. The other two were both made in Starkville, where there is an experimental farm. The labels on the two specimens do not state whether the collections were made from experimental plantings or from plants that had escaped from such plantings.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Ariz.;Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>