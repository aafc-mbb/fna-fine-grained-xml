<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">546</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">SETARIA</taxon_name>
    <taxon_name authority="W.E. Fox" date="unknown" rank="subgenus">Reverchoniae</taxon_name>
    <taxon_name authority="(Vasey) Pilg." date="unknown" rank="species">reverchonii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus setaria;subgenus reverchoniae;species reverchonii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">reverchonii</taxon_name>
    <taxon_hierarchy>genus panicum;species reverchonii</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, rhizomes, short, sometimes knotty.</text>
      <biological_entity id="o19906" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19907" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-90 cm;</text>
      <biological_entity id="o19908" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="90" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous, strigose, or with appressed hairs.</text>
      <biological_entity id="o19909" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o19910" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o19909" id="r3375" name="with" negation="false" src="d0_s3" to="o19910" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths with papillose-based hairs, sometimes nearly glabrous, margins ciliate distally;</text>
      <biological_entity id="o19911" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes nearly" name="pubescence" notes="" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19912" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o19913" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o19911" id="r3376" name="with" negation="false" src="d0_s4" to="o19912" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-2 mm, of stiff hairs;</text>
      <biological_entity id="o19914" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19915" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s5" value="stiff" value_original="stiff" />
      </biological_entity>
      <relation from="o19914" id="r3377" name="consists_of" negation="false" src="d0_s5" to="o19915" />
    </statement>
    <statement id="d0_s6">
      <text>blades 4-30 cm long, 1-7 mm wide, involute, stiff, scabridulous and narrowed basally.</text>
      <biological_entity id="o19916" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" name="fragility" src="d0_s6" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s6" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 5-20 cm, erect, slender, interrupted;</text>
      <biological_entity id="o19917" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s7" to="20" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="interrupted" value_original="interrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>rachises scabrous;</text>
      <biological_entity id="o19918" name="rachis" name_original="rachises" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bristles 2-8 mm.</text>
      <biological_entity id="o19919" name="bristle" name_original="bristles" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2.1-4.5 mm, elliptic to obovate, randomly dis¬tributed on the branch axes.</text>
      <biological_entity id="o19920" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s10" to="obovate" />
      </biological_entity>
      <biological_entity constraint="branch" id="o19921" name="axis" name_original="axes" src="d0_s10" type="structure" />
      <relation from="o19920" id="r3378" modifier="randomly" name="on" negation="false" src="d0_s10" to="o19921" />
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes 1/2 as long as the spikelets, 5-7-veined;</text>
      <biological_entity constraint="lower" id="o19922" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character constraint="as-long-as spikelets" constraintid="o19923" name="quantity" src="d0_s11" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
      <biological_entity id="o19923" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>upper glumes equaling the upper lemmas, 7-9-veined;</text>
      <biological_entity constraint="upper" id="o19924" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="7-9-veined" value_original="7-9-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o19925" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas equaling the upper lemmas;</text>
      <biological_entity constraint="lower" id="o19926" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity constraint="upper" id="o19927" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower paleas absent;</text>
      <biological_entity constraint="lower" id="o19928" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper lemmas indurate, finely and transversely rugose;</text>
      <biological_entity constraint="upper" id="o19929" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="texture" src="d0_s15" value="indurate" value_original="indurate" />
        <character is_modifier="false" modifier="finely; transversely" name="relief" src="d0_s15" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper paleas similar to the upper lemmas.</text>
      <biological_entity constraint="upper" id="o19930" name="palea" name_original="paleas" src="d0_s16" type="structure" />
      <biological_entity constraint="upper" id="o19931" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <relation from="o19930" id="r3379" name="to" negation="false" src="d0_s16" to="o19931" />
    </statement>
  </description>
  <discussion>Setaria reverchonii grows in sandy prairies and limestone hills from eastern New Mexico, southwestern Oklahoma, and Texas to northern Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;Fla.;N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Blades usually more than 15 cm long; spikelets 3.5-4.5 mm long</description>
      <determination>Setaria reverchonii subsp. reverchonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Blades usually less than 15 cm long; spikelets 2.1-3.2 mm long. 2. Blades 2-4 mm wide; spikelets about 2.5 mm long</description>
      <determination>Setaria reverchonii subsp. ramiseta</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Blades 4-7 mm wide; spikelets about 3-3.2 mm long</description>
      <determination>Setaria reverchonii subsp. firmula</determination>
    </key_statement>
  </key>
</bio:treatment>