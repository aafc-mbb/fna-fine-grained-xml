<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">196</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="CO. Goodd." date="unknown" rank="species">brevis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species brevis</taxon_hierarchy>
  </taxon_identification>
  <number>64</number>
  <other_name type="common_name">Short muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted.</text>
      <biological_entity id="o4227" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 3-20 cm.</text>
      <biological_entity id="o4228" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths often longer than the internodes, somewhat inflated, smooth or scabrous;</text>
      <biological_entity id="o4229" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="than the internodes" constraintid="o4230" is_modifier="false" name="length_or_size" src="d0_s3" value="often longer" value_original="often longer" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s3" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o4230" name="internode" name_original="internodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-3 mm, membranous, acute, lacerate, sometimes with lateral lobes;</text>
      <biological_entity id="o4231" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o4232" name="lobe" name_original="lobes" src="d0_s4" type="structure" />
      <relation from="o4231" id="r673" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o4232" />
    </statement>
    <statement id="d0_s5">
      <text>blades 1-4.5 cm long, 0.8-2 mm wide, flat to involute, scabrous to strigose, midveins and margins thickened, whitish.</text>
      <biological_entity id="o4233" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="4.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="involute" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s5" to="strigose" />
      </biological_entity>
      <biological_entity id="o4234" name="midvein" name_original="midveins" src="d0_s5" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o4235" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s5" value="thickened" value_original="thickened" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 3-11.5 cm long, 0.8-1.8 cm wide, contracted;</text>
      <biological_entity id="o4236" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="11.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s6" to="1.8" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>primary branches 1-3.7 cm, closely appressed, spikelets usually in subsessile-pedicellate pairs;</text>
      <biological_entity constraint="primary" id="o4237" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="3.7" to_unit="cm" />
        <character is_modifier="false" modifier="closely" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o4238" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o4239" name="pair" name_original="pairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="subsessile-pedicellate" value_original="subsessile-pedicellate" />
      </biological_entity>
      <relation from="o4238" id="r674" name="in" negation="false" src="d0_s7" to="o4239" />
    </statement>
    <statement id="d0_s8">
      <text>pedicels 0.2-8 mm, stout, closely appressed, scabrous;</text>
    </statement>
    <statement id="d0_s9">
      <text>disarticulation beneath the spikelet pairs.</text>
      <biological_entity id="o4240" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s8" value="stout" value_original="stout" />
        <character is_modifier="false" modifier="closely" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o4241" name="spikelet" name_original="spikelet" src="d0_s9" type="structure" />
      <relation from="o4240" id="r675" name="beneath" negation="false" src="d0_s9" to="o4241" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2.5-6 mm.</text>
      <biological_entity id="o4242" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes to 2/3 as long as the lemmas;</text>
      <biological_entity id="o4243" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="as-long-as lemmas" constraintid="o4244" from="0" name="quantity" src="d0_s11" to="2/3" />
      </biological_entity>
      <biological_entity id="o4244" name="lemma" name_original="lemmas" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 2-3.5 mm, subulate, 2-veined, minutely to deeply bifid, with 2 aristate teeth or awns to 1.8 mm;</text>
      <biological_entity constraint="lower" id="o4245" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subulate" value_original="subulate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-veined" value_original="2-veined" />
        <character is_modifier="false" modifier="minutely to deeply" name="architecture_or_shape" src="d0_s12" value="bifid" value_original="bifid" />
      </biological_entity>
      <biological_entity id="o4246" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s12" value="aristate" value_original="aristate" />
      </biological_entity>
      <biological_entity id="o4247" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
      </biological_entity>
      <relation from="o4245" id="r676" name="with" negation="false" src="d0_s12" to="o4246" />
      <relation from="o4245" id="r677" name="with" negation="false" src="d0_s12" to="o4247" />
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 2.4-4 mm, entire, acuminate to attenuate, 1-veined, awned, awns to 2 mm;</text>
      <biological_entity constraint="upper" id="o4248" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="entire" value_original="entire" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s13" to="attenuate" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o4249" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 3.5-6 mm, narrowly lanceolate, light greenish-brown to purplish, scabrous, appressed-pubescent on the margins and midveins, apices acuminate, often bifid, awned, awns usually 10-20 mm, stiff;</text>
      <biological_entity id="o4250" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="light greenish-brown" name="coloration" src="d0_s14" to="purplish" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
        <character constraint="on midveins" constraintid="o4252" is_modifier="false" name="pubescence" src="d0_s14" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity id="o4251" name="margin" name_original="margins" src="d0_s14" type="structure" />
      <biological_entity id="o4252" name="midvein" name_original="midveins" src="d0_s14" type="structure" />
      <biological_entity id="o4253" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s14" value="bifid" value_original="bifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o4254" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="20" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s14" value="stiff" value_original="stiff" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas 4-6 mm, narrowly lanceolate, intercostal region appressed-pubescent, apices acuminate;</text>
      <biological_entity id="o4255" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o4256" name="region" name_original="region" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity id="o4257" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 0.5-0.9 mm, purplish to yellowish.</text>
      <biological_entity id="o4258" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="purplish" name="coloration" src="d0_s16" to="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 2-2.8 mm, narrowly fusiform, brownish.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 20.</text>
      <biological_entity id="o4259" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="2.8" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s17" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4260" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia brevis grows on rocky slopes, gravelly flats, and rock outcrops, particularly those derived from calcareous parent materials, at elevations of 1700-2500 m, in gramma grasslands, pinyon-juniper woodlands, and pine-oak woodlands. Its range extends from the southwestern United States to central Mexico.</discussion>
  <discussion>Like Muhlenbergia depauperata, M. brevis shares several features with Lycurus, notably the paired spikelets with 2-veined and 2-awned lower glumes, 1-veined and awned upper glumes, acuminate, awned lemmas with shortly pubescent margins, and pubescent paleas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.;Colo.;N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>