<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">292</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Reeder" date="unknown" rank="tribe">ORCUTTIEAE</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="genus">ORCUTTIA</taxon_name>
    <taxon_name authority="Hitchc." date="unknown" rank="species">tenuis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe orcuttieae;genus orcuttia;species tenuis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Slender orcuttgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants sometimes weakly cespitose, but often with a single main culm branching 2-10 cm above the base, sparsely hairy.</text>
      <biological_entity id="o14903" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sometimes weakly" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" constraint="above base" constraintid="o14905" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s0" value="hairy" value_original="hairy" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="main" id="o14904" name="culm" name_original="culm" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="single" value_original="single" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o14905" name="base" name_original="base" src="d0_s0" type="structure" />
      <relation from="o14903" id="r2490" modifier="often" name="with" negation="false" src="d0_s0" to="o14904" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 5-15 (25) cm tall, 0.5-1 mm thick, often strictly erect, but sometimes decumbent when top heavy from profuse branching above.</text>
      <biological_entity id="o14906" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="height" src="d0_s1" unit="cm" value="25" value_original="25" />
        <character char_type="range_value" from="5" from_unit="cm" name="height" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s1" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="often strictly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="when top heavy from profuse branching above" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually without a collar line;</text>
      <biological_entity id="o14907" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="collar" id="o14908" name="line" name_original="line" src="d0_s2" type="structure" />
      <relation from="o14907" id="r2491" name="without" negation="false" src="d0_s2" to="o14908" />
    </statement>
    <statement id="d0_s3">
      <text>blades 1-3 cm long, 1.5-2 mm wide.</text>
      <biological_entity id="o14909" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikes 5-10 cm, more congested distally than basally;</text>
      <biological_entity id="o14910" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="distally" name="architecture_or_arrangement" src="d0_s4" value="congested" value_original="congested" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lower internodes 5-15 mm;</text>
      <biological_entity constraint="lower" id="o14911" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>upper internodes 2-7 mm.</text>
      <biological_entity constraint="upper" id="o14912" name="internode" name_original="internodes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets with 5-20 florets.</text>
      <biological_entity id="o14913" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o14914" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s7" to="20" />
      </biological_entity>
      <relation from="o14913" id="r2492" name="with" negation="false" src="d0_s7" to="o14914" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes subequal or the lower glumes a little shorter than the upper, 3-6 mm, with 3-5 teeth to 1 mm;</text>
      <biological_entity id="o14915" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="lower" id="o14916" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character constraint="than the upper glumes" constraintid="o14917" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o14917" name="glume" name_original="glumes" src="d0_s8" type="structure" />
      <biological_entity id="o14918" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="5" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o14916" id="r2493" name="with" negation="false" src="d0_s8" to="o14918" />
    </statement>
    <statement id="d0_s9">
      <text>lemmas 4.5-6 mm, acute or awn-tipped teeth about equal and 1/2 as long as the lemma, spreading or slightly recurved;</text>
      <biological_entity id="o14919" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o14920" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="awn-tipped" value_original="awn-tipped" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
        <character constraint="as-long-as lemma" constraintid="o14921" name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o14921" name="lemma" name_original="lemma" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>paleas slightly shorter than the lemmas;</text>
      <biological_entity id="o14922" name="palea" name_original="paleas" src="d0_s10" type="structure">
        <character constraint="than the lemmas" constraintid="o14923" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o14923" name="lemma" name_original="lemmas" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>anthers about 3 mm.</text>
      <biological_entity id="o14924" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Caryopses about 3 mm, narrowly oblong;</text>
      <biological_entity id="o14925" name="caryopsis" name_original="caryopses" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>embryos nearly as long as the caryopses.</text>
      <biological_entity id="o14927" name="caryopsis" name_original="caryopses" src="d0_s13" type="structure" />
      <relation from="o14926" id="r2494" name="as long as" negation="false" src="d0_s13" to="o14927" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 26.</text>
      <biological_entity id="o14926" name="embryo" name_original="embryos" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o14928" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Orcuttia tenuis grows at 25-100 m in Shasta and Tehama counties of the Central Valley of California, with outlying populations in Sacramento County and the lower montane regions of Lake, Shasta, and Siskiyou counties, California. It is listed as a threatened species by the U.S. Fish and Wildlife Service.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>