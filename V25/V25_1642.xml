<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles M. Allen;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">687</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Brongn." date="unknown" rank="genus">COELORACHIS</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus coelorachis</taxon_hierarchy>
  </taxon_identification>
  <number>26.24</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose or rhizomatous.</text>
      <biological_entity id="o15092" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 60-400 cm, erect.</text>
      <biological_entity id="o15093" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s2" to="400" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves not aromatic;</text>
    </statement>
    <statement id="d0_s4">
      <text>basal and cauline;</text>
      <biological_entity id="o15094" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s3" value="aromatic" value_original="aromatic" />
        <character is_modifier="false" name="position" src="d0_s4" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths open, glabrous, margins scarious;</text>
      <biological_entity id="o15095" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15096" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>auricles lacking;</text>
      <biological_entity id="o15097" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules membranous, ciliate;</text>
      <biological_entity id="o15098" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades flat to conduplicate, glabrous or sparsely pubescent, margins scarious, sometimes scabrous.</text>
      <biological_entity id="o15099" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s8" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15100" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences terminal and axillary, composed of a solitary, pedunculate rame;</text>
      <biological_entity id="o15102" name="rame" name_original="rame" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="pedunculate" value_original="pedunculate" />
      </biological_entity>
      <relation from="o15101" id="r2519" name="composed of a" negation="false" src="d0_s9" to="o15102" />
    </statement>
    <statement id="d0_s10">
      <text>rames stout;</text>
    </statement>
    <statement id="d0_s11">
      <text>disarticulation in the rames, below the sessile spikelets.</text>
      <biological_entity id="o15101" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s9" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s10" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o15103" name="rame" name_original="rames" src="d0_s11" type="structure" />
      <biological_entity id="o15104" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o15101" id="r2520" name="in" negation="false" src="d0_s11" to="o15103" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets dorsally compressed, in heterogamous sessile-pedicellate pairs.</text>
      <biological_entity id="o15105" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o15106" name="pair" name_original="pairs" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="heterogamous" value_original="heterogamous" />
        <character is_modifier="true" name="architecture" src="d0_s12" value="sessile-pedicellate" value_original="sessile-pedicellate" />
      </biological_entity>
      <relation from="o15105" id="r2521" name="in" negation="false" src="d0_s12" to="o15106" />
    </statement>
    <statement id="d0_s13">
      <text>Sessile spikelets embedded in the rame axes, ovate, with 2 florets, unawned;</text>
      <biological_entity id="o15107" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o15108" name="axis" name_original="axes" src="d0_s13" type="structure" />
      <biological_entity id="o15109" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <relation from="o15107" id="r2522" name="embedded in the rame" negation="false" src="d0_s13" to="o15108" />
      <relation from="o15107" id="r2523" name="with" negation="false" src="d0_s13" to="o15109" />
    </statement>
    <statement id="d0_s14">
      <text>lower glumes indurate, smooth, rugose, or pitted, 7-11-veined, not keeled;</text>
      <biological_entity constraint="lower" id="o15110" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="relief" src="d0_s14" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s14" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="relief" src="d0_s14" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="relief" src="d0_s14" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="relief" src="d0_s14" value="pitted" value_original="pitted" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="7-11-veined" value_original="7-11-veined" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes coriaceous, keeled, 1-veined;</text>
      <biological_entity constraint="upper" id="o15111" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="texture" src="d0_s15" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o15112" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s16" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper florets bisexual, unawned;</text>
      <biological_entity constraint="upper" id="o15113" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s17" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 3.</text>
      <biological_entity id="o15114" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Pedicels short, thick, appressed or partly fused to the side of the rame axes.</text>
      <biological_entity id="o15115" name="pedicel" name_original="pedicels" src="d0_s19" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s19" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s19" value="thick" value_original="thick" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s19" value="appressed" value_original="appressed" />
        <character constraint="to side" constraintid="o15116" is_modifier="false" modifier="partly" name="fusion" src="d0_s19" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o15116" name="side" name_original="side" src="d0_s19" type="structure" />
      <biological_entity id="o15117" name="axis" name_original="axes" src="d0_s19" type="structure" />
      <relation from="o15116" id="r2524" name="part_of" negation="false" src="d0_s19" to="o15117" />
    </statement>
    <statement id="d0_s20">
      <text>Pedicellate spikelets 1-3 mm, usually reduced.</text>
      <biological_entity id="o15118" name="spikelet" name_original="spikelets" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s20" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s20" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Caryopses ellipsoid to broadly ellipsoid, yellow.</text>
    </statement>
    <statement id="d0_s22">
      <text>x = 9.</text>
      <biological_entity id="o15119" name="caryopsis" name_original="caryopses" src="d0_s21" type="structure">
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s21" to="broadly ellipsoid" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity constraint="x" id="o15120" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Coelorachis is a tropical genus of approximately 20 species; four are native to the southeastern United States. Most species tend to favor damp soils. Veldkamp et al. (1986) recommended combining Coelorachis and Hackelochloa with some other small genera in Mnesithea Kunth, but these two seem to be sufficiently distinct to be maintained until more data are available.</discussion>
  <references>
    <reference>Veldkamp, J.F., R. de Koning, and M.S.M. Sosef. 1986. Generic delimitation of Rottboellia and related genera (Gramineae). Blumea 31:281-307.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Kans.;Okla.;Miss.;Tex.;La.;Mo.;Del.;Ala.;N.C.;S.C.;Va.;Ark.;Ga.;N.J.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Culms and sheaths terete; lower glumes of the sessile spikelets with circular pits on the sides, the central region initially smooth, usually developing rectangular pits at maturity, occasionally remaining smooth</description>
      <determination>1 Coelorachis cylindrica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Culms and sheaths compressed-keeled; lower glumes of the sessile spikelets transversely rugose, rectangular-pitted, or smooth.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lower glumes of the sessile spikelets rectangular-pitted</description>
      <determination>2 Coelorachis tessellata</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lower glumes of the sessile spikelets transversely rugose or smooth.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Lower glumes of the sessile spikelets distinctly transversely rugose; rachises distinctly indented below the sessile spikelets</description>
      <determination>3 Coelorachis rugosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Lower glumes of the sessile spikelets smooth to slightly transversely rugose; rachises not, or only slightly, indented below the sessile spikelets</description>
      <determination>4 Coelorachis tuberculosa</determination>
    </key_statement>
  </key>
</bio:treatment>