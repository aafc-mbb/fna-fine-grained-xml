<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">58</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">LEPTOCHLOA</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="species">scabra</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus leptochloa;species scabra</taxon_hierarchy>
  </taxon_identification>
  <number>7</number>
  <other_name type="common_name">Rough sprangletop</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o14948" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (12) 20-125 cm, mostly erect, often strongly compressed, branching;</text>
      <biological_entity id="o14949" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="12" value_original="12" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="125" to_unit="cm" />
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often strongly" name="shape" src="d0_s1" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes hollow.</text>
      <biological_entity id="o14950" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous, smooth to scabrous;</text>
      <biological_entity id="o14951" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="smooth to scabrous" value_original="smooth to scabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="smooth to scabrous" value_original="smooth to scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 1.5-2 mm, membranous, truncate, erose;</text>
      <biological_entity id="o14952" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s4" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 25-35 (50) cm long, 8-16 mm wide, scabrous on both surfaces.</text>
      <biological_entity id="o14953" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="50" value_original="50" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s5" to="35" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s5" to="16" to_unit="mm" />
        <character constraint="on surfaces" constraintid="o14954" is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o14954" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles 8-35 cm, with 50-150 racemose branches;</text>
      <biological_entity id="o14955" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s6" to="35" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14956" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="50" is_modifier="true" name="quantity" src="d0_s6" to="150" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="racemose" value_original="racemose" />
      </biological_entity>
      <relation from="o14955" id="r2497" name="with" negation="false" src="d0_s6" to="o14956" />
    </statement>
    <statement id="d0_s7">
      <text>branches (2) 5-12 cm, lax, sometimes arcuate, lower branches often remaining enclosed in the upper leaf-sheaths.</text>
      <biological_entity id="o14957" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s7" to="12" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="sometimes" name="course_or_shape" src="d0_s7" value="arcuate" value_original="arcuate" />
      </biological_entity>
      <biological_entity constraint="lower" id="o14958" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <biological_entity constraint="upper" id="o14959" name="sheath" name_original="leaf-sheaths" src="d0_s7" type="structure" />
      <relation from="o14958" id="r2498" name="enclosed in the" negation="false" src="d0_s7" to="o14959" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 3-4.5 mm, usually tightly imbricate, green but straw-colored when dry, with 2-6 florets.</text>
      <biological_entity id="o14960" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually tightly" name="arrangement" src="d0_s8" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="when dry" name="coloration" src="d0_s8" value="green but straw-colored" value_original="green but straw-colored" />
      </biological_entity>
      <biological_entity id="o14961" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <relation from="o14960" id="r2499" name="with" negation="false" src="d0_s8" to="o14961" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes sometimes mucronate;</text>
      <biological_entity id="o14962" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s9" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 0.8-1.6 mm, narrowly triangular to lanceolate;</text>
      <biological_entity constraint="lower" id="o14963" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="narrowly triangular" name="shape" src="d0_s10" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 1.1-2.1 mm, ovate;</text>
      <biological_entity constraint="upper" id="o14964" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s11" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>rachilla segments not visible between the florets;</text>
      <biological_entity constraint="rachilla" id="o14965" name="segment" name_original="segments" src="d0_s12" type="structure">
        <character constraint="between florets" constraintid="o14966" is_modifier="false" modifier="not" name="prominence" src="d0_s12" value="visible" value_original="visible" />
      </biological_entity>
      <biological_entity id="o14966" name="floret" name_original="florets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas 2.1-2.4 mm, lanceolate to narrowly ovate, membranous, sparsely sericeous along the lateral-veins, apices acute, unawned;</text>
      <biological_entity id="o14967" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s13" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s13" to="narrowly ovate" />
        <character is_modifier="false" name="texture" src="d0_s13" value="membranous" value_original="membranous" />
        <character constraint="along lateral-veins" constraintid="o14968" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="sericeous" value_original="sericeous" />
      </biological_entity>
      <biological_entity id="o14968" name="lateral-vein" name_original="lateral-veins" src="d0_s13" type="structure" />
      <biological_entity id="o14969" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 0.2-0.4 mm.</text>
      <biological_entity id="o14970" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s14" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 0.8-1.3 mm long, 0.3-0.5 mm wide, elliptic to obovate, depressed obovate in cross-section.</text>
      <biological_entity id="o14972" name="cross-section" name_original="cross-section" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 60.</text>
      <biological_entity id="o14971" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s15" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s15" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s15" to="obovate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="depressed" value_original="depressed" />
        <character constraint="in cross-section" constraintid="o14972" is_modifier="false" name="shape" src="d0_s15" value="obovate" value_original="obovate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14973" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Leptochloa scabra is a neotropical species that extends into Louisiana and southwestern Alabama. It is often confused with L. panicoides, but it has more, flexuous to arcuate panicle branches, shorter spikelets, and less prominent lemma veins. It may also be confused with L. fusca subsp. uninervia, from which it differs in its acute lemmas, and with L. virgata, from which it differs in its hollow, flattened culms and the complete lack of lemma awns.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Ala.;La.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>