<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">350</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Soderstr." date="unknown" rank="subfamily">CENTOTHECOIDEAE</taxon_name>
    <taxon_name authority="C.E. Hubb." date="unknown" rank="tribe">THYSANOLAENEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="genus">THYSANOLAENA</taxon_name>
    <taxon_name authority="(Roxb. ex Hornem.) Honda" date="unknown" rank="species">latifolia</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily centothecoideae;tribe thysanolaeneae;genus thysanolaena;species latifolia</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Asian broomgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Sheaths mostly glabrous, margins often ciliate distally;</text>
      <biological_entity id="o15448" name="sheath" name_original="sheaths" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15449" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often; distally" name="architecture_or_pubescence_or_shape" src="d0_s0" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>collars pubescent;</text>
      <biological_entity id="o15450" name="collar" name_original="collars" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 20-65 cm long, 2-7 (10) cm wide, flat, usually glabrous or mostly so on both surfaces, occasionally sparsely pubescent abaxially, adaxial surfaces often pubescent immediately distal to the collar and ligule.</text>
      <biological_entity id="o15451" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s2" to="65" to_unit="cm" />
        <character name="width" src="d0_s2" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="7" to_unit="cm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s2" value="mostly" value_original="mostly" />
        <character is_modifier="false" modifier="occasionally sparsely; abaxially" name="pubescence" notes="" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15452" name="surface" name_original="surfaces" src="d0_s2" type="structure" />
      <biological_entity constraint="adaxial" id="o15453" name="surface" name_original="surfaces" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
        <character constraint="to ligule" constraintid="o15455" is_modifier="false" modifier="immediately" name="position_or_shape" src="d0_s2" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity id="o15454" name="collar" name_original="collar" src="d0_s2" type="structure" />
      <biological_entity id="o15455" name="ligule" name_original="ligule" src="d0_s2" type="structure" />
      <relation from="o15451" id="r2595" name="on" negation="false" src="d0_s2" to="o15452" />
    </statement>
    <statement id="d0_s3">
      <text>Panicles 17-140 cm;</text>
      <biological_entity id="o15456" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character char_type="range_value" from="17" from_unit="cm" name="some_measurement" src="d0_s3" to="140" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches to 75 cm, often drooping, pulvini pubescent.</text>
      <biological_entity id="o15457" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="75" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s4" value="drooping" value_original="drooping" />
      </biological_entity>
      <biological_entity id="o15458" name="pulvinus" name_original="pulvini" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 1.2-2.8 mm, frequently abortive.</text>
      <biological_entity id="o15459" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s5" to="2.8" to_unit="mm" />
        <character is_modifier="false" modifier="frequently" name="development" src="d0_s5" value="abortive" value_original="abortive" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Lower glumes 0.5-0.9 mm, broadly ovate;</text>
      <biological_entity constraint="lower" id="o15460" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="0.9" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>upper glumes 0.5-1 mm, broadly elliptic;</text>
      <biological_entity constraint="upper" id="o15461" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s7" value="elliptic" value_original="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lower lemmas 1.3-2.4 mm, elliptic-lanceolate, acute to acuminate;</text>
      <biological_entity constraint="lower" id="o15462" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s8" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>upper lemmas 1.2-2.8 mm, lanceolate, acute to acuminate;</text>
      <biological_entity constraint="upper" id="o15463" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 0.6-1 mm, yellow;</text>
      <biological_entity id="o15464" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stigmas purple.</text>
      <biological_entity id="o15465" name="stigma" name_original="stigmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Caryopses 0.4-0.5 mm. 2n = 24.</text>
      <biological_entity id="o15466" name="caryopsis" name_original="caryopses" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15467" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Thysanolaena latifolia is an important pasture species in Asia. It is grown in the United States as an ornamental plant, recommended for frost-free areas with full sunlight or light shade. It is not known to be established in the Flora region.</discussion>
  <discussion>The species has been known as Thysanolaena maxima (Roxb.) Kuntze, but Baaijens and Veldkamp (1991) demonstrated that T. latifolia has priority at the species level.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>