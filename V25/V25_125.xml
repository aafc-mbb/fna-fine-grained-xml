<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">79</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Wolf" date="unknown" rank="genus">ERAGROSTIS</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">lutescens</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eragrostis;species lutescens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>15</number>
  <other_name type="common_name">Sixweeks lovegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted, without innovations.</text>
      <biological_entity id="o8061" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8062" name="innovation" name_original="innovations" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms (2) 6-25 cm, usually erect, sometimes decumbent, glabrous, with elliptical, yellowish, glandular pits below the nodes.</text>
      <biological_entity id="o8063" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o8064" name="pit" name_original="pits" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s2" value="elliptical" value_original="elliptical" />
        <character is_modifier="true" name="coloration" src="d0_s2" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <biological_entity id="o8065" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <relation from="o8063" id="r1301" name="with" negation="false" src="d0_s2" to="o8064" />
      <relation from="o8064" id="r1302" name="below" negation="false" src="d0_s2" to="o8065" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths with elliptical glandular pits, sparsely hairy at the throat, hairs to 2 mm;</text>
      <biological_entity id="o8066" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="at throat" constraintid="o8068" is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o8067" name="pit" name_original="pits" src="d0_s3" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s3" value="elliptical" value_original="elliptical" />
      </biological_entity>
      <biological_entity id="o8068" name="throat" name_original="throat" src="d0_s3" type="structure" />
      <biological_entity id="o8069" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o8066" id="r1303" name="with" negation="false" src="d0_s3" to="o8067" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.2-0.5 mm, ciliate;</text>
      <biological_entity id="o8070" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 2-12 cm long, 1-3 mm wide, flat to involute, abaxial surfaces scabridulous, bases with glandular pits.</text>
      <biological_entity id="o8071" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8072" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o8073" name="base" name_original="bases" src="d0_s5" type="structure" />
      <biological_entity constraint="glandular" id="o8074" name="pit" name_original="pits" src="d0_s5" type="structure" />
      <relation from="o8073" id="r1304" name="with" negation="false" src="d0_s5" to="o8074" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles terminal, 4-10 (15) cm long, 0.5-2 cm wide, narrowly elliptic, contracted, dense;</text>
      <biological_entity id="o8075" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character name="length" src="d0_s6" unit="cm" value="15" value_original="15" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s6" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s6" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="density" src="d0_s6" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>primary branches alternate, usually appressed, occasionally diverging to 30° from the rachises, rachises and branches with glandular pits;</text>
      <biological_entity constraint="primary" id="o8076" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="usually" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character constraint="from rachises" constraintid="o8077" is_modifier="false" modifier="30°" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o8077" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
      <biological_entity id="o8078" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
      <biological_entity id="o8079" name="branch" name_original="branches" src="d0_s7" type="structure" />
      <biological_entity constraint="glandular" id="o8080" name="pit" name_original="pits" src="d0_s7" type="structure" />
      <relation from="o8078" id="r1305" name="with" negation="false" src="d0_s7" to="o8080" />
      <relation from="o8079" id="r1306" name="with" negation="false" src="d0_s7" to="o8080" />
    </statement>
    <statement id="d0_s8">
      <text>pulvini glabrous;</text>
      <biological_entity id="o8081" name="pulvinus" name_original="pulvini" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 1.4-10 mm, appressed or divergent.</text>
      <biological_entity id="o8082" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3.6-7.5 mm long, 1.2-2 mm wide, narrowly ovate, light yellowish, occasionally mottled with red¬dish-purple, with 6-11 (14) florets;</text>
      <biological_entity id="o8084" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="14" value_original="14" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s10" to="11" />
      </biological_entity>
      <relation from="o8083" id="r1307" name="with" negation="false" src="d0_s10" to="o8084" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation acropetal, paleas persistent.</text>
      <biological_entity id="o8083" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.6" from_unit="mm" name="length" src="d0_s10" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="light yellowish" value_original="light yellowish" />
        <character is_modifier="false" modifier="occasionally" name="coloration" src="d0_s10" value="mottled with red" value_original="mottled with red" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="dish-purple" value_original="dish-purple" />
        <character is_modifier="false" name="development" src="d0_s11" value="acropetal" value_original="acropetal" />
      </biological_entity>
      <biological_entity id="o8085" name="palea" name_original="paleas" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes subequal, ovate to lanceolate, hyaline;</text>
      <biological_entity id="o8086" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s12" to="lanceolate" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes (0.7) 0.9-1.4 mm;</text>
      <biological_entity constraint="lower" id="o8087" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="0.7" value_original="0.7" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s13" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 1.2-1.8 mm;</text>
      <biological_entity constraint="upper" id="o8088" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s14" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 1.5-2.2 mm, ovate, subhyaline, stramineous, veins greenish and conspicu¬ous, apices acute;</text>
      <biological_entity id="o8089" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s15" value="subhyaline" value_original="subhyaline" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity id="o8090" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o8091" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 1.2-2 mm, hyaline, keels scabridulous, apices obtuse;</text>
      <biological_entity id="o8092" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o8093" name="keel" name_original="keels" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief" src="d0_s16" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o8094" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, 0.2-0.3 mm, purplish.</text>
      <biological_entity id="o8095" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s17" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 0.5-0.8 mm, pyriform except slightly flattened adaxially, smooth, light-brown.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = unknown.</text>
      <biological_entity id="o8096" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s18" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="pyriform" value_original="pyriform" />
        <character constraint="except" is_modifier="false" modifier="slightly; adaxially" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8097" name="chromosome" name_original="" src="d0_s19" type="structure" />
    </statement>
  </description>
  <discussion>Eragrostis lutescens grows on the sandy banks of streams and lakes and in moist alkaline flats of the western United States at 300-2000 m. It has not been reported from Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Colo.;N.Mex.;Wash.;Calif.;Oreg.;Ariz.;Idaho;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>