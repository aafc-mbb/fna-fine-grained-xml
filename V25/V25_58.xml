<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">39</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Roem. &amp; Schult." date="unknown" rank="genus">TRIDENS</taxon_name>
    <taxon_name authority="(S. Watson) Nash" date="unknown" rank="species">texanus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus tridens;species texanus</taxon_hierarchy>
  </taxon_identification>
  <number>9</number>
  <other_name type="common_name">Texas tridens</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, with knotty, shortly rhizomatous bases.</text>
      <biological_entity id="o27242" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o27243" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="knotty" value_original="knotty" />
        <character is_modifier="true" modifier="shortly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <relation from="o27242" id="r4632" name="with" negation="false" src="d0_s0" to="o27243" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-75 cm, slender, strictly erect;</text>
      <biological_entity id="o27244" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="75" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="strictly" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes glabrous;</text>
      <biological_entity id="o27245" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes often pilose.</text>
      <biological_entity id="o27246" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths mostly glabrous or pilose throughout, collar and distal portion of the margins densely pilose;</text>
      <biological_entity id="o27247" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o27248" name="collar" name_original="collar" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="distal" id="o27249" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o27250" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <relation from="o27248" id="r4633" name="part_of" negation="false" src="d0_s4" to="o27250" />
      <relation from="o27249" id="r4634" name="part_of" negation="false" src="d0_s4" to="o27250" />
    </statement>
    <statement id="d0_s5">
      <text>ligules to 0.5 mm, membranous, ciliate;</text>
      <biological_entity id="o27251" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 1-3 (5) mm wide, flat or becoming inrolled, hispid, with long hairs on the adaxial surface just above the ligule, apices attenuate.</text>
      <biological_entity id="o27252" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="width" src="d0_s6" unit="mm" value="5" value_original="5" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="becoming" name="shape_or_vernation" src="d0_s6" value="inrolled" value_original="inrolled" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o27253" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s6" value="long" value_original="long" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27254" name="surface" name_original="surface" src="d0_s6" type="structure" />
      <biological_entity id="o27255" name="ligule" name_original="ligule" src="d0_s6" type="structure" />
      <biological_entity id="o27256" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <relation from="o27252" id="r4635" name="with" negation="false" src="d0_s6" to="o27253" />
      <relation from="o27253" id="r4636" name="on" negation="false" src="d0_s6" to="o27254" />
      <relation from="o27254" id="r4637" name="above" negation="false" src="d0_s6" to="o27255" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles 5-16 cm long, 2-9 cm wide, open or loosely contracted;</text>
      <biological_entity id="o27257" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="16" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="9" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" modifier="loosely" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches (2) 4-7 cm, slender, lax, strongly divergent to drooping, basal portion naked, spikelets confined to the distal portion;</text>
      <biological_entity id="o27258" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s8" to="7" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="drooping" value_original="drooping" />
      </biological_entity>
      <biological_entity constraint="basal" id="o27259" name="portion" name_original="portion" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o27260" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity constraint="distal" id="o27261" name="portion" name_original="portion" src="d0_s8" type="structure" />
      <relation from="o27260" id="r4638" name="confined to the" negation="false" src="d0_s8" to="o27261" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels (2) 3-6 mm.</text>
      <biological_entity id="o27262" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 6-13 mm, with 6-12 florets.</text>
      <biological_entity id="o27263" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27264" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s10" to="12" />
      </biological_entity>
      <relation from="o27263" id="r4639" name="with" negation="false" src="d0_s10" to="o27264" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes glabrous, 1-veined;</text>
      <biological_entity id="o27265" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 3 mm;</text>
      <biological_entity constraint="lower" id="o27266" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 3.5-4 mm, bright green;</text>
      <biological_entity constraint="upper" id="o27267" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="4" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="bright green" value_original="bright green" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 3-4.5 mm, usually purple or rosy-purple at maturity, beins pilose to midlength, lateral-veins often excurrent as short points;</text>
      <biological_entity id="o27268" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="coloration_or_density" src="d0_s14" value="purple" value_original="purple" />
        <character constraint="at maturity" constraintid="o27269" is_modifier="false" name="coloration_or_density" src="d0_s14" value="rosy-purple" value_original="rosy-purple" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s14" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="position" src="d0_s14" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity id="o27269" name="maturity" name_original="maturity" src="d0_s14" type="structure" />
      <biological_entity id="o27270" name="lateral-vein" name_original="lateral-veins" src="d0_s14" type="structure">
        <character constraint="as points" constraintid="o27271" is_modifier="false" modifier="often" name="architecture" src="d0_s14" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o27271" name="point" name_original="points" src="d0_s14" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s14" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas 3-3.5 mm, glabrous, abruptly broadened and bowed-out below;</text>
      <biological_entity id="o27272" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="abruptly; below" name="width" src="d0_s15" value="broadened" value_original="broadened" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 1-1.5 mm.</text>
      <biological_entity id="o27273" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 1.5-2 mm. 2n = 40.</text>
      <biological_entity id="o27274" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27275" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tridens texanus grows in clayey and sandy loam soils, often in the protection of shrubs and along fenced road right of ways. Its range extends from southern Texas into northern Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>