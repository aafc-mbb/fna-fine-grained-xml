<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">MICROCHLOA</taxon_name>
    <taxon_name authority="Desv." date="unknown" rank="species">kunthii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus microchloa;species kunthii</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Kunth's smallgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>forming small, dense tufts.</text>
      <biological_entity id="o21007" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21008" name="tuft" name_original="tufts" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
        <character is_modifier="true" name="density" src="d0_s1" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o21007" id="r3580" name="forming" negation="false" src="d0_s1" to="o21008" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-30 cm.</text>
      <biological_entity id="o21009" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths generally shorter than the internodes;</text>
      <biological_entity id="o21010" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="than the internodes" constraintid="o21011" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="generally shorter" value_original="generally shorter" />
      </biological_entity>
      <biological_entity id="o21011" name="internode" name_original="internodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-1.5 mm, ciliate;</text>
      <biological_entity id="o21012" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-1.5 mm wide, flat or folded, with thick, scabrous, white margins;</text>
      <biological_entity id="o21013" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s5" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity id="o21014" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="white" value_original="white" />
      </biological_entity>
      <relation from="o21013" id="r3581" name="with" negation="false" src="d0_s5" to="o21014" />
    </statement>
    <statement id="d0_s6">
      <text>cauline blades 1-2.5 cm;</text>
      <biological_entity constraint="cauline" id="o21015" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>innovation blades to 6 cm.</text>
      <biological_entity constraint="innovation" id="o21016" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicle brances 6-15 cml rachises ciliate.</text>
      <biological_entity id="o21017" name="panicle" name_original="panicle" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s8" to="15" />
      </biological_entity>
      <biological_entity id="o21018" name="rachis" name_original="rachises" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 2.5-3.5 mm;</text>
      <biological_entity id="o21019" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lemmas 2-2.5 mm, keel spilose, margins densely pilose, hairs 0.2-1 mm. 2n = 24, 40.</text>
      <biological_entity id="o21020" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21021" name="keel" name_original="keel" src="d0_s10" type="structure" />
      <biological_entity id="o21022" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o21023" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21024" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
        <character name="quantity" src="d0_s10" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Microchloa kunthii grows on granitic outcrops on rocky slopes. Its range extends southward from Carr Canyon, Huachuca Mountains, Arizona and the Big Bend region of Texas through Mexico to Guatemala.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>