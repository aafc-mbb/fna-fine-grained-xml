<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">672</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="genus">SCHIZACHYRIUM</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="species">tenerum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus schizachyrium;species tenerum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Andropogon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tener</taxon_name>
    <taxon_hierarchy>genus andropogon;species tener</taxon_hierarchy>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Slender bluestem</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose.</text>
      <biological_entity id="o10991" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 60-100 cm, sometimes reclining or decumbent, glabrous.</text>
      <biological_entity id="o10992" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="reclining" value_original="reclining" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Collars not elongate, about as wide as the blade;</text>
      <biological_entity id="o10993" name="collar" name_original="collars" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o10994" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <relation from="o10993" id="r1801" name="as wide as" negation="false" src="d0_s2" to="o10994" />
    </statement>
    <statement id="d0_s3">
      <text>ligules to 0.5 mm, ciliolate;</text>
      <biological_entity id="o10995" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 5-15 cm long, 0.5-2 mm wide, involute or flat, glabrous or sparsely hairy basally, with a wide central zone of bulliform cells evident on the adaxial surfaces as a longitudinal stripe of white, spongy tissue.</text>
      <biological_entity constraint="cell" id="o10997" name="zone" name_original="zone" src="d0_s4" type="structure" constraint_original="cell central; cell">
        <character is_modifier="true" name="width" src="d0_s4" value="wide" value_original="wide" />
        <character constraint="on adaxial surfaces" constraintid="o10999" is_modifier="false" name="prominence" src="d0_s4" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o10998" name="cell" name_original="cells" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="bulliform" value_original="bulliform" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10999" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <biological_entity id="o11000" name="stripe" name_original="stripe" src="d0_s4" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s4" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o11001" name="tissue" name_original="tissue" src="d0_s4" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="true" name="texture" src="d0_s4" value="spongy" value_original="spongy" />
      </biological_entity>
      <relation from="o10996" id="r1802" name="with" negation="false" src="d0_s4" to="o10997" />
      <relation from="o10997" id="r1803" name="part_of" negation="false" src="d0_s4" to="o10998" />
      <relation from="o10999" id="r1804" name="as" negation="false" src="d0_s4" to="o11000" />
      <relation from="o11000" id="r1805" name="part_of" negation="false" src="d0_s4" to="o11001" />
    </statement>
    <statement id="d0_s5">
      <text>Rames 2-6 cm, eventually long-exserted;</text>
      <biological_entity id="o10996" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; basally" name="pubescence" src="d0_s4" value="hairy" value_original="hairy" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
        <character is_modifier="false" modifier="eventually" name="position" src="d0_s5" value="long-exserted" value_original="long-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>internodes 2-4 mm, straight, glabrous.</text>
      <biological_entity id="o11002" name="internode" name_original="internodes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sessile spikelets 3.5-4.5 mm;</text>
      <biological_entity id="o11003" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>calluses 0.5-1 mm, hairs to 1.2 mm;</text>
      <biological_entity id="o11004" name="callus" name_original="calluses" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11005" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower glumes glabrous;</text>
      <biological_entity constraint="lower" id="o11006" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper lemmas acute, entire;</text>
      <biological_entity constraint="upper" id="o11007" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>awns 6-10 mm.</text>
      <biological_entity id="o11008" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pedicels 3-5 mm, glabrous.</text>
      <biological_entity id="o11009" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pedicellate spikelets usually as long as or slightly longer than the sessile spikelets, sterile, unawned.</text>
      <biological_entity id="o11012" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o11011" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
        <character is_modifier="true" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o11010" id="r1806" name="as long as" negation="false" src="d0_s13" to="o11012" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 60.</text>
      <biological_entity id="o11010" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s13" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11013" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Schizachyrium tenerum is an uncommon species in the southeastern United States, where it grows on sandy soils in pine forest openings and coastal prairies. Its range extends through Central America into South America.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Okla.;Ga.;Tex.;La.;Ala.;Miss.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>