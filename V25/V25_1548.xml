<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kelly W. Allred;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">639</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Kuntze" date="unknown" rank="genus">BOTHRIOCHLOA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus bothriochloa</taxon_hierarchy>
  </taxon_identification>
  <number>26.12</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose or stoloniferous.</text>
      <biological_entity id="o13539" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-250 cm, with pithy internodes.</text>
      <biological_entity id="o13540" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="250" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13541" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="texture" src="d0_s2" value="pithy" value_original="pithy" />
      </biological_entity>
      <relation from="o13540" id="r2219" name="with" negation="false" src="d0_s2" to="o13541" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal or cauline, not aromatic;</text>
      <biological_entity id="o13542" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="odor" src="d0_s3" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths open;</text>
      <biological_entity id="o13543" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles absent;</text>
      <biological_entity id="o13544" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules membranous, sometimes also ciliate;</text>
      <biological_entity id="o13545" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades usually flat, convolute in the bud.</text>
      <biological_entity id="o13546" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character constraint="in bud" constraintid="o13547" is_modifier="false" name="arrangement_or_shape" src="d0_s7" value="convolute" value_original="convolute" />
      </biological_entity>
      <biological_entity id="o13547" name="bud" name_original="bud" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, panicles of subdigitate to racemosely arranged branches, each branch with (1) 2-many rames, branches not subtended by modified leaves;</text>
      <biological_entity id="o13548" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o13549" name="panicle" name_original="panicles" src="d0_s8" type="structure" />
      <biological_entity id="o13550" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="subdigitate" value_original="subdigitate" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o13551" name="branch" name_original="branch" src="d0_s8" type="structure" />
      <biological_entity id="o13552" name="rame" name_original="rames" src="d0_s8" type="structure">
        <character char_type="range_value" from="(1)2" is_modifier="true" name="quantity" src="d0_s8" to="many" />
      </biological_entity>
      <biological_entity id="o13554" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="true" name="development" src="d0_s8" value="modified" value_original="modified" />
      </biological_entity>
      <relation from="o13549" id="r2220" name="part_of" negation="false" src="d0_s8" to="o13550" />
      <relation from="o13551" id="r2221" name="with" negation="false" src="d0_s8" to="o13552" />
      <relation from="o13553" id="r2222" name="subtended by" negation="false" src="d0_s8" to="o13554" />
    </statement>
    <statement id="d0_s9">
      <text>rames with spikelets in heterogamous sessile-pedicellate pairs, internodes with a translucent, longitudinal groove, often villous on the margins;</text>
      <biological_entity id="o13553" name="branch" name_original="branches" src="d0_s8" type="structure" />
      <biological_entity id="o13555" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o13556" name="pair" name_original="pairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="heterogamous" value_original="heterogamous" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="sessile-pedicellate" value_original="sessile-pedicellate" />
      </biological_entity>
      <biological_entity id="o13558" name="groove" name_original="groove" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s9" value="translucent" value_original="translucent" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s9" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o13559" name="margin" name_original="margins" src="d0_s9" type="structure" />
      <relation from="o13553" id="r2223" name="rames with" negation="false" src="d0_s9" to="o13555" />
      <relation from="o13557" id="r2224" name="with" negation="false" src="d0_s9" to="o13558" />
    </statement>
    <statement id="d0_s10">
      <text>disarticulation in the rames, beneath the sessile spikelets.</text>
      <biological_entity id="o13557" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character constraint="on margins" constraintid="o13559" is_modifier="false" modifier="often" name="pubescence" notes="" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o13560" name="rame" name_original="rames" src="d0_s10" type="structure" />
      <biological_entity id="o13561" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o13557" id="r2225" name="in" negation="false" src="d0_s10" to="o13560" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets dorsally compressed;</text>
      <biological_entity id="o13562" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sessile spikelets with 2 florets;</text>
      <biological_entity id="o13563" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o13564" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="2" value_original="2" />
      </biological_entity>
      <relation from="o13563" id="r2226" name="with" negation="false" src="d0_s12" to="o13564" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes rounded, several-veined, sometimes with a dorsal pit, margins clasping the upper glume;</text>
      <biological_entity constraint="lower" id="o13565" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="several-veined" value_original="several-veined" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o13566" name="pit" name_original="pit" src="d0_s13" type="structure" />
      <biological_entity id="o13567" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <biological_entity constraint="upper" id="o13568" name="glume" name_original="glume" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture_or_fixation" src="d0_s13" value="clasping" value_original="clasping" />
      </biological_entity>
      <relation from="o13565" id="r2227" modifier="sometimes" name="with" negation="false" src="d0_s13" to="o13566" />
    </statement>
    <statement id="d0_s14">
      <text>upper glumes somewhat keeled, 3-veined;</text>
      <biological_entity constraint="upper" id="o13569" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower florets hyaline scales, unawned;</text>
      <biological_entity constraint="lower" id="o13570" name="floret" name_original="florets" src="d0_s15" type="structure" />
      <biological_entity id="o13571" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper florets bisexual;</text>
      <biological_entity constraint="upper" id="o13572" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s16" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper lemmas with a midvein that usually extends into a twisted, geniculate awn, occasionally unawned;</text>
      <biological_entity constraint="upper" id="o13573" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <biological_entity id="o13574" name="midvein" name_original="midvein" src="d0_s17" type="structure" />
      <biological_entity id="o13575" name="awn" name_original="awn" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="twisted" value_original="twisted" />
        <character is_modifier="true" name="shape" src="d0_s17" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <relation from="o13573" id="r2228" name="with" negation="false" src="d0_s17" to="o13574" />
      <relation from="o13574" id="r2229" modifier="occasionally" name="extends into" negation="false" src="d0_s17" to="o13575" />
    </statement>
    <statement id="d0_s18">
      <text>anthers 3.</text>
      <biological_entity id="o13576" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Pedicels similar to the internodes.</text>
      <biological_entity id="o13577" name="pedicel" name_original="pedicels" src="d0_s19" type="structure" />
      <biological_entity id="o13578" name="internode" name_original="internodes" src="d0_s19" type="structure" />
      <relation from="o13577" id="r2230" name="to" negation="false" src="d0_s19" to="o13578" />
    </statement>
    <statement id="d0_s20">
      <text>Pedicellate spikelets reduced or well-developed, sterile or staminate, unawned.</text>
      <biological_entity id="o13579" name="spikelet" name_original="spikelets" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="size" src="d0_s20" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="development" src="d0_s20" value="well-developed" value_original="well-developed" />
        <character is_modifier="false" name="reproduction" src="d0_s20" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Caryopses lanceolate to oblong, somewhat flattened;</text>
      <biological_entity id="o13580" name="caryopsis" name_original="caryopses" src="d0_s21" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s21" to="oblong" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s21" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>hila punctate, basal;</text>
      <biological_entity id="o13581" name="hilum" name_original="hila" src="d0_s22" type="structure">
        <character is_modifier="false" name="coloration_or_relief" src="d0_s22" value="punctate" value_original="punctate" />
        <character is_modifier="false" name="position" src="d0_s22" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>embryos about Yi as long as the caryopses.</text>
      <biological_entity id="o13583" name="caryopsis" name_original="caryopses" src="d0_s23" type="structure" />
      <relation from="o13582" id="r2231" name="as long as" negation="false" src="d0_s23" to="o13583" />
    </statement>
    <statement id="d0_s24">
      <text>x = 10.</text>
      <biological_entity id="o13582" name="embryo" name_original="embryos" src="d0_s23" type="structure" />
      <biological_entity constraint="x" id="o13584" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bothriochloa is a genus of about 35 species that grow in tropical to warm-temperate regions. Nine are native to the Flora region; three Eastern Hemisphere species have been introduced into the southern United States for forage and range rehabilitation. Most species provide fair forage in summer and fall. Polyploidy has been an important mechanism of speciation in the genus.</discussion>
  <references>
    <reference>Allred, K.W. 1983. Systematics of the Bothriochloa saccharoides complex (Poaceae: Andropogoneae). Syst. Bot. 8:168-184</reference>
    <reference>de Wet, J.M.J. 1968. Biosystematics of the Bothriochloa barbinodis complex (Gramineae). Amer. J. Bot. 55:1246-1250</reference>
    <reference>de Wet, J.M.J, and J.R. Harlan. 1970. Bothriochloa intermedia-a taxonomic dilemma. Taxon 19:339-340</reference>
    <reference>Vega, A.S. 2000. Revision taxonomica de las especies americanas del genero Bothriochloa (Poaceae: Panicoideae: Andropogoneae). Darwiniana 38:127-186.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala.;Ark.;Ariz.;Kans.;Miss.;N.Mex.;Okla.;Pacific Islands (Hawaii);Ill.;Nev.;N.Y.;Puerto Rico;S.C.;Tenn.;Utah;Virgin Islands;Fla.;Tex.;La.;Nebr.;Colo.;Md.;Calif.;Ga.;Ind.;Ohio;Mo.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Pedicellate spikelets about as long as the sessile spikelets.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Sessile spikelets 5.5-7 mm long</description>
      <determination>1 Bothriochloa wrightii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Sessile spikelets 3-4.5 mm long.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Rachises longer than the branches</description>
      <determination>10 Bothriochloa bladhii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Rachises shorter than the branches.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Lower glumes of the sessile spikelets with a dorsal pit</description>
      <determination>12 Bothriochloa pertusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Lower glumes of the sessile spikelets without a dorsal pit</description>
      <determination>11 Bothriochloa ischaemum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Pedicellate spikelets much shorter than the sessile spikelets.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Sessile spikelets 2.5-4.5 mm long; awns absent or less than 17 mm long.</description>
      <next_statement_id>6.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Sessile spikelets unawned or with awns less than 6 mm long</description>
      <determination>4 Bothriochloa exaristata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Sessile spikelets with awns 8-17 mm long.</description>
      <next_statement_id>7.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Panicles reddish when mature; hairs below the sessile spikelets about 1/4 as long as the spikelets, sparse, not obscuring the spikelets</description>
      <determination>10 Bothriochloa bladhii</determination>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Panicles silvery-white or light tan; hairs below the sessile spikelets at least 1/2 as long as the spikelets, copious, at least somewhat obscuring the spikelets.</description>
      <next_statement_id>8.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Panicles 9-20 cm long; sessile spikelets narrowly ovate to lanceolate; glumes acute; leaves evenly distributed on the culms; culms 2-4 mm thick</description>
      <determination>3 Bothriochloa longipaniculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Panicles 4-12(14) cm long; sessile spikelets ovate; glumes blunt; leaves often clustered at the base of the culms; culms usually less than 2 mm thick</description>
      <determination>2 Bothriochloa laguroides</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Sessile spikelets 4.5-8.5 mm long; awns 18-35 mm long.</description>
      <next_statement_id>6.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Rachises 5-20 cm long, with numerous branches.</description>
      <next_statement_id>10.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Panicles of the larger shoots 14-25 cm long; culms 130-250 cm tall, 2-4 mm thick, stiffly erect, little-branched distally, glaucous below the nodes; nodes with spreading hairs, the hairs 2-6 mm long</description>
      <determination>5 Bothriochloa alta</determination>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Panicles of the larger shoots 5-14(20) cm long; culms usually 60-120 cm tall, usually less than 2 mm thick, tending to be bent at the base and often branched at maturity, not glaucous below the nodes; nodes with ascending hairs less than 3 mm long</description>
      <determination>6 Bothriochloa barbinodis</determination>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Rachises usually less than 5 cm long, with 2-9 branches.</description>
      <next_statement_id>10.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Cauline nodes densely pubescent, the hairs 3-7 mm long, white, spreading</description>
      <determination>7 Bothriochloa springfieldii</determination>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Cauline nodes glabrous or puberulent, the hairs always less than 3 mm long, usually off-white and ascending.</description>
      <next_statement_id>12.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Lower branches of the inflorescences rebranched; sessile spikelets 4.5-6.5 mm long; lower glumes sparsely hairy near the base; leaves primarily cauline, the blades 2-5 mm wide</description>
      <determination>8 Bothriochloa hybrida</determination>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Lower branches of the inflorescences simple, not rebranched; sessile spikelets 5-8 mm long; lower glumes glabrous; leaves primarily basal, the blades usually less than 2 mm wide</description>
      <determination>9 Bothriochloa edwardsiana</determination>
    </key_statement>
  </key>
</bio:treatment>