<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Jesus Valdes-Reyna;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">44</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Nash" date="unknown" rank="genus">ERIONEURON</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus erioneuron</taxon_hierarchy>
  </taxon_identification>
  <number>17.14</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually cespitose, occasionally stoloniferous.</text>
      <biological_entity id="o6064" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 6-65 cm, erect.</text>
      <biological_entity id="o6065" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="65" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o6066" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6067" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths smooth, glabrous, striate, margins hyaline, collars with tufts of 1-3 mm hairs;</text>
      <biological_entity id="o6068" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s4" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity id="o6069" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o6070" name="collar" name_original="collars" src="d0_s4" type="structure" />
      <biological_entity id="o6071" name="tuft" name_original="tufts" src="d0_s4" type="structure" />
      <biological_entity id="o6072" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o6070" id="r967" name="with" negation="false" src="d0_s4" to="o6071" />
      <relation from="o6071" id="r968" name="part_of" negation="false" src="d0_s4" to="o6072" />
    </statement>
    <statement id="d0_s5">
      <text>blades usually folded, pilose basally, margins white, cartilaginous, apices acute but not sharp.</text>
      <biological_entity id="o6073" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="folded" value_original="folded" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o6074" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s5" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
      <biological_entity id="o6075" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="sharp" value_original="sharp" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, simple panicles (racemes in depauperate specimens), exserted well above the leaves.</text>
      <biological_entity id="o6076" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o6077" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="simple" value_original="simple" />
        <character constraint="above leaves" constraintid="o6078" is_modifier="false" name="position" src="d0_s6" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity id="o6078" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets laterally compressed, with 3-20 florets, distal florets staminate or sterile;</text>
      <biological_entity id="o6079" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o6080" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="20" />
      </biological_entity>
      <relation from="o6079" id="r969" name="with" negation="false" src="d0_s7" to="o6080" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation above the glumes and between the florets.</text>
      <biological_entity constraint="distal" id="o6081" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o6082" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <relation from="o6081" id="r970" name="above the glumes and between" negation="false" src="d0_s8" to="o6082" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes thin, membranous, 1-veined, acute to acuminate;</text>
      <biological_entity id="o6083" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calluses with hairs;</text>
      <biological_entity id="o6084" name="callus" name_original="calluses" src="d0_s10" type="structure" />
      <biological_entity id="o6085" name="hair" name_original="hairs" src="d0_s10" type="structure" />
      <relation from="o6084" id="r971" name="with" negation="false" src="d0_s10" to="o6085" />
    </statement>
    <statement id="d0_s11">
      <text>lemmas rounded on the back, 3-veined, veins conspicuously pilose, at least basally, apices toothed or obtusely 2-lobed, midveins often extended into awns, awns to 4 mm, lateral-veins sometimes extended as small mucros;</text>
      <biological_entity id="o6086" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character constraint="on back" constraintid="o6087" is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o6087" name="back" name_original="back" src="d0_s11" type="structure" />
      <biological_entity id="o6088" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o6089" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="at-least basally; basally" name="shape" src="d0_s11" value="toothed" value_original="toothed" />
        <character is_modifier="false" modifier="obtusely" name="shape" src="d0_s11" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o6090" name="midvein" name_original="midveins" src="d0_s11" type="structure">
        <character constraint="into awns" constraintid="o6091" is_modifier="false" modifier="often" name="size" src="d0_s11" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity id="o6091" name="awn" name_original="awns" src="d0_s11" type="structure" />
      <biological_entity id="o6092" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6093" name="lateral-vein" name_original="lateral-veins" src="d0_s11" type="structure">
        <character constraint="as mucros" constraintid="o6094" is_modifier="false" modifier="sometimes" name="size" src="d0_s11" value="extended" value_original="extended" />
      </biological_entity>
      <biological_entity id="o6094" name="mucro" name_original="mucros" src="d0_s11" type="structure">
        <character is_modifier="true" name="size" src="d0_s11" value="small" value_original="small" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>paleas shorter than the lemmas, keels ciliate, intercostal regions pilose basally;</text>
      <biological_entity id="o6095" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character constraint="than the lemmas" constraintid="o6096" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o6096" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
      <biological_entity id="o6097" name="keel" name_original="keels" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="intercostal" id="o6098" name="region" name_original="regions" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lodicules 2, adnate to the bases of the paleas;</text>
      <biological_entity id="o6099" name="lodicule" name_original="lodicules" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character constraint="to bases" constraintid="o6100" is_modifier="false" name="fusion" src="d0_s13" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o6100" name="base" name_original="bases" src="d0_s13" type="structure" />
      <biological_entity id="o6101" name="palea" name_original="paleas" src="d0_s13" type="structure" />
      <relation from="o6100" id="r972" name="part_of" negation="false" src="d0_s13" to="o6101" />
    </statement>
    <statement id="d0_s14">
      <text>anthers 1 or 3.</text>
      <biological_entity id="o6102" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" unit="or" value="1" value_original="1" />
        <character name="quantity" src="d0_s14" unit="or" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses glossy, translucent;</text>
      <biological_entity id="o6103" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s15" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s15" value="translucent" value_original="translucent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>embryos more than 1/2 as long as the caryopses.</text>
      <biological_entity id="o6105" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>x = 8.</text>
      <biological_entity id="o6104" name="embryo" name_original="embryos" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="1" value_original="1" />
        <character constraint="as-long-as caryopses" constraintid="o6105" name="quantity" src="d0_s16" value="/2" value_original="/2" />
      </biological_entity>
      <biological_entity constraint="x" id="o6106" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Erioneuron is an American genus of three species. Its seedlings appear to have a shaggy, white-villous indumentum, but this is composed of a myriad of small, water-soluble crystals.</discussion>
  <discussion>Stoloniferous plants are unusual in the region covered by the Flora, but they are quite common in populations of Erioneuron nealley and E. avenaceum from central Mexico.</discussion>
  <references>
    <reference>Sanchez, E. 1979. Anatomia foliar de las especies y variedades argentinas de los generos Tridens Roem. et Schult. y Erioneuron Nash (Gramineae-Eragrostoideae-Eragrosteae). Darwiniana 22:159-175</reference>
    <reference>Valdes-Reyna, J. and S.L. Hatch. 1997. A revision of Erioneuron and Dasyochloa (Poaceae: Eragrostideae). Sida 17:645-666.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Kans.;Okla.;Colo.;N.Mex.;Tex.;Utah;Calif.;Ariz.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemmas entire or with teeth to 0.5 mm long, the awns 0.5-2.5 mm long; both glumes shorter than the lowest floret</description>
      <determination>1 Erioneuron pilosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lemmas 2-lobed, the lobes 1-2.5 mm long, the awns 1-4 mm long; upper glumes equaling or exceeding the lowest floret.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemma lobes obtuse to broadly acute, 1-2 mm long; lateral veins not forming mucros; plants 7-40 cm tall</description>
      <determination>2 Erioneuron avenaceum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemma lobes rounded to truncate, 1.5-2.5 mm long; lateral veins forming mucros to 1 mm long; plants 15-65 cm tall</description>
      <determination>3 Erioneuron nealleyi</determination>
    </key_statement>
  </key>
</bio:treatment>