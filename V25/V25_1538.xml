<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">631</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Nash" date="unknown" rank="genus">SORGHASTRUM</taxon_name>
    <taxon_name authority="(L.) Nash" date="unknown" rank="species">nutans</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus sorghastrum;species nutans</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Indiangrass</other_name>
  <other_name type="common_name">Faux-sorgho penche</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rhizomatous, rhizomes short, stout, scaly.</text>
      <biological_entity id="o27931" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o27932" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s0" value="stout" value_original="stout" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s0" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 50-240 cm tall, 1.5-4.5 mm thick, erect;</text>
      <biological_entity id="o27933" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="height" src="d0_s1" to="240" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s1" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes glabrous.</text>
      <biological_entity id="o27934" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous or sparsely hispid;</text>
      <biological_entity id="o27935" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 2-6 mm, usually with thick, pointed auricles;</text>
      <biological_entity id="o27936" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27937" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character is_modifier="true" name="shape" src="d0_s4" value="pointed" value_original="pointed" />
      </biological_entity>
      <relation from="o27936" id="r4757" modifier="usually" name="with" negation="false" src="d0_s4" to="o27937" />
    </statement>
    <statement id="d0_s5">
      <text>blades 10-70 cm long, 1-4 mm wide, usually glabrous.</text>
      <biological_entity id="o27938" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="70" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 20-75 cm, loosely contracted, yellowish to brownish;</text>
      <biological_entity id="o27939" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s6" to="75" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s6" to="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches often flexible.</text>
      <biological_entity id="o27940" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="fragility" src="d0_s7" value="pliable" value_original="flexible" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 5-8.7 mm.</text>
      <biological_entity id="o27941" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="8.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Calluses blunt, villous;</text>
      <biological_entity id="o27942" name="callus" name_original="calluses" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 5-8 mm, pubescent, 7-9-veined;</text>
      <biological_entity constraint="lower" id="o27943" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="7-9-veined" value_original="7-9-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 5-8 mm, 5-veined;</text>
      <biological_entity constraint="upper" id="o27944" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>awns 10-22 (30) mm, about 2-3 times longer than the spikelets, once-geniculate;</text>
      <biological_entity id="o27945" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="30" value_original="30" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s12" to="22" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s12" value="2-3 times longer than" value_original="2-3 times longer than" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="once-geniculate" value_original="once-geniculate" />
      </biological_entity>
      <biological_entity id="o27946" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers (2) 3-5 mm.</text>
      <biological_entity id="o27947" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 2-3 mm.</text>
      <biological_entity id="o27948" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Pedicels 3-6 mm, flexible.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 20, 40, 80.</text>
      <biological_entity id="o27949" name="pedicel" name_original="pedicels" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="6" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s15" value="pliable" value_original="flexible" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27950" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
        <character name="quantity" src="d0_s16" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sorghastrum nutans grows in a wide range of habitats, from prairies to woodlands, savannahs, and scrubland vegetation. It is native from Canada to Mexico and was one of the four principal grasses of the tallgrass prairie that occupied the central United States prior to agricultural development of the region. It is frequently used for forage, for erosion control on slopes and along highways, and in restoration work. It is an attractive plant and can be used to advantage in flower arrangements. It grows readily from seed if adequate moisture is available. There are several cultivars on the market.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wis.;Del.;D.C.;Man.;Ont.;Que.;Sask.;W.Va.;Colo.;Fla.;Wyo.;N.H.;N.Mex.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;Va.;Mass.;Maine;R.I.;Vt.;Ala.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Md.;Ohio;Utah;Mo.;Minn.;Mich.;Mont.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>