<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>H. Oliver Yates;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">22</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">UNIOLA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus uniola</taxon_hierarchy>
  </taxon_identification>
  <number>17.02</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous or stoloniferous.</text>
      <biological_entity id="o25802" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms to 2.5 m, erect, glabrous, unbranched.</text>
      <biological_entity id="o25803" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s2" to="2.5" to_unit="m" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ligules of hairs;</text>
      <biological_entity id="o25804" name="ligule" name_original="ligules" src="d0_s3" type="structure" />
      <biological_entity id="o25805" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <relation from="o25804" id="r4355" name="consists_of" negation="false" src="d0_s3" to="o25805" />
    </statement>
    <statement id="d0_s4">
      <text>blades flat, becoming involute when dry, margins scabrous, tapering to an attenuate apex.</text>
      <biological_entity id="o25806" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="when dry" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o25807" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
        <character constraint="to apex" constraintid="o25808" is_modifier="false" name="shape" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o25808" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, simple panicles, exceeding the leaves;</text>
      <biological_entity id="o25809" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o25811" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o25810" id="r4356" name="exceeding the" negation="false" src="d0_s5" to="o25811" />
    </statement>
    <statement id="d0_s6">
      <text>disarticulation below the glumes.</text>
      <biological_entity id="o25810" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="simple" value_original="simple" />
      </biological_entity>
      <biological_entity id="o25812" name="glume" name_original="glumes" src="d0_s6" type="structure" />
      <relation from="o25810" id="r4357" name="below" negation="false" src="d0_s6" to="o25812" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 8-50 mm long, 6-16 mm wide, ovate-elliptical to ovate-triangular, strongly laterally compressed, with 3-34 florets, lowest 2-8 florets sterile, remaining floret (s) bisexual.</text>
      <biological_entity id="o25813" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s7" to="50" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s7" to="16" to_unit="mm" />
        <character char_type="range_value" from="ovate-elliptical" name="shape" src="d0_s7" to="ovate-triangular" />
        <character is_modifier="false" modifier="strongly laterally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="position" notes="" src="d0_s7" value="lowest" value_original="lowest" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="8" />
      </biological_entity>
      <biological_entity id="o25814" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="34" />
      </biological_entity>
      <biological_entity id="o25815" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o25816" name="floret" name_original="floret" src="d0_s7" type="structure" />
      <relation from="o25813" id="r4358" name="with" negation="false" src="d0_s7" to="o25814" />
      <relation from="o25815" id="r4359" name="remaining" negation="false" src="d0_s7" to="o25816" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes subequal, shorter than the adjacent lemmas, midveins keeled, serrate to serrulate, apices unawned;</text>
      <biological_entity id="o25817" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="subequal" value_original="subequal" />
        <character constraint="than the adjacent lemmas" constraintid="o25818" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o25818" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o25819" name="midvein" name_original="midveins" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="serrate" name="architecture_or_shape" src="d0_s8" to="serrulate" />
      </biological_entity>
      <biological_entity id="o25820" name="apex" name_original="apices" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>lemmas 3-9-veined, midveins keeled, serrate to serrulate, apices somewhat blunt to acute or mucronate, unawned;</text>
      <biological_entity id="o25821" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-9-veined" value_original="3-9-veined" />
      </biological_entity>
      <biological_entity id="o25822" name="midvein" name_original="midveins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s9" value="serrate to serrulate" value_original="serrate to serrulate" />
      </biological_entity>
      <biological_entity id="o25823" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="blunt" name="shape" src="d0_s9" to="acute or mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>paleas, if present, from slightly shorter than to exceeding the lemmas, 2-keeled, keels winged, serrulate or ciliate;</text>
      <biological_entity id="o25824" name="palea" name_original="paleas" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character constraint="than to exceeding the lemmas" constraintid="o25825" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="slightly shorter" value_original="slightly shorter" />
        <character is_modifier="false" modifier="from" name="shape" src="d0_s10" value="2-keeled" value_original="2-keeled" />
      </biological_entity>
      <biological_entity id="o25825" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s10" value="exceeding" value_original="exceeding" />
      </biological_entity>
      <biological_entity id="o25826" name="keel" name_original="keels" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="winged" value_original="winged" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 3;</text>
      <biological_entity id="o25827" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>ovary glabrous;</text>
      <biological_entity id="o25828" name="ovary" name_original="ovary" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 1, with 2 style-branches.</text>
      <biological_entity id="o25829" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o25830" name="style-branch" name_original="style-branches" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
      <relation from="o25829" id="r4360" name="with" negation="false" src="d0_s13" to="o25830" />
    </statement>
    <statement id="d0_s14">
      <text>Caryopses linear;</text>
      <biological_entity id="o25831" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s14" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>embryos less than 1/2 as long as the caryopses.</text>
      <biological_entity id="o25833" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>x = 10.</text>
      <biological_entity id="o25832" name="embryo" name_original="embryos" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="as-long-as caryopses" constraintid="o25833" from="0" name="quantity" src="d0_s15" to="1/2" />
      </biological_entity>
      <biological_entity constraint="x" id="o25834" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Uniola has two species, both of which grow on coastal sand dunes. There is one species native to the Flora region; the second, U. pittieri Hack., extends from northern Mexico to Ecuador, primarily along the Pacific coast. The genus used to be interpreted as including Chasmanthium, a genus that is now included in the Centothecoideae.</discussion>
  <references>
    <reference>Grass Phylogeny Working Group. 2001. Phylogeny and subfamilial classification of the grasses (Poaceae). Ann. Missouri Bot. Gard. 88:373-457</reference>
    <reference>Yates, H.O. 1966a. Morphology and cytology of Uniola (Gramineae). SouthW. Naturalist 11:145-189</reference>
    <reference>Yates, H.O. 1966b. Revision of grasses traditionally referred to Uniola, I. Uniola and Leptochloopsis. SouthW. Naturalist 11:372-394</reference>
    <reference>Yates, H.O. 1966c. Revision of grasses traditionally referred to Uniola, II. Chasmanthium. SouthW. Naturalist 11:415-455.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Miss.;Tex.;La.;Del.;Ala.;N.C.;S.C.;Va.;Ga.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>