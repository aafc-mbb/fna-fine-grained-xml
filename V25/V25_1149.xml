<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">419</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="genus">DICHANTHELIUM</taxon_name>
    <taxon_name authority="(Hitchc.) Freckmann &amp; Lelong" date="unknown" rank="section">Oligosantha</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus dichanthelium;section oligosantha</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually cespitose, often with caudices, clumps sometimes with only 1 culm.</text>
      <biological_entity id="o26665" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o26666" name="caudex" name_original="caudices" src="d0_s0" type="structure" />
      <biological_entity id="o26667" name="clump" name_original="clumps" src="d0_s0" type="structure" />
      <biological_entity id="o26668" name="culm" name_original="culm" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="only" name="quantity" src="d0_s0" value="1" value_original="1" />
      </biological_entity>
      <relation from="o26665" id="r4532" modifier="often" name="with" negation="false" src="d0_s0" to="o26666" />
      <relation from="o26667" id="r4533" name="with" negation="false" src="d0_s0" to="o26668" />
    </statement>
    <statement id="d0_s1">
      <text>Basal rosettes well-differentiated.</text>
      <biological_entity constraint="basal" id="o26669" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" name="variability" src="d0_s1" value="well-differentiated" value_original="well-differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-75 cm, with papillose-based hairs;</text>
      <biological_entity id="o26671" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o26670" id="r4534" name="with" negation="false" src="d0_s2" to="o26671" />
    </statement>
    <statement id="d0_s3">
      <text>fall phase branching from the mid-and upper culm nodes.</text>
      <biological_entity id="o26670" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="75" to_unit="cm" />
        <character constraint="from upper culm nodes" constraintid="o26672" is_modifier="false" name="architecture" src="d0_s3" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="culm" id="o26672" name="node" name_original="nodes" src="d0_s3" type="structure" constraint_original="upper culm" />
    </statement>
    <statement id="d0_s4">
      <text>Cauline leaves 4-7;</text>
      <biological_entity constraint="cauline" id="o26673" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s4" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths glabrous or hirsute, hairs sometimes papillose-based;</text>
      <biological_entity id="o26674" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o26675" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.5-5 mm, of hairs, these often joined at the base into a thickened ring.</text>
      <biological_entity id="o26676" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character constraint="at base" constraintid="o26678" is_modifier="false" modifier="often" name="fusion" notes="" src="d0_s6" value="joined" value_original="joined" />
      </biological_entity>
      <biological_entity id="o26677" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <biological_entity id="o26678" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o26679" name="ring" name_original="ring" src="d0_s6" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s6" value="thickened" value_original="thickened" />
      </biological_entity>
      <relation from="o26676" id="r4535" name="consists_of" negation="false" src="d0_s6" to="o26677" />
      <relation from="o26678" id="r4536" name="into" negation="false" src="d0_s6" to="o26679" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 2.5-4.3 mm, ellipsoid to obovoid, turgid.</text>
      <biological_entity id="o26680" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s7" to="4.3" to_unit="mm" />
        <character char_type="range_value" from="ellipsoid" name="shape" src="d0_s7" to="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s7" value="turgid" value_original="turgid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Upper glumes usually with an orange or purplish spot at the base, with 7-9 prominent veins.</text>
      <biological_entity constraint="upper" id="o26681" name="glume" name_original="glumes" src="d0_s8" type="structure" />
      <biological_entity id="o26682" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="orange" value_original="orange" />
        <character is_modifier="true" name="coloration" src="d0_s8" value="purplish spot" value_original="purplish spot" />
      </biological_entity>
      <biological_entity id="o26683" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s8" to="9" />
        <character is_modifier="true" name="prominence" src="d0_s8" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o26681" id="r4537" name="with" negation="false" src="d0_s8" to="o26682" />
      <relation from="o26681" id="r4538" name="with" negation="false" src="d0_s8" to="o26683" />
    </statement>
    <statement id="d0_s9">
      <text>Upper florets umbonate, sometimes minutely so.</text>
      <biological_entity constraint="upper" id="o26684" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="umbonate" value_original="umbonate" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>