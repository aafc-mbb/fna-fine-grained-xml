<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">554</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">SETARIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Setaria</taxon_name>
    <taxon_name authority="(L.) P. Beauv." date="unknown" rank="species">verticillata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus setaria;subgenus setaria;species verticillata</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chaetochloa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">verticillata</taxon_name>
    <taxon_hierarchy>genus chaetochloa;species verticillata</taxon_hierarchy>
  </taxon_identification>
  <number>20</number>
  <other_name type="common_name">Hooked bristlegrass</other_name>
  <other_name type="common_name">Setaire verticillee</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o13370" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 30-100 cm;</text>
      <biological_entity id="o13371" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes glabrous.</text>
      <biological_entity id="o13372" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous, margins ciliate distally;</text>
      <biological_entity id="o13373" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13374" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules to 1 mm, densely ciliate;</text>
      <biological_entity id="o13375" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 5-15 mm wide, flat, abaxial surfaces scabrous.</text>
      <biological_entity id="o13376" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o13377" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 5-15 cm, tapering to the apices;</text>
      <biological_entity id="o13378" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="15" to_unit="cm" />
        <character constraint="to apices" constraintid="o13379" is_modifier="false" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o13379" name="apex" name_original="apices" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>rachises retrorsely rough hispid;</text>
      <biological_entity id="o13380" name="rachis" name_original="rachises" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="retrorsely" name="pubescence_or_relief" src="d0_s7" value="rough" value_original="rough" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bristles solitary, 4-7 mm, retrorsely scabrous.</text>
      <biological_entity id="o13381" name="bristle" name_original="bristles" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="retrorsely" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 2-2.3 mm.</text>
      <biological_entity id="o13382" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes about 1/3 as long as the spikelets, obtuse, 1 (3) -veined;</text>
      <biological_entity constraint="lower" id="o13383" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character constraint="as-long-as spikelets" constraintid="o13384" name="quantity" src="d0_s10" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
      <biological_entity id="o13384" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>upper glumes nearly as long as the spikelets;</text>
      <biological_entity constraint="upper" id="o13385" name="glume" name_original="glumes" src="d0_s11" type="structure" />
      <biological_entity id="o13386" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
      <relation from="o13385" id="r2197" name="as long as" negation="false" src="d0_s11" to="o13386" />
    </statement>
    <statement id="d0_s12">
      <text>lower paleas about 1/2 as long as the spikelets, broad;</text>
      <biological_entity constraint="lower" id="o13387" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character constraint="as-long-as spikelets" constraintid="o13388" name="quantity" src="d0_s12" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="width" notes="" src="d0_s12" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o13388" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper lemmas finely and transversely rugose;</text>
      <biological_entity constraint="upper" id="o13389" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s13" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper paleas similar to the upper lemmas.</text>
      <biological_entity constraint="upper" id="o13391" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <relation from="o13390" id="r2198" name="to" negation="false" src="d0_s14" to="o13391" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 18, 36, 54, 72, 108.</text>
      <biological_entity constraint="upper" id="o13390" name="palea" name_original="paleas" src="d0_s14" type="structure" />
      <biological_entity constraint="2n" id="o13392" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="18" value_original="18" />
        <character name="quantity" src="d0_s15" value="36" value_original="36" />
        <character name="quantity" src="d0_s15" value="54" value_original="54" />
        <character name="quantity" src="d0_s15" value="72" value_original="72" />
        <character name="quantity" src="d0_s15" value="108" value_original="108" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Setaria verticillata is a European adventive that is now common throughout the cooler regions of the contiguous United States and in southern Canada. It is an aggressive weed in the vineyards of central California. Reports of S. carnei Hitchc. from North America are based on misidentification of this species.</discussion>
  <discussion>Setaria verticillata resembles the S. adhaerans but differs in having longer panicles and spikelets, sheath margins that are ciliate distally, and blades that are scabrous, not hairy. Setaria verticillata is a more northern species than S. adhaerans, but their ranges overlap in the Flora region.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Del.;Wis.;W.Va.;Pacific Islands (Hawaii);Kans.;N.Dak.;Nebr.;S.Dak.;Wyo.;N.Mex.;Tex.;La.;Pa.;D.C.;N.H.;R.I.;Nev.;Va.;Colo.;Md.;Calif.;Ark.;Vt.;Ill.;Ind.;Iowa;Okla.;Ariz.;Idaho;Maine;Mont.;Oreg.;Mass.;Ohio;Utah;Mo.;Minn.;Mich.;B.C.;Man.;N.W.T.;Ont.;Que.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>