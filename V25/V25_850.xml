<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Rich." date="unknown" rank="genus">CYNODON</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">nlemfuënsis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus cynodon;species nlemfuënsis</taxon_hierarchy>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">African bermudagrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants stoloniferous, not rhizomatous;</text>
      <biological_entity id="o1458" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stolons stout, woody, usually lying flat on the ground.</text>
      <biological_entity id="o1459" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character constraint="on ground" constraintid="o1460" is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o1460" name="ground" name_original="ground" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-60 cm tall, 1-5 mm thick, not becoming woody.</text>
      <biological_entity id="o1461" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="height" src="d0_s2" to="60" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s2" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="not becoming" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o1462" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules about 0.3 mm, membranous, ciliolate;</text>
      <biological_entity id="o1463" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="0.3" value_original="0.3" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 5-16 cm long, 2-6 mm wide, abaxial surfaces glabrous or with scattered long hairs, adaxial surfaces with scattered long hairs.</text>
      <biological_entity id="o1464" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="16" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1465" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="with scattered long hairs" value_original="with scattered long hairs" />
      </biological_entity>
      <biological_entity id="o1466" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1467" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o1468" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
      </biological_entity>
      <relation from="o1465" id="r237" name="with" negation="false" src="d0_s5" to="o1466" />
      <relation from="o1467" id="r238" name="with" negation="false" src="d0_s5" to="o1468" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 4-13 branches;</text>
      <biological_entity id="o1469" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o1470" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="13" />
      </biological_entity>
      <relation from="o1469" id="r239" name="with" negation="false" src="d0_s6" to="o1470" />
    </statement>
    <statement id="d0_s7">
      <text>branches (2) 4-7 (10) cm, in 1 (-3) whorls, lax, usually green, axes triquetrous.</text>
      <biological_entity id="o1471" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="7" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" notes="" src="d0_s7" value="lax" value_original="lax" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o1472" name="whorl" name_original="whorls" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s7" to="3" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o1473" name="axis" name_original="axes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="triquetrous" value_original="triquetrous" />
      </biological_entity>
      <relation from="o1471" id="r240" name="in" negation="false" src="d0_s7" to="o1472" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 2-3 mm.</text>
      <biological_entity id="o1474" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Lower glumes 1.7-2 mm;</text>
      <biological_entity constraint="lower" id="o1475" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 1.5-2.3 (3) mm;</text>
      <biological_entity constraint="upper" id="o1476" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 1.9-2.9 mm, keels not winged, shortly pubescent, at least distally;</text>
      <biological_entity id="o1477" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s11" to="2.9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1478" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>paleas glabrous.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 18, 36.</text>
      <biological_entity id="o1479" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1480" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="18" value_original="18" />
        <character name="quantity" src="d0_s13" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cynodon nlemfuënsis is native to east and central Africa, but it is now established in southern Texas (Jones and Jones 1992), and may be present in other parts of the southern United States. It is similar to C. dactylon, but differs in being larger and lacking rhizomes. It is also less hardy, not becoming established where temperatures fall below -4°C. Plants in the Flora region belong to Cynodon nlemfuënsis var. nlemfuënsis which differs from C. nlemfuënsis var. robustus Clayton &amp; J.R Harlan in having shorter inflorescence branches (2-7(10) cm rather than 6-10 cm) and thinner culms (1-1.5 mm rather than 2-5 mm). Cultivars of C. nlemfuënsis include 'Florico', 'Florona', 'Ona', and 'Costa Rica'.</discussion>
  
</bio:treatment>