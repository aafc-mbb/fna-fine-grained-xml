<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">380</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Haller" date="unknown" rank="genus">DIGITARIA</taxon_name>
    <taxon_name authority="(Lam.) Roem. &amp; Schult." date="unknown" rank="species">bicornis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus digitaria;species bicornis</taxon_hierarchy>
  </taxon_identification>
  <number>29</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants of indefinite duration;</text>
    </statement>
    <statement id="d0_s1">
      <text>sometimes stoloniferous.</text>
      <biological_entity id="o8212" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms with erect portion 10-85 cm, long-decumbent, rooting and branching at the lower nodes.</text>
      <biological_entity id="o8213" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" notes="" src="d0_s2" value="long-decumbent" value_original="long-decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character constraint="at lower nodes" constraintid="o8215" is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o8214" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="85" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8215" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <relation from="o8213" id="r1322" name="with" negation="false" src="d0_s2" to="o8214" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths with papillose-based hairs or the upper sheaths glabrous;</text>
      <biological_entity id="o8216" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <biological_entity id="o8217" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity constraint="upper" id="o8218" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o8216" id="r1323" name="with" negation="false" src="d0_s3" to="o8217" />
      <relation from="o8216" id="r1324" name="with" negation="false" src="d0_s3" to="o8218" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-4 mm;</text>
      <biological_entity id="o8219" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 3-14 cm long, 2-9 mm wide, mostly glabrous but the adaxial surfaces with papillose-based hairs basally.</text>
      <biological_entity id="o8220" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="14" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="9" to_unit="mm" />
        <character constraint="but adaxial surfaces" constraintid="o8221" is_modifier="false" modifier="mostly" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8221" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o8222" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o8221" id="r1325" name="with" negation="false" src="d0_s5" to="o8222" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles with (2) 3-6 spikelike primary branches, these digitate or a few solitary branches below;</text>
      <biological_entity id="o8223" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="digitate" value_original="digitate" />
      </biological_entity>
      <biological_entity constraint="primary" id="o8224" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="2" value_original="2" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="6" />
        <character is_modifier="true" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o8225" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="few" value_original="few" />
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o8223" id="r1326" name="with" negation="false" src="d0_s6" to="o8224" />
    </statement>
    <statement id="d0_s7">
      <text>lowest nodes glabrous or with hairs less than 0.4 mm;</text>
      <biological_entity constraint="lowest" id="o8226" name="node" name_original="nodes" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="with hairs" value_original="with hairs" />
      </biological_entity>
      <biological_entity id="o8227" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="0.4" to_unit="mm" />
      </biological_entity>
      <relation from="o8226" id="r1327" name="with" negation="false" src="d0_s7" to="o8227" />
    </statement>
    <statement id="d0_s8">
      <text>primary branches 6.5-21 cm long, 0.6-1.3 mm wide, axes winged, wings at least 1/2 as wide as the midribs, lower and middle portions bearing spikelets in unequally pedicellate pairs, pedicels not adnate to the branches;</text>
      <biological_entity constraint="primary" id="o8228" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="6.5" from_unit="cm" name="length" src="d0_s8" to="21" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s8" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8229" name="axis" name_original="axes" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o8230" name="wing" name_original="wings" src="d0_s8" type="structure">
        <character constraint="as-wide-as midribs" constraintid="o8231" name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o8231" name="midrib" name_original="midribs" src="d0_s8" type="structure" />
      <biological_entity constraint="lower and middle" id="o8232" name="portion" name_original="portions" src="d0_s8" type="structure" />
      <biological_entity id="o8233" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o8234" name="pair" name_original="pairs" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="unequally" name="architecture" src="d0_s8" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o8235" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character constraint="to branches" constraintid="o8236" is_modifier="false" modifier="not" name="fusion" src="d0_s8" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o8236" name="branch" name_original="branches" src="d0_s8" type="structure" />
      <relation from="o8232" id="r1328" name="bearing" negation="false" src="d0_s8" to="o8233" />
      <relation from="o8232" id="r1329" name="in" negation="false" src="d0_s8" to="o8234" />
    </statement>
    <statement id="d0_s9">
      <text>secondary branches absent;</text>
      <biological_entity constraint="secondary" id="o8237" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>shorter pedicels about 0.2 mm;</text>
      <biological_entity constraint="shorter" id="o8238" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>longer pedicels to 2 mm.</text>
      <biological_entity constraint="longer" id="o8239" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 2.6-3.7 mm, spikelet pairs dimorphic in their pubescence and venation pattern of the lower lemmas.</text>
      <biological_entity id="o8240" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s12" to="3.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8241" name="spikelet" name_original="spikelet" src="d0_s12" type="structure">
        <character constraint="in pattern" constraintid="o8242" is_modifier="false" name="growth_form" src="d0_s12" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity id="o8242" name="pattern" name_original="pattern" src="d0_s12" type="structure">
        <character is_modifier="true" name="character" src="d0_s12" value="pubescence" value_original="pubescence" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8243" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
      <relation from="o8242" id="r1330" name="part_of" negation="false" src="d0_s12" to="o8243" />
    </statement>
    <statement id="d0_s13">
      <text>Lower glumes absent or to 0.9 mm, deltoid or bifid;</text>
      <biological_entity constraint="lower" id="o8244" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s13" value="0-0.9 mm" value_original="0-0.9 mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="deltoid" value_original="deltoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="bifid" value_original="bifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 1.7-2.8 mm, 1/2 - 3/4 as long as the spikelets, 3-veined;</text>
      <biological_entity constraint="upper" id="o8245" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s14" to="2.8" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o8246" from="1/2" name="quantity" src="d0_s14" to="3/4" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s14" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o8246" name="spikelet" name_original="spikelets" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>lower lemmas 7-veined, veins smooth;</text>
      <biological_entity constraint="lower" id="o8247" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="7-veined" value_original="7-veined" />
      </biological_entity>
      <biological_entity id="o8248" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower lemmas of shortly pedicellate spikelets with 3 equally spaced, glabrous or shortly pubescent central veins, lemma margins and the region between the 2 lateral-veins with appressed or spreading, 0.5-1 mm hairs;</text>
      <biological_entity constraint="spikelet" id="o8249" name="lemma" name_original="lemmas" src="d0_s16" type="structure" constraint_original="spikelet lower; spikelet">
        <character is_modifier="false" modifier="with 3 equally spaced" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8250" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="shortly" name="architecture" src="d0_s16" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity constraint="central" id="o8251" name="vein" name_original="veins" src="d0_s16" type="structure" />
      <biological_entity constraint="lemma" id="o8252" name="margin" name_original="margins" src="d0_s16" type="structure" />
      <biological_entity constraint="between lemma" constraintid="o8254-o8254" id="o8253" name="region" name_original="region" src="d0_s16" type="structure" constraint_original="between  lemma, " />
      <biological_entity id="o8254" name="lateral-vein" name_original="lateral-veins" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8255" name="hair" name_original="hairs" src="d0_s16" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s16" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="orientation" src="d0_s16" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="0.5" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o8249" id="r1331" name="part_of" negation="false" src="d0_s16" to="o8250" />
      <relation from="o8252" id="r1332" name="with" negation="false" src="d0_s16" to="o8255" />
      <relation from="o8253" id="r1333" name="with" negation="false" src="d0_s16" to="o8255" />
    </statement>
    <statement id="d0_s17">
      <text>lower lemmas of long-pedicellate spikelets with unequally spaced veins, midvein well-separated from the 3 lateral-veins, lateral-veins crowded together near the margins, lemma margins and the region between the 2 inner lateral-veins hairy with appressed or strongly divergent, 1-2 mm hairs, sometimes also with longer, glassy yellow hairs;</text>
      <biological_entity constraint="spikelet" id="o8256" name="lemma" name_original="lemmas" src="d0_s17" type="structure" constraint_original="spikelet lower; spikelet" />
      <biological_entity id="o8257" name="spikelet" name_original="spikelets" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="long-pedicellate" value_original="long-pedicellate" />
      </biological_entity>
      <biological_entity id="o8258" name="vein" name_original="veins" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="unequally" name="arrangement" src="d0_s17" value="spaced" value_original="spaced" />
      </biological_entity>
      <biological_entity id="o8259" name="midvein" name_original="midvein" src="d0_s17" type="structure">
        <character constraint="from lateral-veins" constraintid="o8260" is_modifier="false" name="arrangement" src="d0_s17" value="well-separated" value_original="well-separated" />
      </biological_entity>
      <biological_entity id="o8260" name="lateral-vein" name_original="lateral-veins" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o8261" name="lateral-vein" name_original="lateral-veins" src="d0_s17" type="structure">
        <character constraint="near margins" constraintid="o8262" is_modifier="false" name="arrangement" src="d0_s17" value="crowded" value_original="crowded" />
      </biological_entity>
      <biological_entity id="o8262" name="margin" name_original="margins" src="d0_s17" type="structure" />
      <biological_entity constraint="lemma" id="o8263" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character constraint="with " constraintid="o8266" is_modifier="false" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="between lemma" constraintid="o8265-o8265" id="o8264" name="region" name_original="region" src="d0_s17" type="structure" constraint_original="between  lemma, ">
        <character constraint="with " constraintid="o8266" is_modifier="false" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="inner" id="o8265" name="lateral-vein" name_original="lateral-veins" src="d0_s17" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s17" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o8266" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s17" value="appressed" value_original="appressed" />
        <character is_modifier="true" modifier="strongly" name="arrangement" src="d0_s17" value="divergent" value_original="divergent" />
        <character char_type="range_value" constraint="with " constraintid="o8267" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8267" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s17" value="appressed" value_original="appressed" />
        <character is_modifier="true" modifier="strongly" name="arrangement" src="d0_s17" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o8268" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s17" value="longer" value_original="longer" />
        <character is_modifier="true" name="reflectance" src="d0_s17" value="glassy" value_original="glassy" />
        <character is_modifier="true" name="coloration" src="d0_s17" value="yellow" value_original="yellow" />
      </biological_entity>
      <relation from="o8256" id="r1334" name="part_of" negation="false" src="d0_s17" to="o8257" />
      <relation from="o8256" id="r1335" name="with" negation="false" src="d0_s17" to="o8258" />
      <relation from="o8263" id="r1336" modifier="sometimes" name="with" negation="false" src="d0_s17" to="o8268" />
      <relation from="o8264" id="r1337" modifier="sometimes" name="with" negation="false" src="d0_s17" to="o8268" />
    </statement>
    <statement id="d0_s18">
      <text>upper lemmas of all spikelets usually yellow or gray, sometimes light-brown, at maturity;</text>
      <biological_entity constraint="spikelet" id="o8269" name="lemma" name_original="lemmas" src="d0_s18" type="structure" constraint_original="spikelet upper; spikelet">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s18" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="gray" value_original="gray" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s18" value="light-brown" value_original="light-brown" />
      </biological_entity>
      <biological_entity id="o8270" name="spikelet" name_original="spikelets" src="d0_s18" type="structure" />
      <relation from="o8269" id="r1338" name="part_of" negation="false" src="d0_s18" to="o8270" />
    </statement>
    <statement id="d0_s19">
      <text>anthers 0.5-0.6 mm. 2n = 54, 72.</text>
      <biological_entity id="o8271" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s19" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8272" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="54" value_original="54" />
        <character name="quantity" src="d0_s19" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Digitaria bicornis is a common species on the sandy coastal plain of the southeastern United States. Its range extends through Mexico to Costa Rica and northern South America, as well as to the West Indies. The Californian record reflects a 1926 collection; the species is not known to be established in the state.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Va.;Tex.;La.;Ala.;Miss.;N.C.;S.C.;Ga.;Iowa;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>