<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">240</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">SPARTINA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus spartina</taxon_hierarchy>
  </taxon_identification>
  <number>17.45</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose from knotty bases or rhizomatous.</text>
      <biological_entity id="o19399" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character constraint="from bases" constraintid="o19400" is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o19400" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-350 cm, erect, terete, solitary or in small to large clumps.</text>
      <biological_entity id="o19401" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="350" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character constraint="in clumps" constraintid="o19402" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="in small to large clumps" />
      </biological_entity>
      <biological_entity id="o19402" name="clump" name_original="clumps" src="d0_s2" type="structure">
        <character char_type="range_value" from="small" is_modifier="true" name="size" src="d0_s2" to="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o19403" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o19404" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths open, smooth, sometimes striate;</text>
      <biological_entity id="o19405" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes" name="coloration_or_pubescence_or_relief" src="d0_s4" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules membranous, ciliate, cilia longer than the membranous bases;</text>
      <biological_entity id="o19406" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o19407" name="cilium" name_original="cilia" src="d0_s5" type="structure">
        <character constraint="than the membranous bases" constraintid="o19408" is_modifier="false" name="length_or_size" src="d0_s5" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o19408" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="true" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades flat or involute.</text>
      <biological_entity id="o19409" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, usually exceeding the upper leaves, 3-70 cm, panicles of 1-75 spikelike branches attached to an elongate rachis;</text>
      <biological_entity id="o19410" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="70" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o19411" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o19412" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
      <biological_entity id="o19413" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="75" />
        <character is_modifier="true" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
        <character constraint="to rachis" constraintid="o19414" is_modifier="false" name="fixation" src="d0_s7" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o19414" name="rachis" name_original="rachis" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o19410" id="r3268" modifier="usually" name="exceeding the" negation="false" src="d0_s7" to="o19411" />
      <relation from="o19412" id="r3269" name="consist_of" negation="false" src="d0_s7" to="o19413" />
    </statement>
    <statement id="d0_s8">
      <text>branches racemosely arranged, alternate, opposite, or whorled, appressed to strongly divergent, axes 3-sided, spikelets usually sessile on the 2 lower sides, usually divergent to strongly divergent;</text>
      <biological_entity id="o19415" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="racemosely" name="arrangement" src="d0_s8" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o19416" name="axis" name_original="axes" src="d0_s8" type="structure" />
      <biological_entity constraint="lower" id="o19418" name="side" name_original="sides" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>disarticulation beneath the glumes.</text>
      <biological_entity id="o19417" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character constraint="on lower sides" constraintid="o19418" is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="usually divergent" name="arrangement" notes="" src="d0_s8" to="strongly divergent" />
      </biological_entity>
      <biological_entity id="o19419" name="glume" name_original="glumes" src="d0_s9" type="structure" />
      <relation from="o19417" id="r3270" name="beneath" negation="false" src="d0_s9" to="o19419" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets laterally compressed, with 1 floret.</text>
      <biological_entity id="o19420" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o19421" name="floret" name_original="floret" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <relation from="o19420" id="r3271" name="with" negation="false" src="d0_s10" to="o19421" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes unequal, strongly keeled;</text>
      <biological_entity id="o19422" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes shorter than the florets, 1-veined;</text>
      <biological_entity constraint="lower" id="o19423" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character constraint="than the florets" constraintid="o19424" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o19424" name="floret" name_original="florets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper glumes usually longer than the florets, 1-6-veined;</text>
      <biological_entity constraint="upper" id="o19425" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character constraint="than the florets" constraintid="o19426" is_modifier="false" name="length_or_size" src="d0_s13" value="usually longer" value_original="usually longer" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-6-veined" value_original="1-6-veined" />
      </biological_entity>
      <biological_entity id="o19426" name="floret" name_original="florets" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>lemmas shorter than the paleas, 1-3-veined, midveins keeled, lateral-veins usually obscure;</text>
      <biological_entity id="o19427" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character constraint="than the paleas" constraintid="o19428" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o19428" name="palea" name_original="paleas" src="d0_s14" type="structure" />
      <biological_entity id="o19429" name="midvein" name_original="midveins" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o19430" name="lateral-vein" name_original="lateral-veins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s14" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas thin, papery, 2-veined, obscurely keeled;</text>
      <biological_entity id="o19431" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character is_modifier="false" name="width" src="d0_s15" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s15" value="papery" value_original="papery" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-veined" value_original="2-veined" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 3;</text>
      <biological_entity id="o19432" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lodicules sometimes present, truncate, vascularized;</text>
      <biological_entity id="o19433" name="lodicule" name_original="lodicules" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s17" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>styles 2, plumose.</text>
      <biological_entity id="o19434" name="style" name_original="styles" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s18" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Caryopses rarely produced, x = 10.</text>
      <biological_entity id="o19435" name="caryopsis" name_original="caryopses" src="d0_s19" type="structure" />
      <biological_entity constraint="x" id="o19436" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Spartina is a genus of 15-17 species, most of which grow in moist to wet, saline habitats, both coastal and interior. Reproduction of all the species is almost entirely vegetative.</discussion>
  <discussion>There are nine native and two introduced species in the Flora region, plus three hybrids, one of which is native, the other two being deliberate introductions. One of the introduced species, 5. maritima, grows in both Europe and at a few locations in Africa; the African populations may also represent introductions.</discussion>
  <discussion>On the eastern seaboard of North America, the native species of Spartina extend as far north as Nova Scotia, but the few species native to the western seaboard do not extend north of California. Two species, S. alterniflora and S. densiflora, have, however, become established as far north as Washington and now threaten the health of many coastal salt marshes and mud flats (see http://www.spartina.org/ or http://www.wwta.org/environ/environ_topics.htm).</discussion>
  <discussion>Mobberley (1956), on whose work this treatment is based, described three groups within Spartina, but did not give them formal recognition. Most of the species in the Flora region belong to his third group, which he characterized as having hard culms, scabrous leaf margins, more or less divergent inflorescence branches, usually closely imbricate spikelets, and hispid keels on their glumes and lemmas. Spartina alterniflora and S. foliosa belong to Mobberley's second group, the species of which have rather thick, succulent culms, glabrous blades with smooth margins, and less closely imbricate spikelets than in the other two groups. Spartina spartinae is the only species of his first group to grow in the Flora region. Like other members of the group, it has hard, slender culms, numerous short, closely imbricate inflorescence branches, spikelets that are hispid or villous, at least on the keels of the glumes, and no rhizomes.</discussion>
  <references>
    <reference>Coastal Conservancy. 2002. Invasive Spartina project, http://www.spartina.org/</reference>
    <reference>Kartesz, J. and C.A. Meacham. 1999. Synthesis of the North American Flora, Version 1.0 (CD-ROM). North Carolina Botanical Garden, Chapel Hill, North Carolina, U.S.A.</reference>
    <reference>Mobberley, D.G. 1956. Taxonomy and distribution of the genus Spartina. Iowa State Coll. J. Sci. 30:471-574</reference>
    <reference>Spicher, D. and M. Joselyn. 1985. Spartina (Gramineae) in northern California: Distribution and taxonomic notes. Madrono 32:158-167.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Va.;Del.;D.C;Wis.;W.Va.;Mass.;Maine;N.H.;R.I.;Vt.;Fla.;Wyo.;B.C.;N.Mex.;Tex.;La.;Idaho;Tenn.;N.C.;S.C.;Pa.;Ala.;Miss.;Nev.;Puerto Rico;Colo.;Virgin Islands;Calif.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Man.;Minn.;Ohio;Oreg.;Md.;Utah;Mo.;Mich.;Mont.;Alta.;N.B.;Nfld. and Labr. (Labr.);N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Leaf blades with smooth or slightly scabrous margins.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Panicles branches 2-8 cm long, usually closely appressed and often twisted, the lower branches evidently less closely imbricate than the upper branches; glumes usually curved; plants of California and Baja California, Mexico</description>
      <determination>3 Spartina foliosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Panicle branches 2-24 cm long, usually loosely appressed or divergent, usually not twisted, lower and upper branches more or less equally imbricate; glumes straight; plants of varied distribution, including California and Baja California, Mexico.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Glumes usually mostly glabrous on the sides, sometimes with appressed hairs; panicles with 3-25 branches</description>
      <determination>2 Spartina alterniflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Glumes usually with appressed hairs on the sides, the margins sometimes glabrous; panicles with 1-12 branches.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Ligules 2-3 mm long; anthers 5-13 mm long, well-filled, dehiscent at maturity</description>
      <determination>6 Spartina anglica</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Ligules 0.2-1.8 mm long; anthers 3-10 mm long, sometimes poorly filled and indehiscent at maturity.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Ligules 0.2-0.6 mm long; leaf blades 6-12 cm long; anthers 3-6.5 mm long, well-filled, dehiscent at maturity</description>
      <determination>4 Spartina maritima</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Ligules 1-1.8 mm long; leaf blades 6-30 cm long; anthers 5-10 mm long, poorly filled and indehiscent at maturity</description>
      <determination>5 Spartina xtownsendii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Leaf blades with strongly scabrous margins.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Panicles smooth in outline, with (6)15-75 tightly appressed panicle branches; branches 0.5-4(7) cm long; plants without rhizomes</description>
      <determination>1 Spartina spartinae</determination>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Panicles not smooth in outline, with 2-67 tightly appressed to strongly divergent branches; branches 1-15 cm; plants with more than 15 panicle branches always strongly rhizomatous, those with less than 16 branches with or without rhizomes.</description>
      <next_statement_id>7.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Plants without rhizomes or the rhizomes short; culms usually clumped; panicle branches 2-16.</description>
      <next_statement_id>8.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Upper glumes 1-veined</description>
      <determination>9 Spartina densiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Upper glumes 3-4-veined.</description>
      <next_statement_id>9.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Spikelets 6-9 mm long; culms to 200 cm tall; plants of the southeastern United States</description>
      <determination>7 Spartina bakeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Spikelets 10-17 mm long; culms to 120 cm tall; plants of the northeastern United States</description>
      <determination>12 Spartina xcaespitosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Plants with well-developed rhizomes; culms usually solitary, sometimes a few together; panicle branches 3-67.</description>
      <next_statement_id>8.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Rhizomes whitish; upper glumes 1-veined or with all lateral veins on the same side of the keels; panicles with 2-15 branches, the branches 1-9 cm long.</description>
      <next_statement_id>11.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Spikelets 6-11 mm long, ovate to lanceolate; inland plants of western North America, rarely found east of Lake Winnipeg and the Mississippi Valley</description>
      <determination>10 Spartina gracilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Spikelets  7-17 mm long,  linear-lanceolate to  ovate-lanceolate;  usually coastal, also known from a few inland sites in northeastern North America.</description>
      <next_statement_id>12.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Spikelets 7-12 mm long; blade of the second leaf below the panicles 0.5-4(7) mm wide; plants of disturbed and undisturbed  coastal habitats from the Gulf of St. Lawrence to the Gulf of Mexico and, as an introduction, on the west coast of North America</description>
      <determination>11 Spartina patens</determination>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Spikelets 10-17 mm long; blade of the second leaf below the panicles 2-7 mm wide; plants of disturbed habitats and artificial wetlands from Maine to Maryland</description>
      <determination>12 Spartina xcaespitosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Rhizomes light brown to brownish-purple; upper glumes 1-veined or with lateral veins on either side of the keels; panicles with 3-67 branches, the branches 1.5-15 cm long.</description>
      <next_statement_id>11.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Blade of the second leaf below the panicles 2-5(7) mm wide, usually involute even when fresh; panicles with 3-9 branches, the branches 3-9 cm long</description>
      <determination>12 Spartina xcaespitosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Blade of the second leaf below the panicles 5-14 mm wide, flat when fresh; panicles with 5-67 branches, the branches 1.5-15 cm long.</description>
      <next_statement_id>14.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Lower glumes 3/4 as long as to equaling the adjacent lemmas; upper glumes awned, the awns 3-8 mm, with glabrous, rarely hispid, lateral veins</description>
      <determination>13 Spartina pectinata</determination>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Lower glumes less than 1/2 as long to 2/3 as long as the adjacent lemmas; upper glumes unawned or with awns up to 2 mm long, usually with hispid lateral veins</description>
      <determination>8 Spartina cynosuroides</determination>
    </key_statement>
  </key>
</bio:treatment>