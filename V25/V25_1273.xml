<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">477</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="(Nash) Zuloaga" date="unknown" rank="subgenus">Agrostoidea</taxon_name>
    <taxon_name authority="(Nash) C.C. Hsu" date="unknown" rank="section">Agrostoidea</taxon_name>
    <taxon_name authority="Bosc ex Nees" date="unknown" rank="species">rigidulum</taxon_name>
    <taxon_name authority="(Scribn. &amp; C.R. Ball) Freckmann &amp; Lelong" date="unknown" rank="subspecies">combsii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus agrostoidea;section agrostoidea;species rigidulum;subspecies combsii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">longifolium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">combsii</taxon_name>
    <taxon_hierarchy>genus panicum;species longifolium;variety combsii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">combsii</taxon_name>
    <taxon_hierarchy>genus panicum;species combsii</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants similar to subsp.</text>
    </statement>
    <statement id="d0_s1">
      <text>pubescens, but often shortly rhizomatous, more nearly glabrous, and more deeply and consistently purple throughout.</text>
      <biological_entity id="o22628" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="often shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="deeply; consistently; throughout" name="coloration_or_density" src="d0_s1" value="purple" value_original="purple" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Ligules nearly obsolete;</text>
      <biological_entity id="o22629" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="nearly" name="prominence" src="d0_s2" value="obsolete" value_original="obsolete" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades usually 2-7 mm wide, often folded or involute, usually pilose adaxially, at least near the base, bases about equal in width to the subtending sheaths.</text>
      <biological_entity id="o22630" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s3" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s3" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="usually; adaxially" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o22631" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o22632" name="base" name_original="bases" src="d0_s3" type="structure">
        <character constraint="to subtending sheaths" constraintid="o22633" is_modifier="false" name="width" src="d0_s3" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o22633" name="sheath" name_original="sheaths" src="d0_s3" type="structure" />
      <relation from="o22630" id="r3860" modifier="at-least" name="near" negation="false" src="d0_s3" to="o22631" />
    </statement>
    <statement id="d0_s4">
      <text>Spikelets 2.6-3.8 mm, usually purple, slender, erect on the pedicels.</text>
      <biological_entity id="o22634" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s4" to="3.8" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="coloration_or_density" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" name="size" src="d0_s4" value="slender" value_original="slender" />
        <character constraint="on pedicels" constraintid="o22635" is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o22635" name="pedicel" name_original="pedicels" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Lower glumes to 3/4 as long as the spikelets.</text>
      <biological_entity constraint="lower" id="o22636" name="glume" name_original="glumes" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o22637" from="0" name="quantity" src="d0_s5" to="3/4" />
      </biological_entity>
      <biological_entity id="o22637" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
    </statement>
  </description>
  <discussion>Panicum rigidulum subsp. combsii is restricted to the United States, where it grows in the same moist habitats as subsp. pubescens, but is much less common. Its long, narrow, purple, often gaping spikelets somewhat resem¬ble those of P. virgatum, which often grows in the same habitats.</discussion>
  
</bio:treatment>