<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">216</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Sw." date="unknown" rank="genus">CHLORIS</taxon_name>
    <taxon_name authority="E. Fourn." date="unknown" rank="species">andropogonoides</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus chloris;species andropogonoides</taxon_hierarchy>
  </taxon_identification>
  <number>15</number>
  <other_name type="common_name">Slimspike windmill-grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose to shortly stoloniferous.</text>
      <biological_entity id="o22495" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-40 cm.</text>
      <biological_entity id="o22496" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o22497" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.5-0.8 mm, shortly ciliate;</text>
      <biological_entity id="o22498" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="0.8" to_unit="mm" />
        <character is_modifier="false" modifier="shortly" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades to 15 cm long, to 1 mm wide, sometimes with basal hairs, mostly glabrous or scabrous.</text>
      <biological_entity id="o22499" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o22500" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <relation from="o22499" id="r3827" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o22500" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 6-13, evidently distinct branches, these usually digitate, sometimes with a second, poorly-developed whorl just below the terminal branches;</text>
      <biological_entity id="o22501" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s6" value="digitate" value_original="digitate" />
      </biological_entity>
      <biological_entity id="o22502" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s6" to="13" />
        <character is_modifier="true" modifier="evidently" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o22503" name="whorl" name_original="whorl" src="d0_s6" type="structure">
        <character is_modifier="true" name="development" src="d0_s6" value="poorly-developed" value_original="poorly-developed" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o22504" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <relation from="o22501" id="r3828" name="with" negation="false" src="d0_s6" to="o22502" />
      <relation from="o22501" id="r3829" modifier="sometimes" name="with" negation="false" src="d0_s6" to="o22503" />
    </statement>
    <statement id="d0_s7">
      <text>branches 4-14 cm, spreading, spikelet-bearing to the base, with 4-7 spikelets per cm;</text>
      <biological_entity id="o22506" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o22507" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
      <biological_entity id="o22508" name="cm" name_original="cm" src="d0_s7" type="structure" />
      <relation from="o22505" id="r3830" name="to" negation="false" src="d0_s7" to="o22506" />
      <relation from="o22505" id="r3831" name="with" negation="false" src="d0_s7" to="o22507" />
      <relation from="o22507" id="r3832" name="per" negation="false" src="d0_s7" to="o22508" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation at the uppermost cauline node, panicles falling intact.</text>
      <biological_entity id="o22505" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="14" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="uppermost cauline" id="o22509" name="node" name_original="node" src="d0_s8" type="structure" />
      <biological_entity id="o22510" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s8" value="falling" value_original="falling" />
        <character is_modifier="false" name="condition" src="d0_s8" value="intact" value_original="intact" />
      </biological_entity>
      <relation from="o22505" id="r3833" name="at" negation="false" src="d0_s8" to="o22509" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets with 1 bisexual and 1 sterile floret.</text>
      <biological_entity id="o22511" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o22512" name="floret" name_original="floret" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o22511" id="r3834" name="with" negation="false" src="d0_s9" to="o22512" />
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes 2-2.3 mm;</text>
      <biological_entity constraint="lower" id="o22513" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 3-3.3 mm;</text>
      <biological_entity constraint="upper" id="o22514" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="3.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lowest lemmas 1.9-2.7 mm long, 0.5-0.6 mm wide, lanceolate to elliptic, without conspicuous grooves on the sides, mostly glabrous but the margins and keels appressed-pubescent with hairs less than 1 mm, apices acute, awned, awns 1.9-5.2 mm;</text>
      <biological_entity constraint="lowest" id="o22515" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="length" src="d0_s12" to="2.7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="elliptic" />
        <character is_modifier="false" modifier="mostly" name="pubescence" notes="" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22516" name="groove" name_original="grooves" src="d0_s12" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s12" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o22517" name="side" name_original="sides" src="d0_s12" type="structure" />
      <biological_entity id="o22518" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character constraint="with hairs" constraintid="o22520" is_modifier="false" name="pubescence" src="d0_s12" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity id="o22519" name="keel" name_original="keels" src="d0_s12" type="structure">
        <character constraint="with hairs" constraintid="o22520" is_modifier="false" name="pubescence" src="d0_s12" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity id="o22520" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22521" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <relation from="o22515" id="r3835" name="without" negation="false" src="d0_s12" to="o22516" />
      <relation from="o22516" id="r3836" name="on" negation="false" src="d0_s12" to="o22517" />
    </statement>
    <statement id="d0_s13">
      <text>second florets 0.9-1.7 mm, 0.2-0.5 mm wide, narrowly cylindrical, obtuse, bilobed and awned, lobes less than 1/5 as long as the lemmas, awns 2.5-3.5 mm.</text>
      <biological_entity id="o22522" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s12" to="5.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o22523" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s13" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s13" to="0.5" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="bilobed" value_original="bilobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o22524" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character char_type="range_value" constraint="as-long-as lemmas" constraintid="o22525" from="0" name="quantity" src="d0_s13" to="1/5" />
      </biological_entity>
      <biological_entity id="o22525" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity id="o22526" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 1.3-1.4 mm long, about 0.4 mm wide, ellipsoid.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 40.</text>
      <biological_entity id="o22527" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="length" src="d0_s14" to="1.4" to_unit="mm" />
        <character name="width" src="d0_s14" unit="mm" value="0.4" value_original="0.4" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22528" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chloris andropogonoides grows along grassy roadsides and prairie relicts of the coastal plain of southern Texas and northeastern Mexico.</discussion>
  <discussion>Hybridization and introgression between Chloris cucullata, Chloris verticillata, and Chloris andropogonoides.</discussion>
  <discussion>Anderson (1974, pp. 97-103) noted that Chloris cucullata, C. verticillata, and C. andropogonoides are sympatric in southern and central Texas, and often form mixed populations that include many apparent hybrids and introgressants. These plants combine the morphological features of their parents and often have highly irregular meiosis. Diploid counts of about 60 are common in some populations, but seed set is high even in populations with a high level of meiotic irregularity, suggesting apomixis. In some populations, no 'pure' parental plants are found, eliminated either through competition or hybridization. Some of the morphologically-distinct members of such hybrid complexes have been given formal names but, because morphologically-similar hybrids can have different origins, these names do not reflect true taxonomic entities. Among such names are C. hrevispica Nash, C. verticillata var. aristulata Torr. &amp; A. Gray, C. verticillata var. intermdia Vasey, C. latisquamea Nash, and C. subdolichostachya Mull.-Hal. Plants belonging to such complexes are best named as hybrids between their parents, e.g. "Chloris verticillata x C. andropogonoides", or as being close to one of the probable parents, e.g., "close to Chloris andropogonoides E. Fourn."</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>