<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">265</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Lag." date="unknown" rank="genus">BOUTELOUA</taxon_name>
    <taxon_name authority="(Desv.) A. Gray" date="unknown" rank="subgenus">Chondrosum</taxon_name>
    <taxon_name authority="Lag." date="unknown" rank="species">barbata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">barbata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus bouteloua;subgenus chondrosum;species barbata;variety barbata</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Sixweeks grama</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>not stoloniferous.</text>
      <biological_entity id="o22529" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 1-35 cm, usually decumbent and geniculate, occasionally rooting at the lower nodes.</text>
      <biological_entity id="o22530" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="35" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character constraint="at lower nodes" constraintid="o22531" is_modifier="false" modifier="occasionally" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o22531" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Ligules 0.4-1 mm;</text>
      <biological_entity id="o22532" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 0.5-5 (9) cm long, 0.7-3 mm wide.</text>
      <biological_entity id="o22533" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="cm" value="9" value_original="9" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 0.7-9 cm, with (2) 4-11 branches;</text>
      <biological_entity id="o22534" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s5" to="9" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o22535" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s5" value="2" value_original="2" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s5" to="11" />
      </biological_entity>
      <relation from="o22534" id="r3837" name="with" negation="false" src="d0_s5" to="o22535" />
    </statement>
    <statement id="d0_s6">
      <text>branches 10-27 mm, scabrous, with 20-40 spikelets.</text>
      <biological_entity id="o22537" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s6" to="40" />
      </biological_entity>
      <relation from="o22536" id="r3838" name="with" negation="false" src="d0_s6" to="o22537" />
    </statement>
    <statement id="d0_s7">
      <text>2n = 20.</text>
      <biological_entity id="o22536" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="27" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22538" name="chromosome" name_original="" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bouteloua barbata var. barbata grows in loose sands, rocky slopes, and washes, often on disturbed soils, usually at elevations below 2000 m. Its range extends from the southwestern United States to northwestern Mexico.</discussion>
  
</bio:treatment>