<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">101</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Wolf" date="unknown" rank="genus">ERAGROSTIS</taxon_name>
    <taxon_name authority="(Schrad.) Schult." date="unknown" rank="species">bahiensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eragrostis;species bahiensis</taxon_hierarchy>
  </taxon_identification>
  <number>45</number>
  <other_name type="common_name">Bahia lovegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, with innovations, without rhizomes, not glandular.</text>
      <biological_entity id="o17745" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o17746" name="innovation" name_original="innovations" src="d0_s1" type="structure" />
      <biological_entity id="o17747" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="architecture_or_function_or_pubescence" src="d0_s1" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 25-95 (110) cm, erect, glabrous.</text>
      <biological_entity id="o17748" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="110" value_original="110" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s2" to="95" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous, summits hairy, hairs 1-3 mm;</text>
      <biological_entity id="o17749" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17750" name="summit" name_original="summits" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o17751" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.2-0.4 mm;</text>
      <biological_entity id="o17752" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (8) 12-40 cm long, 2-5 mm wide, flat to involute, abaxial surfaces glabrous, adaxial surfaces scabridulous and glabrous or long ciliate basally.</text>
      <biological_entity id="o17753" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="8" value_original="8" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s5" to="40" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17754" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17755" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles terminal, 15-30 (45) cm long, (4) 8-17 cm wide, narrowly ovate, open to contracted;</text>
      <biological_entity id="o17756" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character name="length" src="d0_s6" unit="cm" value="45" value_original="45" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character name="width" src="d0_s6" unit="cm" value="4" value_original="4" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s6" to="17" to_unit="cm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>primary branches 5-15 cm, diverging 20-90° from the rachises, often capillary, usually naked basally;</text>
      <biological_entity constraint="primary" id="o17757" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s7" to="15" to_unit="cm" />
        <character constraint="from rachises" constraintid="o17758" is_modifier="false" modifier="20-90°" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
        <character is_modifier="false" modifier="often" name="shape" notes="" src="d0_s7" value="capillary" value_original="capillary" />
        <character is_modifier="false" modifier="usually; basally" name="architecture" src="d0_s7" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o17758" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>pulvini glabrous;</text>
      <biological_entity id="o17759" name="pulvinus" name_original="pulvini" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 0.3-6 mm, mostly appressed, scabridulous, always shorter than the spikelets.</text>
      <biological_entity id="o17760" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="relief" src="d0_s9" value="scabridulous" value_original="scabridulous" />
        <character constraint="than the spikelets" constraintid="o17761" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="always shorter" value_original="always shorter" />
      </biological_entity>
      <biological_entity id="o17761" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 6-15 (18) mm long, 1.3-2 (2.2) mm wide, narrowly lanceolate, plumbeous, occasionally with a reddish-purple tinge, with 8-30 (40) florets;</text>
      <biological_entity id="o17763" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s10" value="40" value_original="40" />
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s10" to="30" />
      </biological_entity>
      <relation from="o17762" id="r2990" modifier="with a reddish-purple tinge" name="with" negation="false" src="d0_s10" to="o17763" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation usually in the rachilla below the florets, occasionally the lemmas falling separately, leaving the paleas on the rachilla.</text>
      <biological_entity id="o17762" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character name="length" src="d0_s10" unit="mm" value="18" value_original="18" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s10" to="15" to_unit="mm" />
        <character name="width" src="d0_s10" unit="mm" value="2.2" value_original="2.2" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="plumbeous" value_original="plumbeous" />
      </biological_entity>
      <biological_entity id="o17764" name="rachillum" name_original="rachilla" src="d0_s11" type="structure" />
      <biological_entity id="o17765" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <biological_entity id="o17766" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="separately" name="life_cycle" src="d0_s11" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity id="o17767" name="palea" name_original="paleas" src="d0_s11" type="structure" />
      <biological_entity id="o17768" name="rachillum" name_original="rachilla" src="d0_s11" type="structure" />
      <relation from="o17762" id="r2991" modifier="usually" name="in" negation="false" src="d0_s11" to="o17764" />
      <relation from="o17764" id="r2992" name="below" negation="false" src="d0_s11" to="o17765" />
      <relation from="o17766" id="r2993" name="leaving the" negation="false" src="d0_s11" to="o17767" />
      <relation from="o17766" id="r2994" name="on" negation="false" src="d0_s11" to="o17768" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes lanceolate to ovate, membranous to subhyaline, keeled;</text>
      <biological_entity id="o17769" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="ovate" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s12" value="subhyaline" value_original="subhyaline" />
        <character is_modifier="false" name="shape" src="d0_s12" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 1-1.4 mm;</text>
      <biological_entity constraint="lower" id="o17770" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 1.4-1.7 mm;</text>
      <biological_entity constraint="upper" id="o17771" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s14" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 1.5-2.2 mm, broadly ovate, leathery, scabridulous, lateral-veins evident, apices acute;</text>
      <biological_entity id="o17772" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.2" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s15" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="relief" src="d0_s15" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o17773" name="lateral-vein" name_original="lateral-veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o17774" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 1.4-2.1 mm, hyaline, bases not projecting beyond the lemmas, keels scabridulous, apices acute to obtuse;</text>
      <biological_entity id="o17775" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s16" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o17776" name="base" name_original="bases" src="d0_s16" type="structure">
        <character constraint="beyond lemmas" constraintid="o17777" is_modifier="false" modifier="not" name="orientation" src="d0_s16" value="projecting" value_original="projecting" />
      </biological_entity>
      <biological_entity id="o17777" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity id="o17778" name="keel" name_original="keels" src="d0_s16" type="structure">
        <character is_modifier="false" name="relief" src="d0_s16" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o17779" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s16" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 2, 0.4-0.6 mm, reddish-purple.</text>
      <biological_entity id="o17780" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s17" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s17" value="reddish-purple" value_original="reddish-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 0.6-0.8 mm, obovoid to ellipsoid, terete, somewhat striate, reddish-brown.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = unknown.</text>
      <biological_entity id="o17781" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s18" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="obovoid" name="shape" src="d0_s18" to="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s18" value="terete" value_original="terete" />
        <character is_modifier="false" modifier="somewhat" name="coloration_or_pubescence_or_relief" src="d0_s18" value="striate" value_original="striate" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17782" name="chromosome" name_original="" src="d0_s19" type="structure" />
    </statement>
  </description>
  <discussion>Eragrostis bahiensis grows in sandy soils near river banks, lake shores, and roadsides, at 0-200 m. Its range extends south from the Gulf Coast of the United States through Mexico to Peru, Bolivia, Paraguay, and Argentina.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.J.;La.;Ala.;Ga.;S.C.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>