<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">187</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="Swallen" date="unknown" rank="species">×involuta</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species ×involuta</taxon_hierarchy>
  </taxon_identification>
  <number>50</number>
  <other_name type="common_name">Canyon muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous.</text>
      <biological_entity id="o21461" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 60-140 cm, erect;</text>
      <biological_entity id="o21462" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s2" to="140" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes puberulent or glabrous for most of their length, puberulent below the nodes.</text>
      <biological_entity id="o21463" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="length" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character constraint="for" is_modifier="false" name="length" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="below nodes" constraintid="o21464" is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o21464" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths shorter or longer than the internodes, smooth or scabridulous, tightly imbricate, yellowish-brown, basal sheaths laterally compressed, keeled, not becoming spirally coiled when old;</text>
      <biological_entity id="o21465" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character constraint="than the internodes" constraintid="o21466" is_modifier="false" name="length_or_size" src="d0_s4" value="shorter or longer" value_original="shorter or longer" />
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="tightly" name="arrangement" src="d0_s4" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellowish-brown" value_original="yellowish-brown" />
      </biological_entity>
      <biological_entity id="o21466" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o21468" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s4" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="when old" name="architecture" src="d0_s4" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 3-12 mm, firm and brown basally, membranous distally, acute;</text>
      <biological_entity id="o21469" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="12" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="firm" value_original="firm" />
        <character is_modifier="false" modifier="basally" name="coloration" src="d0_s5" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="distally" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 10-45 cm long, 1.6-5 mm wide, tightly folded, scabrous abaxially, hirsute adaxially.</text>
      <biological_entity id="o21470" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="45" to_unit="cm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="tightly" name="architecture_or_shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" modifier="abaxially" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 18-40 cm long, 1.5-7 cm wide, loosely contracted to open, not dense;</text>
      <biological_entity id="o21471" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="18" from_unit="cm" name="length" src="d0_s7" to="40" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s7" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" modifier="not" name="density" src="d0_s7" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches 1-10 cm, ascending or diverging up to 60° from the rachises, stiff, naked basally;</text>
      <biological_entity id="o21472" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character constraint="from rachises" constraintid="o21473" is_modifier="false" modifier="0-60°" name="orientation" src="d0_s8" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s8" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s8" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o21473" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels 2-8 mm, hirtellous.</text>
      <biological_entity id="o21474" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3-4.2 mm, yellowish to purplish.</text>
      <biological_entity id="o21475" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.2" to_unit="mm" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s10" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes subequal, 2-3 mm, exceeded by the florets, scabridulous or smooth, 1-veined, acute or obtuse, unawned;</text>
      <biological_entity id="o21476" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o21477" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <relation from="o21476" id="r3663" name="exceeded by the" negation="false" src="d0_s11" to="o21477" />
    </statement>
    <statement id="d0_s12">
      <text>lemmas 3-4.2 mm, lanceolate, glabrous or appressed-pubescent on the lower 1/4 of the margins, apices acute to obtuse, usually bifid and awned, teeth to 0.3 mm, awns 0.5-4 mm, straight;</text>
      <biological_entity id="o21478" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character constraint="on lower 1/4" constraintid="o21479" is_modifier="false" name="pubescence" src="d0_s12" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o21479" name="1/4" name_original="1/4" src="d0_s12" type="structure" />
      <biological_entity id="o21480" name="margin" name_original="margins" src="d0_s12" type="structure" />
      <biological_entity id="o21481" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="obtuse" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s12" value="bifid" value_original="bifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o21482" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21483" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o21479" id="r3664" name="part_of" negation="false" src="d0_s12" to="o21480" />
    </statement>
    <statement id="d0_s13">
      <text>paleas 3-4.2 mm, lanceolate, glabrous, acute to acuminate;</text>
      <biological_entity id="o21484" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s13" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 1-1.8 mm, purplish.</text>
      <biological_entity id="o21485" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses not seen.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 24.</text>
      <biological_entity id="o21486" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure" />
      <biological_entity constraint="2n" id="o21487" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia ×involuta grows on rocky, calcareous slopes in openings and along canyons, at elevations of 150-500 m. It has only been found growing naturally in Texas, but it is also available commercially as an ornamental. Swallen (1932) suggested that M. reverchonii and M. lindheimeri were its parents, but M. rigida seems to be another plausible possibility.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>