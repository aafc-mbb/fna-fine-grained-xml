<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">340</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Andy Sudkamp</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Caro" date="unknown" rank="subfamily">ARISTIDOIDEAE</taxon_name>
    <taxon_name authority="C.E. Hubb." date="unknown" rank="tribe">ARISTIDEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ARISTIDA</taxon_name>
    <taxon_name authority="Chapm." date="unknown" rank="species">gyrans</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily aristidoideae;tribe aristideae;genus aristida;species gyrans</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>29</number>
  <other_name type="common_name">Corkscrew threeawn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>tightly cespitose, bases often bleached, without rhizomes.</text>
      <biological_entity id="o13737" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="tightly" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13738" name="base" name_original="bases" src="d0_s1" type="structure" />
      <biological_entity id="o13739" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure" />
      <relation from="o13738" id="r2251" name="without" negation="false" src="d0_s1" to="o13739" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-65 cm tall, 1-4 mm thick at the base, erect, rarely geniculate at the base, unbranched;</text>
      <biological_entity id="o13740" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="height" src="d0_s2" to="65" to_unit="cm" />
        <character char_type="range_value" constraint="at base" constraintid="o13741" from="1" from_unit="mm" name="thickness" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o13742" is_modifier="false" modifier="rarely" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o13741" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o13742" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>internodes often in a sequence of 2 short and 1 long.</text>
      <biological_entity id="o13743" name="internode" name_original="internodes" src="d0_s3" type="structure" />
      <biological_entity id="o13744" name="sequence" name_original="sequence" src="d0_s3" type="structure">
        <character modifier="of 2 short" name="length" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <relation from="o13743" id="r2252" name="in" negation="false" src="d0_s3" to="o13744" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o13745" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o13746" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths usually shorter than the internodes, glabrous, remaining intact at maturity;</text>
      <biological_entity id="o13747" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character constraint="than the internodes" constraintid="o13748" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="usually shorter" value_original="usually shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="at maturity" is_modifier="false" name="condition" src="d0_s5" value="intact" value_original="intact" />
      </biological_entity>
      <biological_entity id="o13748" name="internode" name_original="internodes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>collars glabrous;</text>
      <biological_entity id="o13749" name="collar" name_original="collars" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 0.2-0.3 mm;</text>
      <biological_entity id="o13750" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s7" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades (3) 5-15 cm long, to 1 mm wide, involute, rarely loosely folded or flat, some¬what stiff and arcuate, bases glabrous abaxially, pale green.</text>
      <biological_entity id="o13751" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s8" to="15" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="rarely loosely" name="shape" src="d0_s8" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="fragility" src="d0_s8" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="course_or_shape" src="d0_s8" value="arcuate" value_original="arcuate" />
      </biological_entity>
      <biological_entity id="o13752" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale green" value_original="pale green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences paniculate or racemose, 10-30 cm long, 1-2 cm wide, slender, lax;</text>
      <biological_entity id="o13753" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="racemose" value_original="racemose" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s9" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s9" to="2" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>primary branches 3-5 cm, loosely appressed, without axillary pulvini, with 2-5 spikelets.</text>
      <biological_entity constraint="primary" id="o13754" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s10" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o13755" name="pulvinus" name_original="pulvini" src="d0_s10" type="structure" />
      <biological_entity id="o13756" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="5" />
      </biological_entity>
      <relation from="o13754" id="r2253" name="without" negation="false" src="d0_s10" to="o13755" />
      <relation from="o13754" id="r2254" name="with" negation="false" src="d0_s10" to="o13756" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets appressed.</text>
      <biological_entity id="o13757" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes usually unequal, 1-veined, acuminate or awned, awns to 4 mm, tan to dark brownish or purplish;</text>
      <biological_entity id="o13758" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o13759" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="tan" name="coloration" src="d0_s12" to="dark brownish or purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Lower glumes 6-9 (11) mm;</text>
      <biological_entity constraint="lower" id="o13760" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="11" value_original="11" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 9-12 mm;</text>
      <biological_entity constraint="upper" id="o13761" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>calluses 1-2 mm;</text>
      <biological_entity id="o13762" name="callus" name_original="calluses" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas 5-7 mm, glabrous, brownish, without a column, the junction with the awns not evident;</text>
      <biological_entity id="o13763" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity id="o13764" name="column" name_original="column" src="d0_s16" type="structure" />
      <biological_entity id="o13765" name="junction" name_original="junction" src="d0_s16" type="structure" />
      <biological_entity id="o13766" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s16" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o13763" id="r2255" name="without" negation="false" src="d0_s16" to="o13764" />
      <relation from="o13765" id="r2256" name="with" negation="false" src="d0_s16" to="o13766" />
    </statement>
    <statement id="d0_s17">
      <text>awns 8-15 mm, subequal, loosely spirally contorted, but not coiled, just above the base, ascending to spreading distally, not disarticulating at maturity;</text>
      <biological_entity id="o13767" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s17" to="15" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s17" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="loosely spirally" name="arrangement_or_shape" src="d0_s17" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s17" value="coiled" value_original="coiled" />
        <character char_type="range_value" from="ascending" modifier="distally" name="orientation" notes="" src="d0_s17" to="spreading" />
        <character constraint="at maturity" is_modifier="false" modifier="not" name="architecture" src="d0_s17" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
      <biological_entity id="o13768" name="base" name_original="base" src="d0_s17" type="structure" />
      <relation from="o13767" id="r2257" modifier="just" name="above" negation="false" src="d0_s17" to="o13768" />
    </statement>
    <statement id="d0_s18">
      <text>anthers 3, 1-1.5 mm, brownish.</text>
      <biological_entity id="o13769" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s18" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Caryopses 3-4 mm, somewhat lustrous, chestnut-colored.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = unknown.</text>
      <biological_entity id="o13770" name="caryopsis" name_original="caryopses" src="d0_s19" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s19" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="somewhat" name="reflectance" src="d0_s19" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="chestnut-colored" value_original="chestnut-colored" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13771" name="chromosome" name_original="" src="d0_s20" type="structure" />
    </statement>
  </description>
  <discussion>Aristida gyrans is endemic to the southeastern United States, growing in sandy pine woods and oak scrub. It differs from other species in the genus by its combination of narrow blades, unequal glumes, long calluses, and contorted awns.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.;Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>