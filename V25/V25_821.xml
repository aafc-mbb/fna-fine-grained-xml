<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">225</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="genus">ENTEROPOGON</taxon_name>
    <taxon_name authority="(J. Presl) Clayton" date="unknown" rank="species">chlorideus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus enteropogon;species chlorideus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chloris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">chloridea</taxon_name>
    <taxon_hierarchy>genus chloris;species chloridea</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Buryseed umbrellagrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose and rhizomatous, each rhizome terminating in a cleistogamous spikelet.</text>
      <biological_entity id="o11568" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11569" name="whole-organism" name_original="" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <biological_entity id="o11570" name="rhizome" name_original="rhizome" src="d0_s1" type="structure" />
      <biological_entity id="o11571" name="spikelet" name_original="spikelet" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <relation from="o11570" id="r1903" name="terminating in a" negation="false" src="d0_s1" to="o11571" />
    </statement>
    <statement id="d0_s2">
      <text>Culms to 100 cm, erect.</text>
      <biological_entity id="o11572" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths sparsely pilose near the ligules;</text>
      <biological_entity id="o11573" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="near ligules" constraintid="o11574" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o11574" name="ligule" name_original="ligules" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ligules of lower leaves with a single prominent tuft of hairs;</text>
      <biological_entity id="o11575" name="ligule" name_original="ligules" src="d0_s4" type="structure" constraint="leaf" constraint_original="leaf; leaf" />
      <biological_entity constraint="lower" id="o11576" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o11577" name="tuft" name_original="tuft" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="true" name="prominence" src="d0_s4" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o11578" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <relation from="o11575" id="r1904" name="part_of" negation="false" src="d0_s4" to="o11576" />
      <relation from="o11575" id="r1905" name="with" negation="false" src="d0_s4" to="o11577" />
      <relation from="o11577" id="r1906" name="part_of" negation="false" src="d0_s4" to="o11578" />
    </statement>
    <statement id="d0_s5">
      <text>ligules of upper leaves usually glabrous;</text>
      <biological_entity id="o11579" name="ligule" name_original="ligules" src="d0_s5" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="upper" id="o11580" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o11579" id="r1907" name="part_of" negation="false" src="d0_s5" to="o11580" />
    </statement>
    <statement id="d0_s6">
      <text>blades to 30 cm long, to 1 cm wide, usually scabrous, occasionally pilose.</text>
      <biological_entity id="o11581" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s6" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s6" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles with 3-10 (15) racemosely arranged branches, usually most nodes with more than 1 branch;</text>
      <biological_entity id="o11582" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
      <biological_entity id="o11583" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="15" value_original="15" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="10" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o11584" name="node" name_original="nodes" src="d0_s7" type="structure" />
      <biological_entity id="o11585" name="branch" name_original="branch" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" upper_restricted="false" />
      </biological_entity>
      <relation from="o11582" id="r1908" name="with" negation="false" src="d0_s7" to="o11583" />
      <relation from="o11584" id="r1909" modifier="usually" name="with" negation="false" src="d0_s7" to="o11585" />
    </statement>
    <statement id="d0_s8">
      <text>branches 6-10 cm, naked below, with about 4 spikelets per cm distally.</text>
      <biological_entity id="o11586" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="below" name="architecture" src="d0_s8" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o11587" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o11588" name="cm" name_original="cm" src="d0_s8" type="structure" />
      <relation from="o11586" id="r1910" name="with" negation="false" src="d0_s8" to="o11587" />
      <relation from="o11587" id="r1911" name="per" negation="false" src="d0_s8" to="o11588" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets with 1 bisexual and 1 sterile floret.</text>
      <biological_entity id="o11589" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o11590" name="floret" name_original="floret" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o11589" id="r1912" name="with" negation="false" src="d0_s9" to="o11590" />
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes 1-2 mm;</text>
      <biological_entity constraint="lower" id="o11591" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 2-3.5 mm;</text>
      <biological_entity constraint="upper" id="o11592" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower lemmas 4.5-7.5 mm long, about 1 mm wide, linear to narrowly lanceolate, glabrous or the margins sparsely strigose above, apices acute to acuminate, often bidentate, unawned or awned, awns 6.5-15 mm;</text>
      <biological_entity constraint="lower" id="o11593" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s12" to="7.5" to_unit="mm" />
        <character name="width" src="d0_s12" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s12" to="narrowly lanceolate" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11594" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o11595" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="acuminate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s12" value="bidentate" value_original="bidentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o11596" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s12" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>sterile florets 1.4-3 mm long, to 0.3 mm wide, awns 2-8 mm.</text>
      <biological_entity id="o11597" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s13" to="0.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11598" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Chasmogamous caryopses about 4.5 mm long, about 0.8 mm wide;</text>
      <biological_entity id="o11599" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="chasmogamous" value_original="chasmogamous" />
        <character name="length" src="d0_s14" unit="mm" value="4.5" value_original="4.5" />
        <character name="width" src="d0_s14" unit="mm" value="0.8" value_original="0.8" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>cleistogamous caryopses to 4 mm long, about 2.5 mm wide.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 40, 80.</text>
      <biological_entity id="o11600" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s15" value="cleistogamous" value_original="cleistogamous" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s15" to="4" to_unit="mm" />
        <character name="width" src="d0_s15" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11601" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
        <character name="quantity" src="d0_s16" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Enteropogon chlorideus is native from the south-western United States through Mexico to Honduras. The spikelet-bearing rhizomes distinguish Enteropogon from most other grasses, but they are often missing from herbarium specimens. Seed set is highest in the cleistogamous spikelets.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>