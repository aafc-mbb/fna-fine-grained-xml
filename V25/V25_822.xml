<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">225</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="genus">ENTEROPOGON</taxon_name>
    <taxon_name authority="(Kunth) Clayton" date="unknown" rank="species">prieurii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus enteropogon;species prieurii</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Prieur's umbrellagrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>stoloniferous.</text>
      <biological_entity id="o9128" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms to 80 cm.</text>
      <biological_entity id="o9129" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous, occasionally pilose apically;</text>
      <biological_entity id="o9130" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally; apically" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules short ciliate to long pilose;</text>
      <biological_entity id="o9131" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="short" value_original="short" />
        <character char_type="range_value" from="ciliate" name="pubescence" src="d0_s4" to="long pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 10-30 cm long, to 5 mm wide, glabrous abaxially, scabrous to pilose adaxially.</text>
      <biological_entity id="o9132" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="scabrous" modifier="adaxially" name="pubescence" src="d0_s5" to="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 3-7 branches in a single digitate cluster;</text>
      <biological_entity id="o9133" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o9134" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="7" />
      </biological_entity>
      <relation from="o9133" id="r1505" modifier="in a single digitate cluster" name="with" negation="false" src="d0_s6" to="o9134" />
    </statement>
    <statement id="d0_s7">
      <text>branches 6-11 cm, erect to slightly divergent, with 8-11 spikelets per cm.</text>
      <biological_entity id="o9135" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s7" to="11" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="slightly" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o9136" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s7" to="11" />
      </biological_entity>
      <biological_entity id="o9137" name="cm" name_original="cm" src="d0_s7" type="structure" />
      <relation from="o9135" id="r1506" name="with" negation="false" src="d0_s7" to="o9136" />
      <relation from="o9136" id="r1507" name="per" negation="false" src="d0_s7" to="o9137" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets with 1 bisexual floret and 4-5 sterile florets.</text>
      <biological_entity id="o9138" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o9139" name="floret" name_original="floret" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o9140" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o9138" id="r1508" name="with" negation="false" src="d0_s8" to="o9139" />
      <relation from="o9138" id="r1509" name="with" negation="false" src="d0_s8" to="o9140" />
    </statement>
    <statement id="d0_s9">
      <text>Lower glumes 2.1-2.2 mm;</text>
      <biological_entity constraint="lower" id="o9141" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s9" to="2.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 3.7-4 mm;</text>
      <biological_entity constraint="upper" id="o9142" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lowest lemmas 3.3-4.7 mm long, 0.4-0.7 mm wide, narrowly elliptic, margins densely strigose distally;</text>
      <biological_entity constraint="lowest" id="o9143" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.3" from_unit="mm" name="length" src="d0_s11" to="4.7" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s11" to="0.7" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="arrangement_or_shape" src="d0_s11" value="elliptic" value_original="elliptic" />
      </biological_entity>
      <biological_entity id="o9144" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="densely; distally" name="pubescence" src="d0_s11" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lowest sterile florets 1.5-2.5 mm, cylindrical, awned, awns 8-17 mm;</text>
      <biological_entity constraint="lowest" id="o9145" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cylindrical" value_original="cylindrical" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o9146" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s12" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>distal florets about 0.3 mm, flabellate.</text>
      <biological_entity constraint="distal" id="o9147" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="0.3" value_original="0.3" />
        <character is_modifier="false" name="shape" src="d0_s13" value="flabellate" value_original="flabellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 2-2.5 mm long, about 0.5 mm wide.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = unknown.</text>
      <biological_entity id="o9148" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s14" to="2.5" to_unit="mm" />
        <character name="width" src="d0_s14" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9149" name="chromosome" name_original="" src="d0_s15" type="structure" />
    </statement>
  </description>
  <discussion>Enteropogon prieurii is native to the tropics of the Eastern Hemisphere. It was found near wharves in Alabama and North Carolina at the beginning of the twentieth century, but it is not known to be established in the Flora region.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C.;Ala.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>