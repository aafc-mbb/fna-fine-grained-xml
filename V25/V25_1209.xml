<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">444</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="genus">DICHANTHELIUM</taxon_name>
    <taxon_name authority="(Hitchc.) Freckmann &amp; Lelong" date="unknown" rank="section">Angustifolia</taxon_name>
    <taxon_name authority="(Kunth) Gould &amp; C. A. Clark" date="unknown" rank="species">consanguineum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus dichanthelium;section angustifolia;species consanguineum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">consanguineum</taxon_name>
    <taxon_hierarchy>genus panicum;species consanguineum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">acuminatum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">consanguineum</taxon_name>
    <taxon_hierarchy>genus panicum;species acuminatum;variety consanguineum</taxon_hierarchy>
  </taxon_identification>
  <number>28</number>
  <other_name type="common_name">Kunth's panicgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants grayish-green, cespitose.</text>
      <biological_entity id="o27767" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish-green" value_original="grayish-green" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal rosettes poorly differentiated;</text>
      <biological_entity constraint="basal" id="o27768" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="poorly" name="variability" src="d0_s1" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 2-8 cm, ovate to lanceolate, grading into the cauline blades.</text>
      <biological_entity id="o27769" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o27770" name="blade" name_original="blades" src="d0_s2" type="structure" />
      <relation from="o27769" id="r4734" name="into" negation="false" src="d0_s2" to="o27770" />
    </statement>
    <statement id="d0_s3">
      <text>Culms 20-55 cm, erect;</text>
      <biological_entity id="o27771" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s3" to="55" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes densely bearded;</text>
      <biological_entity id="o27772" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>internodes densely villous;</text>
    </statement>
    <statement id="d0_s6">
      <text>fall phase with spreading culms branching from the lower and midculm nodes, eventually producing flabellate clusters of reduced, flat blades, secondary panicles much reduced.</text>
      <biological_entity id="o27773" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o27774" name="culm" name_original="culms" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character constraint="from lower" constraintid="o27775" is_modifier="false" name="architecture" src="d0_s6" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o27775" name="lower" name_original="lower" src="d0_s6" type="structure" />
      <biological_entity constraint="midculm" id="o27776" name="node" name_original="nodes" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="eventually" name="shape" src="d0_s6" value="flabellate" value_original="flabellate" />
      </biological_entity>
      <biological_entity id="o27777" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
        <character is_modifier="true" name="size" src="d0_s6" value="reduced" value_original="reduced" />
        <character is_modifier="true" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o27778" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <relation from="o27773" id="r4735" name="with" negation="false" src="d0_s6" to="o27774" />
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves 3-4;</text>
      <biological_entity constraint="cauline" id="o27779" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sheaths shorter than the internodes, pilose with ascending papillose-based hairs to villous;</text>
      <biological_entity id="o27780" name="sheath" name_original="sheaths" src="d0_s8" type="structure">
        <character constraint="than the internodes" constraintid="o27781" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character constraint="with hairs" constraintid="o27782" is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o27781" name="internode" name_original="internodes" src="d0_s8" type="structure" />
      <biological_entity id="o27782" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="papillose-based" value_original="papillose-based" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ligules 0.5-2 mm, of hairs;</text>
      <biological_entity id="o27783" name="ligule" name_original="ligules" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27784" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <relation from="o27783" id="r4736" name="consists_of" negation="false" src="d0_s9" to="o27784" />
    </statement>
    <statement id="d0_s10">
      <text>blades 4-12 cm long, 2-8 mm wide, stiffly ascending to erect, often wrinkled along the prominent veins, usually villous on both surfaces, apices involute-pointed, blades of the flag leaves much reduced.</text>
      <biological_entity id="o27785" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s10" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="stiffly ascending" name="orientation" src="d0_s10" to="erect" />
        <character constraint="along veins" constraintid="o27786" is_modifier="false" modifier="often" name="relief" src="d0_s10" value="wrinkled" value_original="wrinkled" />
        <character constraint="on surfaces" constraintid="o27787" is_modifier="false" modifier="usually" name="pubescence" notes="" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o27786" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o27787" name="surface" name_original="surfaces" src="d0_s10" type="structure" />
      <biological_entity id="o27788" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="involute-pointed" value_original="involute-pointed" />
      </biological_entity>
      <biological_entity id="o27789" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="much" name="size" src="d0_s10" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o27790" name="flag" name_original="flag" src="d0_s10" type="structure" />
      <biological_entity id="o27791" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
      <relation from="o27789" id="r4737" name="part_of" negation="false" src="d0_s10" to="o27790" />
      <relation from="o27789" id="r4738" name="part_of" negation="false" src="d0_s10" to="o27791" />
    </statement>
    <statement id="d0_s11">
      <text>Primary panicles 3-7 cm long, 1-4 cm wide, well-exserted;</text>
      <biological_entity constraint="primary" id="o27792" name="panicle" name_original="panicles" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s11" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s11" to="4" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s11" value="well-exserted" value_original="well-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>branches usually ascending, glabrous or puberulent.</text>
      <biological_entity id="o27793" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s12" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 2.3-3 mm long, 1.4-1.8 mm wide, obovoid, biconvex in side view, densely pubescent, attenuate basally.</text>
      <biological_entity id="o27794" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="length" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s13" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="in side view" name="shape" src="d0_s13" value="biconvex" value_original="biconvex" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s13" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Lower glumes about 1/3 as long as the spikelets, attached about 0.2 mm below the upper glumes, clasping at the base, broadly triangular, thinner than the upper glumes, weakly veined;</text>
      <biological_entity constraint="lower" id="o27795" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character constraint="as-long-as spikelets" constraintid="o27796" name="quantity" src="d0_s14" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="fixation" notes="" src="d0_s14" value="attached" value_original="attached" />
        <character constraint="below upper glumes" constraintid="o27797" name="some_measurement" src="d0_s14" unit="mm" value="0.2" value_original="0.2" />
        <character constraint="at base" constraintid="o27798" is_modifier="false" name="architecture_or_fixation" notes="" src="d0_s14" value="clasping" value_original="clasping" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s14" value="triangular" value_original="triangular" />
        <character constraint="than the upper glumes" constraintid="o27799" is_modifier="false" name="width" src="d0_s14" value="thinner" value_original="thinner" />
        <character is_modifier="false" modifier="weakly" name="architecture" src="d0_s14" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity id="o27796" name="spikelet" name_original="spikelets" src="d0_s14" type="structure" />
      <biological_entity constraint="upper" id="o27797" name="glume" name_original="glumes" src="d0_s14" type="structure" />
      <biological_entity id="o27798" name="base" name_original="base" src="d0_s14" type="structure" />
      <biological_entity constraint="upper" id="o27799" name="glume" name_original="glumes" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>upper glumes with 5-9 prominent veins;</text>
      <biological_entity constraint="upper" id="o27800" name="glume" name_original="glumes" src="d0_s15" type="structure" />
      <biological_entity id="o27801" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s15" to="9" />
        <character is_modifier="true" name="prominence" src="d0_s15" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o27800" id="r4739" name="with" negation="false" src="d0_s15" to="o27801" />
    </statement>
    <statement id="d0_s16">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o27802" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s16" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper florets broadly ellipsoid, apices blunt, minutely puberulent.</text>
      <biological_entity constraint="upper" id="o27803" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>2n = 18.</text>
      <biological_entity id="o27804" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="blunt" value_original="blunt" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27805" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dichanthelium consanguineum grows in sandy woodlands and low, boggy pinelands. It is restricted to the southeastern United States. The primary panicles are open-pollinated and produced from April to June; the secondary panicles are cleistogamous and produced from June into fall. Some specimens of D. consanguineum suggest that hybridization occasionally occurs with D. aciculare or D. ovale.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;Miss.;Tex.;La.;Ala.;Tenn.;N.C.;S.C.;Ark.;Ga.;Ind.;Iowa;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="invalid_name">Dichanthelium acuminatum var. Consanguineum; ined.</other_name>
  
</bio:treatment>