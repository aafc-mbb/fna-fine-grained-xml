<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Charles M. Allen;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">685</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">HEMARTHRIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus hemarthria</taxon_hierarchy>
  </taxon_identification>
  <number>26.23</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o23657" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 150 cm, erect or decumbent, rooting at the nodes, usually branched above the bases.</text>
      <biological_entity id="o23658" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="150" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character constraint="at nodes" constraintid="o23659" is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character constraint="above bases" constraintid="o23660" is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o23659" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o23660" name="base" name_original="bases" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves not aromatic;</text>
      <biological_entity id="o23661" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s2" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths mostly glabrous, sometimes ciliate near the base;</text>
      <biological_entity id="o23662" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="near base" constraintid="o23663" is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o23663" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, ciliate;</text>
      <biological_entity id="o23664" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades usually linear-lanceolate, sometimes linear.</text>
      <biological_entity id="o23665" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" modifier="sometimes" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal and axillary, with 1 (2) flattened rames borne on a common peduncle, spikelets partially embedded in the rame axes;</text>
      <biological_entity id="o23666" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o23667" name="rame" name_original="rames" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o23668" name="peduncle" name_original="peduncle" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="common" value_original="common" />
      </biological_entity>
      <biological_entity id="o23670" name="axis" name_original="axes" src="d0_s6" type="structure" />
      <relation from="o23666" id="r4012" name="with" negation="false" src="d0_s6" to="o23667" />
      <relation from="o23667" id="r4013" name="borne on" negation="false" src="d0_s6" to="o23668" />
      <relation from="o23669" id="r4014" modifier="partially" name="embedded in the rame" negation="false" src="d0_s6" to="o23670" />
    </statement>
    <statement id="d0_s7">
      <text>disarticulation in the rames, usually oblique and often tardy.</text>
      <biological_entity id="o23669" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <biological_entity id="o23671" name="rame" name_original="rames" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually; often" name="orientation_or_shape" src="d0_s7" value="oblique" value_original="oblique" />
      </biological_entity>
      <relation from="o23669" id="r4015" name="in" negation="false" src="d0_s7" to="o23671" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets in heterogamous sessile-pedicellate pairs, dorsally compressed.</text>
      <biological_entity id="o23672" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="dorsally" name="shape" notes="" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o23673" name="pair" name_original="pairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="heterogamous" value_original="heterogamous" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="sessile-pedicellate" value_original="sessile-pedicellate" />
      </biological_entity>
      <relation from="o23672" id="r4016" name="in" negation="false" src="d0_s8" to="o23673" />
    </statement>
    <statement id="d0_s9">
      <text>Sessile spikelets with 2 florets;</text>
      <biological_entity id="o23674" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o23675" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <relation from="o23674" id="r4017" name="with" negation="false" src="d0_s9" to="o23675" />
    </statement>
    <statement id="d0_s10">
      <text>calluses blunt;</text>
      <biological_entity id="o23676" name="callus" name_original="calluses" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes coriaceous, smooth;</text>
      <biological_entity constraint="lower" id="o23677" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s11" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes equaling the lower glumes, chartaceous to membranous, sometimes partially adnate to the rame axes, sometimes awned;</text>
      <biological_entity constraint="upper" id="o23678" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="chartaceous" name="texture" notes="" src="d0_s12" to="membranous" />
        <character constraint="to axes" constraintid="o23680" is_modifier="false" modifier="sometimes partially" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" notes="" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="lower" id="o23679" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o23680" name="axis" name_original="axes" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lower florets reduced to hyaline lemmas;</text>
      <biological_entity constraint="lower" id="o23681" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o23682" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper florets bisexual, lemmas unawned.</text>
      <biological_entity constraint="upper" id="o23683" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o23684" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Pedicels thick, fused to the rame axes.</text>
      <biological_entity id="o23685" name="pedicel" name_original="pedicels" src="d0_s15" type="structure">
        <character is_modifier="false" name="width" src="d0_s15" value="thick" value_original="thick" />
        <character constraint="to axes" constraintid="o23686" is_modifier="false" name="fusion" src="d0_s15" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o23686" name="axis" name_original="axes" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Pedicellate spikelets morphologically similar to the sessile spikelets, staminate or sterile, x = 9, 10.</text>
      <biological_entity id="o23687" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s16" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s16" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o23688" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="x" id="o23689" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
      </biological_entity>
      <relation from="o23687" id="r4018" name="to" negation="false" src="d0_s16" to="o23688" />
    </statement>
  </description>
  <discussion>Hemarthria is a genus of 12 species, native to the tropics and subtropics of the Eastern Hemisphere, and possibly to the Western Hemisphere. All the species grow in or near water. One species has been introduced into the Flora region.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pa.;Fla.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>