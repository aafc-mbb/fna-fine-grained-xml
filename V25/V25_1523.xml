<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">623</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Hack." date="unknown" rank="genus">POLYTRIAS</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus polytrias</taxon_hierarchy>
  </taxon_identification>
  <number>26.05</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>stoloniferous.</text>
      <biological_entity id="o15416" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-40 cm, often decumbent and rooting at the lower nodes.</text>
      <biological_entity id="o15417" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o15418" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o15418" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves not aromatic;</text>
      <biological_entity id="o15419" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s3" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, ciliate or fimbriate.</text>
      <biological_entity id="o15420" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, solitary rames, spikelets in homomorphic sessile-pedicellate triplets of 2 sessile spikelets and 1 pedicellate spikelet;</text>
      <biological_entity id="o15421" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s5" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o15422" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <biological_entity id="o15423" name="triplet" name_original="triplets" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="homomorphic" value_original="homomorphic" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile-pedicellate" value_original="sessile-pedicellate" />
      </biological_entity>
      <biological_entity id="o15424" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o15425" name="spikelet" name_original="spikelet" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o15422" id="r2587" name="in" negation="false" src="d0_s5" to="o15423" />
      <relation from="o15423" id="r2588" name="part_of" negation="false" src="d0_s5" to="o15424" />
      <relation from="o15423" id="r2589" name="part_of" negation="false" src="d0_s5" to="o15425" />
    </statement>
    <statement id="d0_s6">
      <text>internodes without a median translucent line;</text>
      <biological_entity constraint="median" id="o15427" name="line" name_original="line" src="d0_s6" type="structure">
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s6" value="translucent" value_original="translucent" />
      </biological_entity>
      <relation from="o15426" id="r2590" name="without" negation="false" src="d0_s6" to="o15427" />
    </statement>
    <statement id="d0_s7">
      <text>disarticulation in the rames below the sessile spikelets, sometimes also beneath the pedicellate spikelets.</text>
      <biological_entity id="o15426" name="internode" name_original="internodes" src="d0_s6" type="structure" />
      <biological_entity id="o15428" name="rame" name_original="rames" src="d0_s7" type="structure" />
      <biological_entity id="o15429" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o15430" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o15426" id="r2591" name="in" negation="false" src="d0_s7" to="o15428" />
      <relation from="o15428" id="r2592" name="below" negation="false" src="d0_s7" to="o15429" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets dorsally compressed, with 1 floret;</text>
      <biological_entity id="o15431" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o15432" name="floret" name_original="floret" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <relation from="o15431" id="r2593" name="with" negation="false" src="d0_s8" to="o15432" />
    </statement>
    <statement id="d0_s9">
      <text>sessile spikelets bisexual;</text>
      <biological_entity id="o15433" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicellate spikelets bisexual, unisexual, or sterile.</text>
      <biological_entity id="o15434" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes equal, oblong, truncate, membranous;</text>
      <biological_entity id="o15435" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character is_modifier="false" name="shape" src="d0_s11" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="texture" src="d0_s11" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes with the margins incurved over the upper glumes;</text>
      <biological_entity constraint="lower" id="o15436" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <biological_entity id="o15437" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character constraint="over upper glumes" constraintid="o15438" is_modifier="false" name="orientation" src="d0_s12" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity constraint="upper" id="o15438" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <relation from="o15436" id="r2594" name="with" negation="false" src="d0_s12" to="o15437" />
    </statement>
    <statement id="d0_s13">
      <text>upper glumes keeled;</text>
      <biological_entity constraint="upper" id="o15439" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>florets bisexual;</text>
      <biological_entity id="o15440" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas hyaline, bifid almost to the base, awned from the cleft;</text>
      <biological_entity id="o15441" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
        <character constraint="to base" constraintid="o15442" is_modifier="false" name="architecture_or_shape" src="d0_s15" value="bifid" value_original="bifid" />
        <character constraint="from the cleft" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o15442" name="base" name_original="base" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>awns twisted, geniculate;</text>
      <biological_entity id="o15443" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="shape" src="d0_s16" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3.</text>
      <biological_entity id="o15444" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Pedicels not fused to the rame axes, x = 10.</text>
      <biological_entity id="o15445" name="pedicel" name_original="pedicels" src="d0_s18" type="structure">
        <character constraint="to axes" constraintid="o15446" is_modifier="false" modifier="not" name="fusion" src="d0_s18" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o15446" name="axis" name_original="axes" src="d0_s18" type="structure" />
      <biological_entity constraint="x" id="o15447" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Polytrias is a monotypic genus of the Asian tropics that has become naturalized in Africa and the Western Hemisphere. It is unusual within the Andropogoneae in having only one floret, rather than two, in its spikelets.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>