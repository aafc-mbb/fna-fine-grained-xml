<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">113</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Willd." date="unknown" rank="genus">DACTYLOCTENIUM</taxon_name>
    <taxon_name authority="(R. Br.) P. Beauv." date="unknown" rank="species">radulans</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus dactyloctenium;species radulans</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Buttongrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annuals or short-lived perennials.</text>
      <biological_entity id="o7254" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o7255" name="whole-organism" name_original="" src="" type="structure">
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 5-20 (50) cm, decumbent or ascending, rarely erect, usually branched.</text>
      <biological_entity id="o7257" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="50" value_original="50" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous or with papillose-based hairs, slightly keeled;</text>
      <biological_entity id="o7258" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="with papillose-based hairs" value_original="with papillose-based hairs" />
        <character is_modifier="false" modifier="slightly" name="shape" notes="" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o7259" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o7258" id="r1186" name="with" negation="false" src="d0_s2" to="o7259" />
    </statement>
    <statement id="d0_s3">
      <text>ligules to 1 mm, membranous, truncate, ciliate;</text>
      <biological_entity id="o7260" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades flat, bases with papillose-based hairs.</text>
      <biological_entity id="o7261" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o7262" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o7263" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o7262" id="r1187" name="with" negation="false" src="d0_s4" to="o7263" />
    </statement>
    <statement id="d0_s5">
      <text>Panicle branches 2-11, 0.4-1.5 cm, almost globose, most of the spikelets in contact with the spikelets of adjacent branches;</text>
      <biological_entity constraint="panicle" id="o7264" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="11" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s5" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="almost" name="shape" src="d0_s5" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o7265" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <biological_entity id="o7266" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <biological_entity id="o7267" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o7264" id="r1188" name="part_of" negation="false" src="d0_s5" to="o7265" />
      <relation from="o7264" id="r1189" name="in contact with" negation="false" src="d0_s5" to="o7266" />
      <relation from="o7266" id="r1190" name="part_of" negation="false" src="d0_s5" to="o7267" />
    </statement>
    <statement id="d0_s6">
      <text>branch axes extending beyond the distal spikelets as 1-1.5 mm points.</text>
      <biological_entity constraint="branch" id="o7268" name="axis" name_original="axes" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o7269" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <biological_entity id="o7270" name="point" name_original="points" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o7268" id="r1191" name="extending beyond" negation="false" src="d0_s6" to="o7269" />
      <relation from="o7269" id="r1192" name="as" negation="false" src="d0_s6" to="o7270" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 3.5-5 mm, with 2-5 florets.</text>
      <biological_entity id="o7271" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7272" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
      <relation from="o7271" id="r1193" name="with" negation="false" src="d0_s7" to="o7272" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes strongly keeled;</text>
      <biological_entity id="o7273" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower glumes 1-2 mm, ovate, acute;</text>
      <biological_entity constraint="lower" id="o7274" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 1.5-3 mm, oblong-elliptic, acuminate, awned, awns 0.5-2.5 mm;</text>
      <biological_entity constraint="upper" id="o7275" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-elliptic" value_original="oblong-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o7276" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 3-4.3 mm, ovate, keels scabrous, 1-veined, veins excurrent about 0.5 mm, apices acuminate to mucronate;</text>
      <biological_entity id="o7277" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o7278" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o7279" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="excurrent" value_original="excurrent" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o7280" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s11" to="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>paleas shorter than the lemmas;</text>
      <biological_entity id="o7281" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character constraint="than the lemmas" constraintid="o7282" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o7282" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers 0.2-0.8 mm, pale-yellow.</text>
      <biological_entity id="o7283" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds about 1.2 mm long, about 0.7 mm wide, transversely rugose, brown.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = unknown.</text>
      <biological_entity id="o7284" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character name="length" src="d0_s14" unit="mm" value="1.2" value_original="1.2" />
        <character name="width" src="d0_s14" unit="mm" value="0.7" value_original="0.7" />
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s14" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7285" name="chromosome" name_original="" src="d0_s15" type="structure" />
    </statement>
  </description>
  <discussion>Dactyloctenium radulans has been found at few locations in the Flora region, most of which were associated with wool waste. It is native to Australia, where it is regarded as a valuable ephemeral pasture grass in the drier inland areas but also as a garden weed. It resembles Dactyloctenium aristatum Link of tropical eastern Africa, differing primarily in having transversely rugose, rather than granular, caryopses.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.;Fla.;Mass.;S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>