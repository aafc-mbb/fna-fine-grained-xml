<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">338</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Andy Sudkamp</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Caro" date="unknown" rank="subfamily">ARISTIDOIDEAE</taxon_name>
    <taxon_name authority="C.E. Hubb." date="unknown" rank="tribe">ARISTIDEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ARISTIDA</taxon_name>
    <taxon_name authority="(Chapm.) Vasey" date="unknown" rank="species">palustris</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily aristidoideae;tribe aristideae;genus aristida;species palustris</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>26</number>
  <other_name type="common_name">Longleaf threeawn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, bases hard, knotty.</text>
      <biological_entity id="o4344" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4345" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="hard" value_original="hard" />
        <character is_modifier="false" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 90-150 cm, often thickened basally, stiffly erect, usually unbranched;</text>
      <biological_entity id="o4346" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="90" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" modifier="often; basally" name="size_or_width" src="d0_s2" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes hollow.</text>
      <biological_entity id="o4347" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline;</text>
      <biological_entity id="o4348" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths usually shorter than the internodes, glabrous, remaining intact at maturity;</text>
      <biological_entity id="o4349" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character constraint="than the internodes" constraintid="o4350" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="usually shorter" value_original="usually shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="at maturity" is_modifier="false" name="condition" src="d0_s5" value="intact" value_original="intact" />
      </biological_entity>
      <biological_entity id="o4350" name="internode" name_original="internodes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>collars glabrous;</text>
      <biological_entity id="o4351" name="collar" name_original="collars" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules to 0.1 mm;</text>
      <biological_entity id="o4352" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>blades (8) 10-30 (35) cm long, 2-4 mm wide, usually flat, occasionally loosely involute, lax, glabrous, light yellow-green to bluish-green when young, drying brownish.</text>
      <biological_entity id="o4353" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="8" value_original="8" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s8" to="30" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="occasionally loosely" name="shape_or_vernation" src="d0_s8" value="involute" value_original="involute" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s8" value="lax" value_original="lax" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="light yellow-green" modifier="when young" name="coloration" src="d0_s8" to="bluish-green" />
        <character is_modifier="false" name="condition" src="d0_s8" value="drying" value_original="drying" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences paniculate, 25-45 (55) cm long, 3-6 cm wide;</text>
      <biological_entity id="o4354" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s9" value="paniculate" value_original="paniculate" />
        <character name="length" src="d0_s9" unit="cm" value="55" value_original="55" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s9" to="45" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s9" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>nodes glabrous;</text>
      <biological_entity id="o4355" name="node" name_original="nodes" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>primary branches 2-8 cm, usually single or paired, appressed to erect, occasionally ascending, without axillary pulvini, with (1) 2-12 spikelets.</text>
      <biological_entity constraint="primary" id="o4356" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s11" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="quantity" src="d0_s11" value="single" value_original="single" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="paired" value_original="paired" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s11" to="erect" />
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s11" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o4357" name="pulvinus" name_original="pulvini" src="d0_s11" type="structure" />
      <biological_entity id="o4358" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s11" value="1" value_original="1" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" to="12" />
      </biological_entity>
      <relation from="o4356" id="r694" name="without" negation="false" src="d0_s11" to="o4357" />
      <relation from="o4356" id="r695" name="with" negation="false" src="d0_s11" to="o4358" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets overlapping, appressed.</text>
      <biological_entity id="o4359" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Glumes (7.5) 9-13.5 mm, subequal, stiff, glabrous or scabridulous, light-brown or greenish-brown;</text>
      <biological_entity id="o4360" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="7.5" value_original="7.5" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s13" to="13.5" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="fragility" src="d0_s13" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish-brown" value_original="greenish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes prominently 2-veined, 2-keeled by the development of 1 lateral-vein, shortly (1-2 mm) awn-tipped;</text>
      <biological_entity constraint="lower" id="o4361" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s14" value="2-veined" value_original="2-veined" />
        <character constraint="by " constraintid="o4362" is_modifier="false" name="shape" src="d0_s14" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" modifier="shortly" name="development" notes="" src="d0_s14" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
      <biological_entity id="o4362" name="lateral-vein" name_original="lateral-vein" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 1-veined, shortly (0.5-1 mm) awn-tipped;</text>
      <biological_entity constraint="upper" id="o4363" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s15" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>calluses 1-1.4 mm;</text>
      <biological_entity id="o4364" name="callus" name_original="calluses" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lemmas 6-9 mm, glabrous, 0.3-0.5 mm wide distally, light tan to brown, junction with the awns not evident;</text>
      <biological_entity id="o4365" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s17" to="9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="0.3" from_unit="mm" modifier="distally" name="width" src="d0_s17" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="light tan" name="coloration" src="d0_s17" to="brown" />
      </biological_entity>
      <biological_entity id="o4366" name="junction" name_original="junction" src="d0_s17" type="structure" />
      <biological_entity id="o4367" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s17" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o4366" id="r696" name="with" negation="false" src="d0_s17" to="o4367" />
    </statement>
    <statement id="d0_s18">
      <text>awns not disarticulating at maturity;</text>
      <biological_entity id="o4368" name="awn" name_original="awns" src="d0_s18" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="not" name="architecture" src="d0_s18" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>central awns 15-40 mm, usually strongly curved basally, strongly divergent to horizontal distally;</text>
      <biological_entity constraint="central" id="o4369" name="awn" name_original="awns" src="d0_s19" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s19" to="40" to_unit="mm" />
        <character is_modifier="false" modifier="usually strongly; basally" name="course" src="d0_s19" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s19" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s19" value="horizontal" value_original="horizontal" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>lateral awns 8-35 mm, at least 1/2 as long as the central awns, erect to strongly divergent;</text>
      <biological_entity constraint="lateral" id="o4370" name="awn" name_original="awns" src="d0_s20" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s20" to="35" to_unit="mm" />
        <character constraint="as-long-as central awns" constraintid="o4371" modifier="at least" name="quantity" src="d0_s20" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s20" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s20" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity constraint="central" id="o4371" name="awn" name_original="awns" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>anthers 3, about 3 mm, purplish.</text>
      <biological_entity id="o4372" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="3" value_original="3" />
        <character name="some_measurement" src="d0_s21" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Caryopses 4.4-5 mm, chestnut-brown.</text>
    </statement>
    <statement id="d0_s23">
      <text>2n = unknown.</text>
      <biological_entity id="o4373" name="caryopsis" name_original="caryopses" src="d0_s22" type="structure">
        <character char_type="range_value" from="4.4" from_unit="mm" name="some_measurement" src="d0_s22" to="5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s22" value="chestnut-brown" value_original="chestnut-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4374" name="chromosome" name_original="" src="d0_s23" type="structure" />
    </statement>
  </description>
  <discussion>Aristida palustris is endemic to the southeastern United States, where it grows in seepage bogs, pitcher plant savannahs, wet pine flatwoods, bald-cypress depressions, and wet prairies. It is a distinctive species of the southeastern coastal plain region that differs from A. lanosa in several reproductive, vegetative, and habitat characteristics.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.;La.;Ala.;Ga.;Miss.;Ky.;Fla.;N.C.;S.C.;Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>