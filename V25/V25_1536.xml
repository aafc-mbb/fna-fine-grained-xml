<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">631</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Nash" date="unknown" rank="genus">SORGHASTRUM</taxon_name>
    <taxon_name authority="(C. Mohr) Nash" date="unknown" rank="species">elliottii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus sorghastrum;species elliottii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sorghastrum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">apalachicolense</taxon_name>
    <taxon_hierarchy>genus sorghastrum;species apalachicolense</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Slender indiangrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not rhizomatous.</text>
      <biological_entity id="o2576" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 70-190 cm tall, 1.2-2.4 mm thick;</text>
      <biological_entity id="o2577" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="70" from_unit="cm" name="height" src="d0_s1" to="190" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="thickness" src="d0_s1" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes glabrous.</text>
      <biological_entity id="o2578" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths mostly glabrous, throats pubescent;</text>
      <biological_entity id="o2579" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2580" name="throat" name_original="throats" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 2-5 mm, decurrent, ciliate;</text>
      <biological_entity id="o2581" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 20-55 cm long, 2.5-5.5 (8) mm wide, mostly glabrous.</text>
      <biological_entity id="o2582" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s5" to="55" to_unit="cm" />
        <character name="width" src="d0_s5" unit="mm" value="8" value_original="8" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s5" to="5.5" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 10-35 cm, open, arching, dark purple;</text>
      <biological_entity id="o2583" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s6" to="35" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="arching" value_original="arching" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>rachises 0.3-0.8 mm thick 1-2 mm above the lowest node;</text>
      <biological_entity id="o2584" name="rachis" name_original="rachises" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="thickness" src="d0_s7" to="0.8" to_unit="mm" />
        <character char_type="range_value" constraint="above lowest node" constraintid="o2585" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o2585" name="node" name_original="node" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>branches capillary, flexible, longest branches 19.5-34.5 cm.</text>
      <biological_entity id="o2586" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="capillary" value_original="capillary" />
        <character is_modifier="false" name="fragility" src="d0_s8" value="pliable" value_original="flexible" />
      </biological_entity>
      <biological_entity constraint="longest" id="o2587" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="19.5" from_unit="cm" name="some_measurement" src="d0_s8" to="34.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 6-7.5 mm long, 1.1-1.4 mm wide, dark chestnut-brown at maturity.</text>
      <biological_entity id="o2588" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s9" to="7.5" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s9" to="1.4" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s9" value="dark chestnut-brown" value_original="dark chestnut-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Calluses 1-1.3 mm, blunt;</text>
      <biological_entity id="o2589" name="callus" name_original="calluses" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 5.5-7.3 mm, glabrous, 5-veined;</text>
      <biological_entity constraint="lower" id="o2590" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="5.5" from_unit="mm" name="some_measurement" src="d0_s11" to="7.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 6.2-7.5 mm;</text>
      <biological_entity constraint="upper" id="o2591" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="6.2" from_unit="mm" name="some_measurement" src="d0_s12" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>awns 25-40 mm, 5 times longer than the spikelets, twice-geniculate;</text>
      <biological_entity id="o2592" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="25" from_unit="mm" name="some_measurement" src="d0_s13" to="40" to_unit="mm" />
        <character is_modifier="false" name="length_or_size" src="d0_s13" value="5 times longer than" value_original="5 times longer than" />
        <character is_modifier="false" name="shape" notes="" src="d0_s13" value="2 times-geniculate" value_original="2 times-geniculate " />
      </biological_entity>
      <biological_entity id="o2593" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>anthers 2-3 mm.</text>
      <biological_entity id="o2594" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 2-2.5 mm.</text>
      <biological_entity id="o2595" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pedicels 3-6.5 mm, flexuous.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 20.</text>
      <biological_entity id="o2596" name="pedicel" name_original="pedicels" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s16" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2597" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sorghastrum elliottii usually grows in dry, open woods on sandy terraces of the lowlands in the southeastern United States, often over a clay subsoil. Plants with straight panicles and sessile spikelets that are 1.3-1.8 mm wide are sometimes called S. apalachicolense D.W. Hall, but the variation appears to be continuous and such plants are included here as S. elliottii.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Okla.;Miss.;Tex.;La.;Ala.;Tenn.;N.C.;S.C.;Va.;Ark.;Ga.;Ind.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>