<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="Lam." date="unknown" rank="species">quadrifarium</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species quadrifarium</taxon_hierarchy>
  </taxon_identification>
  <number>29</number>
  <other_name type="common_name">Paja manse</other_name>
  <other_name type="common_name">Paja colorada</other_name>
  <other_name type="common_name">Tussock paspalum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o3666" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (50) 100-180 cm, erect;</text>
      <biological_entity id="o3667" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="50" value_original="50" />
        <character char_type="range_value" from="100" from_unit="cm" name="some_measurement" src="d0_s2" to="180" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes pubescent.</text>
      <biological_entity id="o3668" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths pubescent, margins extending into auricles;</text>
      <biological_entity id="o3669" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o3670" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <biological_entity id="o3671" name="auricle" name_original="auricles" src="d0_s4" type="structure" />
      <relation from="o3670" id="r588" name="extending into" negation="false" src="d0_s4" to="o3671" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-6.3 mm;</text>
      <biological_entity id="o3672" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="6.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 15-62 cm long, 4.9-6.1 mm wide, involute to flat, glabrous.</text>
      <biological_entity id="o3673" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s6" to="62" to_unit="cm" />
        <character char_type="range_value" from="4.9" from_unit="mm" name="width" src="d0_s6" to="6.1" to_unit="mm" />
        <character char_type="range_value" from="involute" name="shape" src="d0_s6" to="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal, with 15-44 racemosely arranged branches;</text>
      <biological_entity id="o3674" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o3675" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s7" to="44" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o3674" id="r589" name="with" negation="false" src="d0_s7" to="o3675" />
    </statement>
    <statement id="d0_s8">
      <text>branches 1.2-8.5 cm, straight, erect to ascending, lower branches longer than those above;</text>
      <biological_entity id="o3676" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s8" to="8.5" to_unit="cm" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="ascending" />
      </biological_entity>
      <biological_entity constraint="lower" id="o3677" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character constraint="than those above" constraintid="" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branch axes 0.5-0.6 mm wide, narrowly winged, glabrous, margins scabrous, pubescent, terminating in a spikelet.</text>
      <biological_entity constraint="branch" id="o3678" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3679" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o3680" name="spikelet" name_original="spikelet" src="d0_s9" type="structure" />
      <relation from="o3679" id="r590" name="terminating in" negation="false" src="d0_s9" to="o3680" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2-2.5 (3) mm long, 0.9-1.3 mm wide, paired, divergent to spreading from the branch axes, elliptic, brown to stramineous, often purple-tinged.</text>
      <biological_entity id="o3681" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character name="length" src="d0_s10" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s10" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s10" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="paired" value_original="paired" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="divergent" value_original="divergent" />
        <character constraint="from branch axes" constraintid="o3682" is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s10" to="stramineous" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
      <biological_entity constraint="branch" id="o3682" name="axis" name_original="axes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes usually absent, if present, to 0.9 mm, triangular;</text>
      <biological_entity constraint="lower" id="o3683" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes shortly pubescent, 3-veined, purple-spotted, margins entire;</text>
      <biological_entity constraint="upper" id="o3684" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purple-spotted" value_original="purple-spotted" />
      </biological_entity>
      <biological_entity id="o3685" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas glabrous or pubescent, lacking ribs over the veins, 3-veined, margins entire;</text>
      <biological_entity constraint="lower" id="o3686" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o3687" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="lacking" value_original="lacking" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o3688" name="vein" name_original="veins" src="d0_s13" type="structure" />
      <biological_entity id="o3689" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o3687" id="r591" name="over" negation="false" src="d0_s13" to="o3688" />
    </statement>
    <statement id="d0_s14">
      <text>upper florets 2.2-2.5 mm, white.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 20, 30, 40.</text>
      <biological_entity constraint="upper" id="o3690" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3691" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="20" value_original="20" />
        <character name="quantity" src="d0_s15" value="30" value_original="30" />
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum quadrifarium is native to Uruguay, Paraguay, Brazil, and Argentina. It is grown as an ornamental in Florida, but has also become established in disturbed habitats of the southeastern United States. It is considered a noxious weed in New South Wales, Australia.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>