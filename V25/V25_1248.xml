<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">466</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Panicum</taxon_name>
    <taxon_name authority="Sw." date="unknown" rank="species">diffusum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus panicum;section panicum;species diffusum</taxon_hierarchy>
  </taxon_identification>
  <number>11</number>
  <other_name type="common_name">Spreading witchgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose and shortly rhizomatous.</text>
      <biological_entity id="o28295" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (6) 25-100 cm, 0.5-3.5 mm thick, spreading to weakly ascending, usually freely branching;</text>
      <biological_entity id="o28296" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="6" value_original="6" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s2" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="weakly ascending" />
        <character is_modifier="false" modifier="usually freely" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes pilose, hairs spreading to ascending;</text>
      <biological_entity id="o28297" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o28298" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s3" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes pilose, with papillose-based hairs, or sparsely hispid.</text>
      <biological_entity id="o28299" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" notes="" src="d0_s4" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o28300" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o28299" id="r4819" name="with" negation="false" src="d0_s4" to="o28300" />
    </statement>
    <statement id="d0_s5">
      <text>Sheaths rounded, glabrous, margins shortly ciliate distally;</text>
      <biological_entity id="o28301" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o28302" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="shortly; distally" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.6-4 mm, of hairs;</text>
      <biological_entity id="o28303" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o28304" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <relation from="o28303" id="r4820" name="consists_of" negation="false" src="d0_s6" to="o28304" />
    </statement>
    <statement id="d0_s7">
      <text>blades (3) 6-15 cm long, 1-5 mm wide, spreading, abaxial surfaces sparsely hirsute, adaxial surfaces more densely so, hairs papillose-based, midribs prominent and not white, margins involute, bases cuneate, apices subulate.</text>
      <biological_entity id="o28305" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="3" value_original="3" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o28306" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o28307" name="surface" name_original="surfaces" src="d0_s7" type="structure" />
      <biological_entity id="o28308" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture" src="d0_s7" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o28309" name="midrib" name_original="midribs" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o28310" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s7" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o28311" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o28312" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subulate" value_original="subulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Terminal panicles 3-35 cm long, about 1/2 as wide, shortly exserted;</text>
      <biological_entity constraint="terminal" id="o28313" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s8" to="35" to_unit="cm" />
        <character constraint="as exserted" constraintid="o28314" name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o28314" name="exserted" name_original="exserted" src="d0_s8" type="structure">
        <character is_modifier="true" name="width" src="d0_s8" value="wide" value_original="wide" />
        <character is_modifier="true" modifier="shortly exserted" name="position" src="d0_s8" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>axillary panicles smaller, not fully exserted;</text>
      <biological_entity constraint="axillary" id="o28315" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="smaller" value_original="smaller" />
        <character is_modifier="false" modifier="not fully" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>rachises scabridulous;</text>
      <biological_entity id="o28316" name="rachis" name_original="rachises" src="d0_s10" type="structure">
        <character is_modifier="false" name="relief" src="d0_s10" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>primary branches usually solitary, sometimes paired, divergent and widely spaced, secondary branching mostly on the distal 1/3 of the primary branches;</text>
      <biological_entity constraint="primary" id="o28317" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s11" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s11" value="paired" value_original="paired" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s11" value="spaced" value_original="spaced" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s11" value="secondary" value_original="secondary" />
        <character constraint="on distal 1/3" constraintid="o28318" is_modifier="false" name="architecture" src="d0_s11" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28318" name="1/3" name_original="1/3" src="d0_s11" type="structure" />
      <biological_entity constraint="primary" id="o28319" name="branch" name_original="branches" src="d0_s11" type="structure" />
      <relation from="o28318" id="r4821" name="part_of" negation="false" src="d0_s11" to="o28319" />
    </statement>
    <statement id="d0_s12">
      <text>pedicels 1-4 mm, spreading to appressed, confined to the distal portions of the secondary branches.</text>
      <biological_entity id="o28320" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s12" to="appressed" />
      </biological_entity>
      <biological_entity constraint="distal" id="o28321" name="portion" name_original="portions" src="d0_s12" type="structure" />
      <biological_entity constraint="distal secondary" id="o28322" name="branch" name_original="branches" src="d0_s12" type="structure" />
      <relation from="o28320" id="r4822" name="confined to the" negation="false" src="d0_s12" to="o28321" />
      <relation from="o28320" id="r4823" name="confined to the" negation="false" src="d0_s12" to="o28322" />
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 2.1-2.9 mm long, 0.8-1 mm wide, glabrous.</text>
      <biological_entity id="o28323" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="length" src="d0_s13" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s13" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Lower glumes 1-1.2 mm, to 1/2 as long as the spikelets, attenuate, 5-9-veined, veins anastomosing apically;</text>
      <biological_entity constraint="lower" id="o28324" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o28325" from="0" name="quantity" src="d0_s14" to="1/2" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="5-9-veined" value_original="5-9-veined" />
      </biological_entity>
      <biological_entity id="o28325" name="spikelet" name_original="spikelets" src="d0_s14" type="structure" />
      <biological_entity id="o28326" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="apically" name="architecture" src="d0_s14" value="anastomosing" value_original="anastomosing" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes and lower lemmas similar, extending about 0.5 mm beyond the upper florets, 11-13-veined;</text>
      <biological_entity constraint="upper" id="o28327" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="beyond the upper florets" name="architecture" src="d0_s15" value="11-13-veined" value_original="11-13-veined" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o28328" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="beyond the upper florets" name="architecture" src="d0_s15" value="11-13-veined" value_original="11-13-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o28329" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s16" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower paleas 1-1.3 mm;</text>
      <biological_entity constraint="lower" id="o28330" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper florets 1.5-1.8 mm long, 0.6-0.8 mm wide, smooth.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 36.</text>
      <biological_entity constraint="upper" id="o28331" name="floret" name_original="florets" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s18" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s18" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s18" value="smooth" value_original="smooth" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28332" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Panicum diffusum grows along river banks, ditches, and disturbed areas in wet, loamy or clayey soils. Its range extends from Texas to the Caribbean and northern South America.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Virgin Islands;Tex.;La.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>