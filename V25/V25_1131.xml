<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">410</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="genus">DICHANTHELIUM</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Freckmann &amp; Lelong" date="unknown" rank="section">Pedicellata</taxon_name>
    <taxon_name authority="(Vasey) Gould" date="unknown" rank="species">pedicellatum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus dichanthelium;section pedicellata;species pedicellatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pedicellatum</taxon_name>
    <taxon_hierarchy>genus panicum;species pedicellatum</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Corm-based panicgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, not rhizomatous.</text>
      <biological_entity id="o4512" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal rosettes absent.</text>
      <biological_entity constraint="basal" id="o4513" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-70 cm, initially erect, with hard, cormlike bases;</text>
      <biological_entity id="o4514" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="70" to_unit="cm" />
        <character is_modifier="false" modifier="initially" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o4515" name="base" name_original="bases" src="d0_s2" type="structure">
        <character is_modifier="true" name="texture" src="d0_s2" value="hard" value_original="hard" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s2" value="cormlike" value_original="cormlike" />
      </biological_entity>
      <relation from="o4514" id="r723" name="with" negation="false" src="d0_s2" to="o4515" />
    </statement>
    <statement id="d0_s3">
      <text>nodes puberulent to sparsely hirsute;</text>
      <biological_entity id="o4516" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s3" to="sparsely hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes all elongated, puberulent to hirsute;</text>
    </statement>
    <statement id="d0_s5">
      <text>fall phase with decumbent culms, developing divaricate branches from the midculm nodes before the primary panicles mature.</text>
      <biological_entity id="o4517" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="length" src="d0_s4" value="elongated" value_original="elongated" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s4" to="hirsute" />
      </biological_entity>
      <biological_entity id="o4518" name="phase" name_original="phase" src="d0_s5" type="structure" />
      <biological_entity id="o4519" name="culm" name_original="culms" src="d0_s5" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s5" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity id="o4520" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="development" src="d0_s5" value="developing" value_original="developing" />
        <character is_modifier="true" name="arrangement" src="d0_s5" value="divaricate" value_original="divaricate" />
      </biological_entity>
      <biological_entity constraint="midculm" id="o4521" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity constraint="primary" id="o4522" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o4517" id="r724" name="fall" negation="false" src="d0_s5" to="o4518" />
      <relation from="o4520" id="r725" name="from" negation="false" src="d0_s5" to="o4521" />
      <relation from="o4521" id="r726" name="before" negation="false" src="d0_s5" to="o4522" />
    </statement>
    <statement id="d0_s6">
      <text>Cauline leaves 4-7;</text>
      <biological_entity constraint="cauline" id="o4523" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>sheaths sometimes overlapping, puberulent to papillose-hispid, margins ciliate;</text>
      <biological_entity id="o4524" name="sheath" name_original="sheaths" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes" name="arrangement" src="d0_s7" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s7" to="papillose-hispid" />
      </biological_entity>
      <biological_entity id="o4525" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>ligules 0.3-1 mm, membranous and ciliate;</text>
      <biological_entity id="o4526" name="ligule" name_original="ligules" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>blades 3-12 cm long, 2-8 mm wide, widening distal to the rounded or subcordate bases, thin, glabrous or sparsely hirsute, margins with papillose-based cilia.</text>
      <biological_entity id="o4527" name="blade" name_original="blades" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s9" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s9" value="widening" value_original="widening" />
        <character constraint="to bases" constraintid="o4528" is_modifier="false" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o4528" name="base" name_original="bases" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="rounded" value_original="rounded" />
        <character is_modifier="true" name="shape" src="d0_s9" value="subcordate" value_original="subcordate" />
        <character is_modifier="false" name="width" notes="" src="d0_s9" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o4529" name="margin" name_original="margins" src="d0_s9" type="structure" />
      <biological_entity id="o4530" name="cilium" name_original="cilia" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o4529" id="r727" name="with" negation="false" src="d0_s9" to="o4530" />
    </statement>
    <statement id="d0_s10">
      <text>Primary panicles 3-6 cm long, 2-4 cm wide, exserted;</text>
      <biological_entity constraint="primary" id="o4531" name="panicle" name_original="panicles" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s10" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s10" to="4" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>branches spreading at maturity;</text>
      <biological_entity id="o4532" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>pedicels somewhat divergent.</text>
      <biological_entity id="o4533" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="somewhat" name="arrangement" src="d0_s12" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 3.2-4.4 mm long, 1.3-1.6 mm wide, narrowly obovoid-ellipsoid, papillose-hirsute, attenuate to the purplish bases.</text>
      <biological_entity id="o4534" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s13" to="4.4" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s13" to="1.6" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s13" value="obovoid-ellipsoid" value_original="obovoid-ellipsoid" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="papillose-hirsute" value_original="papillose-hirsute" />
        <character constraint="to bases" constraintid="o4535" is_modifier="false" name="shape" src="d0_s13" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o4535" name="base" name_original="bases" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Lower glumes about 1/2 as long as the spikelets, narrowly triangular, subadjacent to the upper glumes, not encircling the pedicels;</text>
      <biological_entity constraint="lower" id="o4536" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character constraint="as-long-as spikelets" constraintid="o4537" name="quantity" src="d0_s14" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s14" value="triangular" value_original="triangular" />
      </biological_entity>
      <biological_entity id="o4537" name="spikelet" name_original="spikelets" src="d0_s14" type="structure" />
      <biological_entity constraint="upper" id="o4538" name="glume" name_original="glumes" src="d0_s14" type="structure" />
      <biological_entity id="o4539" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" />
      <relation from="o4536" id="r728" name="to" negation="false" src="d0_s14" to="o4538" />
      <relation from="o4536" id="r729" name="encircling the" negation="true" src="d0_s14" to="o4539" />
    </statement>
    <statement id="d0_s15">
      <text>upper glumes about 0.3 mm shorter than the upper florets;</text>
      <biological_entity constraint="upper" id="o4540" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="0.3" value_original="0.3" />
        <character constraint="than the upper florets" constraintid="o4541" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="upper" id="o4541" name="floret" name_original="florets" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o4542" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s16" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper florets with pointed, minutely puberulent apices.</text>
      <biological_entity id="o4544" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="true" name="shape" src="d0_s17" value="pointed" value_original="pointed" />
        <character is_modifier="true" modifier="minutely" name="pubescence" src="d0_s17" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o4543" id="r730" name="with" negation="false" src="d0_s17" to="o4544" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 18.</text>
      <biological_entity constraint="upper" id="o4543" name="floret" name_original="florets" src="d0_s17" type="structure" />
      <biological_entity constraint="2n" id="o4545" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dichanthelium pedicellatum grows on limestone outcroppings and in dry, open oak woodlands. Its range extends from Texas into Mexico and Guatemala. Primary panicles develop from late March into June (and sometimes from late August to November) and are open-pollinated; secondary panicles develop from May into fall and are at least partly cleistogamous.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>