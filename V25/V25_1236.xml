<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">460</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Panicum</taxon_name>
    <taxon_name authority="Bernh. ex Trin." date="unknown" rank="species">philadelphicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">philadelphicum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus panicum;section panicum;species philadelphicum;subspecies philadelphicum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">philadelphicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">tuckermanii</taxon_name>
    <taxon_hierarchy>genus panicum;species philadelphicum;variety tuckermanii</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Panic de philadelphie</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants often slender, pilose, yellowish-green.</text>
      <biological_entity id="o16355" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="often" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="yellowish-green" value_original="yellowish-green" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms erect or decumbent.</text>
      <biological_entity id="o16356" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Blades 2-6 mm wide, often erect, those of the flag leaves usually less than 1/2 as long as the panicles.</text>
      <biological_entity id="o16357" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" constraint="as-long-as panicles" constraintid="o16360" from="0" name="quantity" src="d0_s2" to="1/2" />
      </biological_entity>
      <biological_entity id="o16358" name="flag" name_original="flag" src="d0_s2" type="structure" />
      <biological_entity id="o16359" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o16360" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
      <relation from="o16357" id="r2738" name="part_of" negation="false" src="d0_s2" to="o16358" />
      <relation from="o16357" id="r2739" name="part_of" negation="false" src="d0_s2" to="o16359" />
    </statement>
    <statement id="d0_s3">
      <text>Secondary panicle branches usually appressed;</text>
      <biological_entity constraint="panicle" id="o16361" name="branch" name_original="branches" src="d0_s3" type="structure" constraint_original="secondary panicle">
        <character is_modifier="false" modifier="usually" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>pedicels usually short, appressed.</text>
      <biological_entity id="o16362" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 1.4-2.1 mm, ovoid-ellipsoid, pale green to slightly reddish.</text>
      <biological_entity id="o16363" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s5" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovoid-ellipsoid" value_original="ovoid-ellipsoid" />
        <character char_type="range_value" from="pale green" name="coloration" src="d0_s5" to="slightly reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Upper glumes and lower lemmas hooked over the upper florets;</text>
      <biological_entity constraint="upper" id="o16364" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character constraint="over upper florets" constraintid="o16366" is_modifier="false" name="shape" src="d0_s6" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o16365" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character constraint="over upper florets" constraintid="o16366" is_modifier="false" name="shape" src="d0_s6" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity constraint="upper" id="o16366" name="floret" name_original="florets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>mature upper florets more than 1/2 as wide as long, shiny, blackish, with several pale veins.</text>
      <biological_entity constraint="upper" id="o16367" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="mature" value_original="mature" />
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character name="quantity" src="d0_s7" value="/2" value_original="/2" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="long" value_original="long" />
        <character is_modifier="false" name="reflectance" src="d0_s7" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blackish" value_original="blackish" />
      </biological_entity>
      <biological_entity id="o16368" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="several" value_original="several" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="pale" value_original="pale" />
      </biological_entity>
      <relation from="o16367" id="r2740" name="with" negation="false" src="d0_s7" to="o16368" />
    </statement>
  </description>
  <discussion>Panicum philadelphicum subsp. philadelphicum grows in meadows, open woods, sand, and on receding shores.</discussion>
  <discussion>Plants with decumbent culms, glabrous pulvini, flexuous pedicels without hairs over 0.2 mm long, spikelets 1.4-1.7 mm long, and the mature floret not disarticulating have been called Panicum tuckermanii Fernald. They are often fairly distinct on receding lake shores in New England and the Great Lakes area (Darbyshire and Cayoutte 1995), but intergrade with subsp. philadelphicum elsewhere.</discussion>
  
</bio:treatment>