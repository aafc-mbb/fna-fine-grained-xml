<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Rich." date="unknown" rank="genus">PENNISETUM</taxon_name>
    <taxon_name authority="(Nees) Trin" date="unknown" rank="species">nervosum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus pennisetum;species nervosum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Bentspike fountaingrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o11412" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 1.5-2 (4) m, decumbent, geniculate, branching;</text>
      <biological_entity id="o11413" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="m" value="4" value_original="4" />
        <character char_type="range_value" from="1.5" from_unit="m" name="some_measurement" src="d0_s2" to="2" to_unit="m" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o11414" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves green;</text>
      <biological_entity id="o11415" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths glabrous;</text>
      <biological_entity id="o11416" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.5-1.5 mm;</text>
      <biological_entity id="o11417" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 23-40 cm long, 7-12 mm wide, flat, glabrous.</text>
      <biological_entity id="o11418" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="23" from_unit="cm" name="length" src="d0_s7" to="40" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s7" to="12" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles terminal, 15-22 cm long, 15-21 mm wide, fully exerted from the sheaths, flexible, drooping, green;</text>
      <biological_entity id="o11419" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s8" to="22" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s8" to="21" to_unit="mm" />
        <character is_modifier="false" modifier="fully" name="fragility" src="d0_s8" value="pliable" value_original="flexible" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="drooping" value_original="drooping" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o11420" name="sheath" name_original="sheaths" src="d0_s8" type="structure" />
      <relation from="o11419" id="r1865" modifier="fully" name="exerted from the" negation="false" src="d0_s8" to="o11420" />
    </statement>
    <statement id="d0_s9">
      <text>rachises terete, puberulent.</text>
    </statement>
    <statement id="d0_s10">
      <text>Fascicles 24-49 per cm;</text>
      <biological_entity id="o11421" name="rachis" name_original="rachises" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="terete" value_original="terete" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="fascicles" value_original="fascicles" />
        <character char_type="range_value" from="24" name="quantity" src="d0_s10" to="49" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>fascicle axes 0.4-0.5 mm, with 1 spikelet;</text>
      <biological_entity id="o11423" name="spikelet" name_original="spikelet" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <relation from="o11422" id="r1866" name="with" negation="false" src="d0_s11" to="o11423" />
    </statement>
    <statement id="d0_s12">
      <text>most bristles only slightly longer than the spikelets;</text>
      <biological_entity constraint="fascicle" id="o11422" name="axis" name_original="axes" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11424" name="bristle" name_original="bristles" src="d0_s12" type="structure">
        <character constraint="than the spikelets" constraintid="o11425" is_modifier="false" name="length_or_size" src="d0_s12" value="only slightly longer" value_original="only slightly longer" />
      </biological_entity>
      <biological_entity id="o11425" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>outer bristles 27-41, 2.4-8.5 mm;</text>
      <biological_entity constraint="outer" id="o11426" name="bristle" name_original="bristles" src="d0_s13" type="structure">
        <character char_type="range_value" from="27" name="quantity" src="d0_s13" to="41" />
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s13" to="8.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>inner bristles 3-5, 6.2-11 mm, scabrous;</text>
      <biological_entity constraint="inner" id="o11427" name="bristle" name_original="bristles" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s14" to="5" />
        <character char_type="range_value" from="6.2" from_unit="mm" name="some_measurement" src="d0_s14" to="11" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s14" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>primary bristles 7.6-11.8 mm, not noticeably longer than the other bristles, scabrous.</text>
      <biological_entity constraint="primary" id="o11428" name="bristle" name_original="bristles" src="d0_s15" type="structure">
        <character char_type="range_value" from="7.6" from_unit="mm" name="some_measurement" src="d0_s15" to="11.8" to_unit="mm" />
        <character constraint="than the other bristles" constraintid="o11429" is_modifier="false" name="length_or_size" src="d0_s15" value="not noticeably longer" value_original="not noticeably longer" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o11429" name="bristle" name_original="bristles" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>Spikelets 5.2-6.6 mm, sessile;</text>
      <biological_entity id="o11430" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character char_type="range_value" from="5.2" from_unit="mm" name="some_measurement" src="d0_s16" to="6.6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower glumes 1.7-2.9 mm, 1-veined;</text>
      <biological_entity constraint="lower" id="o11431" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s17" to="2.9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper glumes 4.5-6.3 mm, about as long as the spikelets, (7) 9-veined;</text>
      <biological_entity constraint="upper" id="o11432" name="glume" name_original="glumes" src="d0_s18" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s18" to="6.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s18" value="(7)9-veined" value_original="(7)9-veined" />
      </biological_entity>
      <biological_entity id="o11433" name="spikelet" name_original="spikelets" src="d0_s18" type="structure" />
      <relation from="o11432" id="r1867" name="as long as" negation="false" src="d0_s18" to="o11433" />
    </statement>
    <statement id="d0_s19">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o11434" name="floret" name_original="florets" src="d0_s19" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s19" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>lower lemmas 4.8-6 mm, 7-veined, acuminate to attenuate, midvein excurrent for 0-0.6 mm;</text>
      <biological_entity constraint="lower" id="o11435" name="lemma" name_original="lemmas" src="d0_s20" type="structure">
        <character char_type="range_value" from="4.8" from_unit="mm" name="some_measurement" src="d0_s20" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s20" value="7-veined" value_original="7-veined" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s20" to="attenuate" />
      </biological_entity>
      <biological_entity id="o11436" name="midvein" name_original="midvein" src="d0_s20" type="structure">
        <character constraint="for 0-0.6 mm" is_modifier="false" name="architecture" src="d0_s20" value="excurrent" value_original="excurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>lower paleas absent;</text>
      <biological_entity constraint="lower" id="o11437" name="palea" name_original="paleas" src="d0_s21" type="structure">
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>upper lemmas 4.9-6 mm, 5-veined, acuminate to attenuate, midvein excurrent for 0-0.6 mm;</text>
      <biological_entity constraint="upper" id="o11438" name="lemma" name_original="lemmas" src="d0_s22" type="structure">
        <character char_type="range_value" from="4.9" from_unit="mm" name="some_measurement" src="d0_s22" to="6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="5-veined" value_original="5-veined" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s22" to="attenuate" />
      </biological_entity>
      <biological_entity id="o11439" name="midvein" name_original="midvein" src="d0_s22" type="structure">
        <character constraint="for 0-0.6 mm" is_modifier="false" name="architecture" src="d0_s22" value="excurrent" value_original="excurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>anthers 1.3-1.7 mm. 2n = 36.</text>
      <biological_entity id="o11440" name="anther" name_original="anthers" src="d0_s23" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s23" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11441" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Pennisetum nervosum is native to South America. It has been introduced into the Flora region, being known from populations adjacent to the Rio Grande River in Cameron and Hidalgo counties, Texas, and San Diego County, California.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>