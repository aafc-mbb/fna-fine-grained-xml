<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">61</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Roem. &amp; Schult." date="unknown" rank="genus">TRIPOGON</taxon_name>
    <taxon_name authority="(Nees) Ekman" date="unknown" rank="species">spicatus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus tripogon;species spicatus</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">American tripogon</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o27398" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (4.5) 6-34 cm;</text>
      <biological_entity id="o27399" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="4.5" value_original="4.5" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="34" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes 2-3, glabrous.</text>
      <biological_entity id="o27400" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s3" to="3" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o27401" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o27402" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths mostly glabrous, but with tufts of hairs flanking the collar;</text>
      <biological_entity id="o27403" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o27404" name="tuft" name_original="tufts" src="d0_s5" type="structure" />
      <biological_entity id="o27405" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <biological_entity id="o27406" name="collar" name_original="collar" src="d0_s5" type="structure" />
      <relation from="o27403" id="r4669" name="with" negation="false" src="d0_s5" to="o27404" />
      <relation from="o27404" id="r4670" name="part_of" negation="false" src="d0_s5" to="o27405" />
      <relation from="o27404" id="r4671" name="flanking the" negation="false" src="d0_s5" to="o27406" />
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.2-0.3 mm, truncate;</text>
      <biological_entity id="o27407" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 1.9-10 cm long, 0.2-1.1 mm wide, glabrous or the adaxial surfaces and margins sparsely pubescent.</text>
      <biological_entity id="o27408" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.9" from_unit="cm" name="length" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s7" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27409" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o27410" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences (1.5) 4-10 cm long, 1.5-3.5 mm wide, with (6) 13-22 spikelets;</text>
      <biological_entity id="o27411" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s8" to="10" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27412" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="6" value_original="6" />
        <character char_type="range_value" from="13" is_modifier="true" name="quantity" src="d0_s8" to="22" />
      </biological_entity>
      <relation from="o27411" id="r4672" name="with" negation="false" src="d0_s8" to="o27412" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels 0-0.5 mm, glabrous.</text>
      <biological_entity id="o27413" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 4.5-12 mm long, 1-1.3 mm wide, with 5-14 florets;</text>
      <biological_entity id="o27414" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27415" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="14" />
      </biological_entity>
      <relation from="o27414" id="r4673" name="with" negation="false" src="d0_s10" to="o27415" />
    </statement>
    <statement id="d0_s11">
      <text>rachilla segments glabrous except for an apical tuft of hairs.</text>
      <biological_entity constraint="rachilla" id="o27416" name="segment" name_original="segments" src="d0_s11" type="structure">
        <character constraint="except-for apical tuft" constraintid="o27417" is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="apical" id="o27417" name="tuft" name_original="tuft" src="d0_s11" type="structure" />
      <biological_entity id="o27418" name="hair" name_original="hairs" src="d0_s11" type="structure" />
      <relation from="o27417" id="r4674" name="part_of" negation="false" src="d0_s11" to="o27418" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes unequal, exceeded by the basal florets;</text>
      <biological_entity id="o27419" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
      </biological_entity>
      <biological_entity constraint="basal" id="o27420" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <relation from="o27419" id="r4675" name="exceeded by the" negation="false" src="d0_s12" to="o27420" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes (1.2) 1.5-2.4 mm, glabrous, 1-veined, scabridulous over the veins;</text>
      <biological_entity constraint="lower" id="o27421" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="1.2" value_original="1.2" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-veined" value_original="1-veined" />
        <character constraint="over veins" constraintid="o27422" is_modifier="false" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o27422" name="vein" name_original="veins" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 1.9-2.6 mm, glabrous, 1-veined;</text>
      <biological_entity constraint="upper" id="o27423" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s14" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lowest lemmas 2.3-3.1 mm, 3-veined, apical sinuses 0.1-0.3 mm deep;</text>
      <biological_entity constraint="lowest" id="o27424" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s15" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity constraint="apical" id="o27425" name="sinuse" name_original="sinuses" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s15" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="depth" src="d0_s15" value="deep" value_original="deep" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>awns 0.2-0.9 mm, straight;</text>
      <biological_entity id="o27426" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s16" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s16" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>paleas 1.6-2.4 mm, glabrous on the back and minutely pubescent on the margins;</text>
      <biological_entity id="o27427" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s17" to="2.4" to_unit="mm" />
        <character constraint="on back" constraintid="o27428" is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character constraint="on margins" constraintid="o27429" is_modifier="false" modifier="minutely" name="pubescence" notes="" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o27428" name="back" name_original="back" src="d0_s17" type="structure" />
      <biological_entity id="o27429" name="margin" name_original="margins" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>anthers 3, 0.3-0.4 mm, yellow to purple.</text>
      <biological_entity id="o27430" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s18" to="0.4" to_unit="mm" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s18" to="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Caryopses 1-1.5 mm, reddish-brown.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 20.</text>
      <biological_entity id="o27431" name="caryopsis" name_original="caryopses" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s19" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27432" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tripogon spicatus grows in shallow rocky soils, usually on granite outcroppings, occasionally on lime¬stone. The flowering period, April-July(October, November), apparently depends on rainfall. Its range includes the West Indies, Mexico, and South America, in addition to central Texas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>