<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">249</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Linda Bea Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">SPARTINA</taxon_name>
    <taxon_name authority="A. A. Eaton" date="unknown" rank="species">×caespitosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus spartina;species ×caespitosa</taxon_hierarchy>
  </taxon_identification>
  <number>12</number>
  <other_name type="common_name">Mixed cordgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rhizomatous or not;</text>
      <biological_entity id="o12803" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="architecture" src="d0_s0" value="not" value_original="not" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes, when present, thick, usually purplish-brown, scales closely imbricate.</text>
      <biological_entity id="o12804" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="when present" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="purplish-brown" value_original="purplish-brown" />
      </biological_entity>
      <biological_entity id="o12805" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s1" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms to 120 cm tall, 1-3 mm thick, indurate, solitary or in small, dense clumps.</text>
      <biological_entity id="o12806" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="height" src="d0_s2" to="120" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s2" to="3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s2" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="in small , dense clumps" />
      </biological_entity>
      <biological_entity id="o12807" name="clump" name_original="clumps" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
        <character is_modifier="true" name="density" src="d0_s2" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o12806" id="r2101" name="in" negation="false" src="d0_s2" to="o12807" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths mostly glabrous, throats glabrous or short-pilose;</text>
      <biological_entity id="o12808" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o12809" name="throat" name_original="throats" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="short-pilose" value_original="short-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.5-1 mm;</text>
      <biological_entity id="o12810" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 8-56 cm long, 2-6 (7) mm wide, usually involute, abaxial surfaces glabrous, adaxial surfaces glabrous or scabrous, margins strongly scabrous, blade of the second leaf below the panicles 8-56 cm long, 2-5 (7) mm wide.</text>
      <biological_entity id="o12811" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s5" to="56" to_unit="cm" />
        <character name="width" src="d0_s5" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o12812" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o12813" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12814" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="strongly" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12815" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <biological_entity id="o12816" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
      <biological_entity id="o12817" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s5" to="56" to_unit="cm" />
        <character name="width" notes="alterIDs:o12817" src="d0_s5" unit="mm" value="7" value_original="7" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" notes="alterIDs:o12817" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o12815" id="r2102" name="part_of" negation="false" src="d0_s5" to="o12816" />
      <relation from="o12815" id="r2103" name="below" negation="false" src="d0_s5" to="o12817" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles 9-20 cm, not smoothly cylindrical, with 3-9 branches;</text>
      <biological_entity id="o12818" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s6" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="not smoothly" name="shape" src="d0_s6" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
      <biological_entity id="o12819" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="9" />
      </biological_entity>
      <relation from="o12818" id="r2104" name="with" negation="false" src="d0_s6" to="o12819" />
    </statement>
    <statement id="d0_s7">
      <text>branches 3-9 cm, appressed or spreading, with 20-50 spikelets.</text>
      <biological_entity id="o12820" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="9" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o12821" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s7" to="50" />
      </biological_entity>
      <relation from="o12820" id="r2105" name="with" negation="false" src="d0_s7" to="o12821" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 10-17 mm, lanceolate to ovatelanceolate.</text>
      <biological_entity id="o12822" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s8" to="17" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s8" to="ovatelanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Glumes glabrous or sparsely hispidulous, keels glabrous, hispid in whole or in part, or ciliate;</text>
      <biological_entity id="o12823" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o12824" name="keel" name_original="keels" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character constraint="in whole or in part" constraintid="o12825" is_modifier="false" name="pubescence" src="d0_s9" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" notes="" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o12825" name="part" name_original="part" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 4-9 mm, acuminate or awned;</text>
      <biological_entity constraint="lower" id="o12826" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="9" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 10-17 mm, exceeding the florets, keels hispid, lateral-veins prominent, 1 on each side of the keel or 2-3 on 1 side of the keel, apices acuminate or awned;</text>
      <biological_entity constraint="upper" id="o12827" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s11" to="17" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12828" name="floret" name_original="florets" src="d0_s11" type="structure" />
      <biological_entity id="o12829" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o12830" name="lateral-vein" name_original="lateral-veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s11" value="prominent" value_original="prominent" />
        <character constraint="on side" constraintid="o12831" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o12831" name="side" name_original="side" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="of " constraintid="o12835" from="2" name="quantity" src="d0_s11" to="3" />
      </biological_entity>
      <biological_entity id="o12832" name="keel" name_original="keel" src="d0_s11" type="structure" />
      <biological_entity id="o12833" name="side" name_original="side" src="d0_s11" type="structure" />
      <biological_entity id="o12834" name="keel" name_original="keel" src="d0_s11" type="structure" />
      <biological_entity id="o12835" name="side" name_original="side" src="d0_s11" type="structure" />
      <biological_entity id="o12836" name="keel" name_original="keel" src="d0_s11" type="structure" />
      <biological_entity id="o12837" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <relation from="o12827" id="r2106" name="exceeding the" negation="false" src="d0_s11" to="o12828" />
      <relation from="o12831" id="r2107" name="part_of" negation="false" src="d0_s11" to="o12832" />
      <relation from="o12831" id="r2108" name="part_of" negation="false" src="d0_s11" to="o12833" />
      <relation from="o12835" id="r2109" name="part_of" negation="false" src="d0_s11" to="o12836" />
    </statement>
    <statement id="d0_s12">
      <text>lemmas glabrous or sparsely hispidulous, apices obtuse, rounded, obscurely lobed, or apiculate;</text>
      <biological_entity id="o12838" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o12839" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s12" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" modifier="obscurely" name="shape" src="d0_s12" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s12" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 3-6 mm, poorly filled, indehiscent.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 60, 60+2.</text>
      <biological_entity id="o12840" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
        <character is_modifier="false" modifier="poorly" name="dehiscence" src="d0_s13" value="indehiscent" value_original="indehiscent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12841" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="60" value_original="60" />
        <character char_type="range_value" from="60" name="quantity" src="d0_s14" upper_restricted="false" />
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Spartina ×caespitosa is found in disturbed areas of the drier portions of salt and brackish marshes, at some distance above the intertidal zone. It occurs sporadically along the coast from Maine to Maryland, a region where its putative parents, S. pectinata and S. patens, are sympatric. None of the  populations  Mobberley (1956) examined was growing in undisturbed land.</discussion>
  <discussion>Mobberley's (1956) investigations led him to conclude that the populations of S. ×caespitosa are polythetic in origin. Part of the evidence for his conclusion was the variability he observed. It is this variability that makes it necessary to bring out the hybrid at several locations in the key. Its distribution is, however, very limited, a fact that may be more useful for identification than any of the morphological characteristics examined.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Maine;Md.;N.J.;Mass.;N.B.;N.S.;P.E.I.;Va.;Conn.;N.Y.;Del.;N.H.;R.I.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>