<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">617</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Andersson" date="unknown" rank="genus">MISCANTHUS</taxon_name>
    <taxon_name authority="Andersson" date="unknown" rank="species">sinensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus miscanthus;species sinensis</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Eulalia</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, forming large clumps, with short, thick rhizomes.</text>
      <biological_entity id="o15371" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15372" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o15373" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character is_modifier="true" name="width" src="d0_s0" value="thick" value_original="thick" />
      </biological_entity>
      <relation from="o15371" id="r2578" name="forming" negation="false" src="d0_s0" to="o15372" />
      <relation from="o15371" id="r2579" name="with" negation="false" src="d0_s0" to="o15373" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 60-200 cm tall, 3-7 mm thick below.</text>
      <biological_entity id="o15374" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="height" src="d0_s1" to="200" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" modifier="below" name="thickness" src="d0_s1" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves predominantly basal;</text>
      <biological_entity id="o15375" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o15376" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="predominantly" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths mostly glabrous, throats pilose;</text>
      <biological_entity id="o15377" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15378" name="throat" name_original="throats" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-2 mm;</text>
      <biological_entity id="o15379" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 20-70 cm long, 6-20 mm wide, midveins conspicuous abaxially, 1-2 mm wide, whitish.</text>
      <biological_entity id="o15380" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s5" to="70" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s5" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15381" name="midvein" name_original="midveins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s5" value="conspicuous" value_original="conspicuous" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 15-25 cm long, 8-28 cm wide, dense to loose, usually with more than 15 branches;</text>
      <biological_entity id="o15382" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s6" to="25" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s6" to="28" to_unit="cm" />
        <character is_modifier="false" name="density" src="d0_s6" value="dense" value_original="dense" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o15383" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s6" upper_restricted="false" />
      </biological_entity>
      <relation from="o15382" id="r2580" modifier="usually" name="with" negation="false" src="d0_s6" to="o15383" />
    </statement>
    <statement id="d0_s7">
      <text>rachises 6-15 cm, 1/3 – 2/3 as long as the inflorescences;</text>
      <biological_entity id="o15384" name="rachis" name_original="rachises" src="d0_s7" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" constraint="as-long-as inflorescences" constraintid="o15385" from="1/3" name="quantity" src="d0_s7" to="2/3" />
      </biological_entity>
      <biological_entity id="o15385" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>branches 8-15 (30) cm long, about 10 mm wide, sometimes branched at the base;</text>
      <biological_entity id="o15386" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="length" src="d0_s8" unit="cm" value="30" value_original="30" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" src="d0_s8" to="15" to_unit="cm" />
        <character name="width" src="d0_s8" unit="mm" value="10" value_original="10" />
        <character constraint="at base" constraintid="o15387" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s8" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o15387" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>internodes 4-8 mm, glabrous.</text>
      <biological_entity id="o15388" name="internode" name_original="internodes" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Shorter pedicels 1.5-2.5 mm;</text>
      <biological_entity constraint="shorter" id="o15389" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>longer pedicels 3.5-6 mm, slightly recurved at maturity.</text>
      <biological_entity constraint="longer" id="o15390" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 3.5-7 mm, lanceolate to lanceovate;</text>
      <biological_entity id="o15391" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s12" to="7" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="lanceovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>callus hairs 6-12 mm, to twice as long as the spikelets, white, stramineous to reddish.</text>
      <biological_entity constraint="callus" id="o15392" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s13" to="12" to_unit="mm" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s13" value="white" value_original="white" />
        <character char_type="range_value" from="stramineous" name="coloration" src="d0_s13" to="reddish" />
      </biological_entity>
      <biological_entity id="o15393" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <relation from="o15392" id="r2581" name="to" negation="false" src="d0_s13" to="o15393" />
    </statement>
    <statement id="d0_s14">
      <text>Glumes subequal;</text>
      <biological_entity id="o15394" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower glumes 3-veined, ciliolate on the margins;</text>
      <biological_entity constraint="lower" id="o15395" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-veined" value_original="3-veined" />
        <character constraint="on margins" constraintid="o15396" is_modifier="false" name="pubescence" src="d0_s15" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
      <biological_entity id="o15396" name="margin" name_original="margins" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>upper glumes 1-veined;</text>
      <biological_entity constraint="upper" id="o15397" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>awns of upper lemmas 6-12 mm, geniculate below.</text>
      <biological_entity constraint="upper" id="o15399" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <relation from="o15398" id="r2582" name="part_of" negation="false" src="d0_s17" to="o15399" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 38, 40, and dysploids from 35-42.</text>
      <biological_entity id="o15398" name="awn" name_original="awns" src="d0_s17" type="structure" constraint="lemma" constraint_original="lemma; lemma">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s17" to="12" to_unit="mm" />
        <character is_modifier="false" modifier="below" name="shape" src="d0_s17" value="geniculate" value_original="geniculate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15400" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" unit=",,and dysploids" value="38" value_original="38" />
        <character name="quantity" src="d0_s18" unit=",,and dysploids" value="40" value_original="40" />
        <character char_type="range_value" from="35" name="quantity" src="d0_s18" to="42" unit=",,and dysploids" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Miscanthus sinensis is native to southeastern Asia. It is frequently cultivated in the United States and southern Canada, and is now established in some parts of the United States. Approximately 40 forms and cultivars are available, some having white-striped leaves, others differently colored callus hairs and, consequently, differently colored panicles.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Del.;D.C.;W.Va.;Fla.;Mass.;R.I.;La.;Tenn.;Pa.;Va.;Colo.;Ont.;Calif.;Ala.;Ill.;Ga.;Md.;Ohio;Mo.;Mich.;Miss.;Ky.;N.C.;S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>