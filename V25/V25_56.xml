<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">36</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Roem. &amp; Schult." date="unknown" rank="genus">TRIDENS</taxon_name>
    <taxon_name authority="(L.H. Dewey) Nash" date="unknown" rank="species">buckleyanus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus tridens;species buckleyanus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>7</number>
  <other_name type="common_name">Buckley's tridens</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose.</text>
      <biological_entity id="o244" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 30-60 cm, erect;</text>
      <biological_entity id="o245" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>lower nodes sometimes hispid.</text>
      <biological_entity constraint="lower" id="o246" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s2" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths scabridulous, rounded;</text>
      <biological_entity id="o247" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.4-1 mm, membranous, ciliate;</text>
      <biological_entity id="o248" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-4 mm wide, flat, apices attenuate.</text>
      <biological_entity id="o249" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o250" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 10-28 cm long, 1-6 cm wide;</text>
      <biological_entity id="o251" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="28" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches (2) 4-13 cm, widely-spaced, ascending to spreading;</text>
      <biological_entity id="o252" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="13" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="widely-spaced" value_original="widely-spaced" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s7" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels 1-2 mm.</text>
      <biological_entity id="o253" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 7-10 mm, pale to dark purple, with 2-5 florets.</text>
      <biological_entity id="o254" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character char_type="range_value" from="pale" name="coloration" src="d0_s9" to="dark purple" />
      </biological_entity>
      <biological_entity id="o255" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <relation from="o254" id="r35" name="with" negation="false" src="d0_s9" to="o255" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes 1-veined;</text>
      <biological_entity id="o256" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 4-5 (6) mm;</text>
      <biological_entity constraint="lower" id="o257" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes slightly shorter;</text>
      <biological_entity constraint="upper" id="o258" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 4-6 mm, midveins and back pubescent below midlength, lateral-veins pubescent to well above midlength, midveins occasionally excurrent, lateral-veins usually ending before the distal margins, rarely excurrent;</text>
      <biological_entity id="o259" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o260" name="midvein" name_original="midveins" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="back" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o261" name="lateral-vein" name_original="lateral-veins" src="d0_s13" type="structure">
        <character constraint="well above midlength" constraintid="o262" is_modifier="false" modifier="below midlength" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o262" name="midlength" name_original="midlength" src="d0_s13" type="structure" />
      <biological_entity id="o263" name="midvein" name_original="midveins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s13" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o264" name="lateral-vein" name_original="lateral-veins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s13" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o265" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <relation from="o264" id="r36" name="ending before the" negation="false" src="d0_s13" to="o265" />
    </statement>
    <statement id="d0_s14">
      <text>paleas 3.5-4 mm, veins pubescent basally;</text>
      <biological_entity id="o266" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o267" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="basally" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1.5-2 mm.</text>
      <biological_entity id="o268" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses about 2.5 mm. 2n = 32, but this may erroneous (Gould 1975).</text>
      <biological_entity id="o269" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o270" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" unit=",but this may erroneous" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tridens buckleyanus is endemic to the southeastern portion of the Edwards Plateau, Texas. It grows on rocky slopes along shaded stream banks and the borders of woodlands.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>