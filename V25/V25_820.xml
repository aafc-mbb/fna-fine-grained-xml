<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">224</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="genus">ENTEROPOGON</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus enteropogon</taxon_hierarchy>
  </taxon_identification>
  <number>17.37</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, sometimes stoloniferous or rhizomatous.</text>
      <biological_entity id="o12842" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms to 120 cm, not woody.</text>
      <biological_entity id="o12843" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open;</text>
      <biological_entity id="o12844" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, ciliate;</text>
      <biological_entity id="o12845" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades flat.</text>
      <biological_entity id="o12846" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, panicles of 1-20 non-disarticulating, spikelike branches, exceeding the upper leaves;</text>
      <biological_entity id="o12847" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o12848" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o12849" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="20" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="non-disarticulating" value_original="non-disarticulating" />
        <character is_modifier="true" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity constraint="upper" id="o12850" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o12848" id="r2110" name="consist_of" negation="false" src="d0_s6" to="o12849" />
      <relation from="o12848" id="r2111" name="exceeding the" negation="false" src="d0_s6" to="o12850" />
    </statement>
    <statement id="d0_s7">
      <text>branches digitately or racemosely arranged, if racemose, the lower nodes usually with more than 1 branch.</text>
      <biological_entity id="o12851" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="racemosely" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity constraint="lower" id="o12852" name="node" name_original="nodes" src="d0_s7" type="structure" />
      <biological_entity id="o12853" name="branch" name_original="branch" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" upper_restricted="false" />
      </biological_entity>
      <relation from="o12852" id="r2112" name="with" negation="false" src="d0_s7" to="o12853" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets solitary, strongly imbricate, appressed to somewhat divergent, dorsally compressed, with 2-6 florets, lowest floret bisexual, elongate, remaining florets progressively reduced, usually the distal florets rudimentary and sterile, occasionally staminate;</text>
      <biological_entity id="o12854" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s8" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="somewhat" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o12855" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="6" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o12856" name="floret" name_original="floret" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
        <character is_modifier="false" modifier="progressively" name="size" src="d0_s8" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o12857" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <relation from="o12854" id="r2113" name="with" negation="false" src="d0_s8" to="o12855" />
      <relation from="o12856" id="r2114" name="remaining" negation="false" src="d0_s8" to="o12857" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation beneath the glumes.</text>
      <biological_entity constraint="distal" id="o12858" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s8" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o12859" name="glume" name_original="glumes" src="d0_s9" type="structure" />
      <relation from="o12858" id="r2115" name="beneath" negation="false" src="d0_s9" to="o12859" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes unequal, subulate to lanceolate, membranous;</text>
      <biological_entity id="o12860" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="subulate" name="shape" src="d0_s10" to="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes much shorter than the lowest florets, acute or shortly awned;</text>
      <biological_entity constraint="upper" id="o12861" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character constraint="than the lowest florets" constraintid="o12862" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="much shorter" value_original="much shorter" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o12862" name="floret" name_original="florets" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>calluses strigose;</text>
      <biological_entity id="o12863" name="callus" name_original="calluses" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lowest lemmas stiff, 3-veined, ridged over the midveins, apices acute or bidentate, usually awned from between the teeth, without lateral awns;</text>
      <biological_entity constraint="lowest" id="o12864" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s13" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
        <character constraint="over midveins" constraintid="o12865" is_modifier="false" name="shape" src="d0_s13" value="ridged" value_original="ridged" />
      </biological_entity>
      <biological_entity id="o12865" name="midvein" name_original="midveins" src="d0_s13" type="structure" />
      <biological_entity id="o12866" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s13" value="bidentate" value_original="bidentate" />
        <character constraint="from " constraintid="o12867" is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o12867" name="tooth" name_original="teeth" src="d0_s13" type="structure" />
      <biological_entity constraint="lateral" id="o12868" name="awn" name_original="awns" src="d0_s13" type="structure" />
      <relation from="o12866" id="r2116" name="without" negation="false" src="d0_s13" to="o12868" />
    </statement>
    <statement id="d0_s14">
      <text>paleas almost as long as the lemmas, 2-keeled and bidentate;</text>
      <biological_entity id="o12869" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" name="shape" src="d0_s14" value="bidentate" value_original="bidentate" />
      </biological_entity>
      <biological_entity id="o12870" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <relation from="o12869" id="r2117" name="as long as" negation="false" src="d0_s14" to="o12870" />
    </statement>
    <statement id="d0_s15">
      <text>distal lemmas awned;</text>
      <biological_entity constraint="distal" id="o12871" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lodicules 2;</text>
      <biological_entity id="o12872" name="lodicule" name_original="lodicules" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 1-3.</text>
      <biological_entity id="o12873" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s17" to="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses sulcate;</text>
      <biological_entity id="o12874" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s18" value="sulcate" value_original="sulcate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>embryos 1/4 - 1/3 (1/2) as long as the caryopses.</text>
      <biological_entity id="o12876" name="caryopsis" name_original="caryopses" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>x = 10.</text>
      <biological_entity id="o12875" name="embryo" name_original="embryos" src="d0_s19" type="structure">
        <character char_type="range_value" constraint="as-long-as caryopses" constraintid="o12876" from="1/4" name="quantity" src="d0_s19" to="1/31/2" />
      </biological_entity>
      <biological_entity constraint="x" id="o12877" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Enteropogon is a tropical genus of 17 species. Anderson (1974) included it in Chloris, but it is now usually recognized as a separate genus. It differs from Chloris in its dorsally compressed, indurate lemmas that are conspicuously ridged over the midvein. The caryopses also differ, being dorsally flattened with a shallow ventral groove in Enteropogon; in Chloris, the caryopses are basically triangular in cross section although there may be a shallow ventral groove. The embryos also tend to be shorter relative to the caryopses in Enteropogon than in Chloris, but there is some overlap.</discussion>
  <discussion>There is one species native to the Flora region; two others have been found at various locations but have not persisted.</discussion>
  <references>
    <reference>Anderson, D.E. 1974. Taxonomy of the genus Chloris (Gramineae). Brigham Young Univ. Sci. Bull., Biol. Ser. 19:1-133</reference>
    <reference>Jacobs, S.W.L. and J. Highet. 1988. Re-evaluation of the characters used to distinguish Enteropogon from Chloris (Poaceae), Telopea: 3:217-221</reference>
    <reference>Lazarides, M. 1972. A revision of Australian Chlorideae (Gramineae). Austral. J. Bot. (supp. 5):1-51</reference>
    <reference>Pohl, R.W. and G. Davidse. 1994. Enteropogon Nees. R 289 in G. Da.idse, M. Sousa S., and A.O. Chater (eds.). Flora Mesoamericana, vol. 6: Alismataceae a Cyperaceae. Universidad Nacional Autonoma de Mexico, Instituto de Biologia: Mexico, D.F., Mexico. 543 pp.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C.;Ariz.;Ala.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicles with 3-15 branches racemosely arranged; plants rhizomatous</description>
      <determination>1 Enteropogon chlorideus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicles with 1-10 branches in a single, digitate cluster; plants not rhizomatous, sometimes stoloniferous. 2. Panicle branches 6-11 cm long, erect to slightly diverging; spikelets with 5-6 florets, the distal 4-5 sterile; plants stoloniferous</description>
      <determination>2 Enteropogon prieurii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Panicle branches 7-25 cm long, divergent to drooping; spikelets with 2 florets, the distal floret sterile; plants not stoloniferous</description>
      <determination>3 Enteropogon dolichostachyus</determination>
    </key_statement>
  </key>
</bio:treatment>