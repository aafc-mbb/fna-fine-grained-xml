<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">203</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="genus">LYCURUS</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">phleoides</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus lycurus;species phleoides</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Common wolfstail</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 20-50 cm, erect to ascending, often geniculate.</text>
      <biological_entity id="o18929" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="50" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="ascending" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s0" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Ligules 1.5-3 mm, commonly acute to acuminate, with narrow trianfular lobes 1.5-3 (4) mm long extending from the sides of the sheaths;</text>
      <biological_entity id="o18930" name="ligule" name_original="ligules" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
        <character char_type="range_value" from="commonly acute" name="shape" src="d0_s1" to="acuminate" />
      </biological_entity>
      <biological_entity id="o18931" name="trianfular" name_original="trianfular" src="d0_s1" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
        <character name="length" src="d0_s1" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18932" name="lobe" name_original="lobes" src="d0_s1" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s1" value="narrow" value_original="narrow" />
        <character name="length" src="d0_s1" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18933" name="side" name_original="sides" src="d0_s1" type="structure" />
      <biological_entity id="o18934" name="sheath" name_original="sheaths" src="d0_s1" type="structure" />
      <relation from="o18930" id="r3193" name="with" negation="false" src="d0_s1" to="o18931" />
      <relation from="o18930" id="r3194" name="with" negation="false" src="d0_s1" to="o18932" />
      <relation from="o18930" id="r3195" name="extending from the" negation="false" src="d0_s1" to="o18933" />
      <relation from="o18930" id="r3196" name="extending from the" negation="false" src="d0_s1" to="o18934" />
    </statement>
    <statement id="d0_s2">
      <text>blades 4-8 cm long, 1-1.5 mm wide, acute or mucronate, midribs sometimes extending up to 3 mm as a short bristle.</text>
      <biological_entity id="o18935" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s2" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Similar to Lycurus setosus in inflorescence and aspikelet characters, except the upper glumes occasionally with a second shorter, more delicate awn.</text>
      <biological_entity id="o18937" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
      <biological_entity constraint="upper" id="o18938" name="glume" name_original="glumes" src="d0_s3" type="structure" />
      <biological_entity id="o18939" name="awn" name_original="awn" src="d0_s3" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="shorter" value_original="shorter" />
        <character is_modifier="true" name="fragility" src="d0_s3" value="delicate" value_original="delicate" />
      </biological_entity>
      <relation from="o18936" id="r3197" name="in" negation="false" src="d0_s3" to="o18937" />
      <relation from="o18938" id="r3198" name="with" negation="false" src="d0_s3" to="o18939" />
    </statement>
    <statement id="d0_s4">
      <text>2n = 40, ca. 40.</text>
      <biological_entity id="o18936" name="midrib" name_original="midribs" src="d0_s2" type="structure" />
      <biological_entity constraint="2n" id="o18940" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="40" value_original="40" />
        <character name="quantity" src="d0_s4" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Lycurus phleoides grows on rocky hills and open slopes, at elevations of 670-2600 m. It grows from the southwestern United States to southern Mexico, and in northern South America. It flowers from July-October.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Maine;Okla.;N.Mex.;Tex.;Ariz.;Colo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>