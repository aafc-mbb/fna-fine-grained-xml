<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">214</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Sw." date="unknown" rank="genus">CHLORIS</taxon_name>
    <taxon_name authority="Benth." date="unknown" rank="species">pectinata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus chloris;species pectinata</taxon_hierarchy>
  </taxon_identification>
  <number>12</number>
  <other_name type="common_name">Comb windmill-grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o19576" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-75 cm, erect, often branched above.</text>
      <biological_entity id="o19577" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="75" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o19578" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules membranous, ciliate;</text>
      <biological_entity id="o19579" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades to 15 cm long, 2-5 mm wide, sometimes with basal hairs, otherwise glabrous or scabrous.</text>
      <biological_entity id="o19580" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="otherwise" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o19581" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <relation from="o19580" id="r3301" modifier="sometimes" name="with" negation="false" src="d0_s4" to="o19581" />
    </statement>
    <statement id="d0_s5">
      <text>Panicles digitate, with 4-13 easily separable or evidently distinct branches;</text>
      <biological_entity id="o19582" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="digitate" value_original="digitate" />
      </biological_entity>
      <biological_entity id="o19583" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s5" to="13" />
        <character is_modifier="true" modifier="easily" name="fixation" src="d0_s5" value="separable" value_original="separable" />
        <character is_modifier="true" modifier="evidently" name="fusion" src="d0_s5" value="distinct" value_original="distinct" />
      </biological_entity>
      <relation from="o19582" id="r3302" name="with" negation="false" src="d0_s5" to="o19583" />
    </statement>
    <statement id="d0_s6">
      <text>branches 5-11 cm, initially erect, becoming divaricate, with 10-14 spikelets per cm.</text>
      <biological_entity id="o19584" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="11" to_unit="cm" />
        <character is_modifier="false" modifier="initially" name="orientation" src="d0_s6" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="becoming" name="arrangement" src="d0_s6" value="divaricate" value_original="divaricate" />
      </biological_entity>
      <biological_entity id="o19585" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s6" to="14" />
      </biological_entity>
      <biological_entity id="o19586" name="cm" name_original="cm" src="d0_s6" type="structure" />
      <relation from="o19584" id="r3303" name="with" negation="false" src="d0_s6" to="o19585" />
      <relation from="o19585" id="r3304" name="per" negation="false" src="d0_s6" to="o19586" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets pectinate, diverging at a wide angle from the branch axes, with 1 bisexual and 1 staminate floret.</text>
      <biological_entity id="o19587" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="pectinate" value_original="pectinate" />
        <character constraint="at angle" constraintid="o19588" is_modifier="false" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o19588" name="angle" name_original="angle" src="d0_s7" type="structure">
        <character is_modifier="true" name="width" src="d0_s7" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="branch" id="o19589" name="axis" name_original="axes" src="d0_s7" type="structure" />
      <biological_entity id="o19590" name="floret" name_original="floret" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <relation from="o19588" id="r3305" name="from" negation="false" src="d0_s7" to="o19589" />
      <relation from="o19587" id="r3306" name="with" negation="false" src="d0_s7" to="o19590" />
    </statement>
    <statement id="d0_s8">
      <text>Lower glumes 1.4-2.5 mm;</text>
      <biological_entity constraint="lower" id="o19591" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>upper glumes 2.9-4.3 mm;</text>
      <biological_entity constraint="upper" id="o19592" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s9" to="4.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lowest lemmas 3-6.2 mm long, 0.4-0.6 mm wide, linear to narrowly lanceolate, margins glabrous, scabrous, or with hairs less than 0.2 mm, lemma apices bilobed, lobes 0.5-1 mm, sometimes awned, central awns 4-37 mm, awns of lateral lobes, if present, less than 0.6 mm;</text>
      <biological_entity constraint="lowest" id="o19593" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="6.2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s10" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s10" to="narrowly lanceolate" />
      </biological_entity>
      <biological_entity id="o19594" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s10" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o19595" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o19596" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="bilobed" value_original="bilobed" />
      </biological_entity>
      <biological_entity id="o19597" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="central" id="o19598" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="37" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o19600" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <relation from="o19594" id="r3307" name="with" negation="false" src="d0_s10" to="o19595" />
      <relation from="o19599" id="r3308" name="part_of" negation="false" src="d0_s10" to="o19600" />
    </statement>
    <statement id="d0_s11">
      <text>second florets 1.7-2.9 mm long, 0.2-0.3 mm wide, laterally compressed, bilobed, lobes 1/3–1/2 as long as the lemmas, awned, awns 4-10 mm.</text>
      <biological_entity id="o19599" name="awn" name_original="awns" src="d0_s10" type="structure" constraint="lobe" constraint_original="lobe; lobe">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19601" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s11" to="2.9" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s11" to="0.3" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="bilobed" value_original="bilobed" />
      </biological_entity>
      <biological_entity id="o19602" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="as-long-as lemmas" constraintid="o19603" from="1/3" name="quantity" src="d0_s11" to="1/2" />
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o19603" name="lemma" name_original="lemmas" src="d0_s11" type="structure" />
      <biological_entity id="o19604" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s11" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Caryopses about 2.3 mm long, about 0.3 mm wide, narrowly ellipsoid, trigonous.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = unknown.</text>
      <biological_entity id="o19605" name="caryopsis" name_original="caryopses" src="d0_s12" type="structure">
        <character name="length" src="d0_s12" unit="mm" value="2.3" value_original="2.3" />
        <character name="width" src="d0_s12" unit="mm" value="0.3" value_original="0.3" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="trigonous" value_original="trigonous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19606" name="chromosome" name_original="" src="d0_s13" type="structure" />
    </statement>
  </description>
  <discussion>Chloris pectinata is an Australian species that was collected around woolen mills in South Carolina in the first half of the twentieth century.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>