<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">487</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="(Raf.) Pilg." date="unknown" rank="subgenus">Phanopyrum</taxon_name>
    <taxon_name authority="(Nash) C.C. Hsu" date="unknown" rank="section">Verrucosa</taxon_name>
    <taxon_name authority="Steud." date="unknown" rank="species">brachyanthum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus phanopyrum;section verrucosa;species brachyanthum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>34</number>
  <other_name type="common_name">Prairie panicgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>weak, ascending or spreading.</text>
      <biological_entity id="o23835" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="weak" value_original="weak" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms slender, wiry, glabrous, often with minute purple streaks and dots, ascending from a decumbent base, often branching extensively at the base and rooting at the lower nodes.</text>
      <biological_entity id="o23836" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="wiry" value_original="wiry" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration_or_relief" notes="" src="d0_s2" value="dots" value_original="dots" />
        <character constraint="from base" constraintid="o23838" is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character constraint="at base" constraintid="o23839" is_modifier="false" modifier="often" name="architecture" notes="" src="d0_s2" value="branching" value_original="branching" />
        <character constraint="at lower nodes" constraintid="o23840" is_modifier="false" name="architecture" notes="" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o23837" name="streak" name_original="streaks" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="minute" value_original="minute" />
        <character is_modifier="true" name="coloration_or_density" src="d0_s2" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o23838" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity id="o23839" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="lower" id="o23840" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <relation from="o23836" id="r4040" modifier="often" name="with" negation="false" src="d0_s2" to="o23837" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths usually shorter than the internodes, glabrous, margins short-ciliate;</text>
      <biological_entity id="o23841" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="than the internodes" constraintid="o23842" is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="usually shorter" value_original="usually shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23842" name="internode" name_original="internodes" src="d0_s3" type="structure" />
      <biological_entity id="o23843" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="short-ciliate" value_original="short-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules usually less than 0.3 mm, membranous, erose, ciliate;</text>
      <biological_entity id="o23844" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s4" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 4-15 cm long (rarely longer), 2-3 mm wide, flat or slightly involute, glabrous on both surfaces, margins scabridulous, especially towards the apices, bases narrowed.</text>
      <biological_entity id="o23845" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="involute" value_original="involute" />
        <character constraint="on surfaces" constraintid="o23846" is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23846" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o23847" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o23848" name="apex" name_original="apices" src="d0_s5" type="structure" />
      <biological_entity id="o23849" name="base" name_original="bases" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <relation from="o23847" id="r4041" modifier="especially" name="towards" negation="false" src="d0_s5" to="o23848" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles 4-17 cm, 1/2 to nearly as wide as long;</text>
      <biological_entity id="o23850" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s6" to="17" to_unit="cm" />
        <character name="quantity" src="d0_s6" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="nearly" name="length_or_size" src="d0_s6" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches few, capillary, ascending or spreading, scabridulous, with a few spikelets distally;</text>
      <biological_entity id="o23851" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="false" name="shape" src="d0_s7" value="capillary" value_original="capillary" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="relief" src="d0_s7" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o23852" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="few" value_original="few" />
      </biological_entity>
      <relation from="o23851" id="r4042" name="with" negation="false" src="d0_s7" to="o23852" />
    </statement>
    <statement id="d0_s8">
      <text>pedicels 0.5-10 mm.</text>
      <biological_entity id="o23853" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s8" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 3.2-4 mm long, about 1.5 mm wide, broadly ellipsoid or obovoid, tuberculate, hispid, faintly veined, acute or acuminate at the apices.</text>
      <biological_entity id="o23854" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character name="width" src="d0_s9" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s9" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="relief" src="d0_s9" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hispid" value_original="hispid" />
        <character is_modifier="false" modifier="faintly" name="architecture" src="d0_s9" value="veined" value_original="veined" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character constraint="at apices" constraintid="o23855" is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity id="o23855" name="apex" name_original="apices" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes usually less than 1 mm, obtuse or acute;</text>
      <biological_entity constraint="lower" id="o23856" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes and lower lemmas subequal, distinctly tuberculate, hispid, with stiff hairs arising from wartlike bases;</text>
      <biological_entity constraint="upper" id="o23857" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="distinctly" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o23858" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="distinctly" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o23859" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="fragility" src="d0_s11" value="stiff" value_original="stiff" />
        <character constraint="from bases" constraintid="o23860" is_modifier="false" name="orientation" src="d0_s11" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o23860" name="base" name_original="bases" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="wartlike" value_original="wartlike" />
      </biological_entity>
      <relation from="o23857" id="r4043" name="with" negation="false" src="d0_s11" to="o23859" />
      <relation from="o23858" id="r4044" name="with" negation="false" src="d0_s11" to="o23859" />
    </statement>
    <statement id="d0_s12">
      <text>upper florets 2.7-3.2 mm long, 1.3-1.6 mm wide, obovoid or ellipsoid, nearly smooth, minutely papillose, or crossrugulose, subacute to acute.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = unknown.</text>
      <biological_entity constraint="upper" id="o23861" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" src="d0_s12" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s12" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="nearly" name="relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="relief" src="d0_s12" value="crossrugulose" value_original="crossrugulose" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s12" value="papillose" value_original="papillose" />
        <character is_modifier="false" name="relief" src="d0_s12" value="crossrugulose" value_original="crossrugulose" />
        <character char_type="range_value" from="subacute" name="shape" src="d0_s12" to="acute" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23862" name="chromosome" name_original="" src="d0_s13" type="structure" />
    </statement>
  </description>
  <discussion>Panicum brachyanthum grows in dry, sandy or clayey soils of open areas, remnant prairies, woodland borders, and roadsides and, less commonly, along the margins of bogs and on grassy shores in the western portion of the gulf coast plain. It is restricted to the southern United States. It resembles P. verrucosum in its growth habit, but is more restricted in its distribution.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ark.;Okla.;Tex.;Miss.;La.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>