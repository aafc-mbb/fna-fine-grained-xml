<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">618</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Andersson" date="unknown" rank="genus">MISCANTHUS</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="species">oligostachyus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus miscanthus;species oligostachyus</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Small japanese silvergrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, rhizomatous.</text>
      <biological_entity id="o21919" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 80-150 cm tall, 2-3 mm thick below, few together or solitary;</text>
      <biological_entity id="o21920" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="80" from_unit="cm" name="height" src="d0_s1" to="150" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" modifier="below" name="thickness" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="together" name="quantity" src="d0_s1" value="few" value_original="few" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes finely pubescent.</text>
      <biological_entity id="o21921" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="finely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths mostly glabrous, pilose near the summits;</text>
      <biological_entity id="o21922" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="near summits" constraintid="o21923" is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o21923" name="summit" name_original="summits" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 2-3 mm, rounded;</text>
      <biological_entity id="o21924" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades well-developed only on the cauline sheaths, 8-35 cm long, 6-25 mm wide, adaxial surfaces densely pilose basally.</text>
      <biological_entity id="o21925" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character constraint="on cauline sheaths" constraintid="o21926" is_modifier="false" name="development" src="d0_s5" value="well-developed" value_original="well-developed" />
        <character char_type="range_value" from="8" from_unit="cm" name="length" notes="" src="d0_s5" to="35" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o21926" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o21927" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely; basally" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles long-exserted, loose, with 2-5 erect to suberect branches;</text>
      <biological_entity id="o21928" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="position" src="d0_s6" value="long-exserted" value_original="long-exserted" />
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s6" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity id="o21929" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="5" />
        <character char_type="range_value" from="erect" is_modifier="true" name="orientation" src="d0_s6" to="suberect" />
      </biological_entity>
      <relation from="o21928" id="r3742" name="with" negation="false" src="d0_s6" to="o21929" />
    </statement>
    <statement id="d0_s7">
      <text>branches 7-15 cm, densely pilose, with white or purplish-white hairs.</text>
      <biological_entity id="o21930" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s7" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o21931" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="purplish-white" value_original="purplish-white" />
      </biological_entity>
      <relation from="o21930" id="r3743" name="with" negation="false" src="d0_s7" to="o21931" />
    </statement>
    <statement id="d0_s8">
      <text>Shorter pedicels 1.5-2 mm;</text>
      <biological_entity constraint="shorter" id="o21932" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>longer pedicels 5-6 mm, sulcate on 1 side.</text>
      <biological_entity constraint="longer" id="o21933" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character constraint="on side" constraintid="o21934" is_modifier="false" name="architecture" src="d0_s9" value="sulcate" value_original="sulcate" />
      </biological_entity>
      <biological_entity id="o21934" name="side" name_original="side" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 6-8 mm;</text>
      <biological_entity id="o21935" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>callus hairs from 1/2 as long as to equaling the spikelets, silky, white.</text>
      <biological_entity constraint="callus" id="o21936" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character constraint="to spikelets" constraintid="o21937" modifier="from" name="quantity" src="d0_s11" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s11" value="silky" value_original="silky" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o21937" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Lower glumes 6-8 mm, sparsely pilose, 2-keeled above, 2-toothed, teeth densely white-ciliate;</text>
      <biological_entity constraint="lower" id="o21938" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="shape" src="d0_s12" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" name="shape" src="d0_s12" value="2-toothed" value_original="2-toothed" />
      </biological_entity>
      <biological_entity id="o21939" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence_or_shape" src="d0_s12" value="white-ciliate" value_original="white-ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes equaling the lower glumes, 3-5-veined;</text>
      <biological_entity constraint="upper" id="o21940" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity constraint="lower" id="o21941" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>awns of upper lemmas (4) 8-15 mm, twisted at the bases;</text>
      <biological_entity id="o21942" name="awn" name_original="awns" src="d0_s14" type="structure" constraint="lemma" constraint_original="lemma; lemma">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="15" to_unit="mm" />
        <character constraint="at bases" constraintid="o21944" is_modifier="false" name="architecture" src="d0_s14" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity constraint="upper" id="o21943" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity id="o21944" name="base" name_original="bases" src="d0_s14" type="structure" />
      <relation from="o21942" id="r3744" name="part_of" negation="false" src="d0_s14" to="o21943" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 2.5-3 mm. 2n = 38.</text>
      <biological_entity id="o21945" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21946" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Miscanthus oligostachyus is a native of Japanese and Korean forests that is sold as an ornamental species in the United States. It does best in regions with cool summers. Koyama (1987) recognized three subspecies of M. oligostachyus; they have not been evaluated for this treatment.</discussion>
  
</bio:treatment>