<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">110</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Gaertn." date="unknown" rank="genus">ELEUSINE</taxon_name>
    <taxon_name authority="(Lam.) Lam." date="unknown" rank="species">tristachya</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eleusine;species tristachya</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Threespike goosegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o23795" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 10-45 cm, compressed.</text>
      <biological_entity id="o23796" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s1" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Blades 6-25 cm long.</text>
      <biological_entity id="o23797" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Panicles digitate, with (1) 2-3 branches;</text>
      <biological_entity id="o23798" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="digitate" value_original="digitate" />
      </biological_entity>
      <biological_entity id="o23799" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s3" value="1" value_original="1" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <relation from="o23798" id="r4036" name="with" negation="false" src="d0_s3" to="o23799" />
    </statement>
    <statement id="d0_s4">
      <text>branches 1-6 (8) cm long, 5-14 mm wide, oblong.</text>
      <biological_entity id="o23800" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character name="length" src="d0_s4" unit="cm" value="8" value_original="8" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="14" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="oblong" value_original="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 8-10 mm, with 5-9 (11) florets.</text>
      <biological_entity id="o23801" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23802" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s5" value="11" value_original="11" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s5" to="9" />
      </biological_entity>
      <relation from="o23801" id="r4037" name="with" negation="false" src="d0_s5" to="o23802" />
    </statement>
    <statement id="d0_s6">
      <text>Glumes unequal;</text>
      <biological_entity id="o23803" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lower glumes 2-3 mm;</text>
      <biological_entity constraint="lower" id="o23804" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>upper glumes 3-4 mm;</text>
      <biological_entity constraint="upper" id="o23805" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lemmas 4-5 mm. 2n = 18.</text>
      <biological_entity id="o23806" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23807" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the 1800s and early 1900s, Eleusine tristachya was found on ballast dumps at various ports and trans¬portation centers in the United States. It has since been found as a weed in the Imperial Valley of California (Hilu 1980), but records of collections outside of California appear to be historical, with no populations persisting. The species was originally thought to be native to tropical Africa and introduced into tropical America, but it occurs in Africa only as a rare adventive. It is now considered to be native to tropical America.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.;N.J.;Va.;Mo.;Ala.;Oreg.;N.Y.;Calif.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>