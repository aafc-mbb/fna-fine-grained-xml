<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="Scribn. ex Beal" date="unknown" rank="species">porteri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species porteri</taxon_hierarchy>
  </taxon_identification>
  <number>26</number>
  <other_name type="common_name">Bush muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>loosely cespitose from a knotty base, not rhizomatous, distinctly bushy in appearance.</text>
      <biological_entity id="o5290" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character constraint="from base" constraintid="o5291" is_modifier="false" modifier="loosely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5291" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character constraint="in appearance" constraintid="o5292" is_modifier="false" modifier="distinctly" name="growth_form" src="d0_s1" value="bushy" value_original="bushy" />
      </biological_entity>
      <biological_entity id="o5292" name="appearance" name_original="appearance" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 25-100 cm tall, 0.5-1.5 mm thick, erect, geniculate, wiry, freely branched, branches stiff, geniculate, widely divergent;</text>
      <biological_entity id="o5293" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="height" src="d0_s2" to="100" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s2" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="wiry" value_original="wiry" />
        <character is_modifier="false" modifier="freely" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o5294" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s2" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes scabridulous throughout.</text>
      <biological_entity id="o5295" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="throughout" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths shorter than the internodes, glabrous;</text>
      <biological_entity id="o5296" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o5297" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5297" name="internode" name_original="internodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-2.5 (4) mm, truncate, lacerate, with lateral lobes;</text>
      <biological_entity id="o5298" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="lacerate" value_original="lacerate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o5299" name="lobe" name_original="lobes" src="d0_s5" type="structure" />
      <relation from="o5298" id="r846" name="with" negation="false" src="d0_s5" to="o5299" />
    </statement>
    <statement id="d0_s6">
      <text>blades 2-8 cm long, 0.5-2 mm wide, flat or folded, smooth or scabridulous abaxially, scabridulous adaxially.</text>
      <biological_entity id="o5300" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" name="relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="adaxially" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-14 cm long, 6-15 cm wide, open, not dense, usually purple;</text>
      <biological_entity id="o5301" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="14" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="width" src="d0_s7" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" modifier="not" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="usually" name="coloration_or_density" src="d0_s7" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches 1-7.5 cm, diverging 30-90° from the rachises, stiff, naked basally;</text>
      <biological_entity constraint="primary" id="o5302" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="7.5" to_unit="cm" />
        <character constraint="from rachises" constraintid="o5303" is_modifier="false" modifier="30-90°" name="orientation" src="d0_s8" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s8" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s8" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o5303" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels 2-13 (20) mm, scabrous.</text>
      <biological_entity id="o5304" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="20" value_original="20" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="13" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3-4.5 mm.</text>
      <biological_entity id="o5305" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Glumes 2-3 mm, shorter than the lemmas, 1-veined, veins scabrous, apices gradually acute to acuminate, occasionally mucronate, mucros to 0.4 mm;</text>
      <biological_entity id="o5306" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character constraint="than the lemmas" constraintid="o5307" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o5307" name="lemma" name_original="lemmas" src="d0_s11" type="structure" />
      <biological_entity id="o5308" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o5309" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="gradually acute" name="shape" src="d0_s11" to="acuminate" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s11" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o5310" name="mucro" name_original="mucros" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 3-4.5 mm, lanceolate, purplish, appressed-pubescent on the lower 1/2 - 3/4 of the margins and midveins, apices acuminate, awned, awns 2-13 mm, straight;</text>
      <biological_entity id="o5311" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character constraint="on lower 1/2-3/4" constraintid="o5312" is_modifier="false" name="pubescence" src="d0_s12" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o5312" name="1/2-3/4" name_original="1/2-3/4" src="d0_s12" type="structure" />
      <biological_entity id="o5313" name="margin" name_original="margins" src="d0_s12" type="structure" />
      <biological_entity id="o5314" name="midvein" name_original="midveins" src="d0_s12" type="structure" />
      <biological_entity id="o5315" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o5316" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="13" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s12" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o5312" id="r847" name="part_of" negation="false" src="d0_s12" to="o5313" />
      <relation from="o5312" id="r848" name="part_of" negation="false" src="d0_s12" to="o5314" />
    </statement>
    <statement id="d0_s13">
      <text>paleas 3-4.5 mm, lanceolate, acuminate;</text>
      <biological_entity id="o5317" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 1.5-2.3 mm, purple to yellow.</text>
      <biological_entity id="o5318" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="purple" name="coloration" src="d0_s14" to="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 2-2.4 mm, oblong, compressed, yellowish-brown.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 20, 23, 24, 40.</text>
      <biological_entity id="o5319" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s15" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellowish-brown" value_original="yellowish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5320" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="20" value_original="20" />
        <character name="quantity" src="d0_s16" value="23" value_original="23" />
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia porteri grows among boulders on rocky slopes and on cliffs, and in dry arroyos, desert flats, and grasslands, frequently in the protection of shrubs, at elevations of 600-1700 m. Its geographic range extends from the southwestern United States to northern Mexico.</discussion>
  <discussion>Muhlenbergia porteri is highly palatable to all classes of livestock, but is never abundant at any particular location.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;N.Mex.;Tex.;Utah;Calif.;Colo.;Ariz.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>