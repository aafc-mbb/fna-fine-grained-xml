<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">scrobiculatum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species scrobiculatum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">orbiculare</taxon_name>
    <taxon_hierarchy>genus paspalum;species orbiculare</taxon_hierarchy>
    <other_info_on_name type="comment">as "orbiculatum"</other_info_on_name>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Indian paspalum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o2598" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 10-150 cm, erect or decumbent;</text>
      <biological_entity id="o2599" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="150" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes glabrous.</text>
      <biological_entity id="o2600" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o2601" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.3-1.2 mm, often with a row of hairs behind them;</text>
      <biological_entity id="o2602" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s4" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2603" name="row" name_original="row" src="d0_s4" type="structure" />
      <biological_entity id="o2604" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <relation from="o2602" id="r410" modifier="often; behind them" name="with" negation="false" src="d0_s4" to="o2603" />
      <relation from="o2603" id="r411" name="part_of" negation="false" src="d0_s4" to="o2604" />
    </statement>
    <statement id="d0_s5">
      <text>blades 5-30 cm long, 2-8 (12) mm wide, flat, usually glabrous.</text>
      <biological_entity id="o2605" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="30" to_unit="cm" />
        <character name="width" src="d0_s5" unit="mm" value="12" value_original="12" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles terminal, with 1-5 digitately or racemosely arranged branches;</text>
      <biological_entity id="o2606" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o2607" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="5" />
        <character is_modifier="true" modifier="digitately; racemosely" name="arrangement" src="d0_s6" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o2606" id="r412" name="with" negation="false" src="d0_s6" to="o2607" />
    </statement>
    <statement id="d0_s7">
      <text>branches 3-10 cm, diverging to spreading, persistent;</text>
      <biological_entity id="o2608" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="diverging" name="orientation" src="d0_s7" to="spreading" />
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branch axes 1.5-3 mm wide, broadly winged, glabrous, margins scabrous, terminating in a spikelet.</text>
      <biological_entity constraint="branch" id="o2609" name="axis" name_original="axes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s8" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2610" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o2611" name="spikelet" name_original="spikelet" src="d0_s8" type="structure" />
      <relation from="o2610" id="r413" name="terminating in" negation="false" src="d0_s8" to="o2611" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 1.8-3.2 mm long, 2-2.3 mm wide, solitary, diverging from the branch axes, ovate, glabrous, olive green to dark, glossy brown.</text>
      <biological_entity id="o2612" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s9" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
        <character constraint="from branch axes" constraintid="o2613" is_modifier="false" name="orientation" src="d0_s9" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="olive green" name="coloration" src="d0_s9" to="dark" />
        <character is_modifier="false" name="reflectance" src="d0_s9" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="branch" id="o2613" name="axis" name_original="axes" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes absent;</text>
      <biological_entity constraint="lower" id="o2614" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes as long as the lower lemmas, 5-7-veined;</text>
      <biological_entity constraint="upper" id="o2615" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
      <biological_entity constraint="lower" id="o2616" name="lemma" name_original="lemmas" src="d0_s11" type="structure" />
      <relation from="o2615" id="r414" name="as long as" negation="false" src="d0_s11" to="o2616" />
    </statement>
    <statement id="d0_s12">
      <text>lower lemmas 3-5-veined;</text>
      <biological_entity constraint="lower" id="o2617" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper florets 2.5-3 mm long, 1.4-1.8 mm wide, dark glossy brown.</text>
      <biological_entity constraint="upper" id="o2618" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s13" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s13" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark" value_original="dark" />
        <character is_modifier="false" name="reflectance" src="d0_s13" value="glossy" value_original="glossy" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 1.1-1.5 mm, nearly orbicular.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 20, 40, 60, 120.</text>
      <biological_entity id="o2619" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s14" value="orbicular" value_original="orbicular" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2620" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" unit=",,,1" value="20" value_original="20" />
        <character name="quantity" src="d0_s15" unit=",,,1" value="40" value_original="40" />
        <character name="quantity" src="d0_s15" unit=",,,1" value="60" value_original="60" />
        <character name="quantity" src="d0_s15" unit=",,,1" value="120" value_original="120" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum scrobiculatium is native to India. It has been found growing in widely scattered disturbed areas of the southeastern United States, possibly as an escape from cultivation. It is grown as a cereal (Kodo) in India.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;N.J.;Tex.;Ala.;Ga.;Pacific Islands (Hawaii)</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>