<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">368</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Haller" date="unknown" rank="genus">DIGITARIA</taxon_name>
    <taxon_name authority="(Swallen) Henrard" date="unknown" rank="species">patens</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus digitaria;species patens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trichachne</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">patens</taxon_name>
    <taxon_hierarchy>genus trichachne;species patens</taxon_hierarchy>
  </taxon_identification>
  <number>10</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, neither rhizomatous nor stoloniferous.</text>
      <biological_entity id="o6378" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 40-90 cm, erect, sometimes geniculate, not rooting, at the lower nodes.</text>
      <biological_entity id="o6379" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s2" to="90" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o6380" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <relation from="o6379" id="r1010" name="at" negation="false" src="d0_s2" to="o6380" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves mainly cauline;</text>
      <biological_entity id="o6381" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o6382" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mainly" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal sheaths villous;</text>
      <biological_entity constraint="basal" id="o6383" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>upper sheaths glabrous or sparsely to densely hirsute, hairs papillose-based;</text>
      <biological_entity constraint="upper" id="o6384" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character char_type="range_value" from="glabrous or" name="pubescence" src="d0_s5" to="sparsely densely hirsute" />
      </biological_entity>
      <biological_entity id="o6385" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules (1) 1.5-4 mm, entire to lacerate;</text>
      <biological_entity id="o6386" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s6" to="lacerate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 5-15 cm long, 1-4 mm wide, glabrous or sparsely pubescent.</text>
      <biological_entity id="o6387" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles with 4-10 spikelike primary branches on (4) 10-18 cm rachises;</text>
      <biological_entity id="o6388" name="panicle" name_original="panicles" src="d0_s8" type="structure" />
      <biological_entity constraint="primary" id="o6389" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s8" to="10" />
        <character is_modifier="true" name="shape" src="d0_s8" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o6390" name="rachis" name_original="rachises" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_some_measurement" src="d0_s8" unit="cm" value="4" value_original="4" />
        <character char_type="range_value" from="10" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s8" to="18" to_unit="cm" />
      </biological_entity>
      <relation from="o6388" id="r1011" name="with" negation="false" src="d0_s8" to="o6389" />
      <relation from="o6389" id="r1012" name="on" negation="false" src="d0_s8" to="o6390" />
    </statement>
    <statement id="d0_s9">
      <text>primary branches 4-10 cm, usually divergent at maturity, varying to ascending, axes not wing-margined, bearing spikelets in unequally pedicellate pairs;</text>
      <biological_entity constraint="primary" id="o6391" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s9" to="10" to_unit="cm" />
        <character constraint="at axes" constraintid="o6392" is_modifier="false" modifier="usually" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o6392" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="maturity" value_original="maturity" />
        <character is_modifier="true" name="variability" src="d0_s9" value="varying" value_original="varying" />
        <character is_modifier="true" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="wing-margined" value_original="wing-margined" />
      </biological_entity>
      <biological_entity id="o6393" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o6394" name="pair" name_original="pairs" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="unequally" name="architecture" src="d0_s9" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o6391" id="r1013" name="bearing" negation="false" src="d0_s9" to="o6393" />
      <relation from="o6391" id="r1014" name="in" negation="false" src="d0_s9" to="o6394" />
    </statement>
    <statement id="d0_s10">
      <text>internodes (4.5) 6-15 mm (midbranch);</text>
      <biological_entity id="o6395" name="internode" name_original="internodes" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="4.5" value_original="4.5" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>secondary branches rarely present;</text>
      <biological_entity constraint="secondary" id="o6396" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>shorter pedicels 2-2.5 mm;</text>
      <biological_entity constraint="shorter" id="o6397" name="pedicel" name_original="pedicels" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>longer pedicels 7-8 mm;</text>
      <biological_entity constraint="longer" id="o6398" name="pedicel" name_original="pedicels" src="d0_s13" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s13" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>terminal pedicels of primary branches 7.4-20 mm.</text>
      <biological_entity constraint="branch" id="o6399" name="pedicel" name_original="pedicels" src="d0_s14" type="structure" constraint_original="branch terminal; branch">
        <character char_type="range_value" from="7.4" from_unit="mm" name="some_measurement" src="d0_s14" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="primary" id="o6400" name="branch" name_original="branches" src="d0_s14" type="structure" />
      <relation from="o6399" id="r1015" name="part_of" negation="false" src="d0_s14" to="o6400" />
    </statement>
    <statement id="d0_s15">
      <text>Spikelets homomorphic, 3.7-5.8 mm (including pubescence), 2.9-4.3 mm (excluding pubescence).</text>
      <biological_entity id="o6401" name="spikelet" name_original="spikelets" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="homomorphic" value_original="homomorphic" />
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s15" to="5.8" to_unit="mm" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s15" to="4.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Lower glumes 0.3-0.5 mm;</text>
      <biological_entity constraint="lower" id="o6402" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper glumes 2.4-3.5 mm (excluding pubescence), 3-veined, densely villous, hairs 1.5-4 mm, silvery-white to purple, spreading at maturity;</text>
      <biological_entity constraint="upper" id="o6403" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s17" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s17" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o6404" name="hair" name_original="hairs" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s17" to="4" to_unit="mm" />
        <character char_type="range_value" from="silvery-white" name="coloration" src="d0_s17" to="purple" />
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s17" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lower lemmas 2.8-4.2 mm (excluding pubescence), exceeding the upper lemmas by 0.8-2.2 mm, 5-veined and the veins equally spaced or 7-veined and the lateral-veins closer to each other than to the central vein, margins densely villous, hairs 1.5-4 mm, silvery-white to purple, spreading at maturity, apices acuminate;</text>
      <biological_entity constraint="lower" id="o6405" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s18" to="4.2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="7-veined" value_original="7-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o6406" name="lemma" name_original="lemmas" src="d0_s18" type="structure" />
      <biological_entity id="o6407" name="vein" name_original="veins" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s18" to="2.2" to_unit="mm" />
        <character is_modifier="true" name="architecture" src="d0_s18" value="5-veined" value_original="5-veined" />
        <character is_modifier="false" modifier="equally" name="arrangement" src="d0_s18" value="spaced" value_original="spaced" />
      </biological_entity>
      <biological_entity id="o6408" name="lateral-vein" name_original="lateral-veins" src="d0_s18" type="structure">
        <character constraint="to central vein" constraintid="o6409" is_modifier="false" name="arrangement" src="d0_s18" value="closer" value_original="closer" />
      </biological_entity>
      <biological_entity constraint="central" id="o6409" name="vein" name_original="vein" src="d0_s18" type="structure" />
      <biological_entity id="o6410" name="margin" name_original="margins" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s18" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o6411" name="hair" name_original="hairs" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s18" to="4" to_unit="mm" />
        <character char_type="range_value" from="silvery-white" name="coloration" src="d0_s18" to="purple" />
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s18" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o6412" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o6405" id="r1016" name="exceeding the" negation="false" src="d0_s18" to="o6406" />
      <relation from="o6405" id="r1017" name="by" negation="false" src="d0_s18" to="o6407" />
    </statement>
    <statement id="d0_s19">
      <text>upper lemmas 2.6-3.2 mm, lanceolate, brown when immature, dark-brown at maturity, acuminate.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 72.</text>
      <biological_entity constraint="upper" id="o6413" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s19" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s19" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="when immature" name="coloration" src="d0_s19" value="brown" value_original="brown" />
        <character constraint="at maturity" is_modifier="false" name="coloration" src="d0_s19" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s19" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6414" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Digitaria patens is endemic to southwestern and southern Texas and adjacent Mexico. It grows in well-drained, usually sandy, soils, often in disturbed habitats. Gould (1975) suggested that it might be an octoploid derivative of D. californica.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>