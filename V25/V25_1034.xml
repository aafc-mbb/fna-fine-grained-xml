<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>J. Gabriel Sanchez-Ken; Lynn G. Clark;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">344</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Soderstr." date="unknown" rank="subfamily">CENTOTHECOIDEAE</taxon_name>
    <taxon_name authority="Ridl." date="unknown" rank="tribe">CENTOTHECEAE</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="genus">CHASMANTHIUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily centothecoideae;tribe centotheceae;genus chasmanthium</taxon_hierarchy>
  </taxon_identification>
  <number>22.01</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose or loosely colonial, rhizomatous.</text>
      <biological_entity id="o4941" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s1" value="colonial" value_original="colonial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 35-150 cm, simple or branched.</text>
      <biological_entity id="o4942" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves cauline;</text>
      <biological_entity id="o4943" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, ciliate;</text>
      <biological_entity id="o4944" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades not pseudopetiolate, flat.</text>
      <biological_entity id="o4945" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="pseudopetiolate" value_original="pseudopetiolate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles open or contracted, sometimes becoming racemose distally;</text>
    </statement>
    <statement id="d0_s7">
      <text>disarticulation above the glumes and between the florets.</text>
      <biological_entity id="o4946" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="sometimes becoming; distally" name="arrangement" src="d0_s6" value="racemose" value_original="racemose" />
      </biological_entity>
      <biological_entity id="o4947" name="floret" name_original="florets" src="d0_s7" type="structure" />
      <relation from="o4946" id="r795" name="above the glumes and between" negation="false" src="d0_s7" to="o4947" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 4-50 mm, laterally compressed, with 2-many florets, lower 1-4 florets sterile.</text>
      <biological_entity id="o4948" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="50" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o4949" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="many" />
      </biological_entity>
      <biological_entity constraint="lower" id="o4950" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s8" to="4" />
      </biological_entity>
      <biological_entity id="o4951" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o4948" id="r796" name="with" negation="false" src="d0_s8" to="o4949" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes 2, subequal, shorter than the spikelets, glabrous, (2) 3-9-veined, acute to acuminate;</text>
      <biological_entity id="o4952" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character constraint="than the spikelets" constraintid="o4953" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="(2)3-9-veined" value_original="(2)3-9-veined" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="acuminate" />
      </biological_entity>
      <biological_entity id="o4953" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lemmas glabrous, 3-15-veined, compressed-keeled, keels serrate or ciliate, apices acuminate to acute, entire (rarely bifid);</text>
      <biological_entity id="o4954" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="3-15-veined" value_original="3-15-veined" />
        <character is_modifier="false" name="shape" src="d0_s10" value="compressed-keeled" value_original="compressed-keeled" />
      </biological_entity>
      <biological_entity id="o4955" name="keel" name_original="keels" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="serrate" value_original="serrate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s10" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o4956" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s10" to="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>paleas glabrous, gibbous basally, 2-keeled, keels winged, wings glabrous, scabrous, or pilose;</text>
      <biological_entity id="o4957" name="palea" name_original="paleas" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="basally" name="shape" src="d0_s11" value="gibbous" value_original="gibbous" />
        <character is_modifier="false" name="shape" src="d0_s11" value="2-keeled" value_original="2-keeled" />
      </biological_entity>
      <biological_entity id="o4958" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o4959" name="wing" name_original="wings" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lodicules 2, fleshy, cuneate, 2-4-veined, lobed-truncate;</text>
      <biological_entity id="o4960" name="lodicule" name_original="lodicules" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="shape" src="d0_s12" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-4-veined" value_original="2-4-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="lobed-truncate" value_original="lobed-truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 1;</text>
      <biological_entity id="o4961" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>ovaries glabrous;</text>
      <biological_entity id="o4962" name="ovary" name_original="ovaries" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>styles 2;</text>
      <biological_entity id="o4963" name="style" name_original="styles" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>style-branches 2, plumose, reddish-purple at anthesis.</text>
      <biological_entity id="o4964" name="style-branch" name_original="style-branches" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s16" value="plumose" value_original="plumose" />
        <character constraint="at anthesis" is_modifier="false" name="coloration_or_density" src="d0_s16" value="reddish-purple" value_original="reddish-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 1.9-5 mm, laterally compressed, brown to reddish-black or black, x = 12.</text>
      <biological_entity id="o4965" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s17" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s17" value="compressed" value_original="compressed" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s17" to="reddish-black or black" />
      </biological_entity>
      <biological_entity constraint="x" id="o4966" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chasmanthium, a genus of five species endemic to North America, grows primarily in the southeastern and south-central parts of the United States. It was formerly included in Uniola, but it is now recognized as a distinct genus.</discussion>
  <references>
    <reference>Yates, H.O. 1966a. Morphology and cytology of Uniola (Gramineae). SouthW. Naturalist 11:145-189</reference>
    <reference>Yates, H.O. 1966b. Revision of grasses traditionally referred to Uniola, II. Chasmanthium. SouthW. Naturalist 11:451-455.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del.;Wis.;W.Va.;Fla.;N.J.;Tex.;La.;N.C.;Tenn.;S.C.;Pa.;N.Y.;D.C;N.Mex.;Va.;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Md.;Kans.;Okla.;Ohio;Mo.;Mich.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicle branches nodding or drooping; pedicels 10-30 mm long; calluses pilose; lower glumes 4.2-9.1 mm long; keels of fertile lemmas winged, the wings scabrous to pilose their full length; caryopses 2.9-5 mm long</description>
      <determination>1 Chasmanthium latifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicle branches erect or ascending; pedicels 0.5-2.5(5) mm long; calluses glabrous; lower glumes 1.2-5 mm long; keels of fertile lemmas not winged, scabridulous toward or at the apices; caryopses 1.9-3 mm long.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Spikelets 9.5-24 mm long; fertile lemmas 9-13-veined; caryopses enclosed at maturity; blades 7-16(33) cm long, lanceolate-fusiform; culms leafy for 80% of their height.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Axils of the panicle branches scabrous; fertile florets diverging to 45° from the rachilla; sterile florets (0)1(2); lower glumes 3.1-5 mm long, 7-9-veined; ligules entire</description>
      <determination>2 Chasmanthium nitidum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Axils of the panicle branches pilose; fertile florets diverging to 85° from the rachilla; sterile florets 2-4; lower glumes 2.5-2.9 mm long, 2-3-veined; ligules irregularly laciniate</description>
      <determination>3 Chasmanthium ornithorhynchum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Spikelets 4-10 mm long; fertile lemmas 3-9-veined; caryopses exposed at maturity; blades (8)20-50 cm long, linear-lanceolate; culms leafy for 40-50% of their height.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Collars and sheaths pilose; culms (1)2-3.5 mm thick at the nodes; fertile lemmas 7-9-veined, usually curved or irregularly contorted</description>
      <determination>4 Chasmanthium sessiliflorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Collars and sheaths glabrous; culms to 1 mm thick at the nodes; fertile lemmas 3-7-veined, straight</description>
      <determination>5 Chasmanthium laxum</determination>
    </key_statement>
  </key>
</bio:treatment>