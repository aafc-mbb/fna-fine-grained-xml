<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">535</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">ANTHEPHORA</taxon_name>
    <taxon_name authority="(L.) Kuntze" date="unknown" rank="species">hermaphrodita</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus anthephora;species hermaphrodita</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Oldfield grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o21785" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms erect or decumbent, sometimes rooting at the lower nodes, branching from the base.</text>
      <biological_entity id="o21786" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o21787" is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
        <character constraint="from base" constraintid="o21788" is_modifier="false" name="architecture" notes="" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="lower" id="o21787" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o21788" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths densely to sparsely pubescent, hairs papillose-based;</text>
      <biological_entity id="o21789" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="densely to sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21790" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 1.5-3 mm, brownish, entire or dentate, not ciliate;</text>
      <biological_entity id="o21791" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 4-17 cm long, 2-8 mm wide, flat, pubescent on both surfaces.</text>
      <biological_entity id="o21792" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="17" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character constraint="on surfaces" constraintid="o21793" is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21793" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Panicles 4-12 cm long, 5-8 mm wide, with 20-60 fascicles;</text>
    </statement>
    <statement id="d0_s6">
      <text>fascicles 5-7.5 mm;</text>
      <biological_entity id="o21794" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="fascicles" value_original="fascicles" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="7.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 4-7 mm, scabrous.</text>
      <biological_entity id="o21795" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fertile spikelets 3.5-4.5 mm, ovoid, scabrid between the veins, acute;</text>
      <biological_entity id="o21796" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s8" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character constraint="between veins" constraintid="o21797" is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrid" value_original="scabrid" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o21797" name="vein" name_original="veins" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>upper lemmas 3.7-4 mm, glabrous, margins overlapping the edges of the palea.</text>
      <biological_entity constraint="upper" id="o21798" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.7" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21799" name="margin" name_original="margins" src="d0_s9" type="structure" />
      <biological_entity id="o21800" name="edge" name_original="edges" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o21801" name="paleum" name_original="palea" src="d0_s9" type="structure" />
      <relation from="o21800" id="r3718" name="part_of" negation="false" src="d0_s9" to="o21801" />
    </statement>
    <statement id="d0_s10">
      <text>Caryopses about 2 mm. 2n = 18.</text>
      <biological_entity id="o21802" name="caryopsis" name_original="caryopses" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21803" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Anthephora hermaphrodita is a weedy species, native to maritime beaches, lowland pastures, and disturbed areas from Mexico and the Caribbean Islands to Peru and Brazil. It is now established in Alachua County, Florida, having escaped from plantings at the Experiment Station of the University of Florida, Gainesville.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Virgin Islands;Pacific Islands (Hawaii);Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>