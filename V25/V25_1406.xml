<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">550</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">SETARIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Setaria</taxon_name>
    <taxon_name authority="(Scribn. &amp; Merr.) K. Schum." date="unknown" rank="species">macrosperma</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus setaria;subgenus setaria;species macrosperma</taxon_hierarchy>
  </taxon_identification>
  <number>11</number>
  <other_name type="common_name">Coral bristlegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o13502" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 1-1.5 m.</text>
      <biological_entity id="o13503" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="m" name="some_measurement" src="d0_s1" to="1.5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths prominently keeled, margins villous;</text>
      <biological_entity id="o13504" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="prominently" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o13505" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 1-3 mm;</text>
      <biological_entity id="o13506" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 1-2 cm wide, flat, scabrous.</text>
      <biological_entity id="o13507" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles to 25 cm, loosely spicate;</text>
      <biological_entity id="o13508" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s5" to="25" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="architecture" src="d0_s5" value="spicate" value_original="spicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachises readily visible, sparsely villous;</text>
      <biological_entity id="o13509" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="readily" name="prominence" src="d0_s6" value="visible" value_original="visible" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bristles 1 (2), 15-30 mm, flexible, antrorsely scabrous.</text>
      <biological_entity id="o13510" name="bristle" name_original="bristles" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="1" value_original="1" />
        <character name="atypical_quantity" src="d0_s7" value="2" value_original="2" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s7" to="30" to_unit="mm" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="pliable" value_original="flexible" />
        <character is_modifier="false" modifier="antrorsely" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 3-3.2 mm.</text>
      <biological_entity id="o13511" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Lower glumes about 1/3 as long as the spikelets, 3-veined;</text>
      <biological_entity constraint="lower" id="o13512" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character constraint="as-long-as spikelets" constraintid="o13513" name="quantity" src="d0_s9" value="1/3" value_original="1/3" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o13513" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>upper glumes about 3/4 as long as the spikelets, 5-veined;</text>
      <biological_entity constraint="upper" id="o13514" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character constraint="as-long-as spikelets" constraintid="o13515" name="quantity" src="d0_s10" value="3/4" value_original="3/4" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o13515" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>lower lemmas equaling the upper lemmas;</text>
      <biological_entity constraint="lower" id="o13516" name="lemma" name_original="lemmas" src="d0_s11" type="structure" />
      <biological_entity constraint="upper" id="o13517" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower paleas about 1/2 as long as the upper paleas, hyaline, narrow;</text>
      <biological_entity constraint="lower" id="o13518" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character constraint="as-long-as upper paleas" constraintid="o13519" name="quantity" src="d0_s12" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s12" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="size_or_width" src="d0_s12" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="upper" id="o13519" name="palea" name_original="paleas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper lemmas finely and transversely rugose;</text>
      <biological_entity constraint="upper" id="o13520" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s13" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper paleas similar to the upper lemmas.</text>
      <biological_entity constraint="upper" id="o13522" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <relation from="o13521" id="r2214" name="to" negation="false" src="d0_s14" to="o13522" />
    </statement>
    <statement id="d0_s15">
      <text>2n = unknown.</text>
      <biological_entity constraint="upper" id="o13521" name="palea" name_original="paleas" src="d0_s14" type="structure" />
      <biological_entity constraint="2n" id="o13523" name="chromosome" name_original="" src="d0_s15" type="structure" />
    </statement>
  </description>
  <discussion>Setaria macrosperma grows on shell or coral islands, and occasionally in old fields or hammocks. It is most frequent in Florida, but has been collected in both South Carolina and Georgia. It also grows in the Bahamas and Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>S.C.;Fla.;Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>