<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Robert W. Freckmann; Michel G. Lelong;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">563</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Raf." date="unknown" rank="genus">STEINCHISMA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus steinchisma</taxon_hierarchy>
  </taxon_identification>
  <number>25.24</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, rhizomatous, rhizomes short, slender.</text>
      <biological_entity id="o5669" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o5670" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms slender, often compressed.</text>
      <biological_entity id="o5671" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s2" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths usually keeled;</text>
      <biological_entity id="o5672" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules minute, membranous, often erose or ciliate;</text>
      <biological_entity id="o5673" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="size" src="d0_s4" value="minute" value_original="minute" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s4" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades exhibiting Kranz anatomy, with few organelles in the external sheath and 5-7 isodiametric mesophyll cells between the vascular-bundles.</text>
      <biological_entity id="o5674" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="anatomy" src="d0_s5" value="kranz" value_original="kranz" />
      </biological_entity>
      <biological_entity id="o5675" name="organelle" name_original="organelles" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="few" value_original="few" />
      </biological_entity>
      <biological_entity constraint="external" id="o5676" name="sheath" name_original="sheath" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity constraint="between vascular-bundles external" constraintid="o5678-o5678" id="o5677" name="cell" name_original="cells" src="d0_s5" type="structure" constraint_original="between  vascular-bundles external, ">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity id="o5678" name="vascular-bundle" name_original="vascular-bundles" src="d0_s5" type="structure" />
      <relation from="o5674" id="r901" name="with" negation="false" src="d0_s5" to="o5675" />
      <relation from="o5675" id="r902" name="in" negation="false" src="d0_s5" to="o5676" />
      <relation from="o5675" id="r903" name="in" negation="false" src="d0_s5" to="o5677" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, open to contracted panicles;</text>
      <biological_entity id="o5679" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity id="o5680" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="true" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>primary branches few, slender;</text>
      <biological_entity constraint="primary" id="o5681" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="false" name="size" src="d0_s7" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels short, to 1 mm.</text>
      <biological_entity id="o5682" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="short" value_original="short" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets ellipsoid or lanceolate, initially somewhat compressed, ultimately expanding greatly.</text>
      <biological_entity id="o5683" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="initially somewhat" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="ultimately; greatly" name="size" src="d0_s9" value="expanding" value_original="expanding" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Glumes glabrous;</text>
      <biological_entity id="o5684" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 1/3 – 1/2 as long as the spikelets, usually 3 (5) -veined, acute;</text>
      <biological_entity constraint="lower" id="o5685" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o5686" from="1/3" name="quantity" src="d0_s11" to="1/2" />
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s11" value="3(5)-veined" value_original="3(5)-veined" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o5686" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>upper glumes and lower lemmas subequal, 3-5 (7) -veined;</text>
      <biological_entity constraint="upper" id="o5687" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-5(7)-veined" value_original="3-5(7)-veined" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o5688" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-5(7)-veined" value_original="3-5(7)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower florets sterile or staminate, often standing apart from the upper florets at maturity;</text>
      <biological_entity constraint="lower" id="o5689" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="upper" id="o5690" name="floret" name_original="florets" src="d0_s13" type="structure" />
      <relation from="o5689" id="r904" modifier="at maturity" name="standing apart from" negation="false" src="d0_s13" to="o5690" />
    </statement>
    <statement id="d0_s14">
      <text>lower paleas longer than the lower lemmas, greatly inflated at maturity, indurate;</text>
      <biological_entity constraint="lower" id="o5691" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character constraint="than the lower lemmas" constraintid="o5692" is_modifier="false" name="length_or_size" src="d0_s14" value="longer" value_original="longer" />
        <character constraint="at maturity" is_modifier="false" modifier="greatly" name="shape" src="d0_s14" value="inflated" value_original="inflated" />
        <character is_modifier="false" name="texture" src="d0_s14" value="indurate" value_original="indurate" />
      </biological_entity>
      <biological_entity constraint="lower" id="o5692" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>upper florets ovoid or ellipsoid;</text>
      <biological_entity constraint="upper" id="o5693" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper lemmas usually dull-colored, minutely papillose, papillae in longitudinal rows, apices acute, x = 9 or 10.</text>
      <biological_entity constraint="upper" id="o5694" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s16" value="dull-colored" value_original="dull-colored" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s16" value="papillose" value_original="papillose" />
      </biological_entity>
      <biological_entity id="o5695" name="papilla" name_original="papillae" src="d0_s16" type="structure" />
      <biological_entity id="o5696" name="row" name_original="rows" src="d0_s16" type="structure">
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s16" value="longitudinal" value_original="longitudinal" />
      </biological_entity>
      <biological_entity id="o5697" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="x" id="o5698" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="9" value_original="9" />
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
      </biological_entity>
      <relation from="o5695" id="r905" name="in" negation="false" src="d0_s16" to="o5696" />
    </statement>
  </description>
  <discussion>Steinchisma is a genus of 5-6 species that grow in moist or wet, usually open, sandy areas in warm-temperate and tropical regions of the Western Hemisphere. A single species is native the Flora region. It is sometimes included in Panicum, but recent studies support its recognition as a separate genus. Photosynthesis in Steinchisma is intermediate between C3 and C4 plants.</discussion>
  <references>
    <reference>Clifford, H.T. 1996. Etymological Dictionary of Grasses, Version 1.0 (CD-ROM). Expert Center for Taxonomic Identification, Amsterdam, The Netherlands</reference>
    <reference>Zuloaga, F.O., O. Morrone, A.S. Vega, and L.M. Giussani. 1998. Revision y analisis cladistico de Steinchisma (Poaceae: Panicoideae: Paniceae). Ann. Missouri Bot. Gard. 85:631-656.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;Miss.;Tex.;La.;Mo.;N.Mex.;Okla.;Ala.;Tenn.;N.C.;S.C.;Ark.;Ill.;Ga.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>