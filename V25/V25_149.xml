<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">93</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Wolf" date="unknown" rank="genus">ERAGROSTIS</taxon_name>
    <taxon_name authority="Hitchc." date="unknown" rank="species">swallenii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eragrostis;species swallenii</taxon_hierarchy>
  </taxon_identification>
  <number>32</number>
  <other_name type="common_name">Swallen's lovegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, with innovations, without rhizomes.</text>
      <biological_entity id="o21646" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o21647" name="innovation" name_original="innovations" src="d0_s1" type="structure" />
      <biological_entity id="o21648" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 35-70 cm, erect, with glandular bands below the nodes.</text>
      <biological_entity id="o21649" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s2" to="70" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o21650" name="band" name_original="bands" src="d0_s2" type="structure" />
      <biological_entity id="o21651" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <relation from="o21649" id="r3689" name="with" negation="false" src="d0_s2" to="o21650" />
      <relation from="o21650" id="r3690" name="below" negation="false" src="d0_s2" to="o21651" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths hairy on the margins and at the apices, hairs to 4 mm;</text>
      <biological_entity id="o21652" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="on the margins and at apices, hairs" constraintid="o21653, o21654" is_modifier="false" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o21653" name="apex" name_original="apices" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21654" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.2-0.5 mm;</text>
      <biological_entity id="o21655" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (8) 10-25 (30) cm long, 1.5-4 mm wide, flat to involute, abaxial surfaces glabrous, adaxial surfaces scabridulous, sometimes also sparsely hairy, hairs to 4 mm.</text>
      <biological_entity id="o21656" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="8" value_original="8" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o21657" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o21658" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="sometimes; sparsely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o21659" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 12-30 cm long, 5-16 cm wide, ovate, open, an oblique glandular ring present below the lowest rachis node;</text>
      <biological_entity id="o21660" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s6" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="16" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o21661" name="ring" name_original="ring" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation_or_shape" src="d0_s6" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity constraint="rachis" id="o21662" name="node" name_original="node" src="d0_s6" type="structure" constraint_original="lowest rachis" />
      <relation from="o21661" id="r3691" name="present" negation="false" src="d0_s6" to="o21662" />
    </statement>
    <statement id="d0_s7">
      <text>primary branches 2-10 cm, diverging 10-70° from the rachises, flexible;</text>
      <biological_entity constraint="primary" id="o21663" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
        <character constraint="from rachises" constraintid="o21664" is_modifier="false" modifier="10-70°" name="orientation" src="d0_s7" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="fragility" notes="" src="d0_s7" value="pliable" value_original="flexible" />
      </biological_entity>
      <biological_entity id="o21664" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>pulvini glabrous;</text>
      <biological_entity id="o21665" name="pulvinus" name_original="pulvini" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>pedicels 1.5-14 mm, divergent, with a glandular band.</text>
      <biological_entity id="o21666" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="14" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity constraint="glandular" id="o21667" name="band" name_original="band" src="d0_s9" type="structure" />
      <relation from="o21666" id="r3692" name="with" negation="false" src="d0_s9" to="o21667" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 5-16 mm long, 1.2-2.3 mm wide, linear-lanceolate, plumbeous to dark reddish-purple, with 5-25 florets;</text>
      <biological_entity id="o21669" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s10" to="25" />
      </biological_entity>
      <relation from="o21668" id="r3693" name="with" negation="false" src="d0_s10" to="o21669" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation acropetal, paleas persistent.</text>
      <biological_entity id="o21668" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s10" to="16" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s10" to="2.3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="plumbeous" name="coloration" src="d0_s10" to="dark reddish-purple" />
        <character is_modifier="false" name="development" src="d0_s11" value="acropetal" value_original="acropetal" />
      </biological_entity>
      <biological_entity id="o21670" name="palea" name_original="paleas" src="d0_s11" type="structure">
        <character is_modifier="false" name="duration" src="d0_s11" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes ovate, membranous to hyaline;</text>
      <biological_entity id="o21671" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 1.1-1.5 mm;</text>
      <biological_entity constraint="lower" id="o21672" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 1.4-2 mm;</text>
      <biological_entity constraint="upper" id="o21673" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 1.5-2.5 mm, ovate, membranous, strongly keeled, keels without glands, lateral-veins conspicuous, apices acute;</text>
      <biological_entity id="o21674" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o21675" name="keel" name_original="keels" src="d0_s15" type="structure" />
      <biological_entity id="o21676" name="gland" name_original="glands" src="d0_s15" type="structure" />
      <biological_entity id="o21677" name="lateral-vein" name_original="lateral-veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s15" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o21678" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o21675" id="r3694" name="without" negation="false" src="d0_s15" to="o21676" />
    </statement>
    <statement id="d0_s16">
      <text>paleas 1.2-2.1 mm, hyaline, narrower than the lemmas, apices obtuse to truncate;</text>
      <biological_entity id="o21679" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s16" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
        <character constraint="than the lemmas" constraintid="o21680" is_modifier="false" name="width" src="d0_s16" value="narrower" value_original="narrower" />
      </biological_entity>
      <biological_entity id="o21680" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <biological_entity id="o21681" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s16" to="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, 0.3-0.5 mm, purplish.</text>
      <biological_entity id="o21682" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 0.8-1.1 mm, rectangular-prismatic to ellipsoid, somewhat laterally compressed, with a well-developed adaxial groove, smooth, faintly striate, mostly opaque, light reddish-brown.</text>
      <biological_entity constraint="adaxial" id="o21684" name="groove" name_original="groove" src="d0_s18" type="structure">
        <character is_modifier="true" name="development" src="d0_s18" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <relation from="o21683" id="r3695" name="with" negation="false" src="d0_s18" to="o21684" />
    </statement>
    <statement id="d0_s19">
      <text>2n = 84.</text>
      <biological_entity id="o21683" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s18" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="rectangular-prismatic" name="shape" src="d0_s18" to="ellipsoid" />
        <character is_modifier="false" modifier="somewhat laterally" name="shape" src="d0_s18" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s18" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="faintly" name="coloration_or_pubescence_or_relief" src="d0_s18" value="striate" value_original="striate" />
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s18" value="opaque" value_original="opaque" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="light reddish-brown" value_original="light reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21685" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="84" value_original="84" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eragrostis swallenii grows in sandy sites along coastal grasslands and roadsides, often with Andropogon and Spartina, at 30-150 m. Its range extends around the Gulf coast from Texas to Mexico.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>