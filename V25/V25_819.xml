<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">222</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Desv." date="unknown" rank="genus">EUSTACHYS</taxon_name>
    <taxon_name authority="(Lag.) Nees" date="unknown" rank="species">distichophylla</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eustachys;species distichophylla</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chloris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">distichophylla</taxon_name>
    <taxon_hierarchy>genus chloris;species distichophylla</taxon_hierarchy>
  </taxon_identification>
  <number>7</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 60-140 cm, erect.</text>
      <biological_entity id="o5728" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="140" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Blades to 31 cm long, 10-15 mm wide, usually folded, apices obtuse.</text>
      <biological_entity id="o5729" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="31" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s1" to="15" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s1" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity id="o5730" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Panicles with 10-36 branches;</text>
      <biological_entity id="o5731" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
      <biological_entity id="o5732" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s2" to="36" />
      </biological_entity>
      <relation from="o5731" id="r909" name="with" negation="false" src="d0_s2" to="o5732" />
    </statement>
    <statement id="d0_s3">
      <text>branches 6-17 cm, flexible.</text>
      <biological_entity id="o5733" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s3" to="17" to_unit="cm" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="pliable" value_original="flexible" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets 2.4-3 mm;</text>
      <biological_entity id="o5734" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>florets 2 (3).</text>
      <biological_entity id="o5735" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character name="atypical_quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Lower glumes (1.2) 1.5-2 mm, lanceolate, apices acute;</text>
      <biological_entity constraint="lower" id="o5736" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="mm" value="1.2" value_original="1.2" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o5737" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>upper glumes 1.6-2.6 mm, narrowly oblong, apices truncate or bilobed, awned, awns 0.3-0.6 mm;</text>
      <biological_entity constraint="upper" id="o5738" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s7" to="2.6" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o5739" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="bilobed" value_original="bilobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o5740" name="awn" name_original="awns" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s7" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>calluses with a few hairs, hairs about 0.3 mm;</text>
      <biological_entity id="o5741" name="callus" name_original="calluses" src="d0_s8" type="structure" />
      <biological_entity id="o5742" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o5743" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.3" value_original="0.3" />
      </biological_entity>
      <relation from="o5741" id="r910" name="with" negation="false" src="d0_s8" to="o5742" />
    </statement>
    <statement id="d0_s9">
      <text>lowest lemmas (2.2) 2.5-2.9 mm, lanceolate, not strongly keeled, keels glabrous, lateral-veins with strongly spreading, white, 1-1.8 mm hairs, apices acute to mucronate;</text>
      <biological_entity constraint="lowest" id="o5744" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="2.2" value_original="2.2" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2.9" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="not strongly" name="shape" src="d0_s9" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o5745" name="keel" name_original="keels" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o5746" name="lateral-vein" name_original="lateral-veins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="with strongly spreading" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5747" name="hair" name_original="hairs" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>second lemmas 1.2-2 mm, oblanceolate, apices obtuse, third rudimentary lemma occasionally present.</text>
      <biological_entity id="o5748" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="mucronate" />
      </biological_entity>
      <biological_entity id="o5749" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o5750" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o5751" name="lemma" name_original="lemma" src="d0_s10" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s10" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" modifier="occasionally" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Caryopses 1.1-1.2 mm. 2n = 40.</text>
      <biological_entity id="o5752" name="caryopsis" name_original="caryopses" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5753" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eustachys distichopbylla is native to South America, but is now established along sandy roadsides, fields, and waste areas in the southern United States.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.;Fla.;Tex.;Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>