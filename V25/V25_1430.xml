<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kelly W. Allred;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">560</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Trin." date="unknown" rank="genus">STENOTAPHRUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus stenotaphrum</taxon_hierarchy>
  </taxon_identification>
  <number>25.22</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>sometimes rhizomatous or stoloniferous.</text>
      <biological_entity id="o4387" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-60 cm, usually compressed;</text>
      <biological_entity id="o4388" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s2" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes solid.</text>
      <biological_entity id="o4389" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="solid" value_original="solid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline;</text>
      <biological_entity id="o4390" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths shorter than the internodes, compressed;</text>
      <biological_entity id="o4391" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character constraint="than the internodes" constraintid="o4392" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="shape" src="d0_s5" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o4392" name="internode" name_original="internodes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>ligules membranous and ciliate or of hairs;</text>
      <biological_entity id="o4393" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="of hairs" value_original="of hairs" />
      </biological_entity>
      <biological_entity id="o4394" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <relation from="o4393" id="r699" name="consists_of" negation="false" src="d0_s6" to="o4394" />
    </statement>
    <statement id="d0_s7">
      <text>blades flat or folded.</text>
      <biological_entity id="o4395" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s7" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences spikelike panicles;</text>
      <biological_entity id="o4396" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o4397" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="spikelike" value_original="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches very short, with fewer than 10 spikelets, appressed to and partially embedded in the flattened, corky rachises;</text>
      <biological_entity id="o4399" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
      <biological_entity id="o4400" name="rachis" name_original="rachises" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="flattened" value_original="flattened" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s9" value="corky" value_original="corky" />
      </biological_entity>
      <relation from="o4398" id="r700" name="with" negation="false" src="d0_s9" to="o4399" />
      <relation from="o4398" id="r701" modifier="partially" name="embedded in the" negation="false" src="d0_s9" to="o4400" />
    </statement>
    <statement id="d0_s10">
      <text>disarticulation below the glumes, often with a segment of the branch.</text>
      <biological_entity id="o4398" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="very" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
        <character is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s9" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o4401" name="glume" name_original="glumes" src="d0_s10" type="structure" />
      <biological_entity id="o4402" name="segment" name_original="segment" src="d0_s10" type="structure" />
      <biological_entity id="o4403" name="branch" name_original="branch" src="d0_s10" type="structure" />
      <relation from="o4398" id="r702" name="below" negation="false" src="d0_s10" to="o4401" />
      <relation from="o4402" id="r703" name="part_of" negation="false" src="d0_s10" to="o4403" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets lanceolate to ovate, unawned, lower glumes oriented away from the branch axes.</text>
      <biological_entity id="o4404" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="ovate" />
      </biological_entity>
      <biological_entity constraint="lower" id="o4405" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character constraint="away-from branch axes" constraintid="o4406" is_modifier="false" name="orientation" src="d0_s11" value="oriented" value_original="oriented" />
      </biological_entity>
      <biological_entity constraint="branch" id="o4406" name="axis" name_original="axes" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes membranous;</text>
      <biological_entity id="o4407" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes scalelike, usually without veins;</text>
      <biological_entity constraint="lower" id="o4408" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o4409" name="vein" name_original="veins" src="d0_s13" type="structure" />
      <relation from="o4408" id="r704" modifier="usually" name="without" negation="false" src="d0_s13" to="o4409" />
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 5-7-veined;</text>
      <biological_entity constraint="upper" id="o4410" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower florets staminate or sterile, lemmas 3-9-veined;</text>
      <biological_entity constraint="lower" id="o4411" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o4412" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-9-veined" value_original="3-9-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper florets bisexual;</text>
      <biological_entity constraint="upper" id="o4413" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s16" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper lemmas longer than the glumes, papery to subcoriaceous, 3-5-veined;</text>
      <biological_entity constraint="upper" id="o4414" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character constraint="than the glumes" constraintid="o4415" is_modifier="false" name="length_or_size" src="d0_s17" value="longer" value_original="longer" />
        <character char_type="range_value" from="papery" name="texture" src="d0_s17" to="subcoriaceous" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o4415" name="glume" name_original="glumes" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>upper paleas generally indurate, 2-veined;</text>
      <biological_entity constraint="upper" id="o4416" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="generally" name="texture" src="d0_s18" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="architecture" src="d0_s18" value="2-veined" value_original="2-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>anthers 3.</text>
      <biological_entity id="o4417" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Caryopses lanceolate to ovate, often failing to develop, x = 9.</text>
      <biological_entity id="o4418" name="caryopsis" name_original="caryopses" src="d0_s20" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s20" to="ovate" />
      </biological_entity>
      <biological_entity constraint="x" id="o4419" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Stenotaphrum is a genus of seven species that usually grow on the seashore or near the coast, primarily along the Indian Ocean rim. Three species are endemic to Madagascar, and one species is thought to be native to the Flora region.</discussion>
  <references>
    <reference>Busey, P., T.K. Broschat, and B.J. Center. 1982. Classification of St. Augustinegrass. Crop Sci. (Madison) 22:469-473</reference>
    <reference>Sauer, J.D. 1972. Revision of Stenotaphrum (Gramineae: Paniceae) with attention to its historical geography. Brittonia 24:202-222.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;Okla.;Miss.;Tex.;La.;Calif.;Puerto Rico;Virgin Islands;Ala.;Tenn.;N.C.;S.C.;Pacific Islands (Hawaii);Ga.;Fla.;Mo.;N.Mex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>