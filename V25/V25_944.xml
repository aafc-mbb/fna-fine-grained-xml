<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">291</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Reeder" date="unknown" rank="tribe">ORCUTTIEAE</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="genus">ORCUTTIA</taxon_name>
    <taxon_name authority="Hoover" date="unknown" rank="species">inaequalis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe orcuttieae;genus orcuttia;species inaequalis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">San joaquin orcuttgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, conspicuously hairy, grayish.</text>
      <biological_entity id="o10237" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="conspicuously" name="pubescence" src="d0_s0" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 5-15 (25) cm, usually ascending to erect, occasionally spreading and forming mats.</text>
      <biological_entity id="o10238" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="25" value_original="25" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="usually ascending" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o10239" name="mat" name_original="mats" src="d0_s1" type="structure" />
      <relation from="o10238" id="r1684" name="forming" negation="false" src="d0_s1" to="o10239" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves usually without a collar line;</text>
      <biological_entity id="o10240" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity constraint="collar" id="o10241" name="line" name_original="line" src="d0_s2" type="structure" />
      <relation from="o10240" id="r1685" name="without" negation="false" src="d0_s2" to="o10241" />
    </statement>
    <statement id="d0_s3">
      <text>blades 1-4 cm long, 2-4 mm wide.</text>
      <biological_entity id="o10242" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikes 2-3.5 (5) cm, more or less capitate, usually densely congested;</text>
      <biological_entity id="o10243" name="spike" name_original="spikes" src="d0_s4" type="structure">
        <character name="atypical_some_measurement" src="d0_s4" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="3.5" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s4" value="capitate" value_original="capitate" />
        <character is_modifier="false" modifier="usually densely" name="architecture_or_arrangement" src="d0_s4" value="congested" value_original="congested" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lower and upper internodes 1-4 mm.</text>
      <biological_entity constraint="lower and upper" id="o10244" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets with 4-20 (30) florets.</text>
      <biological_entity id="o10245" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <biological_entity id="o10246" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="30" value_original="30" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <relation from="o10245" id="r1686" name="with" negation="false" src="d0_s6" to="o10246" />
    </statement>
    <statement id="d0_s7">
      <text>Glumes subequal, about 3 mm, irregularly toothed;</text>
      <biological_entity id="o10247" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
        <character name="some_measurement" src="d0_s7" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s7" value="toothed" value_original="toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lemmas 4-5 mm, teeth about 1/2 as long as the lemma, central tooth conspicuously longer than the others, teeth sharp, if awn-tipped, awns less than 0.5 mm;</text>
      <biological_entity id="o10248" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10249" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character constraint="as-long-as lemma" constraintid="o10250" name="quantity" src="d0_s8" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o10250" name="lemma" name_original="lemma" src="d0_s8" type="structure" />
      <biological_entity constraint="central" id="o10251" name="tooth" name_original="tooth" src="d0_s8" type="structure">
        <character constraint="than the others" constraintid="o10252" is_modifier="false" name="length_or_size" src="d0_s8" value="conspicuously longer" value_original="conspicuously longer" />
      </biological_entity>
      <biological_entity id="o10252" name="other" name_original="others" src="d0_s8" type="structure" />
      <biological_entity id="o10253" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="sharp" value_original="sharp" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="awn-tipped" value_original="awn-tipped" />
      </biological_entity>
      <biological_entity id="o10254" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>paleas about equal to the lemmas;</text>
      <biological_entity id="o10255" name="palea" name_original="paleas" src="d0_s9" type="structure">
        <character constraint="to lemmas" constraintid="o10256" is_modifier="false" name="variability" src="d0_s9" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o10256" name="lemma" name_original="lemmas" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>anthers about 2 mm.</text>
      <biological_entity id="o10257" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Caryopses 1.3-1.5 mm, broadly elliptical;</text>
      <biological_entity id="o10258" name="caryopsis" name_original="caryopses" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s11" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="arrangement_or_shape" src="d0_s11" value="elliptical" value_original="elliptical" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>embryos about as long as the caryopses.</text>
      <biological_entity id="o10260" name="caryopsis" name_original="caryopses" src="d0_s12" type="structure" />
      <relation from="o10259" id="r1687" name="as long as" negation="false" src="d0_s12" to="o10260" />
    </statement>
    <statement id="d0_s13">
      <text>2n = 24.</text>
      <biological_entity id="o10259" name="embryo" name_original="embryos" src="d0_s12" type="structure" />
      <biological_entity constraint="2n" id="o10261" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Orcuttia inaequalis grows at elevations below 575 m in Fresno, Madera, Merced, Stanislaus, and Tulare counties, California. It is listed as a threatened species by the U.S. Fish and Wildlife Service.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>