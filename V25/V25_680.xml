<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">129</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">SPOROBOLUS</taxon_name>
    <taxon_name authority="(Torr.) A. Gray" date="unknown" rank="species">cryptandrus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus sporobolus;species cryptandrus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sporobolus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cryptandrus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">fuscicolus</taxon_name>
    <taxon_hierarchy>genus sporobolus;species cryptandrus;subspecies fuscicolus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sporobolus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">cryptandrus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">fuscicolor</taxon_name>
    <taxon_hierarchy>genus sporobolus;species cryptandrus;variety fuscicolor</taxon_hierarchy>
  </taxon_identification>
  <number>17</number>
  <other_name type="common_name">Sand dropseed</other_name>
  <other_name type="common_name">Sporobole a fleurs cachees</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, not rhizomatous, bases not hard and knotty.</text>
      <biological_entity id="o18116" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18117" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="hard" value_original="hard" />
        <character is_modifier="false" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-100 (120) cm tall, 1-3.5 mm thick, erect to decumbent.</text>
      <biological_entity id="o18118" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="height" src="d0_s2" unit="cm" value="120" value_original="120" />
        <character char_type="range_value" from="30" from_unit="cm" name="height" src="d0_s2" to="100" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s2" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths rounded below, glabrous or scabridulous, margins sometimes ciliate distally, apices with conspicuous tufts of hairs, hairs to 4 mm;</text>
      <biological_entity id="o18119" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="below" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o18120" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="sometimes; distally" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o18121" name="apex" name_original="apices" src="d0_s3" type="structure" />
      <biological_entity id="o18122" name="tuft" name_original="tufts" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o18123" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <biological_entity id="o18124" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <relation from="o18121" id="r3050" name="with" negation="false" src="d0_s3" to="o18122" />
      <relation from="o18122" id="r3051" name="part_of" negation="false" src="d0_s3" to="o18123" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.5-1 mm;</text>
      <biological_entity id="o18125" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades (2) 5-26 cm long, 2-6 mm wide, flat to involute, glabrous abaxially, scabridulous to scabrous adaxially, margins scabridulous;</text>
      <biological_entity id="o18126" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="2" value_original="2" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="26" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="involute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="scabridulous" modifier="adaxially" name="relief" src="d0_s5" to="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>flag blades nearly perpendicular to the culms.</text>
      <biological_entity id="o18127" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o18128" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character constraint="to culms" constraintid="o18129" is_modifier="false" modifier="nearly" name="orientation" src="d0_s6" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o18129" name="culm" name_original="culms" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles 15-40 cm long, 2-12 (14) cm wide, longer than wide, initially contracted and spikelike, ultimately open and narrowly pyramidal;</text>
      <biological_entity id="o18130" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s7" to="40" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="14" value_original="14" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="12" to_unit="cm" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="longer than wide" value_original="longer than wide" />
        <character is_modifier="false" modifier="initially" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" modifier="ultimately" name="architecture" src="d0_s7" value="open" value_original="open" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="pyramidal" value_original="pyramidal" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>rachises straight, erect;</text>
      <biological_entity id="o18131" name="rachis" name_original="rachises" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower nodes with 1-2 (3) branches;</text>
      <biological_entity constraint="lower" id="o18132" name="node" name_original="nodes" src="d0_s9" type="structure" />
      <biological_entity id="o18133" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s9" value="3" value_original="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <relation from="o18132" id="r3052" name="with" negation="false" src="d0_s9" to="o18133" />
    </statement>
    <statement id="d0_s10">
      <text>primary branches 0.6-6 cm, appressed, spreading, or reflexed to 130° from the rachis, without spikelets on the lower 1/8 – 1/4;</text>
      <biological_entity constraint="primary" id="o18134" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s10" to="6" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character constraint="from rachis" constraintid="o18135" is_modifier="false" modifier="0-130°" name="orientation" src="d0_s10" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o18135" name="rachis" name_original="rachis" src="d0_s10" type="structure" />
      <biological_entity id="o18136" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
      <biological_entity constraint="lower" id="o18137" name="1/8-1/4" name_original="1/8-1/4" src="d0_s10" type="structure" />
      <relation from="o18134" id="r3053" name="without" negation="false" src="d0_s10" to="o18136" />
      <relation from="o18136" id="r3054" name="on" negation="false" src="d0_s10" to="o18137" />
    </statement>
    <statement id="d0_s11">
      <text>lower branches longest, included in the uppermost sheath;</text>
      <biological_entity constraint="lower" id="o18138" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="false" name="length" src="d0_s11" value="longest" value_original="longest" />
      </biological_entity>
      <biological_entity constraint="uppermost" id="o18139" name="sheath" name_original="sheath" src="d0_s11" type="structure" />
      <relation from="o18138" id="r3055" name="included in the" negation="false" src="d0_s11" to="o18139" />
    </statement>
    <statement id="d0_s12">
      <text>secondary branches appressed;</text>
      <biological_entity constraint="secondary" id="o18140" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>pulvini glabrous;</text>
      <biological_entity id="o18141" name="pulvinus" name_original="pulvini" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>pedicels 0.1-1.3 mm, appressed, glabrous or scabridulous.</text>
      <biological_entity id="o18142" name="pedicel" name_original="pedicels" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s14" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Spikelets 1.5-2.5 (2.7) mm, brownish, plumbeous, or purplish-tinged.</text>
      <biological_entity id="o18143" name="spikelet" name_original="spikelets" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="2.7" value_original="2.7" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="plumbeous" value_original="plumbeous" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish-tinged" value_original="purplish-tinged" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="plumbeous" value_original="plumbeous" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purplish-tinged" value_original="purplish-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Glumes unequal, linear-lanceolate to ovate, membranous;</text>
      <biological_entity id="o18144" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s16" to="ovate" />
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower glumes 0.6-1.1 mm;</text>
      <biological_entity constraint="lower" id="o18145" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s17" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper glumes 1.5-2.7 mm, at least 2/3 as long as the florets;</text>
      <biological_entity constraint="upper" id="o18146" name="glume" name_original="glumes" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s18" to="2.7" to_unit="mm" />
        <character constraint="as-long-as florets" constraintid="o18147" modifier="at least" name="quantity" src="d0_s18" value="2/3" value_original="2/3" />
      </biological_entity>
      <biological_entity id="o18147" name="floret" name_original="florets" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>lemmas 1.4-2.5 (2.7) mm, ovate to lanceolate, membranous, glabrous, acute;</text>
      <biological_entity id="o18148" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character name="atypical_some_measurement" src="d0_s19" unit="mm" value="2.7" value_original="2.7" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s19" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s19" to="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s19" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s19" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>paleas 1.2-2.4 mm, lanceolate, membranous;</text>
      <biological_entity id="o18149" name="palea" name_original="paleas" src="d0_s20" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s20" to="2.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s20" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="texture" src="d0_s20" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>anthers 0.5-1 mm, yellowish to purplish.</text>
      <biological_entity id="o18150" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s21" to="1" to_unit="mm" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s21" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>Fruits 0.7-1.1 mm, ellipsoid, light brownish to reddish-orange.</text>
    </statement>
    <statement id="d0_s23">
      <text>2n = 36, 38, 72.</text>
      <biological_entity id="o18151" name="fruit" name_original="fruits" src="d0_s22" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s22" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s22" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="light brownish" name="coloration" src="d0_s22" to="reddish-orange" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18152" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="36" value_original="36" />
        <character name="quantity" src="d0_s23" value="38" value_original="38" />
        <character name="quantity" src="d0_s23" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sporobolus cryptandrus is a widespread North American species, extending from Canada into Mexico. It grows in sandy soils and washes, on rocky slopes and calcareous ridges, and along roadsides in salt-desert scrub, pinyon-juniper woodlands, yellow pine forests, and desert grasslands. Its elevational range is 0-2900 m.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Wash.;Wis.;W.Va.;Kans.;N.Dak.;Nebr.;S.Dak.;Wyo.;N.H.;N.J.;N.Mex.;Tex.;La.;Conn.;N.Y.;N.C.;Tenn.;Pa.;R.I.;Nev.;Va.;Colo.;Calif.;Ark.;Vt.;Ill.;Ga.;Ind.;Iowa;Okla.;Ariz.;Idaho;Alta.;B.C.;Man.;Ont.;Que.;Sask.;Maine;Mass.;Ohio;Utah;Mo.;Minn.;Mich.;Mont.;Miss.;S.C.;Ky.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>