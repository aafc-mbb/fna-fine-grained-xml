<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">374</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Haller" date="unknown" rank="genus">DIGITARIA</taxon_name>
    <taxon_name authority="Hitchc." date="unknown" rank="species">pauciflora</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus digitaria;species pauciflora</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>19</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>not rhizomatous.</text>
      <biological_entity id="o8158" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-100 cm, erect to somewhat decumbent, not rooting at the lower nodes, usually branching at the aerial nodes.</text>
      <biological_entity id="o8159" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect to somewhat" value_original="erect to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o8160" is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character constraint="at nodes" constraintid="o8161" is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8160" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o8161" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths grayish-villous;</text>
      <biological_entity id="o8162" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="grayish-villous" value_original="grayish-villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 1.5-2 mm;</text>
      <biological_entity id="o8163" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades to 12 cm long, 2-2.2 mm wide, flat or folded, densely grayish-villous on both surfaces.</text>
      <biological_entity id="o8164" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="2.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s5" value="folded" value_original="folded" />
        <character constraint="on surfaces" constraintid="o8165" is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="grayish-villous" value_original="grayish-villous" />
      </biological_entity>
      <biological_entity id="o8165" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 2-3 spikelike primary branches, secondary branches not present;</text>
      <biological_entity id="o8166" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity constraint="primary" id="o8167" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="true" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o8168" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o8166" id="r1315" name="with" negation="false" src="d0_s6" to="o8167" />
    </statement>
    <statement id="d0_s7">
      <text>primary branches 5-11 cm, axes not winged, without spikelets or with widely spaced abortive spikelets on the proximal 1-1.5 cm, bearing spikelets in unequally pedicellate pairs at midlength;</text>
      <biological_entity constraint="primary" id="o8169" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s7" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8170" name="axis" name_original="axes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o8171" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o8172" name="pair" name_original="pairs" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="unequally" name="architecture" src="d0_s7" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity id="o8173" name="midlength" name_original="midlength" src="d0_s7" type="structure" />
      <relation from="o8170" id="r1316" modifier="without spikelets or with widely spaced abortive spikelets on the proximal axes 1-1.5 cm" name="bearing" negation="false" src="d0_s7" to="o8171" />
      <relation from="o8170" id="r1317" modifier="without spikelets or with widely spaced abortive spikelets on the proximal axes 1-1.5 cm" name="in" negation="false" src="d0_s7" to="o8172" />
      <relation from="o8172" id="r1318" name="at" negation="false" src="d0_s7" to="o8173" />
    </statement>
    <statement id="d0_s8">
      <text>shorter pedicels about 2 mm;</text>
      <biological_entity constraint="shorter" id="o8174" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="mm" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>longer pedicels about 3 mm, not adnate to the branch axes.</text>
      <biological_entity constraint="longer" id="o8175" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character name="some_measurement" src="d0_s9" unit="mm" value="3" value_original="3" />
        <character constraint="to branch axes" constraintid="o8176" is_modifier="false" modifier="not" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="branch" id="o8176" name="axis" name_original="axes" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 2.7-3.2 mm, appressed, glabrous.</text>
      <biological_entity id="o8177" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s10" to="3.2" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes minute, rounded, erose;</text>
      <biological_entity constraint="lower" id="o8178" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="minute" value_original="minute" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes slightly shorter than the spikelets, 3-veined, glabrous;</text>
      <biological_entity constraint="upper" id="o8179" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character constraint="than the spikelets" constraintid="o8180" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="slightly shorter" value_original="slightly shorter" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8180" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas 7-veined, glabrous;</text>
      <biological_entity constraint="lower" id="o8181" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="7-veined" value_original="7-veined" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper lemmas gray or yellow when immature, becoming purple at maturity.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = unknown.</text>
      <biological_entity constraint="upper" id="o8182" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="when immature" name="coloration" src="d0_s14" value="gray" value_original="gray" />
        <character is_modifier="false" modifier="when immature" name="coloration" src="d0_s14" value="yellow" value_original="yellow" />
        <character constraint="at maturity" is_modifier="false" modifier="becoming" name="coloration_or_density" src="d0_s14" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8183" name="chromosome" name_original="" src="d0_s15" type="structure" />
    </statement>
  </description>
  <discussion>Digitaria pauciflora is known only from the type collection, which was collected in pinelands of Dade County, Florida.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>