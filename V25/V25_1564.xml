<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">646</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Kuntze" date="unknown" rank="genus">BOTHRIOCHLOA</taxon_name>
    <taxon_name authority="(L.) A. Camus" date="unknown" rank="species">pertusa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus bothriochloa;species pertusa</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Andropogon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pertusus</taxon_name>
    <taxon_hierarchy>genus andropogon;species pertusus</taxon_hierarchy>
  </taxon_identification>
  <number>12</number>
  <other_name type="common_name">Pitted bluestem</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose or stoloniferous.</text>
      <biological_entity id="o866" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 100 cm, often decumbent or stoloniferous, freely branching;</text>
      <biological_entity id="o867" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="freely" name="architecture" src="d0_s1" value="branching" value_original="branching" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes bearded.</text>
      <biological_entity id="o868" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="bearded" value_original="bearded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal, green, sometimes glaucous;</text>
      <biological_entity id="o869" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o870" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths glabrous, keeled;</text>
      <biological_entity id="o871" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s4" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.7-1.5 mm;</text>
      <biological_entity id="o872" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 3-15 cm long, 3-4 mm wide, flat, margins and ligule regions hairy.</text>
      <biological_entity id="o873" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity id="o874" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="ligule" id="o875" name="region" name_original="regions" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 3-5 cm, fan-shaped, often purplish;</text>
      <biological_entity id="o876" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="5" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="fan--shaped" value_original="fan--shaped" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>rachises 0.2-2 cm, with 3-8 branches;</text>
      <biological_entity id="o877" name="rachis" name_original="rachises" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.2" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o878" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
      <relation from="o877" id="r149" name="with" negation="false" src="d0_s8" to="o878" />
    </statement>
    <statement id="d0_s9">
      <text>branches 3-4.5 cm, longer than the rachises, usually with 1 rame;</text>
      <biological_entity id="o880" name="rachis" name_original="rachises" src="d0_s9" type="structure" />
      <biological_entity id="o881" name="rame" name_original="rame" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="1" value_original="1" />
      </biological_entity>
      <relation from="o879" id="r150" modifier="usually" name="with" negation="false" src="d0_s9" to="o881" />
    </statement>
    <statement id="d0_s10">
      <text>rame internodes with villous margins, with 1-3 mm hairs.</text>
      <biological_entity id="o879" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s9" to="4.5" to_unit="cm" />
        <character constraint="than the rachises" constraintid="o880" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o882" name="internode" name_original="internodes" src="d0_s10" type="structure" />
      <biological_entity id="o883" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s10" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o884" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o882" id="r151" name="with" negation="false" src="d0_s10" to="o883" />
      <relation from="o882" id="r152" name="with" negation="false" src="d0_s10" to="o884" />
    </statement>
    <statement id="d0_s11">
      <text>Sessile spikelets 3-4 mm, lanceolate;</text>
      <biological_entity id="o885" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>callus hairs about 1 mm;</text>
      <biological_entity constraint="callus" id="o886" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character name="some_measurement" src="d0_s12" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower glumes sparsely hirtellous, with a prominent dorsal pit near the middle;</text>
      <biological_entity constraint="lower" id="o887" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity constraint="dorsal" id="o888" name="pit" name_original="pit" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o889" name="middle" name_original="middle" src="d0_s13" type="structure" />
      <relation from="o887" id="r153" name="with" negation="false" src="d0_s13" to="o888" />
      <relation from="o888" id="r154" name="near" negation="false" src="d0_s13" to="o889" />
    </statement>
    <statement id="d0_s14">
      <text>awns 10-17 mm;</text>
      <biological_entity id="o890" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s14" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1-1.8 mm, yellow.</text>
      <biological_entity id="o891" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pedicellate spikelets the same size as the sessile spikelets, sterile, pitted or not, occasionally with 2 pits.</text>
      <biological_entity id="o893" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character is_modifier="true" name="size" src="d0_s16" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o894" name="pit" name_original="pits" src="d0_s16" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
      <relation from="o892" id="r155" name="as" negation="false" src="d0_s16" to="o893" />
      <relation from="o892" id="r156" modifier="occasionally" name="with" negation="false" src="d0_s16" to="o894" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 40, 60.</text>
      <biological_entity id="o892" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s16" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="relief" src="d0_s16" value="pitted" value_original="pitted" />
        <character name="relief" src="d0_s16" value="not" value_original="not" />
      </biological_entity>
      <biological_entity constraint="2n" id="o895" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="40" value_original="40" />
        <character name="quantity" src="d0_s17" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bothriochloa pertusa is native to the Eastern Hemisphere, and was introduced to the southern United States as a warm-season pasture grass. It now grows in disturbed, moist, grassy places and pastures in the region, at elevations of 2-200 m. It has not persisted at all locations shown on the map.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Md.;Tex.;La.;Virgin Islands;Pacific Islands (Hawaii);Miss.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>