<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">269</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="J. Presl" date="unknown" rank="genus">OPIZIA</taxon_name>
    <taxon_name authority="J. Presl" date="unknown" rank="species">stolonifera</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus opizia;species stolonifera</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Acapulco grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms erect or geniculate.</text>
      <biological_entity id="o19832" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="shape" src="d0_s0" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Sheaths mostly glabrous, often with a few hairs on either side of the collar;</text>
      <biological_entity id="o19833" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o19834" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o19835" name="side" name_original="side" src="d0_s1" type="structure" />
      <biological_entity id="o19836" name="collar" name_original="collar" src="d0_s1" type="structure" />
      <relation from="o19833" id="r3354" modifier="often" name="with" negation="false" src="d0_s1" to="o19834" />
      <relation from="o19834" id="r3355" name="on" negation="false" src="d0_s1" to="o19835" />
      <relation from="o19835" id="r3356" name="part_of" negation="false" src="d0_s1" to="o19836" />
    </statement>
    <statement id="d0_s2">
      <text>ligules 1-1.5 mm;</text>
      <biological_entity id="o19837" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades to 10 cm long, 2-3 mm wide, glabrous abaxially, mostly glabrous or scabrous adaxially, midveins often with a few hairs.</text>
      <biological_entity id="o19838" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o19839" name="midvein" name_original="midveins" src="d0_s3" type="structure" />
      <biological_entity id="o19840" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="few" value_original="few" />
      </biological_entity>
      <relation from="o19839" id="r3357" name="with" negation="false" src="d0_s3" to="o19840" />
    </statement>
    <statement id="d0_s4">
      <text>Staminate culms 5-15 (30) cm;</text>
      <biological_entity id="o19841" name="culm" name_original="culms" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="staminate" value_original="staminate" />
        <character name="atypical_some_measurement" src="d0_s4" unit="cm" value="30" value_original="30" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s4" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>panicles with 1-6 branches;</text>
      <biological_entity id="o19842" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <biological_entity id="o19843" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <relation from="o19842" id="r3358" name="with" negation="false" src="d0_s5" to="o19843" />
    </statement>
    <statement id="d0_s6">
      <text>branches 0.5-2 cm;</text>
      <biological_entity id="o19844" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikelets 3-4 mm, glabrous;</text>
      <biological_entity id="o19845" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>anthers 2-2.5 mm, pale.</text>
      <biological_entity id="o19846" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="pale" value_original="pale" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pistillate culms to 10 cm;</text>
      <biological_entity id="o19847" name="culm" name_original="culms" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>spikes with 6-12 spikelets (lower nodes sometimes with short branches);</text>
      <biological_entity id="o19848" name="spike" name_original="spikes" src="d0_s10" type="structure" />
      <biological_entity id="o19849" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s10" to="12" />
      </biological_entity>
      <relation from="o19848" id="r3359" name="with" negation="false" src="d0_s10" to="o19849" />
    </statement>
    <statement id="d0_s11">
      <text>pistillate spikelets 2.8-4 mm;</text>
      <biological_entity id="o19850" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s11" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>glumes to 3 mm;</text>
      <biological_entity id="o19851" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemma bodies 2-2.5 mm, 3-lobed and 3-awned, awns 3.4-6.8 mm;</text>
      <biological_entity constraint="lemma" id="o19852" name="body" name_original="bodies" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="3-lobed" value_original="3-lobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="3-awned" value_original="3-awned" />
      </biological_entity>
      <biological_entity id="o19853" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s13" to="6.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>palea keels exceeding the lemmas;</text>
      <biological_entity constraint="palea" id="o19854" name="keel" name_original="keels" src="d0_s14" type="structure" />
      <biological_entity id="o19855" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <relation from="o19854" id="r3360" name="exceeding the" negation="false" src="d0_s14" to="o19855" />
    </statement>
    <statement id="d0_s15">
      <text>rudiment 3-awned.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = unknown.</text>
      <biological_entity id="o19856" name="rudiment" name_original="rudiment" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="3-awned" value_original="3-awned" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19857" name="chromosome" name_original="" src="d0_s16" type="structure" />
    </statement>
  </description>
  <discussion>Opizia stolonifera grows along dry roadsides in Florida. No pistillate plants have been found in the Flora region.</discussion>
  
</bio:treatment>