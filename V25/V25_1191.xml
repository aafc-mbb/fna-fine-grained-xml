<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">438</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="genus">DICHANTHELIUM</taxon_name>
    <taxon_name authority="(Hitchc.) Freckmann &amp; Lelong" date="unknown" rank="section">Ensifolia</taxon_name>
    <taxon_name authority="(Trin.) Freckmann &amp; Lelong" date="unknown" rank="species">chamaelonche</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus dichanthelium;section ensifolia;species chamaelonche</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">chamaelonche</taxon_name>
    <taxon_hierarchy>genus panicum;species chamaelonche</taxon_hierarchy>
  </taxon_identification>
  <number>22</number>
  <other_name type="common_name">Small-seeded panicgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually densely cespitose, with caudices.</text>
      <biological_entity id="o1221" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1222" name="caudex" name_original="caudices" src="d0_s0" type="structure" />
      <relation from="o1221" id="r207" name="with" negation="false" src="d0_s0" to="o1222" />
    </statement>
    <statement id="d0_s1">
      <text>Basal rosettes well-differentiated;</text>
      <biological_entity constraint="basal" id="o1223" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" name="variability" src="d0_s1" value="well-differentiated" value_original="well-differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 1-5 cm, ovate to lanceolate.</text>
      <biological_entity id="o1224" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 5-45 cm tall, 0.2-0.8 mm thick, erect, often purplish;</text>
      <biological_entity id="o1225" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="height" src="d0_s3" to="45" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="thickness" src="d0_s3" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s3" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes glabrous or sparsely pubescent;</text>
      <biological_entity id="o1226" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>internodes often ascending-pubescent below;</text>
    </statement>
    <statement id="d0_s6">
      <text>fall phase branching extensively from the basal nodes, usually forming very dense cushions.</text>
      <biological_entity id="o1227" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="often; below" name="pubescence" src="d0_s5" value="ascending-pubescent" value_original="ascending-pubescent" />
        <character constraint="from basal nodes" constraintid="o1228" is_modifier="false" name="architecture" src="d0_s6" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1228" name="node" name_original="nodes" src="d0_s6" type="structure" />
      <biological_entity id="o1229" name="cushion" name_original="cushions" src="d0_s6" type="structure">
        <character is_modifier="true" modifier="very" name="density" src="d0_s6" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o1228" id="r208" modifier="usually" name="forming" negation="false" src="d0_s6" to="o1229" />
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves 3-5;</text>
      <biological_entity constraint="cauline" id="o1230" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sheaths mostly shorter than the internodes, often purplish, glabrous or sparsely pubescent, margins often sparsely ciliate;</text>
      <biological_entity id="o1231" name="sheath" name_original="sheaths" src="d0_s8" type="structure">
        <character constraint="than the internodes" constraintid="o1232" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="mostly shorter" value_original="mostly shorter" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s8" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o1232" name="internode" name_original="internodes" src="d0_s8" type="structure" />
      <biological_entity id="o1233" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often sparsely" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ligules 0.2-0.5 mm, of hairs, without adjacent pseudoligules;</text>
      <biological_entity id="o1234" name="ligule" name_original="ligules" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1235" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <biological_entity id="o1236" name="pseudoligule" name_original="pseudoligules" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o1234" id="r209" name="consists_of" negation="false" src="d0_s9" to="o1235" />
      <relation from="o1234" id="r210" name="without" negation="false" src="d0_s9" to="o1236" />
    </statement>
    <statement id="d0_s10">
      <text>blades 2-5 cm long (rarely longer), 1-4 mm wide, flat or involute, rather firm, ascending, often purplish, usually glabrous on both surfaces, bases subcordate, often with a few long, stiff cilia, margins narrowly white, cartilaginous, and scabridulous, blades of the flag leaves only slightly shorter than those of the lower leaves.</text>
      <biological_entity id="o1237" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s10" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s10" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="rather" name="texture" src="d0_s10" value="firm" value_original="firm" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s10" value="purplish" value_original="purplish" />
        <character constraint="on surfaces" constraintid="o1238" is_modifier="false" modifier="usually" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1238" name="surface" name_original="surfaces" src="d0_s10" type="structure" />
      <biological_entity id="o1239" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o1240" name="cilium" name_original="cilia" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="few" value_original="few" />
        <character is_modifier="true" name="length_or_size" src="d0_s10" value="long" value_original="long" />
        <character is_modifier="true" name="fragility" src="d0_s10" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o1241" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="narrowly" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s10" value="cartilaginous" value_original="cartilaginous" />
        <character is_modifier="false" name="relief" src="d0_s10" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o1242" name="blade" name_original="blades" src="d0_s10" type="structure" />
      <biological_entity id="o1243" name="flag" name_original="flag" src="d0_s10" type="structure" />
      <biological_entity id="o1244" name="leaf" name_original="leaves" src="d0_s10" type="structure" />
      <relation from="o1239" id="r211" modifier="often" name="with" negation="false" src="d0_s10" to="o1240" />
      <relation from="o1242" id="r212" name="part_of" negation="false" src="d0_s10" to="o1243" />
      <relation from="o1242" id="r213" name="part_of" negation="false" src="d0_s10" to="o1244" />
    </statement>
    <statement id="d0_s11">
      <text>Primary panicles 1.5-5 cm (seldom longer), nearly as wide as long, delicate, dense;</text>
      <biological_entity constraint="primary" id="o1245" name="panicle" name_original="panicles" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s11" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="length_or_size" src="d0_s11" value="long" value_original="long" />
        <character is_modifier="false" name="fragility" src="d0_s11" value="delicate" value_original="delicate" />
        <character is_modifier="false" name="density" src="d0_s11" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>branches numerous, flexuous, spreading, often purplish, glabrous or faintly scabridulous.</text>
      <biological_entity id="o1246" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s12" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="course" src="d0_s12" value="flexuous" value_original="flexuous" />
        <character is_modifier="false" name="orientation" src="d0_s12" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s12" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="faintly" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 1.1-1.5 mm long, 0.7-1 mm wide, broadly ellipsoid or obovoid, often purple-tinged, glabrous or puberulent, obtuse or subacute.</text>
      <biological_entity id="o1247" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s13" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s13" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s13" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s13" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Lower glumes approximately 1/3 as long as the spikelets, broadly acute or obtuse;</text>
      <biological_entity constraint="lower" id="o1248" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character constraint="as-long-as spikelets" constraintid="o1249" name="quantity" src="d0_s14" value="1/3" value_original="1/3" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s14" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o1249" name="spikelet" name_original="spikelets" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>upper glumes and lower lemmas subequal or the glumes slightly shorter than the lemmas;</text>
      <biological_entity constraint="upper" id="o1250" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o1251" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="size" src="d0_s15" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity id="o1252" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character constraint="than the lemmas" constraintid="o1253" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o1253" name="lemma" name_original="lemmas" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o1254" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s16" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper florets 0.9-1.2 mm, ellipsoid, apices exceeding the upper glumes and lower lemmas, subacute.</text>
      <biological_entity constraint="upper" id="o1255" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.9" from_unit="mm" name="some_measurement" src="d0_s17" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity id="o1256" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="subacute" value_original="subacute" />
      </biological_entity>
      <biological_entity constraint="upper" id="o1257" name="glume" name_original="glumes" src="d0_s17" type="structure" />
      <biological_entity constraint="upper lower" id="o1258" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <relation from="o1256" id="r214" name="exceeding" negation="false" src="d0_s17" to="o1257" />
      <relation from="o1256" id="r215" name="exceeding" negation="false" src="d0_s17" to="o1258" />
    </statement>
  </description>
  <discussion>Dichanthelium chamaelonche grows in low, open, sandy, coastal pine woods, savannahs, and moist depressions in sand dunes. It is restricted to the south-eastern United States.</discussion>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Culms 5-20 cm tall, glabrous or puberulent; spikelets 1.3-1.5 mm long, puberulent</description>
      <determination>Dichanthelium chamaelonche subsp. breve</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Culms 10-45 cm tall, glabrous; spikelets 1.1-1.4 mm long, glabrous</description>
      <determination>Dichanthelium chamaelonche subsp. chamaelonche</determination>
    </key_statement>
  </key>
</bio:treatment>