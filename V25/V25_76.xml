<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">48</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Nash" date="unknown" rank="genus">BLEPHARONEURON</taxon_name>
    <taxon_name authority="(Torr.) Nash" date="unknown" rank="species">tricholepis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus blepharoneuron;species tricholepis</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Hairy dropseed</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>densely cespitose.</text>
      <biological_entity id="o12558" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-70 cm, erect, glabrous and smooth or scabrous just below the nodes.</text>
      <biological_entity id="o12559" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="70" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s2" value="smooth" value_original="smooth" />
        <character constraint="just below nodes" constraintid="o12560" is_modifier="false" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o12560" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Ligules (0.3) 0.7-2 (2.7) mm, hyaline to membranous, entire;</text>
      <biological_entity id="o12561" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character name="atypical_some_measurement" src="d0_s3" unit="mm" value="0.3" value_original="0.3" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 1-15 cm long, 0.6-2.5 mm wide, scabrous.</text>
      <biological_entity id="o12562" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s4" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 3-25 cm long, 1-10 cm wide;</text>
      <biological_entity id="o12563" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="25" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches ascending;</text>
      <biological_entity id="o12564" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>pedicels 2-9 mm, straight or flexuous.</text>
      <biological_entity id="o12565" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" name="course" src="d0_s7" value="flexuous" value_original="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glumes (1.5) 1.8-2.6 (3) mm, often appearing 3-veined because of the characteristic infolding of the margins;</text>
      <biological_entity id="o12566" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s8" to="2.6" to_unit="mm" />
        <character constraint="of margins" constraintid="o12567" is_modifier="false" modifier="often" name="architecture" src="d0_s8" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o12567" name="margin" name_original="margins" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>lemmas (2) 2.3-3.5 (3.9) mm;</text>
      <biological_entity id="o12568" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>anthers 1.2-2.1 mm, brownish.</text>
      <biological_entity id="o12569" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Caryopses 1.2-1.4 mm. 2n = 16.</text>
      <biological_entity id="o12570" name="caryopsis" name_original="caryopses" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s11" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12571" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="16" value_original="16" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Blepharoneuron tricholepis grows in dry, rocky to sandy slopes, dry meadows, and open woods in pine-oak-madrone forests from Utah and Colorado to the state of Puebla, Mexico, at elevations of 700-3660 m. It flowers from mid-June through November.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah;Ariz.;Colo.;N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>