<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Jesus Valdes-Reyna;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">45</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Willd. ex Rydb." date="unknown" rank="genus">DASYOCHLOA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus dasyochloa</taxon_hierarchy>
  </taxon_identification>
  <number>17.15</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>stoloniferous, sometimes mat-forming.</text>
      <biological_entity id="o14701" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s1" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms (1) 4-15 cm, initially erect, event¬ually bending and rooting at the base of the inflorescence.</text>
      <biological_entity id="o14702" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="initially" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="ually" name="orientation" src="d0_s2" value="bending" value_original="bending" />
        <character constraint="at base" constraintid="o14703" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o14703" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o14704" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure" />
      <relation from="o14703" id="r2443" name="part_of" negation="false" src="d0_s2" to="o14704" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves not basally aggregated on the primary culms;</text>
      <biological_entity id="o14705" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="on primary culms" constraintid="o14706" is_modifier="false" modifier="not basally" name="arrangement" src="d0_s3" value="aggregated" value_original="aggregated" />
      </biological_entity>
      <biological_entity constraint="primary" id="o14706" name="culm" name_original="culms" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>sheaths with a tuft of hairs to 2 mm at the throat;</text>
      <biological_entity id="o14707" name="sheath" name_original="sheaths" src="d0_s4" type="structure" />
      <biological_entity id="o14708" name="tuft" name_original="tuft" src="d0_s4" type="structure">
        <character char_type="range_value" constraint="at throat" constraintid="o14710" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14709" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o14710" name="throat" name_original="throat" src="d0_s4" type="structure" />
      <relation from="o14707" id="r2444" name="with" negation="false" src="d0_s4" to="o14708" />
      <relation from="o14708" id="r2445" name="part_of" negation="false" src="d0_s4" to="o14709" />
    </statement>
    <statement id="d0_s5">
      <text>ligules of hairs;</text>
      <biological_entity id="o14711" name="ligule" name_original="ligules" src="d0_s5" type="structure" />
      <biological_entity id="o14712" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <relation from="o14711" id="r2446" name="consists_of" negation="false" src="d0_s5" to="o14712" />
    </statement>
    <statement id="d0_s6">
      <text>blades involute.</text>
      <biological_entity id="o14713" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, short, dense panicles of spikelike branches, each subtended by leafy bracts and exceeded by the upper leaves;</text>
      <biological_entity id="o14714" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o14715" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="true" name="density" src="d0_s7" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o14716" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o14717" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity constraint="upper" id="o14718" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <relation from="o14715" id="r2447" name="part_of" negation="false" src="d0_s7" to="o14716" />
      <relation from="o14715" id="r2448" name="subtended by" negation="false" src="d0_s7" to="o14717" />
      <relation from="o14715" id="r2449" name="exceeded by the" negation="false" src="d0_s7" to="o14718" />
    </statement>
    <statement id="d0_s8">
      <text>branches with 2-4 subsessile to shortly pedicellate spikelets.</text>
      <biological_entity id="o14719" name="branch" name_original="branches" src="d0_s8" type="structure" />
      <biological_entity id="o14720" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="4" />
        <character char_type="range_value" from="subsessile" is_modifier="true" name="architecture" src="d0_s8" to="shortly pedicellate" />
      </biological_entity>
      <relation from="o14719" id="r2450" name="with" negation="false" src="d0_s8" to="o14720" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets laterally compressed, with 4-10 florets;</text>
      <biological_entity id="o14722" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s9" to="10" />
      </biological_entity>
      <relation from="o14721" id="r2451" name="with" negation="false" src="d0_s9" to="o14722" />
    </statement>
    <statement id="d0_s10">
      <text>disarticulation above the glumes.</text>
      <biological_entity id="o14721" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o14723" name="glume" name_original="glumes" src="d0_s10" type="structure" />
      <relation from="o14721" id="r2452" name="above" negation="false" src="d0_s10" to="o14723" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes subequal to the adjacent lemmas, glabrous, 1-veined, rounded or weakly keeled, shortly awned to mucronate;</text>
      <biological_entity id="o14724" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character constraint="to lemmas" constraintid="o14725" is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character char_type="range_value" from="weakly keeled shortly awned" name="shape" src="d0_s11" to="mucronate" />
        <character char_type="range_value" from="weakly keeled shortly awned" name="shape" src="d0_s11" to="mucronate" />
      </biological_entity>
      <biological_entity id="o14725" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>florets bisexual;</text>
      <biological_entity id="o14726" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas rounded or weakly keeled, densely pilose on the lower 1/2 and on the margins, thinly membranous, 3-veined, 2-lobed, lobes about 1/2 as long as the lemmas and obtuse, midveins extending into awns as long as or longer than the lobes, lateral-veins not excurrent;</text>
      <biological_entity id="o14727" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s13" value="keeled" value_original="keeled" />
        <character constraint="on the lower 1/2 and on margins" constraintid="o14728" is_modifier="false" modifier="densely" name="pubescence" src="d0_s13" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="thinly" name="texture" notes="" src="d0_s13" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="shape" src="d0_s13" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o14728" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <biological_entity id="o14729" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character constraint="as-long-as lemmas" constraintid="o14730" name="quantity" src="d0_s13" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="shape" notes="" src="d0_s13" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o14730" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity id="o14731" name="midvein" name_original="midveins" src="d0_s13" type="structure" />
      <biological_entity id="o14732" name="awn" name_original="awns" src="d0_s13" type="structure" />
      <biological_entity id="o14734" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14733" name="lobe" name_original="lobes" src="d0_s13" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14735" name="lateral-vein" name_original="lateral-veins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s13" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <relation from="o14731" id="r2453" name="extending into" negation="false" src="d0_s13" to="o14732" />
      <relation from="o14731" id="r2454" name="as long as" negation="false" src="d0_s13" to="o14734" />
    </statement>
    <statement id="d0_s14">
      <text>paleas about as long as the lemmas;</text>
      <biological_entity id="o14736" name="palea" name_original="paleas" src="d0_s14" type="structure" />
      <biological_entity id="o14737" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 3.</text>
      <biological_entity id="o14738" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses oval in cross-section, translucent;</text>
      <biological_entity id="o14739" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character constraint="in cross-section" constraintid="o14740" is_modifier="false" name="shape" src="d0_s16" value="oval" value_original="oval" />
        <character is_modifier="false" name="coloration_or_reflectance" notes="" src="d0_s16" value="translucent" value_original="translucent" />
      </biological_entity>
      <biological_entity id="o14740" name="cross-section" name_original="cross-section" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>embryos more than 1/2 as long as the caryopses.</text>
      <biological_entity id="o14742" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>x = 8.</text>
      <biological_entity id="o14741" name="embryo" name_original="embryos" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="1" value_original="1" />
        <character constraint="as-long-as caryopses" constraintid="o14742" name="quantity" src="d0_s17" value="/2" value_original="/2" />
      </biological_entity>
      <biological_entity constraint="x" id="o14743" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dasyochloa is a monotypic genus that is restricted to the United States and Mexico. It has been included in the past in each of the following: Triodia, Tridens, and Erioneuron. Dasyochloa differs from all three of these genera, but resembles Munroa, in its leafy-bracteate inflorescence (Caro 1981). Seedlings of Dasyochloa, like those of Erioneuron, are shaggy-white-villous. This indumentum is composed of myriads of hairlike, water soluble crystals that wash off in water. They are the product of transpiration and evaporation.</discussion>
  <references>
    <reference>Caro, J.A. 1981. Rehabilitacion del genero Dasyochloa (Gramineae). Dominguezia 2:1-17</reference>
    <reference>Sanchez, E. 1983. Dasyochloa Willdenow ex Rydberg (Poaceae). Lilloa 36:131-138</reference>
    <reference>Valdes-Reyna, J. and S.L. Hatch. 1997. A revision of Erioneuron and Dasyochloa (Poaceae: Eragrostideae). Sida 17:645-666.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Colo.;N.Mex.;Tex.;Utah;Calif.;Wyo.;Ariz.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>