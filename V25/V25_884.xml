<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">262</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Lag." date="unknown" rank="genus">BOUTELOUA</taxon_name>
    <taxon_name authority="(Desv.) A. Gray" date="unknown" rank="subgenus">Chondrosum</taxon_name>
    <taxon_name authority="Lag." date="unknown" rank="species">hirsuta</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">hirsuta</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus bouteloua;subgenus chondrosum;species hirsuta;subspecies hirsuta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bouteloua</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hirsuta</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">glandulosa</taxon_name>
    <taxon_hierarchy>genus bouteloua;species hirsuta;variety glandulosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Bouteloua</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">glandulosa</taxon_name>
    <taxon_hierarchy>genus bouteloua;species glandulosa</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Hairy grama</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely or densely cespitose, sometimes stoloniferous.</text>
      <biological_entity id="o22228" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 15-60 cm, usually decumbent and branched basally, sometimes erect, branched or unbranched from the aerial nodes;</text>
      <biological_entity id="o22229" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="basally" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="sometimes" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character constraint="from nodes" constraintid="o22230" is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o22230" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes usually 4-6;</text>
      <biological_entity id="o22231" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s2" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes glabrous or sparsely to densely pubescent with papillose-based hairs.</text>
      <biological_entity id="o22232" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="with hairs" constraintid="o22233" from="glabrous or" name="pubescence" src="d0_s3" to="sparsely densely pubescent" />
      </biological_entity>
      <biological_entity id="o22233" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves basally clustered, sometimes not strongly so;</text>
      <biological_entity id="o22234" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="basally" name="arrangement_or_growth_form" src="d0_s4" value="clustered" value_original="clustered" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths glabrous or pubescent, hairs not papillose-based, sometimes scabrous.</text>
      <biological_entity id="o22235" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22236" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 1-4 branches on 0.7-7.5 (9.2) cm rachises or digitate;</text>
      <biological_entity id="o22237" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="digitate" value_original="digitate" />
      </biological_entity>
      <biological_entity id="o22238" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <biological_entity id="o22239" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_some_measurement" src="d0_s6" unit="cm" value="9.2" value_original="9.2" />
        <character char_type="range_value" from="0.7" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s6" to="7.5" to_unit="cm" />
      </biological_entity>
      <relation from="o22237" id="r3786" name="with" negation="false" src="d0_s6" to="o22238" />
      <relation from="o22238" id="r3787" name="on" negation="false" src="d0_s6" to="o22239" />
    </statement>
    <statement id="d0_s7">
      <text>branches 1-4.</text>
      <biological_entity id="o22240" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Anthers 2-2.5 mm;</text>
      <biological_entity id="o22241" name="anther" name_original="anthers" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>rachilla segments subtending second florets without a distal tuft of hairs.</text>
      <biological_entity constraint="rachilla" id="o22242" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o22243" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity constraint="distal" id="o22244" name="tuft" name_original="tuft" src="d0_s9" type="structure" />
      <biological_entity id="o22245" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <relation from="o22243" id="r3788" name="without" negation="false" src="d0_s9" to="o22244" />
      <relation from="o22244" id="r3789" name="part_of" negation="false" src="d0_s9" to="o22245" />
    </statement>
    <statement id="d0_s10">
      <text>Caryopses 1.4-2 mm. 2n = 20, 40, 50, 60;</text>
      <biological_entity constraint="2n" id="o22247" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="20" value_original="20" />
        <character name="quantity" src="d0_s10" value="40" value_original="40" />
        <character name="quantity" src="d0_s10" value="50" value_original="50" />
        <character name="quantity" src="d0_s10" value="60" value_original="60" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>numerous dysploid numbers also reported.</text>
      <biological_entity id="o22246" name="caryopsis" name_original="caryopses" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character is_modifier="false" name="quantity" src="d0_s11" value="numerous" value_original="numerous" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bouteloua hirsuta subsp. hirsuta grows from the open plains to slightly shaded openings in woods and brush on well-drained, often rocky, soils at 50-300 m. It is morphologically, ecologically, and cytologically more variable than subsp. pectinata. Its range extends from North Dakota and Minnesota to central Mexico. In the northern portion of its range, it is not densely tufted and the culms are decumbent and branched; in the southwestern United States and northern Mexico, it grows in isolated, dense clumps, with erect, stout, unbranched culms and mostly basal leaves.</discussion>
  
</bio:treatment>