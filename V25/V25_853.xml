<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">243</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Linda Bea Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">SPARTINA</taxon_name>
    <taxon_name authority="(Trin.) Merr. ex Hitch." date="unknown" rank="species">spartinae</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus spartina;species spartinae</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Gulf cordgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, not rhizomatous.</text>
      <biological_entity id="o4801" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 40-200 cm, in large clumps, hard, usually glabrous, nodes frequently exposed.</text>
      <biological_entity id="o4802" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="200" to_unit="cm" />
        <character is_modifier="false" name="texture" notes="" src="d0_s1" value="hard" value_original="hard" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4803" name="clump" name_original="clumps" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o4804" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="frequently" name="prominence" src="d0_s1" value="exposed" value_original="exposed" />
      </biological_entity>
      <relation from="o4802" id="r779" name="in" negation="false" src="d0_s1" to="o4803" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths mostly glabrous, throat glabrous, sometimes scabrous;</text>
      <biological_entity id="o4805" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4806" name="throat" name_original="throat" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence_or_relief" src="d0_s2" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules 1-2 mm;</text>
      <biological_entity id="o4807" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s3" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 1.5-4.5 mm wide, involute when fresh, abaxial surfaces glabrous, adaxial surfaces and margins scabrous.</text>
      <biological_entity id="o4808" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s4" to="4.5" to_unit="mm" />
        <character is_modifier="false" modifier="when fresh" name="shape_or_vernation" src="d0_s4" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4809" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4810" name="surface" name_original="surfaces" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4811" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles 6-70 cm, smoothly cylindrical in outline, with (6) 15-75 branches, internodes shorter than the branches;</text>
      <biological_entity id="o4812" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s5" to="70" to_unit="cm" />
        <character constraint="in outline" constraintid="o4813" is_modifier="false" modifier="smoothly" name="shape" src="d0_s5" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
      <biological_entity id="o4813" name="outline" name_original="outline" src="d0_s5" type="structure" />
      <biological_entity id="o4814" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s5" value="6" value_original="6" />
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s5" to="75" />
      </biological_entity>
      <biological_entity id="o4815" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character constraint="than the branches" constraintid="o4816" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o4816" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <relation from="o4812" id="r780" name="with" negation="false" src="d0_s5" to="o4814" />
    </statement>
    <statement id="d0_s6">
      <text>branches 0.5-4 (7) cm, lower branches often longer than those above, all branches tightly appressed, closely imbricate, with 10-60 spikelets.</text>
      <biological_entity id="o4817" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character name="atypical_some_measurement" src="d0_s6" unit="cm" value="7" value_original="7" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o4818" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character constraint="than those above , all branches" constraintid="o4819" is_modifier="false" name="length_or_size" src="d0_s6" value="often longer" value_original="often longer" />
        <character is_modifier="false" modifier="closely" name="arrangement" notes="" src="d0_s6" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o4819" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="tightly" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o4820" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s6" to="60" />
      </biological_entity>
      <relation from="o4818" id="r781" name="with" negation="false" src="d0_s6" to="o4820" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 5-8 (10) mm.</text>
      <biological_entity id="o4821" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glumes glabrous or hispidulous, keels hispid;</text>
      <biological_entity id="o4822" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o4823" name="keel" name_original="keels" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="hispid" value_original="hispid" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower glumes 2-8 mm, acuminate;</text>
      <biological_entity constraint="lower" id="o4824" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 4-8 (10) mm, acuminate to obtuse, keels hispid, lateral-veins 1-2, if 2, these on either side of the keel;</text>
      <biological_entity constraint="upper" id="o4825" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s10" to="obtuse" />
      </biological_entity>
      <biological_entity id="o4826" name="keel" name_original="keels" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o4827" name="lateral-vein" name_original="lateral-veins" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s10" to="2" />
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4828" name="side" name_original="side" src="d0_s10" type="structure" />
      <biological_entity id="o4829" name="keel" name_original="keel" src="d0_s10" type="structure" />
      <relation from="o4827" id="r782" name="on" negation="false" src="d0_s10" to="o4828" />
      <relation from="o4828" id="r783" modifier="either" name="part_of" negation="false" src="d0_s10" to="o4829" />
    </statement>
    <statement id="d0_s11">
      <text>lemmas 5-6 mm, glabrous or hispidulous, keels hispid over the distal 2/3, apices usually acuminate or apiculate, rarely obtuse;</text>
      <biological_entity id="o4830" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
      <biological_entity id="o4831" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character constraint="over distal 2/3" constraintid="o4832" is_modifier="false" name="pubescence" src="d0_s11" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity constraint="distal" id="o4832" name="2/3" name_original="2/3" src="d0_s11" type="structure" />
      <biological_entity id="o4833" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="apiculate" value_original="apiculate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 3-5 mm, dark red to purple.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 40.</text>
      <biological_entity id="o4834" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="dark red" name="coloration" src="d0_s12" to="purple" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4835" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Spartina spartinae grows from the Gulf coast through Mexico to Costa Rica in North America and, in South America, in Paraguay and northern Argentina. In the United States, it grows in sandy beaches, roadsides, ditches, wet meadows, and arid pastures near the coast, the most inland collection being 60 miles from the coast. In other parts of its range it sometimes grows well inland in saline soils where Pinus palustris (longleaf pine) is dominant or co-dominant.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.;Fla.;Ala.;Miss.;La.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>