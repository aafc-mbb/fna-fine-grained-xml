<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">156</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="Scribn." date="unknown" rank="species">glabrifloris</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species glabrifloris</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Inland muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous, not cespitose.</text>
      <biological_entity id="o4728" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-100 cm tall, 0.5-2 mm thick, herbaceous, ascending or decumbent, bushy and much branched above;</text>
      <biological_entity id="o4729" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="height" src="d0_s2" to="100" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s2" to="2" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s2" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="bushy" value_original="bushy" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes smooth and shiny for most of their length, scabridulous or strigulose below the nodes.</text>
      <biological_entity id="o4730" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character constraint="for" is_modifier="false" name="length" src="d0_s3" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
        <character constraint="below nodes" constraintid="o4731" is_modifier="false" name="pubescence" src="d0_s3" value="strigulose" value_original="strigulose" />
      </biological_entity>
      <biological_entity id="o4731" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous, margins hyaline;</text>
      <biological_entity id="o4732" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4733" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.5-1.5 mm, membranous, truncate, lacerate-ciliolate;</text>
      <biological_entity id="o4734" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="lacerate-ciliolate" value_original="lacerate-ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 3-8 cm long, (1.5) 2-4 mm wide, flat, glabrous.</text>
      <biological_entity id="o4735" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 2-6.5 cm long, 0.2-0.8 cm wide, lobed, dense;</text>
      <biological_entity id="o4736" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s7" to="6.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s7" to="0.8" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches 0.3-2.5 cm, ascending, closely appressed;</text>
      <biological_entity id="o4737" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.3" from_unit="cm" name="some_measurement" src="d0_s8" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="closely" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>axillary panicles common, partly included in or exserted from the subtending sheaths;</text>
      <biological_entity constraint="axillary" id="o4738" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s9" value="common" value_original="common" />
      </biological_entity>
      <biological_entity id="o4739" name="exserted" name_original="exserted" src="d0_s9" type="structure" />
      <biological_entity constraint="subtending" id="o4740" name="sheath" name_original="sheaths" src="d0_s9" type="structure" />
      <relation from="o4738" id="r766" modifier="partly" name="included in" negation="false" src="d0_s9" to="o4739" />
      <relation from="o4738" id="r767" name="from" negation="false" src="d0_s9" to="o4740" />
    </statement>
    <statement id="d0_s10">
      <text>pedicels 0.3-2.2 mm, scabrous to strigulose.</text>
      <biological_entity id="o4741" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s10" to="strigulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.2-3.5 mm.</text>
      <biological_entity id="o4742" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes subequal, 1.5-3.5 mm, from 3/4 as long as to longer than the lemmas, smooth or scabridulous near the apices, 1-veined;</text>
      <biological_entity id="o4743" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character constraint="near apices" constraintid="o4746" is_modifier="false" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o4745" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="3/4" value_original="3/4" />
        <character is_modifier="true" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o4744" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="3/4" value_original="3/4" />
        <character is_modifier="true" name="length_or_size" src="d0_s12" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o4746" name="apex" name_original="apices" src="d0_s12" type="structure" />
      <relation from="o4744" id="r768" name="from" negation="false" src="d0_s12" to="o4745" />
    </statement>
    <statement id="d0_s13">
      <text>upper glumes acuminate, acute, unawned or awned, awns to 1.2 mm;</text>
      <biological_entity constraint="upper" id="o4747" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o4748" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas 2.2-3.1 mm, narrowly lanceolate, shiny, stramineous or purplish, usually completely glabrous (rarely with a few appressed hairs), apices scabridulous, acuminate, unawned;</text>
      <biological_entity id="o4749" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.1" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="reflectance" src="d0_s14" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="stramineous" value_original="stramineous" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="purplish" value_original="purplish" />
        <character is_modifier="false" modifier="usually completely" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o4750" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>paleas 2.2-3.1 mm, narrowly lanceolate;</text>
      <biological_entity id="o4751" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s15" to="3.1" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 0.3-0.5 mm, yellow to purplish.</text>
      <biological_entity id="o4752" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.5" to_unit="mm" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s16" to="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 1.2-1.4 mm, fusiform, brown.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 40.</text>
      <biological_entity id="o4753" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s17" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s17" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4754" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia glabrifloris grows at the edge of dry forests, in prairies, thickets, and along roadsides in pine and oak associations, at elevations of 20-400 m. It is restricted to the southern portion of the central contiguous United States. It resembles M. frondosa, but differs from that species in its glabrous lemmas and shorter caryopses (1.2-1.4 mm rather than 1.6-1.9 mm).</discussion>
  
</bio:treatment>