<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">340</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Andy Sudkamp</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Caro" date="unknown" rank="subfamily">ARISTIDOIDEAE</taxon_name>
    <taxon_name authority="C.E. Hubb." date="unknown" rank="tribe">ARISTIDEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ARISTIDA</taxon_name>
    <taxon_name authority="Poir." date="unknown" rank="species">purpurascens</taxon_name>
    <taxon_name authority="(Trin.) Allred" date="unknown" rank="variety">virgata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily aristidoideae;tribe aristideae;genus aristida;species purpurascens;variety virgata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aristida</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virgata</taxon_name>
    <taxon_hierarchy>genus aristida;species virgata</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Lower sheaths shorter or longer than the internodes, glabrous.</text>
      <biological_entity constraint="lower" id="o21146" name="sheath" name_original="sheaths" src="d0_s0" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="shorter" value_original="shorter" />
        <character constraint="than the internodes" constraintid="o21147" is_modifier="false" name="length_or_size" src="d0_s0" value="shorter or longer" value_original="shorter or longer" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21147" name="internode" name_original="internodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Glumes 6-7 mm, equal or the lower glumes slightly longer;</text>
      <biological_entity id="o21149" name="glume" name_original="glumes" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s1" to="7" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s1" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="lower" id="o21150" name="glume" name_original="glumes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="slightly" name="length_or_size" src="d0_s1" value="longer" value_original="longer" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>central awns 13-20 mm, about twice as thick as the lateral awns, divaricate to reflexed at maturity;</text>
      <biological_entity constraint="central" id="o21151" name="awn" name_original="awns" src="d0_s2" type="structure">
        <character char_type="range_value" from="13" from_unit="mm" name="some_measurement" src="d0_s2" to="20" to_unit="mm" />
        <character constraint="awn" constraintid="o21152" is_modifier="false" name="thickness" src="d0_s2" value="2 times as thick as the lateral awns" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="divaricate" value_original="divaricate" />
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s2" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o21152" name="awn" name_original="awns" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>lateral awns 8-13 mm, erect to ascending.</text>
      <biological_entity constraint="lateral" id="o21153" name="awn" name_original="awns" src="d0_s3" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s3" to="13" to_unit="mm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="ascending" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Aristida purpurascens var. virgata grows in wet or moist areas such as seepage bogs, sandy pinelands, and wet prairies of the southeastern United States.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;N.J.;Miss.;Tex.;La.;Del.;Ala.;Tenn.;N.C.;S.C.;Va.;Ark.;Ga.;Ky.;Fla.;N.Y.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>