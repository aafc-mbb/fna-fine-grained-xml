<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kelly W. Allred;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">624</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="genus">TRACHYPOGON</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus trachypogon</taxon_hierarchy>
  </taxon_identification>
  <number>26.07</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose or shortly rhizomatous.</text>
      <biological_entity id="o14774" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="shortly" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-200 cm, unbranched;</text>
      <biological_entity id="o14775" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="200" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes semi-solid.</text>
      <biological_entity id="o14776" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="semi-solid" value_original="semi-solid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves cauline, not aromatic;</text>
      <biological_entity id="o14777" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="position" src="d0_s4" value="cauline" value_original="cauline" />
        <character is_modifier="false" modifier="not" name="odor" src="d0_s4" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths shorter than the internodes, rounded;</text>
      <biological_entity id="o14778" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character constraint="than the internodes" constraintid="o14779" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o14779" name="internode" name_original="internodes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>ligules membranous;</text>
      <biological_entity id="o14780" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades flat to involute.</text>
      <biological_entity id="o14781" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="flat" name="shape" src="d0_s7" to="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, solitary racemes of heterogamous subsessile-pedicellate spikelets pairs (rarely of 2 digitate spikelike branches), axes slender, without a translucent median groove;</text>
      <biological_entity id="o14782" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o14783" name="raceme" name_original="racemes" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o14784" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="heterogamous" value_original="heterogamous" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="subsessile-pedicellate" value_original="subsessile-pedicellate" />
      </biological_entity>
      <biological_entity constraint="median" id="o14786" name="groove" name_original="groove" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s8" value="translucent" value_original="translucent" />
      </biological_entity>
      <relation from="o14783" id="r2464" name="part_of" negation="false" src="d0_s8" to="o14784" />
      <relation from="o14785" id="r2465" name="without" negation="false" src="d0_s8" to="o14786" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation beneath the pedicellate spikelets.</text>
      <biological_entity id="o14785" name="axis" name_original="axes" src="d0_s8" type="structure">
        <character is_modifier="false" name="size" src="d0_s8" value="slender" value_original="slender" />
      </biological_entity>
      <biological_entity id="o14787" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o14785" id="r2466" name="beneath" negation="false" src="d0_s9" to="o14787" />
    </statement>
    <statement id="d0_s10">
      <text>Subsessile spikelets staminate or sterile, without a callus and unawned, otherwise similar to the pedicellate spikelets.</text>
      <biological_entity id="o14788" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o14789" name="callus" name_original="callus" src="d0_s10" type="structure" />
      <biological_entity id="o14790" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o14788" id="r2467" name="without" negation="false" src="d0_s10" to="o14789" />
      <relation from="o14788" id="r2468" modifier="otherwise" name="to" negation="false" src="d0_s10" to="o14790" />
    </statement>
    <statement id="d0_s11">
      <text>Pedicels slender, not fused to the rames axes.</text>
      <biological_entity id="o14791" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="slender" value_original="slender" />
        <character constraint="to axes" constraintid="o14792" is_modifier="false" modifier="not" name="fusion" src="d0_s11" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o14792" name="axis" name_original="axes" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>Pedicellate spikelets bisexual;</text>
      <biological_entity id="o14793" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>calluses sharp, strigose;</text>
      <biological_entity id="o14794" name="callus" name_original="calluses" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="sharp" value_original="sharp" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>glumes firm, enclosing the florets;</text>
      <biological_entity id="o14795" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="firm" value_original="firm" />
      </biological_entity>
      <biological_entity id="o14796" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s14" value="enclosing" value_original="enclosing" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower glumes several-veined, encircling the upper glumes;</text>
      <biological_entity constraint="lower" id="o14797" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="several-veined" value_original="several-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o14798" name="glume" name_original="glumes" src="d0_s15" type="structure" />
      <relation from="o14797" id="r2469" name="encircling the" negation="false" src="d0_s15" to="o14798" />
    </statement>
    <statement id="d0_s16">
      <text>upper glumes 3-veined;</text>
      <biological_entity constraint="upper" id="o14799" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o14800" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s17" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper florets bisexual, lemmas firm but hyaline at the base, tapering to an awn;</text>
      <biological_entity constraint="upper" id="o14801" name="floret" name_original="florets" src="d0_s18" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s18" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o14802" name="lemma" name_original="lemmas" src="d0_s18" type="structure">
        <character is_modifier="false" name="texture" src="d0_s18" value="firm" value_original="firm" />
        <character constraint="at base" constraintid="o14803" is_modifier="false" name="coloration" src="d0_s18" value="hyaline" value_original="hyaline" />
        <character constraint="to awn" constraintid="o14804" is_modifier="false" name="shape" notes="" src="d0_s18" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o14803" name="base" name_original="base" src="d0_s18" type="structure" />
      <biological_entity id="o14804" name="awn" name_original="awn" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>awns (4) 6-15 cm, twisted, pubescent to plumose;</text>
      <biological_entity id="o14805" name="awn" name_original="awns" src="d0_s19" type="structure">
        <character name="atypical_some_measurement" src="d0_s19" unit="cm" value="4" value_original="4" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s19" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s19" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="shape" src="d0_s19" value="plumose" value_original="plumose" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>paleas absent;</text>
      <biological_entity id="o14806" name="palea" name_original="paleas" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>anthers 3.</text>
    </statement>
    <statement id="d0_s22">
      <text>x = 10.</text>
      <biological_entity id="o14807" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="x" id="o14808" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Trachypogon is a tropical or warm-temperate genus that is native to Africa and tropical to subtropical America. Estimates of the number of species included range from one to ten. One species, Trachypogon secundus, is native to the Flora region, but some taxonomists (e.g., Davila 1994) include it in T. plumosus (Humb. &amp; Bonpl. ex Willd.) Nees and others (e.g., Judziewicz 1990) include it, T. plumosus, and various other taxa in T. spicatus (L. f.) Kuntze. The traditional treatment and nomenclature for North American plants is retained here, pending formal study of the taxa involved.</discussion>
  <references>
    <reference>Davila, RD. 1994. Trachypogon Nees. Pp. 380-381 in G. Davidse, M. Sousa S., and A.O. Chater (eds.). Flora Mesoamericana, vol. 6: Alismataceae a Cyperaceae. Universidad Nacional Autonoma de Mexico, Institute de Biologia, Mexico, D.F., Mexico. 543 pp.</reference>
    <reference>Judziewicz, E.J. 1990. Flora of the Guianas: 187. Poaceae (Gramineae). Series A: Phanerogams, Fascicle 8 (series ed. A.R.A. Gorts-Van Rijn). Koeltz Scientific Books, Koenigstein, Germany. 727 pp.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.;N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>