<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">592</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="Lam." date="unknown" rank="species">laxum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species laxum</taxon_hierarchy>
  </taxon_identification>
  <number>33</number>
  <other_name type="common_name">Coconut paspalum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose to short rhizomatous.</text>
      <biological_entity id="o15839" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 80-110 cm, erect;</text>
      <biological_entity id="o15840" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s2" to="110" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o15841" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous, sparsely pubescent apically;</text>
      <biological_entity id="o15842" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; apically" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-2.9 mm;</text>
      <biological_entity id="o15843" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 9-41 cm long, 3-7 mm wide, mostly involute, pubescent above, glabrous below.</text>
      <biological_entity id="o15844" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s6" to="41" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="below" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal, with 1-5 (10) racemosely arranged branches;</text>
      <biological_entity id="o15845" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o15846" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="10" value_original="10" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o15845" id="r2653" name="with" negation="false" src="d0_s7" to="o15846" />
    </statement>
    <statement id="d0_s8">
      <text>branches 1.9-11.4 cm, erect to divergent, terminating in a spikelet;</text>
      <biological_entity id="o15847" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.9" from_unit="cm" name="some_measurement" src="d0_s8" to="11.4" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
      </biological_entity>
      <biological_entity id="o15848" name="spikelet" name_original="spikelet" src="d0_s8" type="structure" />
      <relation from="o15847" id="r2654" name="terminating in a" negation="false" src="d0_s8" to="o15848" />
    </statement>
    <statement id="d0_s9">
      <text>branch axes 0.4-0.7 mm wide, very narrowly winged, scabrous.</text>
      <biological_entity constraint="branch" id="o15849" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s9" to="0.7" to_unit="mm" />
        <character is_modifier="false" modifier="very narrowly" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 1.6-2.2 mm long, 1.1-1.3 mm wide, paired, imbricate, appressed to the branch axes, elliptic-obovate to ovate.</text>
      <biological_entity id="o15850" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s10" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s10" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="paired" value_original="paired" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="imbricate" value_original="imbricate" />
        <character constraint="to branch axes" constraintid="o15851" is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="elliptic-obovate" name="shape" notes="" src="d0_s10" to="ovate" />
      </biological_entity>
      <biological_entity constraint="branch" id="o15851" name="axis" name_original="axes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes absent;</text>
      <biological_entity constraint="lower" id="o15852" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes shortly pubescent, 5-veined, margins entire;</text>
      <biological_entity constraint="upper" id="o15853" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o15854" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas glabrous or shortly pubescent, lacking ribs over the veins, 3-veined, margins entire;</text>
      <biological_entity constraint="lower" id="o15855" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o15856" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="lacking" value_original="lacking" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s13" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o15857" name="vein" name_original="veins" src="d0_s13" type="structure" />
      <biological_entity id="o15858" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o15856" id="r2655" name="over" negation="false" src="d0_s13" to="o15857" />
    </statement>
    <statement id="d0_s14">
      <text>upper florets 1.4-2 mm, white to stramineous.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 60.</text>
      <biological_entity constraint="upper" id="o15859" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s14" to="stramineous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15860" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum laxum grows in hammocks and along roads, often in sandy or limestone soils. It used to be common in coconut groves, hence the English-language name. It grows in southern Florida, the Antilles, and Belize.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Virgin Islands;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>