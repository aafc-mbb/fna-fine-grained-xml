<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">482</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="(Nash) Zuloaga" date="unknown" rank="subgenus">Agrostoidea</taxon_name>
    <taxon_name authority="Freckmann &amp; Lelong" date="unknown" rank="section">Antidotalia</taxon_name>
    <taxon_name authority="Retz." date="unknown" rank="species">antidotale</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus agrostoidea;section antidotalia;species antidotale</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>28</number>
  <other_name type="common_name">Blue panicgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, rhizomatous, rhizomes about 1 cm thick, knotted, pubescent, with large, scalelike leaves.</text>
      <biological_entity id="o22906" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o22907" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character name="thickness" src="d0_s1" unit="cm" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s1" value="knotted" value_original="knotted" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22908" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s1" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <relation from="o22907" id="r3893" name="with" negation="false" src="d0_s1" to="o22908" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-300 cm tall, 2-4 mm thick, often compressed, erect or ascending, hard, becoming almost woody;</text>
      <biological_entity id="o22909" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="height" src="d0_s2" to="300" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s2" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s2" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="texture" src="d0_s2" value="hard" value_original="hard" />
        <character is_modifier="false" modifier="becoming almost" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes swollen, glabrous or pubescent;</text>
      <biological_entity id="o22910" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="swollen" value_original="swollen" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes glabrous, glaucous.</text>
      <biological_entity id="o22911" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sheaths not keeled, shorter than or equal to the internodes, glabrous or the lower sheaths at least partially pubescent, hairs papillose-based;</text>
      <biological_entity id="o22912" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="keeled" value_original="keeled" />
        <character constraint="than or equal to the internodes" constraintid="o22913" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22913" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="true" name="variability" src="d0_s5" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="lower" id="o22914" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="at-least partially" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22915" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.3-1.5 mm;</text>
      <biological_entity id="o22916" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 10-60 cm long, 3-20 mm wide, elongate, flat, abaxial surfaces and margins scabrous, adaxial surfaces occasionally pubescent near the base, with prominent, white midveins, bases rounded to narrowed.</text>
      <biological_entity id="o22917" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s7" to="60" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22918" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o22919" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o22920" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character constraint="near base" constraintid="o22921" is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22921" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o22922" name="midvein" name_original="midveins" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o22923" name="base" name_original="bases" src="d0_s7" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s7" to="narrowed" />
      </biological_entity>
      <relation from="o22920" id="r3894" name="with" negation="false" src="d0_s7" to="o22922" />
    </statement>
    <statement id="d0_s8">
      <text>Panicles 10-45 cm, to 1/2 as wide as long, open or somewhat contracted, with many spikelets;</text>
      <biological_entity id="o22924" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s8" to="45" to_unit="cm" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s8" to="1/2" />
        <character is_modifier="false" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="open" value_original="open" />
        <character is_modifier="false" modifier="somewhat" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o22925" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="many" value_original="many" />
      </biological_entity>
      <relation from="o22924" id="r3895" name="with" negation="false" src="d0_s8" to="o22925" />
    </statement>
    <statement id="d0_s9">
      <text>branches 4-12 cm, opposite or alternate, ascending to spreading;</text>
      <biological_entity id="o22926" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s9" to="12" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s9" to="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels 0.3-2.5 mm, scabridulous to scabrous, appressed to diverging less than 45° from the branch axes.</text>
      <biological_entity id="o22927" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="scabridulous" name="relief" src="d0_s10" to="scabrous" />
        <character char_type="range_value" constraint="from branch axes" constraintid="o22928" from="appressed" modifier="0-45°" name="orientation" src="d0_s10" to="diverging" />
      </biological_entity>
      <biological_entity constraint="branch" id="o22928" name="axis" name_original="axes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.4-3.4 mm long, 1-1.3 mm wide, ellipsoid-lanceoloid to narrowly ovoid, often purplish, glabrous, acute.</text>
      <biological_entity id="o22929" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.4" from_unit="mm" name="length" src="d0_s11" to="3.4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s11" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="ellipsoid-lanceoloid" name="shape" src="d0_s11" to="narrowly ovoid" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s11" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Lower glumes 1.4-2.2 mm, 1/3 – 1/2 as long as the spikelets, 3-5-veined, obtuse;</text>
      <biological_entity constraint="lower" id="o22930" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s12" to="2.2" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o22931" from="1/3" name="quantity" src="d0_s12" to="1/2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="3-5-veined" value_original="3-5-veined" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o22931" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper glumes and lower lemmas subequal, glabrous, 5-9-veined, margins scarious, acute;</text>
      <biological_entity constraint="upper" id="o22932" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="5-9-veined" value_original="5-9-veined" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o22933" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="5-9-veined" value_original="5-9-veined" />
      </biological_entity>
      <biological_entity id="o22934" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="texture" src="d0_s13" value="scarious" value_original="scarious" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower florets staminate;</text>
      <biological_entity constraint="lower" id="o22935" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper florets 1.8-2.8 mm long, 0.9-1.1 mm wide, smooth, lustrous, acute.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 18, 36.</text>
      <biological_entity constraint="upper" id="o22936" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s15" to="2.8" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s15" to="1.1" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s15" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22937" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="18" value_original="18" />
        <character name="quantity" src="d0_s16" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Panicum antidotale is native to India. It is grown in the Flora region as a forage grass, primarily in the southwestern United States. It is now established in the region, being found in open, disturbed areas and fields.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.;Tex.;Utah;Calif.;Ala.;N.C.;S.C.;Pacific Islands (Hawaii);Ariz.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>