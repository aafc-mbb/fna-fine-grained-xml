<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">267</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Lag." date="unknown" rank="genus">BOUTELOUA</taxon_name>
    <taxon_name authority="(Desv.) A. Gray" date="unknown" rank="subgenus">Chondrosum</taxon_name>
    <taxon_name authority="Vasey" date="unknown" rank="species">breviseta</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus bouteloua;subgenus chondrosum;species breviseta</taxon_hierarchy>
  </taxon_identification>
  <number>18</number>
  <other_name type="common_name">Gypsum grama</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>sometimes cespitose, sometimes rhizomatous, rhizomes 1-3 mm thick, short or elongate, scaly.</text>
      <biological_entity id="o14300" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="sometimes" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o14301" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s1" to="3" to_unit="mm" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-40 cm, erect, somewhat woody at the base, branching at the base and, in late fall, sometimes at the aerial nodes;</text>
      <biological_entity id="o14302" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o14303" is_modifier="false" modifier="somewhat" name="texture" src="d0_s2" value="woody" value_original="woody" />
        <character constraint="at base" constraintid="o14304" is_modifier="false" name="architecture" notes="" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o14303" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o14304" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o14305" name="fall" name_original="fall" src="d0_s2" type="structure" />
      <biological_entity id="o14306" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
      </biological_entity>
      <relation from="o14302" id="r2355" name="in" negation="false" src="d0_s2" to="o14305" />
      <relation from="o14302" id="r2356" modifier="sometimes" name="at" negation="false" src="d0_s2" to="o14306" />
    </statement>
    <statement id="d0_s3">
      <text>nodes usually 4-5;</text>
      <biological_entity id="o14307" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes glabrous, distal portions of the lower internodes with a thick, white, chalky bloom.</text>
      <biological_entity id="o14308" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14309" name="portion" name_original="portions" src="d0_s4" type="structure" />
      <biological_entity constraint="lower" id="o14310" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity id="o14311" name="bloom" name_original="bloom" src="d0_s4" type="structure">
        <character is_modifier="true" name="width" src="d0_s4" value="thick" value_original="thick" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="chalky" value_original="chalky" />
      </biological_entity>
      <relation from="o14309" id="r2357" name="part_of" negation="false" src="d0_s4" to="o14310" />
      <relation from="o14309" id="r2358" name="with" negation="false" src="d0_s4" to="o14311" />
    </statement>
    <statement id="d0_s5">
      <text>Ligules 0.1-0.2 mm, of hairs;</text>
      <biological_entity id="o14312" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14313" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <relation from="o14312" id="r2359" name="consists_of" negation="false" src="d0_s5" to="o14313" />
    </statement>
    <statement id="d0_s6">
      <text>blades 1-4 (7) cm long, 0.5-2 mm wide, flat basally, involute and arcuate to reflexed distally.</text>
      <biological_entity id="o14314" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="7" value_original="7" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="basally" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" name="course_or_shape" src="d0_s6" value="arcuate" value_original="arcuate" />
        <character is_modifier="false" modifier="distally" name="orientation" src="d0_s6" value="reflexed" value_original="reflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 2-A cm, with 1-3 (4) branches;</text>
      <biological_entity id="o14315" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
      <biological_entity id="o14316" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="4" value_original="4" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o14315" id="r2360" name="with" negation="false" src="d0_s7" to="o14316" />
    </statement>
    <statement id="d0_s8">
      <text>branches 15-37 mm, persistent, straight to slightly arcuate, mostly appressed, stramineous, with 30-45 spikelets, branches terminating in a reduced, needlelike, 2-5 mm spikelet;</text>
      <biological_entity id="o14317" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s8" to="37" to_unit="mm" />
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="straight" name="course" src="d0_s8" to="slightly arcuate" />
        <character is_modifier="false" modifier="mostly" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity id="o14318" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" is_modifier="true" name="quantity" src="d0_s8" to="45" />
      </biological_entity>
      <biological_entity id="o14320" name="spikelet" name_original="spikelet" src="d0_s8" type="structure">
        <character is_modifier="true" name="size" src="d0_s8" value="reduced" value_original="reduced" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s8" value="needlelike" value_original="needlelike" />
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o14317" id="r2361" name="with" negation="false" src="d0_s8" to="o14318" />
      <relation from="o14319" id="r2362" name="terminating in a" negation="false" src="d0_s8" to="o14320" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation above the glumes.</text>
      <biological_entity id="o14319" name="branch" name_original="branches" src="d0_s8" type="structure" />
      <biological_entity id="o14321" name="glume" name_original="glumes" src="d0_s9" type="structure" />
      <relation from="o14319" id="r2363" name="above" negation="false" src="d0_s9" to="o14321" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets pectinate, with 1 bisexual floret and 1-2 rudimentary florets.</text>
      <biological_entity id="o14322" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="pectinate" value_original="pectinate" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s10" value="rudimentary" value_original="rudimentary" />
      </biological_entity>
      <biological_entity id="o14323" name="floret" name_original="floret" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o14324" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s10" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o14322" id="r2364" name="with" negation="false" src="d0_s10" to="o14323" />
      <relation from="o14322" id="r2365" name="with" negation="false" src="d0_s10" to="o14324" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes acute to acuminate, glabrous or sparsely short-hairy, hairs not papillose-based;</text>
      <biological_entity id="o14325" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="short-hairy" value_original="short-hairy" />
      </biological_entity>
      <biological_entity id="o14326" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 2-2.5 mm;</text>
      <biological_entity constraint="lower" id="o14327" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 2-3.5 mm;</text>
      <biological_entity constraint="upper" id="o14328" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lowest lemmas 2.5-4 mm, sparsely to densely hairy, 3-awned, awns slightly shorter than the lemma bodies, central awns flanked by 2 membranous lobes;</text>
      <biological_entity constraint="lowest" id="o14329" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s14" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="3-awned" value_original="3-awned" />
      </biological_entity>
      <biological_entity id="o14330" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character constraint="than the lemma bodies" constraintid="o14331" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o14331" name="body" name_original="bodies" src="d0_s14" type="structure" />
      <biological_entity constraint="central" id="o14332" name="awn" name_original="awns" src="d0_s14" type="structure" />
      <biological_entity id="o14333" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="true" name="texture" src="d0_s14" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o14332" id="r2366" name="flanked by" negation="false" src="d0_s14" to="o14333" />
    </statement>
    <statement id="d0_s15">
      <text>lowest paleas about 4.5 mm, mostly or completely glabrous, sometimes puberulent distally, acute to acuminate, unawned, veins not excurrent;</text>
      <biological_entity constraint="lowest" id="o14334" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character name="some_measurement" src="d0_s15" unit="mm" value="4.5" value_original="4.5" />
        <character is_modifier="false" modifier="mostly; completely" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes; distally" name="pubescence" src="d0_s15" value="puberulent" value_original="puberulent" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s15" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>second florets about 4.5 mm, 3-awned, awns 3-5 mm;</text>
      <biological_entity id="o14335" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s15" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o14336" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="4.5" value_original="4.5" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s16" value="3-awned" value_original="3-awned" />
      </biological_entity>
      <biological_entity id="o14337" name="awn" name_original="awns" src="d0_s16" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s16" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>rachilla segments subtending second florets with densely pubescent apices;</text>
      <biological_entity constraint="rachilla" id="o14338" name="segment" name_original="segments" src="d0_s17" type="structure">
        <character is_modifier="false" name="position" src="d0_s17" value="subtending" value_original="subtending" />
      </biological_entity>
      <biological_entity id="o14340" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="densely" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o14339" id="r2367" name="with" negation="false" src="d0_s17" to="o14340" />
    </statement>
    <statement id="d0_s18">
      <text>third florets, if present, flabellate scales, 1-awned.</text>
      <biological_entity id="o14339" name="floret" name_original="florets" src="d0_s17" type="structure" />
      <biological_entity id="o14341" name="floret" name_original="florets" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o14342" name="scale" name_original="scales" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="flabellate" value_original="flabellate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s18" value="1-awned" value_original="1-awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Caryopses 1-1.2 mm long, about 0.4 mm wide.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 20.</text>
      <biological_entity id="o14343" name="caryopsis" name_original="caryopses" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s19" to="1.2" to_unit="mm" />
        <character name="width" src="d0_s19" unit="mm" value="0.4" value_original="0.4" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14344" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bouteloua breviseta is locally abundant on gypsum soils in southeastern New Mexico and the northern portion of the Trans Pecos region in Texas. It also grows in the state of Chihuahua, Mexico. Reeder and Reeder (1980) provide an excellent discussion of B. breviseta and B. ramosa.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>