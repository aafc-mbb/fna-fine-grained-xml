<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">405</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Nash" date="unknown" rank="genus">SACCIOLEPIS</taxon_name>
    <taxon_name authority="(L.) Nash" date="unknown" rank="species">striata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus sacciolepis;species striata</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">American cupscale</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>with or without rhizomes.</text>
      <biological_entity id="o15468" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15469" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure" />
      <relation from="o15468" id="r2596" name="with or without" negation="false" src="d0_s1" to="o15469" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 80-150 cm, erect or decumbent, sprawling, trailing, often rooting at the lower nodes;</text>
      <biological_entity id="o15470" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="80" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="trailing" value_original="trailing" />
        <character constraint="at lower nodes" constraintid="o15471" is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o15471" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous or pubescent.</text>
      <biological_entity id="o15472" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths mostly glabrous or with papillose-based hairs, margins sometimes ciliate;</text>
      <biological_entity id="o15473" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="with papillose-based hairs" value_original="with papillose-based hairs" />
      </biological_entity>
      <biological_entity id="o15474" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o15475" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o15473" id="r2597" name="with" negation="false" src="d0_s4" to="o15474" />
    </statement>
    <statement id="d0_s5">
      <text>collars pubescent adaxially, usually glabrous abaxially, margins ciliate;</text>
      <biological_entity id="o15476" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="usually; abaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o15477" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.2-0.7 mm, membranous, ciliate;</text>
      <biological_entity id="o15478" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades (2.3) 4-28.5 cm long, 2.9-22 mm wide, glabrous or with papillose-based hairs, bases cordate, margins ciliate basally.</text>
      <biological_entity id="o15479" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="2.3" value_original="2.3" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="28.5" to_unit="cm" />
        <character char_type="range_value" from="2.9" from_unit="mm" name="width" src="d0_s7" to="22" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="with papillose-based hairs" value_original="with papillose-based hairs" />
      </biological_entity>
      <biological_entity id="o15480" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o15481" name="base" name_original="bases" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o15482" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="basally" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o15479" id="r2598" name="with" negation="false" src="d0_s7" to="o15480" />
    </statement>
    <statement id="d0_s8">
      <text>Panicles 3-29.5 cm long, 0.7-3.1 cm wide, contracted;</text>
      <biological_entity id="o15483" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s8" to="29.5" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s8" to="3.1" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s8" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches ascending to appressed, not fused to the rachises;</text>
      <biological_entity id="o15484" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s9" to="appressed" />
        <character constraint="to rachises" constraintid="o15485" is_modifier="false" modifier="not" name="fusion" src="d0_s9" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o15485" name="rachis" name_original="rachises" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lower branches (0.4) 1-11.5 cm;</text>
      <biological_entity constraint="lower" id="o15486" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="0.4" value_original="0.4" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="11.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicels 0.1-5.1 mm.</text>
      <biological_entity id="o15487" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="5.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 2.9-5 mm, green (occasionally mostly purple), tips of the upper glumes and the sterile lemmas dark purple.</text>
      <biological_entity id="o15488" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o15489" name="tip" name_original="tips" src="d0_s12" type="structure" />
      <biological_entity constraint="upper" id="o15490" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <biological_entity id="o15491" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark purple" value_original="dark purple" />
      </biological_entity>
      <relation from="o15489" id="r2599" name="part_of" negation="false" src="d0_s12" to="o15490" />
    </statement>
    <statement id="d0_s13">
      <text>Lower glumes 0.8-1.7 mm, 3-5-veined, margins hyaline;</text>
      <biological_entity constraint="lower" id="o15492" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o15493" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 2.9-4.8 mm, conspicuously saccate, 11 (12) -veined, sparsely puberulent in the distal 1/3, obtuse to acute;</text>
      <biological_entity constraint="upper" id="o15494" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s14" to="4.8" to_unit="mm" />
        <character is_modifier="false" modifier="conspicuously" name="architecture_or_shape" src="d0_s14" value="saccate" value_original="saccate" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="11(12)-veined" value_original="11(12)-veined" />
        <character constraint="in distal 1/3" constraintid="o15495" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="puberulent" value_original="puberulent" />
        <character char_type="range_value" from="obtuse" name="shape" notes="" src="d0_s14" to="acute" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15495" name="1/3" name_original="1/3" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>lower florets staminate;</text>
      <biological_entity constraint="lower" id="o15496" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower lemmas 2.8-4.9 mm, 5-7-veined, veins unequally spaced, lateral-veins near the margin, margins hyaline and usually overlapping the palea distally;</text>
      <biological_entity constraint="lower" id="o15497" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s16" to="4.9" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
      <biological_entity id="o15498" name="vein" name_original="veins" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="unequally" name="arrangement" src="d0_s16" value="spaced" value_original="spaced" />
      </biological_entity>
      <biological_entity id="o15499" name="lateral-vein" name_original="lateral-veins" src="d0_s16" type="structure" />
      <biological_entity id="o15500" name="margin" name_original="margin" src="d0_s16" type="structure" />
      <biological_entity id="o15501" name="margin" name_original="margins" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity id="o15502" name="paleum" name_original="palea" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="usually" name="arrangement" src="d0_s16" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <relation from="o15499" id="r2600" name="near" negation="false" src="d0_s16" to="o15500" />
    </statement>
    <statement id="d0_s17">
      <text>lower paleas 2-4 mm long, 0.8-1 mm wide, 3/4 to almost equaling the lower lemmas;</text>
      <biological_entity constraint="lower" id="o15503" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s17" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s17" to="1" to_unit="mm" />
        <character name="quantity" src="d0_s17" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity constraint="lower" id="o15504" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="true" modifier="almost" name="variability" src="d0_s17" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 3, 1.1-1.7 mm, yellow to yellow-red to purple;</text>
      <biological_entity id="o15505" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s18" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s18" to="yellow-red" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>upper lemmas 1.5-2 mm, subcoriaceous, shiny, white, glabrous, with 5 obscure veins, rounded to truncate, margins membranous, not inrolled over the paleas, scabridulous;</text>
      <biological_entity constraint="upper" id="o15506" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s19" to="2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s19" value="subcoriaceous" value_original="subcoriaceous" />
        <character is_modifier="false" name="reflectance" src="d0_s19" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="white" value_original="white" />
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="rounded" name="shape" notes="" src="d0_s19" to="truncate" />
      </biological_entity>
      <biological_entity id="o15507" name="vein" name_original="veins" src="d0_s19" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s19" value="5" value_original="5" />
        <character is_modifier="true" name="prominence" src="d0_s19" value="obscure" value_original="obscure" />
      </biological_entity>
      <biological_entity id="o15508" name="margin" name_original="margins" src="d0_s19" type="structure">
        <character is_modifier="false" name="texture" src="d0_s19" value="membranous" value_original="membranous" />
        <character constraint="over paleas" constraintid="o15509" is_modifier="false" modifier="not" name="shape_or_vernation" src="d0_s19" value="inrolled" value_original="inrolled" />
        <character is_modifier="false" name="relief" notes="" src="d0_s19" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o15509" name="palea" name_original="paleas" src="d0_s19" type="structure" />
      <relation from="o15506" id="r2601" name="with" negation="false" src="d0_s19" to="o15507" />
    </statement>
    <statement id="d0_s20">
      <text>upper paleas similar to the lemmas;</text>
      <biological_entity constraint="upper" id="o15510" name="palea" name_original="paleas" src="d0_s20" type="structure" />
      <biological_entity id="o15511" name="lemma" name_original="lemmas" src="d0_s20" type="structure" />
      <relation from="o15510" id="r2602" name="to" negation="false" src="d0_s20" to="o15511" />
    </statement>
    <statement id="d0_s21">
      <text>anthers 3, 0.7-1 mm, yellow;</text>
      <biological_entity id="o15512" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="3" value_original="3" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s21" to="1" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>styles purple.</text>
      <biological_entity id="o15513" name="style" name_original="styles" src="d0_s22" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s22" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Caryopses 1-1.3 mm long, 0.7-0.9 mm wide, glabrous.</text>
    </statement>
    <statement id="d0_s24">
      <text>2n = 36.</text>
      <biological_entity id="o15514" name="caryopsis" name_original="caryopses" src="d0_s23" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s23" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s23" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s23" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15515" name="chromosome" name_original="" src="d0_s24" type="structure">
        <character name="quantity" src="d0_s24" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sacciolepis striata is native to the southeastern United States and the West Indies, and from the Guianas to Venezuela and Amapa, Brazil. It grows along and in ponds, lakes, streams, and ditches, and flowers in late summer to fall.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Md.;N.J.;Del.;Mass.;Maine;Okla.;Miss.;Tex.;La.;Mo.;Ala.;Tenn.;N.C.;S.C.;Va.;Ark.;Ga.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>