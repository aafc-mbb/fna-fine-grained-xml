<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">113</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Willd." date="unknown" rank="genus">DACTYLOCTENIUM</taxon_name>
    <taxon_name authority="(L.) Willd." date="unknown" rank="species">aegyptium</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus dactyloctenium;species aegyptium</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Durban crowfoot</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants tufted annuals or short¬lived, shortly stoloniferous perennials.</text>
      <biological_entity id="o1259" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1260" name="whole-organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
        <character name="duration" value="annual" value_original="annual" />
        <character is_modifier="true" modifier="shortly" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="duration" value="perennial" value_original="perennial" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 10-35 (100) cm, usually geniculately ascending and rooting at the lower nodes.</text>
      <biological_entity id="o1262" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="100" value_original="100" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="35" to_unit="cm" />
        <character is_modifier="false" modifier="usually geniculately" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character constraint="at lower nodes" constraintid="o1263" is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o1263" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths keeled, with papillose-based hairs distally;</text>
      <biological_entity id="o1264" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o1265" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s2" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o1264" id="r216" name="with" negation="false" src="d0_s2" to="o1265" />
    </statement>
    <statement id="d0_s3">
      <text>ligules 0.5-1.5 mm, membranous, ciliate;</text>
      <biological_entity id="o1266" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 5-22 cm long, 2-8 (12) mm wide, with papillose-based hairs.</text>
      <biological_entity id="o1267" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s4" to="22" to_unit="cm" />
        <character name="width" src="d0_s4" unit="mm" value="12" value_original="12" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1268" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o1267" id="r217" name="with" negation="false" src="d0_s4" to="o1268" />
    </statement>
    <statement id="d0_s5">
      <text>Panicle branches (1) 2-6 (8), 1.5-6 cm, only the first few spikelets in contact with the spikelets of adjacent branches;</text>
      <biological_entity constraint="panicle" id="o1269" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character name="atypical_quantity" src="d0_s5" value="1" value_original="1" />
        <character name="atypical_quantity" src="d0_s5" value="8" value_original="8" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="6" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1270" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o1271" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <biological_entity id="o1272" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o1270" id="r218" modifier="only" name="in contact with" negation="false" src="d0_s5" to="o1271" />
      <relation from="o1271" id="r219" name="part_of" negation="false" src="d0_s5" to="o1272" />
    </statement>
    <statement id="d0_s6">
      <text>branch axes extending beyond the spikelets for 1-6 mm.</text>
      <biological_entity constraint="branch" id="o1273" name="axis" name_original="axes" src="d0_s6" type="structure" />
      <biological_entity id="o1274" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
      <relation from="o1273" id="r220" modifier="for 1-6 mm" name="extending beyond" negation="false" src="d0_s6" to="o1274" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 3-4.5 mm long, about 3 mm wide.</text>
      <biological_entity id="o1275" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s7" to="4.5" to_unit="mm" />
        <character name="width" src="d0_s7" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glumes 1.5-2 mm;</text>
      <biological_entity id="o1276" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower glumes ovate, acute;</text>
      <biological_entity constraint="lower" id="o1277" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes oblong-elliptic, obtuse, awned, awns 1-2.5 mm;</text>
      <biological_entity constraint="upper" id="o1278" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong-elliptic" value_original="oblong-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o1279" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 2.5-3.5 mm, ovate, midveins extended into curved, 0.5-1 mm awns;</text>
      <biological_entity id="o1280" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o1281" name="midvein" name_original="midveins" src="d0_s11" type="structure">
        <character constraint="into curved" is_modifier="false" name="size" src="d0_s11" value="extended" value_original="extended" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1282" name="awn" name_original="awns" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>paleas about as long as the lemmas;</text>
      <biological_entity id="o1283" name="palea" name_original="paleas" src="d0_s12" type="structure" />
      <biological_entity id="o1284" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers 0.5-0.8 mm, pale-yellow.</text>
      <biological_entity id="o1285" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pale-yellow" value_original="pale-yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds cuboid, about 1 mm long and wide, transversely rugose, light tan to reddish-brown.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 20, 36, 40, 45, 48.</text>
      <biological_entity id="o1286" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="cuboid" value_original="cuboid" />
        <character name="length" src="d0_s14" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="width" src="d0_s14" value="wide" value_original="wide" />
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s14" value="rugose" value_original="rugose" />
        <character char_type="range_value" from="light tan" name="coloration" src="d0_s14" to="reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1287" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="20" value_original="20" />
        <character name="quantity" src="d0_s15" value="36" value_original="36" />
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
        <character name="quantity" src="d0_s15" value="45" value_original="45" />
        <character name="quantity" src="d0_s15" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dactyloctenium aegyptium is a widely distributed weed of disturbed sites in the Flora region. It is also considered a weed in southern Africa, but the seeds have been used for food and drink in times of famine. In addition, bruised young seeds have been used as a fish poison, and extracts are reputed to help kidney ailments and coughing (Koekemoer 1991). In Australia, it is planted as a sand stabilizer along the coast (Jacobs and Hastings 1993).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pacific Islands (Hawaii);Fla.;N.J.;N.Mex.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;N.Y.;Va.;Colo.;Calif.;Puerto Rico;Virgin Islands;Ala.;Ark.;Ill.;Ga.;Ariz.;Maine;Md.;Mass.;Ohio;Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>