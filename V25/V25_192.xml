<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">121</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="genus">SPOROBOLUS</taxon_name>
    <taxon_name authority="Nash" date="unknown" rank="species">neglectus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus sporobolus;species neglectus</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Sporobolus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">vaginiflorus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">neglectus</taxon_name>
    <taxon_hierarchy>genus sporobolus;species vaginiflorus;variety neglectus</taxon_hierarchy>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Puffsheath dropseed</other_name>
  <other_name type="common_name">Sporobole neglige</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted, delicate, slender.</text>
      <biological_entity id="o29429" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="fragility" src="d0_s1" value="delicate" value_original="delicate" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-45 cm, wiry, erect to decumbent.</text>
      <biological_entity id="o29430" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="45" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="wiry" value_original="wiry" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths inflated, mostly glabrous but the apices with small tufts of hairs, hairs to 3 mm;</text>
      <biological_entity id="o29431" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o29432" name="apex" name_original="apices" src="d0_s3" type="structure" />
      <biological_entity id="o29433" name="tuft" name_original="tufts" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o29434" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <biological_entity id="o29435" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="3" to_unit="mm" />
      </biological_entity>
      <relation from="o29432" id="r5018" name="with" negation="false" src="d0_s3" to="o29433" />
      <relation from="o29433" id="r5019" name="part_of" negation="false" src="d0_s3" to="o29434" />
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.1-0.3 mm;</text>
      <biological_entity id="o29436" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s4" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-12 cm long, 0.6-2 mm wide, flat to loosely involute, abaxial surface glabrous, adaxial surface scabridulous, bases of both surfaces sometimes with papillose-based hairs, margins smooth or scabridulous.</text>
      <biological_entity id="o29437" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="12" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s5" to="loosely involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o29438" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o29439" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o29440" name="base" name_original="bases" src="d0_s5" type="structure" />
      <biological_entity id="o29441" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o29442" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o29443" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s5" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <relation from="o29440" id="r5020" name="part_of" negation="false" src="d0_s5" to="o29441" />
      <relation from="o29440" id="r5021" name="with" negation="false" src="d0_s5" to="o29442" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles terminal and axillary, 2-5 cm long, 0.2-0.5 cm wide, contracted, cylindrical, included in the uppermost sheath;</text>
      <biological_entity id="o29444" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s6" to="0.5" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
      <biological_entity constraint="uppermost" id="o29445" name="sheath" name_original="sheath" src="d0_s6" type="structure" />
      <relation from="o29444" id="r5022" name="included in the" negation="false" src="d0_s6" to="o29445" />
    </statement>
    <statement id="d0_s7">
      <text>lower nodes with 1-2 (3) branches;</text>
      <biological_entity constraint="lower" id="o29446" name="node" name_original="nodes" src="d0_s7" type="structure" />
      <biological_entity id="o29447" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="3" value_original="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="2" />
      </biological_entity>
      <relation from="o29446" id="r5023" name="with" negation="false" src="d0_s7" to="o29447" />
    </statement>
    <statement id="d0_s8">
      <text>primary branches 0.4-1.8 cm, appressed, spikelet-bearing to the base;</text>
      <biological_entity constraint="primary" id="o29448" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s8" to="1.8" to_unit="cm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o29449" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o29448" id="r5024" name="to" negation="false" src="d0_s8" to="o29449" />
    </statement>
    <statement id="d0_s9">
      <text>secondary branches appressed;</text>
      <biological_entity constraint="secondary" id="o29450" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels 0.1-2.5 mm, appressed, scabridulous.</text>
      <biological_entity id="o29451" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="relief" src="d0_s10" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 1.6-3 mm, yellowish to cream-colored, sometimes purple-tinged.</text>
      <biological_entity id="o29452" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character char_type="range_value" from="yellowish" name="coloration" src="d0_s11" to="cream-colored" />
        <character is_modifier="false" modifier="sometimes" name="coloration" src="d0_s11" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes subequal, shorter than the florets, lanceolate to ovate, membra¬nous to chartaceous, glabrous;</text>
      <biological_entity id="o29453" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character constraint="than the florets" constraintid="o29454" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s12" to="ovate" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s12" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o29454" name="floret" name_original="florets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 1.5-2.4 mm, midveins often greenish;</text>
      <biological_entity constraint="lower" id="o29455" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29456" name="midvein" name_original="midveins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 1.7-2.7 mm;</text>
      <biological_entity constraint="upper" id="o29457" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s14" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 1.6-2.9 mm, ovate, chartaceous, glabrous, acute;</text>
      <biological_entity id="o29458" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s15" to="2.9" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s15" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 1.6-3 mm, ovate, chartaceous, glabrous;</text>
      <biological_entity id="o29459" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s16" value="chartaceous" value_original="chartaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, 1.1-1.6 mm, purplish.</text>
      <biological_entity id="o29460" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s17" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Fruits 1.2-1.8 mm, obovoid, laterally flattened, light brownish or orangish-brown, translucent, finely striate.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 36.</text>
      <biological_entity id="o29461" name="fruit" name_original="fruits" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s18" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s18" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="light brownish" value_original="light brownish" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="orangish-brown" value_original="orangish-brown" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s18" value="translucent" value_original="translucent" />
        <character is_modifier="false" modifier="finely" name="coloration_or_pubescence_or_relief" src="d0_s18" value="striate" value_original="striate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29462" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sporobolus neglectus is native to the Flora region, and grows at 0-1300 m in sandy soils, on river shores, and in dry, open areas within many plant communities, often in disturbed sites. It appears to have been extirpated from Maine and Maryland and is considered endangered or of special concern in Connecticut, Massachusetts, New Hampshire, and New Jersey.</discussion>
  <discussion>Sporobolus vaginiflorus is very similar to S. neglectus, but it differs in having strigose lemmas, sheaths that are sparsely hairy towards the base and, usually, longer spikelets.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Alta.;Man.;N.B.;Ont.;Que.;Sask.;D.C.;Wis.;W.Va.;Wyo.;N.H.;N.Mex.;Tex.;La.;Tenn.;Pa.;Va.;Colo.;Calif.;Ala.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Ark.;Vt.;Ill.;Ind.;Iowa;Ariz.;Maine;Md.;Mass.;Ohio;Mo.;Minn.;Mich.;Mont.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>