<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">113</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Willd." date="unknown" rank="genus">DACTYLOCTENIUM</taxon_name>
    <taxon_name authority="Hack." date="unknown" rank="species">geminatum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus dactyloctenium;species geminatum</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Double combgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>stoloniferous, mat-forming.</text>
      <biological_entity id="o1795" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 35-112 cm, ascending.</text>
      <biological_entity id="o1796" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s2" to="112" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Blades 4-25 cm long, 3-6 mm wide, flat, more or less glabrous.</text>
      <biological_entity id="o1797" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s3" to="25" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="6" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicle branches (1) 2 (3), 2.5-7 cm, often slightly falcate, only the first few spikelets in contact with the spikelets of adjacent branches.</text>
      <biological_entity constraint="panicle" id="o1798" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character name="atypical_quantity" src="d0_s4" value="1" value_original="1" />
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character name="atypical_quantity" src="d0_s4" value="3" value_original="3" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="often slightly" name="shape" src="d0_s4" value="falcate" value_original="falcate" />
      </biological_entity>
      <biological_entity id="o1799" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="few" value_original="few" />
      </biological_entity>
      <biological_entity id="o1800" name="spikelet" name_original="spikelets" src="d0_s4" type="structure" />
      <biological_entity id="o1801" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o1799" id="r295" modifier="only" name="in contact with" negation="false" src="d0_s4" to="o1800" />
      <relation from="o1800" id="r296" name="part_of" negation="false" src="d0_s4" to="o1801" />
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 3-5.3 mm, with 3-6 florets.</text>
      <biological_entity id="o1802" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="5.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1803" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <relation from="o1802" id="r297" name="with" negation="false" src="d0_s5" to="o1803" />
    </statement>
    <statement id="d0_s6">
      <text>Glumes subequal, 1.3-1.8 mm, widely elliptic to ovate or obovate in profile, awned, awns 4.5-10 mm;</text>
      <biological_entity id="o1804" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character is_modifier="false" name="size" src="d0_s6" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s6" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="widely elliptic" modifier="in profile" name="shape" src="d0_s6" to="ovate or obovate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o1805" name="awn" name_original="awns" src="d0_s6" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>lemmas 3-3.8 mm, lanceolate, keels smooth or finely scabridulous towards the acute or mucronate apices;</text>
      <biological_entity id="o1806" name="lemma" name_original="lemmas" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s7" to="3.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o1807" name="keel" name_original="keels" src="d0_s7" type="structure">
        <character is_modifier="false" name="relief" src="d0_s7" value="smooth" value_original="smooth" />
        <character constraint="towards apices" constraintid="o1808" is_modifier="false" modifier="finely" name="relief" src="d0_s7" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o1808" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="acute" value_original="acute" />
        <character is_modifier="true" name="shape" src="d0_s7" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>palea keels not winged;</text>
      <biological_entity constraint="palea" id="o1809" name="keel" name_original="keels" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 1.1-1.7 mm.</text>
      <biological_entity id="o1810" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seeds about 1 mm long, transversely rugose.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = unknown.</text>
      <biological_entity id="o1811" name="seed" name_original="seeds" src="d0_s10" type="structure">
        <character name="length" src="d0_s10" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s10" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1812" name="chromosome" name_original="" src="d0_s11" type="structure" />
    </statement>
  </description>
  <discussion>Dactyloctenium geminatum is native to tropical eastern Africa. It was found at one time on ballast dumps in Maryland, but has not survived in North America.</discussion>
  
</bio:treatment>