<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">614</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">SACCHARUM</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">officinarum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus saccharum;species officinarum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>8</number>
  <other_name type="common_name">Sugarcane</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants with short rhizomes.</text>
      <biological_entity id="o11968" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11969" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s0" value="short" value_original="short" />
      </biological_entity>
      <relation from="o11968" id="r1965" name="with" negation="false" src="d0_s0" to="o11969" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 3-6 m tall, 2-5 cm thick, clumped, glabrous throughout or nearly so, lower internodes swollen.</text>
      <biological_entity id="o11970" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="3" from_unit="m" name="height" src="d0_s1" to="6" to_unit="m" />
        <character char_type="range_value" from="2" from_unit="cm" name="thickness" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="clumped" value_original="clumped" />
        <character is_modifier="false" modifier="throughout; throughout; nearly" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o11971" name="internode" name_original="internodes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths sometimes ciliate at the collar margins;</text>
      <biological_entity id="o11972" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character constraint="at collar margins" constraintid="o11973" is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="collar" id="o11973" name="margin" name_original="margins" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>auricles present;</text>
      <biological_entity id="o11974" name="auricle" name_original="auricles" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 2-3 mm;</text>
      <biological_entity id="o11975" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 70-150 cm long, 20-60 mm wide, usually glabrous, occasionally with hairs on the adaxial surfaces.</text>
      <biological_entity id="o11976" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="70" from_unit="cm" name="length" src="d0_s5" to="150" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="mm" name="width" src="d0_s5" to="60" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11977" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o11978" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <relation from="o11976" id="r1966" modifier="occasionally" name="with" negation="false" src="d0_s5" to="o11977" />
      <relation from="o11977" id="r1967" name="on" negation="false" src="d0_s5" to="o11978" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles 20-80 cm, glabrous;</text>
      <biological_entity id="o11979" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s6" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>panicles 50-100 cm long, to 20 cm wide, lanceolate;</text>
      <biological_entity id="o11980" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="length" src="d0_s7" to="100" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s7" to="20" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>rachises 30-80 cm, glabrous;</text>
      <biological_entity id="o11981" name="rachis" name_original="rachises" src="d0_s8" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s8" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>primary branches 10-25 cm, appressed to spreading;</text>
    </statement>
    <statement id="d0_s10">
      <text>rame internodes 3-6 mm, glabrous.</text>
      <biological_entity constraint="primary" id="o11982" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s9" to="25" to_unit="cm" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s9" to="spreading" />
      </biological_entity>
      <biological_entity id="o11983" name="internode" name_original="internodes" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sessile spikelets 3-5 mm long, 0.8-0.9 mm wide, white to gray.</text>
      <biological_entity id="o11984" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s11" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s11" to="0.9" to_unit="mm" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s11" to="gray" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Callus hairs 6-10 mm, exceeding the spikelets, white;</text>
      <biological_entity constraint="callus" id="o11985" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o11986" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
      <relation from="o11985" id="r1968" name="exceeding the" negation="false" src="d0_s12" to="o11986" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes glabrous, 2-4-veined;</text>
      <biological_entity constraint="lower" id="o11987" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="2-4-veined" value_original="2-4-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 3-veined;</text>
      <biological_entity constraint="upper" id="o11988" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower lemmas 3-4.5 mm, 2-3-veined;</text>
      <biological_entity constraint="lower" id="o11989" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="2-3-veined" value_original="2-3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper lemmas without veins, entire;</text>
      <biological_entity constraint="upper" id="o11990" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture_or_shape" notes="" src="d0_s16" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o11991" name="vein" name_original="veins" src="d0_s16" type="structure" />
      <relation from="o11990" id="r1969" name="without" negation="false" src="d0_s16" to="o11991" />
    </statement>
    <statement id="d0_s17">
      <text>awns absent;</text>
      <biological_entity id="o11992" name="awn" name_original="awns" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>lodicule veins not extending into hairlike projections;</text>
      <biological_entity constraint="lodicule" id="o11993" name="vein" name_original="veins" src="d0_s18" type="structure" />
      <biological_entity id="o11994" name="projection" name_original="projections" src="d0_s18" type="structure">
        <character is_modifier="true" name="shape" src="d0_s18" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <relation from="o11993" id="r1970" name="extending into" negation="false" src="d0_s18" to="o11994" />
    </statement>
    <statement id="d0_s19">
      <text>anthers 3.</text>
      <biological_entity id="o11995" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Pedicels 2-5 mm, glabrous.</text>
      <biological_entity id="o11996" name="pedicel" name_original="pedicels" src="d0_s20" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s20" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Pedicellate spikelets similar to the sessile spikelets.</text>
      <biological_entity id="o11998" name="spikelet" name_original="spikelets" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o11997" id="r1971" name="to" negation="false" src="d0_s21" to="o11998" />
    </statement>
    <statement id="d0_s22">
      <text>2n = 80.</text>
      <biological_entity id="o11997" name="spikelet" name_original="spikelets" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11999" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Saccharum officinarum is native to tropical Asia and the Pacific islands. It is cultivated for sugar production in various parts of the world, including Texas, Louisiana, and Florida. It is also becoming popular as an ornamental plant for gardens in warmer parts of the contiguous United States, and appears to be established in some parts of the southeastern United States. A number of different, clonally propagated color forms are available. It hybridizes with S. spontaneum (see discussion above).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Tex.;La.;Virgin Islands;Ala.;Miss.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>