<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">657</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ANDROPOGON</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="section">Leptopogon</taxon_name>
    <taxon_name authority="Ashe" date="unknown" rank="species">gyrans</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus andropogon;section leptopogon;species gyrans</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Andropogon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">subtenuis</taxon_name>
    <taxon_hierarchy>genus andropogon;species subtenuis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Andropogon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">elliottii</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">projectus</taxon_name>
    <taxon_hierarchy>genus andropogon;species elliottii;variety projectus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Andropogon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">elliottii</taxon_name>
    <taxon_hierarchy>genus andropogon;species elliottii</taxon_hierarchy>
  </taxon_identification>
  <number>9</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, cylindrical to ovate above.</text>
      <biological_entity id="o9525" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="cylindrical" name="shape" src="d0_s0" to="ovate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 30-100 (140) cm;</text>
      <biological_entity id="o9526" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="140" value_original="140" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes usually glaucous;</text>
      <biological_entity id="o9527" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches mostly erect, straight.</text>
      <biological_entity id="o9528" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths smooth;</text>
      <biological_entity id="o9529" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.3-1.5 mm, sometimes ciliate, cilia to 0.7 mm;</text>
      <biological_entity id="o9530" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o9531" name="cilium" name_original="cilia" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 6-48 cm long, 0.8-5 mm wide, glabrous or densely pubescent with spreading hairs.</text>
      <biological_entity id="o9532" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s6" to="48" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character constraint="with hairs" constraintid="o9533" is_modifier="false" modifier="densely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o9533" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescence units 2-31 per culm;</text>
      <biological_entity id="o9535" name="culm" name_original="culm" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>subtending sheaths (2.6) 4.1-4.5 (13.5) cm long, (1.5) 2.7-4.7 (8) mm wide;</text>
      <biological_entity constraint="inflorescence" id="o9534" name="unit" name_original="units" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per culm" constraintid="o9535" from="2" name="quantity" src="d0_s7" to="31" />
        <character name="length" src="d0_s8" unit="cm" value="2.6" value_original="2.6" />
        <character char_type="range_value" from="4.1" from_unit="cm" name="length" src="d0_s8" to="4.5" to_unit="cm" />
        <character name="width" src="d0_s8" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2.7" from_unit="mm" name="width" src="d0_s8" to="4.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9536" name="sheath" name_original="sheaths" src="d0_s8" type="structure" />
      <relation from="o9534" id="r1568" name="subtending" negation="false" src="d0_s8" to="o9536" />
    </statement>
    <statement id="d0_s9">
      <text>peduncles (1) 5-31 (195) mm, with 2-5 rames;</text>
      <biological_entity id="o9538" name="rame" name_original="rames" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="5" />
      </biological_entity>
      <relation from="o9537" id="r1569" name="with" negation="false" src="d0_s9" to="o9538" />
    </statement>
    <statement id="d0_s10">
      <text>rames (1.5) 2.8-4.2 (6) cm, exserted or not at maturity, pubescence increasing in density distally within each internode.</text>
      <biological_entity id="o9537" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s9" to="31" to_unit="mm" />
        <character name="atypical_some_measurement" src="d0_s10" unit="cm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2.8" from_unit="cm" name="some_measurement" src="d0_s10" to="4.2" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
        <character name="position" src="d0_s10" value="not" value_original="not" />
        <character is_modifier="false" modifier="at maturity" name="character" src="d0_s10" value="pubescence" value_original="pubescence" />
      </biological_entity>
      <biological_entity id="o9539" name="density" name_original="density" src="d0_s10" type="structure" />
      <biological_entity id="o9540" name="internode" name_original="internode" src="d0_s10" type="structure" />
      <relation from="o9539" id="r1570" name="within" negation="false" src="d0_s10" to="o9540" />
    </statement>
    <statement id="d0_s11">
      <text>Sessile spikelets (3) 3.9-4.7 (5.7) mm;</text>
      <biological_entity id="o9541" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="3.9" from_unit="mm" name="some_measurement" src="d0_s11" to="4.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>callus hairs 1-5 mm;</text>
      <biological_entity constraint="callus" id="o9542" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keels of lower glumes scabrous only beyond midlength;</text>
      <biological_entity id="o9543" name="keel" name_original="keels" src="d0_s13" type="structure" constraint="glume" constraint_original="glume; glume">
        <character constraint="beyond midlength" constraintid="o9545" is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o9544" name="glume" name_original="glumes" src="d0_s13" type="structure" />
      <biological_entity id="o9545" name="midlength" name_original="midlength" src="d0_s13" type="structure" />
      <relation from="o9543" id="r1571" name="part_of" negation="false" src="d0_s13" to="o9544" />
    </statement>
    <statement id="d0_s14">
      <text>awns 8-24 mm;</text>
      <biological_entity id="o9546" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s14" to="24" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1, 0.6-1.4 (1.7) mm, yellow or purple.</text>
      <biological_entity id="o9547" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="1.7" value_original="1.7" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s15" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pedicellate spikelets vestigial or absent.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 20.</text>
      <biological_entity id="o9548" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9549" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Andropogon gyrans extends from the southeastern United States to the Caribbean and Central America.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Del.;D.C.;W.Va.;Fla.;N.J.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;Va.;Ala.;Ark.;Ill.;Ga.;Ind.;Md.;Okla.;Ohio;Mo.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Ligules 0.3-1.1 mm long; rames usually hidden within the more or less overlapping and inflated upper sheaths at maturity; plants usually of well-drained soils</description>
      <determination>Andropogon gyrans var. gyrans</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Ligules 0.8-1.5 mm long; rames usually exposed at maturity; plants of wet habitats</description>
      <determination>Andropogon gyrans var. stenophyllus</determination>
    </key_statement>
  </key>
</bio:treatment>