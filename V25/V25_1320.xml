<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">497</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">UROCHLOA</taxon_name>
    <taxon_name authority="(Hack.) Dandy" date="unknown" rank="species">mosambicensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus urochloa;species mosambicensis</taxon_hierarchy>
  </taxon_identification>
  <number>8</number>
  <other_name type="common_name">Sabi grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, with or without stolons.</text>
      <biological_entity id="o1020" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1021" name="stolon" name_original="stolons" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-150 cm;</text>
      <biological_entity id="o1022" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes pubescent;</text>
      <biological_entity id="o1023" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes with papillose-based hairs.</text>
      <biological_entity id="o1024" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity id="o1025" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o1024" id="r176" name="with" negation="false" src="d0_s4" to="o1025" />
    </statement>
    <statement id="d0_s5">
      <text>Sheaths pubescent, lower sheaths pilose, upper sheaths with papillose-based hairs;</text>
      <biological_entity id="o1026" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o1027" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="upper" id="o1028" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity id="o1029" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o1028" id="r177" name="with" negation="false" src="d0_s5" to="o1029" />
    </statement>
    <statement id="d0_s6">
      <text>ligules 1-2 mm;</text>
      <biological_entity id="o1030" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 3-30 cm long, (1.5) 3-20 mm wide, with scattered papillose-based hairs, margins scabrous.</text>
      <biological_entity id="o1031" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="30" to_unit="cm" />
        <character name="width" src="d0_s7" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1032" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s7" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o1033" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <relation from="o1031" id="r178" name="with" negation="false" src="d0_s7" to="o1032" />
    </statement>
    <statement id="d0_s8">
      <text>Panicles 3-12.5 cm, with 2-6 (15) spikelike branches in 2 ranks;</text>
      <biological_entity id="o1034" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s8" to="12.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1035" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="15" value_original="15" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="6" />
        <character is_modifier="true" name="shape" src="d0_s8" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o1036" name="rank" name_original="ranks" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <relation from="o1034" id="r179" name="with" negation="false" src="d0_s8" to="o1035" />
      <relation from="o1035" id="r180" name="in" negation="false" src="d0_s8" to="o1036" />
    </statement>
    <statement id="d0_s9">
      <text>primary branches 2-10 cm, appressed to ascending, axes 0.8-1.4 mm wide, flat, winged, hispid, hairs not papillose-based;</text>
      <biological_entity constraint="primary" id="o1037" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="10" to_unit="cm" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s9" to="ascending" />
      </biological_entity>
      <biological_entity id="o1038" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s9" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o1039" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s9" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>secondary branches present;</text>
      <biological_entity constraint="secondary" id="o1040" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicels shorter than the spikelets, scabrous, with 1-3 conspicuous hairs.</text>
      <biological_entity id="o1041" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character constraint="than the spikelets" constraintid="o1042" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o1042" name="spikelet" name_original="spikelets" src="d0_s11" type="structure" />
      <biological_entity id="o1043" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="3" />
        <character is_modifier="true" name="prominence" src="d0_s11" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <relation from="o1041" id="r181" name="with" negation="false" src="d0_s11" to="o1043" />
    </statement>
    <statement id="d0_s12">
      <text>Spikelets (3) 4-5 mm long, 1.5-2 mm wide, solitary (rarely paired), appressed to the branch axes, bases glabrous or with a tuft of hairs.</text>
      <biological_entity id="o1044" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character name="length" src="d0_s12" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s12" to="5" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s12" value="solitary" value_original="solitary" />
        <character constraint="to branch axes" constraintid="o1045" is_modifier="false" name="fixation_or_orientation" src="d0_s12" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity constraint="branch" id="o1045" name="axis" name_original="axes" src="d0_s12" type="structure" />
      <biological_entity id="o1046" name="base" name_original="bases" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="with a tuft" value_original="with a tuft" />
      </biological_entity>
      <biological_entity id="o1047" name="tuft" name_original="tuft" src="d0_s12" type="structure" />
      <biological_entity id="o1048" name="hair" name_original="hairs" src="d0_s12" type="structure" />
      <relation from="o1046" id="r182" name="with" negation="false" src="d0_s12" to="o1047" />
      <relation from="o1047" id="r183" name="part_of" negation="false" src="d0_s12" to="o1048" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes scarcely separated, rachilla internode between the glumes not pronounced;</text>
      <biological_entity id="o1049" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="scarcely" name="arrangement" src="d0_s13" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity constraint="between rachilla" constraintid="o1051" id="o1050" name="internode" name_original="internode" src="d0_s13" type="structure" constraint_original="between  rachilla, ">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s13" value="pronounced" value_original="pronounced" />
      </biological_entity>
      <biological_entity id="o1051" name="glume" name_original="glumes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 2.7-3.3 mm, (1/2) 2/3 - 3/4 as long as the spikelets, 3-veined, mostly glabrous but often with 1-3 conspicuous, stiff hairs emanating from the midvein at approximately midlength;</text>
      <biological_entity constraint="lower" id="o1052" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="some_measurement" src="d0_s14" to="3.3" to_unit="mm" />
        <character name="quantity" src="d0_s14" value="[1/2]" value_original="[1/2]" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o1053" from="2/3" name="quantity" src="d0_s14" to="3/4" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s14" value="3-veined" value_original="3-veined" />
        <character constraint="often with hairs" constraintid="o1054" is_modifier="false" modifier="mostly" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1053" name="spikelet" name_original="spikelets" src="d0_s14" type="structure" />
      <biological_entity id="o1054" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s14" to="3" />
        <character is_modifier="true" name="prominence" src="d0_s14" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="fragility" src="d0_s14" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o1055" name="midvein" name_original="midvein" src="d0_s14" type="structure" />
      <biological_entity id="o1056" name="midlength" name_original="midlength" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="approximately midlength" name="position" src="d0_s14" value="midlength" value_original="midlength" />
      </biological_entity>
      <relation from="o1054" id="r184" name="emanating from" negation="false" src="d0_s14" to="o1055" />
      <relation from="o1055" id="r185" name="at" negation="false" src="d0_s14" to="o1056" />
    </statement>
    <statement id="d0_s15">
      <text>upper glumes (3) 4-5 mm, glabrous or pubescent, 5-veined;</text>
      <biological_entity constraint="upper" id="o1057" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower florets staminate;</text>
      <biological_entity constraint="lower" id="o1058" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>lower lemmas (3) 4-5 mm, glabrous or pubescent, 5-veined, with or without a setose fringe along the margins;</text>
      <biological_entity constraint="lower" id="o1059" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character name="atypical_some_measurement" src="d0_s17" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s17" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o1060" name="fringe" name_original="fringe" src="d0_s17" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s17" value="setose" value_original="setose" />
      </biological_entity>
      <biological_entity id="o1061" name="margin" name_original="margins" src="d0_s17" type="structure" />
      <relation from="o1059" id="r186" name="with or without" negation="false" src="d0_s17" to="o1060" />
      <relation from="o1060" id="r187" name="along" negation="false" src="d0_s17" to="o1061" />
    </statement>
    <statement id="d0_s18">
      <text>lower paleas present;</text>
      <biological_entity constraint="lower" id="o1062" name="palea" name_original="paleas" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>upper lemmas 2.2-2.6 mm, apices rounded, shortly awned, awns 0.5-1.2 mm;</text>
      <biological_entity constraint="upper" id="o1063" name="lemma" name_original="lemmas" src="d0_s19" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s19" to="2.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1064" name="apex" name_original="apices" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="shortly" name="architecture_or_shape" src="d0_s19" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o1065" name="awn" name_original="awns" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s19" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>anthers 1.2-1.5 mm. 2n = 30, 42.</text>
      <biological_entity id="o1066" name="anther" name_original="anthers" src="d0_s20" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s20" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1067" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="30" value_original="30" />
        <character name="quantity" src="d0_s20" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Urochloa mosambicensis, native to Africa, has been found in southern Texas (Wipff et al. 1993); it is expected to spread. It is grown for forage and hay in Africa.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>