<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">216</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Sw." date="unknown" rank="genus">CHLORIS</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="species">submutica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus chloris;species submutica</taxon_hierarchy>
  </taxon_identification>
  <number>17</number>
  <other_name type="common_name">Mexican windmill-grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually cespitose, occasionally shortly stoloniferous.</text>
      <biological_entity id="o22251" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="occasionally shortly" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-75 cm, erect.</text>
      <biological_entity id="o22252" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="75" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o22253" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules about 0.5 mm, shortly ciliate;</text>
      <biological_entity id="o22254" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="shortly" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades to 20 cm long, to 5 mm wide, sometimes with long basal hairs, otherwise scabrous.</text>
      <biological_entity id="o22255" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s5" to="20" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="otherwise" name="pubescence_or_relief" notes="" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o22256" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
      </biological_entity>
      <relation from="o22255" id="r3790" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o22256" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 5-17, evidently distinct branches in 1-3 closely-spaced whorls;</text>
      <biological_entity id="o22257" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o22258" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="17" />
        <character is_modifier="true" modifier="evidently" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o22259" name="whorl" name_original="whorls" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="closely-spaced" value_original="closely-spaced" />
      </biological_entity>
      <relation from="o22257" id="r3791" name="with" negation="false" src="d0_s6" to="o22258" />
      <relation from="o22258" id="r3792" name="in" negation="false" src="d0_s6" to="o22259" />
    </statement>
    <statement id="d0_s7">
      <text>branches to 7 cm, usually erect when young, spreading to reflexed at maturity, averaging 12 spikelets per cm.</text>
      <biological_entity id="o22260" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s7" to="7" to_unit="cm" />
        <character is_modifier="false" modifier="when young" name="orientation" src="d0_s7" value="erect" value_original="erect" />
        <character char_type="range_value" constraint="at maturity" from="spreading" name="orientation" src="d0_s7" to="reflexed" />
      </biological_entity>
      <biological_entity id="o22261" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="12" value_original="12" />
      </biological_entity>
      <biological_entity id="o22262" name="cm" name_original="cm" src="d0_s7" type="structure" />
      <relation from="o22260" id="r3793" name="averaging" negation="false" src="d0_s7" to="o22261" />
      <relation from="o22260" id="r3794" name="per" negation="false" src="d0_s7" to="o22262" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets with 1 bisexual and 1 staminate floret.</text>
      <biological_entity id="o22263" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o22264" name="floret" name_original="floret" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <relation from="o22263" id="r3795" name="with" negation="false" src="d0_s8" to="o22264" />
    </statement>
    <statement id="d0_s9">
      <text>Lower glumes 1.5-3.2 mm;</text>
      <biological_entity constraint="lower" id="o22265" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 2.5-3.4 mm;</text>
      <biological_entity constraint="upper" id="o22266" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lowest lemmas 2.8-3.7 mm long, 0.6-1.1 mm wide, broadly linear to elliptic, mostly glabrous but the margins appressed-pubescent, apices obtuse, not conspicuously bilobed, sometimes shortly mucronate;</text>
      <biological_entity constraint="lowest" id="o22267" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="length" src="d0_s11" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s11" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="broadly linear" name="arrangement" src="d0_s11" to="elliptic" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o22268" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>second florets 1.4-2.2 mm long, 0.3-0.9 mm wide, usually at least twice as long as wide, not lobed, unawned, occasionally mucronate.</text>
      <biological_entity id="o22269" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="not conspicuously" name="architecture_or_shape" src="d0_s11" value="bilobed" value_original="bilobed" />
        <character is_modifier="false" modifier="sometimes shortly" name="shape" src="d0_s11" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity id="o22270" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s12" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s12" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s12" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s12" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Caryopses 1.7-2.3 mm long, 0.5-0.6 mm wide, ellipsoid.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = ca. 65, 80.</text>
      <biological_entity id="o22271" name="caryopsis" name_original="caryopses" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s13" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s13" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22272" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="65" value_original="65" />
        <character name="quantity" src="d0_s14" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chloris submutica grows from the southwestern United States through Mexico, Guatemala, and Colombia to Venezuela. In Mexico, it is generally found between 1000-2100 m.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>