<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Christopher S. Campbell;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">649</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ANDROPOGON</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus andropogon</taxon_hierarchy>
  </taxon_identification>
  <number>26.15</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually cespitose, sometimes rhizomatous.</text>
      <biological_entity id="o2091" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 20-310 cm, erect, much-branched distally.</text>
      <biological_entity id="o2092" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="310" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="much-branched" value_original="much-branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves not aromatic;</text>
      <biological_entity id="o2093" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s3" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, sometimes ciliate;</text>
      <biological_entity id="o2094" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades linear, flat, folded, or convolute.</text>
      <biological_entity id="o2095" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s5" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convolute" value_original="convolute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convolute" value_original="convolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal and axillary or a false panicle;</text>
      <biological_entity id="o2096" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity constraint="false" id="o2097" name="panicle" name_original="panicle" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>inflorescence units 1-600+ per culm;</text>
      <biological_entity constraint="inflorescence" id="o2098" name="unit" name_original="units" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per culm" constraintid="o2099" from="1" name="quantity" src="d0_s7" to="600" upper_restricted="false" />
      </biological_entity>
      <biological_entity id="o2099" name="culm" name_original="culm" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>peduncles initially concealed by the subtending leaf-sheaths, sometimes exserted beyond the sheaths at maturity, with (1) 2-5 (13) rames;</text>
      <biological_entity constraint="subtending" id="o2101" name="sheath" name_original="leaf-sheaths" src="d0_s8" type="structure" />
      <biological_entity id="o2102" name="sheath" name_original="sheaths" src="d0_s8" type="structure" />
      <biological_entity id="o2103" name="rame" name_original="rames" src="d0_s8" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s8" value="13" value_original="13" />
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <relation from="o2100" id="r343" modifier="sometimes" name="exserted beyond the" negation="false" src="d0_s8" to="o2102" />
      <relation from="o2100" id="r344" name="with" negation="false" src="d0_s8" to="o2103" />
    </statement>
    <statement id="d0_s9">
      <text>rames not reflexed at maturity, axes slender, terete to flattened, not longitudinally grooved, usually conspicuously pubescent, with spikelets in heterogamous sessile-pedicellate pairs (the terminal spikelets sometimes in triplets of 1 sessile and 2 pedicellate spikelets), apices of the internodes neither cupulate nor fimbriate;</text>
      <biological_entity id="o2100" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character constraint="by subtending leaf-sheaths" constraintid="o2101" is_modifier="false" modifier="initially" name="prominence" src="d0_s8" value="concealed" value_original="concealed" />
        <character constraint="at maturity" is_modifier="false" modifier="not" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o2104" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="slender" value_original="slender" />
        <character char_type="range_value" from="terete" name="shape" src="d0_s9" to="flattened" />
        <character is_modifier="false" modifier="not longitudinally" name="architecture" src="d0_s9" value="grooved" value_original="grooved" />
        <character is_modifier="false" modifier="usually conspicuously" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o2105" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o2106" name="pair" name_original="pairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="heterogamous" value_original="heterogamous" />
        <character is_modifier="true" name="architecture" src="d0_s9" value="sessile-pedicellate" value_original="sessile-pedicellate" />
      </biological_entity>
      <biological_entity id="o2108" name="internode" name_original="internodes" src="d0_s9" type="structure" />
      <relation from="o2104" id="r345" name="with" negation="false" src="d0_s9" to="o2105" />
      <relation from="o2105" id="r346" name="in" negation="false" src="d0_s9" to="o2106" />
      <relation from="o2107" id="r347" name="part_of" negation="false" src="d0_s9" to="o2108" />
    </statement>
    <statement id="d0_s10">
      <text>disarticulation in the rames, below the sessile spikelets.</text>
      <biological_entity id="o2107" name="apex" name_original="apices" src="d0_s9" type="structure" constraint="internode" constraint_original="internode; internode">
        <character is_modifier="false" name="shape" src="d0_s9" value="cupulate" value_original="cupulate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="fimbriate" value_original="fimbriate" />
      </biological_entity>
      <biological_entity id="o2109" name="rame" name_original="rames" src="d0_s10" type="structure" />
      <biological_entity id="o2110" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <relation from="o2107" id="r348" name="in" negation="false" src="d0_s10" to="o2109" />
    </statement>
    <statement id="d0_s11">
      <text>Sessile spikelets bisexual, awned, with short, blunt calluses;</text>
      <biological_entity id="o2111" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o2112" name="callus" name_original="calluses" src="d0_s11" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
        <character is_modifier="true" name="shape" src="d0_s11" value="blunt" value_original="blunt" />
      </biological_entity>
      <relation from="o2111" id="r349" name="with" negation="false" src="d0_s11" to="o2112" />
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 2-keeled, flat or concave, usually not veined between the keels, sometimes 2-9-veined;</text>
      <biological_entity constraint="lower" id="o2113" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="2-keeled" value_original="2-keeled" />
        <character is_modifier="false" name="shape" src="d0_s12" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s12" value="concave" value_original="concave" />
        <character constraint="between keels" constraintid="o2114" is_modifier="false" modifier="usually not" name="architecture" src="d0_s12" value="veined" value_original="veined" />
        <character is_modifier="false" modifier="sometimes" name="architecture" notes="" src="d0_s12" value="2-9-veined" value_original="2-9-veined" />
      </biological_entity>
      <biological_entity id="o2114" name="keel" name_original="keels" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers 1, 3 (2).</text>
      <biological_entity id="o2115" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="1" value_original="1" />
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character name="atypical_quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pedicels usually longer than 3 mm, similar to the rame internodes in shape, length, and pubescence color, not fused to the rame axes.</text>
      <biological_entity id="o2116" name="pedicel" name_original="pedicels" src="d0_s14" type="structure">
        <character modifier="usually longer than" name="some_measurement" src="d0_s14" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s14" value="pubescence" value_original="pubescence" />
        <character constraint="to axes" constraintid="o2118" is_modifier="false" modifier="not" name="fusion" src="d0_s14" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o2117" name="internode" name_original="internodes" src="d0_s14" type="structure" />
      <biological_entity id="o2118" name="axis" name_original="axes" src="d0_s14" type="structure" />
      <relation from="o2116" id="r350" name="to" negation="false" src="d0_s14" to="o2117" />
    </statement>
    <statement id="d0_s15">
      <text>Pedicellate spikelets usually vestigial or absent, sometimes well-developed and staminate.</text>
    </statement>
    <statement id="d0_s16">
      <text>x = 10.</text>
      <biological_entity id="o2119" name="spikelet" name_original="spikelets" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="usually" name="prominence" src="d0_s15" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="development" src="d0_s15" value="well-developed" value_original="well-developed" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity constraint="x" id="o2120" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Andropogon is a cosmopolitan genus of tropical and temperate zones, comprising approximately 120 species. Thirteen species are native to the Flora region. Andropogon bicornis has been found in the region but is not known to be established. All but A. hallii grow in the southeastern United States.</discussion>
  <discussion>Several taxa are ecologically important in North America. Andropogon gerardii is one of the most important native grasses in North America, being one of the dominant species in the tall-grass prairies that used to cover the center of the continent. Many varieties of A. glomeratus and A. virginicus aggressively colonize abandoned fields, cutover timberlands, and roadsides. Some species are used in restoration and landscaping.</discussion>
  <discussion>Species of Andropogon with solitary rames are easily confused with Schizachyrium but, in Andropogon, the lower glumes of the sessile spikelets are flat or concave and the rame internodes are not cupulate, whereas Schizachyrium has convex glumes and rame internodes with strongly cupulate apices. Successful identification of species in Andropogon sect. Leptopogon (numbers 3-14) requires mature, complete specimens and careful field study (Campbell 1983, 1986).</discussion>
  <references>
    <reference>Barnes, P.W. 1986. Variation in the big bluestem (Andropogon gerardii)-sand bluestem (Andropogon hallii) complex along a local dune/meadow gradient in the Nebraska sandhills. Amer. J. Bot. 73:172-184</reference>
    <reference>Campbell, C.S. 1983. Systematics of the Andropogon virginicus complex (Gramineae). J. Arnold Arbor. 64:171-254</reference>
    <reference>Campbell, C.S. 1986. Phylogenetic reconstructions and two new varieties in the Andropogon virginicus complex (Poaceae: Andropogoneae). Syst. Bot. 11:280-292</reference>
    <reference>Clayton, W.D. 1964. Studies in the Gramineae: V. New species of Andropogon. Kew Bull. 17:465-470.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Minn.;Conn.;N.J.;N.Y.;Wis.;Del.;D.C;Man.;Ont.;Que.;Sask.;W.Va.;Pacific Islands (Hawaii);Ark.;Ariz.;Calif.;Ga.;La.;Md.;Miss.;N.C.;N.Mex.;Nev.;Puerto Rico;S.C.;Tex.;Va.;Virgin Islands;Fla.;Wyo.;N.H.;Ala.;Ill.;Ind.;Kans.;Ky.;Mass.;Mich.;Mo.;Okla.;Pa.;R.I.;Tenn.;Utah;Maine;Vt.;Colo.;N.Dak.;Nebr.;S.Dak.;Iowa;Ohio;Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Pedicellate spikelets usually well-developed, (3.5)6-12 mm long, usually staminate; sessile spikelets 5-12 mm long (sect. Andropogon).</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Sessile spikelets with awns 8-25 mm long; ligules 0.4-2.5 mm long; hairs of the rame internodes 2.2-4.2 mm long, sparse to dense; rhizomes sometimes present, the internodes usually less than 2 cm</description>
      <determination>1 Andropogon gerardii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Sessile spikelets unawned or with awns less than 11 mm long; ligules (0.9)2.5-4.5 mm long; hairs of the rame internodes 3.7-6.6 mm long, usually dense; rhizomes always present, the internodes often more than 2 cm long</description>
      <determination>1 Andropogon hallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Pedicellate spikelets usually vestigial or absent, those of the terminal spikelet units occasionally well-developed and staminate; sessile spikelets 2.6-8.4 mm long (sect. Leptopogon).</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Peduncles with solitary rames; plants of southern Florida</description>
      <determination>3 Andropogon gracilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Peduncles with (1)2-13 rames; plants of varied distribution, including southern Florida.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Rames not or scarcely exserted at maturity; peduncles mostly less than 15 mm long at maturity. 5. Culms 30-140 (usually about 80) cm tall; blades 0.8-5 (usually about 2.5) mm wide; inflorescence units 2-31 per culm</description>
      <determination>9 Andropogon gyrans</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Culms 20-250 (usually more than 90) cm tall; blades 1.7-9.5 (usually more than 3) mm wide; inflorescence units 3-600 per culm.</description>
      <next_statement_id>6.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Blades pubescent, most hairs appressed; callus hairs 1.5-5 mm long</description>
      <determination>13 Andropogon longiberbis</determination>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Blades glabrous or with spreading (rarely appressed) hairs; callus hairs 1-3 mm long.</description>
      <next_statement_id>7.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Blades 11-52 cm long; sheaths smooth, rarely somewhat scabrous; ligules 0.2-1 mm  long;  keels  of the  lower  glumes  usually  smooth  below midlength, scabrous distally</description>
      <determination>12 Andropogon virginicus</determination>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Blades 13-109 cm long; sheaths usually scabrous; ligules 0.6-2.2 mm long; keels of the lower glumes sometimes scabrous below midlength</description>
      <determination>14 Andropogon glomeratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Rames sometimes exserted above their subtending sheaths at maturity; 1 or more peduncles more than 15 mm long at maturity.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Anthers 3.</description>
      <next_statement_id>9.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Sessile spikelets 4.5-8.4 mm long; pedicellate spikelets 1.5-3.6 mm long, sterile; plants common and widespread in the southeastern United States</description>
      <determination>4 Andropogon ternarius</determination>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Sessile spikelets 3-4 mm long; pedicellate spikelets mostly vestigial or absent, those of the terminal spikelet units well-developed and staminate; in the Flora region, known only from southern Florida</description>
      <determination>5 Andropogon bicornis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Anthers 1 (rarely 3).</description>
      <next_statement_id>9.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Peduncles with 2-13 rames</description>
      <determination>8 Andropogon liebmannii</determination>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Peduncles usually with 2 (infrequently up to 4) rames or (in A. gyrans var. gyrans and A. virginicus var. virginicus), 2-5 (infrequently up to 7) rames.</description>
      <next_statement_id>11.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Culms 30-120(140) (usually less than 100) cm tall; blades 0.8-5 (usually less than 3) mm wide; inflorescence units 2-31 per culm.</description>
      <next_statement_id>12.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Peduncles with 2-5 rames; anthers 0.6-1.7 mm long; sessile spikelets (3)4.1-4.4(5.7) mm long</description>
      <determination>9 Andropogon gyrans</determination>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Peduncleswith 2 rames; anthers 1.2-2 mm long; sessile spikelets (4)4.8-5(5.5) mm long</description>
      <determination>10 Andropogon tracyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Culms (20)90-310 (usually more than 100) cm tall; blades 1.7-9.5 (usually more than 3) mm wide; inflorescence units 5-210 per culm.</description>
      <next_statement_id>12.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Upper portion of the plants open, the branches conspicuously arching</description>
      <determination>11 Andropogon brachystachyus</determination>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Upper portion of the plants dense, the branches usually straight and erect to ascending.</description>
      <next_statement_id>14.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Rame internodes usually densely and uniformly pubescent over their entire length; anthers 1.3-3.5 mm long; sessile spikelets (3.8)4-6.1 mm long.</description>
      <next_statement_id>15.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15.</statement_id>
      <description type="morphology">Blades 15-35 cm long, often more or less pubescent; sheaths smooth, very rarely somewhat scabrous; anthers 2-3.5 mm long; inflorescence units 5-45 per culm</description>
      <determination>6 Andropogon arctatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>15.</statement_id>
      <description type="morphology">Blades 32-61 cm long, usually glabrous; sheaths often scabrous; anthers 1.3-2 mm long; inflorescence units usually at least 50 (9-210) per culm</description>
      <determination>7 Andropogon floridanus</determination>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Rame internodes sparsely pubescent basally, more densely pubescent distally; anthers 0.5-1.5 mm long; sessile spikelets 2.6-4(5).</description>
      <next_statement_id>15.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16.</statement_id>
      <description type="morphology">Blades 11-52 cm long; sheaths smooth, rarely somewhat scabrous; ligules 0.2-1 mm long; keels of the lower glumes usually smooth below midlength, scabrous distally</description>
      <determination>12 Andropogon virginicus</determination>
    </key_statement>
    <key_statement>
      <statement_id>16.</statement_id>
      <description type="morphology">Blades 13-109 cm long; sheaths usually scabrous, sometimes smooth; ligules 0.6-2.2 mm long; keels of the lower glumes sometimes scabrous below midlength</description>
      <determination>14 Andropogon glomeratus</determination>
    </key_statement>
  </key>
</bio:treatment>