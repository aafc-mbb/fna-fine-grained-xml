<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">440</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="genus">DICHANTHELIUM</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Freckmann &amp; Lelong" date="unknown" rank="section">Sphaerocarpa</taxon_name>
    <taxon_name authority="(Nash) Gould &amp; C.A. Clark" date="unknown" rank="species">erectifolium</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus dichanthelium;section sphaerocarpa;species erectifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">erectifolium</taxon_name>
    <taxon_hierarchy>genus panicum;species erectifolium</taxon_hierarchy>
  </taxon_identification>
  <number>23</number>
  <other_name type="common_name">Florida panicgrass</other_name>
  <other_name type="common_name">Erect-leaf panicgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, with few culms.</text>
      <biological_entity id="o13600" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o13601" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="few" value_original="few" />
      </biological_entity>
      <relation from="o13600" id="r2236" name="with" negation="false" src="d0_s0" to="o13601" />
    </statement>
    <statement id="d0_s1">
      <text>Basal rosettes well-differentiated;</text>
      <biological_entity constraint="basal" id="o13602" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" name="variability" src="d0_s1" value="well-differentiated" value_original="well-differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades numerous, to 15 cm, lowest blades ovate, upper blades lanceolate, grading into the cauline blades.</text>
      <biological_entity id="o13603" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="numerous" value_original="numerous" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="lowest" id="o13604" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="upper" id="o13605" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o13606" name="blade" name_original="blades" src="d0_s2" type="structure" />
      <relation from="o13605" id="r2237" name="into" negation="false" src="d0_s2" to="o13606" />
    </statement>
    <statement id="d0_s3">
      <text>Culms 30-75 cm, nearly erect, stiff, slightly fleshy or thickened;</text>
      <biological_entity id="o13607" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s3" to="75" to_unit="cm" />
        <character is_modifier="false" modifier="nearly" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="fragility" src="d0_s3" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="slightly" name="texture" src="d0_s3" value="fleshy" value_original="fleshy" />
        <character is_modifier="false" name="size_or_width" src="d0_s3" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes glabrous, often with a constricted, yellowish ring;</text>
      <biological_entity id="o13608" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13609" name="ring" name_original="ring" src="d0_s4" type="structure">
        <character is_modifier="true" name="size" src="d0_s4" value="constricted" value_original="constricted" />
        <character is_modifier="true" name="coloration" src="d0_s4" value="yellowish" value_original="yellowish" />
      </biological_entity>
      <relation from="o13608" id="r2238" modifier="often" name="with" negation="false" src="d0_s4" to="o13609" />
    </statement>
    <statement id="d0_s5">
      <text>internodes glabrous;</text>
    </statement>
    <statement id="d0_s6">
      <text>fall phase with few, long, suberect branches, sparingly rebranched, branches arising mostly from near the base.</text>
      <biological_entity id="o13610" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13611" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="few" value_original="few" />
        <character is_modifier="true" name="length_or_size" src="d0_s6" value="long" value_original="long" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="suberect" value_original="suberect" />
        <character is_modifier="false" modifier="sparingly" name="architecture" src="d0_s6" value="rebranched" value_original="rebranched" />
      </biological_entity>
      <biological_entity id="o13612" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character constraint="from " constraintid="o13613" is_modifier="false" name="orientation" src="d0_s6" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o13613" name="base" name_original="base" src="d0_s6" type="structure" />
      <relation from="o13610" id="r2239" name="with" negation="false" src="d0_s6" to="o13611" />
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves 4-7;</text>
      <biological_entity constraint="cauline" id="o13614" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sheaths shorter than the internodes, mostly glabrous, margins ciliate;</text>
      <biological_entity id="o13615" name="sheath" name_original="sheaths" src="d0_s8" type="structure">
        <character constraint="than the internodes" constraintid="o13616" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13616" name="internode" name_original="internodes" src="d0_s8" type="structure" />
      <biological_entity id="o13617" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s8" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ligules 0.2-0.5 mm;</text>
      <biological_entity id="o13618" name="ligule" name_original="ligules" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>blades 5-10 cm long, 5-10 mm wide, stiffly ascending, thick, glabrous, veins evident, bases cordate, with papillose-based cilia, margins whitish, cartilaginous.</text>
      <biological_entity id="o13619" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s10" to="10" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s10" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="width" src="d0_s10" value="thick" value_original="thick" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13620" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o13621" name="base" name_original="bases" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o13622" name="cilium" name_original="cilia" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o13623" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s10" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
      <relation from="o13621" id="r2240" name="with" negation="false" src="d0_s10" to="o13622" />
    </statement>
    <statement id="d0_s11">
      <text>Primary panicles 5-14 cm, 1/2 - 2/3 as wide as long, exserted.</text>
      <biological_entity constraint="primary" id="o13624" name="panicle" name_original="panicles" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s11" to="14" to_unit="cm" />
        <character char_type="range_value" from="1/2" name="quantity" src="d0_s11" to="2/3" />
        <character is_modifier="false" name="length_or_size" src="d0_s11" value="long" value_original="long" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 1-1.4 mm, broadly obovoid-spherical, puberulent to subglabrous.</text>
      <biological_entity id="o13625" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.4" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="obovoid-spherical" value_original="obovoid-spherical" />
        <character char_type="range_value" from="puberulent" name="pubescence" src="d0_s12" to="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Lower glumes 0.2-0.4 mm, acute, upper florets 0.8-1.1 mm, broadly ellipsoid, minutely umbonate.</text>
      <biological_entity constraint="lower" id="o13626" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n = unknown.</text>
      <biological_entity constraint="upper" id="o13627" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1.1" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s13" value="umbonate" value_original="umbonate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13628" name="chromosome" name_original="" src="d0_s14" type="structure" />
    </statement>
  </description>
  <discussion>Dichanthelium erectifolium grows in sand and peat in wet pinelands, bogs, and the shores of ponds. Its range extends from the southeastern Flora region into the Caribbean.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga.;La.;Ala.;N.C.;S.C.;Miss.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>