<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">446</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="genus">DICHANTHELIUM</taxon_name>
    <taxon_name authority="Freckmann &amp; Lelong" date="unknown" rank="section">Strigosa</taxon_name>
    <taxon_name authority="(Lam.) Gould" date="unknown" rank="species">laxiflorum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus dichanthelium;section strigosa;species laxiflorum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">×alapense</taxon_name>
    <taxon_hierarchy>genus panicum;species ×alapense</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">laxiflorum</taxon_name>
    <taxon_hierarchy>genus panicum;species laxiflorum</taxon_hierarchy>
  </taxon_identification>
  <number>29</number>
  <other_name type="common_name">Soft-tufted panicgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants densely cespitose.</text>
      <biological_entity id="o11093" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="densely" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Basal rosettes poorly differentiated;</text>
      <biological_entity constraint="basal" id="o11094" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="poorly" name="variability" src="d0_s1" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades ovate to lanceolate.</text>
      <biological_entity id="o11095" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 15-55 cm, slender, erect or radiating from a large tuft of predominantly basal leaves, lower internodes short, upper 3-5 internodes elongate;</text>
      <biological_entity id="o11096" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s3" to="55" to_unit="cm" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character constraint="from tuft" constraintid="o11097" is_modifier="false" name="arrangement" src="d0_s3" value="radiating" value_original="radiating" />
      </biological_entity>
      <biological_entity id="o11097" name="tuft" name_original="tuft" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o11098" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="predominantly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="lower" id="o11099" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="upper" id="o11100" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <biological_entity id="o11101" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o11097" id="r1820" name="part_of" negation="false" src="d0_s3" to="o11098" />
    </statement>
    <statement id="d0_s4">
      <text>nodes bearded with soft, spreading or retrorse hairs;</text>
      <biological_entity id="o11102" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character constraint="with hairs" constraintid="o11103" is_modifier="false" name="pubescence" src="d0_s4" value="bearded" value_original="bearded" />
      </biological_entity>
      <biological_entity id="o11103" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s4" value="soft" value_original="soft" />
        <character is_modifier="true" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="orientation" src="d0_s4" value="retrorse" value_original="retrorse" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>internodes glabrous;</text>
    </statement>
    <statement id="d0_s6">
      <text>fall phase branching extensively from the basal nodes, forming a dense cushion that overwinters.</text>
      <biological_entity id="o11104" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="from basal nodes" constraintid="o11105" is_modifier="false" name="architecture" src="d0_s6" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="basal" id="o11105" name="node" name_original="nodes" src="d0_s6" type="structure" />
      <biological_entity id="o11106" name="cushion" name_original="cushion" src="d0_s6" type="structure">
        <character is_modifier="true" name="density" src="d0_s6" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o11105" id="r1821" name="forming a" negation="false" src="d0_s6" to="o11106" />
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves 2-4;</text>
      <biological_entity constraint="cauline" id="o11107" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sheaths usually longer than the internodes, pilose, hairs to 4 mm, retrorse or spreading;</text>
      <biological_entity id="o11108" name="sheath" name_original="sheaths" src="d0_s8" type="structure">
        <character constraint="than the internodes" constraintid="o11109" is_modifier="false" name="length_or_size" src="d0_s8" value="usually longer" value_original="usually longer" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o11109" name="internode" name_original="internodes" src="d0_s8" type="structure" />
      <biological_entity id="o11110" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="retrorse" value_original="retrorse" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>ligules 0.2-1 mm, at low magnification appearing to membranous and ciliate, at high magnification evidently of hairs that coherent at the base;</text>
      <biological_entity id="o11111" name="ligule" name_original="ligules" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11112" name="magnification" name_original="magnification" src="d0_s9" type="structure">
        <character is_modifier="true" name="position" src="d0_s9" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o11113" name="magnification" name_original="magnification" src="d0_s9" type="structure">
        <character is_modifier="true" name="height" src="d0_s9" value="high" value_original="high" />
        <character is_modifier="true" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o11114" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="height" src="d0_s9" value="high" value_original="high" />
        <character is_modifier="true" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o11115" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="true" name="height" src="d0_s9" value="high" value_original="high" />
        <character is_modifier="true" name="fusion" src="d0_s9" value="coherent" value_original="coherent" />
        <character is_modifier="true" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o11111" id="r1822" name="at" negation="false" src="d0_s9" to="o11112" />
      <relation from="o11112" id="r1823" name="appearing to" negation="false" src="d0_s9" to="o11113" />
      <relation from="o11112" id="r1824" name="appearing to" negation="false" src="d0_s9" to="o11114" />
      <relation from="o11112" id="r1825" name="appearing to" negation="false" src="d0_s9" to="o11115" />
    </statement>
    <statement id="d0_s10">
      <text>blades 4-17 cm long, 4-12 mm wide, lanceolate, at least 3/4 as long as the basal blades, spreading to suberect, thin, soft, lax, yellowish-green, nearly glabrous or densely pilose on 1 or both surfaces, margins usually finely short-ciliate, at least on the basal 1/2, cilia not papillose-based.</text>
      <biological_entity id="o11116" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s10" to="17" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s10" to="12" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character constraint="as-long-as basal blades" constraintid="o11117" modifier="at least" name="quantity" src="d0_s10" value="3/4" value_original="3/4" />
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s10" to="suberect" />
        <character is_modifier="false" name="width" src="d0_s10" value="thin" value_original="thin" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s10" value="soft" value_original="soft" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s10" value="lax" value_original="lax" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowish-green" value_original="yellowish-green" />
        <character is_modifier="false" modifier="nearly" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character constraint="on surfaces" constraintid="o11118" is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="basal" id="o11117" name="blade" name_original="blades" src="d0_s10" type="structure" />
      <biological_entity id="o11118" name="surface" name_original="surfaces" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o11119" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually finely" name="architecture_or_pubescence_or_shape" src="d0_s10" value="short-ciliate" value_original="short-ciliate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o11120" name="1/2" name_original="1/2" src="d0_s10" type="structure" />
      <biological_entity id="o11121" name="cilium" name_original="cilia" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o11119" id="r1826" modifier="at-least" name="on" negation="false" src="d0_s10" to="o11120" />
    </statement>
    <statement id="d0_s11">
      <text>Primary panicles 4-12 cm long, 3-8 cm wide, well-exserted;</text>
      <biological_entity constraint="primary" id="o11122" name="panicle" name_original="panicles" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s11" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s11" to="8" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s11" value="well-exserted" value_original="well-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>secondary panicles more compact, usually not exserted above the crowded basal leaves;</text>
      <biological_entity constraint="secondary" id="o11123" name="panicle" name_original="panicles" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s12" value="compact" value_original="compact" />
      </biological_entity>
      <biological_entity constraint="basal" id="o11124" name="leaf" name_original="leaves" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="crowded" value_original="crowded" />
      </biological_entity>
      <relation from="o11123" id="r1827" modifier="usually not" name="exserted above the" negation="false" src="d0_s12" to="o11124" />
    </statement>
    <statement id="d0_s13">
      <text>rachises and branches wiry, spreading or deflexed, often pilose.</text>
      <biological_entity id="o11125" name="rachis" name_original="rachises" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="wiry" value_original="wiry" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="deflexed" value_original="deflexed" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s13" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o11126" name="branch" name_original="branches" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="wiry" value_original="wiry" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="deflexed" value_original="deflexed" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s13" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Spikelets 1.7-2.3 mm long, 1-1.2 mm wide, broadly ovate or oblong-obovoid, with papillose-based hairs, obtuse.</text>
      <biological_entity id="o11127" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s14" to="2.3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s14" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong-obovoid" value_original="oblong-obovoid" />
        <character is_modifier="false" name="shape" notes="" src="d0_s14" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <biological_entity id="o11128" name="hair" name_original="hairs" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o11127" id="r1828" name="with" negation="false" src="d0_s14" to="o11128" />
    </statement>
    <statement id="d0_s15">
      <text>Lower glumes 1/4 - 1/3 as long as the spikelets, broadly deltoid;</text>
      <biological_entity constraint="lower" id="o11129" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o11130" from="1/4" name="quantity" src="d0_s15" to="1/3" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s15" value="deltoid" value_original="deltoid" />
      </biological_entity>
      <biological_entity id="o11130" name="spikelet" name_original="spikelets" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>upper glumes and lower lemmas subequal, usually fully covering the upper florets;</text>
      <biological_entity constraint="upper" id="o11131" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o11132" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="size" src="d0_s16" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="upper" id="o11133" name="floret" name_original="florets" src="d0_s16" type="structure" />
      <relation from="o11131" id="r1829" modifier="usually fully" name="covering the" negation="false" src="d0_s16" to="o11133" />
      <relation from="o11132" id="r1830" modifier="usually fully" name="covering the" negation="false" src="d0_s16" to="o11133" />
    </statement>
    <statement id="d0_s17">
      <text>upper florets 1.5-1.8 mm long, 1-1.2 mm wide, broadly ellipsoid or obovoid, minutely umbonate.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 18.</text>
      <biological_entity constraint="upper" id="o11134" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s17" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s17" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s17" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="minutely" name="shape" src="d0_s17" value="umbonate" value_original="umbonate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11135" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dichanthelium laxiflorum is a widespread, common species that grows in mesic deciduous woods, and occasionally in drier, more open woodlands. Its range extends south from the Flora region into Mexico. The density of the pubescence on the blade surfaces varies greatly.</discussion>
  <discussion>The primary (spring) panicles are apparently chasmogamous; the secondary panicles are largely cleistogamous and are produced from late spring to winter.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Okla.;Miss.;Tex.;La.;W.Va.;Pa.;Mo.;Ohio;Ala.;D.C.;Tenn.;N.C.;S.C.;Va.;Ark.;Ill.;Ga.;Ind.;Ky.;Fla.;R.I.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>