<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">416</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="(Hitchc. &amp; Chase) Gould" date="unknown" rank="genus">DICHANTHELIUM</taxon_name>
    <taxon_name authority="Freckmann &amp; Lelong" date="unknown" rank="section">Macrocarpa</taxon_name>
    <taxon_name authority="(Vasey) Freckmann" date="unknown" rank="species">leibergii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus dichanthelium;section macrocarpa;species leibergii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">leibergii</taxon_name>
    <taxon_hierarchy>genus panicum;species leibergii</taxon_hierarchy>
  </taxon_identification>
  <number>6</number>
  <other_name type="common_name">Leibergs panicgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, with knotty rhizomes no more than 2 mm thick.</text>
      <biological_entity id="o14867" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character char_type="range_value" from="2" from_unit="mm" name="thickness" src="d0_s0" upper_restricted="false" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o14868" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="knotty" value_original="knotty" />
        <character is_modifier="false" name="quantity" src="d0_s0" value="no" value_original="no" />
      </biological_entity>
      <relation from="o14867" id="r2483" name="with" negation="false" src="d0_s0" to="o14868" />
    </statement>
    <statement id="d0_s1">
      <text>Basal rosettes well-differentiated;</text>
      <biological_entity constraint="basal" id="o14869" name="rosette" name_original="rosettes" src="d0_s1" type="structure">
        <character is_modifier="false" name="variability" src="d0_s1" value="well-differentiated" value_original="well-differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades few, small, ovate to lanceolate.</text>
      <biological_entity id="o14870" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s2" value="few" value_original="few" />
        <character is_modifier="false" name="size" src="d0_s2" value="small" value_original="small" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s2" to="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 24-80 cm, glabrous or puberulent;</text>
      <biological_entity id="o14871" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="24" from_unit="cm" name="some_measurement" src="d0_s3" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>nodes sparsely, spreading-pilose;</text>
      <biological_entity id="o14872" name="node" name_original="nodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="spreading-pilose" value_original="spreading-pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>internodes mostly elongated, glabrous or puberulent;</text>
    </statement>
    <statement id="d0_s6">
      <text>fall phase with a few suberect branches from the lower and midculm nodes, blades slightly reduced, secondary panicles partially exserted.</text>
      <biological_entity id="o14873" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="length" src="d0_s5" value="elongated" value_original="elongated" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o14874" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="few" value_original="few" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="suberect" value_original="suberect" />
      </biological_entity>
      <biological_entity id="o14875" name="lower" name_original="lower" src="d0_s6" type="structure" />
      <biological_entity constraint="midculm" id="o14876" name="node" name_original="nodes" src="d0_s6" type="structure" />
      <biological_entity id="o14877" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o14878" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="partially" name="position" src="d0_s6" value="exserted" value_original="exserted" />
      </biological_entity>
      <relation from="o14873" id="r2484" name="with" negation="false" src="d0_s6" to="o14874" />
      <relation from="o14874" id="r2485" name="from" negation="false" src="d0_s6" to="o14875" />
    </statement>
    <statement id="d0_s7">
      <text>Cauline leaves 3-4;</text>
      <biological_entity constraint="cauline" id="o14879" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sheaths not overlapping, with ascending papillose-based hairs;</text>
      <biological_entity id="o14880" name="sheath" name_original="sheaths" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s8" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o14881" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o14880" id="r2486" name="with" negation="false" src="d0_s8" to="o14881" />
    </statement>
    <statement id="d0_s9">
      <text>ligules 0.3-0.5 mm, membranous, ciliate, cilia longer than the membranous portion;</text>
      <biological_entity id="o14882" name="ligule" name_original="ligules" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o14883" name="cilium" name_original="cilia" src="d0_s9" type="structure">
        <character constraint="than the membranous portion" constraintid="o14884" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14884" name="portion" name_original="portion" src="d0_s9" type="structure">
        <character is_modifier="true" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>blades 5-15 cm long, 7-13 mm wide, ascending to erect, sparsely to densely pubescent with papillose-based hairs, with 9-11 prominent major veins and 25-50 minor veins, bases truncate to cordate, margins with papillose-based cilia.</text>
      <biological_entity id="o14885" name="blade" name_original="blades" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s10" to="15" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s10" to="13" to_unit="mm" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s10" to="erect" />
        <character constraint="with hairs" constraintid="o14886" is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o14886" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o14888" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s10" to="11" />
        <character is_modifier="true" name="prominence" src="d0_s10" value="prominent" value_original="prominent" />
        <character is_modifier="true" name="size" src="d0_s10" value="major" value_original="major" />
      </biological_entity>
      <biological_entity constraint="minor" id="o14889" name="vein" name_original="veins" src="d0_s10" type="structure" />
      <biological_entity id="o14890" name="base" name_original="bases" src="d0_s10" type="structure">
        <character char_type="range_value" from="truncate" name="shape" src="d0_s10" to="cordate" />
      </biological_entity>
      <biological_entity id="o14891" name="margin" name_original="margins" src="d0_s10" type="structure" />
      <biological_entity id="o14892" name="cilium" name_original="cilia" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o14885" id="r2487" name="with" negation="false" src="d0_s10" to="o14888" />
      <relation from="o14891" id="r2488" name="with" negation="false" src="d0_s10" to="o14892" />
    </statement>
    <statement id="d0_s11">
      <text>Panicles 6-10 cm long, 3-5 cm wide, their length usually less than twice their width, eventually well-exserted, with 20-40 spikelets;</text>
      <biological_entity id="o14893" name="panicle" name_original="panicles" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s11" to="10" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s11" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="length" src="d0_s11" value="0-2 times their width" value_original="0-2 times their width" />
        <character is_modifier="false" modifier="eventually" name="position" src="d0_s11" value="well-exserted" value_original="well-exserted" />
      </biological_entity>
      <biological_entity id="o14894" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s11" to="40" />
      </biological_entity>
      <relation from="o14893" id="r2489" name="with" negation="false" src="d0_s11" to="o14894" />
    </statement>
    <statement id="d0_s12">
      <text>branches spreading to ascending.</text>
      <biological_entity id="o14895" name="branch" name_original="branches" src="d0_s12" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s12" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spikelets 3.3-3.8 mm long, 1.6-2 mm wide, ellipsoid-obovoid, turgid, pubescent, hairs papillose-based, apices rounded.</text>
      <biological_entity id="o14896" name="spikelet" name_original="spikelets" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.3" from_unit="mm" name="length" src="d0_s13" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid-obovoid" value_original="ellipsoid-obovoid" />
        <character is_modifier="false" name="shape" src="d0_s13" value="turgid" value_original="turgid" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o14897" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o14898" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Lower glumes about 1.8 mm, narrowly triangular;</text>
      <biological_entity constraint="lower" id="o14899" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character name="some_measurement" src="d0_s14" unit="mm" value="1.8" value_original="1.8" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s14" value="triangular" value_original="triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower florets staminate;</text>
      <biological_entity constraint="lower" id="o14900" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper florets mucronate.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 18.</text>
      <biological_entity constraint="upper" id="o14901" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="mucronate" value_original="mucronate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14902" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dichanthelium leibergii grows primarily on prairie relics, but is occasionally found in sandy woodlands. It is restricted to the Flora region. The primary panicles are produced from mid-May through July, the secondary panicles from late June to September. Sterile putative hybrids with D. acuminatum and D. xantho-physum are occasionally found.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Minn.;N.Y.;Mo.;Alta.;Man.;Ont.;Sask.;Mich.;Wis.;Kans.;N.Dak.;Nebr.;S.Dak.;Pa.;Ill.;Ind.;Iowa;Ohio</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>