<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">42</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">TRIPLASIS</taxon_name>
    <taxon_name authority="(Walter) Chapm." date="unknown" rank="species">purpurea</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus triplasis;species purpurea</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Purple sandgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual and tufted or perennial and occasionally rhizomatous.</text>
      <biological_entity id="o1418" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 14-100 cm, usually ascending;</text>
      <biological_entity id="o1419" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="14" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes glabrous.</text>
      <biological_entity id="o1420" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Ligules to 1 mm, of hairs;</text>
      <biological_entity id="o1421" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1422" name="hair" name_original="hairs" src="d0_s3" type="structure" />
      <relation from="o1421" id="r229" name="consists_of" negation="false" src="d0_s3" to="o1422" />
    </statement>
    <statement id="d0_s4">
      <text>blades 1-5 mm wide, flat or involute, hispid or with papillose-based hairs.</text>
      <biological_entity id="o1423" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s4" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s4" value="involute" value_original="involute" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="with papillose-based hairs" value_original="with papillose-based hairs" />
      </biological_entity>
      <biological_entity id="o1424" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o1423" id="r230" name="with" negation="false" src="d0_s4" to="o1424" />
    </statement>
    <statement id="d0_s5">
      <text>Panicles 3-7 cm long, 1-6 cm wide.</text>
      <biological_entity id="o1425" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s5" to="7" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 6.5-9 mm, with 3-4 florets.</text>
      <biological_entity id="o1426" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="6.5" from_unit="mm" name="some_measurement" src="d0_s6" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1427" name="floret" name_original="florets" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="4" />
      </biological_entity>
      <relation from="o1426" id="r231" name="with" negation="false" src="d0_s6" to="o1427" />
    </statement>
    <statement id="d0_s7">
      <text>Glumes about 2 mm, glabrous or scabrous, apices erose;</text>
      <biological_entity id="o1428" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character name="some_measurement" src="d0_s7" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s7" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o1429" name="apex" name_original="apices" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s7" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lemmas 3-4 mm, lobes shorter than 1 mm, rounded;</text>
      <biological_entity id="o1430" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1431" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s8" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>awns shorter than 2 mm, straight;</text>
      <biological_entity id="o1432" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>paleas about 2.5 mm, keels ciliate;</text>
      <biological_entity id="o1433" name="palea" name_original="paleas" src="d0_s10" type="structure">
        <character name="some_measurement" src="d0_s10" unit="mm" value="2.5" value_original="2.5" />
      </biological_entity>
      <biological_entity id="o1434" name="keel" name_original="keels" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s10" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers about 2 mm, reddish-purple.</text>
      <biological_entity id="o1435" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="2" value_original="2" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s11" value="reddish-purple" value_original="reddish-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Caryopses about 2 mm long, 0.6 mm wide, tapering distally, tan.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 40.</text>
      <biological_entity id="o1436" name="caryopsis" name_original="caryopses" src="d0_s12" type="structure">
        <character name="length" src="d0_s12" unit="mm" value="2" value_original="2" />
        <character name="width" src="d0_s12" unit="mm" value="0.6" value_original="0.6" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s12" value="tapering" value_original="tapering" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="tan" value_original="tan" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1437" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Triplasis purpurea grows in sandy soils throughout the eastern and central portion of the Flora region, extending southward through Mexico to Costa Rica. It is far more common in maritime dunes than T. americana. Plants in the Flora region belong to Triplasis purpurea (Walter) Chapm. var. purpurea.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Del.;Wis.;Fla.;N.H.;N.Mex.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;Va.;Colo.;Ont.;Ala.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Ark.;Ill.;Ga.;Ind.;Iowa;Ky.;Maine;Md.;Mass.;Ohio;Mo.;Mich.;R.I.;Miss.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>