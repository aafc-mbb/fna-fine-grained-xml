<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="Michx." date="unknown" rank="species">setaceum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">setaceum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species setaceum;variety setaceum</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Thin paspalum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect.</text>
      <biological_entity id="o28471" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly cauline;</text>
      <biological_entity id="o28472" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o28473" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s1" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades to 22 cm long, 1.5-7 mm wide, lax to straight, conspicuously hirsute, usually with long stiff hairs and short soft hairs, grayish-green, margins hirsute.</text>
      <biological_entity id="o28474" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="22" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s2" to="7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="lax" value_original="lax" />
        <character is_modifier="false" name="course" src="d0_s2" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="conspicuously" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s2" value="grayish-green" value_original="grayish-green" />
      </biological_entity>
      <biological_entity id="o28475" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
        <character is_modifier="true" name="fragility" src="d0_s2" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o28476" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity id="o28477" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <relation from="o28474" id="r4854" modifier="usually" name="with" negation="false" src="d0_s2" to="o28475" />
      <relation from="o28474" id="r4855" modifier="usually" name="with" negation="false" src="d0_s2" to="o28476" />
    </statement>
    <statement id="d0_s3">
      <text>Panicle branches 2-11.2 cm;</text>
      <biological_entity constraint="panicle" id="o28478" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="11.2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branch axes 0.3-0.9 mm wide.</text>
      <biological_entity constraint="branch" id="o28479" name="axis" name_original="axes" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s4" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 1.4-1.9 mm long, 1.1-1.6 mm wide, elliptic, obovate, orbicular, or suborbicular, pubescent to nearly glabrous;</text>
      <biological_entity id="o28480" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s5" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="1.1" from_unit="mm" name="width" src="d0_s5" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbicular" value_original="suborbicular" />
        <character is_modifier="false" name="shape" src="d0_s5" value="orbicular" value_original="orbicular" />
        <character is_modifier="false" name="shape" src="d0_s5" value="suborbicular" value_original="suborbicular" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s5" to="nearly glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>lower lemmas without an evident midvein;</text>
      <biological_entity constraint="lower" id="o28481" name="lemma" name_original="lemmas" src="d0_s6" type="structure" />
      <biological_entity id="o28482" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o28481" id="r4856" name="without" negation="false" src="d0_s6" to="o28482" />
    </statement>
    <statement id="d0_s7">
      <text>upper lemmas 1.3-2 mm.</text>
      <biological_entity constraint="upper" id="o28483" name="lemma" name_original="lemmas" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum setaceum var. setaceum grows in open areas and sandy soils, often at the edges of forests, primarily on the southeastern coastal plain of the United States, from southern New England to eastern Mexico, but extending inland to western Virginia, Missouri, and Arkansas. It also grows in Cuba.</discussion>
  
</bio:treatment>