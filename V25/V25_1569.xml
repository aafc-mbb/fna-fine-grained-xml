<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">649</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">APLUDA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus apluda</taxon_hierarchy>
  </taxon_identification>
  <number>26.14</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>often scrambling.</text>
      <biological_entity id="o8998" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="often" name="growth_form" src="d0_s1" value="scrambling" value_original="scrambling" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms to 3 m, decumbent.</text>
      <biological_entity id="o8999" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s2" to="3" to_unit="m" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves not aromatic;</text>
      <biological_entity id="o9000" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s3" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths open;</text>
      <biological_entity id="o9001" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules membranous;</text>
      <biological_entity id="o9002" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades linear, often pseudopetiolate.</text>
      <biological_entity id="o9003" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s6" value="pseudopetiolate" value_original="pseudopetiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences false panicles, individual inflorescence units with solitary rames;</text>
      <biological_entity id="o9004" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity constraint="false" id="o9005" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
      <biological_entity id="o9007" name="rame" name_original="rames" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o9006" id="r1474" name="with" negation="false" src="d0_s7" to="o9007" />
    </statement>
    <statement id="d0_s8">
      <text>rames to 1 cm, often enclosed by the subtending leaf-sheath, with 1 sessile and 2 unequally pedicellate spikelets;</text>
      <biological_entity constraint="subtending" id="o9008" name="sheath" name_original="leaf-sheath" src="d0_s8" type="structure" />
      <biological_entity id="o9009" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" modifier="unequally" name="architecture" src="d0_s8" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o9006" id="r1475" modifier="often" name="enclosed by the" negation="false" src="d0_s8" to="o9008" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation at the base of the sessile spikelets, sometimes also at the base of the pedicellate spikelets.</text>
      <biological_entity constraint="inflorescence" id="o9006" name="unit" name_original="units" src="d0_s7" type="structure" constraint_original="individual inflorescence" />
      <biological_entity id="o9010" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o9011" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o9012" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o9013" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o9006" id="r1476" name="at" negation="false" src="d0_s9" to="o9010" />
      <relation from="o9010" id="r1477" name="part_of" negation="false" src="d0_s9" to="o9011" />
      <relation from="o9012" id="r1478" name="part_of" negation="false" src="d0_s9" to="o9013" />
    </statement>
    <statement id="d0_s10">
      <text>Sessile spikelets laterally compressed, with a large, bulbous callus;</text>
      <biological_entity id="o9014" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o9015" name="callus" name_original="callus" src="d0_s10" type="structure">
        <character is_modifier="true" name="size" src="d0_s10" value="large" value_original="large" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="bulbous" value_original="bulbous" />
      </biological_entity>
      <relation from="o9014" id="r1479" name="with" negation="false" src="d0_s10" to="o9015" />
    </statement>
    <statement id="d0_s11">
      <text>lower glumes coriaceous, without keels or wings, smooth, bidentate;</text>
      <biological_entity constraint="lower" id="o9016" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="texture" src="d0_s11" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" notes="" src="d0_s11" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="shape" src="d0_s11" value="bidentate" value_original="bidentate" />
      </biological_entity>
      <biological_entity id="o9017" name="keel" name_original="keels" src="d0_s11" type="structure" />
      <biological_entity id="o9018" name="wing" name_original="wings" src="d0_s11" type="structure" />
      <relation from="o9016" id="r1480" name="without" negation="false" src="d0_s11" to="o9017" />
      <relation from="o9016" id="r1481" name="without" negation="false" src="d0_s11" to="o9018" />
    </statement>
    <statement id="d0_s12">
      <text>upper glumes unawned;</text>
      <biological_entity constraint="upper" id="o9019" name="glume" name_original="glumes" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper lemmas awned or unawned.</text>
      <biological_entity constraint="upper" id="o9020" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
        <character name="architecture_or_shape" src="d0_s13" value="unawned" value_original="unawned" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Pedicels flat, wide, adjacent to each other, appressed but not fused to the rame axes.</text>
      <biological_entity id="o9021" name="pedicel" name_original="pedicels" src="d0_s14" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s14" value="flat" value_original="flat" />
        <character is_modifier="false" name="width" src="d0_s14" value="wide" value_original="wide" />
        <character is_modifier="false" name="arrangement" src="d0_s14" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s14" value="appressed" value_original="appressed" />
        <character constraint="to axes" constraintid="o9022" is_modifier="false" modifier="not" name="fusion" src="d0_s14" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o9022" name="axis" name_original="axes" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>Pedicellate spikelets usually unequal, unawned, 1 staminate or bisexual and as large as the sessile spikelet, the other sterile and usually smaller, x = 10.</text>
      <biological_entity id="o9023" name="spikelet" name_original="spikelets" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s15" value="unequal" value_original="unequal" />
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" notes="" src="d0_s15" value="sterile" value_original="sterile" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s15" value="smaller" value_original="smaller" />
      </biological_entity>
      <biological_entity id="o9024" name="spikelet" name_original="spikelet" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="x" id="o9025" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
      </biological_entity>
      <relation from="o9023" id="r1482" name="as large as" negation="false" src="d0_s15" to="o9024" />
    </statement>
  </description>
  <discussion>Apluda is treated here as consisting of a single weedy species that is native to tropical Asia and Australia, where it grows primarily in thickets and forest margins. It is not known to be established in the Flora region.</discussion>
  <references>
    <reference>Clayton, W.D. 1994. Apluda. Pp. 35-37 in W.D. Clayton, G. Davidse, F. Gould, M. Lazarides, and T.R. Soderstrom. A Revised Handbook to the Flora of Ceylon, vol. 8 (ed. M.D. Dassanayake). Amerind Publishing Co., New Delhi, India. 458 pp.</reference>
    <reference>Reed, C.F. 1964. A flora of the chrome and manganese ore piles at Canton, in the Port of Baltimore, Maryland and at Newport News, Virginia, with descriptions of genera and species new to the flora of the eastern United States. Phytologia 10:321-405.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>