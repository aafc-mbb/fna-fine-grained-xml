<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">649</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">APLUDA</taxon_name>
    <taxon_name authority="L." date="unknown" rank="species">mutica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus apluda;species mutica</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Mauritian grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms to 3 m, decumbent and rooting from the lower nodes.</text>
      <biological_entity id="o12572" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="3" to_unit="m" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character constraint="from lower nodes" constraintid="o12573" is_modifier="false" name="architecture" src="d0_s0" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o12573" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Blades 5-25 cm long, 2-10 mm wide, flat, attenuate distally.</text>
      <biological_entity id="o12574" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s1" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s1" to="10" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s1" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences 3-40 cm;</text>
    </statement>
    <statement id="d0_s3">
      <text>rames to 10 mm long, subtending sheaths 3.5-10 mm, narrowly ovate in side view.</text>
      <biological_entity id="o12575" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s3" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly; in side view" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o12576" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="true" name="some_measurement" src="d0_s3" unit="mm" value="10" value_original="10" />
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
      </biological_entity>
      <relation from="o12575" id="r2065" name="rames to" negation="false" src="d0_s3" to="o12576" />
    </statement>
    <statement id="d0_s4">
      <text>Sessile spikelets 2-6 mm;</text>
      <biological_entity id="o12577" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lower glumes narrowly elliptic-lanceolate;</text>
      <biological_entity constraint="lower" id="o12578" name="glume" name_original="glumes" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="elliptic-lanceolate" value_original="elliptic-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>upper lemmas unawned or awned;</text>
      <biological_entity constraint="upper" id="o12579" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>awns 4-12 mm.</text>
      <biological_entity id="o12580" name="awn" name_original="awns" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicellate spikelets broadly lanceolate;</text>
      <biological_entity id="o12581" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>larger spikelets 2-5 mm;</text>
      <biological_entity constraint="larger" id="o12582" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>smaller spikelets 2-4 mm. 2n = 20, 40.</text>
      <biological_entity constraint="smaller" id="o12583" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12584" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="20" value_original="20" />
        <character name="quantity" src="d0_s10" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Reed (1964) reported finding Apluda mutica on chrome ore piles in Canton, Maryland (a temporary unloading ground for ores). The report has not been verified because his voucher specimens were not accessible at the time of writing. They were acquired by the Missouri Botanical Garden in 2001.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>