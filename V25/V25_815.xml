<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">222</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Desv." date="unknown" rank="genus">EUSTACHYS</taxon_name>
    <taxon_name authority="Chapm." date="unknown" rank="species">floridana</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eustachys;species floridana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chloris</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">floridana</taxon_name>
    <taxon_hierarchy>genus chloris;species floridana</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Florida fingergrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 50-100 cm, erect.</text>
      <biological_entity id="o11170" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s0" to="100" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Blades to 30 cm long, to 8.2 mm wide, apices acute.</text>
      <biological_entity id="o11171" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s1" to="8.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11172" name="apex" name_original="apices" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Panicles with 1-3 branches;</text>
      <biological_entity id="o11173" name="panicle" name_original="panicles" src="d0_s2" type="structure" />
      <biological_entity id="o11174" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s2" to="3" />
      </biological_entity>
      <relation from="o11173" id="r1835" name="with" negation="false" src="d0_s2" to="o11174" />
    </statement>
    <statement id="d0_s3">
      <text>branches 5-13 cm.</text>
      <biological_entity id="o11175" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Spikelets 3-3.7 mm;</text>
      <biological_entity id="o11176" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s4" to="3.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>florets 3.</text>
      <biological_entity id="o11177" name="floret" name_original="florets" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Lower glumes 1.8-2 mm, apices acute to obtuse;</text>
      <biological_entity constraint="lower" id="o11178" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11179" name="apex" name_original="apices" src="d0_s6" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s6" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>upper glumes 2.2-3.1 mm, oblanceolate, truncate to occasionally bilobed, lobes obtuse, awned from between the lobes, awns 0.5-1 mm;</text>
      <biological_entity constraint="upper" id="o11180" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s7" value="truncate to occasionally" value_original="truncate to occasionally" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s7" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape_or_architecture" src="d0_s7" value="truncate to occasionally" value_original="truncate to occasionally" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s7" value="bilobed" value_original="bilobed" />
      </biological_entity>
      <biological_entity id="o11181" name="lobe" name_original="lobes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="obtuse" value_original="obtuse" />
        <character constraint="from " constraintid="o11182" is_modifier="false" name="architecture_or_shape" src="d0_s7" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o11182" name="lobe" name_original="lobes" src="d0_s7" type="structure" />
      <biological_entity id="o11183" name="awn" name_original="awns" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>calluses with tufts of hairs, hairs to 0.6 mm;</text>
      <biological_entity id="o11184" name="callus" name_original="calluses" src="d0_s8" type="structure" />
      <biological_entity id="o11185" name="tuft" name_original="tufts" src="d0_s8" type="structure" />
      <biological_entity id="o11186" name="hair" name_original="hairs" src="d0_s8" type="structure" />
      <biological_entity id="o11187" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s8" to="0.6" to_unit="mm" />
      </biological_entity>
      <relation from="o11184" id="r1836" name="with" negation="false" src="d0_s8" to="o11185" />
      <relation from="o11185" id="r1837" name="part_of" negation="false" src="d0_s8" to="o11186" />
    </statement>
    <statement id="d0_s9">
      <text>lowest lemmas 2.9-3.7 mm, ovate, tan to light-brown at maturity, lateral-veins and keels with appressed, whitish to golden hairs, hairs to 0.7 mm, apices acute, awned, awns 0.4-0.6 mm, arising from just below the apices;</text>
      <biological_entity constraint="lowest" id="o11188" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s9" to="3.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character char_type="range_value" constraint="at keels" constraintid="o11190" from="tan" name="coloration" src="d0_s9" to="light-brown" />
      </biological_entity>
      <biological_entity id="o11189" name="lateral-vein" name_original="lateral-veins" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="maturity" value_original="maturity" />
      </biological_entity>
      <biological_entity id="o11190" name="keel" name_original="keels" src="d0_s9" type="structure" />
      <biological_entity id="o11191" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="whitish" is_modifier="true" name="coloration" src="d0_s9" to="golden" />
      </biological_entity>
      <biological_entity id="o11192" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11193" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o11195" name="apex" name_original="apices" src="d0_s9" type="structure" />
      <relation from="o11190" id="r1838" name="with" negation="false" src="d0_s9" to="o11191" />
    </statement>
    <statement id="d0_s10">
      <text>second lemmas 1.5-2.6 mm, obovate, acute, mucronate.</text>
      <biological_entity id="o11194" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s9" to="0.6" to_unit="mm" />
        <character constraint="just below apices" constraintid="o11195" is_modifier="false" name="orientation" src="d0_s9" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o11196" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s10" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Caryopses about 1.7 mm. 2n = unknown.</text>
      <biological_entity id="o11197" name="caryopsis" name_original="caryopses" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="1.7" value_original="1.7" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11198" name="chromosome" name_original="" src="d0_s11" type="structure" />
    </statement>
  </description>
  <discussion>Eustachys floridana grows in dry, sandy woods and old fields. It is endemic to the southeastern United States.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.;Ala.;Ga.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>