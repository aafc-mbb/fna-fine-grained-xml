<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">659</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ANDROPOGON</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="section">Leptopogon</taxon_name>
    <taxon_name authority="Nash" date="unknown" rank="species">tracyi</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus andropogon;section leptopogon;species tracyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>10</number>
  <other_name type="common_name">Tracy's bluestem</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, upper portion dense, cylindrical.</text>
      <biological_entity id="o9869" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="upper" id="o9870" name="portion" name_original="portion" src="d0_s0" type="structure">
        <character is_modifier="false" name="density" src="d0_s0" value="dense" value_original="dense" />
        <character is_modifier="false" name="shape" src="d0_s0" value="cylindrical" value_original="cylindrical" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 50-120 cm;</text>
      <biological_entity id="o9871" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s1" to="120" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes not glaucous;</text>
      <biological_entity id="o9872" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="not" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>branches mostly erect, straight.</text>
      <biological_entity id="o9873" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s3" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths smooth;</text>
      <biological_entity id="o9874" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s4" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.2-0.5 mm, ciliate, cilia 0.2-0.8 mm;</text>
      <biological_entity id="o9875" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o9876" name="cilium" name_original="cilia" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 10-22 cm long, 1.2-2.6 mm wide, glabrous or sparsely pubescent, with spreading hairs.</text>
      <biological_entity id="o9877" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="22" to_unit="cm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s6" to="2.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o9878" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o9877" id="r1617" name="with" negation="false" src="d0_s6" to="o9878" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescence units 3-11 per culm;</text>
      <biological_entity id="o9880" name="culm" name_original="culm" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>subtending sheaths (2.8) 4.1-5.8 (7.2) cm long, (3) 4-4.7 (5.8) mm wide;</text>
      <biological_entity constraint="inflorescence" id="o9879" name="unit" name_original="units" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="per culm" constraintid="o9880" from="3" name="quantity" src="d0_s7" to="11" />
        <character name="length" src="d0_s8" unit="cm" value="2.8" value_original="2.8" />
        <character char_type="range_value" from="4.1" from_unit="cm" name="length" src="d0_s8" to="5.8" to_unit="cm" />
        <character name="width" src="d0_s8" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="4.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9881" name="sheath" name_original="sheaths" src="d0_s8" type="structure" />
      <relation from="o9879" id="r1618" name="subtending" negation="false" src="d0_s8" to="o9881" />
    </statement>
    <statement id="d0_s9">
      <text>peduncles (9) 14-31 (65) mm, with 2 rames;</text>
      <biological_entity id="o9883" name="rame" name_original="rames" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <relation from="o9882" id="r1619" name="with" negation="false" src="d0_s9" to="o9883" />
    </statement>
    <statement id="d0_s10">
      <text>rames (1.5) 2.4-3.6 (4.2) cm, usually exserted at maturity, pubescence increasing in density distally within each internode.</text>
      <biological_entity id="o9882" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="9" value_original="9" />
        <character char_type="range_value" from="14" from_unit="mm" name="some_measurement" src="d0_s9" to="31" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9884" name="internode" name_original="internode" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sessile spikelets (4) 4.8-5 (5.5) mm;</text>
      <biological_entity id="o9885" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="4.8" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>callus hairs 1.5-3.5 mm;</text>
      <biological_entity constraint="callus" id="o9886" name="hair" name_original="hairs" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>keels of lower glumes scabrous only above the midpoint;</text>
      <biological_entity id="o9887" name="keel" name_original="keels" src="d0_s13" type="structure" constraint="glume" constraint_original="glume; glume">
        <character constraint="above midpoint" constraintid="o9889" is_modifier="false" name="pubescence_or_relief" src="d0_s13" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o9888" name="glume" name_original="glumes" src="d0_s13" type="structure" />
      <biological_entity id="o9889" name="midpoint" name_original="midpoint" src="d0_s13" type="structure" />
      <relation from="o9887" id="r1620" name="part_of" negation="false" src="d0_s13" to="o9888" />
    </statement>
    <statement id="d0_s14">
      <text>awns 11-23 mm;</text>
      <biological_entity id="o9890" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="11" from_unit="mm" name="some_measurement" src="d0_s14" to="23" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1, 1.2-2 mm, yellow.</text>
      <biological_entity id="o9891" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="1" value_original="1" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s15" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Pedicellate spikelets vestigial or absent.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 20.</text>
      <biological_entity id="o9892" name="spikelet" name_original="spikelets" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pedicellate" value_original="pedicellate" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="vestigial" value_original="vestigial" />
        <character is_modifier="false" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9893" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Andropogon tracyi grows on sandhills, sandy pinelands, and scrublands of the southeastern United States. It resembles A. longiberbis, but usually differs in having sparsely pubescent blades and a more slender appearance.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga.;N.C.;Ala.;Miss.;S.C.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>