<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">24</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Raf." date="unknown" rank="genus">DISTICHLIS</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus distichlis</taxon_hierarchy>
  </taxon_identification>
  <number>17.04</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>usually unisexual, occasionally bisexual;</text>
    </statement>
    <statement id="d0_s2">
      <text>strongly rhizomatous and/or stoloniferous.</text>
      <biological_entity id="o23137" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s1" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="occasionally" name="reproduction" src="d0_s1" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s2" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms to 60 cm, usually erect, glabrous.</text>
      <biological_entity id="o23138" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="60" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves conspicuously distichous;</text>
      <biological_entity id="o23139" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="arrangement" src="d0_s4" value="distichous" value_original="distichous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lower leaves reduced to scalelike sheaths;</text>
      <biological_entity constraint="lower" id="o23140" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o23141" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="scale-like" value_original="scalelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>upper leaf-sheaths strongly overlapping;</text>
      <biological_entity constraint="upper" id="o23142" name="sheath" name_original="leaf-sheaths" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s6" value="overlapping" value_original="overlapping" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules shorter than 1 mm, membranous, serrate;</text>
      <biological_entity id="o23143" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character modifier="shorter than" name="some_measurement" src="d0_s7" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>upper blades stiff, glabrous, ascending to spreading, usually equaling or exceeding the pistillate panicles.</text>
      <biological_entity constraint="upper" id="o23144" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s8" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s8" to="spreading" />
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s8" value="exceeding the pistillate panicles" value_original="exceeding the pistillate panicles" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences terminal, contracted panicles or racemes, sometimes exceeding the upper leaves.</text>
      <biological_entity id="o23145" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o23146" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="true" name="condition_or_size" src="d0_s9" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity id="o23147" name="raceme" name_original="racemes" src="d0_s9" type="structure">
        <character is_modifier="true" name="condition_or_size" src="d0_s9" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity constraint="upper" id="o23148" name="leaf" name_original="leaves" src="d0_s9" type="structure" />
      <relation from="o23146" id="r3924" modifier="sometimes" name="exceeding the" negation="false" src="d0_s9" to="o23148" />
      <relation from="o23147" id="r3925" modifier="sometimes" name="exceeding the" negation="false" src="d0_s9" to="o23148" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets laterally compressed, with 2-20 florets;</text>
      <biological_entity id="o23150" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="20" />
      </biological_entity>
      <relation from="o23149" id="r3926" name="with" negation="false" src="d0_s10" to="o23150" />
    </statement>
    <statement id="d0_s11">
      <text>disarticulation of the pistillate spikelets above the glumes and below the florets, staminate spikelets not disarticulating.</text>
      <biological_entity id="o23149" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" constraint="spikelet" constraint_original="spikelet; spikelet">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s10" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o23151" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o23152" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
      <biological_entity id="o23153" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
      <relation from="o23149" id="r3927" name="part_of" negation="false" src="d0_s11" to="o23151" />
      <relation from="o23149" id="r3928" name="above the glumes and below" negation="false" src="d0_s11" to="o23152" />
      <relation from="o23149" id="r3929" name="above the glumes and below" negation="false" src="d0_s11" to="o23153" />
    </statement>
    <statement id="d0_s12">
      <text>Glumes 3-7-veined;</text>
      <biological_entity id="o23154" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-7-veined" value_original="3-7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas coriaceous, staminate lemmas thinner than the pistillate lemmas, 9-11-veined, unawned;</text>
      <biological_entity id="o23155" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="texture" src="d0_s13" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o23156" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
        <character constraint="than the pistillate lemmas" constraintid="o23157" is_modifier="false" name="width" src="d0_s13" value="thinner" value_original="thinner" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="9-11-veined" value_original="9-11-veined" />
      </biological_entity>
      <biological_entity id="o23157" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas 2-keeled, keels narrowly to broadly winged, serrate to toothed, sometimes with excurrent veins;</text>
      <biological_entity id="o23158" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="2-keeled" value_original="2-keeled" />
      </biological_entity>
      <biological_entity id="o23159" name="keel" name_original="keels" src="d0_s14" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="architecture" src="d0_s14" value="winged" value_original="winged" />
        <character char_type="range_value" from="serrate" name="shape" src="d0_s14" to="toothed" />
      </biological_entity>
      <biological_entity id="o23160" name="vein" name_original="veins" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <relation from="o23159" id="r3930" modifier="sometimes" name="with" negation="false" src="d0_s14" to="o23160" />
    </statement>
    <statement id="d0_s15">
      <text>anthers 3.</text>
      <biological_entity id="o23161" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses glabrous, free from the palea at maturity, brown, x = 10.</text>
      <biological_entity id="o23162" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character constraint="from palea" constraintid="o23163" is_modifier="false" name="fusion" src="d0_s16" value="free" value_original="free" />
        <character is_modifier="false" modifier="at maturity" name="coloration" notes="" src="d0_s16" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o23163" name="paleum" name_original="palea" src="d0_s16" type="structure" />
      <biological_entity constraint="x" id="o23164" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Distichlis, a genus of about five species, grows in saline soils of the coasts and interior deserts of the Western Hemisphere and Australia. All the species grow in South America, but only one, Distichlis spicata, is found in North America.</discussion>
  <references>
    <reference>Beetle, A.A. 1943. The North American variations of Disticblis spicata. Bull. Torrey Bot. Club 70:638-650.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Del.;Wis.;Ariz.;N.Mex.;Pacific Islands (Hawaii);Fla.;Wyo.;N.H.;Tex.;La.;Alta.;B.C.;Man.;N.B.;N.S.;N.W.T.;Ont.;P.E.I.;Sask.;N.C.;S.C.;Pa.;Ala.;Miss.;Nev.;Va.;Colo.;Calif.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Ill.;Ga.;Iowa;Idaho;Maine;Md.;Mass.;Ohio;Utah;Mo.;Minn.;R.I.;Mont.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>