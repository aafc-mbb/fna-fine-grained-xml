<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">42</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">TRIPLASIS</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="species">americana</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus triplasis;species americana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Perennial sandgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o3220" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-80 cm, usually erect;</text>
      <biological_entity id="o3221" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes and internodes appressed-pubescent.</text>
      <biological_entity id="o3222" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity id="o3223" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous or pilose, margins ciliate;</text>
      <biological_entity id="o3224" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o3225" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules to 2 mm, membranous, ciliate;</text>
      <biological_entity id="o3226" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades to 20 cm long, usually less than 2 mm wide, filiform, scabrous adaxially.</text>
      <biological_entity id="o3227" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" modifier="usually" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="adaxially" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 1-5 cm long, 1-3 cm wide, occasionally reduced to a raceme.</text>
      <biological_entity id="o3228" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="3" to_unit="cm" />
        <character constraint="to raceme" constraintid="o3229" is_modifier="false" modifier="occasionally" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o3229" name="raceme" name_original="raceme" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 9-12 mm, with 2-5 florets.</text>
      <biological_entity id="o3230" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3231" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <relation from="o3230" id="r525" name="with" negation="false" src="d0_s8" to="o3231" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes subequal, 3.4-4.5 mm, acuminate;</text>
      <biological_entity id="o3232" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s9" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lemmas 4-8 mm, lobes 4.5-8 mm, tapering to the acute apices;</text>
      <biological_entity id="o3233" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3234" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
        <character constraint="to apices" constraintid="o3235" is_modifier="false" name="shape" src="d0_s10" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o3235" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>awns 8-11 mm, divergent;</text>
      <biological_entity id="o3236" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="11" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="divergent" value_original="divergent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>paleas 2-3 mm, keels ciliate;</text>
      <biological_entity id="o3237" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3238" name="keel" name_original="keels" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 1.5-2 mm, yellow.</text>
      <biological_entity id="o3239" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s13" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 1.5-2.5 mm, ovoid, tan.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = unknown.</text>
      <biological_entity id="o3240" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s14" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="tan" value_original="tan" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3241" name="chromosome" name_original="" src="d0_s15" type="structure" />
    </statement>
  </description>
  <discussion>Triplasis americana is endemic to the southeastern United States. It grows on sandy soils in prairies and woods, being less common in maritime dunes than Triplasis purpurea.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga.;La.;Ala.;N.C.;S.C.;Miss.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>