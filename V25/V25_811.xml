<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">218</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Sw." date="unknown" rank="genus">CHLORIS</taxon_name>
    <taxon_name authority="(L.) Sw." date="unknown" rank="species">radiata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus chloris;species radiata</taxon_hierarchy>
  </taxon_identification>
  <number>18</number>
  <other_name type="common_name">Radiate windmill-grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>with dense fibrous-root growth, not stoloniferous.</text>
      <biological_entity id="o14345" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="fibrous-root" id="o14346" name="growth" name_original="growth" src="d0_s1" type="structure">
        <character is_modifier="true" name="density" src="d0_s1" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
      </biological_entity>
      <relation from="o14345" id="r2368" name="with" negation="false" src="d0_s1" to="o14346" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-60 cm, erect or decumbent, occasionally rooting at the lower nodes.</text>
      <biological_entity id="o14347" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o14348" is_modifier="false" modifier="occasionally" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o14348" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths usually glabrous, occasionally pilose;</text>
      <biological_entity id="o14349" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, shortly ciliate;</text>
      <biological_entity id="o14350" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="shortly" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 10-30 cm long, to 10 mm wide, sometimes with long basal hairs, usually pilose elsewhere, occasionally glabrous or scabrous.</text>
      <biological_entity id="o14351" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="usually; elsewhere" name="pubescence" notes="" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="basal" id="o14352" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
      </biological_entity>
      <relation from="o14351" id="r2369" modifier="sometimes" name="with" negation="false" src="d0_s5" to="o14352" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles with 5-15, evidently distinct branches in 1-2 (3) whorls;</text>
      <biological_entity id="o14353" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o14354" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s6" to="15" />
        <character is_modifier="true" modifier="evidently" name="fusion" src="d0_s6" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o14355" name="whorl" name_original="whorls" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="3" value_original="3" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
      <relation from="o14353" id="r2370" name="with" negation="false" src="d0_s6" to="o14354" />
      <relation from="o14354" id="r2371" name="in" negation="false" src="d0_s6" to="o14355" />
    </statement>
    <statement id="d0_s7">
      <text>branches 4.5-8 cm, spikelet-bearing to the base, with 11-15 spikelets per cm distally.</text>
      <biological_entity id="o14356" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="4.5" from_unit="cm" name="some_measurement" src="d0_s7" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14357" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o14358" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="11" is_modifier="true" name="quantity" src="d0_s7" to="15" />
      </biological_entity>
      <biological_entity id="o14359" name="cm" name_original="cm" src="d0_s7" type="structure" />
      <relation from="o14356" id="r2372" name="to" negation="false" src="d0_s7" to="o14357" />
      <relation from="o14356" id="r2373" name="with" negation="false" src="d0_s7" to="o14358" />
      <relation from="o14358" id="r2374" name="per" negation="false" src="d0_s7" to="o14359" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets with 1 bisexual and 1 sterile floret.</text>
      <biological_entity id="o14360" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o14361" name="floret" name_original="floret" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o14360" id="r2375" name="with" negation="false" src="d0_s8" to="o14361" />
    </statement>
    <statement id="d0_s9">
      <text>Lower glumes 0.7-1.6 mm;</text>
      <biological_entity constraint="lower" id="o14362" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 2-2.7 mm;</text>
      <biological_entity constraint="upper" id="o14363" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s10" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lowest lemmas 2.8-3.3 mm long, 0.4-0.6 mm wide, lanceolate to elliptic, sides not conspicuously grooved, mostly glabrous, margins shortly ciliate distally, hairs less than 1 mm, apices awned, awns 6-13 mm;</text>
      <biological_entity constraint="lowest" id="o14364" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="length" src="d0_s11" to="3.3" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s11" to="0.6" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s11" to="elliptic" />
      </biological_entity>
      <biological_entity id="o14365" name="side" name_original="sides" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not conspicuously" name="architecture" src="d0_s11" value="grooved" value_original="grooved" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o14366" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="shortly; distally" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o14367" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14368" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>second florets 0.4-0.7 long, about 0.1 mm wide, borne on an equally long or longer rachilla segment, not or inconspicuously bilobed, awned, awns 3-5 mm.</text>
      <biological_entity id="o14369" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14370" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.4" name="length" src="d0_s12" to="0.7" />
        <character name="width" src="d0_s12" unit="mm" value="0.1" value_original="0.1" />
        <character is_modifier="false" modifier="not; inconspicuously" name="architecture_or_shape" notes="" src="d0_s12" value="bilobed" value_original="bilobed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity constraint="rachilla" id="o14371" name="segment" name_original="segment" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="equally" name="length_or_size" src="d0_s12" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o14372" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o14371" id="r2376" name="borne on" negation="false" src="d0_s12" to="o14371" />
    </statement>
    <statement id="d0_s13">
      <text>Caryopses 1.4-1.5 mm long, 0.3-0.4 mm wide, ellipsoidal.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 40.</text>
      <biological_entity id="o14373" name="caryopsis" name_original="caryopses" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s13" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s13" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoidal" value_original="ellipsoidal" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14374" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Chloris radiata is a weedy species of the eastern Caribbean, Central America, and northern South America. It may be native to Florida, but the record from Linton, Oregon, was from a ballast dump. The species is no longer found in Oregon.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Virgin Islands;Pacific Islands (Hawaii);Fla.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>