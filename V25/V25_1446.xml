<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">572</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="Raddi" date="unknown" rank="species">acuminatum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species acuminatum</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Brook paspalum</other_name>
  <other_name type="common_name">Canoegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o23739" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-100 cm, strongly decumbent, upright portion usually not standing more than 20 cm tall, much branched;</text>
      <biological_entity id="o23740" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character is_modifier="false" modifier="strongly" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
      </biological_entity>
      <biological_entity id="o23741" name="portion" name_original="portion" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="upright" value_original="upright" />
        <character is_modifier="false" name="height" src="d0_s2" value="tall" value_original="tall" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o23742" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o23743" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-2.4 mm;</text>
      <biological_entity id="o23744" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades to 7 cm long, 3-6.5 mm wide, flat.</text>
      <biological_entity id="o23745" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="6.5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal, with 2-5 racemosely arranged branches;</text>
      <biological_entity id="o23746" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o23747" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o23746" id="r4030" name="with" negation="false" src="d0_s7" to="o23747" />
    </statement>
    <statement id="d0_s8">
      <text>branches 2-6 cm, diverging, persistent;</text>
      <biological_entity id="o23748" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="6" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branch axes 2-3.3 mm wide, broadly winged, glabrous, margins scabrous, terminating in a spikelet.</text>
      <biological_entity constraint="branch" id="o23749" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s9" to="3.3" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23750" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o23751" name="spikelet" name_original="spikelet" src="d0_s9" type="structure" />
      <relation from="o23750" id="r4031" name="terminating in" negation="false" src="d0_s9" to="o23751" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3.2-4 mm long, 1.6-1.7 mm wide, solitary, appressed to the branch axes, elliptic, abruptly pointed, stramineous.</text>
      <biological_entity id="o23752" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.2" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="width" src="d0_s10" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
        <character constraint="to branch axes" constraintid="o23753" is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s10" value="pointed" value_original="pointed" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity constraint="branch" id="o23753" name="axis" name_original="axes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes absent;</text>
      <biological_entity constraint="lower" id="o23754" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes and lower lemmas glabrous, 5-veined;</text>
      <biological_entity constraint="upper" id="o23755" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o23756" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper florets stramineous, lemmas with a few minute hairs at the apices.</text>
      <biological_entity constraint="upper" id="o23757" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="stramineous" value_original="stramineous" />
      </biological_entity>
      <biological_entity id="o23758" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity id="o23759" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="few" value_original="few" />
        <character is_modifier="true" name="size" src="d0_s13" value="minute" value_original="minute" />
      </biological_entity>
      <biological_entity id="o23760" name="apex" name_original="apices" src="d0_s13" type="structure" />
      <relation from="o23758" id="r4032" name="with" negation="false" src="d0_s13" to="o23759" />
      <relation from="o23759" id="r4033" name="at" negation="false" src="d0_s13" to="o23760" />
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 2-3 mm, white.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 40.</text>
      <biological_entity id="o23761" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23762" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum acuminatum grows at the edges of lakes, ponds, rice fields, and wet roadside ditches. It is native to the Americas, with a range that extends from the southern United States to Argentina.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ga.;Tex.;La.;Ala.;Miss.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>