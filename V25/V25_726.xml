<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">162</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="(DC.) Trin." date="unknown" rank="species">microsperma</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species microsperma</taxon_hierarchy>
  </taxon_identification>
  <number>15</number>
  <other_name type="common_name">Littleseed muhly</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, sometimes appearing as short-lived perennials;</text>
      <biological_entity id="o11200" name="perennial" name_original="perennials" src="d0_s0" type="structure">
        <character is_modifier="true" name="duration" src="d0_s0" value="short-lived" value_original="short-lived" />
      </biological_entity>
      <relation from="o11199" id="r1839" modifier="sometimes" name="appearing as" negation="false" src="d0_s0" to="o11200" />
    </statement>
    <statement id="d0_s1">
      <text>tufted.</text>
      <biological_entity id="o11199" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-80 cm, often geniculate at the base, much branched near the base;</text>
      <biological_entity id="o11201" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character constraint="at base" constraintid="o11202" is_modifier="false" modifier="often" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character constraint="near base" constraintid="o11203" is_modifier="false" modifier="much" name="architecture" notes="" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o11202" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o11203" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>internodes mostly scabridulous or smooth, always scabridulous below the nodes.</text>
      <biological_entity id="o11204" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character constraint="below nodes" constraintid="o11205" is_modifier="false" modifier="always" name="relief" src="d0_s3" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o11205" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths often shorter than the internodes, glabrous, smooth or scabridulous;</text>
      <biological_entity id="o11206" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o11207" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="often shorter" value_original="often shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="relief" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o11207" name="internode" name_original="internodes" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-2 mm, membranous to hyaline, truncate to obtuse;</text>
      <biological_entity id="o11208" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 3-8.5 (10) cm long, 1-2.5 mm wide, flat or loosely involute, scabrous abaxially, strigulose adaxially.</text>
      <biological_entity id="o11209" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="10" value_original="10" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="8.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="loosely" name="shape" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="abaxially" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="strigulose" value_original="strigulose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 6.5-13.5 cm long, 1-6.5 cm wide, not dense, often purplish;</text>
      <biological_entity id="o11210" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="6.5" from_unit="cm" name="length" src="d0_s7" to="13.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="6.5" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s7" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches 1.6-4 cm, ascending or diverging up to 80° from the rachises, spikelet-bearing to the base;</text>
      <biological_entity id="o11211" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.6" from_unit="cm" name="some_measurement" src="d0_s8" to="4" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character constraint="from rachises" constraintid="o11212" is_modifier="false" modifier="0-80°" name="orientation" src="d0_s8" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o11212" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
      <biological_entity id="o11213" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o11211" id="r1840" name="to" negation="false" src="d0_s8" to="o11213" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels 2-6 mm, appressed to divaricate, antrorsely scabrous;</text>
    </statement>
    <statement id="d0_s10">
      <text>disarticulation above the glumes.</text>
      <biological_entity id="o11214" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="divaricate" value_original="divaricate" />
        <character is_modifier="false" modifier="antrorsely" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o11215" name="glume" name_original="glumes" src="d0_s10" type="structure" />
      <relation from="o11214" id="r1841" name="above" negation="false" src="d0_s10" to="o11215" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.5-5.5 mm, borne singly.</text>
      <biological_entity id="o11216" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes 0.4-1.3 mm, exceeded by the florets, 1-veined, obtuse, often minutely erose;</text>
      <biological_entity id="o11217" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="often minutely" name="architecture_or_relief" src="d0_s12" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o11218" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <relation from="o11217" id="r1842" name="exceeded by the" negation="false" src="d0_s12" to="o11218" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes 0.4-1 mm;</text>
      <biological_entity constraint="lower" id="o11219" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s13" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 0.6-1.3 mm;</text>
      <biological_entity constraint="upper" id="o11220" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 2.5-3.8 (5.3) mm, narrowly lanceolate, mostly smooth, scabridulous distally, hairy on the calluses, lower 1/2 of the margins, and midveins, hairs 0.2-0.5 mm, apices acuminate, awned, awns 10-30 mm, straight to flexuous;</text>
      <biological_entity id="o11221" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character name="atypical_some_measurement" src="d0_s15" unit="mm" value="5.3" value_original="5.3" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3.8" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s15" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_pubescence_or_relief" src="d0_s15" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s15" value="scabridulous" value_original="scabridulous" />
        <character constraint="on calluses" constraintid="o11222" is_modifier="false" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="position" notes="" src="d0_s15" value="lower" value_original="lower" />
        <character constraint="of margins" constraintid="o11223" name="quantity" src="d0_s15" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o11222" name="callus" name_original="calluses" src="d0_s15" type="structure" />
      <biological_entity id="o11223" name="margin" name_original="margins" src="d0_s15" type="structure" />
      <biological_entity id="o11224" name="midvein" name_original="midveins" src="d0_s15" type="structure" />
      <biological_entity id="o11225" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s15" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11226" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o11227" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s15" to="30" to_unit="mm" />
        <character char_type="range_value" from="straight" name="course" src="d0_s15" to="flexuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas 2.2-4.8 mm, narrowly lanceolate, acuminate;</text>
      <biological_entity id="o11228" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s16" to="4.8" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s16" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 0.3-1.2 mm, purplish.</text>
      <biological_entity id="o11229" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s17" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 1.7-2.5 mm, fusiform, reddish-brown.</text>
      <biological_entity id="o11230" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s18" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Cleistogamous panicles with 1-3 spikelets present in the axils of the lower leaves.</text>
      <biological_entity id="o11232" name="spikelet" name_original="spikelets" src="d0_s19" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s19" to="3" />
      </biological_entity>
      <biological_entity id="o11233" name="axil" name_original="axils" src="d0_s19" type="structure" />
      <biological_entity constraint="lower" id="o11234" name="leaf" name_original="leaves" src="d0_s19" type="structure" />
      <relation from="o11231" id="r1843" name="with" negation="false" src="d0_s19" to="o11232" />
      <relation from="o11232" id="r1844" name="present in the" negation="false" src="d0_s19" to="o11233" />
      <relation from="o11232" id="r1845" name="present in the" negation="false" src="d0_s19" to="o11234" />
    </statement>
    <statement id="d0_s20">
      <text>2n = 20, 40, 60.</text>
      <biological_entity id="o11231" name="panicle" name_original="panicles" src="d0_s19" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s19" value="cleistogamous" value_original="cleistogamous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11235" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="20" value_original="20" />
        <character name="quantity" src="d0_s20" value="40" value_original="40" />
        <character name="quantity" src="d0_s20" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia microsperma grows on sandy slopes, drainages, cliffs, rock outcrops, and disturbed road¬sides, at elevations of 0-2400 m. It is usually found in creosote scrub, thorn-scrub forest, sarcocaulescent desert, and oak-pinyon woodland associations. Its range extends from the southwestern United States through Central America to Peru and Venezuela. Morphological variation among and within its populations is marked.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Utah;Calif.;Ariz.;Pacific Islands (Hawaii);Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>