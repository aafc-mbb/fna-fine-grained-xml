<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">670</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="genus">SCHIZACHYRIUM</taxon_name>
    <taxon_name authority="(Michx.) Nash" date="unknown" rank="species">scoparium</taxon_name>
    <taxon_name authority="(Nash)" date="unknown" rank="variety">stoloniferum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus schizachyrium;species scoparium;variety stoloniferum</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Creeping bluestem</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not cespitose, with long, scaly rhizomes.</text>
      <biological_entity id="o18099" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18100" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s0" value="long" value_original="long" />
        <character is_modifier="true" name="architecture_or_pubescence" src="d0_s0" value="scaly" value_original="scaly" />
      </biological_entity>
      <relation from="o18099" id="r3048" name="with" negation="false" src="d0_s0" to="o18100" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 58-210 cm.</text>
      <biological_entity id="o18101" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="58" from_unit="cm" name="some_measurement" src="d0_s1" to="210" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths usually pubescent near the collars;</text>
      <biological_entity id="o18102" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character constraint="near collars" constraintid="o18103" is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o18103" name="collar" name_original="collars" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blades 10-39 cm long, 3.5-9 mm wide, pubescent near the collars.</text>
      <biological_entity id="o18105" name="collar" name_original="collars" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Rames 2-6.5 cm, with 6-14 spikelets, usually partially to fully exserted;</text>
      <biological_entity id="o18104" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s3" to="39" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s3" to="9" to_unit="mm" />
        <character constraint="near collars" constraintid="o18105" is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="6.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o18106" name="spikelet" name_original="spikelets" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s4" to="14" />
        <character is_modifier="false" modifier="usually partially; partially to fully" name="position" src="d0_s4" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>internodes pubescent, hairs to 4.5 mm.</text>
      <biological_entity id="o18107" name="internode" name_original="internodes" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o18108" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Sessile spikelets 5-10 mm;</text>
      <biological_entity id="o18109" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>calluses with hairs to 2.5 mm;</text>
      <biological_entity id="o18110" name="callus" name_original="calluses" src="d0_s7" type="structure" />
      <biological_entity id="o18111" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s7" to="2.5" to_unit="mm" />
      </biological_entity>
      <relation from="o18110" id="r3049" name="with" negation="false" src="d0_s7" to="o18111" />
    </statement>
    <statement id="d0_s8">
      <text>awns 6-14 mm.</text>
      <biological_entity id="o18112" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels 3.5-5 mm, curving out at maturity.</text>
      <biological_entity id="o18113" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character constraint="at out maturity" is_modifier="false" name="course" src="d0_s9" value="curving" value_original="curving" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicellate spikelets 0.75-4 mm, sterile, awned, awns 1-3 mm.</text>
      <biological_entity id="o18114" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="0.75" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o18115" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Schizachyrium scoparium var. stoloniferum grows in sandy soils of woodland openings and roadsides from southern Alabama and Georgia south to the Everglades. Northern populations consist of widely spaced, weak culms growing in rather bare sand; southern populations consist of dense, vigorous stands with taller, more robust culms growing primarily along roadsides, possibly spread by grading equipment. Some clones, particularly in the south, are largely sterile.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Miss.;Fla.;Ala.;Ga.;N.C.;S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>