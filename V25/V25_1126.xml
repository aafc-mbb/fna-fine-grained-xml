<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">404</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Nash" date="unknown" rank="genus">SACCIOLEPIS</taxon_name>
    <taxon_name authority="(L.) Chase" date="unknown" rank="species">indica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus sacciolepis;species indica</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Chase's glenwoodgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o13472" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-100 cm, decumbent, spreading, trailing, often rooting at the lower nodes;</text>
      <biological_entity id="o13473" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="trailing" value_original="trailing" />
        <character constraint="at lower nodes" constraintid="o13474" is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o13474" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o13475" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths and collars glabrous;</text>
      <biological_entity id="o13476" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o13477" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.1-0.7 mm long, membranous, truncate;</text>
      <biological_entity id="o13478" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="length" src="d0_s5" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 1-14.3 cm long, 1.5-5.5 mm wide, glabrous, not cordate at the base.</text>
      <biological_entity id="o13479" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="14.3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s6" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character constraint="at base" constraintid="o13480" is_modifier="false" modifier="not" name="shape" src="d0_s6" value="cordate" value_original="cordate" />
      </biological_entity>
      <biological_entity id="o13480" name="base" name_original="base" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles 0.5-9 (13) cm long, 4-7 mm wide, contracted;</text>
      <biological_entity id="o13481" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="13" value_original="13" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s7" to="9" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s7" to="7" to_unit="mm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches fused to the rachises for at least 3/4 of their length;</text>
      <biological_entity constraint="primary" id="o13482" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character constraint="to rachises" constraintid="o13483" is_modifier="false" name="fusion" src="d0_s8" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o13483" name="rachis" name_original="rachises" src="d0_s8" type="structure">
        <character modifier="at least" name="length" src="d0_s8" value="3/4" value_original="3/4" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower branches 0.1-0.5 cm;</text>
      <biological_entity constraint="lower" id="o13484" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s9" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels 0.3-1.8 mm.</text>
      <biological_entity id="o13485" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.1-3.3 mm, with or without papillose-based hairs on the upper glumes and lower lemmas, green to dark purple.</text>
      <biological_entity id="o13486" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s11" to="3.3" to_unit="mm" />
        <character char_type="range_value" from="green" name="coloration" notes="" src="d0_s11" to="dark purple" />
      </biological_entity>
      <biological_entity id="o13487" name="hair" name_original="hairs" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o13486" id="r2211" name="with or without" negation="false" src="d0_s11" to="o13487" />
    </statement>
    <statement id="d0_s12">
      <text>Lower glumes 1.1-1.9 mm, glabrous, 3-5 (7) -veined, margins hyaline;</text>
      <biological_entity constraint="lower" id="o13488" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-5(7)-veined" value_original="3-5(7)-veined" />
      </biological_entity>
      <biological_entity id="o13489" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 2-3.3 mm, slightly saccate, glabrous adaxially, 9-veined;</text>
      <biological_entity constraint="upper" id="o13490" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3.3" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_shape" src="d0_s13" value="saccate" value_original="saccate" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="9-veined" value_original="9-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower florets sterile (rarely staminate);</text>
      <biological_entity constraint="lower" id="o13491" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower lemmas 1.9-3.1 mm, 7-9-veined, veins equidistant;</text>
      <biological_entity constraint="lower" id="o13492" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s15" to="3.1" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="7-9-veined" value_original="7-9-veined" />
      </biological_entity>
      <biological_entity id="o13493" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="equidistant" value_original="equidistant" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower paleas 0.5-1 mm long, 0.1-0.2 mm wide, 1/2 or less as long as the lower lemmas, narrow, membranous, white, not veined;</text>
      <biological_entity constraint="lower" id="o13494" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s16" to="1" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s16" to="0.2" to_unit="mm" />
        <character name="quantity" src="d0_s16" value="1/2" value_original="1/2" />
        <character name="quantity" src="d0_s16" value="less" value_original="less" />
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s16" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="white" value_original="white" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s16" value="veined" value_original="veined" />
      </biological_entity>
      <biological_entity constraint="lower" id="o13495" name="lemma" name_original="lemmas" src="d0_s16" type="structure" />
      <relation from="o13494" id="r2212" name="as long as" negation="false" src="d0_s16" to="o13495" />
    </statement>
    <statement id="d0_s17">
      <text>upper lemmas 1.3-1.6 mm, subcoriaceous, glabrous, shiny, white, with 3-5 obscure veins, acute;</text>
      <biological_entity constraint="upper" id="o13496" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s17" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s17" value="subcoriaceous" value_original="subcoriaceous" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="white" value_original="white" />
        <character is_modifier="false" name="shape" notes="" src="d0_s17" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity id="o13497" name="vein" name_original="veins" src="d0_s17" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s17" to="5" />
        <character is_modifier="true" name="prominence" src="d0_s17" value="obscure" value_original="obscure" />
      </biological_entity>
      <relation from="o13496" id="r2213" name="with" negation="false" src="d0_s17" to="o13497" />
    </statement>
    <statement id="d0_s18">
      <text>anthers 3, 0.5-0.8 mm, dark reddish-brown to reddish-purple;</text>
      <biological_entity id="o13498" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s18" to="0.8" to_unit="mm" />
        <character char_type="range_value" from="dark reddish-brown" name="coloration" src="d0_s18" to="reddish-purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>styles purple.</text>
      <biological_entity id="o13499" name="style" name_original="styles" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration_or_density" src="d0_s19" value="purple" value_original="purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Caryopses 1-1.3 mm long, 0.5-0.7 mm wide, glabrous.</text>
    </statement>
    <statement id="d0_s21">
      <text>2n = 18, 36.</text>
      <biological_entity id="o13500" name="caryopsis" name_original="caryopses" src="d0_s20" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s20" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s20" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13501" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="18" value_original="18" />
        <character name="quantity" src="d0_s21" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Sacciolepis indica is native to the Eastern Hemisphere tropics. It is now established in the coastal states of the southeastern United States, where it grows in and along streams, ponds, lakes, ditches, and other moist places. It flowers from late summer to fall.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Ga.;Tex.;La.;N.C.;Pacific Islands (Hawaii);Miss.;S.C.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>