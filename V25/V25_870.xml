<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">254</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Lag." date="unknown" rank="genus">BOUTELOUA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Bouteloua</taxon_name>
    <taxon_name authority="(Michx.) Torr." date="unknown" rank="species">curtipendula</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">curtipendula</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus bouteloua;subgenus bouteloua;species curtipendula;variety curtipendula</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants not cespitose, with long rhizomes.</text>
      <biological_entity id="o15914" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="not" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o15915" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s0" value="long" value_original="long" />
      </biological_entity>
      <relation from="o15914" id="r2668" name="with" negation="false" src="d0_s0" to="o15915" />
    </statement>
    <statement id="d0_s1">
      <text>Culms solitary or in small clumps.</text>
      <biological_entity id="o15916" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s1" value="in small clumps" value_original="in small clumps" />
      </biological_entity>
      <biological_entity id="o15917" name="clump" name_original="clumps" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="small" value_original="small" />
      </biological_entity>
      <relation from="o15916" id="r2669" name="in" negation="false" src="d0_s1" to="o15917" />
    </statement>
    <statement id="d0_s2">
      <text>Blades 3-7 mm, flat.</text>
      <biological_entity id="o15918" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s2" to="7" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Panicles with 40-70 branches, averaging 3-7 spikelets per branch.</text>
      <biological_entity id="o15919" name="panicle" name_original="panicles" src="d0_s3" type="structure" />
      <biological_entity id="o15920" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="40" is_modifier="true" name="quantity" src="d0_s3" to="70" />
      </biological_entity>
      <biological_entity id="o15921" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s3" to="7" />
      </biological_entity>
      <biological_entity id="o15922" name="branch" name_original="branch" src="d0_s3" type="structure" />
      <relation from="o15919" id="r2670" name="with" negation="false" src="d0_s3" to="o15920" />
      <relation from="o15919" id="r2671" name="averaging" negation="false" src="d0_s3" to="o15921" />
      <relation from="o15919" id="r2672" name="per" negation="false" src="d0_s3" to="o15922" />
    </statement>
    <statement id="d0_s4">
      <text>Glumes and lemmas typically purple or purple-tinged;</text>
      <biological_entity id="o15923" name="glume" name_original="glumes" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="typically" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
      <biological_entity id="o15924" name="lemma" name_original="lemmas" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="typically" name="coloration" src="d0_s4" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>anthers red or red-orange, infrequently yellow, orange, or purple.</text>
    </statement>
    <statement id="d0_s6">
      <text>2n = 40, 41-66.</text>
      <biological_entity id="o15925" name="anther" name_original="anthers" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" modifier="infrequently" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" modifier="infrequently" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" modifier="infrequently" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="red-orange" value_original="red-orange" />
        <character is_modifier="false" modifier="infrequently" name="coloration" src="d0_s5" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15926" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="40" value_original="40" />
        <character char_type="range_value" from="41" name="quantity" src="d0_s6" to="66" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Bouteloua curtipendula var. curtipendula is the common variety of B. curtipendula in most of the Flora region. It grows on rich, loamy, well-drained prairie soils. Its elevational range extends from below 100 m to 2500 m.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>W.Va.;Alta.;B.C.;Man.;Ont.;Sask.;Ala.;Ariz.;Ga.;Ill.;Ind.;Ky.;Md.;Mich.;Minn.;Miss.;N.Dak.;Nebr.;N.J.;N.Mex.;N.Y.;Ohio;Pa.;S.Dak.;Tenn.;Utah;Wis.;Wyo.;Conn.;Idaho;Maine;S.C.;Fla.;Tex.;La.;Va.;Colo.;Ark.;D.C.;Pacific Islands (Hawaii);Oreg.;Wash.;Iowa;Kans.;Okla.;Mo.;Mont.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>