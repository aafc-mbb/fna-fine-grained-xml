<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">244</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Linda Bea Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">SPARTINA</taxon_name>
    <taxon_name authority="Loisel." date="unknown" rank="species">alterniflora</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus spartina;species alterniflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spartina</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">alterniflora</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">pilosa</taxon_name>
    <taxon_hierarchy>genus spartina;species alterniflora;variety pilosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Spartina</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">alterniflora</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">glabra</taxon_name>
    <taxon_hierarchy>genus spartina;species alterniflora;variety glabra</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Smooth cordgrass</other_name>
  <other_name type="common_name">Spartine alterniflore</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants rhizomatous;</text>
      <biological_entity id="o11833" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>rhizomes elongate, flaccid, white, scales inflated, not or only slightly imbricate.</text>
      <biological_entity id="o11834" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="texture" src="d0_s1" value="flaccid" value_original="flaccid" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o11835" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="inflated" value_original="inflated" />
        <character is_modifier="false" modifier="not; only slightly" name="arrangement" src="d0_s1" value="imbricate" value_original="imbricate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms to 250 cm tall, (0.3) 5-15 (20) mm thick, erect, solitary or in small clumps, succulent, glabrous, having an unpleasant, sulphurous odor when fresh.</text>
      <biological_entity id="o11836" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="height" src="d0_s2" to="250" to_unit="cm" />
        <character name="thickness" src="d0_s2" unit="mm" value="0.3" value_original="0.3" />
        <character char_type="range_value" from="5" from_unit="mm" name="thickness" src="d0_s2" to="15" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s2" value="in small clumps" value_original="in small clumps" />
        <character is_modifier="false" name="texture" notes="" src="d0_s2" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="odor" src="d0_s2" value="unpleasant" value_original="unpleasant" />
      </biological_entity>
      <biological_entity id="o11837" name="clump" name_original="clumps" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="small" value_original="small" />
      </biological_entity>
      <relation from="o11836" id="r1942" name="in" negation="false" src="d0_s2" to="o11837" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths mostly glabrous, throat glabrous or minutely pilose, lower sheaths often wrinkled;</text>
      <biological_entity id="o11838" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11839" name="throat" name_original="throat" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="lower" id="o11840" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="relief" src="d0_s3" value="wrinkled" value_original="wrinkled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 1-2 mm;</text>
      <biological_entity id="o11841" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades to 60 cm long, 3-25 mm wide, lower blades shorter than those above, usually flat basally, becoming involute distally, abaxial surfaces glabrous, adaxial surfaces glabrous or sparsely pilose, margins usually smooth, sometimes slightly scabrous, apices attenuate.</text>
      <biological_entity id="o11842" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s5" to="60" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o11843" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character constraint="than those above" constraintid="" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="usually; basally" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="becoming; distally" name="shape_or_vernation" src="d0_s5" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11844" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o11845" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o11846" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="sometimes slightly" name="pubescence_or_relief" src="d0_s5" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o11847" name="apex" name_original="apices" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 10-40 cm, with 3-25 branches, often partially enclosed in the uppermost sheath;</text>
      <biological_entity id="o11848" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s6" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o11849" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="25" />
      </biological_entity>
      <biological_entity constraint="uppermost" id="o11850" name="sheath" name_original="sheath" src="d0_s6" type="structure" />
      <relation from="o11848" id="r1943" name="with" negation="false" src="d0_s6" to="o11849" />
      <relation from="o11848" id="r1944" modifier="often partially" name="enclosed in the" negation="false" src="d0_s6" to="o11850" />
    </statement>
    <statement id="d0_s7">
      <text>branches 5-15 cm, loosely appressed, not twisted, more or less equally subremote to moderately imbricate throughout the panicle, axes often prolonged beyond the distal spikelets, with 10-30 spikelets.</text>
      <biological_entity id="o11851" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s7" to="15" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="twisted" value_original="twisted" />
      </biological_entity>
      <biological_entity id="o11852" name="panicle" name_original="panicle" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="moderately" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o11853" name="axis" name_original="axes" src="d0_s7" type="structure">
        <character constraint="beyond distal spikelets" constraintid="o11854" is_modifier="false" modifier="often" name="length" src="d0_s7" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity constraint="distal" id="o11854" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o11855" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s7" to="30" />
      </biological_entity>
      <relation from="o11851" id="r1945" modifier="more or less equally; equally" name="to" negation="false" src="d0_s7" to="o11852" />
      <relation from="o11853" id="r1946" name="with" negation="false" src="d0_s7" to="o11855" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 8-14 mm, straight, usually divergent, more or less equally imbricate on all the branches.</text>
      <biological_entity id="o11856" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s8" to="14" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character constraint="on branches" constraintid="o11857" is_modifier="false" modifier="more or less equally" name="arrangement" src="d0_s8" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o11857" name="branch" name_original="branches" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes straight, sides usually glabrous, sometimes pilose near the base or appressed-pubescent, hairs to 0.3 mm;</text>
      <biological_entity id="o11858" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o11859" name="side" name_original="sides" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character constraint="near " constraintid="o11861" is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o11860" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o11861" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s9" value="appressed-pubescent" value_original="appressed-pubescent" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lower glumes 4-10 mm, acute;</text>
      <biological_entity constraint="lower" id="o11862" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 8-14 mm, keels glabrous, lateral-veins not present, apices acuminate to obtuse, occasionally apiculate;</text>
      <biological_entity constraint="upper" id="o11863" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s11" to="14" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11864" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11865" name="lateral-vein" name_original="lateral-veins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11866" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character char_type="range_value" from="acuminate" name="shape" src="d0_s11" to="obtuse" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s11" value="apiculate" value_original="apiculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas glabrous or sparsely pilose, apices usually acuminate;</text>
      <biological_entity id="o11867" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o11868" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleas slightly exceeding the lemmas, thin, papery, apices obtuse or rounded;</text>
      <biological_entity id="o11869" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character is_modifier="false" name="width" src="d0_s13" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s13" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o11870" name="lemma" name_original="lemmas" src="d0_s13" type="structure" />
      <biological_entity id="o11871" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o11869" id="r1947" modifier="slightly" name="exceeding the" negation="false" src="d0_s13" to="o11870" />
    </statement>
    <statement id="d0_s14">
      <text>anthers 3-6 mm. 2n = 62.</text>
      <biological_entity id="o11872" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11873" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="62" value_original="62" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Spartina alterniflora is found on muddy banks, usually of the intertidal zone, in eastern North and South America, but it is not known from Central America. In addition, it has become established on the west coast of North America, and in England and southeastern France. It hybridizes with S. maritima in Europe, with S. pectinata in Massachusetts, and with S. foliosa in California.</discussion>
  <discussion>The rhizomes and scales of S. alterniflora have large air spaces, presumably an adaptation to the anaerobic soils of its usual habitat. Decaploid plants tend to be larger than octoploids, but they cannot be reliably distinguished without a chromosome count.</discussion>
  <discussion>Spartina alterniflora is considered a serious threat to coastal ecosystems in Washington and California. It out-competes many of the native species in these habitats and frequently invades mud flats and channels, converting them to marshlands. Pure S. alterniflora grows within the lower elevational marsh zones in its native range but, in San Francisco Bay, its hybrids with S. foliosa grow both below and above the range of that species.</discussion>
  <discussion>Spartina alterniflora is now a major weed problem in southeastern China (Normile 2004).</discussion>
  <references>
    <reference>Normile, D. 2004. Expanding trade with China creates ecological backlash. Science 306:968-969</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Del.;Fla.;N.H.;Tex.;La.;N.C.;S.C.;Ala.;Miss.;R.I.;Va.;Calif.;Ga.;N.B.;Nfld. and Labr. (Labr.);N.S.;P.E.I.;Que.;Maine;Md.;Mass.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Labr.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>