<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">535</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">ANTHEPHORA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus anthephora</taxon_hierarchy>
  </taxon_identification>
  <number>25.17</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted or cespitose.</text>
      <biological_entity id="o29804" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 15-50 cm, not woody.</text>
      <biological_entity id="o29805" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s2" to="50" to_unit="cm" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open;</text>
      <biological_entity id="o29806" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous.</text>
      <biological_entity id="o29807" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, spikelike panicles, each node supporting a highly reduced branch or fascicle of spikelets;</text>
      <biological_entity id="o29808" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o29809" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o29810" name="node" name_original="node" src="d0_s5" type="structure" />
      <biological_entity id="o29811" name="branch" name_original="branch" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="highly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o29813" name="spikelet" name_original="spikelets" src="d0_s5" type="structure" />
      <relation from="o29810" id="r5086" name="supporting a" negation="false" src="d0_s5" to="o29811" />
      <relation from="o29812" id="r5087" name="part_of" negation="false" src="d0_s5" to="o29813" />
    </statement>
    <statement id="d0_s6">
      <text>fascicles imbricate, with a short, thick, basal stipe sub¬tending 4 thick, rigid, coriaceous, many-veined, flat, narrowly elliptic to ovate bracts;</text>
      <biological_entity id="o29812" name="fascicle" name_original="fascicle" src="d0_s5" type="structure" constraint="spikelet" constraint_original="spikelet; spikelet">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="fascicles" value_original="fascicles" />
      </biological_entity>
      <biological_entity constraint="basal" id="o29814" name="stipe" name_original="stipe" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="short" value_original="short" />
        <character is_modifier="true" name="width" src="d0_s6" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o29815" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="4" value_original="4" />
        <character is_modifier="true" name="width" src="d0_s6" value="thick" value_original="thick" />
        <character is_modifier="true" name="texture" src="d0_s6" value="rigid" value_original="rigid" />
        <character is_modifier="true" name="texture" src="d0_s6" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="many-veined" value_original="many-veined" />
        <character is_modifier="true" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character char_type="range_value" from="narrowly elliptic" is_modifier="true" name="shape" src="d0_s6" to="ovate" />
      </biological_entity>
      <relation from="o29814" id="r5088" name="sub ¬ tending" negation="false" src="d0_s6" to="o29815" />
    </statement>
    <statement id="d0_s7">
      <text>bracts fused at the base, enclosing 2-11 spikelets, 1-2 of the spikelets sterile or reduced;</text>
      <biological_entity id="o29817" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o29818" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="11" />
      </biological_entity>
      <biological_entity id="o29819" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
      </biological_entity>
      <relation from="o29816" id="r5089" name="enclosing" negation="false" src="d0_s7" to="o29818" />
    </statement>
    <statement id="d0_s8">
      <text>disarticula¬tion beneath the fascicles.</text>
      <biological_entity id="o29816" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character constraint="at base" constraintid="o29817" is_modifier="false" name="fusion" src="d0_s7" value="fused" value_original="fused" />
        <character char_type="range_value" constraint="of spikelets" constraintid="o29819" from="1" name="quantity" src="d0_s7" to="2" />
        <character is_modifier="false" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets with 2 florets.</text>
      <biological_entity id="o29820" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o29821" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <relation from="o29820" id="r5090" name="with" negation="false" src="d0_s9" to="o29821" />
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes absent;</text>
      <biological_entity constraint="lower" id="o29822" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes acicular, 1-veined, awned;</text>
      <biological_entity constraint="upper" id="o29823" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acicular" value_original="acicular" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower florets sterile, sometimes reduced;</text>
      <biological_entity constraint="lower" id="o29824" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
        <character is_modifier="false" modifier="sometimes" name="size" src="d0_s12" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas 7-veined;</text>
      <biological_entity constraint="lower" id="o29825" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="7-veined" value_original="7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower paleas subequal to the lower lemmas;</text>
      <biological_entity constraint="lower" id="o29826" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character constraint="to lower lemmas" constraintid="o29827" is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="lower" id="o29827" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>upper florets bisexual, sterile, or reduced;</text>
      <biological_entity constraint="upper" id="o29828" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="size" src="d0_s15" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper lemmas faintly 3-veined.</text>
      <biological_entity constraint="upper" id="o29829" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" modifier="faintly" name="architecture" src="d0_s16" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses ellipsoidal, x = 9.</text>
      <biological_entity id="o29830" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoidal" value_original="ellipsoidal" />
      </biological_entity>
      <biological_entity constraint="x" id="o29831" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Anthephora is a genus of 12 species. Most of the species are native to Africa and Arabia. One species is native to tropical America and has become established in the Flora region.</discussion>
  <discussion>The fascicle bracts are often interpreted as the lower glumes of the spikelets, but the devel¬opmental studies needed to evaluate this interpretation have not been conducted.</discussion>
  <references>
    <reference>Reeder, J.R. 1960. The systematic position of the grass genus Anthephora. Trans. Amer. Microscop. Soc. 79:211-218.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Virgin Islands;Pacific Islands (Hawaii);Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>