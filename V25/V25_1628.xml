<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">678</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Andersson ex E. Fourn." date="unknown" rank="genus">HYPARRHENIA</taxon_name>
    <taxon_name authority="(Nees) Stapf" date="unknown" rank="species">rufa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus hyparrhenia;species rufa</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Jaragua grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose but with short rhizomes.</text>
      <biological_entity id="o24817" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character constraint="with rhizomes" constraintid="o24818" is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24818" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-350 cm.</text>
      <biological_entity id="o24819" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="350" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o24820" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 30-60 cm long, 2-8 mm wide.</text>
      <biological_entity id="o24821" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="length" src="d0_s4" to="60" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Peduncles 0.7-7 cm;</text>
    </statement>
    <statement id="d0_s6">
      <text>rames 1.5-2.5 cm, 1 almost sessile, the other with a 6-10 mm stalk, both with 7-14 heterogamous spikelet pairs.</text>
      <biological_entity id="o24822" name="peduncle" name_original="peduncles" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s5" to="7" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s6" to="2.5" to_unit="cm" />
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o24823" name="stalk" name_original="stalk" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="almost" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character char_type="range_value" from="6" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s6" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24824" name="spikelet" name_original="spikelet" src="d0_s6" type="structure">
        <character char_type="range_value" from="7" is_modifier="true" name="quantity" src="d0_s6" to="14" />
        <character is_modifier="true" name="reproduction" src="d0_s6" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Glumes of all spikelets moderately densely pubescent, hairs reddish.</text>
      <biological_entity id="o24825" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="moderately densely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24826" name="spikelet" name_original="spikelets" src="d0_s7" type="structure" />
      <biological_entity id="o24827" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
      </biological_entity>
      <relation from="o24825" id="r4201" name="part_of" negation="false" src="d0_s7" to="o24826" />
    </statement>
    <statement id="d0_s8">
      <text>Sessile spikelets of homogamous pairs 3-5.5 mm, sessile spikelets of heterogamous pairs 3.2-4.2 mm;</text>
      <biological_entity id="o24828" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o24829" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lemmas awned, awns 2-3 cm.</text>
      <biological_entity id="o24830" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o24831" name="awn" name_original="awns" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s9" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pedicellate spikelets 3-5 mm. 2n = 30, 36, 40.</text>
      <biological_entity id="o24832" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="pedicellate" value_original="pedicellate" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24833" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="30" value_original="30" />
        <character name="quantity" src="d0_s10" value="36" value_original="36" />
        <character name="quantity" src="d0_s10" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hyparrhenia rufa is native to the Eastern Hemisphere tropics, but is now established in tropical America. It grows in ditches, pastures, swamps, and pine flatwoods, and along roadsides, in the southeastern United States.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Pacific Islands (Hawaii);Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>