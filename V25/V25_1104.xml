<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">390</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">OPLISMENUS</taxon_name>
    <taxon_name authority="(L.) P. Beauv." date="unknown" rank="species">hirtellus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus oplismenus;species hirtellus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oplismenus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hirtellus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">setarius</taxon_name>
    <taxon_hierarchy>genus oplismenus;species hirtellus;subspecies setarius</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Oplismenus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">hirtellus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">fasciculatus</taxon_name>
    <taxon_hierarchy>genus oplismenus;species hirtellus;subspecies fasciculatus</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Bristle basketgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o22751" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 90 cm, mostly decumbent and rooting at the nodes, distal 15-35 cm ascending when flowering;</text>
      <biological_entity id="o22752" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="90" to_unit="cm" />
        <character is_modifier="false" modifier="mostly" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character constraint="at nodes" constraintid="o22753" is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o22753" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity constraint="distal" id="o22754" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="35" to_unit="cm" />
        <character is_modifier="false" modifier="when flowering" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes usually pubescent;</text>
      <biological_entity id="o22755" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes usually pubescent along 1 side (sometimes glabrous).</text>
      <biological_entity id="o22756" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character constraint="along side" constraintid="o22757" is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22757" name="side" name_original="side" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths conspicuously ciliate on the margins;</text>
      <biological_entity id="o22758" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="on margins" constraintid="o22759" is_modifier="false" modifier="conspicuously" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o22759" name="margin" name_original="margins" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.4-1.6 (2.3) mm, ciliate;</text>
      <biological_entity id="o22760" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="mm" value="2.3" value_original="2.3" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s5" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades (0.6) 1.3-11.5 cm long, 2-20 mm wide, glabrous, scabrous, or pubescent, margin (s) usually undulating.</text>
      <biological_entity id="o22761" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="0.6" value_original="0.6" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="length" src="d0_s6" to="11.5" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o22762" name="margin" name_original="margin" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Panicles (1.5) 2.5-16.5 cm, with 2-10 primary branches;</text>
      <biological_entity id="o22763" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="cm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s7" to="16.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="primary" id="o22764" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="10" />
      </biological_entity>
      <relation from="o22763" id="r3876" name="with" negation="false" src="d0_s7" to="o22764" />
    </statement>
    <statement id="d0_s8">
      <text>branches 0.1-2.5 cm.</text>
      <biological_entity id="o22765" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s8" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 2.2-3.5 (4.5) mm;</text>
      <biological_entity id="o22766" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="4.5" value_original="4.5" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calluses shortly pubescent at the base awns usually purplish.</text>
      <biological_entity id="o22767" name="callus" name_original="calluses" src="d0_s10" type="structure">
        <character constraint="at base awns" constraintid="o22768" is_modifier="false" modifier="shortly" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="base" id="o22768" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes 1.5-3 mm, scabridulous and/or pubescent, 3-5-veined, awns (1.6) 3.2-14.5 mm;</text>
      <biological_entity constraint="lower" id="o22769" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s11" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity id="o22770" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="1.6" value_original="1.6" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="some_measurement" src="d0_s11" to="14.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 1.5-2.5 mm, sparsely pubescent, 5-7-veined, awns 0.8-6 (10) mm;</text>
      <biological_entity constraint="upper" id="o22771" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
      <biological_entity id="o22772" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower florets usually sterile, occasionally staminate;</text>
      <biological_entity constraint="lower" id="o22773" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s13" value="sterile" value_original="sterile" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s13" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower lemmas 2.2-3.1 mm, sparsely pubescent above, (5) 7-9-veined, awns 0.1-1.2 mm;</text>
      <biological_entity constraint="lower" id="o22774" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.1" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="(5)7-9-veined" value_original="(5)7-9-veined" />
      </biological_entity>
      <biological_entity id="o22775" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower paleas absent or to 2.3 mm, hyaline;</text>
      <biological_entity constraint="lower" id="o22776" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s15" value="0-2.3 mm" value_original="0-2.3 mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper lemmas (2.1) 2.3-3 mm, glabrous, weakly cartilaginous, white to cream-colored;</text>
      <biological_entity constraint="upper" id="o22777" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="2.1" value_original="2.1" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s16" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="weakly" name="pubescence_or_texture" src="d0_s16" value="cartilaginous" value_original="cartilaginous" />
        <character char_type="range_value" from="white" name="coloration" src="d0_s16" to="cream-colored" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, 1.3-1.7 mm.</text>
      <biological_entity id="o22778" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s17" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 1.7-1.8 mm long, 0.5-0.9 mm wide, glabrous.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 36, (54), 72, (90).</text>
      <biological_entity id="o22779" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s18" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s18" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o22780" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" value_original="36" />
        <character name="atypical_quantity" src="d0_s19" value="54" value_original="54" />
        <character name="quantity" src="d0_s19" value="72" value_original="72" />
        <character name="atypical_quantity" src="d0_s19" value="90" value_original="90" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Oplismenus hirtellus grows at scattered locations in the southeastern United States, extending south in subtropical and tropical habitats to Argentina. Scholz (1981) recognized 11 subspecies and two forms within the species, but they overlap, both morphologically and geographically. The key below is included for convenience. It includes the three subspecies attributed to the Flora region. In addition, a variegated form culti</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;Okla.;Miss.;Ala.;N.C.;La.;Puerto Rico;Virgin Islands;Va.;S.C.;Ark.;Fla.;Ga.;Pacific Islands (Hawaii);Mo.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <discussion>According to Scholz (1981), Oplismenus hirtellus subsp. fasciculatus U. Scholz is restricted to southern Louisiana and eastern Mexico; O. hirtellus subsp. setarius (Lam.) Mez ex Ekman is more widely distributed, growing both in the southeastern United States and Mexico. Oplismenus hirtellus subsp. undulatifolius (Ard.) U. Scholz was found recently in Maryland (Peterson et al. 1999); it is native to the Eastern Hemisphere.</discussion>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Sheaths and culms noticeably pilose, the hairs 1-3 mm long; lemmas 7-veined</description>
      <determination>Oplismenus hirtellus subsp. undulatifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Sheaths and culms glabrous or with a few, scattered hairs less than 1 mm long; lemmas (7)9-11-veined.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Spikelets  2.2-2.7(3.3) mm long; lowest panicle branches 0.1-0.5 cm long</description>
      <determination>Oplismenus hirtellus subsp. setarius</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Spikelets 3-3.4(4.5) mm long; lowest panicle branches 0.5-0.7 cm long</description>
      <determination>Oplismenus hirtellus subsp. fasciculatus</determination>
    </key_statement>
  </key>
</bio:treatment>