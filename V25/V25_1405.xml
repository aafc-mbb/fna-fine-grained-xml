<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">548</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">SETARIA</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Setaria</taxon_name>
    <taxon_name authority="(Scribn. &amp; Merr.) K. Schum." date="unknown" rank="species">villosissima</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus setaria;subgenus setaria;species villosissima</taxon_hierarchy>
  </taxon_identification>
  <number>10</number>
  <other_name type="common_name">Hairyleaf bristlegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose.</text>
      <biological_entity id="o7529" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 40-100 cm.</text>
      <biological_entity id="o7530" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s2" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths villous distally, margins ciliate;</text>
      <biological_entity id="o7531" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o7532" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules about 1 mm, densely ciliate, hairs white;</text>
      <biological_entity id="o7533" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o7534" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 15-30 cm long, 5-8 mm wide, both surfaces villous.</text>
      <biological_entity id="o7535" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7536" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="villous" value_original="villous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 10-20 cm, loosely spicate;</text>
      <biological_entity id="o7537" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s6" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="architecture" src="d0_s6" value="spicate" value_original="spicate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bristles usually solitary, 10-20 mm.</text>
      <biological_entity id="o7538" name="bristle" name_original="bristles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s7" to="20" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 2.8-3 mm.</text>
      <biological_entity id="o7539" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s8" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Lower glumes about 1/3 as long as the spikelets, broadly ovate, 3-veined;</text>
      <biological_entity constraint="lower" id="o7540" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character constraint="as-long-as spikelets" constraintid="o7541" name="quantity" src="d0_s9" value="1/3" value_original="1/3" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s9" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o7541" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>upper glumes nearly equaling the spikelets, 5-7-veined;</text>
      <biological_entity constraint="upper" id="o7542" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="5-7-veined" value_original="5-7-veined" />
      </biological_entity>
      <biological_entity id="o7543" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="nearly" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower lemmas equaling the upper lemmas, 5-veined;</text>
      <biological_entity constraint="lower" id="o7544" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o7545" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower paleas about 1/5 as long as the upper paleas, lanceolate;</text>
      <biological_entity constraint="lower" id="o7546" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character constraint="as-long-as upper paleas" constraintid="o7547" name="quantity" src="d0_s12" value="1/5" value_original="1/5" />
        <character is_modifier="false" name="shape" notes="" src="d0_s12" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="upper" id="o7547" name="palea" name_original="paleas" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper lemmas finely and transversely undulate-rugose basally, striate and punctate distally;</text>
      <biological_entity constraint="upper" id="o7548" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="transversely; basally" name="relief" src="d0_s13" value="undulate-rugose" value_original="undulate-rugose" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s13" value="striate and punctate" value_original="striate and punctate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper paleas similar, ovatelanceolate.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 54.</text>
      <biological_entity constraint="upper" id="o7549" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7550" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Setaria villosissima is a rare species that grows on granitic soils in southwestern Texas and northern Mexico. The villous sheaths and blades and large spikelets of S. villosissima aid in its identification. A.S. Hitchcock's (1951) report of Setaria villosissima from Arizona is based on misidentification of a specimen of S. leucophila (Reeder 1994).</discussion>
  <references>
    <reference>Reeder, J.R. 1994. Setaria villosissima (Gramineae) in Arizona: Fact or fiction. Phytologia 77:452-455</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>