<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">326</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Andy Sudkamp</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Caro" date="unknown" rank="subfamily">ARISTIDOIDEAE</taxon_name>
    <taxon_name authority="C.E. Hubb." date="unknown" rank="tribe">ARISTIDEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">ARISTIDA</taxon_name>
    <taxon_name authority="Beetle" date="unknown" rank="species">gypsophila</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily aristidoideae;tribe aristideae;genus aristida;species gypsophila</taxon_hierarchy>
  </taxon_identification>
  <number>11</number>
  <other_name type="common_name">Gypsum threeawn</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o3296" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 45-80 cm, erect, usually unbranched.</text>
      <biological_entity id="o3297" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="45" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o3298" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths longer than the internodes, glabrous except at the summit;</text>
      <biological_entity id="o3299" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character constraint="than the internodes" constraintid="o3300" is_modifier="false" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
        <character constraint="except " constraintid="o3301" is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3300" name="internode" name_original="internodes" src="d0_s3" type="structure" />
      <biological_entity id="o3301" name="summit" name_original="summit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>collars densely pilose, hairs 1-3 mm, cobwebby and tangled, often deflexed;</text>
      <biological_entity id="o3302" name="collar" name_original="collars" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o3303" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="cobwebby" value_original="cobwebby" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="tangled" value_original="tangled" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s4" value="deflexed" value_original="deflexed" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules less than 0.5 mm;</text>
      <biological_entity id="o3304" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 5-15 cm long, about 0.5 mm wide, usually involute, occasionally loosely folded, glabrous, light green.</text>
      <biological_entity id="o3305" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="occasionally loosely" name="architecture_or_shape" src="d0_s6" value="folded" value_original="folded" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="light green" value_original="light green" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences paniculate, 12-20 cm long, 2-8 cm wide;</text>
      <biological_entity id="o3306" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="paniculate" value_original="paniculate" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s7" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches 2-5 cm, erect to horizontal, with or without axillary pulvini, with 1-5 spikelets.</text>
      <biological_entity constraint="primary" id="o3307" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s8" to="horizontal" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o3308" name="pulvinus" name_original="pulvini" src="d0_s8" type="structure" />
      <biological_entity id="o3309" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="5" />
      </biological_entity>
      <relation from="o3307" id="r537" name="with or without" negation="false" src="d0_s8" to="o3308" />
      <relation from="o3307" id="r538" name="with" negation="false" src="d0_s8" to="o3309" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets appressed or with axillary pulvini and spreading.</text>
      <biological_entity id="o3310" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="with axillary pulvini" value_original="with axillary pulvini" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s9" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o3311" name="pulvinus" name_original="pulvini" src="d0_s9" type="structure" />
      <relation from="o3310" id="r539" name="with" negation="false" src="d0_s9" to="o3311" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes 6-10 (12) mm, equal or the lower glumes slightly shorter, 1-veined, brownish;</text>
      <biological_entity id="o3312" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character name="atypical_some_measurement" src="d0_s10" unit="mm" value="12" value_original="12" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s10" to="10" to_unit="mm" />
        <character is_modifier="false" name="variability" src="d0_s10" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="lower" id="o3313" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calluses about 0.5 mm;</text>
      <biological_entity id="o3314" name="callus" name_original="calluses" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas (6) 7-14 (16) mm, mostly smooth, mottled, terminating in a 2-4 mm, usually twisted, scabrous beak;</text>
      <biological_entity id="o3315" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character name="atypical_some_measurement" src="d0_s12" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s12" to="14" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="architecture_or_pubescence_or_relief" src="d0_s12" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="coloration" src="d0_s12" value="mottled" value_original="mottled" />
      </biological_entity>
      <biological_entity id="o3316" name="beak" name_original="beak" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s12" value="twisted" value_original="twisted" />
        <character is_modifier="true" name="pubescence_or_relief" src="d0_s12" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <relation from="o3315" id="r540" name="terminating in a" negation="false" src="d0_s12" to="o3316" />
    </statement>
    <statement id="d0_s13">
      <text>central awns 5-10 mm, sharply curved at the base, spreading distally;</text>
      <biological_entity constraint="central" id="o3317" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="10" to_unit="mm" />
        <character constraint="at base" constraintid="o3318" is_modifier="false" modifier="sharply" name="course" src="d0_s13" value="curved" value_original="curved" />
        <character is_modifier="false" modifier="distally" name="orientation" notes="" src="d0_s13" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o3318" name="base" name_original="base" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>lateral awns absent or to 3 mm, erect;</text>
      <biological_entity constraint="lateral" id="o3319" name="awn" name_original="awns" src="d0_s14" type="structure">
        <character is_modifier="false" name="presence" src="d0_s14" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s14" value="0-3 mm" value_original="0-3 mm" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 3, about 1.5 mm, brown.</text>
      <biological_entity id="o3320" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="3" value_original="3" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 5-8 mm. 2n = unknown.</text>
      <biological_entity id="o3321" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s16" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3322" name="chromosome" name_original="" src="d0_s16" type="structure" />
    </statement>
  </description>
  <discussion>Aristida gypsophila grows on rocky limestone or gypsum hills in thorn-scrub communities of the Chihuahuan Desert, almost always growing in the protection of shrubs. It is very similar to A. pansa, which differs in having three well-developed awns and being, usually, shorter in stature. Both species have involute blades with a characteristic tuft of cobwebby hairs at the collar. Plants from the United States have spreading primary branches with axillary pulvini and appressed spikelets. Mexican plants sometimes have primary branches with no axillary pulvini.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>