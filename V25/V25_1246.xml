<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Panicum</taxon_name>
    <taxon_name authority="Sw." date="unknown" rank="species">hirsutum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus panicum;section panicum;species hirsutum</taxon_hierarchy>
  </taxon_identification>
  <number>9</number>
  <other_name type="common_name">Giant witchgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>forming large clumps from short rhizomes.</text>
      <biological_entity id="o24430" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24431" name="clump" name_original="clumps" src="d0_s1" type="structure">
        <character is_modifier="true" name="size" src="d0_s1" value="large" value_original="large" />
      </biological_entity>
      <biological_entity id="o24432" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
      <relation from="o24430" id="r4142" name="forming" negation="false" src="d0_s1" to="o24431" />
    </statement>
    <statement id="d0_s2">
      <text>Culms 100-300 cm tall, 4-10 mm thick, decumbent, semiwoody at the base, simple or branching from the middle nodes, prophylls prominent, to 15 cm;</text>
      <biological_entity id="o24433" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="100" from_unit="cm" name="height" src="d0_s2" to="300" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="thickness" src="d0_s2" to="10" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at base" constraintid="o24434" is_modifier="false" name="texture" src="d0_s2" value="semiwoody" value_original="semiwoody" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character constraint="from middle nodes" constraintid="o24435" is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity id="o24434" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity constraint="middle" id="o24435" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o24436" name="prophyll" name_original="prophylls" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes contracted, pilose, sericeous;</text>
      <biological_entity id="o24437" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="condition_or_size" src="d0_s3" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="sericeous" value_original="sericeous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>internodes glabrous or with papillose-based hairs below the nodes.</text>
      <biological_entity id="o24438" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="with papillose-based hairs" value_original="with papillose-based hairs" />
      </biological_entity>
      <biological_entity id="o24439" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o24440" name="node" name_original="nodes" src="d0_s4" type="structure" />
      <relation from="o24438" id="r4143" name="with" negation="false" src="d0_s4" to="o24439" />
      <relation from="o24439" id="r4144" name="below" negation="false" src="d0_s4" to="o24440" />
    </statement>
    <statement id="d0_s5">
      <text>Sheaths shorter or longer than the internodes, rounded, sparsely hispid, hairs papillose-based, thick, fragile, penetrating and irritating the skin when handled, margins glabrous or ciliate;</text>
      <biological_entity id="o24441" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
        <character constraint="than the internodes" constraintid="o24442" is_modifier="false" name="length_or_size" src="d0_s5" value="shorter or longer" value_original="shorter or longer" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o24442" name="internode" name_original="internodes" src="d0_s5" type="structure" />
      <biological_entity id="o24444" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="fragile" value_original="fragile" />
        <character is_modifier="false" name="position_relational" src="d0_s5" value="penetrating" value_original="penetrating" />
      </biological_entity>
      <biological_entity id="o24445" name="skin" name_original="skin" src="d0_s5" type="structure">
        <character is_modifier="true" name="toxicity" src="d0_s5" value="irritating" value_original="irritating" />
      </biological_entity>
      <biological_entity id="o24446" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="when handled" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>collars more densely pubescent than the sheaths, hairs papillose-based;</text>
      <biological_entity id="o24447" name="collar" name_original="collars" src="d0_s6" type="structure">
        <character constraint="than the sheaths" constraintid="o24448" is_modifier="false" name="pubescence" src="d0_s6" value="more densely pubescent" value_original="more densely pubescent" />
      </biological_entity>
      <biological_entity id="o24448" name="sheath" name_original="sheaths" src="d0_s6" type="structure" />
      <biological_entity id="o24449" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules 1.5-2 mm, with longer hairs immediately behind, growing from the base of the blades;</text>
      <biological_entity id="o24450" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer" id="o24451" name="hair" name_original="hairs" src="d0_s7" type="structure" />
      <biological_entity id="o24452" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity id="o24453" name="blade" name_original="blades" src="d0_s7" type="structure" />
      <relation from="o24450" id="r4145" name="with" negation="false" src="d0_s7" to="o24451" />
      <relation from="o24450" id="r4146" name="growing from the" negation="false" src="d0_s7" to="o24452" />
      <relation from="o24450" id="r4147" name="growing from the" negation="false" src="d0_s7" to="o24453" />
    </statement>
    <statement id="d0_s8">
      <text>blades 20-50 cm long, 15-40 mm wide, spreading, flat or with involute margins, bases subcordate to cordate, margins glabrous or sparsely hairy.</text>
      <biological_entity id="o24454" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s8" to="50" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="mm" name="width" src="d0_s8" to="40" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s8" value="with involute margins" value_original="with involute margins" />
      </biological_entity>
      <biological_entity id="o24455" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape_or_vernation" src="d0_s8" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o24456" name="base" name_original="bases" src="d0_s8" type="structure">
        <character char_type="range_value" from="subcordate" name="shape" src="d0_s8" to="cordate" />
      </biological_entity>
      <biological_entity id="o24457" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o24454" id="r4148" name="with" negation="false" src="d0_s8" to="o24455" />
    </statement>
    <statement id="d0_s9">
      <text>Panicles terminal, 25-45 cm long, 5-15 cm wide, lax, contracted to diffuse, not breaking at the base and becoming tumbleweeds, all or most secondary branches confined to the distal 1/3;</text>
      <biological_entity id="o24458" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s9" to="45" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s9" to="15" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="lax" value_original="lax" />
        <character is_modifier="false" name="condition_or_size" src="d0_s9" value="contracted" value_original="contracted" />
        <character is_modifier="false" name="density" src="d0_s9" value="diffuse" value_original="diffuse" />
      </biological_entity>
      <biological_entity id="o24459" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o24460" name="tumbleweed" name_original="tumbleweeds" src="d0_s9" type="structure" />
      <biological_entity constraint="secondary" id="o24461" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
        <character name="quantity" src="d0_s9" value="1/3" value_original="1/3" />
      </biological_entity>
      <relation from="o24458" id="r4149" name="breaking at the" negation="true" src="d0_s9" to="o24459" />
    </statement>
    <statement id="d0_s10">
      <text>lower branches whorled;</text>
      <biological_entity constraint="lower" id="o24462" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="whorled" value_original="whorled" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>pedicels 0.5-2 mm, appressed.</text>
      <biological_entity id="o24463" name="pedicel" name_original="pedicels" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s11" value="appressed" value_original="appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spikelets 1.8-2.5 mm long, 0.5-1 mm wide, narrowly ellipsoid, glabrous.</text>
      <biological_entity id="o24464" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s12" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Lower glumes 0.7-1.4 mm, about 1/2 as long as the spikelets, 3-5-veined, acute to attenuate;</text>
      <biological_entity constraint="lower" id="o24465" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s13" to="1.4" to_unit="mm" />
        <character constraint="as-long-as spikelets" constraintid="o24466" name="quantity" src="d0_s13" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s13" to="attenuate" />
      </biological_entity>
      <biological_entity id="o24466" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>upper glumes and lower lemmas subequal, about as long as the spikelets, 7-11-veined;</text>
      <biological_entity constraint="upper" id="o24467" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s14" value="7-11-veined" value_original="7-11-veined" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o24468" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s14" value="7-11-veined" value_original="7-11-veined" />
      </biological_entity>
      <biological_entity id="o24469" name="spikelet" name_original="spikelets" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o24470" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s15" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower paleas 1.3-1.7 mm;</text>
      <biological_entity constraint="lower" id="o24471" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s16" to="1.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper florets 1.2-1.6 mm long, 0.5-0.7 mm wide, glabrous, smooth, shiny, chestnut-brown to dark-brown.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 36.</text>
      <biological_entity constraint="upper" id="o24472" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="length" src="d0_s17" to="1.6" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s17" to="0.7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s17" value="shiny" value_original="shiny" />
        <character char_type="range_value" from="chestnut-brown" name="coloration" src="d0_s17" to="dark-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24473" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Panicum hirsutum grows along river banks or in ditches, often among shrubs in partial shade. Its range extends from southern Texas through eastern Mexico, Central America, Cuba, and the West Indies to Ecuador, Brazil, and Argentina.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>