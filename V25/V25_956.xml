<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Kelly W. Allred;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">298</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="N.P. Barker &amp; H.P. Under" date="unknown" rank="subfamily">DANTHONIOIDEAE</taxon_name>
    <taxon_name authority="Zotov" date="unknown" rank="tribe">DANTHONIEAE</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="genus">CORTADERIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily danthonioideae;tribe danthonieae;genus cortaderia</taxon_hierarchy>
  </taxon_identification>
  <number>20.01</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>often dioecious or monoecious;</text>
    </statement>
    <statement id="d0_s2">
      <text>cespitose.</text>
      <biological_entity id="o2736" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="monoecious" value_original="monoecious" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 2-7 m, erect, densely clumped.</text>
      <biological_entity id="o2737" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="m" name="some_measurement" src="d0_s3" to="7" to_unit="m" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="densely" name="arrangement" src="d0_s3" value="clumped" value_original="clumped" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves primarily basal;</text>
      <biological_entity id="o2738" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o2739" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="primarily" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths open, often overlapping, glabrous or hairy;</text>
      <biological_entity id="o2740" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>auricles absent;</text>
      <biological_entity id="o2741" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules of hairs;</text>
      <biological_entity id="o2742" name="ligule" name_original="ligules" src="d0_s7" type="structure" />
      <biological_entity id="o2743" name="hair" name_original="hairs" src="d0_s7" type="structure" />
      <relation from="o2742" id="r436" name="consists_of" negation="false" src="d0_s7" to="o2743" />
    </statement>
    <statement id="d0_s8">
      <text>blades to 2 m, flat to folded, arching, edges usually sharply serrate.</text>
      <biological_entity id="o2744" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s8" to="2" to_unit="m" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s8" to="folded" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="arching" value_original="arching" />
      </biological_entity>
      <biological_entity id="o2745" name="edge" name_original="edges" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually sharply" name="architecture_or_shape" src="d0_s8" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences terminal, plumose panicles, 30-130 cm, subtended by a long, ciliate bract;</text>
      <biological_entity id="o2746" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o2747" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="plumose" value_original="plumose" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s9" to="130" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o2748" name="bract" name_original="bract" src="d0_s9" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s9" value="long" value_original="long" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <relation from="o2747" id="r437" name="subtended by a" negation="false" src="d0_s9" to="o2748" />
    </statement>
    <statement id="d0_s10">
      <text>branches stiff to flexible.</text>
      <biological_entity id="o2749" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character char_type="range_value" from="stiff" name="fragility" src="d0_s10" to="flexible" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets somewhat laterally compressed, usually unisexual, sometimes bisexual, with 2-9 unisexual florets;</text>
      <biological_entity id="o2751" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" to="9" />
        <character is_modifier="true" name="reproduction" src="d0_s11" value="unisexual" value_original="unisexual" />
      </biological_entity>
      <relation from="o2750" id="r438" name="with" negation="false" src="d0_s11" to="o2751" />
    </statement>
    <statement id="d0_s12">
      <text>disarticulation above the glumes and below the florets.</text>
      <biological_entity id="o2750" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="somewhat laterally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="usually" name="reproduction" src="d0_s11" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="sometimes" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o2752" name="floret" name_original="florets" src="d0_s12" type="structure" />
      <relation from="o2750" id="r439" name="above the glumes and below" negation="false" src="d0_s12" to="o2752" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes unequal, nearly as long as the spikelets, hyaline, 1-veined;</text>
      <biological_entity id="o2753" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s13" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o2754" name="spikelet" name_original="spikelets" src="d0_s13" type="structure" />
      <relation from="o2753" id="r440" modifier="nearly" name="as long as" negation="false" src="d0_s13" to="o2754" />
    </statement>
    <statement id="d0_s14">
      <text>calluses pilose;</text>
      <biological_entity id="o2755" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 3-5 (7) -veined, long-acuminate, bifid and awned or entire and mucronate;</text>
      <biological_entity id="o2756" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-5(7)-veined" value_original="3-5(7)-veined" />
        <character is_modifier="false" name="shape" src="d0_s15" value="long-acuminate" value_original="long-acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="bifid" value_original="bifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s15" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas of pistillate and bisexual florets usually long-sericeous;</text>
      <biological_entity id="o2757" name="lemma" name_original="lemmas" src="d0_s16" type="structure" constraint="floret" constraint_original="floret; floret">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s16" value="long-sericeous" value_original="long-sericeous" />
      </biological_entity>
      <biological_entity id="o2758" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s16" value="pistillate" value_original="pistillate" />
        <character is_modifier="true" name="reproduction" src="d0_s16" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o2757" id="r441" name="part_of" negation="false" src="d0_s16" to="o2758" />
    </statement>
    <statement id="d0_s17">
      <text>lemmas of staminate florets less hairy or glabrous;</text>
      <biological_entity id="o2759" name="lemma" name_original="lemmas" src="d0_s17" type="structure" constraint="floret" constraint_original="floret; floret">
        <character is_modifier="false" name="pubescence" src="d0_s17" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o2760" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="staminate" value_original="staminate" />
      </biological_entity>
      <relation from="o2759" id="r442" name="part_of" negation="false" src="d0_s17" to="o2760" />
    </statement>
    <statement id="d0_s18">
      <text>lodicules 2, cuneate and irregularly lobed, ciliate;</text>
      <biological_entity id="o2761" name="lodicule" name_original="lodicules" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s18" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s18" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s18" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>paleas about 1/2 as long as the lemmas, 2-veined;</text>
      <biological_entity id="o2762" name="palea" name_original="paleas" src="d0_s19" type="structure">
        <character constraint="as-long-as lemmas" constraintid="o2763" name="quantity" src="d0_s19" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s19" value="2-veined" value_original="2-veined" />
      </biological_entity>
      <biological_entity id="o2763" name="lemma" name_original="lemmas" src="d0_s19" type="structure" />
    </statement>
    <statement id="d0_s20">
      <text>anthers of bisexual florets 3, 1.5-6 mm, those of the pistillate florets smaller or absent.</text>
      <biological_entity id="o2764" name="anther" name_original="anthers" src="d0_s20" type="structure" constraint="floret" constraint_original="floret; floret">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" notes="" src="d0_s20" to="6" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s20" value="smaller" value_original="smaller" />
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o2765" name="floret" name_original="florets" src="d0_s20" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s20" value="bisexual" value_original="bisexual" />
        <character name="quantity" src="d0_s20" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o2766" name="floret" name_original="florets" src="d0_s20" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s20" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <relation from="o2764" id="r443" name="consist_of" negation="false" src="d0_s20" to="o2765" />
      <relation from="o2764" id="r444" name="part_of" negation="false" src="d0_s20" to="o2766" />
    </statement>
    <statement id="d0_s21">
      <text>Caryopses 1.5-3 mm;</text>
      <biological_entity id="o2767" name="caryopsis" name_original="caryopses" src="d0_s21" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s21" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>hila linear, about 1/2 as long as the caryopses;</text>
      <biological_entity id="o2768" name="hilum" name_original="hila" src="d0_s22" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s22" value="linear" value_original="linear" />
        <character constraint="as-long-as caryopses" constraintid="o2769" name="quantity" src="d0_s22" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o2769" name="caryopsis" name_original="caryopses" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>embryos usually shorter than 1 mm. x = 9.</text>
      <biological_entity id="o2770" name="embryo" name_original="embryos" src="d0_s23" type="structure">
        <character modifier="usually shorter than" name="some_measurement" src="d0_s23" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="x" id="o2771" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cortaderia, a genus of about 25 species, is native to South America and New Zealand, with the majority of species being South American. Recent evidence suggests that the species in the two regions represent different lineages, each of which merits generic recognition. The species treated here would remain in Cortaderia if this change were made.</discussion>
  <discussion>Both of the species that are found in North America were originally introduced as ornamental species; both are now considered aggressive weeds in parts of the Flora region.</discussion>
  <references>
    <reference>Barker, N.P., CM. Morton and H.P. Linder. 2000. The Danthonieae: Generic composition and relationships. Pp. 221-229 in S.W.L. Jacobs and J. Everett (eds.). Grasses: Systematics and Evolution. International Symposium on Grass Systematics and Evolution (3rd:1998). CSIRO Publishing, Collingwood, Victoria, Australia. 408 pp.</reference>
    <reference>Connor, H.E. and E. Edgar. 1974. Names and types in Cortaderia Stapf (Gramineae). Taxon 23:595-605</reference>
    <reference>Costas-Lippmann, M. 1977. More on the weedy "pampas grass" in California. Fremontia 4:25-27</reference>
    <reference>Hitchcock, A.S. 1951 [title page 1950]. Manual of the Grasses of the United States, ed. 2, rev. A. Chase. U.S.D.A. Miscellaneous Publication No. 200. U.S. Government Printing Office, Washington, D.C., U.S.A. 1051 pp.</reference>
    <reference>Linder, H.P. and N.P. Barker. 2000. Biogeography of the Danthonieae. Pp. 231-238 in S.W.L. Jacobs and J. Everett (eds.). Grasses: Systematics and Evolution. International Symposium on Grass Systematics and Evolution (3rd:1998). CSIRO Publishing, Collingwood, Victoria, Australia. 408 pp.</reference>
    <reference>Walsh, N.G. 1994. Cortaderia. Pp. 546-548 in N.G. Walsh and T.J. Entwisle. Flora of Victoria, vol. 2: Ferns and Allied Plants, Conifers and Monocotyledons. Inkata Press, Melbourne, Australia. 946 pp.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;N.J.;Wash.;Tex.;La.;Utah;Calif.;Ala.;Tenn.;Pacific Islands (Hawaii);Ga.;S.C.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Sheaths hairy; panicles elevated well above the foliage; culms 4-5 times as long as the panicles</description>
      <determination>1 Cortaderia jubata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Sheaths glabrous or sparsely hairy; panicles elevated only slightly, if at all, above the foliage; culms 2-4 times as long as the panicles</description>
      <determination>2 Cortaderia selloana</determination>
    </key_statement>
  </key>
</bio:treatment>