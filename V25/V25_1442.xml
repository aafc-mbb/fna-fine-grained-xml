<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">571</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="P.J. Bergius" date="unknown" rank="species">repens</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species repens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">repens</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">fluitans</taxon_name>
    <taxon_hierarchy>genus paspalum;species repens;variety fluitans</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">fluitans</taxon_name>
    <taxon_hierarchy>genus paspalum;species fluitans</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Water paspalum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>aquatic, floating or rhizomatous.</text>
      <biological_entity id="o17239" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s1" value="aquatic" value_original="aquatic" />
        <character is_modifier="false" name="growth_form_or_location" src="d0_s1" value="floating" value_original="floating" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 4-55 cm, erect;</text>
      <biological_entity id="o17240" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="55" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes pubescent.</text>
      <biological_entity id="o17241" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous or pubescent;</text>
      <biological_entity id="o17242" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-4 mm;</text>
      <biological_entity id="o17243" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 10-40 cm long, 8-22 mm wide, flat, glabrous or sparsely pubescent.</text>
      <biological_entity id="o17244" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="40" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s6" to="22" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal, with (7) 20-70 racemosely arranged branches;</text>
      <biological_entity id="o17245" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o17246" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="7" value_original="7" />
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s7" to="70" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o17245" id="r2897" name="with" negation="false" src="d0_s7" to="o17246" />
    </statement>
    <statement id="d0_s8">
      <text>branches 1.2-9.5 cm, diverging to spreading, occasion¬ally arcuate, disarticulating at maturity;</text>
      <biological_entity id="o17247" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="some_measurement" src="d0_s8" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="diverging" name="orientation" src="d0_s8" to="spreading" />
        <character is_modifier="false" name="course_or_shape" src="d0_s8" value="arcuate" value_original="arcuate" />
        <character constraint="at maturity" is_modifier="false" name="architecture" src="d0_s8" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branch axes 0.7-1.5 mm wide, broadly winged, glabrous, margins scabrous, extending beyond the distal spikelet.</text>
      <biological_entity constraint="branch" id="o17248" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s9" to="1.5" to_unit="mm" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17249" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="distal" id="o17250" name="spikelet" name_original="spikelet" src="d0_s9" type="structure" />
      <relation from="o17249" id="r2898" name="extending beyond" negation="false" src="d0_s9" to="o17250" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 1.1-1.9 mm long, 0.5-0.8 mm wide, solitary, appressed to the branch axes, elliptic, pubescent, white.</text>
      <biological_entity id="o17251" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="length" src="d0_s10" to="1.9" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s10" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
        <character constraint="to branch axes" constraintid="o17252" is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="arrangement_or_shape" notes="" src="d0_s10" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="branch" id="o17252" name="axis" name_original="axes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes absent;</text>
      <biological_entity constraint="lower" id="o17253" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes and lower lemmas veinless;</text>
      <biological_entity constraint="upper" id="o17254" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="veinless" value_original="veinless" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o17255" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="veinless" value_original="veinless" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper florets white.</text>
      <biological_entity constraint="upper" id="o17256" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Caryopses 0.8-0.9 mm, translucent, white.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n = 20.</text>
      <biological_entity id="o17257" name="caryopsis" name_original="caryopses" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s14" value="translucent" value_original="translucent" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17258" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum repens is a native species that grows along the edges of lakes, streams, and roadside ditches in the southeastern United States. Its range extends through tropical America to Peru, Bolivia, and Argentina.</discussion>
  
</bio:treatment>