<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">240</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Rich." date="unknown" rank="genus">CYNODON</taxon_name>
    <taxon_name authority="Nees" date="unknown" rank="species">incompletus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus cynodon;species incompletus</taxon_hierarchy>
  </taxon_identification>
  <number>7</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants stoloniferous, not rhizomatous.</text>
      <biological_entity id="o30026" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 5-30 cm.</text>
      <biological_entity id="o30027" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous;</text>
      <biological_entity id="o30028" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>ligules membranous;</text>
      <biological_entity id="o30029" name="ligule" name_original="ligules" src="d0_s3" type="structure">
        <character is_modifier="false" name="texture" src="d0_s3" value="membranous" value_original="membranous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blades 1.5-6 cm long, 2-4 mm wide, glabrous or pubescent.</text>
      <biological_entity id="o30030" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s4" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Panicles with 2-6 branches;</text>
      <biological_entity id="o30031" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <biological_entity id="o30032" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="6" />
      </biological_entity>
      <relation from="o30031" id="r5124" name="with" negation="false" src="d0_s5" to="o30032" />
    </statement>
    <statement id="d0_s6">
      <text>branches 2-5 cm, in a single whorl, axes flattened.</text>
      <biological_entity id="o30033" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s6" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o30034" name="whorl" name_original="whorl" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o30035" name="axis" name_original="axes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o30033" id="r5125" name="in" negation="false" src="d0_s6" to="o30034" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 2-3 mm, narrowly to broadly ovate.</text>
      <biological_entity id="o30036" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Glumes 1.7-2.1 mm, exceeded by the florets;</text>
      <biological_entity id="o30037" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s8" to="2.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o30038" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <relation from="o30037" id="r5126" name="exceeded by the" negation="false" src="d0_s8" to="o30038" />
    </statement>
    <statement id="d0_s9">
      <text>lemmas 2.2-2.6 mm, keels winged and pubescent, mar¬gins glabrous.</text>
      <biological_entity id="o30039" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>2n = 18, 36.</text>
      <biological_entity id="o30040" name="keel" name_original="keels" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o30041" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="18" value_original="18" />
        <character name="quantity" src="d0_s10" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cynodon incompletus is native to southern Africa. A hybrid between the two varieties identified below, Cynodon xbradleyi Stent, is used as a lawn grass in North America (de Wet and Harlan 1970).</discussion>
  <discussion>1. Blades glabrous or sparsely hirsute; spikelets 2.5-3 mm long, narrowly ovate 	var. incompletus 1. Blades densely hirsute; spikelets 2-2.5 mm long, broadly ovate 	var. hirsutus</discussion>
  
</bio:treatment>