<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">162</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="J.E Gmel." date="unknown" rank="species">schreberi</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species schreberi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muhlenbergia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">palustris</taxon_name>
    <taxon_hierarchy>genus muhlenbergia;species palustris</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muhlenbergia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">diffusa</taxon_name>
    <taxon_hierarchy>genus muhlenbergia;species diffusa</taxon_hierarchy>
  </taxon_identification>
  <number>13</number>
  <other_name type="common_name">Nimblewill</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial (appearing annual);</text>
    </statement>
    <statement id="d0_s1">
      <text>usually cespitose, not rhizomatous, sometimes stoloniferous.</text>
      <biological_entity id="o21094" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-45 (70) cm, geniculate, often rooting at the lower nodes, glabrous or puberulent below the nodes;</text>
      <biological_entity id="o21095" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="70" value_original="70" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="45" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s2" value="geniculate" value_original="geniculate" />
        <character constraint="at lower nodes" constraintid="o21096" is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character constraint="below nodes" constraintid="o21097" is_modifier="false" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o21096" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o21097" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>internodes often smooth, shiny, glabrous.</text>
      <biological_entity id="o21098" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths shorter than the internodes, glabrous for most of their length, margins shortly (0.3-1.2 mm) pubescent distally, not becoming spirally coiled when old;</text>
      <biological_entity id="o21099" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o21100" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character constraint="for" is_modifier="false" name="length" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o21100" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity id="o21101" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="shortly; distally" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="when old" name="architecture" src="d0_s4" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.2-0.5 mm, truncate, erose, ciliate;</text>
      <biological_entity id="o21102" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s5" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades (1) 3-10 cm long, 1-4.5 mm wide, flat, smooth or scabridulous.</text>
      <biological_entity id="o21103" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 3-15 cm long, 1-1.6 cm wide, contracted, often interrupted below;</text>
      <biological_entity id="o21104" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="15" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s7" to="1.6" to_unit="cm" />
        <character is_modifier="false" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
        <character is_modifier="false" modifier="often; below" name="architecture" src="d0_s7" value="interrupted" value_original="interrupted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>branches 0.4-5.5 cm, appressed or diverging up to 30° from the rachises, spikelet-bearing to the base;</text>
      <biological_entity id="o21105" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s8" to="5.5" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="appressed" value_original="appressed" />
        <character constraint="from rachises" constraintid="o21106" is_modifier="false" modifier="0-30°" name="orientation" src="d0_s8" value="diverging" value_original="diverging" />
      </biological_entity>
      <biological_entity id="o21106" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
      <biological_entity id="o21107" name="base" name_original="base" src="d0_s8" type="structure" />
      <relation from="o21105" id="r3592" name="to" negation="false" src="d0_s8" to="o21107" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels 0.1-4 mm, scabrous to hirsute;</text>
    </statement>
    <statement id="d0_s10">
      <text>disarticulation above the glumes.</text>
      <biological_entity id="o21108" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="scabrous" name="pubescence" src="d0_s9" to="hirsute" />
      </biological_entity>
      <biological_entity id="o21109" name="glume" name_original="glumes" src="d0_s10" type="structure" />
      <relation from="o21108" id="r3593" name="above" negation="false" src="d0_s10" to="o21109" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 1.8-2.8 mm, borne singly.</text>
      <biological_entity id="o21110" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s11" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s11" value="singly" value_original="singly" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes unequal, shorter than the florets, thin and membranous throughout, unawned;</text>
      <biological_entity id="o21111" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="unequal" value_original="unequal" />
        <character constraint="than the florets" constraintid="o21112" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="width" src="d0_s12" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="throughout" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o21112" name="floret" name_original="florets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>lower glumes lacking or rudimentary, veinless, rounded and often erose;</text>
      <biological_entity constraint="lower" id="o21113" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s13" value="lacking" value_original="lacking" />
        <character is_modifier="false" name="prominence" src="d0_s13" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="veinless" value_original="veinless" />
        <character is_modifier="false" name="shape" src="d0_s13" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="often" name="architecture_or_relief" src="d0_s13" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes 0.1-0.3 mm, veinless;</text>
      <biological_entity constraint="upper" id="o21114" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s14" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="veinless" value_original="veinless" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas 1.8-2.8 mm, oblongelliptic, mostly scabrous, calluses hairy, hairs to 0.8 mm, veins greenish, lower 1/4 of the midveins with a few appressed hairs, apices acute to acuminate, awned, awns 1.5-5 mm, straight;</text>
      <biological_entity id="o21115" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s15" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" modifier="mostly" name="pubescence_or_relief" src="d0_s15" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o21116" name="callus" name_original="calluses" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o21117" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21118" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="position" src="d0_s15" value="lower" value_original="lower" />
        <character constraint="of midveins" constraintid="o21119" name="quantity" src="d0_s15" value="1/4" value_original="1/4" />
      </biological_entity>
      <biological_entity id="o21119" name="midvein" name_original="midveins" src="d0_s15" type="structure" />
      <biological_entity id="o21120" name="hair" name_original="hairs" src="d0_s15" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s15" value="few" value_original="few" />
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s15" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o21121" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s15" to="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o21122" name="awn" name_original="awns" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s15" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o21119" id="r3594" name="with" negation="false" src="d0_s15" to="o21120" />
    </statement>
    <statement id="d0_s16">
      <text>paleas 1.8-2.8 mm, oblongelliptic, acute to acuminate;</text>
      <biological_entity id="o21123" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s16" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s16" to="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 0.2-0.5 mm, yellow.</text>
      <biological_entity id="o21124" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s17" to="0.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Caryopses 1-1.4 mm, fusiform, brownish.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = 40, 42.</text>
      <biological_entity id="o21125" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s18" to="1.4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s18" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s18" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21126" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="40" value_original="40" />
        <character name="quantity" src="d0_s19" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia schreberi grows in moist to dry woods and prairies on rocky slopes, in ravines, and along sandy riverbanks, at elevations of 60-1600 m. It is also common in disturbed sites near cultivated fields, pastures, and roads at these elevations. Its geographic range includes central, but not northern, Mexico. Records from the western United States probably reflect receent introductions. The species is considered a noxious, invasive weed in California.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Del.;D.C.;Wis.;W.Va.;Ont.;Mass.;Maine;N.H.;R.I.;Vt.;Fla.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;Va.;Colo.;Md.;Calif.;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Kans.;Nebr.;Okla.;Ohio;Utah;Mo.;Minn.;Mich.;Miss.;Ky.;S.Dak.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>