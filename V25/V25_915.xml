<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">276</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Kunth" date="unknown" rank="genus">HILARIA</taxon_name>
    <taxon_name authority="(Thurb.) Benth. ex Scribn." date="unknown" rank="species">rigida</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus hilaria;species rigida</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pleuraphis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">rigida</taxon_name>
    <taxon_hierarchy>genus pleuraphis;species rigida</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Big galleta</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, sometimes rhizomatous.</text>
      <biological_entity id="o26715" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 35-250 cm, decumbent, much branched above the base, becoming almost woody;</text>
      <biological_entity id="o26716" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="35" from_unit="cm" name="some_measurement" src="d0_s2" to="250" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="above base" constraintid="o26717" is_modifier="false" modifier="much" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="becoming almost" name="texture" notes="" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o26717" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>upper nodes glabrous or villous, hairs to 1.5 mm;</text>
      <biological_entity constraint="upper" id="o26718" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o26719" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>lower internodes tomentose.</text>
      <biological_entity constraint="lower" id="o26720" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ligules 1-2 mm, densely ciliate;</text>
      <biological_entity id="o26721" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 2-10 (16) cm long, 2-5 mm wide, flat basally, involute distally.</text>
      <biological_entity id="o26722" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="16" value_original="16" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="basally" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="distally" name="shape_or_vernation" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 4-12 cm;</text>
    </statement>
    <statement id="d0_s8">
      <text>fascicles 6-12 mm.</text>
      <biological_entity id="o26723" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s7" to="12" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="fascicles" value_original="fascicles" />
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s8" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Lateral spikelets with 2-4 florets, lower 2 florets staminate, other florets (if present) usually ster¬ile;</text>
      <biological_entity constraint="lateral" id="o26724" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o26725" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="4" />
      </biological_entity>
      <biological_entity constraint="lower" id="o26726" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o26727" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26728" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <relation from="o26724" id="r4546" name="with" negation="false" src="d0_s9" to="o26725" />
    </statement>
    <statement id="d0_s10">
      <text>glumes thin, membranous, not fused at the base, lanceolate or parallel-sided, 7-veined, awned, awns exceeding the glume apices, apices 2-4-lobed, lobes acute to rounded, long-ciliate, sometimes with 1-3 excurrent veins that form additional slender awns to 1.8 mm;</text>
      <biological_entity id="o26729" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
        <character constraint="at base" constraintid="o26730" is_modifier="false" modifier="not" name="fusion" src="d0_s10" value="fused" value_original="fused" />
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="parallel-sided" value_original="parallel-sided" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="7-veined" value_original="7-veined" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o26730" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o26731" name="awn" name_original="awns" src="d0_s10" type="structure" />
      <biological_entity constraint="glume" id="o26732" name="apex" name_original="apices" src="d0_s10" type="structure" />
      <biological_entity id="o26733" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="2-4-lobed" value_original="2-4-lobed" />
      </biological_entity>
      <biological_entity id="o26734" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s10" to="rounded" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s10" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
      <biological_entity id="o26735" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s10" to="3" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="slender" id="o26736" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="additional" value_original="additional" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
      </biological_entity>
      <relation from="o26731" id="r4547" name="exceeding the" negation="false" src="d0_s10" to="o26732" />
      <relation from="o26734" id="r4548" modifier="sometimes" name="with" negation="false" src="d0_s10" to="o26735" />
    </statement>
    <statement id="d0_s11">
      <text>lower glumes with dorsal, divergent awns;</text>
      <biological_entity constraint="lower" id="o26737" name="glume" name_original="glumes" src="d0_s11" type="structure" />
      <biological_entity id="o26738" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character is_modifier="true" name="position" src="d0_s11" value="dorsal" value_original="dorsal" />
        <character is_modifier="true" name="arrangement" src="d0_s11" value="divergent" value_original="divergent" />
      </biological_entity>
      <relation from="o26737" id="r4549" name="with" negation="false" src="d0_s11" to="o26738" />
    </statement>
    <statement id="d0_s12">
      <text>upper glumes with subapical awns;</text>
      <biological_entity constraint="upper" id="o26739" name="glume" name_original="glumes" src="d0_s12" type="structure" />
      <biological_entity constraint="subapical" id="o26740" name="awn" name_original="awns" src="d0_s12" type="structure" />
      <relation from="o26739" id="r4550" name="with" negation="false" src="d0_s12" to="o26740" />
    </statement>
    <statement id="d0_s13">
      <text>anthers 3, 4-4.5 mm.</text>
      <biological_entity id="o26741" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s13" to="4.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Central spikelets equaling or exceeding the lateral spikelets, with 1 stipitate, bisexual floret;</text>
      <biological_entity constraint="central" id="o26742" name="spikelet" name_original="spikelets" src="d0_s14" type="structure">
        <character is_modifier="false" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s14" value="exceeding the lateral spikelets" value_original="exceeding the lateral spikelets" />
      </biological_entity>
      <biological_entity id="o26743" name="floret" name_original="floret" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
        <character is_modifier="true" name="architecture" src="d0_s14" value="stipitate" value_original="stipitate" />
        <character is_modifier="true" name="reproduction" src="d0_s14" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o26742" id="r4551" name="with" negation="false" src="d0_s14" to="o26743" />
    </statement>
    <statement id="d0_s15">
      <text>glumes thin, membranous, narrow, deeply cleft into few-several acuminate, ciliate lobes and slender awns;</text>
      <biological_entity id="o26744" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="width" src="d0_s15" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="size_or_width" src="d0_s15" value="narrow" value_original="narrow" />
        <character constraint="into slender awns" constraintid="o26746" is_modifier="false" modifier="deeply" name="architecture_or_shape" src="d0_s15" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o26745" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character char_type="range_value" from="few" is_modifier="true" name="quantity" src="d0_s15" to="several" />
        <character is_modifier="true" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s15" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity constraint="slender" id="o26746" name="awn" name_original="awns" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>lemmas often exceeding the glumes, thin, ciliate, 2-lobed, midveins excurrent.</text>
      <biological_entity id="o26747" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="false" name="width" src="d0_s16" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s16" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o26748" name="glume" name_original="glumes" src="d0_s16" type="structure" />
      <relation from="o26747" id="r4552" modifier="often" name="exceeding the" negation="false" src="d0_s16" to="o26748" />
    </statement>
    <statement id="d0_s17">
      <text>2n = 18, 36, 54.</text>
      <biological_entity id="o26749" name="midvein" name_original="midveins" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o26750" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="18" value_original="18" />
        <character name="quantity" src="d0_s17" value="36" value_original="36" />
        <character name="quantity" src="d0_s17" value="54" value_original="54" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hilaria rigida grows in deserts and open juniper stands, at low elevations, from the southwestern United States to central Mexico. Although almost shrubby, it is very popular with pack horses.</discussion>
  
</bio:treatment>