<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">505</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">UROCHLOA</taxon_name>
    <taxon_name authority="(Hack, ex T. Durand &amp; Schinz) Morrone &amp; Zuloaga" date="unknown" rank="species">arrecta</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus urochloa;species arrecta</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>16</number>
  <other_name type="common_name">African signalgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>stoloniferous.</text>
      <biological_entity id="o23194" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 50-120 cm, branching and rooting at the lower nodes;</text>
      <biological_entity id="o23195" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="50" from_unit="cm" name="some_measurement" src="d0_s2" to="120" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character constraint="at lower nodes" constraintid="o23196" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o23196" name="node" name_original="nodes" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o23197" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous, margins ciliate;</text>
      <biological_entity id="o23198" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23199" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules about 1 mm;</text>
      <biological_entity id="o23200" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character name="some_measurement" src="d0_s5" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 5-15 cm long, 7-15 mm wide, glabrous, bases subcordate, margins scabrous.</text>
      <biological_entity id="o23201" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s6" to="15" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23202" name="base" name_original="bases" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="subcordate" value_original="subcordate" />
      </biological_entity>
      <biological_entity id="o23203" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles (5) 9-18 (25) cm long, 3-4 cm wide, with 4-10 (15) spikelike primary branches in 2 ranks;</text>
      <biological_entity id="o23204" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character name="length" src="d0_s7" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s7" to="18" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s7" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="primary" id="o23205" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="15" value_original="15" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s7" to="10" />
        <character is_modifier="true" name="shape" src="d0_s7" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o23206" name="rank" name_original="ranks" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o23204" id="r3939" name="with" negation="false" src="d0_s7" to="o23205" />
      <relation from="o23205" id="r3940" name="in" negation="false" src="d0_s7" to="o23206" />
    </statement>
    <statement id="d0_s8">
      <text>primary branches (1) 2-5 (10) cm, axes 0.5-2 mm wide, glabrous, margins scabrous;</text>
      <biological_entity constraint="primary" id="o23207" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="cm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s8" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o23208" name="axis" name_original="axes" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o23209" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>secondary branches rarely present, pedicels shorter than the spikelets, mostly scabrous, apices with hairs.</text>
      <biological_entity constraint="secondary" id="o23210" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="rarely" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o23211" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character constraint="than the spikelets" constraintid="o23212" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="mostly" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o23212" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o23213" name="apex" name_original="apices" src="d0_s9" type="structure" />
      <biological_entity id="o23214" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <relation from="o23213" id="r3941" name="with" negation="false" src="d0_s9" to="o23214" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets (3) 3.3-4.4 mm long, 1.4-1.7 mm wide, ellipsoid, solitary, imbricate, in 2 rows, appressed to the branches.</text>
      <biological_entity id="o23215" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character name="length" src="d0_s10" unit="mm" value="3" value_original="3" />
        <character char_type="range_value" from="3.3" from_unit="mm" name="length" src="d0_s10" to="4.4" to_unit="mm" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="width" src="d0_s10" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="imbricate" value_original="imbricate" />
        <character constraint="to branches" constraintid="o23217" is_modifier="false" name="fixation_or_orientation" notes="" src="d0_s10" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o23216" name="row" name_original="rows" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o23217" name="branch" name_original="branches" src="d0_s10" type="structure" />
      <relation from="o23215" id="r3942" name="in" negation="false" src="d0_s10" to="o23216" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes scarcely separated;</text>
      <biological_entity id="o23218" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="scarcely" name="arrangement" src="d0_s11" value="separated" value_original="separated" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes 1.5-1.8 mm, glabrous, 5-veined, not clasping the base of the spikelets;</text>
      <biological_entity constraint="lower" id="o23219" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s12" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o23220" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="true" modifier="not" name="architecture_or_fixation" src="d0_s12" value="clasping" value_original="clasping" />
      </biological_entity>
      <biological_entity id="o23221" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
      <relation from="o23220" id="r3943" name="part_of" negation="false" src="d0_s12" to="o23221" />
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 3.4-4.1 mm, glabrous, 7-veined;</text>
      <biological_entity constraint="upper" id="o23222" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s13" to="4.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="7-veined" value_original="7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower florets staminate;</text>
      <biological_entity constraint="lower" id="o23223" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower lemmas 3.4-4.1 mm, glabrous, 5-veined;</text>
      <biological_entity constraint="lower" id="o23224" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="3.4" from_unit="mm" name="some_measurement" src="d0_s15" to="4.1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="5-veined" value_original="5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>upper lemmas 2.7-3.5 mm long, 1.3-1.6 mm wide, apices rounded, incurved;</text>
      <biological_entity constraint="upper" id="o23225" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character char_type="range_value" from="2.7" from_unit="mm" name="length" src="d0_s16" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="width" src="d0_s16" to="1.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23226" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="orientation" src="d0_s16" value="incurved" value_original="incurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 1.6-1.8 mm. 2n = unknown.</text>
      <biological_entity id="o23227" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s17" to="1.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23228" name="chromosome" name_original="" src="d0_s17" type="structure" />
    </statement>
  </description>
  <discussion>Urochloa arrecta is native to Africa, but it has been introduced into Florida and Brazil as a forage grass. It is reported to be established in Collier County, Florida.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>