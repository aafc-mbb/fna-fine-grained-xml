<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Stephen J. Darbyshire;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">308</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="N.P. Barker &amp; H.P. Under" date="unknown" rank="subfamily">DANTHONIOIDEAE</taxon_name>
    <taxon_name authority="Zotov" date="unknown" rank="tribe">DANTHONIEAE</taxon_name>
    <taxon_name authority="Conert &amp; Türpe" date="unknown" rank="genus">KARROOCHLOA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily danthonioideae;tribe danthonieae;genus karroochloa</taxon_hierarchy>
  </taxon_identification>
  <number>20.04</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, stoloniferous or rhizomatous.</text>
      <biological_entity id="o14474" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 4-40 cm.</text>
      <biological_entity id="o14475" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths open;</text>
      <biological_entity id="o14476" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules of hairs;</text>
      <biological_entity id="o14477" name="ligule" name_original="ligules" src="d0_s4" type="structure" />
      <biological_entity id="o14478" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <relation from="o14477" id="r2401" name="consists_of" negation="false" src="d0_s4" to="o14478" />
    </statement>
    <statement id="d0_s5">
      <text>blades to 2 mm wide, flat, rolled, or involute.</text>
      <biological_entity id="o14479" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rolled" value_original="rolled" />
        <character is_modifier="false" name="shape" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rolled" value_original="rolled" />
        <character is_modifier="false" name="shape" src="d0_s5" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, contracted panicles, 0.5-6 cm.</text>
      <biological_entity id="o14480" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o14481" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="true" name="condition_or_size" src="d0_s6" value="contracted" value_original="contracted" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 4-8 mm, laterally compressed, with 3-7 florets;</text>
      <biological_entity id="o14483" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="7" />
      </biological_entity>
      <relation from="o14482" id="r2402" name="with" negation="false" src="d0_s7" to="o14483" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation above the glumes.</text>
      <biological_entity id="o14482" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o14484" name="glume" name_original="glumes" src="d0_s8" type="structure" />
      <relation from="o14482" id="r2403" name="above" negation="false" src="d0_s8" to="o14484" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes 3.5-7 mm, subequal, equaling or exceeding the florets, 3-5 (7) -veined;</text>
      <biological_entity id="o14485" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="size" src="d0_s9" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
        <character name="variability" src="d0_s9" value="exceeding the florets" value_original="exceeding the florets" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="3-5(7)-veined" value_original="3-5(7)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>calluses with lateral tufts of hairs;</text>
      <biological_entity id="o14486" name="callus" name_original="calluses" src="d0_s10" type="structure" />
      <biological_entity constraint="lateral" id="o14487" name="tuft" name_original="tufts" src="d0_s10" type="structure" />
      <biological_entity id="o14488" name="hair" name_original="hairs" src="d0_s10" type="structure" />
      <relation from="o14486" id="r2404" name="with" negation="false" src="d0_s10" to="o14487" />
      <relation from="o14487" id="r2405" name="part_of" negation="false" src="d0_s10" to="o14488" />
    </statement>
    <statement id="d0_s11">
      <text>lemmas pubescent and/or with fringes, rows, or tufts of hair, 9-veined, awned from between the 2 apical lobes or teeth, awns twisted and geniculate;</text>
      <biological_entity id="o14489" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character constraint="with tufts" constraintid="o14492" is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s11" value="9-veined" value_original="9-veined" />
        <character constraint="from apical lobes, teeth" constraintid="o14493, o14494" is_modifier="false" name="architecture_or_shape" src="d0_s11" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o14490" name="fringe" name_original="fringes" src="d0_s11" type="structure" />
      <biological_entity id="o14491" name="row" name_original="rows" src="d0_s11" type="structure" />
      <biological_entity id="o14492" name="tuft" name_original="tufts" src="d0_s11" type="structure" />
      <biological_entity constraint="apical" id="o14493" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="apical" id="o14494" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o14495" name="awn" name_original="awns" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="shape" src="d0_s11" value="geniculate" value_original="geniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>paleas 2-veined;</text>
      <biological_entity id="o14496" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-veined" value_original="2-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lodicules 2, pubescent;</text>
      <biological_entity id="o14497" name="lodicule" name_original="lodicules" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 3;</text>
      <biological_entity id="o14498" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>ovaries glabrous.</text>
      <biological_entity id="o14499" name="ovary" name_original="ovaries" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses about 1 mm;</text>
      <biological_entity id="o14500" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character name="some_measurement" src="d0_s16" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>hila short, x = 6.</text>
      <biological_entity id="o14501" name="hilum" name_original="hila" src="d0_s17" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s17" value="short" value_original="short" />
      </biological_entity>
      <biological_entity constraint="x" id="o14502" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="6" value_original="6" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Karroochloa is a southern African genus of four species.</discussion>
  <references>
    <reference>Conert, H.J. and A.M. Tiirpe. 1969. Karroochloa, ein neue Gattung der Gramineen (Poaceae, Arundinoideae, Danthonieae). Senckenberg. Biol. 50:289-318.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>