<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">74</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Wolf" date="unknown" rank="genus">ERAGROSTIS</taxon_name>
    <taxon_name authority="(Michx.) Nees" date="unknown" rank="species">reptans</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus eragrostis;species reptans</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Neeragrostis</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">reptans</taxon_name>
    <taxon_hierarchy>genus neeragrostis;species reptans</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Creeping lovegrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>unisexual, pistillate and staminate plants morphologically similar;</text>
      <biological_entity id="o24907" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="pistillate" value_original="pistillate" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>mat-forming, without innovations, without glands.</text>
      <biological_entity id="o24908" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="growth_form" src="d0_s2" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o24909" name="innovation" name_original="innovations" src="d0_s2" type="structure" />
      <biological_entity id="o24910" name="gland" name_original="glands" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Culms rooting at the lower nodes, erect portion 5-20 cm, glabrous, pilose, or villous, particularly below the panicles.</text>
      <biological_entity id="o24911" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character constraint="at lower nodes" constraintid="o24912" is_modifier="false" name="architecture" src="d0_s3" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o24912" name="node" name_original="nodes" src="d0_s3" type="structure" />
      <biological_entity id="o24913" name="portion" name_original="portion" src="d0_s3" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity id="o24914" name="panicle" name_original="panicles" src="d0_s3" type="structure" />
      <relation from="o24913" id="r4214" name="particularly below" negation="false" src="d0_s3" to="o24914" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths mostly scabrous, margins sometimes with 0.1-0.4 mm hairs;</text>
      <biological_entity id="o24915" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence_or_relief" src="d0_s4" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o24916" name="margin" name_original="margins" src="d0_s4" type="structure" />
      <biological_entity id="o24917" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
      <relation from="o24916" id="r4215" name="with" negation="false" src="d0_s4" to="o24917" />
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.1-0.6 mm;</text>
      <biological_entity id="o24918" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s5" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 1-4 cm long, 1-4.5 mm wide, flat or conduplicate, abaxial surfaces glabrous, adaxial surfaces appressed-pubescent, hairs about 0.2 mm.</text>
      <biological_entity id="o24919" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="4.5" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s6" value="conduplicate" value_original="conduplicate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24920" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o24921" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity id="o24922" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character name="some_measurement" src="d0_s6" unit="mm" value="0.2" value_original="0.2" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal, 1-3 cm long, 0.6-2.5 cm wide, ovate, contracted, exerted or partially included in the upper leaf-sheaths, rachises somewhat viscid, pilose or glabrous;</text>
      <biological_entity id="o24923" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s7" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="width" src="d0_s7" to="2.5" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
      </biological_entity>
      <biological_entity constraint="upper" id="o24924" name="sheath" name_original="leaf-sheaths" src="d0_s7" type="structure" />
      <biological_entity id="o24925" name="rachis" name_original="rachises" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="somewhat" name="coating" src="d0_s7" value="viscid" value_original="viscid" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o24923" id="r4216" modifier="partially" name="included in the" negation="false" src="d0_s7" to="o24924" />
    </statement>
    <statement id="d0_s8">
      <text>primary branches 0.5-1.5 cm, appressed to the rachises, each terminating in a spikelet;</text>
      <biological_entity constraint="primary" id="o24926" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
        <character constraint="to rachises" constraintid="o24927" is_modifier="false" name="fixation_or_orientation" src="d0_s8" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o24927" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
      <biological_entity id="o24928" name="spikelet" name_original="spikelet" src="d0_s8" type="structure" />
      <relation from="o24926" id="r4217" name="terminating in" negation="false" src="d0_s8" to="o24928" />
    </statement>
    <statement id="d0_s9">
      <text>pulvini sparsely pilose or glabrous;</text>
      <biological_entity id="o24929" name="pulvinus" name_original="pulvini" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels 0.2-2 mm, shorter than the spikelets, glabrous or hairy.</text>
      <biological_entity id="o24930" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="2" to_unit="mm" />
        <character constraint="than the spikelets" constraintid="o24931" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o24931" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 5-26 mm long, 1.5-4.7 mm wide, linear to ovate, greenish to stramineous, with 16-60 florets;</text>
      <biological_entity id="o24933" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="16" is_modifier="true" name="quantity" src="d0_s11" to="60" />
      </biological_entity>
      <relation from="o24932" id="r4218" name="with" negation="false" src="d0_s11" to="o24933" />
    </statement>
    <statement id="d0_s12">
      <text>disarticulation in the pistillate florets basipetal, the lemmas falling separately, staminate spikelets not or tardily disarticulating.</text>
      <biological_entity id="o24932" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s11" to="26" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s11" to="4.7" to_unit="mm" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="ovate" />
        <character char_type="range_value" from="greenish" name="coloration" src="d0_s11" to="stramineous" />
      </biological_entity>
      <biological_entity id="o24934" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="development" src="d0_s12" value="basipetal" value_original="basipetal" />
      </biological_entity>
      <biological_entity id="o24935" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="separately" name="life_cycle" src="d0_s12" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity id="o24936" name="spikelet" name_original="spikelets" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
        <character is_modifier="false" modifier="tardily" name="architecture" src="d0_s12" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
      <relation from="o24932" id="r4219" name="in" negation="false" src="d0_s12" to="o24934" />
    </statement>
    <statement id="d0_s13">
      <text>Glumes unequal, ovate, hyaline, glabrous or sparsely hirsute;</text>
      <biological_entity id="o24937" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s13" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes 0.8-1.6 mm, 1-veined;</text>
      <biological_entity constraint="lower" id="o24938" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s14" to="1.6" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s14" value="1-veined" value_original="1-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>upper glumes 1.5-2.5 mm, 1-3-veined;</text>
      <biological_entity constraint="upper" id="o24939" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lemmas (1.5) 1.8-4 mm, ovate, hyaline to membranous, lateral-veins conspicuous, greenish, apices acute to acuminate, sometimes prolonged into a mucro, mucros to 0.4 mm;</text>
      <biological_entity id="o24940" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="ovate" value_original="ovate" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="texture" src="d0_s16" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o24941" name="lateral-vein" name_original="lateral-veins" src="d0_s16" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s16" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o24942" name="apex" name_original="apices" src="d0_s16" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s16" to="acuminate" />
        <character constraint="into mucro" constraintid="o24943" is_modifier="false" modifier="sometimes" name="length" src="d0_s16" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity id="o24943" name="mucro" name_original="mucro" src="d0_s16" type="structure" />
      <biological_entity id="o24944" name="mucro" name_original="mucros" src="d0_s16" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s16" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>paleas 0.7-3.8 mm, hyaline, about 1/2 as long as the lemmas in pistillate florets, as long as the lemmas in staminate florets, keels scabridulous;</text>
      <biological_entity id="o24945" name="palea" name_original="paleas" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s17" to="3.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="hyaline" value_original="hyaline" />
        <character constraint="as-long-as lemmas" constraintid="o24946" name="quantity" src="d0_s17" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o24946" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <biological_entity id="o24947" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o24948" name="lemma" name_original="lemmas" src="d0_s17" type="structure" />
      <biological_entity id="o24949" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s17" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o24950" name="keel" name_original="keels" src="d0_s17" type="structure">
        <character is_modifier="false" name="relief" src="d0_s17" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <relation from="o24946" id="r4220" name="in" negation="false" src="d0_s17" to="o24947" />
      <relation from="o24945" id="r4221" name="as long as" negation="false" src="d0_s17" to="o24948" />
      <relation from="o24948" id="r4222" name="in" negation="false" src="d0_s17" to="o24949" />
    </statement>
    <statement id="d0_s18">
      <text>anthers 3, 1.4-2.2 mm, reddish to yellowish.</text>
      <biological_entity id="o24951" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="3" value_original="3" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s18" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="reddish" name="coloration" src="d0_s18" to="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Caryopses 0.4-0.6 mm, ellipsoid, somewhat laterally compressed, smooth, light reddish-brown.</text>
    </statement>
    <statement id="d0_s20">
      <text>2n = 60.</text>
      <biological_entity id="o24952" name="caryopsis" name_original="caryopses" src="d0_s19" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s19" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s19" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" modifier="somewhat laterally" name="shape" src="d0_s19" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="coloration" src="d0_s19" value="light reddish-brown" value_original="light reddish-brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24953" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Eragrostis reptans grows in wet sand, gravel, and clay soils along rivers and lake margins from the United States to northern Mexico, at 0-400 m, frequently with Cynodon dactylon and Heliotropium. It flowers from April through November.</discussion>
  
</bio:treatment>