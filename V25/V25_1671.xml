<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">704</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">COIX</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">lacryma-jobi</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus coix;species lacryma-jobi</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Job's-tears</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial.</text>
      <biological_entity id="o29133" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 3 m.</text>
      <biological_entity id="o29134" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="3" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves mostly cauline, evidently distichous;</text>
      <biological_entity id="o29135" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="evidently" name="arrangement" notes="" src="d0_s2" value="distichous" value_original="distichous" />
      </biological_entity>
      <biological_entity id="o29136" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades to 75 cm long, 1.5-6 cm wide.</text>
      <biological_entity id="o29137" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s3" to="75" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Involucres usually 8-12 mm, varying in color.</text>
      <biological_entity id="o29138" name="involucre" name_original="involucres" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="varying" value_original="varying" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Lower glumes of functional pistillate spikelets 6-10 mm, hyaline below, 5-7-veined, with a 1-3 mm coriaceous beak.</text>
      <biological_entity constraint="functional" id="o29140" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="pistillate" value_original="pistillate" />
      </biological_entity>
      <biological_entity id="o29141" name="beak" name_original="beak" src="d0_s5" type="structure">
        <character is_modifier="true" name="texture" src="d0_s5" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <relation from="o29139" id="r4972" name="part_of" negation="false" src="d0_s5" to="o29140" />
    </statement>
    <statement id="d0_s6">
      <text>Staminate rames 10-35 mm, with 3-25 spikelet pairs, disarticulating at maturity;</text>
      <biological_entity constraint="lower" id="o29139" name="glume" name_original="glumes" src="d0_s5" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s5" to="10" to_unit="mm" />
        <character is_modifier="false" modifier="below" name="coloration" src="d0_s5" value="hyaline" value_original="hyaline" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="5-7-veined" value_original="5-7-veined" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="with" name="some_measurement" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o29142" name="spikelet" name_original="spikelet" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="35" to_unit="mm" />
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s6" to="25" />
        <character constraint="at maturity" is_modifier="false" name="architecture" src="d0_s6" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>spikelets 5-9 mm, dorsally compressed;</text>
      <biological_entity id="o29143" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>glumes exceeding the florets, with 15+ veins;</text>
      <biological_entity id="o29144" name="glume" name_original="glumes" src="d0_s8" type="structure" />
      <biological_entity id="o29145" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <biological_entity id="o29146" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character char_type="range_value" from="15" is_modifier="true" name="quantity" src="d0_s8" upper_restricted="false" />
      </biological_entity>
      <relation from="o29144" id="r4973" name="exceeding the" negation="false" src="d0_s8" to="o29145" />
      <relation from="o29144" id="r4974" name="with" negation="false" src="d0_s8" to="o29146" />
    </statement>
    <statement id="d0_s9">
      <text>lower glumes elliptic to obovate, somewhat asymmetrical, margins folded inward, apices obtuse;</text>
      <biological_entity constraint="lower" id="o29147" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s9" to="obovate" />
        <character is_modifier="false" modifier="somewhat" name="architecture_or_shape" src="d0_s9" value="asymmetrical" value_original="asymmetrical" />
      </biological_entity>
      <biological_entity id="o29148" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity id="o29149" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes lanceolate to narrowly elliptic, keels often winged, apices acute;</text>
      <biological_entity constraint="upper" id="o29150" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="narrowly elliptic" />
      </biological_entity>
      <biological_entity id="o29151" name="keel" name_original="keels" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s10" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o29152" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper lemmas 5-8 mm, hyaline, elliptic to ovate, 3-veined;</text>
      <biological_entity constraint="upper" id="o29153" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="hyaline" value_original="hyaline" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s11" to="ovate" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper paleas similar but 2-veined;</text>
      <biological_entity constraint="upper" id="o29154" name="palea" name_original="paleas" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-veined" value_original="2-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers 3-6 mm. 2n = 20.</text>
      <biological_entity id="o29155" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s13" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29156" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Coix lacryma-jobi is a tall, maize-like plant. In North America, it is usually grown as an ornamental, but it has become established at scattered locations in the Flora region. The involucres, which can be used as beads, may be white, blue, pink, straw, gray, brown, or black, with the color being distributed evenly, irregularly, or in stripes. Cultivars with easily removed involucres are grown for food and beverage, especially in Asia.</discussion>
  
</bio:treatment>