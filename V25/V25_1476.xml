<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">590</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="Michx." date="unknown" rank="species">setaceum</taxon_name>
    <taxon_name authority="(Leconte) Alph. Wood" date="unknown" rank="variety">longepedunculatum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species setaceum;variety longepedunculatum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Paspalum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">longepedunculatum</taxon_name>
    <taxon_hierarchy>genus paspalum;species longepedunculatum</taxon_hierarchy>
  </taxon_identification>
  <number>NA</number>
  <other_name type="common_name">Barestem paspalum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect.</text>
      <biological_entity id="o8909" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves mostly basal;</text>
      <biological_entity id="o8910" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o8911" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades to 15 cm long, 3-10 mm wide, glabrous, usually recurved, yellow-green, margins ciliate.</text>
      <biological_entity id="o8912" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s2" to="10" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="recurved" value_original="recurved" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellow-green" value_original="yellow-green" />
      </biological_entity>
      <biological_entity id="o8913" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s2" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Panicle branches 2.4-9.2 cm long, ascending or nodding, arcuate;</text>
      <biological_entity constraint="panicle" id="o8914" name="branch" name_original="branches" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.4" from_unit="cm" name="length" src="d0_s3" to="9.2" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="nodding" value_original="nodding" />
        <character is_modifier="false" name="course_or_shape" src="d0_s3" value="arcuate" value_original="arcuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branch axes 0.2-0.6 mm wide.</text>
      <biological_entity constraint="branch" id="o8915" name="axis" name_original="axes" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 1.4-1.8 mm long, 0.9-1.3 mm wide, elliptic to obovate or suborbicular, glabrous or with scattered glandular-hairs, not spotted;</text>
      <biological_entity id="o8916" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s5" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="width" src="d0_s5" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="obovate or suborbicular" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="with scattered glandular-hairs" value_original="with scattered glandular-hairs" />
        <character is_modifier="false" modifier="not" name="coloration" notes="" src="d0_s5" value="spotted" value_original="spotted" />
      </biological_entity>
      <biological_entity id="o8917" name="glandular-hair" name_original="glandular-hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o8916" id="r1455" name="with" negation="false" src="d0_s5" to="o8917" />
    </statement>
    <statement id="d0_s6">
      <text>lower lemmas without an evident midvein;</text>
      <biological_entity constraint="lower" id="o8918" name="lemma" name_original="lemmas" src="d0_s6" type="structure" />
      <biological_entity id="o8919" name="midvein" name_original="midvein" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o8918" id="r1456" name="without" negation="false" src="d0_s6" to="o8919" />
    </statement>
    <statement id="d0_s7">
      <text>upper florets 1.4-1.8 mm.</text>
      <biological_entity constraint="upper" id="o8920" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s7" to="1.8" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Paspalum setaceum var. longepedunculatum grows on open ground, usually in moist areas such as along ditches and roadsides, as well as in flatwoods. It is found primarily in the coastal plain of the southeastern United States, but has also been found in Ohio, Kentucky, and Tennessee. It is similar to var. villosissimum, differing in its glabrous leaves, more delicate habit, and more poorly-developed rhizomes. Both varieties grow in peninsular Florida, but var. longepedunculatum also grows along the coast as far west as the Mississippi delta and as far north as southern North Carolina.</discussion>
  
</bio:treatment>