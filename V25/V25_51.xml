<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">34</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Roem. &amp; Schult." date="unknown" rank="genus">TRIDENS</taxon_name>
    <taxon_name authority="(Torr.) Nash" date="unknown" rank="species">muticus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus tridens;species muticus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Triodia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">mutica</taxon_name>
    <taxon_hierarchy>genus triodia;species mutica</taxon_hierarchy>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Slim tridens</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants cespitose, with knotty, shortly rhizomatous bases.</text>
      <biological_entity id="o18246" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18247" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="shape" src="d0_s0" value="knotty" value_original="knotty" />
        <character is_modifier="true" modifier="shortly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
      <relation from="o18246" id="r3071" name="with" negation="false" src="d0_s0" to="o18247" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-80 cm;</text>
      <biological_entity id="o18248" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes often with soft, 1-2 mm hairs.</text>
      <biological_entity id="o18249" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o18250" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s2" value="soft" value_original="soft" />
        <character char_type="range_value" from="1" from_unit="mm" is_modifier="true" name="some_measurement" src="d0_s2" to="2" to_unit="mm" />
      </biological_entity>
      <relation from="o18249" id="r3072" name="with" negation="false" src="d0_s2" to="o18250" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths rounded, lower sheaths often strigose or pilose, upper sheaths glabrous or scabrous;</text>
      <biological_entity id="o18251" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18252" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity constraint="upper" id="o18253" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.5-1 mm, membranous, ciliate;</text>
      <biological_entity id="o18254" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 1-4 mm wide, usually involute or loosely infolded, glabrous, scabrous, or sparsely pilose, attenuate distally.</text>
      <biological_entity id="o18255" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="loosely" name="shape" src="d0_s5" value="infolded" value_original="infolded" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s5" value="attenuate" value_original="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles 7-20 (25) cm long, 0.3-0.8 cm wide;</text>
      <biological_entity id="o18256" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character name="length" src="d0_s6" unit="cm" value="25" value_original="25" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s6" to="0.8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches erect, spikelets imbricate but usually not crowded;</text>
      <biological_entity id="o18257" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o18258" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s7" value="imbricate" value_original="imbricate" />
        <character is_modifier="false" modifier="usually not" name="arrangement" src="d0_s7" value="crowded" value_original="crowded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>pedicels 1-2 mm.</text>
      <biological_entity id="o18259" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 8-13 mm, with 5-11 florets.</text>
      <biological_entity id="o18260" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s9" to="13" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18261" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s9" to="11" />
      </biological_entity>
      <relation from="o18260" id="r3073" name="with" negation="false" src="d0_s9" to="o18261" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes glabrous, usually purple-tinged;</text>
      <biological_entity id="o18262" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes 3-8 (10) mm, 1-3-veined;</text>
      <biological_entity constraint="lower" id="o18263" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character name="atypical_some_measurement" src="d0_s11" unit="mm" value="10" value_original="10" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 4-10 mm, 1-7-veined;</text>
      <biological_entity constraint="upper" id="o18264" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="10" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="1-7-veined" value_original="1-7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas 3.5-7 mm, usually purple-tinged, midveins pilose on the basal 1/3 - 1/2, rarely excurrent, lateral-veins pilose to well above midlength, never excurrent;</text>
      <biological_entity id="o18265" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s13" value="purple-tinged" value_original="purple-tinged" />
      </biological_entity>
      <biological_entity id="o18266" name="midvein" name_original="midveins" src="d0_s13" type="structure">
        <character constraint="on lateral-veins" constraintid="o18267" is_modifier="false" name="pubescence" src="d0_s13" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="never" name="architecture" notes="" src="d0_s13" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o18267" name="lateral-vein" name_original="lateral-veins" src="d0_s13" type="structure">
        <character is_modifier="true" name="position" src="d0_s13" value="basal" value_original="basal" />
        <character char_type="range_value" from="1/3" is_modifier="true" name="quantity" src="d0_s13" to="1/2" />
        <character is_modifier="true" modifier="rarely" name="architecture" src="d0_s13" value="excurrent" value_original="excurrent" />
        <character constraint="well above midlength" constraintid="o18268" is_modifier="false" name="pubescence" src="d0_s13" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o18268" name="midlength" name_original="midlength" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>paleas 1-2 mm shorter than the lemmas, margins pubescent;</text>
      <biological_entity id="o18269" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="2" to_unit="mm" />
        <character constraint="than the lemmas" constraintid="o18270" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o18270" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity id="o18271" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 1-1.5 mm.</text>
      <biological_entity id="o18272" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 1.5-2.3 mm. 2n = 40.</text>
      <biological_entity id="o18273" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s16" to="2.3" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18274" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Kans.;Okla.;Colo.;N.Mex.;Tex.;La.;Utah;Calif.;Ark.;Ariz.;Mo.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Upper glumes 4-5(6) mm long, 1-veined</description>
      <determination>Tridens muticus var. muticus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Upper glumes usually 5.5-10 mm long, 3-7-veined</description>
      <determination>Tridens muticus var. elongatus</determination>
    </key_statement>
  </key>
</bio:treatment>