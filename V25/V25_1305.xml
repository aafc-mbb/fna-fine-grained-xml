<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>J.K. Wipff; Rahmona A. Thompson;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">488</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Veldkamp" date="unknown" rank="genus">MOOROCHLOA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus moorochloa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">BRACHIARIA</taxon_name>
    <taxon_hierarchy>genus brachiaria</taxon_hierarchy>
  </taxon_identification>
  <number>25.11</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o18203" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 10-60 cm, herbaceous, not woody, often creeping.</text>
      <biological_entity id="o18204" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves cauline;</text>
      <biological_entity id="o18205" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>sheaths open, glabrous or pubescent;</text>
      <biological_entity id="o18206" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, with a ciliate fringe, fringe longer than the membranous base.</text>
      <biological_entity id="o18207" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o18208" name="fringe" name_original="fringe" src="d0_s4" type="structure">
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o18209" name="fringe" name_original="fringe" src="d0_s4" type="structure">
        <character constraint="than the membranous base" constraintid="o18210" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o18210" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="true" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o18207" id="r3062" name="with" negation="false" src="d0_s4" to="o18208" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, secund panicles of 1-sided branches;</text>
      <biological_entity id="o18211" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o18212" name="panicle" name_original="panicles" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="secund" value_original="secund" />
      </biological_entity>
      <biological_entity id="o18213" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <relation from="o18212" id="r3063" name="part_of" negation="false" src="d0_s5" to="o18213" />
    </statement>
    <statement id="d0_s6">
      <text>branches erect to ascending, axes triquetrous, terminating in a well-developed spikelet;</text>
      <biological_entity id="o18214" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s6" to="ascending" />
      </biological_entity>
      <biological_entity id="o18215" name="axis" name_original="axes" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="triquetrous" value_original="triquetrous" />
      </biological_entity>
      <biological_entity id="o18216" name="spikelet" name_original="spikelet" src="d0_s6" type="structure">
        <character is_modifier="true" name="development" src="d0_s6" value="well-developed" value_original="well-developed" />
      </biological_entity>
      <relation from="o18215" id="r3064" name="terminating in a" negation="false" src="d0_s6" to="o18216" />
    </statement>
    <statement id="d0_s7">
      <text>secondary branches, when present, shorter than the primary branches;</text>
      <biological_entity constraint="primary" id="o18218" name="branch" name_original="branches" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation below the glumes and beneath the upper florets.</text>
      <biological_entity constraint="secondary" id="o18217" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character constraint="than the primary branches" constraintid="o18218" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="upper" id="o18219" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <relation from="o18217" id="r3065" name="below the glumes and beneath" negation="false" src="d0_s8" to="o18219" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets solitary, subsessile, dorsally compressed, unequally convex, in 2 rows, the lower glumes and lemmas appressed or adjacent to the branch axes, with 2 florets;</text>
      <biological_entity id="o18220" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="unequally" name="shape" src="d0_s9" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity id="o18221" name="row" name_original="rows" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18222" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character constraint="to branch axes" constraintid="o18224" is_modifier="false" name="arrangement" src="d0_s9" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o18223" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character constraint="to branch axes" constraintid="o18224" is_modifier="false" name="arrangement" src="d0_s9" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity constraint="branch" id="o18224" name="axis" name_original="axes" src="d0_s9" type="structure" />
      <biological_entity id="o18225" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="2" value_original="2" />
      </biological_entity>
      <relation from="o18220" id="r3066" name="in" negation="false" src="d0_s9" to="o18221" />
      <relation from="o18222" id="r3067" name="with" negation="false" src="d0_s9" to="o18225" />
      <relation from="o18223" id="r3068" name="with" negation="false" src="d0_s9" to="o18225" />
    </statement>
    <statement id="d0_s10">
      <text>lower florets sterile or staminate;</text>
      <biological_entity constraint="lower" id="o18226" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper florets stipitate, bisexual, usually glabrous, readily disarticulating, acuminate.</text>
      <biological_entity constraint="upper" id="o18227" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s11" value="stipitate" value_original="stipitate" />
        <character is_modifier="false" name="reproduction" src="d0_s11" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="readily" name="architecture" src="d0_s11" value="disarticulating" value_original="disarticulating" />
        <character is_modifier="false" name="shape" src="d0_s11" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Lower glumes to 0.5 mm, less than 1/2 as long as the spikelets, glabrous, adjacent to the branch axes, 0-1-veined;</text>
      <biological_entity constraint="lower" id="o18228" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="0.5" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o18229" from="0" name="quantity" src="d0_s12" to="1/2" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character constraint="to branch axes" constraintid="o18230" is_modifier="false" name="arrangement" src="d0_s12" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="0-1-veined" value_original="0-1-veined" />
      </biological_entity>
      <biological_entity id="o18229" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
      <biological_entity constraint="branch" id="o18230" name="axis" name_original="axes" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper glumes and lower lemmas subequal, villous, 3-5-veined;</text>
      <biological_entity constraint="upper" id="o18231" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o18232" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="size" src="d0_s13" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="villous" value_original="villous" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>upper glumes subequal to or slightly exceeding the upper florets, not saccate;</text>
      <biological_entity constraint="upper" id="o18233" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
        <character is_modifier="false" modifier="not" name="architecture_or_shape" src="d0_s14" value="saccate" value_original="saccate" />
      </biological_entity>
      <biological_entity constraint="upper" id="o18234" name="floret" name_original="florets" src="d0_s14" type="structure" />
      <relation from="o18233" id="r3069" modifier="slightly" name="exceeding the" negation="false" src="d0_s14" to="o18234" />
    </statement>
    <statement id="d0_s15">
      <text>lower paleas present;</text>
      <biological_entity constraint="lower" id="o18235" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers (if present) 3;</text>
      <biological_entity id="o18236" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper lemmas equaling the second-glume, glabrous, indurate, smooth, shiny to lustrous, 5-veined or 7-veined, margins involute, apices round to muticous;</text>
      <biological_entity constraint="upper" id="o18237" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="false" name="pubescence" notes="" src="d0_s17" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="texture" src="d0_s17" value="indurate" value_original="indurate" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s17" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="shiny" name="reflectance" src="d0_s17" to="lustrous" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="5-veined" value_original="5-veined" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="7-veined" value_original="7-veined" />
      </biological_entity>
      <biological_entity id="o18238" name="second-glume" name_original="second-glume" src="d0_s17" type="structure">
        <character is_modifier="true" name="variability" src="d0_s17" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o18239" name="margin" name_original="margins" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape_or_vernation" src="d0_s17" value="involute" value_original="involute" />
      </biological_entity>
      <biological_entity id="o18240" name="apex" name_original="apices" src="d0_s17" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s17" to="muticous" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>upper paleas similar to the upper lemmas;</text>
      <biological_entity constraint="upper" id="o18241" name="palea" name_original="paleas" src="d0_s18" type="structure" />
      <biological_entity constraint="upper" id="o18242" name="lemma" name_original="lemmas" src="d0_s18" type="structure" />
      <relation from="o18241" id="r3070" name="to" negation="false" src="d0_s18" to="o18242" />
    </statement>
    <statement id="d0_s19">
      <text>anthers 3.</text>
      <biological_entity id="o18243" name="anther" name_original="anthers" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Caryopses ovoid, dorsally compressed, x = 9.</text>
      <biological_entity id="o18244" name="caryopsis" name_original="caryopses" src="d0_s20" type="structure">
        <character is_modifier="false" name="shape" src="d0_s20" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s20" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="x" id="o18245" name="chromosome" name_original="" src="d0_s20" type="structure">
        <character name="quantity" src="d0_s20" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Moorochloa, as now interpreted, includes three species, all native to the Eastern Hemisphere. It differs from Urochloa in its smooth, rounded, distal floret and from Panicum in its secund panicle and stipitate, shiny to lustrous, disarticulating distal floret. Many of the species previously placed in the genus are now placed in Urochloa. One species is established in the Flora region.</discussion>
  <references>
    <reference>Morrone, O. and F.O. Zuloaga. 1992. Revision de las especies Sudamericanas nativas e introducisas de los generos Brachiaria y Urochloa (Poaceae: Panicoideae: Paniceae). Darwiniana 31:43-109.</reference>
  </references>
  
</bio:treatment>