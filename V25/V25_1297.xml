<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">485</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PANICUM</taxon_name>
    <taxon_name authority="(Raf.) Pilg." date="unknown" rank="subgenus">Phanopyrum</taxon_name>
    <taxon_name authority="Stapf" date="unknown" rank="section">Monticola</taxon_name>
    <taxon_name authority="Sw." date="unknown" rank="species">trichoides</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus phanopyrum;section monticola;species trichoides</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>31</number>
  <other_name type="common_name">Small-flowered panicgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o9294" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 15-100 cm tall, 0.5-1 (2) mm thick, sprawling to erect, without cormlike bases, freely branching and rooting from the lower nodes;</text>
      <biological_entity id="o9295" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="height" src="d0_s1" to="100" to_unit="cm" />
        <character name="thickness" src="d0_s1" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="thickness" src="d0_s1" to="1" to_unit="mm" />
        <character char_type="range_value" from="sprawling" name="orientation" src="d0_s1" to="erect" />
        <character is_modifier="false" modifier="freely" name="architecture" notes="" src="d0_s1" value="branching" value_original="branching" />
        <character constraint="from lower nodes" constraintid="o9297" is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity id="o9296" name="base" name_original="bases" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="cormlike" value_original="cormlike" />
      </biological_entity>
      <biological_entity constraint="lower" id="o9297" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <relation from="o9295" id="r1537" name="without" negation="false" src="d0_s1" to="o9296" />
    </statement>
    <statement id="d0_s2">
      <text>nodes prominent, glabrous or pubescent;</text>
      <biological_entity id="o9298" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes not succulent, pilose.</text>
      <biological_entity id="o9299" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s3" value="succulent" value_original="succulent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths shorter than the internodes, rounded, hairs papillose-based;</text>
      <biological_entity id="o9300" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o9301" is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o9301" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity id="o9302" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>collars pilose;</text>
      <biological_entity id="o9303" name="collar" name_original="collars" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ligules 0.2-0.5 mm;</text>
      <biological_entity id="o9304" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>blades 2-7 cm long, 5-20 mm wide, 4-6 times longer than wide, lanceolate, thin, flat, sparsely to densely pilose, hairs papillose-based, bases asymmetrically cordate to subcordate, lower margins ciliate, papillose.</text>
      <biological_entity id="o9305" name="blade" name_original="blades" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s7" to="7" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s7" to="20" to_unit="mm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s7" value="4-6" value_original="4-6" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="width" src="d0_s7" value="thin" value_original="thin" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s7" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="sparsely to densely" name="pubescence" src="d0_s7" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o9306" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o9307" name="base" name_original="bases" src="d0_s7" type="structure">
        <character char_type="range_value" from="asymmetrically cordate" name="shape" src="d0_s7" to="subcordate" />
      </biological_entity>
      <biological_entity constraint="lower" id="o9308" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
        <character is_modifier="false" name="relief" src="d0_s7" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Panicles 4-24 cm, almost as wide as long, diffuse, partially included or exerted;</text>
      <biological_entity id="o9309" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s8" to="24" to_unit="cm" />
        <character is_modifier="false" modifier="almost" name="length_or_size" src="d0_s8" value="long" value_original="long" />
        <character is_modifier="false" name="density" src="d0_s8" value="diffuse" value_original="diffuse" />
        <character is_modifier="false" modifier="partially" name="position" src="d0_s8" value="included" value_original="included" />
        <character name="position" src="d0_s8" value="exerted" value_original="exerted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>primary branches to 10 cm, alternate, ascending to reflexed, branching in the distal 2/3;</text>
      <biological_entity constraint="primary" id="o9310" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s9" to="10" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="alternate" value_original="alternate" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s9" to="reflexed" />
        <character constraint="in distal 2/3" constraintid="o9311" is_modifier="false" name="architecture" src="d0_s9" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="distal" id="o9311" name="2/3" name_original="2/3" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>pedicels 9-20 mm, threadlike.</text>
      <biological_entity id="o9312" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s10" to="20" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s10" value="thread-like" value_original="threadlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 1-1.4 mm long, 0.5-0.6 mm wide, not secund, lanceoloid to narrowly ovoid, planoconvex in side view, sparsely pubescent.</text>
      <biological_entity id="o9313" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s11" to="1.4" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s11" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="secund" value_original="secund" />
        <character char_type="range_value" from="lanceoloid" name="shape" src="d0_s11" to="narrowly ovoid" />
        <character is_modifier="false" modifier="in side view" name="shape" src="d0_s11" value="planoconvex" value_original="planoconvex" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Lower glumes 0.4-0.8 mm, 1/3 – 1/2 as long as the spikelets, 1-3-veined, subacute;</text>
      <biological_entity constraint="lower" id="o9314" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s12" to="0.8" to_unit="mm" />
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o9315" from="1/3" name="quantity" src="d0_s12" to="1/2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="1-3-veined" value_original="1-3-veined" />
        <character is_modifier="false" name="shape" src="d0_s12" value="subacute" value_original="subacute" />
      </biological_entity>
      <biological_entity id="o9315" name="spikelet" name_original="spikelets" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>upper glumes 0.8-1.2 mm, arising 0.2 mm above the lower glumes, 3-5-veined;</text>
      <biological_entity constraint="upper" id="o9316" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s13" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s13" value="arising" value_original="arising" />
        <character constraint="above lower glumes" constraintid="o9317" name="some_measurement" src="d0_s13" unit="mm" value="0.2" value_original="0.2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s13" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity constraint="lower" id="o9317" name="glume" name_original="glumes" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o9318" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s14" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lower lemmas 0.1-0.2 mm longer than the upper glumes, 3-5-veined;</text>
      <biological_entity constraint="lower" id="o9319" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s15" to="0.2" to_unit="mm" />
        <character constraint="than the upper glumes" constraintid="o9320" is_modifier="false" name="length_or_size" src="d0_s15" value="longer" value_original="longer" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-5-veined" value_original="3-5-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o9320" name="glume" name_original="glumes" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>lower paleas 0.5-0.8 mm, hyaline;</text>
      <biological_entity constraint="lower" id="o9321" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s16" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper florets 0.8-1.2 mm long, 0.4-0.6 mm wide, finely rugose, lemmas strongly convex.</text>
      <biological_entity constraint="upper" id="o9322" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s17" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s17" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s17" value="rugose" value_original="rugose" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>2n = 18.</text>
      <biological_entity id="o9323" name="lemma" name_original="lemmas" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s17" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9324" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Panicum trichoides grows in moist, often weedy fields, woodlands, and savannahs of Mexico, Central and tropical America, and the Caribbean. It has been found, as a weed, in Brownsville and Austin, Texas, and is probably introduced to the Flora region. It has also been introduced into Africa, tropical Asia, and the Pacific islands. In the Flora region, it flowers from August through October.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Virgin Islands;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>