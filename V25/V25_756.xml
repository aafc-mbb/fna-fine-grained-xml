<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">181</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="(Muhl.) Fernald" date="unknown" rank="species">uniflora</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species uniflora</taxon_hierarchy>
  </taxon_identification>
  <number>42</number>
  <other_name type="common_name">Bog muhly</other_name>
  <other_name type="common_name">Muhlenbergie uniflore</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>loosely matted.</text>
      <biological_entity id="o11014" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="loosely" name="growth_form" src="d0_s1" value="matted" value_original="matted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-45 cm, arising from the bases of old depressed culms, compressed-keeled, developing branches below the lower leaf nodes;</text>
      <biological_entity id="o11015" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="45" to_unit="cm" />
        <character constraint="from bases" constraintid="o11016" is_modifier="false" name="orientation" src="d0_s2" value="arising" value_original="arising" />
        <character is_modifier="false" name="shape" notes="" src="d0_s2" value="compressed-keeled" value_original="compressed-keeled" />
      </biological_entity>
      <biological_entity id="o11016" name="base" name_original="bases" src="d0_s2" type="structure" />
      <biological_entity id="o11017" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="old" value_original="old" />
        <character is_modifier="true" name="shape" src="d0_s2" value="depressed" value_original="depressed" />
      </biological_entity>
      <biological_entity id="o11018" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="development" src="d0_s2" value="developing" value_original="developing" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o11019" name="node" name_original="nodes" src="d0_s2" type="structure" constraint_original="lower leaf" />
      <relation from="o11016" id="r1807" name="part_of" negation="false" src="d0_s2" to="o11017" />
      <relation from="o11018" id="r1808" name="below" negation="false" src="d0_s2" to="o11019" />
    </statement>
    <statement id="d0_s3">
      <text>internodes mostly glabrous, sometimes minutely puberulent below the nodes.</text>
      <biological_entity id="o11020" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character constraint="below nodes" constraintid="o11021" is_modifier="false" modifier="sometimes minutely" name="pubescence" src="d0_s3" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o11021" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths longer than the internodes, keeled, keels scabridulous, not becoming papery or spirally coiled when old;</text>
      <biological_entity id="o11022" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character constraint="than the internodes" constraintid="o11023" is_modifier="false" name="length_or_size" src="d0_s4" value="longer" value_original="longer" />
        <character is_modifier="false" name="shape" src="d0_s4" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o11023" name="internode" name_original="internodes" src="d0_s4" type="structure" />
      <biological_entity id="o11024" name="keel" name_original="keels" src="d0_s4" type="structure">
        <character is_modifier="false" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="not becoming" name="texture" src="d0_s4" value="papery" value_original="papery" />
        <character is_modifier="false" modifier="when old" name="architecture" src="d0_s4" value="coiled" value_original="coiled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 0.5-1.5 mm, membranous, truncate to obtuse, erose, without lateral lobes;</text>
      <biological_entity id="o11025" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="architecture_or_relief" src="d0_s5" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o11026" name="lobe" name_original="lobes" src="d0_s5" type="structure" />
      <relation from="o11025" id="r1809" name="without" negation="false" src="d0_s5" to="o11026" />
    </statement>
    <statement id="d0_s6">
      <text>blades 1-15 cm long, 0.8-2 mm wide, flat to conduplicate, smooth or scabridulous abaxially, hirtellous adaxially, midveins thickened and whitish proximally.</text>
      <biological_entity id="o11027" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s6" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="arrangement_or_vernation" src="d0_s6" value="conduplicate" value_original="conduplicate" />
        <character is_modifier="false" name="relief" src="d0_s6" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="abaxially" name="relief" src="d0_s6" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s6" value="hirtellous" value_original="hirtellous" />
      </biological_entity>
      <biological_entity id="o11028" name="midvein" name_original="midveins" src="d0_s6" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s6" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s6" value="whitish" value_original="whitish" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 2-20 cm long, (0.2) 2.5-6 cm wide, diffuse;</text>
      <biological_entity id="o11029" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s7" to="20" to_unit="cm" />
        <character name="width" src="d0_s7" unit="cm" value="0.2" value_original="0.2" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s7" to="6" to_unit="cm" />
        <character is_modifier="false" name="density" src="d0_s7" value="diffuse" value_original="diffuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches 0.4-6 cm long, about 0.1 mm thick, ascending, diverging 10-60° from the rachises, naked basally;</text>
      <biological_entity constraint="primary" id="o11030" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.4" from_unit="cm" name="length" src="d0_s8" to="6" to_unit="cm" />
        <character name="thickness" src="d0_s8" unit="mm" value="0.1" value_original="0.1" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
        <character constraint="from rachises" constraintid="o11031" is_modifier="false" modifier="10-60°" name="orientation" src="d0_s8" value="diverging" value_original="diverging" />
        <character is_modifier="false" modifier="basally" name="architecture" notes="" src="d0_s8" value="naked" value_original="naked" />
      </biological_entity>
      <biological_entity id="o11031" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>pedicels 0.2-7 mm, glabrous.</text>
      <biological_entity id="o11032" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="7" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 1.3-2.1 mm, dark purplish to plumbeous, occasionally with 2 florets.</text>
      <biological_entity id="o11033" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s10" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="dark purplish" name="coloration" src="d0_s10" to="plumbeous" />
      </biological_entity>
      <biological_entity id="o11034" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <relation from="o11033" id="r1810" modifier="occasionally" name="with" negation="false" src="d0_s10" to="o11034" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes equal, 0.4-1.3 mm, glabrous, 1-veined, apices scabrous, acute to obtuse, sometimes erose or notched, unawned;</text>
      <biological_entity id="o11035" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="variability" src="d0_s11" value="equal" value_original="equal" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="1.3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o11036" name="apex" name_original="apices" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s11" value="scabrous" value_original="scabrous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="obtuse" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
        <character is_modifier="false" name="shape" src="d0_s11" value="notched" value_original="notched" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 1.2-2 mm, oblongelliptic, dark purplish to plumbeous, glabrous, faintly 3-veined, apices acute to obtuse, unawned;</text>
      <biological_entity id="o11037" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s12" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s12" value="oblongelliptic" value_original="oblongelliptic" />
        <character char_type="range_value" from="dark purplish" name="coloration" src="d0_s12" to="plumbeous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="faintly" name="architecture" src="d0_s12" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o11038" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s12" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleas 1.3-2.1 mm, oblongelliptic, glabrous, acute to obtuse;</text>
      <biological_entity id="o11039" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s13" to="2.1" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="oblongelliptic" value_original="oblongelliptic" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s13" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers 0.6-0.9 mm, dark purple.</text>
      <biological_entity id="o11040" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s14" to="0.9" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="dark purple" value_original="dark purple" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 0.6-0.8 mm, ovoid, brownish.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = 42.</text>
      <biological_entity id="o11041" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s15" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="brownish" value_original="brownish" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11042" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="42" value_original="42" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia uniflora grows in bogs, wet meadows, and lake shores in sandy or peaty, often acidic, soils, at elevations of 0-650 m. It is native to eastern North America, but was collected once in British Columbia, probably having been introduced from ship ballast, and was recently collected from a commercial cranberry bog in Oregon. The collection from Texas may also be an introduction.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Maine;Md.;N.J.;Conn.;N.Y.;Mass.;N.H.;Minn.;Mich.;Wis.;R.I.;Pa.;Vt.;B.C.;N.B.;Nfld. and Labr.;N.S.;Ont.;Que.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>