<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">108</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Hitchc." date="unknown" rank="genus">VASEYOCHLOA</taxon_name>
    <taxon_name authority="(Vasey) Hitchc." date="unknown" rank="species">multinervosa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus vaseyochloa;species multinervosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Texasgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms 60-110 cm.</text>
      <biological_entity id="o9994" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s0" to="110" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Ligules 1-3 mm;</text>
      <biological_entity id="o9995" name="ligule" name_original="ligules" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 16-35 cm long, 1-6 mm wide, glabrous, lower blades usually folded, upper blades flat.</text>
      <biological_entity id="o9996" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character char_type="range_value" from="16" from_unit="cm" name="length" src="d0_s2" to="35" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o9997" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s2" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity constraint="upper" id="o9998" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s2" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Panicles 10-30 cm long, 1-3 cm wide;</text>
      <biological_entity id="o9999" name="panicle" name_original="panicles" src="d0_s3" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>branches 4-13 cm, ascending or the lower branches occasionally spreading, each axil with a tuft of hairs.</text>
      <biological_entity id="o10000" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s4" to="13" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="lower" id="o10001" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="occasionally" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o10002" name="axil" name_original="axil" src="d0_s4" type="structure" />
      <biological_entity id="o10003" name="tuft" name_original="tuft" src="d0_s4" type="structure" />
      <biological_entity id="o10004" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <relation from="o10002" id="r1639" name="with" negation="false" src="d0_s4" to="o10003" />
      <relation from="o10003" id="r1640" name="part_of" negation="false" src="d0_s4" to="o10004" />
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 10-16 mm long, 2.5-5 mm wide.</text>
      <biological_entity id="o10005" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s5" to="16" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Lower glumes 2.5-4 mm, 1-7-veined;</text>
      <biological_entity constraint="lower" id="o10006" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="1-7-veined" value_original="1-7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>upper glumes 4-5.5 mm, 5-9-veined;</text>
      <biological_entity constraint="upper" id="o10007" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5.5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="5-9-veined" value_original="5-9-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lemmas 5-7 mm;</text>
      <biological_entity id="o10008" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>anthers 3, 0.5-2 mm. 2n = 56, 60, 68.</text>
      <biological_entity id="o10009" name="anther" name_original="anthers" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" unit=",0.5-2 mm" />
        <character name="some_measurement" src="d0_s9" unit=",0.5-2 mm" value="3" value_original="3" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10010" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="56" value_original="56" />
        <character name="quantity" src="d0_s9" value="60" value_original="60" />
        <character name="quantity" src="d0_s9" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Vaseyochloa multinervosa grows in islands of live oaks within rolling sand dunes on the Texas mainland, North Padre Island, and on naturally occurring islands in the Laguna Madre of Texas.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>