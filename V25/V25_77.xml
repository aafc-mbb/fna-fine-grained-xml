<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Jesus Valdes-Reyna;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">48</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Hack." date="unknown" rank="genus">BLEPHARIDACHNE</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus blepharidachne</taxon_hierarchy>
  </taxon_identification>
  <number>17.17</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial (rarely annual);</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, from a knotty base, often mat-forming.</text>
      <biological_entity id="o14535" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o14536" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="true" name="shape" src="d0_s1" value="knotty" value_original="knotty" />
        <character is_modifier="false" modifier="often" name="growth_form" src="d0_s1" value="mat-forming" value_original="mat-forming" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 3-8 (20) cm, often decumbent and rooting at the lower nodes, frequently branched above the bases, forming short spur shoots at the ends of long internodes;</text>
      <biological_entity id="o14537" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
        <character is_modifier="false" modifier="often" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o14538" is_modifier="false" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character constraint="above bases" constraintid="o14539" is_modifier="false" modifier="frequently" name="architecture" notes="" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity constraint="lower" id="o14538" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o14539" name="base" name_original="bases" src="d0_s2" type="structure" />
      <biological_entity constraint="spur" id="o14540" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o14541" name="end" name_original="ends" src="d0_s2" type="structure" />
      <biological_entity id="o14542" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s2" value="long" value_original="long" />
      </biological_entity>
      <relation from="o14537" id="r2407" name="forming" negation="false" src="d0_s2" to="o14540" />
      <relation from="o14537" id="r2408" name="at" negation="false" src="d0_s2" to="o14541" />
      <relation from="o14541" id="r2409" name="part_of" negation="false" src="d0_s2" to="o14542" />
    </statement>
    <statement id="d0_s3">
      <text>internodes minutely pubescent.</text>
      <biological_entity id="o14543" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves clustered at the bases of the primary and spur shoots;</text>
      <biological_entity id="o14544" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="at bases" constraintid="o14545" is_modifier="false" name="arrangement_or_growth_form" src="d0_s4" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o14545" name="base" name_original="bases" src="d0_s4" type="structure" />
      <biological_entity id="o14546" name="primary" name_original="primary" src="d0_s4" type="structure" />
      <biological_entity constraint="spur" id="o14547" name="shoot" name_original="shoots" src="d0_s4" type="structure" />
      <relation from="o14545" id="r2410" name="part_of" negation="false" src="d0_s4" to="o14546" />
      <relation from="o14545" id="r2411" name="part_of" negation="false" src="d0_s4" to="o14547" />
    </statement>
    <statement id="d0_s5">
      <text>basal sheaths shorter than the internodes;</text>
      <biological_entity constraint="basal" id="o14548" name="sheath" name_original="sheaths" src="d0_s5" type="structure">
        <character constraint="than the internodes" constraintid="o14549" is_modifier="false" name="height_or_length_or_size" src="d0_s5" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o14549" name="internode" name_original="internodes" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>ligules of hairs or absent;</text>
      <biological_entity id="o14550" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o14551" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <relation from="o14550" id="r2412" name="consists_of" negation="false" src="d0_s6" to="o14551" />
    </statement>
    <statement id="d0_s7">
      <text>blades linear to triangular, convolute to conduplicate, or flat to plicate, sharp, those of the upper leaves usually exceeding the inflorescences.</text>
      <biological_entity id="o14552" name="blade" name_original="blades" src="d0_s7" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="triangular" />
        <character char_type="range_value" from="convolute" name="arrangement" src="d0_s7" to="conduplicate" />
        <character char_type="range_value" from="flat" name="shape" src="d0_s7" to="plicate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="sharp" value_original="sharp" />
      </biological_entity>
      <biological_entity constraint="upper" id="o14553" name="leaf" name_original="leaves" src="d0_s7" type="structure" />
      <biological_entity id="o14554" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <relation from="o14552" id="r2413" name="part_of" negation="false" src="d0_s7" to="o14553" />
      <relation from="o14552" id="r2414" modifier="usually" name="exceeding the" negation="false" src="d0_s7" to="o14554" />
    </statement>
    <statement id="d0_s8">
      <text>Inflorescences terminal, compact panicles, exserted or partially included in the upper sheath (s).</text>
      <biological_entity id="o14555" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s8" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o14556" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s8" value="compact" value_original="compact" />
        <character is_modifier="false" name="position" src="d0_s8" value="exserted" value_original="exserted" />
        <character constraint="in upper sheath" constraintid="o14557" is_modifier="false" modifier="partially" name="position" src="d0_s8" value="included" value_original="included" />
      </biological_entity>
      <biological_entity constraint="upper" id="o14557" name="sheath" name_original="sheath" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets laterally compressed, subsessile or pedicellate, with 4 florets per spikelet;</text>
      <biological_entity id="o14559" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o14560" name="spikelet" name_original="spikelet" src="d0_s9" type="structure" />
      <relation from="o14558" id="r2415" name="with" negation="false" src="d0_s9" to="o14559" />
      <relation from="o14559" id="r2416" name="per" negation="false" src="d0_s9" to="o14560" />
    </statement>
    <statement id="d0_s10">
      <text>disarticulation above the glumes but not between the florets.</text>
      <biological_entity id="o14558" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s9" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="subsessile" value_original="subsessile" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <biological_entity constraint="between florets" constraintid="o14562" id="o14561" name="glume" name_original="glumes" src="d0_s10" type="structure" constraint_original="between  florets, " />
      <biological_entity id="o14562" name="floret" name_original="florets" src="d0_s10" type="structure" />
      <relation from="o14558" id="r2417" name="above" negation="false" src="d0_s10" to="o14561" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes subequal to each other and the lowest lemma, rounded or weakly keeled, 1-veined, awn-tipped or unawned;</text>
      <biological_entity constraint="lowest" id="o14564" name="lemma" name_original="lemma" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lowest 2 florets in each spikelet staminate or sterile;</text>
      <biological_entity id="o14563" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="subequal" value_original="subequal" />
        <character is_modifier="false" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="weakly" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="awn-tipped" value_original="awn-tipped" />
        <character name="architecture" src="d0_s11" value="unawned" value_original="unawned" />
        <character is_modifier="false" name="position" src="d0_s12" value="lowest" value_original="lowest" />
      </biological_entity>
      <biological_entity id="o14566" name="spikelet" name_original="spikelet" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="staminate" value_original="staminate" />
      </biological_entity>
      <relation from="o14565" id="r2418" name="in" negation="false" src="d0_s12" to="o14566" />
    </statement>
    <statement id="d0_s13">
      <text>third floret pistillate or bisexual;</text>
      <biological_entity id="o14565" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="2" value_original="2" />
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o14567" name="floret" name_original="floret" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="pistillate" value_original="pistillate" />
        <character is_modifier="false" name="reproduction" src="d0_s13" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lemmas rounded on the back, 3-veined, mostly glabrous but pilose across the bases and on the margins, strongly 3-lobed, lateral lobes wider than the central lobes, all lobes ciliate on 1 or both margins, lower lemmas with the lateral lobes rounded or mucronate to awned, central lobes awned;</text>
      <biological_entity id="o14568" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character constraint="on back" constraintid="o14569" is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s14" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
        <character constraint="across the bases and on margins" constraintid="o14570" is_modifier="false" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
        <character is_modifier="false" modifier="strongly" name="shape" notes="" src="d0_s14" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o14569" name="back" name_original="back" src="d0_s14" type="structure" />
      <biological_entity id="o14570" name="margin" name_original="margins" src="d0_s14" type="structure" />
      <biological_entity constraint="lateral" id="o14571" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character constraint="than the central lobes" constraintid="o14572" is_modifier="false" name="width" src="d0_s14" value="wider" value_original="wider" />
      </biological_entity>
      <biological_entity constraint="central" id="o14572" name="lobe" name_original="lobes" src="d0_s14" type="structure" />
      <biological_entity id="o14573" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character constraint="on margins" constraintid="o14574" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s14" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o14574" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="lower" id="o14575" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
      <biological_entity constraint="lateral" id="o14576" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character char_type="range_value" from="mucronate" name="shape" src="d0_s14" to="awned" />
      </biological_entity>
      <relation from="o14575" id="r2419" name="with" negation="false" src="d0_s14" to="o14576" />
    </statement>
    <statement id="d0_s15">
      <text>third lemmas 3-lobed, lobes awned;</text>
      <biological_entity constraint="central" id="o14577" name="lobe" name_original="lobes" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o14578" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="3-lobed" value_original="3-lobed" />
      </biological_entity>
      <biological_entity id="o14579" name="lobe" name_original="lobes" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas from slightly shorter to slightly longer than the lemmas;</text>
      <biological_entity id="o14580" name="palea" name_original="paleas" src="d0_s16" type="structure" />
      <biological_entity id="o14582" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="slightly" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
        <character is_modifier="true" name="size_or_length" src="d0_s16" value="shorter to slightly" value_original="shorter to slightly" />
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o14581" name="lemma" name_original="lemmas" src="d0_s16" type="structure">
        <character is_modifier="true" modifier="slightly" name="height_or_length_or_size" src="d0_s16" value="shorter" value_original="shorter" />
        <character is_modifier="true" name="size_or_length" src="d0_s16" value="shorter to slightly" value_original="shorter to slightly" />
        <character is_modifier="true" modifier="slightly" name="length_or_size" src="d0_s16" value="longer" value_original="longer" />
      </biological_entity>
      <relation from="o14580" id="r2420" name="from" negation="false" src="d0_s16" to="o14582" />
    </statement>
    <statement id="d0_s17">
      <text>lodicules absent;</text>
      <biological_entity id="o14583" name="lodicule" name_original="lodicules" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>anthers 2 or 3 (rarely 1);</text>
      <biological_entity id="o14584" name="anther" name_original="anthers" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s18" unit="or" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>style-branches 2.</text>
      <biological_entity id="o14585" name="style-branch" name_original="style-branches" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Distal florets rudimentary, 3-awned, plumose, or hairy.</text>
      <biological_entity constraint="distal" id="o14586" name="floret" name_original="florets" src="d0_s20" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s20" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="3-awned" value_original="3-awned" />
        <character is_modifier="false" name="shape" src="d0_s20" value="plumose" value_original="plumose" />
        <character is_modifier="false" name="pubescence" src="d0_s20" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>Caryopses laterally compressed, x = 7.</text>
      <biological_entity id="o14587" name="caryopsis" name_original="caryopses" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s21" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="x" id="o14588" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="7" value_original="7" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The four species comprising Blepharidachne are restricted to the Americas, growing in arid and semi-arid regions of the United States, Mexico, and Argentina. Blepharidachne bigelovii and B. kingii are endemic to North America, whereas B. benthamiana (Hack.) Hitchc. and B. hitchcockii Lahitte are native to Argentina. Blepharidachne differs from all other genera in the tribe in having four florets per spikelet, with the first two florets being sterile or staminate, the third bisexual or pistillate, and the fourth a rudimentary 3-awned structure.</discussion>
  <references>
    <reference>Hunziker,  A.T. and A.M. Anton. 1979. A synoptical revision of Blepharidachne (Poaceae). Brittonia. 31:446-453.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.;Tex.;Utah;Calif.;Idaho;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Glumes subacute, exceeded by the distal florets</description>
      <determination>1 Blepharidachne bigelovii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Glumes acuminate or awn-tipped, exceeding the florets</description>
      <determination>2 Blepharidachne kingii</determination>
    </key_statement>
  </key>
</bio:treatment>