<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">232</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Panz." date="unknown" rank="genus">CTENIUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus ctenium</taxon_hierarchy>
  </taxon_identification>
  <number>17.42</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually perennial, sometimes annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose or rhizomatous.</text>
      <biological_entity id="o21566" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="sometimes" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 10-150 cm, simple.</text>
      <biological_entity id="o21567" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="150" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves often aromatic;</text>
      <biological_entity id="o21568" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="odor" src="d0_s3" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, sometimes ciliate;</text>
      <biological_entity id="o21569" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="sometimes" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades flat or convolute, upper blades reduced.</text>
      <biological_entity id="o21570" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s5" value="convolute" value_original="convolute" />
      </biological_entity>
      <biological_entity constraint="upper" id="o21571" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, panicles of 1-3 strongly pectinate branches, usually exceeding the upper leaves;</text>
      <biological_entity id="o21572" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o21573" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o21574" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="3" />
        <character is_modifier="true" modifier="strongly" name="shape" src="d0_s6" value="pectinate" value_original="pectinate" />
      </biological_entity>
      <biological_entity constraint="upper" id="o21575" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o21573" id="r3676" name="consist_of" negation="false" src="d0_s6" to="o21574" />
      <relation from="o21573" id="r3677" modifier="usually" name="exceeding the" negation="false" src="d0_s6" to="o21575" />
    </statement>
    <statement id="d0_s7">
      <text>branches usually falcate, if more than 1, digitately arranged, axes crescentic in cross-section, with 2 rows of solitary, subsessile spikelets.</text>
      <biological_entity id="o21576" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="falcate" value_original="falcate" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s7" upper_restricted="false" />
        <character is_modifier="false" modifier="digitately" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <biological_entity id="o21577" name="axis" name_original="axes" src="d0_s7" type="structure">
        <character constraint="in cross-section" constraintid="o21578" is_modifier="false" name="shape" src="d0_s7" value="crescentic" value_original="crescentic" />
      </biological_entity>
      <biological_entity id="o21578" name="cross-section" name_original="cross-section" src="d0_s7" type="structure" />
      <biological_entity id="o21579" name="row" name_original="rows" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o21580" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="subsessile" value_original="subsessile" />
      </biological_entity>
      <relation from="o21577" id="r3678" name="with" negation="false" src="d0_s7" to="o21579" />
      <relation from="o21579" id="r3679" name="part_of" negation="false" src="d0_s7" to="o21580" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets strongly divergent, laterally compressed, with 2 well-developed sterile or staminate florets below the single bisexual floret, reduced sterile or staminate florets also present beyond the bisexual floret;</text>
      <biological_entity id="o21581" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="strongly" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s8" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="size" notes="" src="d0_s8" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o21582" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
        <character is_modifier="true" name="development" src="d0_s8" value="well-developed" value_original="well-developed" />
        <character is_modifier="true" name="reproduction" src="d0_s8" value="sterile" value_original="sterile" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o21583" name="floret" name_original="floret" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="single" value_original="single" />
        <character is_modifier="true" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <biological_entity id="o21585" name="floret" name_original="floret" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o21581" id="r3680" name="with" negation="false" src="d0_s8" to="o21582" />
      <relation from="o21582" id="r3681" name="below" negation="false" src="d0_s8" to="o21583" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation above the glumes.</text>
      <biological_entity id="o21584" name="floret" name_original="florets" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="staminate" value_original="staminate" />
        <character constraint="beyond floret" constraintid="o21585" is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o21586" name="glume" name_original="glumes" src="d0_s9" type="structure" />
      <relation from="o21584" id="r3682" name="above" negation="false" src="d0_s9" to="o21586" />
    </statement>
    <statement id="d0_s10">
      <text>Glumes unequal;</text>
      <biological_entity id="o21587" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="size" src="d0_s10" value="unequal" value_original="unequal" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lower glumes shorter than the upper glumes, 1-veined, keeled;</text>
      <biological_entity constraint="lower" id="o21588" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character constraint="than the upper glumes" constraintid="o21589" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity constraint="upper" id="o21589" name="glume" name_original="glumes" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 2-3-veined, awned dorsally;</text>
      <biological_entity constraint="upper" id="o21590" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="2-3-veined" value_original="2-3-veined" />
        <character is_modifier="false" modifier="dorsally" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lemmas thin, 3-veined, entire or bidentate, awned, awns dorsal, attached just below the lemma apices, or terminal;</text>
      <biological_entity id="o21591" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="width" src="d0_s13" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" name="shape" src="d0_s13" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s13" value="bidentate" value_original="bidentate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o21592" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character is_modifier="false" name="position" src="d0_s13" value="dorsal" value_original="dorsal" />
        <character constraint="just below lemma apices" constraintid="o21593" is_modifier="false" name="fixation" src="d0_s13" value="attached" value_original="attached" />
        <character is_modifier="false" name="position_or_structure_subtype" notes="" src="d0_s13" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o21593" name="apex" name_original="apices" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>lodicules 2, glabrous;</text>
      <biological_entity id="o21594" name="lodicule" name_original="lodicules" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="2" value_original="2" />
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 3 in bisexual florets, 2 in staminate florets;</text>
      <biological_entity id="o21595" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character constraint="in florets" constraintid="o21596" name="quantity" src="d0_s15" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o21596" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s15" value="bisexual" value_original="bisexual" />
        <character constraint="in florets" constraintid="o21597" name="quantity" src="d0_s15" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o21597" name="floret" name_original="florets" src="d0_s15" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s15" value="staminate" value_original="staminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 2.</text>
      <biological_entity id="o21598" name="style" name_original="styles" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses ellipsoid, x = 9.</text>
      <biological_entity id="o21599" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character is_modifier="false" name="shape" src="d0_s17" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity constraint="x" id="o21600" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="9" value_original="9" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Ctenium is a genus of 17-20 species, native to tropical areas of Africa and the Americas, generally being found in savannah associations. The awned upper glumes and the presence of sterile or staminate florets both below and above the fertile floret set it apart from other genera. The aroma of the leaves is described as being like turpentine.</discussion>
  <discussion>The two native North American species are highly fire-adapted, flourishing in communities that regularly burn on a 1-5 year basis. In the fall, the panicle branches of both species form curves, loops, and corkscrews, which are attractive in floral arrangements.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;N.J.;Ga.;La.;Ala.;N.C.;S.C.;Miss.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants without rhizomes, forming dense tufts; upper glumes with a row of prominent glands on each side of the midvein; awns of the upper glumes strongly diverging at maturity</description>
      <determination>1 Ctenium aromaticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants with slender, scaly rhizomes; upper glumes without glands, or the glands inconspicuous; awns of the upper glumes straight to ascending</description>
      <determination>2 Ctenium floridanum</determination>
    </key_statement>
  </key>
</bio:treatment>