<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">396</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">ECHINOCHLOA</taxon_name>
    <taxon_name authority="(Pursh) A. Heller" date="unknown" rank="species">walteri</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus echinochloa;species walteri</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Echinochloa</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">walteri</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="forma">laevigata</taxon_name>
    <taxon_hierarchy>genus echinochloa;species walteri;forma laevigata</taxon_hierarchy>
  </taxon_identification>
  <number>5</number>
  <other_name type="common_name">Coast barnyard grass</other_name>
  <other_name type="common_name">Echinochloa de walter</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o16601" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms (30) 50-200+ cm tall, to 2.5 cm thick;</text>
      <biological_entity id="o16602" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="height" src="d0_s1" unit="cm" value="30" value_original="30" />
        <character char_type="range_value" from="50" from_unit="cm" name="height" src="d0_s1" to="200" to_unit="cm" upper_restricted="false" />
        <character char_type="range_value" from="0" from_unit="cm" name="thickness" src="d0_s1" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>nodes pilose or villous, upper nodes usually with sparser and shorter pubescence, occasionally glabrous.</text>
      <biological_entity id="o16603" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="villous" value_original="villous" />
      </biological_entity>
      <biological_entity constraint="upper" id="o16604" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Lower sheaths usually hispid, hairs papillose-based, sometimes just papillose;</text>
      <biological_entity constraint="lower" id="o16605" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s3" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o16606" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="papillose-based" value_original="papillose-based" />
        <character is_modifier="false" modifier="sometimes just" name="relief" src="d0_s3" value="papillose" value_original="papillose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>upper sheaths hispid or glabrous;</text>
      <biological_entity constraint="upper" id="o16607" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hispid" value_original="hispid" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules absent;</text>
      <biological_entity id="o16608" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades to 55 cm long, 10-35 (60) mm wide, scabrous.</text>
      <biological_entity id="o16609" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="55" to_unit="cm" />
        <character name="width" src="d0_s6" unit="mm" value="60" value_original="60" />
        <character char_type="range_value" from="10" from_unit="mm" name="width" src="d0_s6" to="35" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s6" value="scabrous" value_original="scabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles 8.5-35 cm, erect to slightly drooping, nodes hispid, hairs 3.5-5 mm, papillose-based, sometimes sparsely so, internodes usually glabrous, sometimes hispid, hairs papillose-based;</text>
      <biological_entity id="o16610" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character char_type="range_value" from="8.5" from_unit="cm" name="some_measurement" src="d0_s7" to="35" to_unit="cm" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s7" to="slightly drooping" />
      </biological_entity>
      <biological_entity id="o16611" name="node" name_original="nodes" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o16612" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s7" to="5" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o16613" name="internode" name_original="internodes" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="sometimes sparsely; sparsely; usually" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s7" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o16614" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>primary branches 1-10 cm, loosely erect, not concealed by the spikelets, nodes usually hispid, hairs papillose-based, sometimes glabrous, internodes scabrous, sometimes also sparsely hispid, hairs papillose-based;</text>
      <biological_entity constraint="primary" id="o16615" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="loosely" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character constraint="by spikelets" constraintid="o16616" is_modifier="false" modifier="not" name="prominence" src="d0_s8" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o16616" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o16617" name="node" name_original="nodes" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s8" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o16618" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="papillose-based" value_original="papillose-based" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o16619" name="internode" name_original="internodes" src="d0_s8" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s8" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" modifier="sometimes; sparsely" name="pubescence" src="d0_s8" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o16620" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>secondary branches present on the longer primary branches.</text>
      <biological_entity constraint="secondary" id="o16621" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <biological_entity constraint="longer primary" id="o16622" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <relation from="o16621" id="r2786" name="present on the" negation="false" src="d0_s9" to="o16622" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3-5 mm, disarticulating at maturity, scabrous to variously muricate and hairy, hairs usually not papillose-based, margins sometimes with a few papillose-based hairs.</text>
      <biological_entity id="o16623" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" name="architecture" src="d0_s10" value="disarticulating" value_original="disarticulating" />
        <character char_type="range_value" from="scabrous" name="relief" src="d0_s10" to="variously muricate" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o16624" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually not" name="architecture" src="d0_s10" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <biological_entity id="o16625" name="margin" name_original="margins" src="d0_s10" type="structure" />
      <biological_entity id="o16626" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="few" value_original="few" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o16625" id="r2787" name="with" negation="false" src="d0_s10" to="o16626" />
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes usually more than 1/2 as long as the spikelets, abruptly narrowing to a fine, 0.5 mm point;</text>
      <biological_entity constraint="lower" id="o16627" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="1" value_original="1" />
        <character constraint="as-long-as spikelets" constraintid="o16628" name="quantity" src="d0_s11" value="/2" value_original="/2" />
        <character char_type="range_value" from="abruptly narrowing" name="width" src="d0_s11" to="fine" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o16628" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="abruptly" name="width" notes="" src="d0_s11" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o16629" name="point" name_original="point" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>lower florets sterile;</text>
      <biological_entity constraint="lower" id="o16630" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas usually awned, awns 8-25 (60) mm;</text>
      <biological_entity constraint="lower" id="o16631" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o16632" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character name="atypical_some_measurement" src="d0_s13" unit="mm" value="60" value_original="60" />
        <character char_type="range_value" from="8" from_unit="mm" name="some_measurement" src="d0_s13" to="25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower paleas subequal to the lower lemmas;</text>
      <biological_entity constraint="lower" id="o16633" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character constraint="to lower lemmas" constraintid="o16634" is_modifier="false" name="size" src="d0_s14" value="subequal" value_original="subequal" />
      </biological_entity>
      <biological_entity constraint="lower" id="o16634" name="lemma" name_original="lemmas" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>upper lemmas 3-5 mm long, about 1.5 mm wide, not or scarcely exceeding the upper glumes, narrowly ovate to elliptical, coriaceous portion subacute, tips acuminate, membranous, without a line of hairs at the base of the tip;</text>
      <biological_entity constraint="upper" id="o16635" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s15" to="5" to_unit="mm" />
        <character name="width" src="d0_s15" unit="mm" value="1.5" value_original="1.5" />
        <character char_type="range_value" from="narrowly ovate" modifier="not" name="shape" src="d0_s15" to="elliptical" />
      </biological_entity>
      <biological_entity constraint="upper" id="o16636" name="glume" name_original="glumes" src="d0_s15" type="structure" />
      <biological_entity id="o16637" name="portion" name_original="portion" src="d0_s15" type="structure">
        <character is_modifier="true" name="texture" src="d0_s15" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="shape" src="d0_s15" value="subacute" value_original="subacute" />
      </biological_entity>
      <biological_entity id="o16638" name="tip" name_original="tips" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o16639" name="line" name_original="line" src="d0_s15" type="structure" />
      <biological_entity id="o16640" name="hair" name_original="hairs" src="d0_s15" type="structure" />
      <biological_entity id="o16641" name="base" name_original="base" src="d0_s15" type="structure" />
      <biological_entity id="o16642" name="tip" name_original="tip" src="d0_s15" type="structure" />
      <relation from="o16635" id="r2788" modifier="scarcely" name="exceeding the" negation="true" src="d0_s15" to="o16636" />
      <relation from="o16638" id="r2789" name="without" negation="false" src="d0_s15" to="o16639" />
      <relation from="o16639" id="r2790" name="part_of" negation="false" src="d0_s15" to="o16640" />
      <relation from="o16639" id="r2791" name="at" negation="false" src="d0_s15" to="o16641" />
      <relation from="o16641" id="r2792" name="part_of" negation="false" src="d0_s15" to="o16642" />
    </statement>
    <statement id="d0_s16">
      <text>anthers 0.6-1 (1.2) mm.</text>
      <biological_entity id="o16643" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="atypical_some_measurement" src="d0_s16" unit="mm" value="1.2" value_original="1.2" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses 1.2-1.8 mm, brownish;</text>
      <biological_entity id="o16644" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s17" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>embryos 52-77% as long as the caryopses.</text>
      <biological_entity id="o16646" name="caryopsis" name_original="caryopses" src="d0_s18" type="structure" />
      <relation from="o16645" id="r2793" modifier="52-77%" name="as long as" negation="false" src="d0_s18" to="o16646" />
    </statement>
    <statement id="d0_s19">
      <text>2n = 36.</text>
      <biological_entity id="o16645" name="embryo" name_original="embryos" src="d0_s18" type="structure" />
      <biological_entity constraint="2n" id="o16647" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Echinochloa walteri grows in wet places, often in shallow water and brackish marshes. It is a native species that extends through Mexico to Guatamala. It is found in both disturbed and undisturbed sites although not in rice fields. Occasional specimens of E. walteri with glabrous lower sheaths and short awns can be distinguished from E. crus-pavonis by their less dense panicles.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;W.Va.;Del.;D.C.;Wis.;Ont.;Que.;Pacific Islands (Hawaii);Fla.;N.H.;Mass.;R.I.;La.;Tenn.;N.C.;S.C.;Pa.;Va.;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Okla.;Md.;Tex.;Ohio;Mo.;Minn.;Mich.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>