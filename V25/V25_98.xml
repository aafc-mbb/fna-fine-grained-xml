<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>J.K. Wipff;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">61</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Roem. &amp; Schult." date="unknown" rank="genus">TRIPOGON</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus tripogon</taxon_hierarchy>
  </taxon_identification>
  <number>17.20</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial or annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose or tufted.</text>
      <biological_entity id="o26235" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 4-65 cm, erect, slender.</text>
      <biological_entity id="o26236" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="65" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves linear, flat, usually becoming folded and filiform;</text>
      <biological_entity id="o26237" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s3" value="linear" value_original="linear" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="usually becoming" name="architecture_or_shape" src="d0_s3" value="folded" value_original="folded" />
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, ciliate.</text>
      <biological_entity id="o26238" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, unilateral linear spikes or spikelike racemes, with 1 spikelet per node, exceeding the leaves;</text>
      <biological_entity id="o26239" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity constraint="unilateral" id="o26240" name="spike" name_original="spikes" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="unilateral" id="o26241" name="raceme" name_original="racemes" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="true" name="shape" src="d0_s5" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o26242" name="spikelet" name_original="spikelet" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o26243" name="node" name_original="node" src="d0_s5" type="structure" />
      <biological_entity id="o26244" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <relation from="o26240" id="r4426" name="with" negation="false" src="d0_s5" to="o26242" />
      <relation from="o26241" id="r4427" name="with" negation="false" src="d0_s5" to="o26242" />
      <relation from="o26242" id="r4428" name="per" negation="false" src="d0_s5" to="o26243" />
      <relation from="o26240" id="r4429" name="exceeding the" negation="false" src="d0_s5" to="o26244" />
      <relation from="o26241" id="r4430" name="exceeding the" negation="false" src="d0_s5" to="o26244" />
    </statement>
    <statement id="d0_s6">
      <text>rachises visible, not concealed by the spikelets.</text>
      <biological_entity id="o26245" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="visible" value_original="visible" />
        <character constraint="by spikelets" constraintid="o26246" is_modifier="false" modifier="not" name="prominence" src="d0_s6" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o26246" name="spikelet" name_original="spikelets" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets appressed, in 2 rows along 1 side of the rachises, with 3-20 bisexual florets, distal florets sterile or staminate;</text>
      <biological_entity id="o26247" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
      </biological_entity>
      <biological_entity id="o26248" name="row" name_original="rows" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o26249" name="side" name_original="side" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o26250" name="rachis" name_original="rachises" src="d0_s7" type="structure" />
      <biological_entity id="o26251" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s7" to="20" />
        <character is_modifier="true" name="reproduction" src="d0_s7" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o26247" id="r4431" name="in" negation="false" src="d0_s7" to="o26248" />
      <relation from="o26248" id="r4432" name="along" negation="false" src="d0_s7" to="o26249" />
      <relation from="o26249" id="r4433" name="part_of" negation="false" src="d0_s7" to="o26250" />
      <relation from="o26247" id="r4434" name="with" negation="false" src="d0_s7" to="o26251" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation above the glumes and between the florets.</text>
      <biological_entity constraint="distal" id="o26252" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s7" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="staminate" value_original="staminate" />
      </biological_entity>
      <biological_entity id="o26253" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <relation from="o26252" id="r4435" name="above the glumes and between" negation="false" src="d0_s8" to="o26253" />
    </statement>
    <statement id="d0_s9">
      <text>Glumes unequal, 1 (3) -veined;</text>
      <biological_entity id="o26254" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="unequal" value_original="unequal" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="1(3)-veined" value_original="1(3)-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lemmas 1-3-veined, backs slightly keeled or rounded, apices lobed or bifid, mucronate or awned from between the lobes, lateral-veins sometimes also excurrent, awns usually straight;</text>
      <biological_entity id="o26255" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="1-3-veined" value_original="1-3-veined" />
      </biological_entity>
      <biological_entity id="o26256" name="back" name_original="backs" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o26257" name="apex" name_original="apices" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s10" value="bifid" value_original="bifid" />
        <character is_modifier="false" name="shape" src="d0_s10" value="mucronate" value_original="mucronate" />
        <character constraint="between lobes" constraintid="o26258" is_modifier="false" name="architecture_or_shape" src="d0_s10" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o26258" name="lobe" name_original="lobes" src="d0_s10" type="structure" />
      <biological_entity id="o26259" name="lateral-vein" name_original="lateral-veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s10" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o26260" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="course" src="d0_s10" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers 1-3.</text>
    </statement>
    <statement id="d0_s12">
      <text>x = 10.</text>
      <biological_entity id="o26261" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s11" to="3" />
      </biological_entity>
      <biological_entity constraint="x" id="o26262" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Tripogon is a genus of approximately 30 species, most of which are native to the tropics of the Eastern Hemisphere, especially Africa and India, but with one, Tripogon spicatus, native to the Western Hemisphere.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.J.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>