<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>J.K. Wipff;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">227</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Hack." date="unknown" rank="genus">WILLKOMMIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus willkommia</taxon_hierarchy>
  </taxon_identification>
  <number>17.39</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose or tufted, often stoloniferous.</text>
      <biological_entity id="o6556" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 12-80 cm, herbaceous, unbranched distally.</text>
      <biological_entity id="o6557" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s2" to="80" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s2" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="distally" name="architecture" src="d0_s2" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves involute to flat;</text>
      <biological_entity id="o6558" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character char_type="range_value" from="involute" name="shape" src="d0_s3" to="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, ciliate.</text>
      <biological_entity id="o6559" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences terminal, panicles of several, racemosely arranged, spikelike branches, mostly exceeding the leaves, lower branches sometimes partially included in the upper leaf-sheaths at maturity;</text>
      <biological_entity id="o6560" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o6561" name="panicle" name_original="panicles" src="d0_s5" type="structure" />
      <biological_entity id="o6562" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="several" value_original="several" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
        <character is_modifier="true" name="shape" src="d0_s5" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o6563" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="lower" id="o6564" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <biological_entity constraint="upper" id="o6565" name="sheath" name_original="leaf-sheaths" src="d0_s5" type="structure" />
      <relation from="o6561" id="r1032" name="consist_of" negation="false" src="d0_s5" to="o6562" />
      <relation from="o6561" id="r1033" modifier="mostly" name="exceeding the" negation="false" src="d0_s5" to="o6563" />
      <relation from="o6564" id="r1034" modifier="sometimes partially" name="included in the" negation="false" src="d0_s5" to="o6565" />
    </statement>
    <statement id="d0_s6">
      <text>branches not woody, unilateral, with 2 rows of appressed, imbricate, solitary spikelets, terminating in a rudimentary, sterile spikelet, sterile spikelets sometimes consisting of 1 or 2 small scales.</text>
      <biological_entity id="o6566" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="not" name="texture" src="d0_s6" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture_or_orientation_or_position" src="d0_s6" value="unilateral" value_original="unilateral" />
      </biological_entity>
      <biological_entity id="o6567" name="row" name_original="rows" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o6568" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character is_modifier="true" name="arrangement" src="d0_s6" value="imbricate" value_original="imbricate" />
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o6569" name="spikelet" name_original="spikelet" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="true" name="reproduction" src="d0_s6" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o6570" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s6" value="sterile" value_original="sterile" />
        <character is_modifier="true" name="prominence" src="d0_s6" value="rudimentary" value_original="rudimentary" />
        <character is_modifier="true" name="reproduction" src="d0_s6" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o6571" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <relation from="o6566" id="r1035" name="with" negation="false" src="d0_s6" to="o6567" />
      <relation from="o6567" id="r1036" name="part_of" negation="false" src="d0_s6" to="o6568" />
      <relation from="o6566" id="r1037" name="terminating in" negation="false" src="d0_s6" to="o6569" />
      <relation from="o6566" id="r1038" name="terminating in" negation="false" src="d0_s6" to="o6570" />
      <relation from="o6569" id="r1039" name="consisting of" negation="false" src="d0_s6" to="o6571" />
      <relation from="o6570" id="r1040" name="consisting of" negation="false" src="d0_s6" to="o6571" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets dorsally compressed, with 1 floret;</text>
      <biological_entity id="o6573" name="floret" name_original="floret" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <relation from="o6572" id="r1041" name="with" negation="false" src="d0_s7" to="o6573" />
    </statement>
    <statement id="d0_s8">
      <text>disarticulation below the glumes.</text>
      <biological_entity id="o6572" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s7" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o6574" name="glume" name_original="glumes" src="d0_s8" type="structure" />
      <relation from="o6572" id="r1042" name="below" negation="false" src="d0_s8" to="o6574" />
    </statement>
    <statement id="d0_s9">
      <text>Lower glumes to 2/3 as long as the spikelets, veinless;</text>
      <biological_entity constraint="lower" id="o6575" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="as-long-as spikelets" constraintid="o6576" from="0" name="quantity" src="d0_s9" to="2/3" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="veinless" value_original="veinless" />
      </biological_entity>
      <biological_entity id="o6576" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>upper glumes equaling the florets, 1-veined;</text>
      <biological_entity constraint="upper" id="o6577" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o6578" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character is_modifier="true" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>calluses acute to pointed;</text>
      <biological_entity id="o6579" name="callus" name_original="calluses" src="d0_s11" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s11" to="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas thinly membranous, 3-veined, apices rounded to acute, mucronate, shortly awned, or unawned.</text>
      <biological_entity id="o6580" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="thinly" name="texture" src="d0_s12" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="3-veined" value_original="3-veined" />
      </biological_entity>
      <biological_entity id="o6581" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s12" to="acute" />
        <character is_modifier="false" name="shape" src="d0_s12" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" modifier="shortly" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Caryopses ellipsoid, x = 10.</text>
      <biological_entity id="o6582" name="caryopsis" name_original="caryopses" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity constraint="x" id="o6583" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Willkommia is a genus of four species, three native to southern tropical Africa and one to the Americas, including the Flora region.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Okla.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>