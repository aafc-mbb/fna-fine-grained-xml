<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">160</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Schreb." date="unknown" rank="genus">MUHLENBERGIA</taxon_name>
    <taxon_name authority="(Torn) Torr. ex A. Gray" date="unknown" rank="species">sylvatica</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus muhlenbergia;species sylvatica</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muhlenbergia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sylvatica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">robusta</taxon_name>
    <taxon_hierarchy>genus muhlenbergia;species sylvatica;variety robusta</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Muhlenbergia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">sylvatica</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="forma">attenuata</taxon_name>
    <taxon_hierarchy>genus muhlenbergia;species sylvatica;forma attenuata</taxon_hierarchy>
  </taxon_identification>
  <number>12</number>
  <other_name type="common_name">Woodland muhly</other_name>
  <other_name type="common_name">Muhlenbergie des bois</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o23107" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 40-110 cm tall, 1-2 mm thick, erect;</text>
      <biological_entity id="o23108" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="40" from_unit="cm" name="height" src="d0_s2" to="110" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="thickness" src="d0_s2" to="2" to_unit="mm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>internodes puberulent for most of their length, strigose below the nodes.</text>
      <biological_entity id="o23109" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character constraint="for" is_modifier="false" name="length" src="d0_s3" value="puberulent" value_original="puberulent" />
        <character constraint="below nodes" constraintid="o23110" is_modifier="false" name="pubescence" src="d0_s3" value="strigose" value_original="strigose" />
      </biological_entity>
      <biological_entity id="o23110" name="node" name_original="nodes" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous and smooth for most of their length, scabridulous distally, margins hyaline;</text>
      <biological_entity id="o23111" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="for" is_modifier="false" name="length" src="d0_s4" value="smooth" value_original="smooth" />
        <character is_modifier="false" modifier="distally" name="relief" src="d0_s4" value="scabridulous" value_original="scabridulous" />
      </biological_entity>
      <biological_entity id="o23112" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="hyaline" value_original="hyaline" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 1-2.5 mm, membranous, truncate, lacerate-ciliolate;</text>
      <biological_entity id="o23113" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="lacerate-ciliolate" value_original="lacerate-ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades 5-18 cm long, 3-7 mm wide, flat, scabrous to scabridulous, occasionally smooth.</text>
      <biological_entity id="o23114" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="18" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s6" to="7" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
        <character char_type="range_value" from="scabrous" name="relief" src="d0_s6" to="scabridulous" />
        <character is_modifier="false" modifier="occasionally" name="architecture_or_pubescence_or_relief" src="d0_s6" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal and axillary, 6-21 cm long, 0.2-1 cm wide, narrow, not dense;</text>
      <biological_entity id="o23115" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s7" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s7" to="21" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s7" to="1" to_unit="cm" />
        <character is_modifier="false" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="not" name="density" src="d0_s7" value="dense" value_original="dense" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>axillary panicles usually exserted at maturity;</text>
      <biological_entity constraint="axillary" id="o23116" name="panicle" name_original="panicles" src="d0_s8" type="structure">
        <character constraint="at maturity" is_modifier="false" modifier="usually" name="position" src="d0_s8" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branches 0.8-6 cm, ascending to closely appressed;</text>
      <biological_entity id="o23117" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="some_measurement" src="d0_s9" to="6" to_unit="cm" />
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s9" to="closely appressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>pedicels 0.8-3.5 mm, strigose.</text>
      <biological_entity id="o23118" name="pedicel" name_original="pedicels" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spikelets 2.2-3.7 mm.</text>
      <biological_entity id="o23119" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s11" to="3.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Glumes subequal, 1.8-3 mm, nearly as long as the lemmas, 1-veined, tapering from near the base, apices scabridulous, acuminate, unawned or awned, awns to 1 mm;</text>
      <biological_entity id="o23120" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="size" src="d0_s12" value="subequal" value_original="subequal" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s12" value="1-veined" value_original="1-veined" />
        <character constraint="near base" constraintid="o23122" is_modifier="false" name="shape" src="d0_s12" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o23121" name="lemma" name_original="lemmas" src="d0_s12" type="structure" />
      <biological_entity id="o23122" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o23123" name="apex" name_original="apices" src="d0_s12" type="structure">
        <character is_modifier="false" name="relief" src="d0_s12" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="shape" src="d0_s12" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o23124" name="awn" name_original="awns" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o23120" id="r3923" modifier="nearly" name="as long as" negation="false" src="d0_s12" to="o23121" />
    </statement>
    <statement id="d0_s13">
      <text>lemmas 2.2-3.7 mm, lanceolate to narrowly lanceolate, hairy on the calluses, lower 1/2 of the midveins, and margins, hairs 0.2-0.5 mm, apices scabridulous, acuminate, awned, awns 5-18 mm, purplish;</text>
      <biological_entity id="o23125" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s13" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s13" to="narrowly lanceolate" />
        <character constraint="on calluses" constraintid="o23126" is_modifier="false" name="pubescence" src="d0_s13" value="hairy" value_original="hairy" />
        <character is_modifier="false" name="position" notes="" src="d0_s13" value="lower" value_original="lower" />
        <character constraint="of midveins" constraintid="o23127" name="quantity" src="d0_s13" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o23126" name="callus" name_original="calluses" src="d0_s13" type="structure" />
      <biological_entity id="o23127" name="midvein" name_original="midveins" src="d0_s13" type="structure" />
      <biological_entity id="o23128" name="margin" name_original="margins" src="d0_s13" type="structure" />
      <biological_entity id="o23129" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s13" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o23130" name="apex" name_original="apices" src="d0_s13" type="structure">
        <character is_modifier="false" name="relief" src="d0_s13" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="shape" src="d0_s13" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o23131" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="18" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purplish" value_original="purplish" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>paleas 2-3.5 mm, lanceolate, proximal 1/2 shortly pilose, apices scabridulous, acuminate;</text>
      <biological_entity id="o23132" name="palea" name_original="paleas" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="position" src="d0_s14" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s14" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="shortly" name="pubescence" src="d0_s14" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o23133" name="apex" name_original="apices" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="scabridulous" value_original="scabridulous" />
        <character is_modifier="false" name="shape" src="d0_s14" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>anthers 0.4-0.8 mm, yellow.</text>
      <biological_entity id="o23134" name="anther" name_original="anthers" src="d0_s15" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s15" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Caryopses 1.4-2 mm, fusiform, brown.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 40.</text>
      <biological_entity id="o23135" name="caryopsis" name_original="caryopses" src="d0_s16" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s16" value="fusiform" value_original="fusiform" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o23136" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Muhlenbergia sylvatica grows in upland forests, along creeks and hollows, on rocky ledges derived from sandstone, shale, or calcareous parent materials, moist prairies, and swamps, at elevations from 30-1500 m. It is restricted to the Flora region, its primary range being southeastern Canada and the midwestern and eastern United States. Reports from British Columbia were based on a misidentification (Douglas et al. 2002). The record from Arizona is based on the report in Kearney and Peebles (1951) of a collection made by Toumey at Grapevine Creek in the Grand Canyon.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;W.Va.;Del.;D.C.;Wis.;Ont.;Que.;N.H.;Tex.;N.C.;Tenn.;Pa.;Kans.;Nebr.;Okla.;S.Dak.;R.I.;Va.;Mass.;Maine;Vt.;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Md.;Ohio;Mo.;Minn.;Mich.;Miss.;S.C.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>