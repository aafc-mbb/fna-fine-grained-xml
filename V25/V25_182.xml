<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Stephan L. Hatch;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">112</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Willd." date="unknown" rank="genus">DACTYLOCTENIUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus dactyloctenium</taxon_hierarchy>
  </taxon_identification>
  <number>17.29</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>tufted, stoloniferous, or rhizomatous.</text>
      <biological_entity id="o9588" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s1" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 5-115 (160) cm, erect or decumbent, often rooting at the lower nodes, not branching above the base.</text>
      <biological_entity id="o9589" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="160" value_original="160" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="115" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o9590" is_modifier="false" modifier="often" name="architecture" src="d0_s2" value="rooting" value_original="rooting" />
        <character constraint="above base" constraintid="o9591" is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s2" value="branching" value_original="branching" />
      </biological_entity>
      <biological_entity constraint="lower" id="o9590" name="node" name_original="nodes" src="d0_s2" type="structure" />
      <biological_entity id="o9591" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths not over¬lapping, open, keeled;</text>
      <biological_entity id="o9592" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="over ¬ lapping" name="architecture" src="d0_s3" value="open" value_original="open" />
        <character is_modifier="false" name="shape" src="d0_s3" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>auricles absent;</text>
      <biological_entity id="o9593" name="auricle" name_original="auricles" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules membranous, membranous and ciliate, or of hairs;</text>
      <biological_entity id="o9594" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o9595" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <relation from="o9594" id="r1579" name="consists_of" negation="false" src="d0_s5" to="o9595" />
    </statement>
    <statement id="d0_s6">
      <text>blades flat or involute.</text>
      <biological_entity id="o9596" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences terminal, panicles of 2-11, digitately arranged spicate branches;</text>
      <biological_entity id="o9597" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o9598" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
      <biological_entity id="o9599" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="11" />
        <character is_modifier="true" modifier="digitately" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="spicate" value_original="spicate" />
      </biological_entity>
      <relation from="o9598" id="r1580" name="consist_of" negation="false" src="d0_s7" to="o9599" />
    </statement>
    <statement id="d0_s8">
      <text>branches with axes 0.8-11 cm long, extending beyond the spikelets, terminating in a point, the spikelets imbricate in 2 rows on the lower sides.</text>
      <biological_entity id="o9600" name="branch" name_original="branches" src="d0_s8" type="structure" />
      <biological_entity id="o9601" name="axis" name_original="axes" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.8" from_unit="cm" name="length" src="d0_s8" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9602" name="spikelet" name_original="spikelets" src="d0_s8" type="structure" />
      <biological_entity id="o9603" name="point" name_original="point" src="d0_s8" type="structure" />
      <biological_entity id="o9604" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character constraint="in rows" constraintid="o9605" is_modifier="false" name="arrangement" src="d0_s8" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o9605" name="row" name_original="rows" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="lower" id="o9606" name="side" name_original="sides" src="d0_s8" type="structure" />
      <relation from="o9600" id="r1581" name="with" negation="false" src="d0_s8" to="o9601" />
      <relation from="o9600" id="r1582" name="extending beyond the" negation="false" src="d0_s8" to="o9602" />
      <relation from="o9600" id="r1583" name="terminating in" negation="false" src="d0_s8" to="o9603" />
      <relation from="o9605" id="r1584" name="on" negation="false" src="d0_s8" to="o9606" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets with 3-7 bisexual florets, additional sterile florets distally;</text>
      <biological_entity id="o9607" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o9608" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s9" to="7" />
        <character is_modifier="true" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o9607" id="r1585" name="with" negation="false" src="d0_s9" to="o9608" />
    </statement>
    <statement id="d0_s10">
      <text>disarticulation usually above the glumes, the florets falling as a unit.</text>
      <biological_entity id="o9609" name="floret" name_original="florets" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="additional" value_original="additional" />
        <character is_modifier="true" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o9610" name="glume" name_original="glumes" src="d0_s10" type="structure" />
      <biological_entity id="o9611" name="floret" name_original="florets" src="d0_s10" type="structure">
        <character constraint="as unit" constraintid="o9612" is_modifier="false" name="life_cycle" src="d0_s10" value="falling" value_original="falling" />
      </biological_entity>
      <biological_entity id="o9612" name="unit" name_original="unit" src="d0_s10" type="structure" />
      <relation from="o9609" id="r1586" modifier="usually" name="above" negation="false" src="d0_s10" to="o9610" />
    </statement>
    <statement id="d0_s11">
      <text>Glumes unequal, shorter than the adjacent lemmas, 1-veined, keeled;</text>
      <biological_entity id="o9613" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="size" src="d0_s11" value="unequal" value_original="unequal" />
        <character constraint="than the adjacent lemmas" constraintid="o9614" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o9614" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lower glumes acute, mucronate;</text>
      <biological_entity constraint="lower" id="o9615" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s12" value="mucronate" value_original="mucronate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>upper glumes subapically awned, awns curved;</text>
      <biological_entity constraint="upper" id="o9616" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="subapically" name="architecture_or_shape" src="d0_s13" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o9617" name="awn" name_original="awns" src="d0_s13" type="structure">
        <character is_modifier="false" name="course" src="d0_s13" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>calluses glabrous;</text>
      <biological_entity id="o9618" name="callus" name_original="calluses" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>lemmas membranous, glabrous, 3-veined (lateral-veins sometimes indistinct), strongly keeled, apices entire, mucronate, or awned;</text>
      <biological_entity id="o9619" name="lemma" name_original="lemmas" src="d0_s15" type="structure">
        <character is_modifier="false" name="texture" src="d0_s15" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o9620" name="apex" name_original="apices" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s15" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="awned" value_original="awned" />
        <character is_modifier="false" name="shape" src="d0_s15" value="mucronate" value_original="mucronate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="awned" value_original="awned" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>paleas glabrous;</text>
      <biological_entity id="o9621" name="palea" name_original="paleas" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>anthers 3, yellow;</text>
      <biological_entity id="o9622" name="anther" name_original="anthers" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s17" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>ovaries glabrous;</text>
      <biological_entity id="o9623" name="ovary" name_original="ovaries" src="d0_s18" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s18" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>styles fused.</text>
      <biological_entity id="o9624" name="style" name_original="styles" src="d0_s19" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s19" value="fused" value_original="fused" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Fruit utricles;</text>
      <biological_entity constraint="fruit" id="o9625" name="utricle" name_original="utricles" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>seeds falling free of the hyaline pericarp, transversely rugose or granular, x = 10.</text>
      <biological_entity id="o9626" name="seed" name_original="seeds" src="d0_s21" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s21" value="falling" value_original="falling" />
        <character constraint="of pericarp" constraintid="o9627" is_modifier="false" name="fusion" src="d0_s21" value="free" value_original="free" />
        <character is_modifier="false" modifier="transversely" name="relief" src="d0_s21" value="rugose" value_original="rugose" />
        <character is_modifier="false" name="relief" src="d0_s21" value="granular" value_original="granular" />
      </biological_entity>
      <biological_entity id="o9627" name="pericarp" name_original="pericarp" src="d0_s21" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s21" value="hyaline" value_original="hyaline" />
      </biological_entity>
      <biological_entity constraint="x" id="o9628" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dactyloctenium is primarily an African and Australian genus of 10-13 species. Three species have been introduced in the Flora region, two of which have become established. Dactyloctenium aegyptium is widespread throughout the warmer areas of the world.</discussion>
  <references>
    <reference>Black, J.M. 1978. Gramineae. Pp. 88-249 in J.M. Black. Flora of South Australia, Part I, ed. 3 (rev. J.P. Jessop). D.J. Woolman, Government Printer, Adelaide, South Australia, Australia. 466 pp.</reference>
    <reference>Clayton, W.D., S.M. Phillips, and S.A. Renvoize. 1974. Flora of Tropical East Africa. Gramineae (Part 2) (ed. R.M. Pohill). Whitefriars Press, Ltd., London, England. 373 pp.</reference>
    <reference>Jacobs, S.W.L. and S.M. Hastings. 1993. Dactyloctenium. Pp. 527-529 in G.J. Harden (ed.). Flora of New South Wales, vol. 4. New South Wales University Press, Kensington, New South Wales, Australia. 775 pp.</reference>
    <reference>Koekemoer, M. 1991. Dactylocteniium Wild. Pp. 99-101 in G.E. Gibbs Russell, L. Watson, M. Koekemoer, L. Smook, N.P. Barker, H.M. Anderson, and M.J. Dallwitz. Grasses of Southern Africa (ed. O.A. Leistner). National Botanic Gardens, Botanical Research Institute, Pretoria, Republic of South Africa. 437 pp.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Pacific Islands (Hawaii);Fla.;N.J.;N.Mex.;Tex.;La.;Tenn.;N.C.;S.C.;Pa.;Ariz.;Mass.;N.Y.;Va.;Colo.;Calif.;Puerto Rico;Virgin Islands;Ala.;Ark.;Ill.;Ga.;Maine;Md.;Ohio;Miss.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicle branches 0.4-1.5 cm long; most spikelets touching those of an adjacent branch</description>
      <determination>2 Dactyloctenium radulans</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Panicle branches 1.5-7 cm long; only the first few proximal spikelets on each branch in contact with those on an adjacent branch.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Anthers 0.5-0.9 mm long; upper glume awns 1-2.5 mm long</description>
      <determination>1 Dactyloctenium aegyptium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Anthers 1.1-1.7 mm long; upper glume awns 4.5-10 mm long</description>
      <determination>3 Dactyloctenium geminatum</determination>
    </key_statement>
  </key>
</bio:treatment>