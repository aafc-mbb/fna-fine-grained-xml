<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">238</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Rich." date="unknown" rank="genus">CYNODON</taxon_name>
    <taxon_name authority="(L.) Pers." date="unknown" rank="species">dactylon</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus cynodon;species dactylon</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cynosurus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">dactylon</taxon_name>
    <taxon_hierarchy>genus cynosurus;species dactylon</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cynodon</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">erectus</taxon_name>
    <taxon_hierarchy>genus cynodon;species erectus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Capriola</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">dactylon</taxon_name>
    <taxon_hierarchy>genus capriola;species dactylon</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Bermudagrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants stoloniferous, usually also rhizomatous.</text>
      <biological_entity id="o8098" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 5-40 (50) cm, not becoming woody.</text>
      <biological_entity id="o8099" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character name="atypical_some_measurement" src="d0_s1" unit="cm" value="50" value_original="50" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" modifier="not becoming" name="texture" src="d0_s1" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Sheaths glabrous or with scattered hairs;</text>
      <biological_entity id="o8100" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="with scattered hairs" value_original="with scattered hairs" />
      </biological_entity>
      <biological_entity id="o8101" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o8100" id="r1308" name="with" negation="false" src="d0_s2" to="o8101" />
    </statement>
    <statement id="d0_s3">
      <text>collars usually with long hairs, particularly at the margins;</text>
      <biological_entity id="o8102" name="collar" name_original="collars" src="d0_s3" type="structure" />
      <biological_entity id="o8103" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s3" value="long" value_original="long" />
      </biological_entity>
      <biological_entity id="o8104" name="margin" name_original="margins" src="d0_s3" type="structure" />
      <relation from="o8102" id="r1309" name="with" negation="false" src="d0_s3" to="o8103" />
      <relation from="o8102" id="r1310" modifier="particularly" name="at" negation="false" src="d0_s3" to="o8104" />
    </statement>
    <statement id="d0_s4">
      <text>ligules about 0.5 mm, of hairs;</text>
      <biological_entity id="o8105" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character name="some_measurement" src="d0_s4" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
      <biological_entity id="o8106" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <relation from="o8105" id="r1311" name="consists_of" negation="false" src="d0_s4" to="o8106" />
    </statement>
    <statement id="d0_s5">
      <text>blades 1-6 (16) cm long, (1) 2-4 (5) mm wide, flat at maturity, conduplicate or convolute in bud, glabrous or the adaxial surfaces pilose.</text>
      <biological_entity id="o8107" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character name="length" src="d0_s5" unit="cm" value="16" value_original="16" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="6" to_unit="cm" />
        <character name="width" src="d0_s5" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character constraint="at maturity" is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="conduplicate" value_original="conduplicate" />
        <character constraint="in bud" constraintid="o8108" is_modifier="false" name="arrangement" src="d0_s5" value="convolute" value_original="convolute" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8108" name="bud" name_original="bud" src="d0_s5" type="structure" />
      <biological_entity constraint="adaxial" id="o8109" name="surface" name_original="surfaces" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Panicles with (2) 4-6 (9) branches;</text>
      <biological_entity id="o8110" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity id="o8111" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="9" value_original="9" />
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
      <relation from="o8110" id="r1312" name="with" negation="false" src="d0_s6" to="o8111" />
    </statement>
    <statement id="d0_s7">
      <text>branches 2-6 cm, in a single whorl, axes triquetrous.</text>
      <biological_entity id="o8112" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8113" name="whorl" name_original="whorl" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o8114" name="axis" name_original="axes" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="triquetrous" value_original="triquetrous" />
      </biological_entity>
      <relation from="o8112" id="r1313" name="in" negation="false" src="d0_s7" to="o8113" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 2-3.2 mm.</text>
      <biological_entity id="o8115" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="3.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Lower glumes 1.5-2 mm;</text>
      <biological_entity constraint="lower" id="o8116" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>upper glumes 1.4-2.3 mm;</text>
      <biological_entity constraint="upper" id="o8117" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s10" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>lemmas 1.9-3.1 mm, keels not winged, pubescent, margins usually less densely pubescent;</text>
      <biological_entity id="o8118" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.9" from_unit="mm" name="some_measurement" src="d0_s11" to="3.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8119" name="keel" name_original="keels" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s11" value="winged" value_original="winged" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o8120" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually less densely" name="pubescence" src="d0_s11" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers dehiscent at maturity;</text>
      <biological_entity id="o8121" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character constraint="at maturity" is_modifier="false" name="dehiscence" src="d0_s12" value="dehiscent" value_original="dehiscent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleas glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 18, 36.</text>
      <biological_entity id="o8122" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8123" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
        <character name="quantity" src="d0_s14" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cynodon dactylon is a variable species, but taxonomists disagree on just how variable. Caro and Sanchez (1969) limited C. dactylon to plants with conduplicate leaves, placing those with convolute leaves in a number of other species, such as C. affinis Caro &amp; Sanchez and C. aristiglumis Caro &amp; Sanchez; de Wet and Harlan (1970) do not mention this character in their study of Cynodon, Caro and Sanchez also employed several other characters in the key separating C. dactylon from the species with convolute immature leaves, but the overlap between the two sides of the lead is substantial. Pending further study, the broader interpretation, in which C. dactylon includes plants with both convolute and conduplicate leaves, has been adopted.</discussion>
  <discussion>Several varieties of C. dactylon have been described, in addition to which numerous cultivars have been developed, some as turf grasses for lawns or putting greens, others as pasture or forage grasses. Their useful range is limited because C. dactylon is not cold hardy, going dormant and turning brown when nighttime temperatures fall below freezing or average daytime temperatures are below 10°C.</discussion>
  <discussion>The most commonly encountered variety, both in the Flora region and in other parts of the world, is C. dactylon var. dactylon, largely because it thrives in severely disturbed, exposed sites; it does not invade natural grass-lands or forests. Determining how many other varieties are established in the Flora region is almost impossible, because there has been no global study of variation in the species. The presence of numerous cultivars complicates an already difficult problem. The two varieties keyed out below are the only two that grow in the Flora region according to de Wet and Harlan (1970), but these authors do not appear to have considered the taxa recognized by Caro and Sanchez   (1969). For most purposes, it is probably neither necessary nor feasible to identify the variety of C. dactylon encountered.</discussion>
  <discussion>Cynodon dactylon is considered a weed in many countries and it is true that, once established, it is difficult to eradicate. It does, however, have some redeeming values. It is rich in vitamin C, and its leaves are sometimes used for an herbal tea. It is claimed to have various medicinal properties, but these have not been verified. It is considered a good pasture grass, in addition to which it is sometimes grown as an ornamental and for erosion control on exposed soils.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Va.;Del.;D.C.;W.Va.;Pacific Islands (Hawaii);Fla.;N.H.;N.Mex.;Tex.;La.;B.C.;Md.;Tenn.;N.C.;S.C.;Pa.;Nev.;Puerto Rico;Colo.;Kans.;Nebr.;Okla.;Virgin Islands;Calif.;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Ariz.;Idaho;Mont.;Oreg.;Mass.;Ohio;Utah;Mo.;Mich.;Miss.;Ky.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" value="Virgin Islands" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Rhizomes near the surface (sometimes surfacing for a short distance before submerging again), the tips eventually surfacing and, like the lateral buds, producing tillers</description>
      <determination>Cynodon dactylon var. dactylon</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Rhizomes growing up to 1 m deep, the tips remaining below ground, only the lateral buds producing tillers</description>
      <determination>Cynodon dactylon var. aridus</determination>
    </key_statement>
  </key>
</bio:treatment>