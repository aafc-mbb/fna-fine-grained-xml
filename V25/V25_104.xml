<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">64</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Jacq." date="unknown" rank="genus">DINEBRA</taxon_name>
    <taxon_name authority="(Vahl) Panz." date="unknown" rank="species">retroflexa</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus dinebra;species retroflexa</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Viper grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants loosely tufted.</text>
      <biological_entity id="o25937" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="loosely" name="arrangement_or_pubescence" src="d0_s0" value="tufted" value_original="tufted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 13-120 cm, decumbent, straggling, often rooting at the lower nodes.</text>
      <biological_entity id="o25938" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="13" from_unit="cm" name="some_measurement" src="d0_s1" to="120" to_unit="cm" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="straggling" value_original="straggling" />
        <character constraint="at lower nodes" constraintid="o25939" is_modifier="false" modifier="often" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o25939" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves sometimes glandular, particularly on the sheaths;</text>
      <biological_entity id="o25940" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sometimes" name="architecture_or_function_or_pubescence" src="d0_s2" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o25941" name="sheath" name_original="sheaths" src="d0_s2" type="structure" />
      <relation from="o25940" id="r4373" modifier="particularly" name="on" negation="false" src="d0_s2" to="o25941" />
    </statement>
    <statement id="d0_s3">
      <text>blades 4.5-28 cm long, 4-8 mm wide, finely pointed.</text>
      <biological_entity id="o25942" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="4.5" from_unit="cm" name="length" src="d0_s3" to="28" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="finely" name="shape" src="d0_s3" value="pointed" value_original="pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicles 8-34 cm;</text>
      <biological_entity id="o25943" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s4" to="34" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>branches 0.6-5 (7) cm, stiff, initially ascending, reflexed at maturity;</text>
    </statement>
    <statement id="d0_s6">
      <text>disarticulation at the base of the branches.</text>
      <biological_entity id="o25944" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character name="atypical_some_measurement" src="d0_s5" unit="cm" value="7" value_original="7" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="stiff" value_original="stiff" />
        <character is_modifier="false" modifier="initially" name="orientation" src="d0_s5" value="ascending" value_original="ascending" />
        <character constraint="at maturity" is_modifier="false" name="orientation" src="d0_s5" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o25945" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o25946" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <relation from="o25944" id="r4374" name="at" negation="false" src="d0_s6" to="o25945" />
      <relation from="o25945" id="r4375" name="part_of" negation="false" src="d0_s6" to="o25946" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 5.7-9 mm, with 1-3 florets.</text>
      <biological_entity id="o25947" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="5.7" from_unit="mm" name="some_measurement" src="d0_s7" to="9" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o25948" name="floret" name_original="florets" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o25947" id="r4376" name="with" negation="false" src="d0_s7" to="o25948" />
    </statement>
    <statement id="d0_s8">
      <text>Glumes 5.7-9 mm, asymmetric, coriaceous, keels glandular, apices caudate-curving;</text>
      <biological_entity id="o25949" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="5.7" from_unit="mm" name="some_measurement" src="d0_s8" to="9" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="asymmetric" value_original="asymmetric" />
        <character is_modifier="false" name="texture" src="d0_s8" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity id="o25950" name="keel" name_original="keels" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o25951" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="caudate-curving" value_original="caudate-curving" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lemmas 2.1-2.9 mm, narrowly ovate, appressed-pubescent on the lateral-veins and adjacent to the lower 1/2 of the central vein;</text>
      <biological_entity id="o25952" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s9" to="2.9" to_unit="mm" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s9" value="ovate" value_original="ovate" />
        <character constraint="on lateral-veins" constraintid="o25953" is_modifier="false" name="pubescence" src="d0_s9" value="appressed-pubescent" value_original="appressed-pubescent" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s9" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="position" src="d0_s9" value="lower" value_original="lower" />
        <character constraint="of central vein" constraintid="o25954" name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
      </biological_entity>
      <biological_entity id="o25953" name="lateral-vein" name_original="lateral-veins" src="d0_s9" type="structure" />
      <biological_entity constraint="central" id="o25954" name="vein" name_original="vein" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>paleas appressed-pubescent on the flaps adjacent to the keels.</text>
      <biological_entity id="o25956" name="flap" name_original="flaps" src="d0_s10" type="structure">
        <character constraint="to keels" constraintid="o25957" is_modifier="false" name="arrangement" src="d0_s10" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o25957" name="keel" name_original="keels" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 20.</text>
      <biological_entity id="o25955" name="palea" name_original="paleas" src="d0_s10" type="structure">
        <character constraint="on flaps" constraintid="o25956" is_modifier="false" name="pubescence" src="d0_s10" value="appressed-pubescent" value_original="appressed-pubescent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o25958" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Dinebra retroflexa is native from southern Africa through tropical Africa to Egypt, Iraq, Pakistan, and India. It has reportedly been found on chrome ore piles in Canton, Maryland, a temporary unloading ground for ores in the Port of Baltimore (Reed 1964), and in Mecklenberg County, North Carolina. It is a common weed of rich soils in moist, tropical regions.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md.;N.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>