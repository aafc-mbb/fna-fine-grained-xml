<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">601</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="Hitchc." date="unknown" rank="genus">REIMAROCHLOA</taxon_name>
    <taxon_name authority="(Munro ex Benth.) Hitchc." date="unknown" rank="species">oligostachya</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus reimarochloa;species oligostachya</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Florida reimargrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants stoloniferous.</text>
      <biological_entity id="o28986" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 20-40 cm or longer, compressed, decumbent, rooting at the lower nodes.</text>
      <biological_entity id="o28987" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="40" to_unit="cm" />
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="longer" value_original="longer" />
        <character is_modifier="false" name="shape" src="d0_s1" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character constraint="at lower nodes" constraintid="o28988" is_modifier="false" name="architecture" src="d0_s1" value="rooting" value_original="rooting" />
      </biological_entity>
      <biological_entity constraint="lower" id="o28988" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Sheaths loose, lower sheaths pubescent, upper sheaths glabrous;</text>
      <biological_entity id="o28989" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_fragility" src="d0_s2" value="loose" value_original="loose" />
      </biological_entity>
      <biological_entity constraint="lower" id="o28990" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="upper" id="o28991" name="sheath" name_original="sheaths" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades to 13 cm long, 2-4 mm wide, glabrous, scabrous, or pubescent.</text>
      <biological_entity id="o28992" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s3" to="13" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scabrous" value_original="scabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicle branches (1) 2-3 (4), 2.5-8 cm.</text>
      <biological_entity constraint="panicle" id="o28993" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character name="atypical_quantity" src="d0_s4" value="1" value_original="1" />
        <character name="atypical_quantity" src="d0_s4" value="4" value_original="4" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="3" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="some_measurement" src="d0_s4" to="8" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 3.8-5.2 mm long, 1-1.5 mm wide, glabrous;</text>
      <biological_entity id="o28994" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="3.8" from_unit="mm" name="length" src="d0_s5" to="5.2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>upper lemmas thinly coriaceous.</text>
    </statement>
    <statement id="d0_s7">
      <text>2n = unknown.</text>
      <biological_entity constraint="upper" id="o28995" name="lemma" name_original="lemmas" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="thinly" name="texture" src="d0_s6" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28996" name="chromosome" name_original="" src="d0_s7" type="structure" />
    </statement>
  </description>
  <discussion>Reimarochloa oligostachya grows in water or wet soil of hammocks, riverbanks, ditches, and disturbed areas. While not common in the Flora region, it grows in peninsular Florida, Mobile County, Alabama, and Cuba. The Alabama record is probably an introduction.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.;Ala.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>