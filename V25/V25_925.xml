<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">281</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Haller" date="unknown" rank="genus">TRAGUS</taxon_name>
    <taxon_name authority="Clayton" date="unknown" rank="species">heptaneuron</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus tragus;species heptaneuron</taxon_hierarchy>
  </taxon_identification>
  <number>3</number>
  <other_name type="common_name">Seven-veined burgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o16514" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 15-41cm.</text>
      <biological_entity id="o16515" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="some_measurement" src="d0_s1" to="41" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Ligules 0.5-1 mm;</text>
      <biological_entity id="o16516" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s2" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 2.5-7 cm long, 3-5 mm wide, glabrous.</text>
      <biological_entity id="o16517" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s3" to="7" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicles 4-9.5 cm long, 7-9 mm wide;</text>
      <biological_entity id="o16518" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s4" to="9.5" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s4" to="9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>rachises pubescent;</text>
      <biological_entity id="o16519" name="rachis" name_original="rachises" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>branches 0.5-1 mm, pubescent, with 2 (3) spikelets, axes not extending past the distal spikelets;</text>
      <biological_entity id="o16520" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o16521" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
        <character is_modifier="true" name="atypical_quantity" src="d0_s6" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o16522" name="axis" name_original="axes" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o16523" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="position_relational" src="d0_s6" value="past" value_original="past" />
      </biological_entity>
      <relation from="o16520" id="r2773" name="with" negation="false" src="d0_s6" to="o16521" />
      <relation from="o16522" id="r2774" name="extending" negation="false" src="d0_s6" to="o16523" />
    </statement>
    <statement id="d0_s7">
      <text>proximal internodes 0.5-0.8 mm, longer than the second internodes.</text>
      <biological_entity constraint="proximal" id="o16524" name="internode" name_original="internodes" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="0.8" to_unit="mm" />
        <character constraint="than the second internodes" constraintid="o16525" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o16525" name="internode" name_original="internodes" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Spikelets bisexual;</text>
      <biological_entity id="o16526" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>proximal spikelets 3-3.5 mm;</text>
    </statement>
    <statement id="d0_s10">
      <text>second spikelets 2.9-3.3 mm.</text>
      <biological_entity constraint="proximal" id="o16527" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16528" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2.9" from_unit="mm" name="some_measurement" src="d0_s10" to="3.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes absent or to 0.4 mm, glabrous;</text>
      <biological_entity constraint="lower" id="o16529" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s11" value="0-0.4 mm" value_original="0-0.4 mm" />
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes 3-3.5 mm, minutely pubescent, 7-veined;</text>
      <biological_entity constraint="upper" id="o16530" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s12" to="3.5" to_unit="mm" />
        <character is_modifier="false" modifier="minutely" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="7-veined" value_original="7-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>glume projections 6-9, in 7 rows, 0.1-0.6 (1) mm, uncinate;</text>
      <biological_entity constraint="glume" id="o16531" name="projection" name_original="projections" src="d0_s13" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s13" to="9" />
        <character name="atypical_some_measurement" notes="" src="d0_s13" unit="mm" value="1" value_original="1" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" notes="" src="d0_s13" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s13" value="uncinate" value_original="uncinate" />
      </biological_entity>
      <biological_entity id="o16532" name="row" name_original="rows" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="7" value_original="7" />
      </biological_entity>
      <relation from="o16531" id="r2775" name="in" negation="false" src="d0_s13" to="o16532" />
    </statement>
    <statement id="d0_s14">
      <text>lemmas (2) 2.3-2.7 mm, sparsely pubescent on the back;</text>
      <biological_entity id="o16533" name="lemma" name_original="lemmas" src="d0_s14" type="structure">
        <character name="atypical_some_measurement" src="d0_s14" unit="mm" value="2" value_original="2" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="some_measurement" src="d0_s14" to="2.7" to_unit="mm" />
        <character constraint="on back" constraintid="o16534" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s14" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o16534" name="back" name_original="back" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>paleas 2.1-2.3 mm;</text>
      <biological_entity id="o16535" name="palea" name_original="paleas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.1" from_unit="mm" name="some_measurement" src="d0_s15" to="2.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>anthers 3, 0.3-0.6 mm, yellow.</text>
      <biological_entity id="o16536" name="anther" name_original="anthers" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="3" value_original="3" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s16" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s16" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Caryopses (1.2) 1.4-1.6 mm long, 0.6 mm wide.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = unknown.</text>
      <biological_entity id="o16537" name="caryopsis" name_original="caryopses" src="d0_s17" type="structure">
        <character name="length" src="d0_s17" unit="mm" value="1.2" value_original="1.2" />
        <character char_type="range_value" from="1.4" from_unit="mm" name="length" src="d0_s17" to="1.6" to_unit="mm" />
        <character name="width" src="d0_s17" unit="mm" value="0.6" value_original="0.6" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16538" name="chromosome" name_original="" src="d0_s18" type="structure" />
    </statement>
  </description>
  <discussion>Tragus heptaneuron is native to tropical Africa, but it has been collected in Florence County, South Carolina.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>S.C.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>