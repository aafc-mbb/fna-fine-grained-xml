<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">586</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="L." date="unknown" rank="genus">PASPALUM</taxon_name>
    <taxon_name authority="(Bertol.) Nash" date="unknown" rank="species">bifidum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus paspalum;species bifidum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>30</number>
  <other_name type="common_name">Pitchfork paspalum</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous.</text>
      <biological_entity id="o24406" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 60-140 cm, erect;</text>
      <biological_entity id="o24407" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character char_type="range_value" from="60" from_unit="cm" name="some_measurement" src="d0_s2" to="140" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>nodes glabrous.</text>
      <biological_entity id="o24408" name="node" name_original="nodes" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths pubescent;</text>
      <biological_entity id="o24409" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>ligules 2-4 mm;</text>
      <biological_entity id="o24410" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s5" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades to 37 cm long, 2.2-11 mm wide, flat.</text>
      <biological_entity id="o24411" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="37" to_unit="cm" />
        <character char_type="range_value" from="2.2" from_unit="mm" name="width" src="d0_s6" to="11" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s6" value="flat" value_original="flat" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles terminal, with 2-5 racemosely arranged branches;</text>
      <biological_entity id="o24412" name="panicle" name_original="panicles" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o24413" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="5" />
        <character is_modifier="true" modifier="racemosely" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
      </biological_entity>
      <relation from="o24412" id="r4139" name="with" negation="false" src="d0_s7" to="o24413" />
    </statement>
    <statement id="d0_s8">
      <text>branches 3.7-13 cm, divergent to erect;</text>
      <biological_entity id="o24414" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="3.7" from_unit="cm" name="some_measurement" src="d0_s8" to="13" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s8" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>branch axes 0.2-0.8 mm wide, glabrous, margins scabrous, terminating in a spikelet.</text>
      <biological_entity constraint="branch" id="o24415" name="axis" name_original="axes" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s9" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24416" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s9" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity id="o24417" name="spikelet" name_original="spikelet" src="d0_s9" type="structure" />
      <relation from="o24416" id="r4140" name="terminating in" negation="false" src="d0_s9" to="o24417" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets 3.1-4 mm long, 2-2.5 mm wide, paired, not imbricate, appressed to the branch axes, elliptic to obovate, yellowbrown.</text>
      <biological_entity id="o24418" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="3.1" from_unit="mm" name="length" src="d0_s10" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s10" value="paired" value_original="paired" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s10" value="imbricate" value_original="imbricate" />
        <character constraint="to branch axes" constraintid="o24419" is_modifier="false" name="fixation_or_orientation" src="d0_s10" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="elliptic" name="shape" notes="" src="d0_s10" to="obovate" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowbrown" value_original="yellowbrown" />
      </biological_entity>
      <biological_entity constraint="branch" id="o24419" name="axis" name_original="axes" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>Lower glumes present or absent;</text>
      <biological_entity constraint="lower" id="o24420" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>upper glumes glabrous or sparsely pubescent basally, (6) 7-veined, margins entire;</text>
      <biological_entity constraint="upper" id="o24421" name="glume" name_original="glumes" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; basally" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="(6)7-veined" value_original="(6)7-veined" />
      </biological_entity>
      <biological_entity id="o24422" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>lower lemmas glabrous or sparsely pubescent basally, lacking ribs over the veins, 5-veined, margins entire;</text>
      <biological_entity constraint="lower" id="o24423" name="lemma" name_original="lemmas" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; basally" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24424" name="rib" name_original="ribs" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="lacking" value_original="lacking" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s13" value="5-veined" value_original="5-veined" />
      </biological_entity>
      <biological_entity id="o24425" name="vein" name_original="veins" src="d0_s13" type="structure" />
      <biological_entity id="o24426" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s13" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o24424" id="r4141" name="over" negation="false" src="d0_s13" to="o24425" />
    </statement>
    <statement id="d0_s14">
      <text>upper florets white.</text>
      <biological_entity constraint="upper" id="o24427" name="floret" name_original="florets" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="white" value_original="white" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Caryopses 2.6-2.9 mm, purple.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n = unknown.</text>
      <biological_entity id="o24428" name="caryopsis" name_original="caryopses" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.6" from_unit="mm" name="some_measurement" src="d0_s15" to="2.9" to_unit="mm" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s15" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24429" name="chromosome" name_original="" src="d0_s16" type="structure" />
    </statement>
  </description>
  <discussion>Paspalum bifidum is restricted to the southeastern United States. It grows at the edges of forests in longleaf pine-oak-grass ecosystems, usually in dry to mesic loamy sandy soils. It grows vigorously following fire.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Va.;Okla.;Miss.;Tex.;La.;Mo.;Ala.;Tenn.;N.C.;S.C.;Ark.;Ga.;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>