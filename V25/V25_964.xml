<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">303</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="N.P. Barker &amp; H.P. Under" date="unknown" rank="subfamily">DANTHONIOIDEAE</taxon_name>
    <taxon_name authority="Zotov" date="unknown" rank="tribe">DANTHONIEAE</taxon_name>
    <taxon_name authority="DC." date="unknown" rank="genus">DANTHONIA</taxon_name>
    <taxon_name authority="(L.) P. Beauv. ex Roem. &amp; Schult." date="unknown" rank="species">spicata</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily danthonioideae;tribe danthonieae;genus danthonia;species spicata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Danthonia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">spicata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">pinetorum</taxon_name>
    <taxon_hierarchy>genus danthonia;species spicata;variety pinetorum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Danthonia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">spicata</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">longipila</taxon_name>
    <taxon_hierarchy>genus danthonia;species spicata;variety longipila</taxon_hierarchy>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Poverty oatgrass</other_name>
  <other_name type="common_name">Danthonie à épi</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Culms (7) 10-70 (100) cm, disarticulating at the nodes when mature.</text>
      <biological_entity id="o20278" name="culm" name_original="culms" src="d0_s0" type="structure">
        <character name="atypical_some_measurement" src="d0_s0" unit="cm" value="7" value_original="7" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s0" to="70" to_unit="cm" />
        <character constraint="at nodes" constraintid="o20279" is_modifier="false" name="architecture" src="d0_s0" value="disarticulating" value_original="disarticulating" />
      </biological_entity>
      <biological_entity id="o20279" name="node" name_original="nodes" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Sheaths pilose or glabrous;</text>
      <biological_entity id="o20280" name="sheath" name_original="sheaths" src="d0_s1" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s1" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>blades 6-15 (20) cm long, 0.8-3 (4) mm wide, usually becoming curled at maturity, glabrous or pilose, uppermost cauline blades erect to ascending.</text>
      <biological_entity id="o20281" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character name="length" src="d0_s2" unit="cm" value="20" value_original="20" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character name="width" src="d0_s2" unit="mm" value="4" value_original="4" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
        <character constraint="at uppermost cauline blades" constraintid="o20282" is_modifier="false" modifier="usually becoming" name="shape" src="d0_s2" value="curled" value_original="curled" />
      </biological_entity>
      <biological_entity constraint="uppermost cauline" id="o20282" name="blade" name_original="blades" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="maturity" value_original="maturity" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="pilose" value_original="pilose" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences with 5-10 (18) spikelets;</text>
      <biological_entity id="o20283" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure" />
      <biological_entity id="o20284" name="spikelet" name_original="spikelets" src="d0_s3" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s3" value="18" value_original="18" />
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s3" to="10" />
      </biological_entity>
      <relation from="o20283" id="r3448" name="with" negation="false" src="d0_s3" to="o20284" />
    </statement>
    <statement id="d0_s4">
      <text>branches stiff, appressed to strongly ascending after anthesis;</text>
      <biological_entity id="o20285" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s4" value="stiff" value_original="stiff" />
        <character char_type="range_value" constraint="after anthesis" from="appressed" name="orientation" src="d0_s4" to="strongly ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>lower branches with 1-3 spikelets;</text>
      <biological_entity constraint="lower" id="o20286" name="branch" name_original="branches" src="d0_s5" type="structure" />
      <biological_entity id="o20287" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s5" to="3" />
      </biological_entity>
      <relation from="o20286" id="r3449" name="with" negation="false" src="d0_s5" to="o20287" />
    </statement>
    <statement id="d0_s6">
      <text>pedicels on the lowest branch from shorter than to equaling the spikelets.</text>
      <biological_entity id="o20288" name="pedicel" name_original="pedicels" src="d0_s6" type="structure" />
      <biological_entity constraint="lowest" id="o20289" name="branch" name_original="branch" src="d0_s6" type="structure" />
      <biological_entity id="o20291" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
        <character is_modifier="true" name="variability" src="d0_s6" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o20290" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s6" value="shorter" value_original="shorter" />
        <character is_modifier="true" name="variability" src="d0_s6" value="equaling" value_original="equaling" />
      </biological_entity>
      <relation from="o20288" id="r3450" name="on" negation="false" src="d0_s6" to="o20289" />
      <relation from="o20289" id="r3451" name="from" negation="false" src="d0_s6" to="o20291" />
    </statement>
    <statement id="d0_s7">
      <text>Spikelets 7-15 mm.</text>
      <biological_entity id="o20292" name="spikelet" name_original="spikelets" src="d0_s7" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s7" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Calluses of middle florets about as long as wide, convex abaxially;</text>
      <biological_entity id="o20293" name="callus" name_original="calluses" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="wide" value_original="wide" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s8" value="convex" value_original="convex" />
      </biological_entity>
      <biological_entity constraint="middle" id="o20294" name="floret" name_original="florets" src="d0_s8" type="structure" />
      <relation from="o20293" id="r3452" name="part_of" negation="false" src="d0_s8" to="o20294" />
    </statement>
    <statement id="d0_s9">
      <text>lemma bodies 2.5-5 mm, usually pilose (sometimes glabrous) over the back, margins pilose to about midlength, longest hairs 0.5-2 mm, apical teeth 0.5-2 mm, acute to aristate, less than 2/3 as long as the lemma bodies;</text>
      <biological_entity constraint="lemma" id="o20295" name="body" name_original="bodies" src="d0_s9" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s9" to="5" to_unit="mm" />
        <character constraint="over back" constraintid="o20296" is_modifier="false" modifier="usually" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o20296" name="back" name_original="back" src="d0_s9" type="structure" />
      <biological_entity id="o20297" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
        <character is_modifier="false" name="position" src="d0_s9" value="midlength" value_original="midlength" />
      </biological_entity>
      <biological_entity constraint="longest" id="o20298" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="apical" id="o20299" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s9" to="2" to_unit="mm" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s9" to="aristate" />
        <character char_type="range_value" constraint="as-long-as lemma bodies" constraintid="o20300" from="0" name="quantity" src="d0_s9" to="2/3" />
      </biological_entity>
      <biological_entity constraint="lemma" id="o20300" name="body" name_original="bodies" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>awns 5-8 mm;</text>
      <biological_entity id="o20301" name="awn" name_original="awns" src="d0_s10" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>anthers to 2.5 mm.</text>
      <biological_entity id="o20302" name="anther" name_original="anthers" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Caryopses 1.5-2 (2.3) mm long, 0.7-1 mm wide.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 31, 36.</text>
      <biological_entity id="o20303" name="caryopsis" name_original="caryopses" src="d0_s12" type="structure">
        <character name="length" src="d0_s12" unit="mm" value="2.3" value_original="2.3" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="length" src="d0_s12" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s12" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20304" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="31" value_original="31" />
        <character name="quantity" src="d0_s13" value="36" value_original="36" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Danthonia spicata grows in dry rocky, sandy, or mineral soils, generally in open sunny places. Its range includes most of boreal and temperate North America and extends south into northeastern Mexico.</discussion>
  <discussion>Phenotypically, Danthonia spicata is quite variable, expressing different growth forms under different conditions (Dore and McNeill 1980; Darbyshire and Cayouette 1989). Slow clonal growth, extensive cleistogamy, and limited dispersal contribute to the establish¬ment of morphologically uniform populations, some of which have been given scientific names. For instance, D. spicata var. pinetorum Piper is sometimes applied to depauperate plants and D. allenii Austin misapplied to more robust or 'second growth' plants (Dore and McNeill 1980). Plants of shady or moist habitats often lack the distinctive curled or twisted blades usually found on plants growing in open habits. Such plants, which tend to have smaller spikelets and pilose foliage, have been called D. spicata var. longipila Scribn. &amp; Merr. The terminal inflorescence is usually primarily cleistogamous, but plants with chasmogamous inflorescences are found throughout the range of the species. Chasmogamous plants differ in having divergent inflorescence branches at anthesis, larger anthers, and well-developed lodicules.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Conn.;N.J.;N.Y.;Wash.;Del.;D.C.;Wis.;Alta.;B.C.;Greenland;Man.;N.B.;Nfld. and Labr.;N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;Yukon;W.Va.;Mass.;Maine;N.H.;R.I.;Vt.;Fla.;Wyo.;N.Mex.;Tex.;La.;Nebr.;Tenn.;N.C.;S.C.;Pa.;Va.;Colo.;Kans.;N.Dak.;Okla.;S.Dak.;Alaska;Ala.;Ark.;Ill.;Ga.;Ind.;Iowa;Idaho;Md.;Ohio;Mo.;Minn.;Mich.;Mont.;Ariz.;Miss.;Ky.;Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>