<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">307</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="N.P. Barker &amp; H.P. Under" date="unknown" rank="subfamily">DANTHONIOIDEAE</taxon_name>
    <taxon_name authority="Zotov" date="unknown" rank="tribe">DANTHONIEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">SCHISMUS</taxon_name>
    <taxon_name authority="(Loefl. ex L.) Thell." date="unknown" rank="species">barbatus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily danthonioideae;tribe danthonieae;genus schismus;species barbatus</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">Common mediterranean grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual.</text>
      <biological_entity id="o8034" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms 6-27 cm.</text>
      <biological_entity id="o8035" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s1" to="27" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Ligules 0.3-1.1 mm, of hairs;</text>
      <biological_entity id="o8036" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s2" to="1.1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8037" name="hair" name_original="hairs" src="d0_s2" type="structure" />
      <relation from="o8036" id="r1294" name="consists_of" negation="false" src="d0_s2" to="o8037" />
    </statement>
    <statement id="d0_s3">
      <text>blades 3-15 cm long, 0.3-1.5 mm wide, abaxial surfaces glabrous or scabrous, adaxial surfaces scabrous, sparsely long-pubescent near the ligules.</text>
      <biological_entity id="o8038" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s3" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8039" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="scabrous" value_original="scabrous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o8040" name="surface" name_original="surfaces" src="d0_s3" type="structure">
        <character is_modifier="false" name="pubescence_or_relief" src="d0_s3" value="scabrous" value_original="scabrous" />
        <character constraint="near ligules" constraintid="o8041" is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s3" value="long-pubescent" value_original="long-pubescent" />
      </biological_entity>
      <biological_entity id="o8041" name="ligule" name_original="ligules" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Panicles 1-6 (7.5) cm.</text>
      <biological_entity id="o8042" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character name="atypical_some_measurement" src="d0_s4" unit="cm" value="7.5" value_original="7.5" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Spikelets 4.5-7 mm.</text>
      <biological_entity id="o8043" name="spikelet" name_original="spikelets" src="d0_s5" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s5" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Lower glumes 4-5.2 mm, exceeded by the distal florets;</text>
      <biological_entity constraint="lower" id="o8044" name="glume" name_original="glumes" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s6" to="5.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="distal" id="o8045" name="floret" name_original="florets" src="d0_s6" type="structure" />
      <relation from="o8044" id="r1295" name="exceeded by the" negation="false" src="d0_s6" to="o8045" />
    </statement>
    <statement id="d0_s7">
      <text>upper glumes 4-5.3 mm;</text>
      <biological_entity constraint="upper" id="o8046" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="5.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>lemmas 1.5-2 (2.5) mm, with sparse, appressed pubescence between the veins, or glabrous and with spreading hairs on the margins, lobes as wide as or wider than long, acute to obtuse;</text>
      <biological_entity id="o8047" name="lemma" name_original="lemmas" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="2.5" value_original="2.5" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s8" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" notes="o8048." src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8048" name="vein" name_original="veins" src="d0_s8" type="structure" />
      <biological_entity id="o8049.o8048." name="vein-vein" name_original="veins" src="d0_s8" type="structure" />
      <biological_entity id="o8050" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity id="o8051" name="margin" name_original="margins" src="d0_s8" type="structure" />
      <biological_entity id="o8052" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="width" src="d0_s8" value="wider than long" value_original="wider than long" />
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="obtuse" />
      </biological_entity>
      <relation from="o8047" id="r1296" name="with" negation="false" src="d0_s8" to="o8050" />
      <relation from="o8050" id="r1297" name="on" negation="false" src="d0_s8" to="o8051" />
    </statement>
    <statement id="d0_s9">
      <text>paleas 1.7-2.2 (2.6) mm, those of the lower florets in the spikelets as long as or longer than the lemmas;</text>
      <biological_entity id="o8053" name="palea" name_original="paleas" src="d0_s9" type="structure" constraint="floret" constraint_original="floret; floret">
        <character name="atypical_some_measurement" src="d0_s9" unit="mm" value="2.6" value_original="2.6" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="some_measurement" src="d0_s9" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="lower" id="o8054" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o8055" name="spikelet" name_original="spikelets" src="d0_s9" type="structure" />
      <biological_entity id="o8057" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o8056" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <relation from="o8053" id="r1298" name="part_of" negation="false" src="d0_s9" to="o8054" />
      <relation from="o8053" id="r1299" name="in" negation="false" src="d0_s9" to="o8055" />
      <relation from="o8055" id="r1300" name="as long as" negation="false" src="d0_s9" to="o8057" />
    </statement>
    <statement id="d0_s10">
      <text>anthers 0.2-0.4 mm.</text>
      <biological_entity id="o8058" name="anther" name_original="anthers" src="d0_s10" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Caryopses 0.6-0.8 mm. 2n = 12.</text>
      <biological_entity id="o8059" name="caryopsis" name_original="caryopses" src="d0_s11" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s11" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8060" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Schismus barbatus is native to Eurasia, but it is now established in the southwestern United States. It grows in sandy, disturbed sites along roadsides and fields and in dry riverbeds.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex.;Tex.;Utah;Calif.;Ariz.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>