<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">565</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Karen Klitz</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">AXONOPUS</taxon_name>
    <taxon_name authority="(Raddi) Kuhlm." date="unknown" rank="species">fissifolius</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus axonopus;species fissifolius</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Axonopus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">affinis</taxon_name>
    <taxon_hierarchy>genus axonopus;species affinis</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Common carpetgrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants usually cespitose, sometimes stoloniferous, nodes of the stolons often pilose.</text>
      <biological_entity id="o8637" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8638" name="node" name_original="nodes" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s0" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o8639" name="stolon" name_original="stolons" src="d0_s0" type="structure" />
      <relation from="o8638" id="r1401" name="part_of" negation="false" src="d0_s0" to="o8639" />
    </statement>
    <statement id="d0_s1">
      <text>Culms 10-75 cm, erect or depressed-decumbent;</text>
      <biological_entity id="o8640" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="75" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="depressed-decumbent" value_original="depressed-decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>cauline nodes glabrous or slightly pubescent.</text>
      <biological_entity constraint="cauline" id="o8641" name="node" name_original="nodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="slightly" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sheaths compressed, mostly glabrous, margins ciliate;</text>
      <biological_entity id="o8642" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8643" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s3" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules 0.2-0.4 mm;</text>
      <biological_entity id="o8644" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s4" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades 4-15 cm long, 1.5-6 mm wide, flat, mostly glabrous, margins with papillose-based cilia.</text>
      <biological_entity id="o8645" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="15" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="6" to_unit="mm" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8646" name="margin" name_original="margins" src="d0_s5" type="structure" />
      <biological_entity id="o8647" name="cilium" name_original="cilia" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="papillose-based" value_original="papillose-based" />
      </biological_entity>
      <relation from="o8646" id="r1402" name="with" negation="false" src="d0_s5" to="o8647" />
    </statement>
    <statement id="d0_s6">
      <text>Panicles terminal and axillary, 5-11 cm overall, rachises to 3 cm, with 2-7 branches;</text>
      <biological_entity id="o8648" name="panicle" name_original="panicles" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s6" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s6" to="11" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8649" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8650" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="7" />
      </biological_entity>
      <relation from="o8649" id="r1403" name="with" negation="false" src="d0_s6" to="o8650" />
    </statement>
    <statement id="d0_s7">
      <text>branches 2-9 (12) cm, spreading or ascending.</text>
      <biological_entity id="o8651" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character name="atypical_some_measurement" src="d0_s7" unit="cm" value="12" value_original="12" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="9" to_unit="cm" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Spikelets 1.6-2.2 (2.8) mm, ovoid or ellipsoid, obtuse to acute.</text>
      <biological_entity id="o8652" name="spikelet" name_original="spikelets" src="d0_s8" type="structure">
        <character name="atypical_some_measurement" src="d0_s8" unit="mm" value="2.8" value_original="2.8" />
        <character char_type="range_value" from="1.6" from_unit="mm" name="some_measurement" src="d0_s8" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="ellipsoid obtuse" name="shape" src="d0_s8" to="acute" />
        <character char_type="range_value" from="ellipsoid obtuse" name="shape" src="d0_s8" to="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Upper glumes and lower lemmas scarcely extending beyond the upper florets, 2-veined, margins sparsely pilose, apices obtuse to subacute;</text>
      <biological_entity constraint="upper" id="o8653" name="glume" name_original="glumes" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="2-veined" value_original="2-veined" />
      </biological_entity>
      <biological_entity constraint="upper lower" id="o8654" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" notes="" src="d0_s9" value="2-veined" value_original="2-veined" />
      </biological_entity>
      <biological_entity constraint="upper" id="o8655" name="floret" name_original="florets" src="d0_s9" type="structure" />
      <biological_entity id="o8656" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o8657" name="apex" name_original="apices" src="d0_s9" type="structure">
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s9" to="subacute" />
      </biological_entity>
      <relation from="o8653" id="r1404" name="extending beyond" negation="false" src="d0_s9" to="o8655" />
      <relation from="o8654" id="r1405" name="extending beyond" negation="false" src="d0_s9" to="o8655" />
    </statement>
    <statement id="d0_s10">
      <text>upper lemmas and paleas 1.6-2.1 mm long, 0.5-0.7 mm wide.</text>
      <biological_entity constraint="upper" id="o8658" name="lemma" name_original="lemmas" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s10" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="upper" id="o8659" name="palea" name_original="paleas" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.6" from_unit="mm" name="length" src="d0_s10" to="2.1" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s10" to="0.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Caryopses 1.5-1.8 mm, gray.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 20, 40, 80, 100.</text>
      <biological_entity id="o8660" name="caryopsis" name_original="caryopses" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="gray" value_original="gray" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8661" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="20" value_original="20" />
        <character name="quantity" src="d0_s12" value="40" value_original="40" />
        <character name="quantity" src="d0_s12" value="80" value_original="80" />
        <character name="quantity" src="d0_s12" value="100" value_original="100" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Axonopus fissifolius is sometimes used as a lawn or pasture grass, but it is also an invasive weedy species, often growing in moist, disturbed sites. It is native in the southeastern United States and from central Mexico south to Bolivia and Argentina. It has also been introduced into tropical and subtropical regions of the Eastern Hemisphere.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Va.;Okla.;Miss.;Tex.;La.;Calif.;Ala.;N.C.;S.C.;Pacific Islands (Hawaii);Ga.;Fla.;Ark.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>