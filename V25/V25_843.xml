<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">237</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Rich." date="unknown" rank="genus">CYNODON</taxon_name>
    <taxon_name authority="Burtt Davy" date="unknown" rank="species">transvaalensis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus cynodon;species transvaalensis</taxon_hierarchy>
  </taxon_identification>
  <number>2</number>
  <other_name type="common_name">African dogstooth grass</other_name>
  <other_name type="common_name">Floridagrass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants stoloniferous and rhizomatous;</text>
      <biological_entity id="o19750" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stolons slender, prostrate;</text>
      <biological_entity id="o19751" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>rhizomes slender.</text>
      <biological_entity id="o19752" name="rhizome" name_original="rhizomes" src="d0_s2" type="structure">
        <character is_modifier="false" name="size" src="d0_s2" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Culms 5-30 cm tall, to 0.4 mm thick.</text>
      <biological_entity id="o19753" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="height" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="mm" name="thickness" src="d0_s3" to="0.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sheaths glabrous or with scattered hairs;</text>
      <biological_entity id="o19754" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="with scattered hairs" value_original="with scattered hairs" />
      </biological_entity>
      <biological_entity id="o19755" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o19754" id="r3339" name="with" negation="false" src="d0_s4" to="o19755" />
    </statement>
    <statement id="d0_s5">
      <text>ligules to 0.3 mm, membranous and ciliolate;</text>
      <biological_entity id="o19756" name="ligule" name_original="ligules" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s5" to="0.3" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="ciliolate" value_original="ciliolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>blades to 4 cm long, 1-1.5 mm wide, flat or involute and filiform, both surfaces pubescent.</text>
      <biological_entity id="o19757" name="blade" name_original="blades" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s6" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s6" value="involute" value_original="involute" />
        <character is_modifier="false" name="shape" src="d0_s6" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o19758" name="surface" name_original="surfaces" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Panicles with 1-3 (4) branches;</text>
      <biological_entity id="o19759" name="panicle" name_original="panicles" src="d0_s7" type="structure" />
      <biological_entity id="o19760" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="true" name="atypical_quantity" src="d0_s7" value="4" value_original="4" />
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s7" to="3" />
      </biological_entity>
      <relation from="o19759" id="r3340" name="with" negation="false" src="d0_s7" to="o19760" />
    </statement>
    <statement id="d0_s8">
      <text>branches 0.7-2.1 cm, in a single whorl, reflexed at maturity, axes triquetrous.</text>
      <biological_entity id="o19761" name="branch" name_original="branches" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.7" from_unit="cm" name="some_measurement" src="d0_s8" to="2.1" to_unit="cm" />
        <character constraint="at maturity" is_modifier="false" name="orientation" notes="" src="d0_s8" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o19762" name="whorl" name_original="whorl" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o19763" name="axis" name_original="axes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="triquetrous" value_original="triquetrous" />
      </biological_entity>
      <relation from="o19761" id="r3341" name="in" negation="false" src="d0_s8" to="o19762" />
    </statement>
    <statement id="d0_s9">
      <text>Spikelets 2-2.7 mm.</text>
      <biological_entity id="o19764" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s9" to="2.7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Lower glumes 1.2-1.4 mm;</text>
      <biological_entity constraint="lower" id="o19765" name="glume" name_original="glumes" src="d0_s10" type="structure">
        <character char_type="range_value" from="1.2" from_unit="mm" name="some_measurement" src="d0_s10" to="1.4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper glumes 1.1-1.3 mm;</text>
      <biological_entity constraint="upper" id="o19766" name="glume" name_original="glumes" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s11" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>lemmas 2.2-2.7 mm, keels not winged, stiffly and sparsely pubescent, margins glabrous or hispidulous;</text>
      <biological_entity id="o19767" name="lemma" name_original="lemmas" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.2" from_unit="mm" name="some_measurement" src="d0_s12" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o19768" name="keel" name_original="keels" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="winged" value_original="winged" />
        <character is_modifier="false" modifier="stiffly; sparsely" name="pubescence" src="d0_s12" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o19769" name="margin" name_original="margins" src="d0_s12" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="hispidulous" value_original="hispidulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>paleas glabrous.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 18.</text>
      <biological_entity id="o19770" name="palea" name_original="paleas" src="d0_s13" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o19771" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Cynodon transvaalensis is native to southern Africa. Hitchcock (1951, p. 504) reported that it was "coming into cultivation as a lawn grass", but it is no longer sold in the Flora region, nor is there any evidence that earlier plantings have led to its establishment. Strains tested in Florida for use in putting greens were unable to withstand the mowing and moisture conditions used to maintain such areas (Busey and Boyer 2002). Strains of the species have, however, been crossed with strains of C. dactylon and cultivars developed from these crosses are sometimes used as turf grasses in the southern United States and in similar climates throughout the world.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Iowa;Calif.;Ala.;Tex.;Nebr.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>