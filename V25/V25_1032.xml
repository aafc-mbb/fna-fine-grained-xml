<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Grass Phylogeny Working Group;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">343</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Soderstr." date="unknown" rank="subfamily">CENTOTHECOIDEAE</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily centothecoideae</taxon_hierarchy>
  </taxon_identification>
  <number>9</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>rhizomatous or stoloniferous.</text>
      <biological_entity id="o26404" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms annual, sometimes becoming woody, internodes solid or hollow.</text>
      <biological_entity id="o26405" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="sometimes becoming" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o26406" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves distichous;</text>
      <biological_entity id="o26407" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="distichous" value_original="distichous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>sheaths usually open;</text>
      <biological_entity id="o26408" name="sheath" name_original="sheaths" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s4" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>auricles sometimes present;</text>
      <biological_entity id="o26409" name="auricle" name_original="auricles" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sometimes" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>abaxial ligules absent or of hairs;</text>
      <biological_entity constraint="abaxial" id="o26410" name="ligule" name_original="ligules" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character is_modifier="false" name="quantity" src="d0_s6" value="of hairs" value_original="of hairs" />
      </biological_entity>
      <biological_entity id="o26411" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <relation from="o26410" id="r4473" name="consists_of" negation="false" src="d0_s6" to="o26411" />
    </statement>
    <statement id="d0_s7">
      <text>adaxial ligules membranous, ciliate or not, or of hairs;</text>
      <biological_entity constraint="adaxial" id="o26412" name="ligule" name_original="ligules" src="d0_s7" type="structure">
        <character is_modifier="false" name="texture" src="d0_s7" value="membranous" value_original="membranous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="ciliate" value_original="ciliate" />
        <character name="architecture_or_pubescence_or_shape" src="d0_s7" value="not" value_original="not" />
      </biological_entity>
      <biological_entity id="o26413" name="hair" name_original="hairs" src="d0_s7" type="structure" />
      <relation from="o26412" id="r4474" name="consists_of" negation="false" src="d0_s7" to="o26413" />
    </statement>
    <statement id="d0_s8">
      <text>blades often pseudopetiolate;</text>
    </statement>
    <statement id="d0_s9">
      <text>mesophyll cells non-radiate;</text>
      <biological_entity id="o26414" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s8" value="pseudopetiolate" value_original="pseudopetiolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>adaxial palisade layer often present;</text>
      <biological_entity id="o26415" name="cell" name_original="cells" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="non-radiate" value_original="non-radiate" />
        <character is_modifier="false" name="position" src="d0_s10" value="adaxial" value_original="adaxial" />
      </biological_entity>
      <biological_entity id="o26416" name="layer" name_original="layer" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="often" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>fusoid cells absent, but fusoidlike cells frequently present as extensions of the outer parenchyma bundle sheath;</text>
      <biological_entity constraint="fusoid" id="o26417" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o26418" name="cell" name_original="cells" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="fusoidlike" value_original="fusoidlike" />
      </biological_entity>
      <biological_entity id="o26419" name="extension" name_original="extensions" src="d0_s11" type="structure" />
      <biological_entity constraint="bundle" id="o26420" name="sheath" name_original="sheath" src="d0_s11" type="structure" constraint_original="parenchyma bundle">
        <character is_modifier="true" name="position" src="d0_s11" value="outer" value_original="outer" />
      </biological_entity>
      <relation from="o26418" id="r4475" modifier="frequently" name="present as" negation="false" src="d0_s11" to="o26419" />
      <relation from="o26418" id="r4476" modifier="frequently" name="present as" negation="false" src="d0_s11" to="o26420" />
    </statement>
    <statement id="d0_s12">
      <text>arm cells absent;</text>
    </statement>
    <statement id="d0_s13">
      <text>kranz anatomy absent;</text>
      <biological_entity constraint="arm" id="o26421" name="cell" name_original="cells" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
        <character is_modifier="false" name="anatomy" src="d0_s13" value="kranz" value_original="kranz" />
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>midrib simple;</text>
      <biological_entity id="o26422" name="midrib" name_original="midrib" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>adaxial bulliform cells present, large;</text>
      <biological_entity constraint="adaxial" id="o26423" name="cell" name_original="cells" src="d0_s15" type="structure">
        <character is_modifier="true" name="shape" src="d0_s15" value="bulliform" value_original="bulliform" />
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
        <character is_modifier="false" name="size" src="d0_s15" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>stomata with dome-shaped or triangular subsidiary-cells;</text>
      <biological_entity id="o26425" name="subsidiary-cell" name_original="subsidiary-cells" src="d0_s16" type="structure">
        <character is_modifier="true" name="shape" src="d0_s16" value="dome--shaped" value_original="dome--shaped" />
        <character is_modifier="true" name="shape" src="d0_s16" value="triangular" value_original="triangular" />
      </biological_entity>
      <relation from="o26424" id="r4477" name="with" negation="false" src="d0_s16" to="o26425" />
    </statement>
    <statement id="d0_s17">
      <text>bicellular microhairs present, with long, tapering apical cells;</text>
      <biological_entity id="o26424" name="stoma" name_original="stomata" src="d0_s16" type="structure" />
      <biological_entity id="o26426" name="microhair" name_original="microhairs" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="apical" id="o26427" name="cell" name_original="cells" src="d0_s17" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s17" value="long" value_original="long" />
        <character is_modifier="true" name="shape" src="d0_s17" value="tapering" value_original="tapering" />
      </biological_entity>
      <relation from="o26426" id="r4478" name="with" negation="false" src="d0_s17" to="o26427" />
    </statement>
    <statement id="d0_s18">
      <text>papillae absent.</text>
      <biological_entity id="o26428" name="papilla" name_original="papillae" src="d0_s18" type="structure">
        <character is_modifier="false" name="presence" src="d0_s18" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>Inflorescences ebracteate, racemose or paniculate, panicle branches sometimes spikelike;</text>
      <biological_entity id="o26429" name="inflorescence" name_original="inflorescences" src="d0_s19" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s19" value="ebracteate" value_original="ebracteate" />
        <character is_modifier="false" name="arrangement" src="d0_s19" value="racemose" value_original="racemose" />
        <character is_modifier="false" name="arrangement" src="d0_s19" value="paniculate" value_original="paniculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>disarticulation below the spikelets or the florets, sometimes at the base of the pedicels.</text>
      <biological_entity constraint="panicle" id="o26430" name="branch" name_original="branches" src="d0_s19" type="structure">
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s19" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <biological_entity id="o26431" name="spikelet" name_original="spikelets" src="d0_s20" type="structure" />
      <biological_entity id="o26432" name="floret" name_original="florets" src="d0_s20" type="structure" />
      <biological_entity id="o26433" name="base" name_original="base" src="d0_s20" type="structure" />
      <biological_entity id="o26434" name="pedicel" name_original="pedicels" src="d0_s20" type="structure" />
      <relation from="o26430" id="r4479" name="below" negation="false" src="d0_s20" to="o26431" />
      <relation from="o26430" id="r4480" name="below" negation="false" src="d0_s20" to="o26432" />
      <relation from="o26433" id="r4481" name="part_of" negation="false" src="d0_s20" to="o26434" />
    </statement>
    <statement id="d0_s21">
      <text>Spikelets bisexual or unisexual, often laterally compressed, with (1) 2-many florets, reduced florets present, distal or basal to the functional florets.</text>
      <biological_entity id="o26435" name="spikelet" name_original="spikelets" src="d0_s21" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s21" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="reproduction" src="d0_s21" value="unisexual" value_original="unisexual" />
        <character is_modifier="false" modifier="often laterally" name="shape" src="d0_s21" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o26436" name="floret" name_original="florets" src="d0_s21" type="structure">
        <character char_type="range_value" from="(1)2" is_modifier="true" name="quantity" src="d0_s21" to="many" />
      </biological_entity>
      <biological_entity id="o26437" name="floret" name_original="florets" src="d0_s21" type="structure">
        <character is_modifier="true" name="size" src="d0_s21" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="presence" src="d0_s21" value="absent" value_original="absent" />
        <character is_modifier="false" name="position" src="d0_s21" value="distal" value_original="distal" />
        <character is_modifier="false" name="position" src="d0_s21" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="functional" id="o26438" name="floret" name_original="florets" src="d0_s21" type="structure" />
      <relation from="o26435" id="r4482" name="with" negation="false" src="d0_s21" to="o26436" />
    </statement>
    <statement id="d0_s22">
      <text>Glumes shorter than the lemmas;</text>
      <biological_entity id="o26439" name="glume" name_original="glumes" src="d0_s22" type="structure">
        <character constraint="than the lemmas" constraintid="o26440" is_modifier="false" name="height_or_length_or_size" src="d0_s22" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o26440" name="lemma" name_original="lemmas" src="d0_s22" type="structure" />
    </statement>
    <statement id="d0_s23">
      <text>lemmas lacking uncinate hairs, usually 5-9-veined, unawned or with single, terminal awns;</text>
      <biological_entity id="o26441" name="lemma" name_original="lemmas" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" notes="" src="d0_s23" value="5-9-veined" value_original="5-9-veined" />
      </biological_entity>
      <biological_entity id="o26442" name="hair" name_original="hairs" src="d0_s23" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s23" value="lacking" value_original="lacking" />
        <character is_modifier="true" name="shape" src="d0_s23" value="uncinate" value_original="uncinate" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o26443" name="awn" name_original="awns" src="d0_s23" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s23" value="single" value_original="single" />
      </biological_entity>
      <relation from="o26441" id="r4483" name="with" negation="false" src="d0_s23" to="o26443" />
    </statement>
    <statement id="d0_s24">
      <text>paleas usually well-developed, sometimes short compared to the lemmas;</text>
      <biological_entity id="o26444" name="palea" name_original="paleas" src="d0_s24" type="structure">
        <character is_modifier="false" modifier="usually" name="development" src="d0_s24" value="well-developed" value_original="well-developed" />
        <character is_modifier="false" modifier="sometimes" name="height_or_length_or_size" src="d0_s24" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o26445" name="lemma" name_original="lemmas" src="d0_s24" type="structure" />
      <relation from="o26444" id="r4484" name="compared to" negation="false" src="d0_s24" to="o26445" />
    </statement>
    <statement id="d0_s25">
      <text>lodicules 2 or none, cuneate, usually well-vascularized, varying to not or scarcely vascularized;</text>
      <biological_entity id="o26446" name="lodicule" name_original="lodicules" src="d0_s25" type="structure">
        <character name="quantity" src="d0_s25" value="2" value_original="2" />
        <character is_modifier="false" name="quantity" src="d0_s25" value="0" value_original="none" />
        <character is_modifier="false" name="shape" src="d0_s25" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="usually; scarcely; not" name="variability" src="d0_s25" value="varying" value_original="varying" />
      </biological_entity>
    </statement>
    <statement id="d0_s26">
      <text>stamens 2;</text>
      <biological_entity id="o26447" name="stamen" name_original="stamens" src="d0_s26" type="structure">
        <character name="quantity" src="d0_s26" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s27">
      <text>ovaries glabrous;</text>
    </statement>
    <statement id="d0_s28">
      <text>haustorial synergids presumed absent;</text>
      <biological_entity id="o26448" name="ovary" name_original="ovaries" src="d0_s27" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s27" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="presence" src="d0_s28" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s29">
      <text>styles 2, sometimes fused at the base, if free, close.</text>
      <biological_entity id="o26449" name="style" name_original="styles" src="d0_s29" type="structure">
        <character name="quantity" src="d0_s29" value="2" value_original="2" />
        <character constraint="at base" constraintid="o26450" is_modifier="false" modifier="sometimes" name="fusion" src="d0_s29" value="fused" value_original="fused" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s29" value="free" value_original="free" />
        <character is_modifier="false" name="arrangement" src="d0_s29" value="close" value_original="close" />
      </biological_entity>
      <biological_entity id="o26450" name="base" name_original="base" src="d0_s29" type="structure" />
    </statement>
    <statement id="d0_s30">
      <text>Hila basal, punctiform;</text>
      <biological_entity id="o26451" name="hilum" name_original="hila" src="d0_s30" type="structure">
        <character is_modifier="false" name="position" src="d0_s30" value="basal" value_original="basal" />
        <character is_modifier="false" name="shape" src="d0_s30" value="punctiform" value_original="punctiform" />
      </biological_entity>
    </statement>
    <statement id="d0_s31">
      <text>endosperm hard, without lipid;</text>
      <biological_entity id="o26452" name="endosperm" name_original="endosperm" src="d0_s31" type="structure">
        <character is_modifier="false" name="texture" src="d0_s31" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity id="o26453" name="lipid" name_original="lipid" src="d0_s31" type="structure" />
      <relation from="o26452" id="r4485" name="without" negation="false" src="d0_s31" to="o26453" />
    </statement>
    <statement id="d0_s32">
      <text>starch-grains simple;</text>
      <biological_entity id="o26454" name="starch-grain" name_original="starch-grains" src="d0_s32" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s32" value="simple" value_original="simple" />
      </biological_entity>
    </statement>
    <statement id="d0_s33">
      <text>embryos small or large relative to the caryopses;</text>
      <biological_entity id="o26456" name="caryopsis" name_original="caryopses" src="d0_s33" type="structure" />
    </statement>
    <statement id="d0_s34">
      <text>epiblasts present;</text>
    </statement>
    <statement id="d0_s35">
      <text>scutellar cleft present;</text>
    </statement>
    <statement id="d0_s36">
      <text>mesocotyl internode present;</text>
      <biological_entity id="o26455" name="embryo" name_original="embryos" src="d0_s33" type="structure">
        <character is_modifier="false" name="size" src="d0_s33" value="small" value_original="small" />
        <character constraint="to caryopses" constraintid="o26456" is_modifier="false" name="size" src="d0_s33" value="large" value_original="large" />
        <character is_modifier="false" name="presence" src="d0_s34" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s35" value="cleft" value_original="cleft" />
        <character is_modifier="false" name="presence" src="d0_s35" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s37">
      <text>embryonic leaf margins overlapping, x = (11) 12.</text>
      <biological_entity id="o26457" name="internode" name_original="internode" src="d0_s36" type="structure">
        <character is_modifier="false" name="presence" src="d0_s36" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="leaf" id="o26458" name="margin" name_original="margins" src="d0_s37" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s37" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="x" id="o26459" name="chromosome" name_original="" src="d0_s37" type="structure">
        <character name="atypical_quantity" src="d0_s37" value="11" value_original="11" />
        <character name="quantity" src="d0_s37" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <discussion>The subfamily Centothecoideae is one of the subfamilies that cannot be characterized by a suite of morphological characteristics, but anatomical, micromorphological, and nucleic acid data all support its recognition. It is most abundant in warm-temperate woodlands and tropical forests. Clayton and Renvoize (1986) suggested that it was an offshoot of the Arundinoideae, but molecular data (Hilu et al. 1999; Grass Phylogeny Working Group 2001) argue for a sister group relationship with the Panicoideae. The treatment here, in which two tribes are recognized, follows that of the Grass Phylogeny Working Group (2001). Some of the genera, however, are as yet poorly known in terms of the characters used in making such decisions.</discussion>
  <references>
    <reference>Clayton, W.D. and S.A. Renvoize. 1986. Genera Graminum: Grasses of the World. Kew Bull., Addit. Ser. 13. Her Majesty's Stationery Office, London, England. 389 pp.</reference>
    <reference>Decker, H.F. 1964. An anatomic-systematic study of the classical tribe Festuceae (Gramineae). Amer. J. Bot. 51:453-463</reference>
    <reference>Grass Phylogeny Working Group. 2001. Phylogeny and subfamilial classification of the grasses (Poaceae). Ann. Missouri Bot. Gard. 88:373-457</reference>
    <reference>Hilu, K.W, L.A. Alice and J. Liang. 1999. Phylogeny of the Poaceae inferred from matK sequences. Ann. Missouri Bot. Gard. 86:835-851</reference>
    <reference>Hilu, K.W. and K. Wright. 1982. Systematics of Gramineae: A cluster analysis study. Taxon 31:9-36</reference>
    <reference>Soderstrom, T.R. 1981.The grass subfamily Centostecoideae. Taxon 30:614-616.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets 4-50 mm long, with 1-15 florets, the lowest florets sometimes sterile, the upper florets bisexual; disarticulation at the base of the florets or the base of the spikelets; leaves not pseudopetiolate; culms 35-150 cm tall; plants not reedlike</description>
      <determination>22 Centotheceae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets 1.2-1.8 mm long, with 2(3-4) florets, the lower florets sterile, the upper florets bisexual; disarticulation at the pedicel bases, subsequently below the spikelets; leaves pseudopetiolate; culms 150-400 cm tall; plants reedlike</description>
      <determination>23 Thysanolaeneae</determination>
    </key_statement>
  </key>
</bio:treatment>