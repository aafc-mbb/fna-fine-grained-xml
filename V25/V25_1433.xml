<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">563</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="R. Br." date="unknown" rank="tribe">PANICEAE</taxon_name>
    <taxon_name authority="P. Beauv." date="unknown" rank="genus">HYMENACHNE</taxon_name>
    <taxon_name authority="(Rudge) Nees" date="unknown" rank="species">amplexicaulis</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus hymenachne;species amplexicaulis</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">West indian marsh grass</other_name>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial.</text>
      <biological_entity id="o20507" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Culms to 3.5 m tall, 1 cm or more thick, decumbent.</text>
      <biological_entity id="o20508" name="culm" name_original="culms" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="height" src="d0_s1" to="3.5" to_unit="m" />
        <character name="some_measurement" src="d0_s1" unit="cm" value="1" value_original="1" />
        <character is_modifier="false" name="width" src="d0_s1" value="thick" value_original="thick" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Ligules 1-2.5 mm, brownish;</text>
      <biological_entity id="o20509" name="ligule" name_original="ligules" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s2" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blades 15-33 cm long, 12-28 mm wide, lax, flat, glabrous.</text>
      <biological_entity id="o20510" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s3" to="33" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="mm" name="width" src="d0_s3" to="28" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="lax" value_original="lax" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Panicles 10-40 cm long, 0.7-1.2 cm thick, spikelike, dense, sometimes lobed near the base;</text>
      <biological_entity id="o20511" name="panicle" name_original="panicles" src="d0_s4" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s4" to="40" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="thickness" src="d0_s4" to="1.2" to_unit="cm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="spikelike" value_original="spikelike" />
        <character is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
        <character constraint="near base" constraintid="o20512" is_modifier="false" modifier="sometimes" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o20512" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>basal branches 1.5-5 cm, strictly erect.</text>
      <biological_entity constraint="basal" id="o20513" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s5" to="5" to_unit="cm" />
        <character is_modifier="false" modifier="strictly" name="orientation" src="d0_s5" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Spikelets 3.5-5 mm, lanceolate, acuminate.</text>
      <biological_entity id="o20514" name="spikelet" name_original="spikelets" src="d0_s6" type="structure">
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s6" to="5" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Lower glumes 1-1.7 mm, 3-veined;</text>
      <biological_entity constraint="lower" id="o20515" name="glume" name_original="glumes" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="3-veined" value_original="3-veined" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>upper glumes 2.8-3.9 mm;</text>
      <biological_entity constraint="upper" id="o20516" name="glume" name_original="glumes" src="d0_s8" type="structure">
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s8" to="3.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>lower lemmas 3.6-4.6 mm, longer than the upper glumes, attenuate to subaristate;</text>
      <biological_entity constraint="lower" id="o20517" name="lemma" name_original="lemmas" src="d0_s9" type="structure">
        <character char_type="range_value" from="3.6" from_unit="mm" name="some_measurement" src="d0_s9" to="4.6" to_unit="mm" />
        <character constraint="than the upper glumes" constraintid="o20518" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
        <character is_modifier="false" name="shape" src="d0_s9" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity constraint="upper" id="o20518" name="glume" name_original="glumes" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>lower paleas absent;</text>
      <biological_entity constraint="lower" id="o20519" name="palea" name_original="paleas" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>upper lemmas 2.5-3.5 mm;</text>
      <biological_entity constraint="upper" id="o20520" name="lemma" name_original="lemmas" src="d0_s11" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s11" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>anthers 1.1-1.2 mm. 2n = 10.</text>
      <biological_entity id="o20521" name="anther" name_original="anthers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s12" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o20522" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the Flora region, Hymenachne amplexicaulis is known only from low, wet pastures in southern Florida and it is rare even in that state. It is more abundant in the remainder of its range which extends through Mexico to Argentina.</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>