<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Jesus Valdes-Reyna;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">51</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Kunth ex Beilschm." date="unknown" rank="subfamily">CHLORIDOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">CYNODONTEAE</taxon_name>
    <taxon_name authority="Torr." date="unknown" rank="genus">MUNROA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily chloridoideae;tribe cynodonteae;genus munroa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">MONROA</taxon_name>
    <taxon_hierarchy>genus monroa</taxon_hierarchy>
  </taxon_identification>
  <number>17.18</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual;</text>
    </statement>
    <statement id="d0_s1">
      <text>stoloniferous, mat-forming;</text>
      <biological_entity id="o4574" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="stoloniferous" value_original="stoloniferous" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="mat-forming" value_original="mat-forming" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>stolons 2-8 cm, terminating in fascicles of leaves from which new culms arise.</text>
      <biological_entity id="o4575" name="stolon" name_original="stolons" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="8" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4576" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o4577" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="new" value_original="new" />
      </biological_entity>
      <relation from="o4575" id="r736" name="part_of" negation="false" src="d0_s2" to="o4576" />
    </statement>
    <statement id="d0_s3">
      <text>Culms 3-15 (30) cm.</text>
      <biological_entity id="o4578" name="culm" name_original="culms" src="d0_s3" type="structure">
        <character name="atypical_some_measurement" src="d0_s3" unit="cm" value="30" value_original="30" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly basal, sometimes with a purple tint;</text>
      <biological_entity id="o4579" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o4580" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>sheaths with a tuft of hairs at the throat;</text>
      <biological_entity id="o4581" name="sheath" name_original="sheaths" src="d0_s5" type="structure" />
      <biological_entity id="o4582" name="tuft" name_original="tuft" src="d0_s5" type="structure" />
      <biological_entity id="o4583" name="hair" name_original="hairs" src="d0_s5" type="structure" />
      <biological_entity id="o4584" name="throat" name_original="throat" src="d0_s5" type="structure" />
      <relation from="o4581" id="r737" name="with" negation="false" src="d0_s5" to="o4582" />
      <relation from="o4582" id="r738" name="part_of" negation="false" src="d0_s5" to="o4583" />
      <relation from="o4582" id="r739" name="at" negation="false" src="d0_s5" to="o4584" />
    </statement>
    <statement id="d0_s6">
      <text>auricles absent;</text>
      <biological_entity id="o4585" name="auricle" name_original="auricles" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>ligules of hairs;</text>
      <biological_entity id="o4586" name="ligule" name_original="ligules" src="d0_s7" type="structure" />
      <biological_entity id="o4587" name="hair" name_original="hairs" src="d0_s7" type="structure" />
      <relation from="o4586" id="r740" name="consists_of" negation="false" src="d0_s7" to="o4587" />
    </statement>
    <statement id="d0_s8">
      <text>blades linear, usually involute, sometimes flat or folded, with white, thickened margins, apices sharply pointed.</text>
      <biological_entity id="o4588" name="blade" name_original="blades" src="d0_s8" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" modifier="usually" name="shape_or_vernation" src="d0_s8" value="involute" value_original="involute" />
        <character is_modifier="false" modifier="sometimes" name="shape" src="d0_s8" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s8" value="folded" value_original="folded" />
      </biological_entity>
      <biological_entity id="o4589" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s8" value="white" value_original="white" />
        <character is_modifier="true" name="size_or_width" src="d0_s8" value="thickened" value_original="thickened" />
      </biological_entity>
      <biological_entity id="o4590" name="apex" name_original="apices" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s8" value="pointed" value_original="pointed" />
      </biological_entity>
      <relation from="o4588" id="r741" name="with" negation="false" src="d0_s8" to="o4589" />
    </statement>
    <statement id="d0_s9">
      <text>Inflorescences terminal, capitate panicles of spikelike branches;</text>
      <biological_entity id="o4591" name="inflorescence" name_original="inflorescences" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o4592" name="panicle" name_original="panicles" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s9" value="capitate" value_original="capitate" />
      </biological_entity>
      <biological_entity id="o4593" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="spikelike" value_original="spikelike" />
      </biological_entity>
      <relation from="o4592" id="r742" name="part_of" negation="false" src="d0_s9" to="o4593" />
    </statement>
    <statement id="d0_s10">
      <text>branches almost completely hidden in a subtending leafy bract, bearing 2-4 subsessile or pedicellate spikelets.</text>
      <biological_entity id="o4594" name="branch" name_original="branches" src="d0_s10" type="structure">
        <character constraint="in subtending bract" constraintid="o4595" is_modifier="false" modifier="almost completely" name="prominence" src="d0_s10" value="hidden" value_original="hidden" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o4595" name="bract" name_original="bract" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="leafy" value_original="leafy" />
      </biological_entity>
      <biological_entity id="o4596" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s10" to="4" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="subsessile" value_original="subsessile" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="pedicellate" value_original="pedicellate" />
      </biological_entity>
      <relation from="o4594" id="r743" name="bearing" negation="false" src="d0_s10" to="o4596" />
    </statement>
    <statement id="d0_s11">
      <text>Spikelets laterally compressed, with 2-10 florets;</text>
      <biological_entity id="o4597" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity id="o4598" name="floret" name_original="florets" src="d0_s11" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s11" to="10" />
      </biological_entity>
      <relation from="o4597" id="r744" name="with" negation="false" src="d0_s11" to="o4598" />
    </statement>
    <statement id="d0_s12">
      <text>lower florets bisexual or pistillate;</text>
      <biological_entity constraint="lower" id="o4599" name="floret" name_original="florets" src="d0_s12" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s12" value="bisexual" value_original="bisexual" />
        <character is_modifier="false" name="architecture" src="d0_s12" value="pistillate" value_original="pistillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>terminal florets sterile;</text>
    </statement>
    <statement id="d0_s14">
      <text>disarticulation above the glumes or beneath the leaves subtending the branches.</text>
      <biological_entity constraint="terminal" id="o4600" name="floret" name_original="florets" src="d0_s13" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s13" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o4601" name="branch" name_original="branches" src="d0_s14" type="structure" />
      <relation from="o4600" id="r745" name="above the glumes or beneath the leaves subtending" negation="false" src="d0_s14" to="o4601" />
    </statement>
    <statement id="d0_s15">
      <text>Glumes shorter than the spikelets, keeled, 1-veined, unawned;</text>
      <biological_entity id="o4602" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character constraint="than the spikelets" constraintid="o4603" is_modifier="false" name="height_or_length_or_size" src="d0_s15" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="architecture" src="d0_s15" value="1-veined" value_original="1-veined" />
      </biological_entity>
      <biological_entity id="o4603" name="spikelet" name_original="spikelets" src="d0_s15" type="structure" />
    </statement>
    <statement id="d0_s16">
      <text>lower glumes usually present on all spikelets (absent from all spikelets in M. mendocina);</text>
      <biological_entity constraint="lower" id="o4604" name="glume" name_original="glumes" src="d0_s16" type="structure">
        <character constraint="on spikelets" constraintid="o4605" is_modifier="false" modifier="usually" name="presence" src="d0_s16" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o4605" name="spikelet" name_original="spikelets" src="d0_s16" type="structure" />
    </statement>
    <statement id="d0_s17">
      <text>upper glumes absent or reduced on the terminal spikelet;</text>
      <biological_entity constraint="upper" id="o4606" name="glume" name_original="glumes" src="d0_s17" type="structure">
        <character is_modifier="false" name="presence" src="d0_s17" value="absent" value_original="absent" />
        <character constraint="on terminal spikelet" constraintid="o4607" is_modifier="false" name="size" src="d0_s17" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o4607" name="spikelet" name_original="spikelet" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>lemmas with a pilose tuft of hairs along the margins at midlength, membranous or coriaceous, 3-veined, lateral-veins occasionally shortly excurrent, apices emarginate or 2-lobed;</text>
      <biological_entity id="o4608" name="lemma" name_original="lemmas" src="d0_s18" type="structure" />
      <biological_entity id="o4609" name="tuft" name_original="tuft" src="d0_s18" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s18" value="pilose" value_original="pilose" />
      </biological_entity>
      <biological_entity id="o4610" name="hair" name_original="hairs" src="d0_s18" type="structure" />
      <biological_entity id="o4611" name="margin" name_original="margins" src="d0_s18" type="structure" />
      <biological_entity id="o4612" name="lateral-vein" name_original="lateral-veins" src="d0_s18" type="structure">
        <character is_modifier="true" name="position" src="d0_s18" value="midlength" value_original="midlength" />
        <character is_modifier="true" name="texture" src="d0_s18" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="texture" src="d0_s18" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="true" name="architecture" src="d0_s18" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="occasionally shortly" name="architecture" src="d0_s18" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="shape" src="d0_s18" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s18" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <biological_entity id="o4613" name="apex" name_original="apices" src="d0_s18" type="structure">
        <character is_modifier="true" name="position" src="d0_s18" value="midlength" value_original="midlength" />
        <character is_modifier="true" name="texture" src="d0_s18" value="membranous" value_original="membranous" />
        <character is_modifier="true" name="texture" src="d0_s18" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="true" name="architecture" src="d0_s18" value="3-veined" value_original="3-veined" />
        <character is_modifier="false" modifier="occasionally shortly" name="architecture" src="d0_s18" value="excurrent" value_original="excurrent" />
        <character is_modifier="false" name="shape" src="d0_s18" value="emarginate" value_original="emarginate" />
        <character is_modifier="false" name="shape" src="d0_s18" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
      <relation from="o4608" id="r746" name="with" negation="false" src="d0_s18" to="o4609" />
      <relation from="o4609" id="r747" name="part_of" negation="false" src="d0_s18" to="o4610" />
      <relation from="o4609" id="r748" name="along" negation="false" src="d0_s18" to="o4611" />
      <relation from="o4611" id="r749" name="at" negation="false" src="d0_s18" to="o4612" />
      <relation from="o4611" id="r750" name="at" negation="false" src="d0_s18" to="o4613" />
    </statement>
    <statement id="d0_s19">
      <text>paleas glabrous, smooth;</text>
      <biological_entity id="o4614" name="palea" name_original="paleas" src="d0_s19" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s19" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s19" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>lodicules present or absent, truncate;</text>
      <biological_entity id="o4615" name="lodicule" name_original="lodicules" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s20" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>anthers 2 or 3, yellow;</text>
      <biological_entity id="o4616" name="anther" name_original="anthers" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" unit="or" value="2" value_original="2" />
        <character name="quantity" src="d0_s21" unit="or" value="3" value_original="3" />
        <character is_modifier="false" name="coloration" src="d0_s21" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>style-branches elongate, 2 (3), barbellate.</text>
      <biological_entity id="o4617" name="style-branch" name_original="style-branches" src="d0_s22" type="structure">
        <character is_modifier="false" name="shape" src="d0_s22" value="elongate" value_original="elongate" />
        <character name="quantity" src="d0_s22" value="2" value_original="2" />
        <character name="atypical_quantity" src="d0_s22" value="3" value_original="3" />
        <character is_modifier="false" name="architecture" src="d0_s22" value="barbellate" value_original="barbellate" />
      </biological_entity>
    </statement>
    <statement id="d0_s23">
      <text>Caryopses dorsally compressed, x = 7 or 8.</text>
      <biological_entity id="o4618" name="caryopsis" name_original="caryopses" src="d0_s23" type="structure">
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s23" value="compressed" value_original="compressed" />
      </biological_entity>
      <biological_entity constraint="x" id="o4619" name="chromosome" name_original="" src="d0_s23" type="structure">
        <character name="quantity" src="d0_s23" value="7" value_original="7" />
        <character name="quantity" src="d0_s23" value="8" value_original="8" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Munroa, a genus of five species, is endemic to the Western Hemisphere. One species occurs in the Flora region, the remainder being confined to South America. Its closest relatives are thought to be Blepharidachne and Dasyochloa, both of which are stoloniferous, mat-forming species with leafy-bracteate panicles. Munroa differs from both in its annual habit.</discussion>
  <references>
    <reference>Anton, A.M. and A.T. Hunziker. 1978. El genero Munroa (Poaceae): Sinopsis morgologica y taxonomica. Bol. Acad. Nac. Ci. 52:229-252.</reference>
    <reference>Parodi, L.R. 1934. Contribution al estudio de las gramfneas del genero Munroa. Revista Mus. La Plata 34:171-193</reference>
    <reference>Sanchez, E. 1984. Estudios anatomicos en el genero Munroa (Poaceae, Chloridoideae, Eragrostideae). Darwiniana 25:43-57.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta.;Man.;Sask.;Colo.;N.Mex.;Tex.;Utah;Calif.;Minn.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Mont.;Wyo.;Ariz.;Oreg.;Nev.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  
</bio:treatment>