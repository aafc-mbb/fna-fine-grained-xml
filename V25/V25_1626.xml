<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/07/06 19:25:13</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">678</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Barnhart" date="unknown" rank="family">Poaceae</taxon_name>
    <taxon_name authority="Link" date="unknown" rank="subfamily">PANICOIDEAE</taxon_name>
    <taxon_name authority="Dumort." date="unknown" rank="tribe">ANDROPOGONEAE</taxon_name>
    <taxon_name authority="Andersson ex E. Fourn." date="unknown" rank="genus">HYPARRHENIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe andropogoneae;genus hyparrhenia</taxon_hierarchy>
  </taxon_identification>
  <number>26.19</number>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual or perennial;</text>
    </statement>
    <statement id="d0_s1">
      <text>cespitose, often with short rhizomes.</text>
      <biological_entity id="o971" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o972" name="rhizome" name_original="rhizomes" src="d0_s1" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s1" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Culms 30-350 (400) cm, usually erect, much branched above the bases.</text>
      <biological_entity id="o973" name="culm" name_original="culms" src="d0_s2" type="structure">
        <character name="atypical_some_measurement" src="d0_s2" unit="cm" value="400" value_original="400" />
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s2" to="350" to_unit="cm" />
        <character is_modifier="false" modifier="usually" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="above bases" constraintid="o974" is_modifier="false" modifier="much" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o974" name="base" name_original="bases" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves not aromatic;</text>
      <biological_entity id="o975" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="odor" src="d0_s3" value="aromatic" value_original="aromatic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>ligules membranous, not ciliate;</text>
      <biological_entity id="o976" name="ligule" name_original="ligules" src="d0_s4" type="structure">
        <character is_modifier="false" name="texture" src="d0_s4" value="membranous" value_original="membranous" />
        <character is_modifier="false" modifier="not" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blades usually flat or folded.</text>
      <biological_entity id="o977" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s5" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s5" value="folded" value_original="folded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences false panicles with numerous inflorescence units;</text>
      <biological_entity id="o978" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure" />
      <biological_entity constraint="false" id="o979" name="panicle" name_original="panicles" src="d0_s6" type="structure" />
      <biological_entity constraint="inflorescence" id="o980" name="unit" name_original="units" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="numerous" value_original="numerous" />
      </biological_entity>
      <relation from="o979" id="r167" name="with" negation="false" src="d0_s6" to="o980" />
    </statement>
    <statement id="d0_s7">
      <text>peduncles with 2 rames in digitate clusters;</text>
      <biological_entity id="o982" name="rame" name_original="rames" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o981" id="r168" modifier="in digitate clusters" name="with" negation="false" src="d0_s7" to="o982" />
    </statement>
    <statement id="d0_s8">
      <text>rames with naked, often deflexed bases, axes without a translucent median groove;</text>
      <biological_entity id="o981" name="peduncle" name_original="peduncles" src="d0_s7" type="structure" />
      <biological_entity id="o983" name="base" name_original="bases" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="naked" value_original="naked" />
        <character is_modifier="true" modifier="often" name="orientation" src="d0_s8" value="deflexed" value_original="deflexed" />
      </biological_entity>
      <biological_entity constraint="median" id="o985" name="groove" name_original="groove" src="d0_s8" type="structure">
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s8" value="translucent" value_original="translucent" />
      </biological_entity>
      <relation from="o981" id="r169" name="rames with" negation="false" src="d0_s8" to="o983" />
      <relation from="o984" id="r170" name="without" negation="false" src="d0_s8" to="o985" />
    </statement>
    <statement id="d0_s9">
      <text>disarticulation in the rames, beneath the bisexual spikelets.</text>
      <biological_entity id="o984" name="axis" name_original="axes" src="d0_s8" type="structure" />
      <biological_entity id="o986" name="rame" name_original="rames" src="d0_s9" type="structure" />
      <biological_entity id="o987" name="spikelet" name_original="spikelets" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="bisexual" value_original="bisexual" />
      </biological_entity>
      <relation from="o984" id="r171" name="in" negation="false" src="d0_s9" to="o986" />
    </statement>
    <statement id="d0_s10">
      <text>Spikelets in sessile-pedicellate pairs, basal 1-2 pairs on each rame homogamous, morphologically similar to the heterogamous pairs, staminate or sterile, unawned, not forming an involucre, tardily deciduous, remaining pairs heterogamous.</text>
      <biological_entity id="o988" name="spikelet" name_original="spikelets" src="d0_s10" type="structure" />
      <biological_entity id="o989" name="pair" name_original="pairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="sessile-pedicellate" value_original="sessile-pedicellate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o990" name="spikelet" name_original="spikelets" src="d0_s10" type="structure">
        <character char_type="range_value" constraint="on rame" constraintid="o991" from="1" name="quantity" src="d0_s10" to="2" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s10" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="sterile" value_original="sterile" />
        <character is_modifier="false" modifier="not; tardily" name="duration" src="d0_s10" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="reproduction" src="d0_s10" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity id="o991" name="rame" name_original="rame" src="d0_s10" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s10" value="homogamous" value_original="homogamous" />
      </biological_entity>
      <biological_entity id="o992" name="pair" name_original="pairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s10" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity id="o993" name="involucre" name_original="involucre" src="d0_s10" type="structure" />
      <relation from="o988" id="r172" name="in" negation="false" src="d0_s10" to="o989" />
      <relation from="o990" id="r173" modifier="morphologically" name="to" negation="false" src="d0_s10" to="o992" />
      <relation from="o990" id="r174" name="forming an" negation="true" src="d0_s10" to="o993" />
    </statement>
    <statement id="d0_s11">
      <text>Heterogamous spikelet units: sessile spikelets dorsally compressed or subterete;</text>
      <biological_entity constraint="spikelet" id="o994" name="unit" name_original="units" src="d0_s11" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s11" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity id="o995" name="spikelet" name_original="spikelets" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
        <character is_modifier="false" modifier="dorsally" name="shape" src="d0_s11" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="shape" src="d0_s11" value="subterete" value_original="subterete" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>calluses blunt to sharp, strigose;</text>
      <biological_entity constraint="spikelet" id="o996" name="unit" name_original="units" src="d0_s12" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s12" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity id="o997" name="callus" name_original="calluses" src="d0_s12" type="structure">
        <character char_type="range_value" from="blunt" name="shape" src="d0_s12" to="sharp" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="strigose" value_original="strigose" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>glumes equal, pubescent;</text>
      <biological_entity constraint="spikelet" id="o998" name="unit" name_original="units" src="d0_s13" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s13" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity id="o999" name="glume" name_original="glumes" src="d0_s13" type="structure">
        <character is_modifier="false" name="variability" src="d0_s13" value="equal" value_original="equal" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>lower glumes coriaceous, rounded, without keels, truncate to slightly bilobed;</text>
      <biological_entity constraint="spikelet" id="o1000" name="unit" name_original="units" src="d0_s14" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s14" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o1001" name="glume" name_original="glumes" src="d0_s14" type="structure">
        <character is_modifier="false" name="texture" src="d0_s14" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="shape" src="d0_s14" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="truncate" name="architecture_or_shape" notes="" src="d0_s14" to="slightly bilobed" />
      </biological_entity>
      <biological_entity id="o1002" name="keel" name_original="keels" src="d0_s14" type="structure" />
      <relation from="o1001" id="r175" name="without" negation="false" src="d0_s14" to="o1002" />
    </statement>
    <statement id="d0_s15">
      <text>upper glumes narrower, shallowly keeled;</text>
      <biological_entity constraint="spikelet" id="o1003" name="unit" name_original="units" src="d0_s15" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s15" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity constraint="upper" id="o1004" name="glume" name_original="glumes" src="d0_s15" type="structure">
        <character is_modifier="false" name="width" src="d0_s15" value="narrower" value_original="narrower" />
        <character is_modifier="false" modifier="shallowly" name="shape" src="d0_s15" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>lower florets sterile, reduced;</text>
      <biological_entity constraint="spikelet" id="o1005" name="unit" name_original="units" src="d0_s16" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s16" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity constraint="lower" id="o1006" name="floret" name_original="florets" src="d0_s16" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s16" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="size" src="d0_s16" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>upper florets bisexual, awned from between the teeth of the bifid lemma;</text>
      <biological_entity constraint="spikelet" id="o1007" name="unit" name_original="units" src="d0_s17" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s17" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity constraint="upper" id="o1008" name="floret" name_original="florets" src="d0_s17" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s17" value="bisexual" value_original="bisexual" />
        <character constraint="from teeth" constraintid="o1009" is_modifier="false" name="architecture_or_shape" src="d0_s17" value="awned" value_original="awned" />
      </biological_entity>
      <biological_entity id="o1009" name="tooth" name_original="teeth" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>awns usually present, to 3.5 (19) cm, pubescent on the lower portion.</text>
      <biological_entity constraint="spikelet" id="o1010" name="unit" name_original="units" src="d0_s18" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s18" value="heterogamous" value_original="heterogamous" />
      </biological_entity>
      <biological_entity id="o1011" name="awn" name_original="awns" src="d0_s18" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s18" value="absent" value_original="absent" />
        <character name="atypical_some_measurement" src="d0_s18" unit="cm" value="19" value_original="19" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s18" to="3.5" to_unit="cm" />
        <character constraint="on lower portion" constraintid="o1012" is_modifier="false" name="pubescence" src="d0_s18" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="lower" id="o1012" name="portion" name_original="portion" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>Caryopses oblong, subterete.</text>
      <biological_entity id="o1013" name="caryopsis" name_original="caryopses" src="d0_s19" type="structure">
        <character is_modifier="false" name="shape" src="d0_s19" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s19" value="subterete" value_original="subterete" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Pedicels slender, not adnate to the rame axes.</text>
      <biological_entity id="o1014" name="pedicel" name_original="pedicels" src="d0_s20" type="structure">
        <character is_modifier="false" name="size" src="d0_s20" value="slender" value_original="slender" />
        <character constraint="to axes" constraintid="o1015" is_modifier="false" modifier="not" name="fusion" src="d0_s20" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o1015" name="axis" name_original="axes" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>Pedicellate spikelets usually slightly longer than the sessile spikelets, staminate or sterile, usually unawned, lower glumes sometimes aristulate.</text>
      <biological_entity id="o1016" name="spikelet" name_original="spikelets" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="pedicellate" value_original="pedicellate" />
        <character constraint="than the sessile spikelets" constraintid="o1017" is_modifier="false" name="length_or_size" src="d0_s21" value="usually slightly longer" value_original="usually slightly longer" />
        <character is_modifier="false" name="architecture" src="d0_s21" value="staminate" value_original="staminate" />
        <character is_modifier="false" name="reproduction" src="d0_s21" value="sterile" value_original="sterile" />
      </biological_entity>
      <biological_entity id="o1017" name="spikelet" name_original="spikelets" src="d0_s21" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s21" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s22">
      <text>x = 10, 15.</text>
      <biological_entity constraint="lower" id="o1018" name="glume" name_original="glumes" src="d0_s21" type="structure">
        <character is_modifier="false" modifier="usually; sometimes" name="pubescence" src="d0_s21" value="aristulate" value_original="aristulate" />
      </biological_entity>
      <biological_entity constraint="x" id="o1019" name="chromosome" name_original="" src="d0_s22" type="structure">
        <character name="quantity" src="d0_s22" value="10" value_original="10" />
        <character name="quantity" src="d0_s22" value="15" value_original="15" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Hyparrhenia is a genus of approximately 55 mostly African species. Two have been introduced into the Flora region, but only one is known to be established. Clayton (1969) provides a detailed discussion of the structure of the inflorescence.</discussion>
  <references>
    <reference>Clayton, W.D. 1969. A Revision of the Genus Hyparrhenia. Kew Bull., Addit. Ser. 2. Her Majesty's Stationery Office, London, England. 196 pp.</reference>
  </references>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Puerto Rico;Calif.;Pacific Islands (Hawaii);Fla.;Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Puerto Rico" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Pacific Islands (Hawaii)" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets with whitish to dark yellow hairs</description>
      <determination>1 Hyparrhenia hirta</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets with reddish hairs</description>
      <determination>2 Hyparrhenia rufa</determination>
    </key_statement>
  </key>
</bio:treatment>