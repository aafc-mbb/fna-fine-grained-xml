<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David H. Wagner</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Roth" date="1799" rank="genus">Polystichum</taxon_name>
    <place_of_publication>
      <publication_title>Tent. Fl. Germ.</publication_title>
      <place_in_publication>3: 31, 69. 1799</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus Polystichum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek poly, many, and stichos, row, presumably in reference to the rows of sori on each pinna</other_info_on_name>
    <other_info_on_name type="fna_id">126461</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial.</text>
      <biological_entity id="o1300" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems decumbent to erect, stolons absent.</text>
      <biological_entity id="o1301" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="decumbent" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
      <biological_entity id="o1302" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves monomorphic (dimorphic in P. acrostichoides), evergreen.</text>
      <biological_entity id="o1303" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="duration" src="d0_s2" value="evergreen" value_original="evergreen" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 1/9–1 times length of blade, bases swollen or not;</text>
      <biological_entity id="o1304" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character constraint="blade" constraintid="o1305" is_modifier="false" name="length" src="d0_s3" value="1/9-1 times length of blade" />
      </biological_entity>
      <biological_entity id="o1305" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o1306" name="base" name_original="bases" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="swollen" value_original="swollen" />
        <character name="shape" src="d0_s3" value="not" value_original="not" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>vascular-bundles more than 3, arranged in an arc, ± round in cross-section.</text>
      <biological_entity id="o1307" name="vascular-bundle" name_original="vascular-bundles" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" upper_restricted="false" />
        <character constraint="in arc" constraintid="o1308" is_modifier="false" name="arrangement" src="d0_s4" value="arranged" value_original="arranged" />
        <character constraint="in cross-section" constraintid="o1309" is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s4" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o1308" name="arc" name_original="arc" src="d0_s4" type="structure" />
      <biological_entity id="o1309" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Blade linear-lanceolate to broadly lanceolate, 1–3-pinnate, gradually reduced distally to pinnatifid apex, somewhat leathery to leathery.</text>
      <biological_entity id="o1310" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s5" to="broadly lanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="1-3-pinnate" value_original="1-3-pinnate" />
        <character constraint="to apex" constraintid="o1311" is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="leathery" modifier="somewhat" name="texture" notes="" src="d0_s5" to="leathery" />
      </biological_entity>
      <biological_entity id="o1311" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pinnae not articulate to rachis, segment or pinna margins spinulose-toothed (except P. lemmonii);</text>
      <biological_entity id="o1312" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character constraint="to pinna margins" constraintid="o1315" is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o1313" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity id="o1314" name="segment" name_original="segment" src="d0_s6" type="structure" />
      <biological_entity constraint="pinna" id="o1315" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="spinulose-toothed" value_original="spinulose-toothed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal pinnae (several pairs) usually gradually reduced, sessile to short-petiolulate, bases usually inequilateral with acroscopic lobe;</text>
      <biological_entity constraint="proximal" id="o1316" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="usually gradually" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s7" to="short-petiolulate" />
      </biological_entity>
      <biological_entity id="o1317" name="base" name_original="bases" src="d0_s7" type="structure">
        <character constraint="with lobe" constraintid="o1318" is_modifier="false" modifier="usually" name="shape" src="d0_s7" value="inequilateral" value_original="inequilateral" />
      </biological_entity>
      <biological_entity id="o1318" name="lobe" name_original="lobe" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costae adaxially grooved, grooves continuous from rachis to costae;</text>
      <biological_entity id="o1319" name="costa" name_original="costae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s8" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o1320" name="groove" name_original="grooves" src="d0_s8" type="structure">
        <character constraint="from rachis" constraintid="o1321" is_modifier="false" name="architecture" src="d0_s8" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o1321" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
      <biological_entity id="o1322" name="costa" name_original="costae" src="d0_s8" type="structure" />
      <relation from="o1321" id="r314" name="to" negation="false" src="d0_s8" to="o1322" />
    </statement>
    <statement id="d0_s9">
      <text>indument of linear to lanceolate scales on costae and sometimes between veins abaxially (microscales), ± glabrous or similarly scaly adaxially (scales forming loosely tangled network over blade and sori in P. dudleyi).</text>
      <biological_entity id="o1323" name="indument" name_original="indument" src="d0_s9" type="structure" constraint="scale" constraint_original="scale; scale">
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="similarly; adaxially" name="pubescence" src="d0_s9" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o1324" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s9" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o1325" name="costa" name_original="costae" src="d0_s9" type="structure" />
      <biological_entity id="o1326" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity id="o1327.o1326." name="vein-vein" name_original="veins" src="d0_s9" type="structure" />
      <relation from="o1323" id="r315" name="part_of" negation="false" src="d0_s9" to="o1324" />
      <relation from="o1323" id="r316" name="on" negation="false" src="d0_s9" to="o1325" />
    </statement>
    <statement id="d0_s10">
      <text>Veins free, forked, rarely (P. imbricans) anastomosing.</text>
      <biological_entity id="o1328" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
        <character is_modifier="false" name="shape" src="d0_s10" value="forked" value_original="forked" />
        <character is_modifier="false" modifier="rarely" name="architecture" src="d0_s10" value="anastomosing" value_original="anastomosing" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sori in 1 row (to several) between midrib and margins, round (confluent, covering abaxial surface in P. acrostichoides);</text>
      <biological_entity id="o1329" name="sorus" name_original="sori" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="between midrib and margins" constraintid="o1331-o1332" id="o1330" name="row" name_original="row" src="d0_s11" type="structure" constraint_original="between  midrib and  margins, ">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o1331" name="midrib" name_original="midrib" src="d0_s11" type="structure" />
      <biological_entity id="o1332" name="margin" name_original="margins" src="d0_s11" type="structure" />
      <relation from="o1329" id="r317" name="in" negation="false" src="d0_s11" to="o1330" />
    </statement>
    <statement id="d0_s12">
      <text>indusia peltate, persistent or caducous [absent].</text>
      <biological_entity id="o1333" name="indusium" name_original="indusia" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s12" value="peltate" value_original="peltate" />
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" name="duration" src="d0_s12" value="caducous" value_original="caducous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores yellow or brownish to black, with inflated folds.</text>
      <biological_entity id="o1335" name="fold" name_original="folds" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="inflated" value_original="inflated" />
      </biological_entity>
      <relation from="o1334" id="r318" name="with" negation="false" src="d0_s13" to="o1335" />
    </statement>
    <statement id="d0_s14">
      <text>x = 41.</text>
      <biological_entity id="o1334" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="brownish" name="coloration" src="d0_s13" to="black" />
      </biological_entity>
      <biological_entity constraint="x" id="o1336" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="41" value_original="41" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12</number>
  <discussion>The mating systems of Polystichum seem to be highly outcrossing (P. S. Soltis and D. E. Soltis 1987; P. S. Soltis et al. 1989); hybrids are frequent where two or more species occur. Sterile hybrids are discussed under one of their putative parents.</discussion>
  <discussion>Sterile hybrids are best recognized by their misshapen sporangia, which produce little black dots at the end of the season instead of forming the fuzzy brown bump typical of sori after spores have been expelled. In many cases the intermediacy and robustness of hybrids make them stand out as odd. At least one or two hybrid plants are to be expected in large, mixed populations. The allopolyploids, having hybrid origins, present particular problems. They exhibit the Vavilov effect: allopolyploids tend to resemble one of their parental species when they grow with, or in the habitat typical of, that species (D. S. Barrington et al. 1989).</discussion>
  <discussion>In the flora there are six diploids, five tetraploids, one hexaploid, and three species whose chomosome number is unknown. Relationships among the diploids are generally not very close; that is, each is probably more closely related to a species outside the flora than to one of the other species in the flora. The exception to this is the group composed of Polystichum acrostichoides, P. imbricans, and P. munitum. Polystichum acrostichoides appears to share a Tertiary common ancestor with P. munitum, and P. imbricans is more recently derived from P. munitum. All of the polyploid species are fertile allopolyploids. One of these species (P. braunii) is also involved in the formation of the hexaploid P. setigerum (see below).</discussion>
  <discussion>Relationships among Polystichum Species</discussion>
  <discussion>Allopolyploid Presumed Originating Crosses andersonii kwakiutlii × munitum californicum dudleyi × imbricans or dudleyi × munitum kruckebergii lemmonii × lonchitis scopulinum lemmonii × imbricans or lemmonii × munitum setigerum braunii × munitum</discussion>
  <discussion>The morphological similarity among Polystichum species may make identification difficult, particularly among the species with more divided leaves. The keys presented here are designed for mature, typical individuals. Some of the characters mentioned in the keys and descriptions require the use of a microscope. The microscales (small trichomes that occur on the abaxial leaf surface of all species and adaxially in some) are best observed by peeling them off with cellophane tape and mounting the tape on a slide, sticky side up, under a coverslip. The tape can also be used to lift off the components of the sori. Polystichum acrostichoides, P. andersonii, P. lemmonii, and P. munitum are known to have sclereid clusters in their pith. Polystichum imbricans lacks such clusters, and data are not available for the other species.</discussion>
  <discussion>Species ca. 180 (15 in the flora).</discussion>
  <references>
    <reference>Wagner, D. H. 1979. Systematics of Polystichum in western North America north of Mexico. Pteridologia 1: 1–64.</reference>
    <reference>Wagner, W. H. Jr. 1973. Reticulation of holly ferns (Polystichum) in the western United States and adjacent Canada. Amer. Fern J. 63: 99–115.</reference>
    <reference>Welsh, S. L. 1974. Anderson's Flora of Alaska and Adjacent Parts of Canada. Provo.</reference>
    <reference>Yatskievych, G., D. B. Stein, and G. J. Gastony. 1988. Chloroplast DNA evolution and systematics of Phanerophlebia  (Dryopteridaceae) and related fern genera. Proc. Natl. Acad. Sci. U.S.A. 85: 2589–2593.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fertile pinnae contracted, sori confluent, completely covering abaxial surface.</description>
      <determination>1 Polystichum acrostichoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fertile pinnae not contracted, sori often distinct.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves 1-pinnate.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves 1-pinnate-pinnatifid or 2-pinnate.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pinnae denticulate but not spiny; pinna apex rounded; microscales dense on both leaf surfaces; restricted to the Aleutian Islands.</description>
      <determination>2 Polystichum aleuticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pinnae serrulate-spiny; pinna apex acute to cuspidate; microscales prominent on abaxial surface only; widespread.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petioles mostly less than 1/6 length of leaf; blades narrowing toward base; proximal pinnae ±deltate; pinnae spreading-spinulose.</description>
      <determination>11 Polystichum lonchitis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petioles usually greater than 1/5 length of leaf; blades narrowing slightly, if at all, toward base; proximal pinnae auriculate-ovate to falcate; pinnae incurved-spinulose.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Indusia ciliate; pinna apex acuminate, base cuneate.</description>
      <determination>13 Polystichum munitum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Indusia entire to sharply dentate; pinna apex apiculate or cuspidate, base oblique.</description>
      <determination>7 Polystichum imbricans</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves 2-pinnate, pinnules petiolate.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves 1-pinnate-pinnatifid but in some species with deeply incised pinna margins appearing 2-pinnate, segments (pinnules) sessile, adnate to costa for at least 2 mm.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Pinnules rounded at tip, margins not spiny.</description>
      <determination>10 Polystichum lemmonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Pinnules apiculate at tip, margins spiny.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Proliferous bulblets present on distal portion of leaves.</description>
      <determination>9 Polystichum kwakiutlii</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Proliferous bulblets absent.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Blades narrowed toward base; microscales on abaxial surface dense but not forming tangled network.</description>
      <determination>4 Polystichum braunii</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Blades not narrowed toward base; microscales on abaxial surface forming loosely tangled network over blades and sori.</description>
      <determination>6 Polystichum dudleyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Microscales lanceolate to linear-lanceolate, on abaxial leaf surface only; leaves often smaller than 3 dm; to 3500 m.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Microscales filiform on both leaf surfaces; leaves often larger than 3 dm; to 1700 m.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Pinna apex with spreading teeth (visible without magnification), subapical teeth nearly equal to apical tooth; apex of at least proximal pinnae acute.</description>
      <determination>8 Polystichum kruckebergii</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Pinna apex with incurved teeth, subapical teeth much smaller than apical tooth; pinnae apices obtuse.</description>
      <determination>14 Polystichum scopulinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Rachis with 1 or more bulblets at pinna base(s) on distal 1/3 of blade.</description>
      <determination>3 Polystichum andersonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Rachis without bulblets.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Pinnae not incised to costa; California to s British Columbia.</description>
      <determination>5 Polystichum californicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Pinnae incised to costa; s British Columbia northward.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Pinnule margins spinulose-dentate but not incised; Alaska to British Columbia.</description>
      <determination>15 Polystichum setigerum</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Pinnule margins deeply incised; w tip of Aleutian Islands.</description>
      <determination>12 Polystichum microchlamys</determination>
    </key_statement>
  </key>
</bio:treatment>