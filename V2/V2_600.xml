<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ching ex Pichi Sermolli" date="unknown" rank="family">thelypteridaceae</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">thelypteris</taxon_name>
    <taxon_name authority="(Blume) C. F. Reed" date="1968" rank="subgenus">Stegnogramma</taxon_name>
    <taxon_name authority="(M. Martens &amp; Galeotti) Crawford" date="1951" rank="species">pilosa</taxon_name>
    <taxon_name authority="Crawford" date="1951" rank="variety">alabamensis</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>41: 19. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family thelypteridaceae;genus thelypteris;subgenus stegnogramma;species pilosa;variety alabamensis;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233501295</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Leptogramma</taxon_name>
    <taxon_name authority="(M. Martens &amp; Galeotti) K. Iwatsuki" date="unknown" rank="species">pilosa</taxon_name>
    <taxon_name authority="(Crawford) Wherry" date="unknown" rank="variety">alabamensis</taxon_name>
    <taxon_hierarchy>genus Leptogramma;species pilosa;variety alabamensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Stegnogramma</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pilosa</taxon_name>
    <taxon_name authority="(Crawford) K. Iwatsuki" date="unknown" rank="variety">alabamensis</taxon_name>
    <taxon_hierarchy>genus Stegnogramma;species pilosa;variety alabamensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems short-creeping, 1.5–2.5 mm diam.</text>
      <biological_entity id="o10062" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="short-creeping" value_original="short-creeping" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="diameter" src="d0_s0" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves monomorphic, evergreen, clustered, 0.1–1 cm apart, 10–45 cm.</text>
      <biological_entity id="o10063" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="duration" src="d0_s1" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s1" to="1" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="apart" value_original="apart" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s1" to="45" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petiole straw-colored, (1.5–) 5–20 cm × ca. 1 mm, at base very sparsely set with ovatelanceolate, hairy scales;</text>
      <biological_entity id="o10064" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="straw-colored" value_original="straw-colored" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s2" to="5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="20" to_unit="cm" />
        <character name="width" src="d0_s2" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o10065" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o10066" name="set" name_original="set" src="d0_s2" type="structure" />
      <biological_entity id="o10067" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o10064" id="r2164" name="at" negation="false" src="d0_s2" to="o10065" />
      <relation from="o10066" id="r2165" name="with" negation="false" src="d0_s2" to="o10067" />
    </statement>
    <statement id="d0_s3">
      <text>blades linear-lanceolate, to ca. 30 (–40) cm, slightly narrowed at base, proximal 1–3 pinna pairs slightly shortened, gradually tapered to pinnatifid apex.</text>
      <biological_entity id="o10068" name="blade" name_original="blades" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="30" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="40" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="30" to_unit="cm" />
        <character constraint="at base" constraintid="o10069" is_modifier="false" modifier="slightly" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="position" notes="" src="d0_s3" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s3" to="3" />
      </biological_entity>
      <biological_entity id="o10069" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o10070" name="pinna" name_original="pinna" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="slightly" name="length" src="d0_s3" value="shortened" value_original="shortened" />
        <character char_type="range_value" from="gradually tapered" name="shape" src="d0_s3" to="pinnatifid" />
      </biological_entity>
      <biological_entity id="o10071" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Pinnae spreading, rounded at tip, 0.5–3 × 0.5–1.3 cm, proximal pinnae stalked to 3 mm, distal pinnae broadly adnate and basiscopically decurrent, crenate or incised 1/4–1/2 width or single basal acroscopic segment on proximal pinnae nearly or quite free;</text>
      <biological_entity id="o10072" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character constraint="at tip" constraintid="o10073" is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" notes="" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" notes="" src="d0_s4" to="1.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10073" name="tip" name_original="tip" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o10074" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character constraint="to distal pinnae" constraintid="o10075" is_modifier="false" name="architecture" src="d0_s4" value="stalked" value_original="stalked" />
        <character is_modifier="false" modifier="basiscopically" name="shape" src="d0_s4" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="shape" src="d0_s4" value="crenate" value_original="crenate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="incised" value_original="incised" />
        <character char_type="range_value" from="1/4" name="width" src="d0_s4" to="1/2" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10075" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character is_modifier="true" name="some_measurement" src="d0_s4" unit="mm" value="3" value_original="3" />
        <character is_modifier="false" modifier="broadly" name="fusion" src="d0_s4" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10076" name="segment" name_original="segment" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="single" value_original="single" />
        <character is_modifier="true" name="orientation" src="d0_s4" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" modifier="quite" name="fusion" notes="" src="d0_s4" value="free" value_original="free" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o10077" name="pinna" name_original="pinnae" src="d0_s4" type="structure" />
      <relation from="o10076" id="r2166" name="on" negation="false" src="d0_s4" to="o10077" />
    </statement>
    <statement id="d0_s5">
      <text>segments somewhat oblique, rounded at tip;</text>
      <biological_entity id="o10078" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="somewhat" name="orientation_or_shape" src="d0_s5" value="oblique" value_original="oblique" />
        <character constraint="at tip" constraintid="o10079" is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o10079" name="tip" name_original="tip" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>proximal pair of veins from adjacent segments running to sinus or nearly so.</text>
      <biological_entity constraint="vein" id="o10080" name="segment" name_original="segments" src="d0_s6" type="structure" constraint_original="vein proximal; vein" />
      <biological_entity id="o10081" name="vein" name_original="veins" src="d0_s6" type="structure" />
      <biological_entity id="o10082" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o10083" name="sinus" name_original="sinus" src="d0_s6" type="structure" />
      <relation from="o10080" id="r2167" name="part_of" negation="false" src="d0_s6" to="o10081" />
      <relation from="o10080" id="r2168" name="from" negation="false" src="d0_s6" to="o10082" />
      <relation from="o10082" id="r2169" name="running to" negation="false" src="d0_s6" to="o10083" />
    </statement>
    <statement id="d0_s7">
      <text>Indument on both sides of thin to stout hairs mostly 0.2–1.5 mm on costae, veins, and blade tissue.</text>
      <biological_entity id="o10084" name="indument" name_original="indument" src="d0_s7" type="structure" />
      <biological_entity id="o10085" name="side" name_original="sides" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="on costae, veins; on costae , veins , and blade tissue" constraintid="o10089, o10090" from="0.2" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10086" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="width" src="d0_s7" value="thin" value_original="thin" />
        <character is_modifier="true" name="fragility_or_size" src="d0_s7" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity id="o10087" name="costa" name_original="costae" src="d0_s7" type="structure" />
      <biological_entity id="o10088" name="vein" name_original="veins" src="d0_s7" type="structure" />
      <biological_entity id="o10089" name="costa" name_original="costae" src="d0_s7" type="structure" />
      <biological_entity id="o10090" name="vein" name_original="veins" src="d0_s7" type="structure" />
      <relation from="o10084" id="r2170" name="on" negation="false" src="d0_s7" to="o10085" />
      <relation from="o10085" id="r2171" name="part_of" negation="false" src="d0_s7" to="o10086" />
    </statement>
    <statement id="d0_s8">
      <text>Sori elongate along veins, lacking indusia;</text>
      <biological_entity id="o10091" name="sorus" name_original="sori" src="d0_s8" type="structure">
        <character constraint="along veins" constraintid="o10092" is_modifier="false" name="shape" src="d0_s8" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o10092" name="vein" name_original="veins" src="d0_s8" type="structure" />
      <biological_entity id="o10093" name="indusium" name_original="indusia" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sporangia often minutely hairy on capsule, hairs 0.1–0.2 mm.</text>
      <biological_entity id="o10094" name="sporangium" name_original="sporangia" src="d0_s9" type="structure">
        <character constraint="on capsule" constraintid="o10095" is_modifier="false" modifier="often minutely" name="pubescence" src="d0_s9" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o10095" name="capsule" name_original="capsule" src="d0_s9" type="structure" />
      <biological_entity id="o10096" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s9" to="0.2" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>On sandstone cliffs in river gorges</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandstone cliffs" modifier="on" constraint="in river gorges" />
        <character name="habitat" value="river gorges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>150 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="150" to_unit="m" from="150" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala.; Mexico in Chihuahua, Sonora.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" value="Mexico in Chihuahua" establishment_means="native" />
        <character name="distribution" value="Sonora" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16a</number>
  <other_name type="common_name">Alabama streak-sorus fern</other_name>
  <discussion>Thelypteris pilosa is included in the Asian and African genus Stegnogramma by K. Iwatsuki (1964). Variety alabamensis differs from var. pilosa in Mexico and Central America by the much narrower blades, spreading (vs. ascending) pinnae with rounded tips, and free or nearly free basal acroscopic segment on the proximal pinnae. The Winston County, Alabama, populations are remarkably disjunct, about 2000 km from the nearest Mexican populations of both varieties.</discussion>
  
</bio:treatment>