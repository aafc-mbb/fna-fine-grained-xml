<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="Swartz" date="1806" rank="genus">cheilanthes</taxon_name>
    <taxon_name authority="(Maxon) Maxon in Abrams" date="1923" rank="species">intertexta</taxon_name>
    <place_of_publication>
      <publication_title>in Abrams,Ill. Fl. Pacific States</publication_title>
      <place_in_publication>1: 28. 1923</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus cheilanthes;species intertexta</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500355</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheilanthes</taxon_name>
    <taxon_name authority="Maxon" date="unknown" rank="species">covillei</taxon_name>
    <taxon_name authority="Maxon" date="unknown" rank="subspecies">intertexta</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>31: 149. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Cheilanthes;species covillei;subspecies intertexta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems short-creeping, usually 3–7 mm diam.;</text>
      <biological_entity id="o1647" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="short-creeping" value_original="short-creeping" />
        <character char_type="range_value" from="3" from_unit="mm" modifier="usually" name="diameter" src="d0_s0" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales usually bicolored, with broad, well-defined, dark, central stripe and narrow, light-brown margins, linear-lanceolate, straight to slightly contorted, strongly appressed, persistent.</text>
      <biological_entity id="o1648" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s1" value="bicolored" value_original="bicolored" />
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s1" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="central" id="o1649" name="stripe" name_original="stripe" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="broad" value_original="broad" />
        <character is_modifier="true" name="prominence" src="d0_s1" value="well-defined" value_original="well-defined" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o1650" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="arrangement_or_shape" src="d0_s1" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="strongly" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o1648" id="r379" name="with" negation="false" src="d0_s1" to="o1649" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves clustered, 4–25 cm;</text>
    </statement>
    <statement id="d0_s3">
      <text>vernation noncircinate.</text>
      <biological_entity id="o1651" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole dark-brown, rounded adaxially.</text>
      <biological_entity id="o1652" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade lanceolate to ovate-deltate, usually 3-pinnate at base, 1–4 cm wide;</text>
      <biological_entity id="o1653" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="ovate-deltate" />
        <character constraint="at base" constraintid="o1654" is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s5" value="3-pinnate" value_original="3-pinnate" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" notes="" src="d0_s5" to="4" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1654" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>rachis rounded adaxially, with scattered scales and sparse monomorphic pubescence.</text>
      <biological_entity id="o1655" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="count_or_density" notes="" src="d0_s6" value="sparse" value_original="sparse" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
      <biological_entity id="o1656" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o1655" id="r380" name="with" negation="false" src="d0_s6" to="o1656" />
    </statement>
    <statement id="d0_s7">
      <text>Pinnae not articulate, dark color of stalk continuing into pinna base, basal pair not conspicuously larger than adjacent pair, usually equilateral, appearing glabrous to sparsely pubescent adaxially.</text>
      <biological_entity id="o1657" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="articulate" value_original="articulate" />
        <character constraint="of stalk" constraintid="o1658" is_modifier="false" name="coloration" src="d0_s7" value="dark color" value_original="dark color" />
      </biological_entity>
      <biological_entity id="o1658" name="stalk" name_original="stalk" src="d0_s7" type="structure" />
      <biological_entity constraint="pinna" id="o1659" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o1660" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="not conspicuously larger than adjacent pair" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="equilateral" value_original="equilateral" />
        <character char_type="range_value" from="glabrous" modifier="adaxially" name="pubescence" src="d0_s7" to="sparsely pubescent" />
      </biological_entity>
      <relation from="o1658" id="r381" name="continuing into" negation="false" src="d0_s7" to="o1659" />
    </statement>
    <statement id="d0_s8">
      <text>Costae green adaxially for most of length;</text>
      <biological_entity id="o1661" name="costa" name_original="costae" src="d0_s8" type="structure">
        <character constraint="for" is_modifier="false" name="length" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>abaxial scales multiseriate, ovatelanceolate, deeply cordate at base, with overlapping basal lobes, conspicuous, the longest 0.4–1 mm wide, imbricate, often concealing ultimate segments, long-ciliate, cilia usually confined to proximal 1/2.</text>
      <biological_entity constraint="abaxial" id="o1662" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="multiseriate" value_original="multiseriate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character constraint="at base" constraintid="o1663" is_modifier="false" modifier="deeply" name="shape" src="d0_s9" value="cordate" value_original="cordate" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s9" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="length" src="d0_s9" value="longest" value_original="longest" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s9" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o1663" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity constraint="basal" id="o1664" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o1665" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="often" name="position" src="d0_s9" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
      <biological_entity id="o1666" name="cilium" name_original="cilia" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s9" value="1/2" value_original="1/2" />
      </biological_entity>
      <relation from="o1662" id="r382" name="with" negation="false" src="d0_s9" to="o1664" />
    </statement>
    <statement id="d0_s10">
      <text>Ultimate segments oblong to ovate, beadlike, the largest 1–3 mm, abaxially densely covered with branched hairs and small, ciliate scales, adaxially with scattered branched hairs or glabrescent.</text>
      <biological_entity constraint="ultimate" id="o1667" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="ovate" />
        <character is_modifier="false" name="shape" src="d0_s10" value="beadlike" value_original="beadlike" />
        <character is_modifier="false" name="size" src="d0_s10" value="largest" value_original="largest" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o1668" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o1669" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="branched" value_original="branched" />
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s10" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o1670" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o1667" id="r383" modifier="abaxially densely" name="covered with" negation="false" src="d0_s10" to="o1668" />
      <relation from="o1667" id="r384" modifier="abaxially densely" name="covered with" negation="false" src="d0_s10" to="o1669" />
      <relation from="o1667" id="r385" modifier="adaxially" name="with" negation="false" src="d0_s10" to="o1670" />
    </statement>
    <statement id="d0_s11">
      <text>False indusia marginal, weakly differentiated, 0.05–0.25 mm wide.</text>
      <biological_entity constraint="false" id="o1671" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="marginal" value_original="marginal" />
        <character is_modifier="false" modifier="weakly" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s11" to="0.25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sori ± continuous around segment margins.</text>
      <biological_entity id="o1672" name="sorus" name_original="sori" src="d0_s12" type="structure">
        <character constraint="around segment margins" constraintid="o1673" is_modifier="false" modifier="more or less" name="architecture" src="d0_s12" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity constraint="segment" id="o1673" name="margin" name_original="margins" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Sporangia containing 64 spores.</text>
      <biological_entity id="o1674" name="sporangium" name_original="sporangia" src="d0_s13" type="structure" />
      <biological_entity id="o1675" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="64" value_original="64" />
      </biological_entity>
      <relation from="o1674" id="r386" name="containing" negation="false" src="d0_s13" to="o1675" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating late spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and ledges, usually on igneous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="igneous substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9</number>
  <other_name type="common_name">Coastal lip fern</other_name>
  <discussion>Preliminary isozyme analyses support D. B. Lellinger's (1985) suggestion that Cheilanthes intertexta is a fertile allotetraploid hybrid between C. gracillima and C. covillei. It is morphologically most similar to the latter parent (see comments under C. covillei), but it is occasionally confused with C. gracillima, with which it apparently hybridizes to form sterile intermediates that have been called C. gracillima var. aberrans M. E. Jones. Cheilanthes intertexta may also be confused with C. clevelandii, with which it is partially sympatric. In addition to the characters given in the key, C. intertexta is distinguished from closely related sexual species by having larger spores averaging more than 55 µm in diameter.</discussion>
  
</bio:treatment>