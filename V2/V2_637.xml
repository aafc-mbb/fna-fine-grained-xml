<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Presl &amp; C. Presl" date="unknown" rank="family">polypodiaceae</taxon_name>
    <taxon_name authority="M. G. Price" date="1983" rank="genus">pecluma</taxon_name>
    <taxon_name authority="(Kunze) M. G. Price" date="1983" rank="species">ptilodon</taxon_name>
    <taxon_name authority="(Jenman) Lellinger" date="1985" rank="variety">caespitosa</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>98: 387. 1985</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polypodiaceae;genus pecluma;species ptilodon;variety caespitosa</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500870</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Kunze" date="unknown" rank="species">pectinatum</taxon_name>
    <taxon_name authority="Jenman" date="unknown" rank="variety">caespitosum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Bot. Dept., Jamaica, n.s.</publication_title>
      <place_in_publication>4: 125. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species pectinatum;variety caespitosum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">ptilodon</taxon_name>
    <taxon_name authority="(Jenman) A. M. Evans" date="unknown" rank="variety">caespitosum</taxon_name>
    <taxon_hierarchy>genus Polypodium;species ptilodon;variety caespitosum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 5–8 mm diam.;</text>
      <biological_entity id="o16851" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s0" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales blackish, linear-lanceolate.</text>
      <biological_entity id="o16852" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect, or arching in large plants.</text>
      <biological_entity id="o16853" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character constraint="in plants" constraintid="o16854" is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
      </biological_entity>
      <biological_entity id="o16854" name="plant" name_original="plants" src="d0_s2" type="structure">
        <character is_modifier="true" name="size" src="d0_s2" value="large" value_original="large" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole dark-brown, less than 1/10 length of blade, pubescent with both simple and branched, short, multicellular hairs;</text>
      <biological_entity id="o16855" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="dark-brown" value_original="dark-brown" />
        <character char_type="range_value" from="0 length of blade" name="length" src="d0_s3" to="1/10 length of blade" />
        <character constraint="with hairs" constraintid="o16856" is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o16856" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s3" value="short" value_original="short" />
        <character is_modifier="true" name="architecture" src="d0_s3" value="multicellular" value_original="multicellular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>scales threadlike, linear, scattered, inconspicuous.</text>
      <biological_entity id="o16857" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="thread-like" value_original="threadlike" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade lanceolate, 25–90 × 6.5–18 cm;</text>
      <biological_entity id="o16858" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s5" to="90" to_unit="cm" />
        <character char_type="range_value" from="6.5" from_unit="cm" name="width" src="d0_s5" to="18" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>base narrowly cuneate;</text>
      <biological_entity id="o16859" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>apex acute.</text>
      <biological_entity id="o16860" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Segments lanceolate, 4–8 mm wide, sparsely pubescent, at base of blade reduced to auricles.</text>
      <biological_entity id="o16861" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s8" to="8" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s8" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o16862" name="base" name_original="base" src="d0_s8" type="structure">
        <character constraint="to auricles" constraintid="o16864" is_modifier="false" name="size" src="d0_s8" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o16863" name="blade" name_original="blade" src="d0_s8" type="structure" />
      <biological_entity id="o16864" name="auricle" name_original="auricles" src="d0_s8" type="structure" />
      <relation from="o16861" id="r3704" name="at" negation="false" src="d0_s8" to="o16862" />
      <relation from="o16862" id="r3705" name="part_of" negation="false" src="d0_s8" to="o16863" />
    </statement>
    <statement id="d0_s9">
      <text>Veins 2–3-forked.</text>
      <biological_entity id="o16865" name="vein" name_original="veins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="2-3-forked" value_original="2-3-forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Sori oval, blade pubescence denser and longer around sorus;</text>
      <biological_entity id="o16866" name="sorus" name_original="sori" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oval" value_original="oval" />
      </biological_entity>
      <biological_entity id="o16867" name="blade" name_original="blade" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="denser" value_original="denser" />
        <character constraint="around sorus" constraintid="o16868" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o16868" name="sorus" name_original="sorus" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>sporangia with 64 spores.</text>
      <biological_entity id="o16870" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="64" value_original="64" />
      </biological_entity>
      <relation from="o16869" id="r3706" name="with" negation="false" src="d0_s11" to="o16870" />
    </statement>
    <statement id="d0_s12">
      <text>2n = 148.</text>
      <biological_entity id="o16869" name="sporangium" name_original="sporangia" src="d0_s11" type="structure" />
      <biological_entity constraint="2n" id="o16871" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="148" value_original="148" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating all year.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial, or on logs or tree bases, in hammocks, swamps, and wet woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tree bases" modifier="terrestrial or on logs" constraint="in hammocks , swamps , and wet woods" />
        <character name="habitat" value="hammocks" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="wet woods" />
        <character name="habitat" value="terrestrial" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a</number>
  
</bio:treatment>