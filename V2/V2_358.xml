<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Michaux ex DeCandolle" date="unknown" rank="family">equisetaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">equisetum</taxon_name>
    <taxon_name authority="(J. Milde) Baker" date="1887" rank="subgenus">Hippochaete</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">palustre</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1061. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family equisetaceae;genus equisetum;subgenus hippochaete;species palustre;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500621</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Equisetum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">palustre</taxon_name>
    <taxon_name authority="Victorin" date="unknown" rank="variety">americanum</taxon_name>
    <taxon_hierarchy>genus Equisetum;species palustre;variety americanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Aerial stems monomorphic, green, branched or unbranched, 20–80 cm;</text>
      <biological_entity id="o11909" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="location" src="d0_s0" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s0" to="80" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>hollow center small, to 1/3 stem diam.;</text>
      <biological_entity id="o11910" name="center" name_original="center" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="size" src="d0_s1" value="small" value_original="small" />
        <character char_type="range_value" from="0" name="quantity" src="d0_s1" to="1/3" />
      </biological_entity>
      <biological_entity id="o11911" name="stem" name_original="stem" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>vallecular canals nearly as large.</text>
      <biological_entity constraint="vallecular" id="o11912" name="canal" name_original="canals" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Sheaths elongate, 4–9 × 2–5 mm;</text>
      <biological_entity id="o11913" name="sheath" name_original="sheaths" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elongate" value_original="elongate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="9" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s3" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>teeth dark, 5–10, narrow, 2–5 mm, margins white, scarious.</text>
      <biological_entity id="o11914" name="tooth" name_original="teeth" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark" value_original="dark" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s4" to="10" />
        <character is_modifier="false" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11915" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="white" value_original="white" />
        <character is_modifier="false" name="texture" src="d0_s4" value="scarious" value_original="scarious" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Branches when present only from midstem nodes, spreading, hollow;</text>
      <biological_entity id="o11916" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="when present only from midstem nodes" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="hollow" value_original="hollow" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>ridges 4–6;</text>
      <biological_entity id="o11917" name="ridge" name_original="ridges" src="d0_s6" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s6" to="6" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>valleys rounded;</text>
    </statement>
    <statement id="d0_s8">
      <text>1st internode of each branch shorter than subtending stem sheath;</text>
      <biological_entity id="o11918" name="valley" name_original="valleys" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o11919" name="internode" name_original="internode" src="d0_s8" type="structure">
        <character constraint="than subtending stem sheath" constraintid="o11921" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o11920" name="branch" name_original="branch" src="d0_s8" type="structure" />
      <biological_entity constraint="stem" id="o11921" name="sheath" name_original="sheath" src="d0_s8" type="structure" constraint_original="subtending stem" />
      <relation from="o11919" id="r2572" name="part_of" negation="false" src="d0_s8" to="o11920" />
    </statement>
    <statement id="d0_s9">
      <text>sheath teeth narrow.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n =216.</text>
      <biological_entity constraint="sheath" id="o11922" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11923" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="216" value_original="216" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Cones maturing in summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="cones maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Marshes and swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="marshes" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1500m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Calif., Idaho, Maine, Mich., Minn., Mont., N.H., N.Y., N.Dak., Oreg., Pa., Vt., Wash., Wis.; Eurasia s to Himalayas, n China, Korea, Japan.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Eurasia s to Himalayas" establishment_means="native" />
        <character name="distribution" value="n China" establishment_means="native" />
        <character name="distribution" value="Korea" establishment_means="native" />
        <character name="distribution" value="Japan" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">Marsh horsetail</other_name>
  <other_name type="common_name">prêle des marais</other_name>
  <discussion>The name Equisetum palustre var. americanum has been used for specimens from the flora that have longer teeth than those from Eurasia.</discussion>
  
</bio:treatment>