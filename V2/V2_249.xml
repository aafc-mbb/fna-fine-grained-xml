<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Bartlett" date="unknown" rank="family">cupressaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">juniperus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="section">Juniperus</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">communis</taxon_name>
    <taxon_name authority="Pursh" date="1814" rank="variety">depressa</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 646. 1814</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cupressaceae;genus juniperus;section juniperus;species communis;variety depressa;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500725</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Juniperus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">communis</taxon_name>
    <taxon_name authority="(Pursh) Franco" date="unknown" rank="subspecies">depressa</taxon_name>
    <taxon_hierarchy>genus Juniperus;species communis;subspecies depressa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shrubs prostrate or low with ascending branchlet tips (occasionally spreading shrubs to 3 m, rarely small trees to 10 m).</text>
      <biological_entity id="o8669" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character constraint="with branchlet tips" constraintid="o8670" is_modifier="false" name="position" src="d0_s0" value="low" value_original="low" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
      <biological_entity constraint="branchlet" id="o8670" name="tip" name_original="tips" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves upturned, to 15 × 1.6 mm, rarely spreading, linear, glaucous stomatal band about as wide as each green marginal band, apex acute and mucronate to acuminate.</text>
      <biological_entity id="o8671" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="upturned" value_original="upturned" />
        <character char_type="range_value" from="0" from_unit="mm" name="length" src="d0_s1" to="15" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s1" to="1.6" to_unit="mm" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s1" value="linear" value_original="linear" />
      </biological_entity>
      <biological_entity constraint="stomatal" id="o8672" name="band" name_original="band" src="d0_s1" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s1" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o8673" name="band" name_original="band" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o8674" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="acute" value_original="acute" />
        <character char_type="range_value" from="mucronate" name="shape" src="d0_s1" to="acuminate" />
      </biological_entity>
      <relation from="o8672" id="r1838" name="as wide as" negation="false" src="d0_s1" to="o8673" />
    </statement>
    <statement id="d0_s2">
      <text>Seed-cones 6–9 mm, shorter than leaves.</text>
      <biological_entity id="o8676" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>2n = 22.</text>
      <biological_entity id="o8675" name="cone-seed" name_original="seed-cones" src="d0_s2" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="distance" src="d0_s2" to="9" to_unit="mm" />
        <character constraint="than leaves" constraintid="o8676" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8677" name="chromosome" name_original="" src="d0_s3" type="structure">
        <character name="quantity" src="d0_s3" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky soil, slopes, and summits</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky soil" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="summits" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2800" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Conn., Ga., Idaho, Ill., Ind., Maine, Mass., Mich., Minn., Mont., Nev., N.H., N.Mex., N.Y., N.C., N.Dak., Ohio, Oreg., Pa., R.I., S.C., S.Dak., Utah, Vt., Va., Wash., Wis., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1c</number>
  <discussion>In the flora, larger individuals of this variety (to 10 m) have been misidentified as var. communis.</discussion>
  
</bio:treatment>