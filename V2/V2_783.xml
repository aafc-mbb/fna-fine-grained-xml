<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">abies</taxon_name>
    <taxon_name authority="(Linnaeus) Miller" date="1768" rank="species">balsamea</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict., ed. 8</publication_title>
      <place_in_publication>Abies no. 3. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus abies;species balsamea</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500002</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pinus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">balsamea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1002. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pinus;species balsamea;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 23m;</text>
      <biological_entity id="o2325" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="23" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 0.6m diam.;</text>
      <biological_entity id="o2326" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="0.6" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>crown spirelike.</text>
      <biological_entity id="o2327" name="crown" name_original="crown" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="spirelike" value_original="spirelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark gray, thin, smooth, in age often becoming broken into irregular brownish scales.</text>
      <biological_entity id="o2328" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray" value_original="gray" />
        <character is_modifier="false" name="width" src="d0_s3" value="thin" value_original="thin" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s3" value="smooth" value_original="smooth" />
        <character constraint="into scales" constraintid="o2329" is_modifier="false" modifier="often becoming" name="condition_or_fragility" src="d0_s3" value="broken" value_original="broken" />
      </biological_entity>
      <biological_entity id="o2329" name="scale" name_original="scales" src="d0_s3" type="structure">
        <character is_modifier="true" name="architecture_or_course" src="d0_s3" value="irregular" value_original="irregular" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="brownish" value_original="brownish" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Branches diverging from trunk at right angles, the lower often spreading and drooping;</text>
      <biological_entity id="o2330" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character constraint="from trunk" constraintid="o2331" is_modifier="false" name="orientation" src="d0_s4" value="diverging" value_original="diverging" />
        <character is_modifier="false" name="position" notes="" src="d0_s4" value="lower" value_original="lower" />
        <character is_modifier="false" modifier="often" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="drooping" value_original="drooping" />
      </biological_entity>
      <biological_entity id="o2331" name="trunk" name_original="trunk" src="d0_s4" type="structure" />
      <biological_entity id="o2332" name="angle" name_original="angles" src="d0_s4" type="structure" />
      <relation from="o2331" id="r523" name="at" negation="false" src="d0_s4" to="o2332" />
    </statement>
    <statement id="d0_s5">
      <text>twigs mostly opposite, greenish brown, pubescence sparse.</text>
      <biological_entity id="o2333" name="twig" name_original="twigs" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s5" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="greenish brown" value_original="greenish brown" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Buds hidden by leaves or exposed, brown, conic, small, resinous, apex acute;</text>
      <biological_entity id="o2334" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character constraint="by " constraintid="o2336" is_modifier="false" name="prominence" src="d0_s6" value="hidden" value_original="hidden" />
      </biological_entity>
      <biological_entity id="o2335" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <biological_entity id="o2336" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="exposed" value_original="exposed" />
        <character is_modifier="true" name="coloration" src="d0_s6" value="brown" value_original="brown" />
        <character is_modifier="true" name="shape" src="d0_s6" value="conic" value_original="conic" />
        <character is_modifier="true" name="size" src="d0_s6" value="small" value_original="small" />
        <character is_modifier="true" name="coating" src="d0_s6" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>basal scales short, broad, nearly equilaterally triangular, glabrous, resinous, margins entire, apex sharp-pointed.</text>
      <biological_entity constraint="basal" id="o2337" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="short" value_original="short" />
        <character is_modifier="false" name="width" src="d0_s7" value="broad" value_original="broad" />
        <character is_modifier="false" modifier="nearly equilaterally" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="coating" src="d0_s7" value="resinous" value_original="resinous" />
      </biological_entity>
      <biological_entity id="o2338" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o2339" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="sharp-pointed" value_original="sharp-pointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Leaves 1.2–2.5cm × 1.5–2mm, 1-ranked (particularly on lower branches) to spiraled, flexible;</text>
      <biological_entity id="o2340" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.2" from_unit="cm" name="length" src="d0_s8" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
        <character char_type="range_value" from="1-ranked" name="arrangement" src="d0_s8" to="spiraled" />
        <character is_modifier="false" name="fragility" src="d0_s8" value="pliable" value_original="flexible" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>cross-section flat, grooved adaxially;</text>
    </statement>
    <statement id="d0_s10">
      <text>odor pinelike (copious ß-pinene);</text>
      <biological_entity id="o2341" name="cross-section" name_original="cross-section" src="d0_s9" type="structure">
        <character is_modifier="false" name="prominence_or_shape" src="d0_s9" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s9" value="grooved" value_original="grooved" />
        <character is_modifier="false" name="odor" src="d0_s10" value="pinelike" value_original="pinelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>abaxial surface with (4–) 6–7 (–8) stomatal rows on each side of midrib;</text>
      <biological_entity constraint="abaxial" id="o2342" name="surface" name_original="surface" src="d0_s11" type="structure" />
      <biological_entity id="o2343" name="stomatal" name_original="stomatal" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="atypical_quantity" src="d0_s11" to="6" to_inclusive="false" />
        <character char_type="range_value" from="7" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s11" to="8" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s11" to="7" />
      </biological_entity>
      <biological_entity id="o2344" name="row" name_original="rows" src="d0_s11" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="atypical_quantity" src="d0_s11" to="6" to_inclusive="false" />
        <character char_type="range_value" from="7" from_inclusive="false" is_modifier="true" name="atypical_quantity" src="d0_s11" to="8" />
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s11" to="7" />
      </biological_entity>
      <biological_entity id="o2345" name="side" name_original="side" src="d0_s11" type="structure" />
      <biological_entity id="o2346" name="midrib" name_original="midrib" src="d0_s11" type="structure" />
      <relation from="o2342" id="r524" name="with" negation="false" src="d0_s11" to="o2343" />
      <relation from="o2342" id="r525" name="with" negation="false" src="d0_s11" to="o2344" />
      <relation from="o2343" id="r526" name="on" negation="false" src="d0_s11" to="o2345" />
      <relation from="o2344" id="r527" name="on" negation="false" src="d0_s11" to="o2345" />
      <relation from="o2345" id="r528" name="part_of" negation="false" src="d0_s11" to="o2346" />
    </statement>
    <statement id="d0_s12">
      <text>adaxial surface dark green, slightly or not glaucous, with 0–3 stomatal rows at midleaf, these more numerous toward leaf apex;</text>
      <biological_entity constraint="adaxial" id="o2347" name="surface" name_original="surface" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="slightly; not" name="pubescence" src="d0_s12" value="glaucous" value_original="glaucous" />
        <character constraint="toward leaf apex" constraintid="o2351" is_modifier="false" name="quantity" notes="" src="d0_s12" value="numerous" value_original="numerous" />
      </biological_entity>
      <biological_entity id="o2348" name="stomatal" name_original="stomatal" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s12" to="3" />
      </biological_entity>
      <biological_entity id="o2349" name="row" name_original="rows" src="d0_s12" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s12" to="3" />
      </biological_entity>
      <biological_entity id="o2350" name="midleaf" name_original="midleaf" src="d0_s12" type="structure" />
      <biological_entity constraint="leaf" id="o2351" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <relation from="o2347" id="r529" name="with" negation="false" src="d0_s12" to="o2348" />
      <relation from="o2347" id="r530" name="with" negation="false" src="d0_s12" to="o2349" />
      <relation from="o2348" id="r531" name="at" negation="false" src="d0_s12" to="o2350" />
      <relation from="o2349" id="r532" name="at" negation="false" src="d0_s12" to="o2350" />
    </statement>
    <statement id="d0_s13">
      <text>apex slightly notched to rounded;</text>
      <biological_entity id="o2352" name="apex" name_original="apex" src="d0_s13" type="structure">
        <character char_type="range_value" from="slightly notched" name="shape" src="d0_s13" to="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>resin canals large, ± median, away from margins, midway between abaxial and adaxial epidermal layers.</text>
      <biological_entity constraint="resin" id="o2353" name="canal" name_original="canals" src="d0_s14" type="structure">
        <character is_modifier="false" name="size" src="d0_s14" value="large" value_original="large" />
        <character is_modifier="false" modifier="more or less" name="position" src="d0_s14" value="median" value_original="median" />
        <character constraint="between abaxial adaxial epidermal, layers" constraintid="o2355, o2356" is_modifier="false" name="position" notes="" src="d0_s14" value="midway" value_original="midway" />
      </biological_entity>
      <biological_entity id="o2354" name="margin" name_original="margins" src="d0_s14" type="structure" />
      <biological_entity constraint="abaxial and adaxial" id="o2355" name="epidermal" name_original="epidermal" src="d0_s14" type="structure" />
      <biological_entity constraint="abaxial and adaxial" id="o2356" name="layer" name_original="layers" src="d0_s14" type="structure" />
      <relation from="o2353" id="r533" name="away from" negation="false" src="d0_s14" to="o2354" />
    </statement>
    <statement id="d0_s15">
      <text>Pollen cones at pollination red, purplish, bluish, greenish, or orange.</text>
      <biological_entity constraint="pollen" id="o2357" name="cone" name_original="cones" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s15" value="purplish" value_original="purplish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="bluish" value_original="bluish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="orange" value_original="orange" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s15" value="orange" value_original="orange" />
      </biological_entity>
      <biological_entity id="o2358" name="pollination" name_original="pollination" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="red" value_original="red" />
      </biological_entity>
      <relation from="o2357" id="r534" name="at" negation="false" src="d0_s15" to="o2358" />
    </statement>
    <statement id="d0_s16">
      <text>Seed-cones cylindric, 4–7 × 1.5–3cm, gray-purple, turning brown before scale shed, sessile, apex round to obtuse;</text>
      <biological_entity id="o2359" name="seed-cone" name_original="seed-cones" src="d0_s16" type="structure">
        <character is_modifier="false" name="shape" src="d0_s16" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s16" to="7" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s16" to="3" to_unit="cm" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s16" value="gray-purple" value_original="gray-purple" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o2360" name="scale" name_original="scale" src="d0_s16" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s16" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o2361" name="apex" name_original="apex" src="d0_s16" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s16" to="obtuse" />
      </biological_entity>
      <relation from="o2359" id="r535" name="turning" negation="false" src="d0_s16" to="o2360" />
    </statement>
    <statement id="d0_s17">
      <text>scales ca. 1–l. 5 × 0.7–1.7cm (relationship reversed in more western collections), pubescent;</text>
      <biological_entity id="o2362" name="scale" name_original="scales" src="d0_s17" type="structure">
        <character name="length" src="d0_s17" unit="cm" value="5" value_original="5" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="width" src="d0_s17" to="1.7" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>bracts included or exserted and reflexed over scales.</text>
      <biological_entity id="o2363" name="bract" name_original="bracts" src="d0_s18" type="structure">
        <character is_modifier="false" name="position" src="d0_s18" value="included" value_original="included" />
        <character is_modifier="false" name="position" src="d0_s18" value="exserted" value_original="exserted" />
        <character constraint="over scales" constraintid="o2364" is_modifier="false" name="orientation" src="d0_s18" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o2364" name="scale" name_original="scales" src="d0_s18" type="structure" />
    </statement>
    <statement id="d0_s19">
      <text>Seeds 3–6 × 2–3mm, body brown;</text>
      <biological_entity id="o2365" name="seed" name_original="seeds" src="d0_s19" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s19" to="6" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s19" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2366" name="body" name_original="body" src="d0_s19" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s19" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>wing about twice as long as body, brown-purple;</text>
      <biological_entity id="o2367" name="wing" name_original="wing" src="d0_s20" type="structure">
        <character constraint="body" constraintid="o2368" is_modifier="false" name="length" src="d0_s20" value="2 times as long as body" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s20" value="brown-purple" value_original="brown-purple" />
      </biological_entity>
      <biological_entity id="o2368" name="body" name_original="body" src="d0_s20" type="structure" />
    </statement>
    <statement id="d0_s21">
      <text>cotyledons ca. 4.2n =24.</text>
      <biological_entity id="o2369" name="cotyledon" name_original="cotyledons" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="4" value_original="4" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2370" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Boreal and northern forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="boreal and northern forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1700m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>St. Pierre and Miquelon; Alta., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask.; Conn., Iowa, Maine, Mass., Mich., Minn., N.H., N.Y., Pa., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <other_name type="common_name">Balsam fir</other_name>
  <other_name type="common_name">sapin baumler</other_name>
  <discussion>Balsam fir is frequently segregated into two varieties (e.g., H.J. Scoggan 1978–1979) based on whether the bracts are included (var. balsamea) or exserted (var. phanerolepis Fernald), the latter considered by Liu T. S. (1971) to be a hybrid between Abies balsamea and A. fraseri. D.T. Lester (1968) demonstrated, however, that bract length may vary within a cone, annually, and from tree to tree. Nevertheless, a tendency exists for the exserted variety to be found most commonly from Newfoundland south through New England (R.C. Hosie 1969; B.F. Jacobs et al. 1984); it is not found west of Ontario. Western populations lack 3-carene and have other minor chemical differences separating them from eastern balsam fir (E.Zavarin and K.Snajberk 1972; R.S. Hunt and E.von Rudloff 1974). Morphologic variation in balsam fir has been studied mainly east of Ontario; the populations to the west have been ignored for the most part, although they may yield stronger evidence for species subdivision.</discussion>
  <discussion>In Alberta, populations intermediate between western Abies balsamea and A. bifolia (E.H. Moss 1953; R.S. Hunt and E.von Rudloff 1974, 1979) may be classified as A. balsamea × bifolia. In West Virginia and Virginia, populations of balsam fir tend to be more similar to A. fraseri than are more northern populations (B.F. Jacobs et al. 1984).</discussion>
  <discussion>Balsam fir (Abies balsamea) is the provincial tree of New Brunswick.</discussion>
  <references>
    <reference>Lester, D.T. 1968. Variation in cone morphology of balsam fir,  Abies balsamea. Rhodora 70: 83–94.</reference>
  </references>
  
</bio:treatment>