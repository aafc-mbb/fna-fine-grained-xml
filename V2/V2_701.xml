<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Newman" date="1851" rank="genus">gymnocarpium</taxon_name>
    <taxon_name authority="(Ruprecht) Ching" date="1965" rank="species">disjunctum</taxon_name>
    <place_of_publication>
      <publication_title>Acta Phytotax. Sin.</publication_title>
      <place_in_publication>10: 304. 1965</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus gymnocarpium;species disjunctum</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500657</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">dryopteris</taxon_name>
    <taxon_name authority="Ruprecht" date="unknown" rank="variety">disjunctum</taxon_name>
    <place_of_publication>
      <publication_title>Distr. Crypt. Vasc. Ross.,</publication_title>
      <place_in_publication>52. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species dryopteris;variety disjunctum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryopteris</taxon_name>
    <taxon_name authority="(Ruprecht) C. V. Morton" date="unknown" rank="species">disjuncta</taxon_name>
    <taxon_hierarchy>genus Dryopteris;species disjuncta;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gymnocarpium</taxon_name>
    <taxon_name authority="(Linnaeus) Newman" date="unknown" rank="species">dryopteris</taxon_name>
    <taxon_name authority="(Ruprecht) Sarvela" date="unknown" rank="subspecies">disjunctum</taxon_name>
    <taxon_hierarchy>genus Gymnocarpium;species dryopteris;subspecies disjunctum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gymnocarpium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">dryopteris</taxon_name>
    <taxon_name authority="(Ruprecht) Ching" date="unknown" rank="variety">disjunctum</taxon_name>
    <taxon_hierarchy>genus Gymnocarpium;species dryopteris;variety disjunctum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 1–3 mm diam.;</text>
      <biological_entity id="o6954" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s0" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales 2–4 mm.</text>
      <biological_entity id="o6955" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Fertile leaves usually 20–68 cm.</text>
      <biological_entity id="o6956" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s2" to="68" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 12–44 cm with sparse glandular-hairs distally;</text>
      <biological_entity id="o6957" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" constraint="with glandular-hairs" constraintid="o6958" from="12" from_unit="cm" name="some_measurement" src="d0_s3" to="44" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6958" name="glandular-hair" name_original="glandular-hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>scales 2–6 mm.</text>
      <biological_entity id="o6959" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade broadly deltate, 3-pinnate-pinnatifid, 8–24 cm, lax and delicate, abaxial surface and rachis glabrous or with sparse glandular-hairs, adaxial surface glabrous.</text>
      <biological_entity id="o6960" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="3-pinnate-pinnatifid" value_original="3-pinnate-pinnatifid" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s5" to="24" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="delicate" value_original="delicate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6961" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="with sparse glandular-hairs" value_original="with sparse glandular-hairs" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o6962" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6963" name="glandular-hair" name_original="glandular-hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s5" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o6964" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o6961" id="r1465" name="with" negation="false" src="d0_s5" to="o6963" />
      <relation from="o6962" id="r1466" name="with" negation="false" src="d0_s5" to="o6963" />
    </statement>
    <statement id="d0_s6">
      <text>Pinna apex acuminate.</text>
      <biological_entity constraint="pinna" id="o6965" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Proximal pinnae 5–18 cm, ± perpendicular to rachis, with basiscopic pinnules ± perpendicular to costa;</text>
      <biological_entity constraint="proximal" id="o6966" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s7" to="18" to_unit="cm" />
        <character constraint="to rachis" constraintid="o6967" is_modifier="false" modifier="more or less" name="orientation" src="d0_s7" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o6967" name="rachis" name_original="rachis" src="d0_s7" type="structure" />
      <biological_entity id="o6968" name="pinnule" name_original="pinnules" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="basiscopic" value_original="basiscopic" />
        <character constraint="to costa" constraintid="o6969" is_modifier="false" modifier="more or less" name="orientation" src="d0_s7" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o6969" name="costa" name_original="costa" src="d0_s7" type="structure" />
      <relation from="o6966" id="r1467" name="with" negation="false" src="d0_s7" to="o6968" />
    </statement>
    <statement id="d0_s8">
      <text>basal basiscopic pinnule sessile, pinnate-pinnatifid (with basal pinnulets, and sometimes 2 adjacent pinnulets, separate), basal basiscopic pinnulet usually longer (sometimes equaling or shorter) than adjacent pinnulet;</text>
      <biological_entity constraint="basal" id="o6970" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o6971" name="pinnule" name_original="pinnule" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s8" value="pinnate-pinnatifid" value_original="pinnate-pinnatifid" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6972" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o6973" name="pinnulet" name_original="pinnulet" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character constraint="than adjacent pinnulet" constraintid="o6974" is_modifier="false" name="length_or_size" src="d0_s8" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity id="o6974" name="pinnulet" name_original="pinnulet" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2d basal basiscopic pinnule sessile with basal basiscopic pinnulet usually longer than or equaling adjacent pinnulet;</text>
      <biological_entity constraint="basal" id="o6975" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o6976" name="pinnule" name_original="pinnule" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character constraint="with basal pinnae" constraintid="o6977" is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6977" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o6978" name="pinnulet" name_original="pinnulet" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character constraint="than or equaling adjacent pinnulet" constraintid="o6979" is_modifier="false" name="length_or_size" src="d0_s9" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity id="o6979" name="pinnulet" name_original="pinnulet" src="d0_s9" type="structure">
        <character is_modifier="true" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s9" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>basal acroscopic pinnule sessile, with basal basiscopic pinnulet usually longer than or equaling adjacent pinnulet.</text>
      <biological_entity constraint="basal" id="o6980" name="pinna" name_original="pinnae" src="d0_s10" type="structure" />
      <biological_entity id="o6981" name="pinnule" name_original="pinnule" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6982" name="pinna" name_original="pinnae" src="d0_s10" type="structure" />
      <biological_entity id="o6983" name="pinnulet" name_original="pinnulet" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="basiscopic" value_original="basiscopic" />
        <character constraint="than or equaling adjacent pinnulet" constraintid="o6984" is_modifier="false" name="length_or_size" src="d0_s10" value="usually longer" value_original="usually longer" />
      </biological_entity>
      <biological_entity id="o6984" name="pinnulet" name_original="pinnulet" src="d0_s10" type="structure">
        <character is_modifier="true" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s10" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o6981" id="r1468" name="with" negation="false" src="d0_s10" to="o6982" />
    </statement>
    <statement id="d0_s11">
      <text>Pinnae of 2d pair usually sessile with basal basiscopic pinnule longer than or equaling adjacent pinnule and markedly longer than basal acroscopic pinnule;</text>
      <biological_entity id="o6985" name="pinna" name_original="pinnae" src="d0_s11" type="structure">
        <character constraint="with basal pinnae" constraintid="o6987" is_modifier="false" modifier="usually" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o6986" name="pair" name_original="pair" src="d0_s11" type="structure" />
      <biological_entity constraint="basal" id="o6987" name="pinna" name_original="pinnae" src="d0_s11" type="structure" />
      <biological_entity id="o6988" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s11" value="basiscopic" value_original="basiscopic" />
        <character constraint="than or equaling adjacent pinnule" constraintid="o6989" is_modifier="false" name="length_or_size" src="d0_s11" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o6989" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s11" value="adjacent" value_original="adjacent" />
        <character constraint="than basal acroscopic pinnule" constraintid="o6990" is_modifier="false" name="length_or_size" notes="" src="d0_s11" value="markedly longer" value_original="markedly longer" />
      </biological_entity>
      <biological_entity constraint="basal" id="o6990" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s11" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o6985" id="r1469" name="part_of" negation="false" src="d0_s11" to="o6986" />
    </statement>
    <statement id="d0_s12">
      <text>basal acroscopic pinnule distinctly shorter than adjacent pinnule or rarely absent, apex often crenulate, obtuse.</text>
      <biological_entity constraint="basal" id="o6991" name="pinna" name_original="pinnae" src="d0_s12" type="structure" />
      <biological_entity id="o6992" name="pinnule" name_original="pinnule" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="acroscopic" value_original="acroscopic" />
        <character constraint="than adjacent pinnule" constraintid="o6993" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="distinctly shorter" value_original="distinctly shorter" />
        <character is_modifier="false" modifier="rarely" name="presence" notes="" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6993" name="pinnule" name_original="pinnule" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o6994" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="often" name="shape" src="d0_s12" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pinnae of 3d pair usually sessile with basal basiscopic pinnule longer than or equaling adjacent pinnule and longer than basal acroscopic pinnule;</text>
      <biological_entity id="o6995" name="pinna" name_original="pinnae" src="d0_s13" type="structure">
        <character constraint="with basal pinnae" constraintid="o6997" is_modifier="false" modifier="usually" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o6996" name="pair" name_original="pair" src="d0_s13" type="structure" />
      <biological_entity constraint="basal" id="o6997" name="pinna" name_original="pinnae" src="d0_s13" type="structure" />
      <biological_entity id="o6998" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="basiscopic" value_original="basiscopic" />
        <character constraint="than or equaling adjacent pinnule" constraintid="o6999" is_modifier="false" name="length_or_size" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o6999" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="length_or_size" notes="" src="d0_s13" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="basal" id="o7000" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o6995" id="r1470" name="part_of" negation="false" src="d0_s13" to="o6996" />
    </statement>
    <statement id="d0_s14">
      <text>basal acroscopic pinnule shorter than adjacent pinnule.</text>
      <biological_entity constraint="basal" id="o7001" name="pinna" name_original="pinnae" src="d0_s14" type="structure" />
      <biological_entity id="o7002" name="pinnule" name_original="pinnule" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="acroscopic" value_original="acroscopic" />
        <character constraint="than adjacent pinnule" constraintid="o7003" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o7003" name="pinnule" name_original="pinnule" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s14" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Ultimate segments of proximal pinnae oblong, crenate to slightly lobed, apex crenulate, acute.</text>
      <biological_entity constraint="ultimate" id="o7004" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="crenate" name="shape" src="d0_s15" to="slightly lobed" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o7005" name="pinna" name_original="pinnae" src="d0_s15" type="structure" />
      <biological_entity id="o7006" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s15" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o7004" id="r1471" name="part_of" negation="false" src="d0_s15" to="o7005" />
    </statement>
    <statement id="d0_s16">
      <text>Spores 27–31 µm. 2n = 80.</text>
      <biological_entity id="o7007" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="27" from_unit="um" name="some_measurement" src="d0_s16" to="31" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7008" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded, rocky slopes and ravines, mixed coniferous woods, moist stream and creek banks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" modifier="shaded" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="mixed coniferous woods" />
        <character name="habitat" value="moist stream" />
        <character name="habitat" value="creek banks" />
        <character name="habitat" value="shaded" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Alaska, Idaho, Mont., Oreg., Wash., Wyo.; Asia in ne former Soviet republics.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Asia in ne former Soviet republics" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">Western oak fern</other_name>
  <discussion>In addition to the west coast of North America, Gymnocarpium disjunctum is found on Sakhalin Island in southern Kamchatka, in the former Soviet republics.</discussion>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>