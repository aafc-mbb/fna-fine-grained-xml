<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Clifton E. Nauman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">Pteris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1073. 1753; Gen. Pl. ed 5, 484. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus Pteris</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pteris, fern, derived from pteron, wing or feather, for the closely spaced pinnae, which give the leaves a likeness to feathers</other_info_on_name>
    <other_info_on_name type="fna_id">127430</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial or on rock.</text>
      <biological_entity id="o17398" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="on rock" is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="on rock" value_original="on rock" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect or creeping, branched;</text>
      <biological_entity id="o17399" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>scales pale-brown to black, concolored, elongate, margins entire.</text>
      <biological_entity id="o17400" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character char_type="range_value" from="pale-brown" name="coloration" src="d0_s2" to="black" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="concolored" value_original="concolored" />
        <character is_modifier="false" name="shape" src="d0_s2" value="elongate" value_original="elongate" />
      </biological_entity>
      <biological_entity id="o17401" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves monomorphic, clustered or closely spaced, 1–20 dm.</text>
      <biological_entity id="o17402" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="clustered" value_original="clustered" />
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s3" value="spaced" value_original="spaced" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s3" to="20" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole straw-colored, green, brownish red to purple black, longitudinally ridged, 2–3-grooved adaxially, scaly at base, glabrous or scaly distally, with 1 (less often 2 or more) vascular-bundle.</text>
      <biological_entity id="o17403" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="straw-colored" value_original="straw-colored" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="brownish red" name="coloration" src="d0_s4" to="purple black" />
        <character is_modifier="false" modifier="longitudinally" name="shape" src="d0_s4" value="ridged" value_original="ridged" />
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s4" value="2-3-grooved" value_original="2-3-grooved" />
        <character constraint="at base" constraintid="o17404" is_modifier="false" name="architecture_or_pubescence" src="d0_s4" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="distally" name="pubescence" src="d0_s4" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o17404" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o17405" name="vascular-bundle" name_original="vascular-bundle" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="1" value_original="1" />
      </biological_entity>
      <relation from="o17403" id="r3802" name="with" negation="false" src="d0_s4" to="o17405" />
    </statement>
    <statement id="d0_s5">
      <text>Blade oblong to lanceolate to deltate, 1–4-pinnate, herbaceous to leathery, abaxially and adaxially glabrous or sometimes pubescent or scaly, adaxially dull, not striate;</text>
      <biological_entity id="o17406" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s5" to="lanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="1-4-pinnate" value_original="1-4-pinnate" />
        <character char_type="range_value" from="herbaceous" name="texture" src="d0_s5" to="leathery" />
        <character is_modifier="false" modifier="abaxially; adaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s5" value="sometimes" value_original="sometimes" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="scaly" value_original="scaly" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s5" value="dull" value_original="dull" />
        <character is_modifier="false" modifier="not" name="coloration_or_pubescence_or_relief" src="d0_s5" value="striate" value_original="striate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachis straight.</text>
      <biological_entity id="o17407" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ultimate segments of blade sessile to short-stalked, linear to oblong-lanceolate, 1.5–8 mm wide;</text>
      <biological_entity constraint="ultimate" id="o17408" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s7" to="short-stalked" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="oblong-lanceolate" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s7" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17409" name="blade" name_original="blade" src="d0_s7" type="structure" />
      <relation from="o17408" id="r3803" name="part_of" negation="false" src="d0_s7" to="o17409" />
    </statement>
    <statement id="d0_s8">
      <text>base truncate or narrowed to stalk, stalk when present green, not lustrous;</text>
      <biological_entity id="o17410" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="truncate" value_original="truncate" />
        <character constraint="to stalk" constraintid="o17411" is_modifier="false" name="shape" src="d0_s8" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o17411" name="stalk" name_original="stalk" src="d0_s8" type="structure" />
      <biological_entity id="o17412" name="stalk" name_original="stalk" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="when present" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" modifier="not" name="reflectance" src="d0_s8" value="lustrous" value_original="lustrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>margins plane or reflexed to form false indusia.</text>
      <biological_entity id="o17413" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="plane" value_original="plane" />
        <character constraint="to form" constraintid="o17414" is_modifier="false" name="orientation" src="d0_s9" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o17414" name="form" name_original="form" src="d0_s9" type="structure" />
      <biological_entity constraint="false" id="o17415" name="indusium" name_original="indusia" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Veins in leaves conspicuous, free (except in sori) and forking well above base of segment, or highly anastomosing.</text>
      <biological_entity id="o17416" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" notes="" src="d0_s10" value="free" value_original="free" />
        <character constraint="above base" constraintid="o17418" is_modifier="false" name="architecture_or_shape" src="d0_s10" value="forking" value_original="forking" />
        <character is_modifier="false" modifier="highly" name="architecture" notes="" src="d0_s10" value="anastomosing" value_original="anastomosing" />
      </biological_entity>
      <biological_entity id="o17417" name="leaf" name_original="leaves" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o17418" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o17419" name="segment" name_original="segment" src="d0_s10" type="structure" />
      <relation from="o17416" id="r3804" name="in" negation="false" src="d0_s10" to="o17417" />
      <relation from="o17418" id="r3805" name="part_of" negation="false" src="d0_s10" to="o17419" />
    </statement>
    <statement id="d0_s11">
      <text>False indusia pale, scarious, covering sori.</text>
      <biological_entity constraint="false" id="o17420" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pale" value_original="pale" />
        <character is_modifier="false" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o17421" name="sorus" name_original="sori" src="d0_s11" type="structure" />
      <relation from="o17420" id="r3806" name="covering" negation="false" src="d0_s11" to="o17421" />
    </statement>
    <statement id="d0_s12">
      <text>Sporangia intramarginal, sori usually continuous except at pinna or segment apex and sinuses, paraphyses present.</text>
      <biological_entity id="o17422" name="sporangium" name_original="sporangia" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="intramarginal" value_original="intramarginal" />
      </biological_entity>
      <biological_entity id="o17423" name="sorus" name_original="sori" src="d0_s12" type="structure">
        <character constraint="except pinna, apex" constraintid="o17424, o17425" is_modifier="false" modifier="usually" name="architecture" src="d0_s12" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o17424" name="pinna" name_original="pinna" src="d0_s12" type="structure" />
      <biological_entity id="o17425" name="apex" name_original="apex" src="d0_s12" type="structure" />
      <biological_entity constraint="segment" id="o17426" name="pinna" name_original="pinna" src="d0_s12" type="structure" />
      <biological_entity id="o17427" name="sinuse" name_original="sinuses" src="d0_s12" type="structure" />
      <biological_entity id="o17428" name="paraphyse" name_original="paraphyses" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores brown, trilete, tetrahedral, rugate and/or tuberculate, usually with prominent equatorial flange.</text>
      <biological_entity constraint="equatorial" id="o17430" name="flange" name_original="flange" src="d0_s13" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s13" value="prominent" value_original="prominent" />
      </biological_entity>
      <relation from="o17429" id="r3807" modifier="usually" name="with" negation="false" src="d0_s13" to="o17430" />
    </statement>
    <statement id="d0_s14">
      <text>x = 29.</text>
      <biological_entity id="o17429" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="trilete" value_original="trilete" />
        <character is_modifier="false" name="shape" src="d0_s13" value="tetrahedral" value_original="tetrahedral" />
        <character is_modifier="false" name="relief" src="d0_s13" value="rugate" value_original="rugate" />
        <character is_modifier="false" name="relief" src="d0_s13" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="x" id="o17431" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="29" value_original="29" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide, warm and tropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
        <character name="distribution" value="warm and tropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Brake</other_name>
  <discussion>Species ca. 300 (5 species and 1 hybrid in the flora).</discussion>
  <references>
    <reference>Kramer, K. U. 1990. Pteris. In: K. Kubitzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 1+ vol. Berlin etc. Vol. 1, pp. 250–252.</reference>
    <reference>Lakela, O. and R. W. Long. 1976. Ferns of Florida. Miami.</reference>
    <reference>Wunderlin, R. P. 1982. Guide to the Vascular Plants of Central Florida. Tampa.</reference>
    <reference>Wagner, W. H. Jr. and C. E. Nauman. 1982. Pteris × delchampsii, a spontaneous fern hybrid from southern Florida. Amer. Fern J. 72: 97–102.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Veins in leaves anastomosing except sometimes near margins of ultimate segments.</description>
      <determination>1 Pteris tripartita</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Veins in leaves entirely free.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves not strictly 1-pinnate, at least proximal pinnae pinnatifid-lobed or variously forked or divided.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves strictly 1-pinnate, pinnae not lobed or divided.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pinnae of mature leaves decurrent to relatively broad-winged rachis in at least distal 1/2 of leaf.</description>
      <determination>4 Pteris multifida</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pinnae of mature leaves not decurrent to relatively broad-winged rachis or only terminal pinna decurrent on rachis.</description>
      <determination>5 Pteris cretica</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petioles and often also rachises densely scaly, scales light to reddish, often grading into hairs on abaxial costae; pinnae appearing not articulate to rachis, apices long-attenuate or sharply acute; sori narrow, with most of abaxial blade surface exposed.</description>
      <determination>2 Pteris vittata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Petioles often sparsely scaly or scaly only proximally, scales dark brown to nearly black, scales absent or few on rachises, abaxial costae with or without hairs; pinnae appearing articulate to rachis, apices acute; sori broad, little or no abaxial blade tissue exposed.</description>
      <determination>3 Pteris bahamensis</determination>
    </key_statement>
  </key>
</bio:treatment>