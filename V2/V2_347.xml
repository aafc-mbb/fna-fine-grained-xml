<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">marsileaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">marsilea</taxon_name>
    <taxon_name authority="Hooker &amp; Greville" date="1830" rank="species">vestita</taxon_name>
    <place_of_publication>
      <publication_title>Icon. Filic.</publication_title>
      <place_in_publication>2: plate 159. 1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family marsileaceae;genus marsilea;species vestita</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200005214</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Marsilea</taxon_name>
    <taxon_name authority="C. Christensen" date="unknown" rank="species">fournieri</taxon_name>
    <taxon_hierarchy>genus Marsilea;species fournieri;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Marsilea</taxon_name>
    <taxon_name authority="A. Braun" date="unknown" rank="species">mucronata</taxon_name>
    <taxon_hierarchy>genus Marsilea;species mucronata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Marsilea</taxon_name>
    <taxon_name authority="Engelmann ex A. Braun" date="unknown" rank="species">tenuifolia</taxon_name>
    <taxon_hierarchy>genus Marsilea;species tenuifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Marsilea</taxon_name>
    <taxon_name authority="A. Braun" date="unknown" rank="species">uncinata</taxon_name>
    <taxon_hierarchy>genus Marsilea;species uncinata;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Marsilea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">vestita</taxon_name>
    <taxon_name authority="(Engelmann ex A. Braun) D. M. Johnson" date="unknown" rank="subspecies">tenuifolia</taxon_name>
    <taxon_hierarchy>genus Marsilea;species vestita;subspecies tenuifolia;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming diffuse or dense clones.</text>
      <biological_entity id="o6503" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6504" name="clone" name_original="clones" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="diffuse" value_original="diffuse" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o6503" id="r1371" name="forming" negation="false" src="d0_s0" to="o6504" />
    </statement>
    <statement id="d0_s1">
      <text>Roots arising at nodes.</text>
      <biological_entity id="o6505" name="root" name_original="roots" src="d0_s1" type="structure">
        <character constraint="at nodes" constraintid="o6506" is_modifier="false" name="orientation" src="d0_s1" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o6506" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Petioles 2–20 cm, sparsely pubescent.</text>
      <biological_entity id="o6507" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s2" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pinnae 4–19 × 4–16 mm, pubescent to glabrous.</text>
      <biological_entity id="o6508" name="pinna" name_original="pinnae" src="d0_s3" type="structure">
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s3" to="19" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s3" to="16" to_unit="mm" />
        <character char_type="range_value" from="pubescent" name="pubescence" src="d0_s3" to="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sporocarp stalks erect, unbranched, attached at base of petiole (occasionally up to 3 mm above it), not hooked at apex, 0.5–25 mm.</text>
      <biological_entity constraint="sporocarp" id="o6509" name="stalk" name_original="stalks" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character constraint="at base" constraintid="o6510" is_modifier="false" name="fixation" src="d0_s4" value="attached" value_original="attached" />
        <character constraint="at apex" constraintid="o6512" is_modifier="false" modifier="not" name="shape" notes="" src="d0_s4" value="hooked" value_original="hooked" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6510" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o6511" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o6512" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o6510" id="r1372" name="part_of" negation="false" src="d0_s4" to="o6511" />
    </statement>
    <statement id="d0_s5">
      <text>Sporocarps perpendicular or slightly nodding, 3.6–7.6 × 3–6.5 mm, 1.5–2 mm thick, elliptic to nearly round in lateral view, pubescent but soon glabrate, scars left by fallen trichomes often appearing as purple or brown specks;</text>
      <biological_entity id="o6513" name="sporocarp" name_original="sporocarps" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="perpendicular" value_original="perpendicular" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s5" value="nodding" value_original="nodding" />
        <character name="thickness" src="d0_s5" unit="mm" value="3.6-7.6×3-6.5" value_original="3.6-7.6×3-6.5" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="thickness" src="d0_s5" to="2" to_unit="mm" />
        <character char_type="range_value" from="elliptic" modifier="in lateral view" name="shape" src="d0_s5" to="nearly round" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <biological_entity id="o6514" name="scar" name_original="scars" src="d0_s5" type="structure" />
      <biological_entity id="o6515" name="trichome" name_original="trichomes" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="fallen" value_original="fallen" />
      </biological_entity>
      <relation from="o6514" id="r1373" name="left by" negation="false" src="d0_s5" to="o6515" />
    </statement>
    <statement id="d0_s6">
      <text>raphe 1.1–1.7 mm, proximal tooth 0.3–0.6 mm, blunt, distal tooth 0.4–1.2 mm, acute, often hooked at apex.</text>
      <biological_entity id="o6516" name="raphe" name_original="raphe" src="d0_s6" type="structure">
        <character char_type="range_value" from="1.1" from_unit="mm" name="some_measurement" src="d0_s6" to="1.7" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o6517" name="tooth" name_original="tooth" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="blunt" value_original="blunt" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6518" name="tooth" name_original="tooth" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s6" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character constraint="at apex" constraintid="o6519" is_modifier="false" modifier="often" name="shape" src="d0_s6" value="hooked" value_original="hooked" />
      </biological_entity>
      <biological_entity id="o6519" name="apex" name_original="apex" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>Sori 14–22.</text>
      <biological_entity id="o6520" name="sorus" name_original="sori" src="d0_s7" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s7" to="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporocarps produced spring–fall (Apr–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporocarps appearing time" char_type="range_value" to="fall" from="spring" />
        <character name="sporocarps appearing time" char_type="range_value" to="Oct" from="Apr" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Widespread and variable, in ponds and wet depressions and on river floodplains</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ponds" modifier="widespread and variable in" />
        <character name="habitat" value="wet depressions" />
        <character name="habitat" value="river floodplains" modifier="and on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Sask.; Ariz., Ark., Calif., Colo., Idaho, Iowa, Kans., La., Minn., Mont., Nebr., Nev., N.Mex., N.Dak., Okla., Oreg., S.Dak., Tex., Utah, Wash., Wyo.; Mexico; South America in Peru.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="South America in Peru" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6</number>
  <discussion>A number of segregate species have been named and recognized in regional floras in North America: Marsilea mucronata A. Braun (less hairy, found east of Rocky Mountains), M. uncinata (glabrous, sporocarp stalks long, distal tooth of sporocarp hooked, south central United States), M. tenuifolia (pinnae very narrow, central Texas), and M. fournieri (small plants and pinnae, southwest). The features upon which these species are based intergrade into one another. The species are therefore best treated as conspecific with M. vestita (D. M. Johnson 1986).</discussion>
  <discussion>Putative hybrids between Marsilea macropoda and this species are discussed under the former.</discussion>
  
</bio:treatment>