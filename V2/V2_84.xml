<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Roth" date="1799" rank="genus">polystichum</taxon_name>
    <taxon_name authority="(H. Christ) Matsumura" date="1904" rank="species">microchlamys</taxon_name>
    <place_of_publication>
      <publication_title>Index Pl. Jap.</publication_title>
      <place_in_publication>1: 343. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus polystichum;species microchlamys</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500992</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aspidium</taxon_name>
    <taxon_name authority="H. Christ" date="unknown" rank="species">microchlamys</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Herb. Boissier</publication_title>
      <place_in_publication>7: 820. 1899</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aspidium;species microchlamys;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect.</text>
      <biological_entity id="o180" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves arching, 3–8 dm;</text>
      <biological_entity id="o181" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s1" to="8" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bulblets absent.</text>
      <biological_entity id="o182" name="bulblet" name_original="bulblets" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 1/8–1/4 length of leaf, densely scaly;</text>
      <biological_entity id="o183" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1/8 length of leaf" name="length" src="d0_s3" to="1/4 length of leaf" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>scales brown, diminishing in size distally.</text>
      <biological_entity id="o184" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="diminishing" value_original="diminishing" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade broadly lanceolate, 1-pinnate-pinnatifid, base slightly narrowed.</text>
      <biological_entity id="o185" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="1-pinnate-pinnatifid" value_original="1-pinnate-pinnatifid" />
      </biological_entity>
      <biological_entity id="o186" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pinnae narrowly lanceolate, not overlapping, in 1 plane, 3–13 cm;</text>
      <biological_entity id="o187" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="3" from_unit="cm" modifier="in 1 plane" name="some_measurement" src="d0_s6" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base oblique, proximal acroscopic segments enlarged;</text>
      <biological_entity id="o188" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s7" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o189" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" name="size" src="d0_s7" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins incised to costa but segments sessile and adnate to costa for at least 2 mm, segments excised and decurrent, serrulate-spiny with teeth spreading to ascending;</text>
      <biological_entity id="o190" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character constraint="to costa" constraintid="o191" is_modifier="false" name="shape" src="d0_s8" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o191" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <biological_entity id="o192" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character constraint="to costa" constraintid="o193" is_modifier="false" name="fusion" src="d0_s8" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o193" name="costa" name_original="costa" src="d0_s8" type="structure" />
      <biological_entity id="o194" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character char_type="range_value" from="spreading" modifier="for at-least 2 mm" name="orientation" src="d0_s8" to="ascending" />
      </biological_entity>
      <biological_entity id="o195" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="decurrent" value_original="decurrent" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s8" value="serrulate-spiny" value_original="serrulate-spiny" />
      </biological_entity>
      <relation from="o194" id="r39" modifier="for at-least 2 mm" name="excised" negation="false" src="d0_s8" to="o195" />
    </statement>
    <statement id="d0_s9">
      <text>apex acute with subapical and apical teeth same size;</text>
      <biological_entity id="o196" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character constraint="with subapical apical teeth" constraintid="o197" is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="subapical and apical" id="o197" name="tooth" name_original="teeth" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>microscales filiform, dense abaxially, sparse adaxially.</text>
      <biological_entity id="o198" name="microscale" name_original="microscales" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="abaxially" name="density" src="d0_s10" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="adaxially" name="count_or_density" src="d0_s10" value="sparse" value_original="sparse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Indusia erose-dentate.</text>
      <biological_entity id="o199" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="erose-dentate" value_original="erose-dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spore color unknown.</text>
      <biological_entity id="o200" name="spore" name_original="spore" src="d0_s12" type="structure" />
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="terrestrial" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alaska; Asia in Kamtchatka and Japan.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia in Kamtchatka and Japan" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>12</number>
  <discussion>Polystichum microchlamys is found in the flora only on Attu, at the western tip of the Aleutian Archipelago.</discussion>
  
</bio:treatment>