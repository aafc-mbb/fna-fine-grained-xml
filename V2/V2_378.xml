<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Roth" date="1799" rank="genus">polystichum</taxon_name>
    <taxon_name authority="M. Hopkins" date="1913" rank="species">andersonii</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>3: 116, plate 9. 1913</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus polystichum;species andersonii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500984</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polystichum</taxon_name>
    <taxon_name authority="(Spenner) Fée" date="unknown" rank="species">braunii</taxon_name>
    <taxon_name authority="(M. Hopkins) Calder &amp; Roy L. Taylor" date="unknown" rank="subspecies">andersonii</taxon_name>
    <taxon_hierarchy>genus Polystichum;species braunii;subspecies andersonii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polystichum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">braunii</taxon_name>
    <taxon_name authority="(M. Hopkins) Hultén" date="unknown" rank="variety">andersonii</taxon_name>
    <taxon_hierarchy>genus Polystichum;species braunii;variety andersonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect.</text>
      <biological_entity id="o14307" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves monomorphic, arching, 3–10 dm;</text>
      <biological_entity id="o14308" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s1" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bulblets 1 or more, on distal 1/3 of rachis.</text>
      <biological_entity id="o14309" name="bulblet" name_original="bulblets" src="d0_s2" type="structure">
        <character name="quantity" src="d0_s2" unit="or more" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14310" name="1/3" name_original="1/3" src="d0_s2" type="structure" />
      <biological_entity id="o14311" name="rachis" name_original="rachis" src="d0_s2" type="structure" />
      <relation from="o14309" id="r3133" name="on" negation="false" src="d0_s2" to="o14310" />
      <relation from="o14310" id="r3134" name="part_of" negation="false" src="d0_s2" to="o14311" />
    </statement>
    <statement id="d0_s3">
      <text>Petiole 1/8–1/4 length of leaf, densely scaly;</text>
      <biological_entity id="o14312" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1/8 length of leaf" name="length" src="d0_s3" to="1/4 length of leaf" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>scales light-brown, diminishing in size distally.</text>
      <biological_entity id="o14313" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s4" value="diminishing" value_original="diminishing" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade lanceolate, 1-pinnate-pinnatifid;</text>
      <biological_entity id="o14314" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="1-pinnate-pinnatifid" value_original="1-pinnate-pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>base narrowed.</text>
      <biological_entity id="o14315" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pinnae lanceolate-falcate, proximal pinnae ± triangular, not overlapping, in 1 plane, 2–10 cm;</text>
      <biological_entity id="o14316" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate-falcate" value_original="lanceolate-falcate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14317" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="triangular" value_original="triangular" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s7" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="2" from_unit="cm" modifier="in 1 plane" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>base oblique, acroscopic auricle well developed;</text>
      <biological_entity id="o14318" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s8" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity id="o14319" name="auricle" name_original="auricle" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s8" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>margins incised to costae, segments adnate to costa for at least 2 mm, serrulate-spiny with teeth ascending;</text>
      <biological_entity id="o14320" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character constraint="to costae" constraintid="o14321" is_modifier="false" name="shape" src="d0_s9" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o14321" name="costa" name_original="costae" src="d0_s9" type="structure" />
      <biological_entity id="o14322" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character constraint="to costa" constraintid="o14323" is_modifier="false" name="fusion" src="d0_s9" value="adnate" value_original="adnate" />
        <character constraint="with teeth" constraintid="o14324" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s9" value="serrulate-spiny" value_original="serrulate-spiny" />
      </biological_entity>
      <biological_entity id="o14323" name="costa" name_original="costa" src="d0_s9" type="structure">
        <character modifier="at least" name="some_measurement" src="d0_s9" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o14324" name="tooth" name_original="teeth" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>apex acute with subapical and apical teeth same size;</text>
      <biological_entity id="o14325" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character constraint="with subapical apical teeth" constraintid="o14326" is_modifier="false" name="shape" src="d0_s10" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="subapical and apical" id="o14326" name="tooth" name_original="teeth" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>microscales filiform, with contorted projections, dense abaxially, sparse adaxially.</text>
      <biological_entity id="o14327" name="microscale" name_original="microscales" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="abaxially" name="density" notes="" src="d0_s11" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="adaxially" name="count_or_density" src="d0_s11" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o14328" name="projection" name_original="projections" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s11" value="contorted" value_original="contorted" />
      </biological_entity>
      <relation from="o14327" id="r3135" name="with" negation="false" src="d0_s11" to="o14328" />
    </statement>
    <statement id="d0_s12">
      <text>Indusia sparsely ciliate.</text>
      <biological_entity id="o14329" name="indusium" name_original="indusia" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores light-brown to brown.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 164.</text>
      <biological_entity id="o14330" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s13" to="brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14331" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="164" value_original="164" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Lowland coastal to midmontane forests, interior moist forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lowland coastal to midmontane forests" />
        <character name="habitat" value="interior moist forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1700" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Anderson's sword fern</other_name>
  <discussion>Polystichum andersonii is an allotetraploid (D. H. Wagner 1979); its diploid parents are P. munitum and P. kwakiutlii. The triploid cross, P. munitum × andersonii, has been analyzed cytologically (W. H. Wagner Jr. 1973). It is the only sterile hybrid in the genus that develops large colonies through vegetative propagation by its bulblets. Hybrids look very much like some of the more deeply incised forms of Polystichum munitum except that they have abundant filiform scales, abortive sori, and nearly triangular lowermost pinnae with ± equally incised acroscopic and basiscopic auricles.</discussion>
  
</bio:treatment>