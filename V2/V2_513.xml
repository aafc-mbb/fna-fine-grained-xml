<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Reichenbach" date="unknown" rank="family">isoëtaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">isoëtes</taxon_name>
    <taxon_name authority="A. Braun" date="unknown" rank="species">engelmannii</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>29: 178. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family isoëtaceae;genus isoëtes;species engelmannii</taxon_hierarchy>
    <other_info_on_name type="fna_id">250076852</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Isoëtes</taxon_name>
    <taxon_name authority="(Engelmann) Clute" date="unknown" rank="species">valida</taxon_name>
    <taxon_hierarchy>genus Isoëtes;species valida;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants aquatic, emergent.</text>
      <biological_entity id="o14636" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="aquatic" value_original="aquatic" />
        <character is_modifier="false" name="location" src="d0_s0" value="emergent" value_original="emergent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Rootstock nearly globose, 2-lobed.</text>
      <biological_entity id="o14637" name="rootstock" name_original="rootstock" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="nearly" name="shape" src="d0_s1" value="globose" value_original="globose" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves evergreen, bright green, pale toward base, spirally arranged, to 60 (–90) cm, pliant, gradually tapering to tip.</text>
      <biological_entity id="o14638" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="duration" src="d0_s2" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="bright green" value_original="bright green" />
        <character constraint="toward base" constraintid="o14639" is_modifier="false" name="coloration" src="d0_s2" value="pale" value_original="pale" />
        <character is_modifier="false" modifier="spirally" name="arrangement" notes="" src="d0_s2" value="arranged" value_original="arranged" />
        <character char_type="range_value" from="60" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="90" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="60" to_unit="cm" />
        <character is_modifier="false" name="fragility_or_texture" src="d0_s2" value="pliant" value_original="pliant" />
        <character constraint="to tip" constraintid="o14640" is_modifier="false" modifier="gradually" name="shape" src="d0_s2" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o14639" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o14640" name="tip" name_original="tip" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Velum covering less than 1/4 of sporangium.</text>
      <biological_entity id="o14641" name="velum" name_original="velum" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_relational" src="d0_s3" value="covering" value_original="covering" />
        <character char_type="range_value" constraint="of sporangium" constraintid="o14642" from="0" name="quantity" src="d0_s3" to="1/4" />
      </biological_entity>
      <biological_entity id="o14642" name="sporangium" name_original="sporangium" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Sporangium wall usually unpigmented, occasionally ± brown-streaked.</text>
      <biological_entity constraint="sporangium" id="o14643" name="wall" name_original="wall" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s4" value="unpigmented" value_original="unpigmented" />
        <character is_modifier="false" modifier="occasionally more or less" name="coloration" src="d0_s4" value="brown-streaked" value_original="brown-streaked" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Megaspores white, 400–560 μm diam., reticulate, with unbroken lamellate ridges;</text>
      <biological_entity id="o14644" name="megaspore" name_original="megaspores" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="white" value_original="white" />
        <character char_type="range_value" from="400" from_unit="um" name="diameter" src="d0_s5" to="560" to_unit="um" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s5" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o14645" name="ridge" name_original="ridges" src="d0_s5" type="structure">
        <character is_modifier="true" name="condition" src="d0_s5" value="unbroken" value_original="unbroken" />
        <character is_modifier="true" name="shape" src="d0_s5" value="lamellate" value_original="lamellate" />
      </biological_entity>
      <relation from="o14644" id="r3204" name="with" negation="false" src="d0_s5" to="o14645" />
    </statement>
    <statement id="d0_s6">
      <text>girdle obscure.</text>
      <biological_entity id="o14646" name="girdle" name_original="girdle" src="d0_s6" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s6" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Microspores gray in mass, 20–30 μm, smooth to papillose.</text>
      <biological_entity id="o14648" name="mass" name_original="mass" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>2n = 22.</text>
      <biological_entity id="o14647" name="microspore" name_original="microspores" src="d0_s7" type="structure">
        <character constraint="in mass" constraintid="o14648" is_modifier="false" name="coloration" src="d0_s7" value="gray" value_original="gray" />
        <character char_type="range_value" from="20" from_unit="um" name="some_measurement" notes="" src="d0_s7" to="30" to_unit="um" />
        <character char_type="range_value" from="smooth" name="relief" src="d0_s7" to="papillose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14649" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="22" value_original="22" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Spores mature in summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="spores maturing time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Emergent or in shallow water of lakes, ponds, streams, and ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="emergent" />
        <character name="habitat" value="shallow water" modifier="or in" constraint="of lakes , ponds , streams , and ditches" />
        <character name="habitat" value="lakes" />
        <character name="habitat" value="ponds" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont.; Ala., Ark., Conn., Del., Fla., Ga., Ill., Ind., Ky., Md., Mass., Mich., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Engelmann's quillwort</other_name>
  <discussion>Isoëtes engelmannii is the most widely distributed quillwort of eastern North America. Plants with larger megaspores, ranging from 480–560 μm, have been called I. engelmannii var. georgiana Engelmann. This variety may represent a tetraploid cytotype. A tetraploid population of I. engelmannii (2n = 44) from northern Florida has larger megaspores characteristic of var. georgiana.</discussion>
  <discussion>Isoëtes engelmannii hybridizes with I. echinospora [ = I. x eatonii Dodge (later synonym = I. x gravesii A. A. Eaton); I. flaccida; I. tuckermanii [ = I. x foveolata A. A. Eaton ex Dodge]; I. lacustris; and I. riparia [ = I. x brittonii Brunton &amp; W. C. Taylor].</discussion>
  
</bio:treatment>