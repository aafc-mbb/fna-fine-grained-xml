<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Martius" date="unknown" rank="family">hymenophyllaceae</taxon_name>
    <taxon_name authority="Smith" date="1793" rank="genus">hymenophyllum</taxon_name>
    <taxon_name authority="(Linnaeus) Smith in J. E. Smith et al." date="unknown" rank="species">tunbrigense</taxon_name>
    <place_of_publication>
      <place_in_publication>5: 418. 1793</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hymenophyllaceae;genus hymenophyllum;species tunbrigense</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500686</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Trichomanes</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">tunbrigense</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1098. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Trichomanes;species tunbrigense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants on rock.</text>
      <biological_entity id="o2510" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o2511" name="rock" name_original="rock" src="d0_s0" type="structure" />
      <relation from="o2510" id="r556" name="on" negation="false" src="d0_s0" to="o2511" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves oblong, 2–3-pinnatifid, 2–6 × 0.5–1.5 cm, with minute, 2-celled, glandular-hairs scattered on veins;</text>
      <biological_entity id="o2512" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s1" value="2-3-pinnatifid" value_original="2-3-pinnatifid" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" constraint="with minute" from="0.5" from_unit="cm" name="width" src="d0_s1" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="2-celled" value_original="2-celled" />
      </biological_entity>
      <biological_entity id="o2513" name="glandular-hair" name_original="glandular-hairs" src="d0_s1" type="structure">
        <character constraint="on veins" constraintid="o2514" is_modifier="false" name="arrangement" src="d0_s1" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o2514" name="vein" name_original="veins" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>margins distantly dentate.</text>
      <biological_entity id="o2515" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="distantly" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Gametophyte gemmae absent.</text>
    </statement>
    <statement id="d0_s4">
      <text>2n = 26.</text>
      <biological_entity constraint="gametophyte" id="o2516" name="gemma" name_original="gemmae" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2517" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>On rock, forming imbricate mats on vertical cliffs in narrow gorges usually near waterfalls and cascades</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" modifier="on" />
        <character name="habitat" value="imbricate mats" constraint="on vertical cliffs in narrow gorges" />
        <character name="habitat" value="vertical cliffs" constraint="in narrow gorges" />
        <character name="habitat" value="narrow gorges" />
        <character name="habitat" value="waterfalls" />
        <character name="habitat" value="cascades" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>350–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="350" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>S.C.; Mexico; West Indies; Central America; South America; Europe; Asia; in tropical and temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="in tropical and temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Tunbridge filmy fern</other_name>
  <discussion>About two dozen small populations of Hymenophyllum tunbrigense exist in a single river gorge in Pickens County, South Carolina. It is slow to recover from disturbance, and its numbers have been substantially reduced by collecting since its initial discovery in 1936. Gametophytes characteristic of the genus but lacking gemmae have been described from Great Britain, where populations are more vigorous and where spore production and sexual reproduction via gametophytes are more common (F. J. Rumsey et al. 1990; C. A. Raine et al. 1991). In plants in the flora, spore production is relatively rare, and gametophytes have not been observed.</discussion>
  
</bio:treatment>