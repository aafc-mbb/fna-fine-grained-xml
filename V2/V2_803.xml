<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Carol A. Jacobs, James H. Peck</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ching" date="unknown" rank="family">dennstaedtiaceae</taxon_name>
    <taxon_name authority="Gleditsch ex Scopoli" date="1760" rank="genus">Pteridium</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carniol.</publication_title>
      <place_in_publication>169. 1760</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dennstaedtiaceae;genus Pteridium</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek pteridion, a small fern</other_info_on_name>
    <other_info_on_name type="fna_id">127416</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial, often forming colonies or thickets.</text>
      <biological_entity id="o18048" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o18049" name="thicket" name_original="thickets" src="d0_s0" type="structure" />
      <relation from="o18048" id="r3955" modifier="often" name="forming" negation="false" src="d0_s0" to="o18049" />
    </statement>
    <statement id="d0_s1">
      <text>Stems subterranean, slender, long-creeping;</text>
      <biological_entity id="o18050" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="location" src="d0_s1" value="subterranean" value_original="subterranean" />
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="long-creeping" value_original="long-creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hairs pale to dark, jointed;</text>
      <biological_entity id="o18051" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character char_type="range_value" from="pale" name="coloration" src="d0_s2" to="dark" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="jointed" value_original="jointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>scales absent;</text>
      <biological_entity id="o18052" name="scale" name_original="scales" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>true vessels present (absent in other Dennstaedtiaceae genera in the flora).</text>
      <biological_entity id="o18053" name="vessel" name_original="vessels" src="d0_s4" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s4" value="true" value_original="true" />
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves widely spaced, broadly deltate, 0.5–4.5 m.</text>
      <biological_entity id="o18054" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s5" value="spaced" value_original="spaced" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="0.5" from_unit="m" name="some_measurement" src="d0_s5" to="4.5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Petiole glabrous to short-hairy, without prickles, with stem buds near base, vascular-bundles numerous, U or O-shaped in cross-section.</text>
      <biological_entity id="o18055" name="petiole" name_original="petiole" src="d0_s6" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s6" to="short-hairy" />
      </biological_entity>
      <biological_entity id="o18056" name="prickle" name_original="prickles" src="d0_s6" type="structure" />
      <biological_entity constraint="stem" id="o18057" name="bud" name_original="buds" src="d0_s6" type="structure" />
      <biological_entity id="o18058" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o18059" name="vascular-bundle" name_original="vascular-bundles" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="shape" src="d0_s6" value="u" value_original="u" />
        <character constraint="in cross-section" constraintid="o18060" is_modifier="false" name="shape" src="d0_s6" value="o--shaped" value_original="o--shaped" />
      </biological_entity>
      <biological_entity id="o18060" name="cross-section" name_original="cross-section" src="d0_s6" type="structure" />
      <relation from="o18055" id="r3956" name="without" negation="false" src="d0_s6" to="o18056" />
      <relation from="o18055" id="r3957" name="with" negation="false" src="d0_s6" to="o18057" />
      <relation from="o18057" id="r3958" name="near" negation="false" src="d0_s6" to="o18058" />
    </statement>
    <statement id="d0_s7">
      <text>Blade 2–4-pinnate, rachis and costae grooved adaxially;</text>
      <biological_entity id="o18061" name="blade" name_original="blade" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="2-4-pinnate" value_original="2-4-pinnate" />
      </biological_entity>
      <biological_entity id="o18062" name="rachis" name_original="rachis" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s7" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o18063" name="costa" name_original="costae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s7" value="grooved" value_original="grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>rachis without prickles;</text>
      <biological_entity id="o18064" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
      <biological_entity id="o18065" name="prickle" name_original="prickles" src="d0_s8" type="structure" />
      <relation from="o18064" id="r3959" name="without" negation="false" src="d0_s8" to="o18065" />
    </statement>
    <statement id="d0_s9">
      <text>nectaries at base of proximal and sometimes distal pinnae.</text>
      <biological_entity id="o18066" name="nectary" name_original="nectaries" src="d0_s9" type="structure" />
      <biological_entity id="o18067" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o18068" name="pinna" name_original="pinnae" src="d0_s9" type="structure">
        <character is_modifier="true" name="position" src="d0_s9" value="proximal" value_original="proximal" />
        <character is_modifier="true" modifier="sometimes" name="position_or_shape" src="d0_s9" value="distal" value_original="distal" />
      </biological_entity>
      <relation from="o18066" id="r3960" name="at" negation="false" src="d0_s9" to="o18067" />
      <relation from="o18067" id="r3961" name="part_of" negation="false" src="d0_s9" to="o18068" />
    </statement>
    <statement id="d0_s10">
      <text>Segments pinnately divided, ultimate segments ovate to oblong to linear, base extending proximally on costae (decurrent) or proximally (surcurrent), margins entire.</text>
      <biological_entity id="o18069" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s10" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o18070" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="oblong" />
      </biological_entity>
      <biological_entity id="o18071" name="base" name_original="base" src="d0_s10" type="structure" />
      <biological_entity id="o18072" name="costa" name_original="costae" src="d0_s10" type="structure" />
      <biological_entity id="o18073" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o18071" id="r3962" name="extending" negation="false" src="d0_s10" to="o18072" />
    </statement>
    <statement id="d0_s11">
      <text>Veins free or joined at margin by commissural vein beneath sori, pinnately 2–3-forked.</text>
      <biological_entity id="o18074" name="vein" name_original="veins" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="free" value_original="free" />
        <character constraint="at margin" constraintid="o18075" is_modifier="false" name="fusion" src="d0_s11" value="joined" value_original="joined" />
        <character is_modifier="false" modifier="pinnately" name="shape" notes="" src="d0_s11" value="2-3-forked" value_original="2-3-forked" />
      </biological_entity>
      <biological_entity id="o18075" name="margin" name_original="margin" src="d0_s11" type="structure" />
      <biological_entity id="o18076" name="commissural" name_original="commissural" src="d0_s11" type="structure" />
      <biological_entity id="o18077" name="vein" name_original="vein" src="d0_s11" type="structure" />
      <biological_entity id="o18078" name="sorus" name_original="sori" src="d0_s11" type="structure" />
      <relation from="o18075" id="r3963" name="by" negation="false" src="d0_s11" to="o18076" />
      <relation from="o18075" id="r3964" name="by" negation="false" src="d0_s11" to="o18077" />
      <relation from="o18076" id="r3965" name="beneath" negation="false" src="d0_s11" to="o18078" />
      <relation from="o18077" id="r3966" name="beneath" negation="false" src="d0_s11" to="o18078" />
    </statement>
    <statement id="d0_s12">
      <text>Sori ± continuous, covered by recurved, outer false indusium and obscure, extrorse, inner true indusium.</text>
      <biological_entity id="o18079" name="sorus" name_original="sori" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s12" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity constraint="inner outer false" id="o18081" name="indusium" name_original="indusium" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="recurved" value_original="recurved" />
        <character is_modifier="true" name="prominence" src="d0_s12" value="obscure" value_original="obscure" />
        <character is_modifier="true" name="dehiscence_or_orientation" src="d0_s12" value="extrorse" value_original="extrorse" />
        <character is_modifier="true" name="derivation" src="d0_s12" value="true" value_original="true" />
      </biological_entity>
      <relation from="o18079" id="r3967" name="covered by" negation="false" src="d0_s12" to="o18081" />
    </statement>
    <statement id="d0_s13">
      <text>Spores tetrahedral-globose, trilete, very finely granulate.</text>
    </statement>
    <statement id="d0_s14">
      <text>x = 26.</text>
      <biological_entity id="o18082" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="tetrahedral-globose" value_original="tetrahedral-globose" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="trilete" value_original="trilete" />
        <character is_modifier="false" modifier="very finely" name="texture" src="d0_s13" value="granulate" value_original="granulate" />
      </biological_entity>
      <biological_entity constraint="x" id="o18083" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="26" value_original="26" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Almost worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Almost worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <discussion>Species 1 (1 species, 4 varieties in the flora).</discussion>
  <references>
    <reference>Page, C. N. 1976. The taxonomy and phytogeography of bracken—A review. J. Linn. Soc., Bot. 73: 1–34.</reference>
    <reference>Perring, F. H. and B. G. Gardener, eds. 1976. The biology of bracken. [Symposium.] J. Linn. Soc., Bot. 73(1–3): i–vi, 1–302.</reference>
    <reference>Tryon, R. M. 1941. A revision of the genus Pteridium. Rhodora 43: 1–31, 37–67.</reference>
  </references>
  
</bio:treatment>