<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frank D. Watson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Bartlett" date="unknown" rank="family">cupressaceae</taxon_name>
    <taxon_name authority="Richard" date="1810" rank="genus">Taxodium</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Mus. Natl. Hist. Nat.</publication_title>
      <place_in_publication>16: 298. 1810</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cupressaceae;genus Taxodium</taxon_hierarchy>
    <other_info_on_name type="etymology">Taxus, generic name of yew, and Greek -oides, like</other_info_on_name>
    <other_info_on_name type="fna_id">132353</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees deciduous or evergreen.</text>
      <biological_entity id="o5455" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Branchlets terete.</text>
      <biological_entity id="o5456" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="terete" value_original="terete" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Lateral roots commonly producing erect, irregularly conic to rounded knees in periodically flooded habitats.</text>
      <biological_entity constraint="lateral" id="o5457" name="root" name_original="roots" src="d0_s2" type="structure" />
      <biological_entity id="o5458" name="knee" name_original="knees" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="irregularly conic" is_modifier="true" name="shape" src="d0_s2" to="rounded" />
      </biological_entity>
      <biological_entity id="o5459" name="habitat" name_original="habitats" src="d0_s2" type="structure" />
      <relation from="o5457" id="r1179" name="producing" negation="false" src="d0_s2" to="o5458" />
      <relation from="o5457" id="r1180" modifier="periodically" name="flooded" negation="false" src="d0_s2" to="o5459" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves alternate, in 2 ranks or not.</text>
      <biological_entity id="o5460" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="alternate" value_original="alternate" />
      </biological_entity>
      <biological_entity id="o5461" name="rank" name_original="ranks" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="2" value_original="2" />
      </biological_entity>
      <relation from="o5460" id="r1181" name="in" negation="false" src="d0_s3" to="o5461" />
    </statement>
    <statement id="d0_s4">
      <text>Adult leaves divergent to strongly appressed, linear or linear-lanceolate to deltate, generally flattened, free portion to ca. 17 mm;</text>
      <biological_entity id="o5462" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="adult" value_original="adult" />
        <character is_modifier="false" name="arrangement" src="d0_s4" value="divergent" value_original="divergent" />
        <character is_modifier="false" modifier="strongly" name="fixation_or_orientation" src="d0_s4" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s4" to="deltate" />
        <character is_modifier="false" modifier="generally" name="shape" src="d0_s4" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o5463" name="portion" name_original="portion" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" value_original="free" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial glands absent.</text>
      <biological_entity constraint="abaxial" id="o5464" name="gland" name_original="glands" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pollen cones with 10–20 sporophylls, each sporophyll with 2–10 pollen-sacs.</text>
      <biological_entity constraint="pollen" id="o5465" name="cone" name_original="cones" src="d0_s6" type="structure" />
      <biological_entity id="o5466" name="sporophyll" name_original="sporophylls" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s6" to="20" />
      </biological_entity>
      <biological_entity id="o5467" name="sporophyll" name_original="sporophyll" src="d0_s6" type="structure" />
      <biological_entity id="o5468" name="pollen-sac" name_original="pollen-sacs" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s6" to="10" />
      </biological_entity>
      <relation from="o5465" id="r1182" name="with" negation="false" src="d0_s6" to="o5466" />
      <relation from="o5467" id="r1183" name="with" negation="false" src="d0_s6" to="o5468" />
    </statement>
    <statement id="d0_s7">
      <text>Seed-cones maturing and shattering in 1 season, nearly globose;</text>
      <biological_entity id="o5469" name="seed-cone" name_original="seed-cones" src="d0_s7" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s7" value="maturing" value_original="maturing" />
        <character is_modifier="false" modifier="nearly" name="shape" notes="" src="d0_s7" value="globose" value_original="globose" />
      </biological_entity>
      <biological_entity id="o5470" name="season" name_original="season" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="1" value_original="1" />
      </biological_entity>
      <relation from="o5469" id="r1184" name="shattering in" negation="false" src="d0_s7" to="o5470" />
    </statement>
    <statement id="d0_s8">
      <text>scales falling early, 5–10, valvate, ± peltate, thin and woody.</text>
      <biological_entity id="o5471" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s8" value="falling" value_original="falling" />
        <character char_type="range_value" from="5" name="quantity" src="d0_s8" to="10" />
        <character is_modifier="false" name="arrangement_or_dehiscence" src="d0_s8" value="valvate" value_original="valvate" />
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s8" value="peltate" value_original="peltate" />
        <character is_modifier="false" name="width" src="d0_s8" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s8" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Seeds (1–) 2 per scale, irregularly 3-angled, wingless;</text>
      <biological_entity id="o5472" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" name="atypical_quantity" src="d0_s9" to="2" to_inclusive="false" />
        <character constraint="per scale" constraintid="o5473" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character is_modifier="false" modifier="irregularly" name="shape" notes="" src="d0_s9" value="3-angled" value_original="3-angled" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="wingless" value_original="wingless" />
      </biological_entity>
      <biological_entity id="o5473" name="scale" name_original="scale" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>cotyledons 4–9.</text>
    </statement>
    <statement id="d0_s11">
      <text>x = 11.</text>
      <biological_entity id="o5474" name="cotyledon" name_original="cotyledons" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" name="quantity" src="d0_s10" to="9" />
      </biological_entity>
      <biological_entity constraint="x" id="o5475" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Mexico, Central America in Guatemala.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America in Guatemala" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <discussion>The pollen cones are usually borne at the base of alternate leaves, forming pendent axillary panicles; they occur less commonly singly or in racemes.</discussion>
  <discussion>Taxodium is variously treated as one to three species but treated here as one polymorphic species with two varieties in the flora. A third taxon tentatively accorded varietal rank--- Taxodium distichum (Linnaeus) Richard var. mexicanum Gordon (= Taxodium mucronatum Tenore)—and recognized by some authors as occurring within our range, appears to differ only in minor phenological characters. Whether or not populations from farther south in Mexico and Guatemala differ sufficiently for formal taxonomic recognition at any rank has yet to be determined.</discussion>
  <discussion>Species 1(–3) (1 in the flora).</discussion>
  <references>
    <reference>Watson, F. D. 1985. The nomenclature of pondcypress and baldcypress. Taxon 34: 506–509.</reference>
  </references>
  
</bio:treatment>