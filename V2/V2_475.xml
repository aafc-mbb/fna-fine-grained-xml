<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">37</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">lycopodiaceae</taxon_name>
    <taxon_name authority="Holub" date="1964" rank="genus">lycopodiella</taxon_name>
    <taxon_name authority="J. G. Bruce" date="1991" rank="species">margueritae</taxon_name>
    <place_of_publication>
      <publication_title>W. H. Wagner, &amp; Beitel, Michigan Bot.</publication_title>
      <place_in_publication>30: 9. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lycopodiaceae;genus lycopodiella;species margueritae</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500757</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Horizontal stems flat on ground, 10–18 × 1–1.6 cm, stems (excluding leaves) thick, 1.8–2.2 mm diam.;</text>
      <biological_entity id="o3135" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
        <character constraint="on stems" constraintid="o3136" is_modifier="false" name="prominence_or_shape" src="d0_s0" value="flat" value_original="flat" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="diameter" notes="" src="d0_s0" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3136" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" is_modifier="true" name="length" src="d0_s0" to="18" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" is_modifier="true" name="width" src="d0_s0" to="1.6" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s0" value="thick" value_original="thick" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>leaves monomorphic, spreading, and nearly perpendicular to stem, 6–13 × 0.8–1.2 mm;</text>
      <biological_entity id="o3137" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
        <character constraint="to stem" constraintid="o3138" is_modifier="false" modifier="nearly" name="orientation" src="d0_s1" value="perpendicular" value_original="perpendicular" />
        <character char_type="range_value" from="6" from_unit="mm" name="length" notes="" src="d0_s1" to="13" to_unit="mm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" notes="" src="d0_s1" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3138" name="stem" name_original="stem" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>marginal teeth 3–4 per side, mainly on proximal 1/2.</text>
      <biological_entity constraint="marginal" id="o3139" name="tooth" name_original="teeth" src="d0_s2" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o3140" from="3" name="quantity" src="d0_s2" to="4" />
      </biological_entity>
      <biological_entity id="o3140" name="side" name_original="side" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o3141" name="1/2" name_original="1/2" src="d0_s2" type="structure" />
      <relation from="o3139" id="r674" modifier="mainly" name="on" negation="false" src="d0_s2" to="o3141" />
    </statement>
    <statement id="d0_s3">
      <text>Upright shoots 1 (–2) per plant, 13–17 × 0.3–0.7 cm;</text>
      <biological_entity id="o3142" name="shoot" name_original="shoots" src="d0_s3" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s3" value="upright" value_original="upright" />
        <character char_type="range_value" from="1" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="2" />
        <character constraint="per plant" constraintid="o3143" name="quantity" src="d0_s3" value="1" value_original="1" />
        <character char_type="range_value" from="13" from_unit="cm" name="length" notes="" src="d0_s3" to="17" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" notes="" src="d0_s3" to="0.7" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3143" name="plant" name_original="plant" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>strobilus 1/2–1/3 total length;</text>
      <biological_entity id="o3144" name="strobilus" name_original="strobilus" src="d0_s4" type="structure">
        <character char_type="range_value" from="1/2" name="length" src="d0_s4" to="1/3" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>leaves initially divergent, then incurved, almost appressed, 5–6 × 0.4–0.8 mm, marginal teeth 0–2 per side.</text>
      <biological_entity id="o3145" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="initially" name="arrangement" src="d0_s5" value="divergent" value_original="divergent" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="incurved" value_original="incurved" />
        <character is_modifier="false" modifier="almost" name="fixation_or_orientation" src="d0_s5" value="appressed" value_original="appressed" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s5" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o3146" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="per side" constraintid="o3147" from="0" name="quantity" src="d0_s5" to="2" />
      </biological_entity>
      <biological_entity id="o3147" name="side" name_original="side" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Strobili 5–8 × 0.4–0.9 cm.</text>
      <biological_entity id="o3148" name="strobilus" name_original="strobili" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s6" to="0.9" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sporophylls appressed, incurved, 4–6 × 0.4–0.5 mm, marginal teeth absent.</text>
      <biological_entity id="o3149" name="sporophyll" name_original="sporophylls" src="d0_s7" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="incurved" value_original="incurved" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s7" to="6" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s7" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 312.</text>
      <biological_entity constraint="marginal" id="o3150" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3151" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="312" value_original="312" />
      </biological_entity>
    </statement>
  </description>
  <number>6</number>
  <other_name type="common_name">Northern prostrate club-moss</other_name>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Wet, acidic ditches and borrow pits.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="wet" />
        <character name="habitat" value="acidic ditches" />
        <character name="habitat" value="pits" modifier="and borrow" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Of conservation concern; Mich.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Of conservation concern" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Lycopodiella margueritae forms apparently fertile hybrids with the other tetraploid species, L. subappressa. Its hybrids with the diploid L. inundata are sterile, however. Lycopodiella margueritae can be distinguished from L. appressa and L. subappressa by the rather thick and large strobili, 1/3–1/2 the total length of upright shoots, and the more spreading leaves of the strobili and upright shoots.</discussion>
  
</bio:treatment>