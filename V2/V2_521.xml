<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Willkomm in Willkomm &amp; Lange" date="unknown" rank="family">selaginellaceae</taxon_name>
    <taxon_name authority="Palisot de Beauvois" date="1805" rank="genus">selaginella</taxon_name>
    <taxon_name authority="Jermy" date="1986" rank="subgenus">tetragonostachys</taxon_name>
    <taxon_name authority="(J. Milde) Hieronymus" date="1900" rank="species">sibirica</taxon_name>
    <place_of_publication>
      <publication_title>Hedwigia</publication_title>
      <place_in_publication>39: 290. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family selaginellaceae;genus selaginella;subgenus tetragonostachys;species sibirica</taxon_hierarchy>
    <other_info_on_name type="fna_id">200002820</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Selaginella</taxon_name>
    <taxon_name authority="(Linnaeus) Spring forma sibirica J. Milde" date="unknown" rank="species">rupestris</taxon_name>
    <place_of_publication>
      <publication_title>Fil. Eur.,</publication_title>
      <place_in_publication>262. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Selaginella;species rupestris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants on rock or terrestrial, forming discrete long-spreading mats or seldom cushionlike mats.</text>
      <biological_entity id="o0" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s0" value="discrete" value_original="discrete" />
        <character is_modifier="true" name="orientation" src="d0_s0" value="long-spreading" value_original="long-spreading" />
      </biological_entity>
      <biological_entity id="o2" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" modifier="seldom" name="shape" src="d0_s0" value="cushionlike" value_original="cushionlike" />
      </biological_entity>
      <relation from="o0" id="r0" name="forming" negation="false" src="d0_s0" to="o1" />
    </statement>
    <statement id="d0_s1">
      <text>Stems radially symmetric, creeping or decumbent, not readily fragmenting, irregularly forked, without budlike arrested branches, tips straight;</text>
      <biological_entity id="o3" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s1" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" modifier="not readily; readily; irregularly" name="shape" src="d0_s1" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity id="o4" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="budlike" value_original="budlike" />
      </biological_entity>
      <biological_entity id="o5" name="tip" name_original="tips" src="d0_s1" type="structure">
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o3" id="r1" name="without" negation="false" src="d0_s1" to="o4" />
    </statement>
    <statement id="d0_s2">
      <text>main-stem indeterminate, lateral branches conspicuously or inconspicuously determinate, often strongly ascending, 1–3-forked.</text>
      <biological_entity id="o6" name="main-stem" name_original="main-stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="development" src="d0_s2" value="indeterminate" value_original="indeterminate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o7" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="inconspicuously" name="development" src="d0_s2" value="determinate" value_original="determinate" />
        <character is_modifier="false" modifier="often strongly" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="shape" src="d0_s2" value="1-3-forked" value_original="1-3-forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Rhizophores borne on upperside of stems, throughout stem length, 0.2–0.37 mm diam.</text>
      <biological_entity id="o8" name="rhizophore" name_original="rhizophores" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="diameter" notes="" src="d0_s3" to="0.37" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9" name="upperside" name_original="upperside" src="d0_s3" type="structure" />
      <biological_entity id="o10" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity id="o11" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <relation from="o8" id="r2" name="borne on" negation="false" src="d0_s3" to="o9" />
      <relation from="o8" id="r3" name="borne on" negation="false" src="d0_s3" to="o10" />
      <relation from="o8" id="r4" name="throughout" negation="false" src="d0_s3" to="o11" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves monomorphic, in alternate pseudowhorls of 5, tightly appressed, ascending, green, linear-lanceolate to narrowly lanceolate, 2–3.5 × 0.35–0.5 mm (smaller on lateral branches);</text>
      <biological_entity id="o12" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="tightly" name="fixation_or_orientation" notes="" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s4" to="narrowly lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s4" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="0.35" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13" name="pseudowhorl" name_original="pseudowhorls" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character name="quantity" src="d0_s4" value="5" value_original="5" />
      </biological_entity>
      <relation from="o12" id="r5" name="in" negation="false" src="d0_s4" to="o13" />
    </statement>
    <statement id="d0_s5">
      <text>abaxial ridges prominent;</text>
      <biological_entity constraint="abaxial" id="o14" name="ridge" name_original="ridges" src="d0_s5" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s5" value="prominent" value_original="prominent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>base cuneate and decurrent to rounded and adnate on young lateral branches or buds, glabrous or sometimes pubescent;</text>
      <biological_entity id="o15" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cuneate" value_original="cuneate" />
        <character char_type="range_value" from="decurrent" name="shape" src="d0_s6" to="rounded" />
        <character constraint="on buds" constraintid="o17" is_modifier="false" name="fusion" src="d0_s6" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o16" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s6" value="young" value_original="young" />
      </biological_entity>
      <biological_entity id="o17" name="bud" name_original="buds" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>margins long-ciliate, cilia transparent, spreading to ascending, 0.07–0.17 mm;</text>
      <biological_entity id="o18" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s7" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
      <biological_entity id="o19" name="cilium" name_original="cilia" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="transparent" value_original="transparent" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s7" to="ascending" />
        <character char_type="range_value" from="0.07" from_unit="mm" name="some_measurement" src="d0_s7" to="0.17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex keeled, truncate in profile, obtuse to attenuate;</text>
      <biological_entity id="o20" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character is_modifier="false" modifier="in profile" name="architecture_or_shape" src="d0_s8" value="truncate" value_original="truncate" />
        <character char_type="range_value" from="obtuse" name="shape" src="d0_s8" to="attenuate" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bristle white to whitish or transparent, puberulent, 0.45–0.8 mm.</text>
      <biological_entity id="o21" name="bristle" name_original="bristle" src="d0_s9" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s9" to="whitish or transparent" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="puberulent" value_original="puberulent" />
        <character char_type="range_value" from="0.45" from_unit="mm" name="some_measurement" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Strobili solitary, 0.5–2.5 cm;</text>
      <biological_entity id="o22" name="strobilus" name_original="strobili" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s10" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sporophylls deltate-ovate to ovatelanceolate, abaxial ridges well defined, base glabrous, margins ciliate, apex truncate in profile, bristled.</text>
      <biological_entity id="o23" name="sporophyll" name_original="sporophylls" src="d0_s11" type="structure">
        <character char_type="range_value" from="deltate-ovate" name="shape" src="d0_s11" to="ovatelanceolate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o24" name="ridge" name_original="ridges" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="well" name="prominence" src="d0_s11" value="defined" value_original="defined" />
      </biological_entity>
      <biological_entity id="o25" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o26" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 18.</text>
      <biological_entity id="o27" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="in profile" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="bristled" value_original="bristled" />
      </biological_entity>
      <biological_entity constraint="2n" id="o28" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry, alpine, rocky slopes, rock crevices, granite rock, limestone boulders, sandstone, bare open grassy tundra</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine" modifier="dry" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="rock crevices" />
        <character name="habitat" value="granite rock" />
        <character name="habitat" value="limestone boulders" />
        <character name="habitat" value="sandstone" />
        <character name="habitat" value="bare open grassy tundra" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>130–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="130" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.W.T., Yukon; Alaska; Asia in Japan and the former Soviet republics.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Asia in Japan and the former Soviet republics" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>16</number>
  <other_name type="common_name">Siberian spike-moss</other_name>
  <discussion>Selaginella sibirica is most closely allied to S. rupestris. In addition to differences noted in the descriptions, it can be distinguished from S. rupestris by the numerous marginal cilia on the leaves and by the transparent sporophyll margins; S. rupestris has a variable number (usually few) of marginal cilia and nontransparent sporophyll margins.</discussion>
  
</bio:treatment>