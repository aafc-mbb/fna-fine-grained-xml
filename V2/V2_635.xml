<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ching ex Pichi Sermolli" date="unknown" rank="family">thelypteridaceae</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">thelypteris</taxon_name>
    <taxon_name authority="(Link) C. V. Morton" date="1963" rank="subgenus">Cyclosorus</taxon_name>
    <taxon_name authority="A. R. Smith" date="1971" rank="species">grandis</taxon_name>
    <taxon_name authority="A. R. Smith" date="unknown" rank="variety">grandis</taxon_name>
    <taxon_hierarchy>family thelypteridaceae;genus thelypteris;subgenus cyclosorus;species grandis;variety grandis;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501281</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems long-creeping, ca. 1 cm diam.</text>
      <biological_entity id="o8555" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="long-creeping" value_original="long-creeping" />
        <character name="diameter" src="d0_s0" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves monomorphic, evergreen, 4–8 cm apart, to ca. 3 m.</text>
      <biological_entity id="o8556" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="duration" src="d0_s1" value="evergreen" value_original="evergreen" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s1" to="8" to_unit="cm" />
        <character is_modifier="false" name="arrangement" src="d0_s1" value="apart" value_original="apart" />
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s1" to="3" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petiole straw-colored, to 1.3 m × 0.8–1.2 cm, at base sparsely set with brown, lanceolate, hairy scales.</text>
      <biological_entity id="o8557" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="straw-colored" value_original="straw-colored" />
        <character char_type="range_value" from="0" from_unit="m" name="length" src="d0_s2" to="1.3" to_unit="m" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="width" src="d0_s2" to="1.2" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o8558" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o8559" name="set" name_original="set" src="d0_s2" type="structure" />
      <biological_entity id="o8560" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="true" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="hairy" value_original="hairy" />
      </biological_entity>
      <relation from="o8557" id="r1818" name="at" negation="false" src="d0_s2" to="o8558" />
      <relation from="o8559" id="r1819" name="with" negation="false" src="d0_s2" to="o8560" />
    </statement>
    <statement id="d0_s3">
      <text>Blade to ca. 1.7 m, broadest at or near base, gradually to somewhat abruptly tapered to pinnatifid apex.</text>
      <biological_entity id="o8561" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s3" to="1.7" to_unit="m" />
        <character constraint="at or near base" constraintid="o8562" is_modifier="false" name="width" src="d0_s3" value="broadest" value_original="broadest" />
        <character char_type="range_value" from="somewhat abruptly tapered" modifier="gradually to somewhat" name="shape" notes="" src="d0_s3" to="pinnatifid" />
      </biological_entity>
      <biological_entity id="o8562" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o8563" name="apex" name_original="apex" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Pinnae (15–) 20–45 × 2–3.5 (–4.8) cm, incised mostly 3/4–9/10 of width;</text>
      <biological_entity id="o8564" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_length" src="d0_s4" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="length" src="d0_s4" to="45" to_unit="cm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="4.8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s4" to="3.5" to_unit="cm" />
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="incised" value_original="incised" />
        <character char_type="range_value" from="3/4" name="width" src="d0_s4" to="9/10" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>segments oblique, curved, 2–4 basal basiscopic ones (those facing base of petiole) on proximal pinnae greatly reduced or wanting;</text>
      <biological_entity id="o8565" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s5" value="oblique" value_original="oblique" />
        <character is_modifier="false" name="course" src="d0_s5" value="curved" value_original="curved" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity constraint="basal" id="o8566" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character constraint="on proximal pinnae" constraintid="o8567" is_modifier="false" name="orientation" src="d0_s5" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" name="quantity" src="d0_s5" value="wanting" value_original="wanting" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o8567" name="pinna" name_original="pinnae" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="greatly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal 1–2 (–3) pairs of veins from adjacent segments connivent at sinus.</text>
      <biological_entity constraint="proximal" id="o8568" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s6" to="3" />
        <character char_type="range_value" constraint="of veins" constraintid="o8569" from="1" name="quantity" src="d0_s6" to="2" />
      </biological_entity>
      <biological_entity id="o8569" name="vein" name_original="veins" src="d0_s6" type="structure" />
      <biological_entity id="o8570" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="adjacent" value_original="adjacent" />
        <character constraint="at sinus" constraintid="o8571" is_modifier="false" name="arrangement" src="d0_s6" value="connivent" value_original="connivent" />
      </biological_entity>
      <biological_entity id="o8571" name="sinus" name_original="sinus" src="d0_s6" type="structure" />
      <relation from="o8569" id="r1820" name="from" negation="false" src="d0_s6" to="o8570" />
    </statement>
    <statement id="d0_s7">
      <text>Indument abaxially of hairs 0.1 mm on costae and veins, also of brownish, hairy scales on costae;</text>
      <biological_entity id="o8572" name="indument" name_original="indument" src="d0_s7" type="structure">
        <character constraint="on veins" constraintid="o8575" name="some_measurement" src="d0_s7" unit="mm" value="0.1" value_original="0.1" />
      </biological_entity>
      <biological_entity id="o8573" name="hair" name_original="hairs" src="d0_s7" type="structure" />
      <biological_entity id="o8574" name="costa" name_original="costae" src="d0_s7" type="structure" />
      <biological_entity id="o8575" name="vein" name_original="veins" src="d0_s7" type="structure" />
      <biological_entity id="o8576" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="brownish" value_original="brownish" />
        <character is_modifier="true" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o8577" name="costa" name_original="costae" src="d0_s7" type="structure" />
      <relation from="o8572" id="r1821" name="part_of" negation="false" src="d0_s7" to="o8573" />
      <relation from="o8572" id="r1822" name="part_of" negation="false" src="d0_s7" to="o8576" />
      <relation from="o8572" id="r1823" name="on" negation="false" src="d0_s7" to="o8577" />
    </statement>
    <statement id="d0_s8">
      <text>blade tissue glabrous on both sides.</text>
      <biological_entity constraint="blade" id="o8578" name="tissue" name_original="tissue" src="d0_s8" type="structure">
        <character constraint="on sides" constraintid="o8579" is_modifier="false" name="pubescence" src="d0_s8" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8579" name="side" name_original="sides" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Sori round, medial to supramedial;</text>
      <biological_entity id="o8580" name="sorus" name_original="sori" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="round" value_original="round" />
        <character char_type="range_value" from="medial" name="position" src="d0_s9" to="supramedial" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>indusia pinkish to reddish-brown, glabrous;</text>
      <biological_entity id="o8581" name="indusium" name_original="indusia" src="d0_s10" type="structure">
        <character char_type="range_value" from="pinkish" name="coloration" src="d0_s10" to="reddish-brown" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sporangia glabrous.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 72.</text>
      <biological_entity id="o8582" name="sporangium" name_original="sporangia" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o8583" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mixed swamps and old logging roads</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mixed swamps" />
        <character name="habitat" value="old logging roads" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies in the Greater Antilles, St. Christopher (St. Kitts).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies in the Greater Antilles" establishment_means="native" />
        <character name="distribution" value="St. Christopher (St. Kitts)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8a</number>
  <discussion>In the flora Thelypteris grandis var. grandis is known only from one population in Collier County, Florida.</discussion>
  
</bio:treatment>