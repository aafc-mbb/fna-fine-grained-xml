<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">adiantum</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">pedatum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1095. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus adiantum;species pedatum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200003542</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Adiantum</taxon_name>
    <taxon_name authority="forma billingsae Kittredge" date="unknown" rank="species">pedatum</taxon_name>
    <taxon_hierarchy>genus Adiantum;species pedatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Adiantum</taxon_name>
    <taxon_name authority="forma laciniatum (Hopkins) Weatherby" date="unknown" rank="species">pedatum</taxon_name>
    <taxon_hierarchy>genus Adiantum;species pedatum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems short-creeping;</text>
      <biological_entity id="o11246" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="short-creeping" value_original="short-creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales bronzy deep yellow, concolored, margins entire.</text>
      <biological_entity id="o11247" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="bronzy" value_original="bronzy" />
        <character is_modifier="false" name="depth" src="d0_s1" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="concolored" value_original="concolored" />
      </biological_entity>
      <biological_entity id="o11248" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves lax-arching (rarely pendent), closely spaced, 40–75 cm.</text>
      <biological_entity id="o11249" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="lax-arching" value_original="lax-arching" />
        <character is_modifier="false" modifier="closely" name="arrangement" src="d0_s2" value="spaced" value_original="spaced" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s2" to="75" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 1–2 mm diam., glabrous, occasionally glaucous.</text>
      <biological_entity id="o11250" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s3" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Blade fan-shaped, pseudopedate, 1-pinnate distally, 15–30 × 15–35 cm, glabrous;</text>
      <biological_entity id="o11251" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="fan--shaped" value_original="fan--shaped" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="pseudopedate" value_original="pseudopedate" />
        <character is_modifier="false" modifier="distally" name="architecture_or_shape" src="d0_s4" value="1-pinnate" value_original="1-pinnate" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s4" to="30" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="width" src="d0_s4" to="35" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>proximal pinnae 3–9-pinnate;</text>
      <biological_entity constraint="proximal" id="o11252" name="pinna" name_original="pinnae" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="3-9-pinnate" value_original="3-9-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachis straight, glabrous, occasionally glaucous.</text>
      <biological_entity id="o11253" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="occasionally" name="pubescence" src="d0_s6" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Segment stalks 0.5–1.5 (–1.7) mm, dark color entering into segment base.</text>
      <biological_entity constraint="segment" id="o11254" name="stalk" name_original="stalks" src="d0_s7" type="structure">
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s7" to="1.7" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="dark color" value_original="dark color" />
      </biological_entity>
      <biological_entity constraint="segment" id="o11255" name="base" name_original="base" src="d0_s7" type="structure" />
      <relation from="o11254" id="r2434" name="entering into" negation="false" src="d0_s7" to="o11255" />
    </statement>
    <statement id="d0_s8">
      <text>Ultimate segments oblong, ca. 3 times as long as broad;</text>
      <biological_entity constraint="ultimate" id="o11256" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="length_or_width" src="d0_s8" value="3 times as long as broad" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>basiscopic margin straight;</text>
      <biological_entity id="o11257" name="margin" name_original="margin" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>acroscopic margin lobed, lobes separated by narrow incisions 0–0.9 (–1.1) mm wide;</text>
      <biological_entity id="o11258" name="margin" name_original="margin" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o11259" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character constraint="by incisions" constraintid="o11260" is_modifier="false" name="arrangement" src="d0_s10" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o11260" name="incision" name_original="incisions" src="d0_s10" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s10" value="narrow" value_original="narrow" />
        <character char_type="range_value" from="0.9" from_inclusive="false" from_unit="mm" name="width" src="d0_s10" to="1.1" to_unit="mm" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s10" to="0.9" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>apex obtuse, divided into shallow, rounded lobes separated by shallow sinuses 0.1–2 (–3.7) mm deep, margins of lobes crenulate or crenate-denticulate.</text>
      <biological_entity id="o11261" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse" value_original="obtuse" />
        <character constraint="into lobes" constraintid="o11262" is_modifier="false" name="shape" src="d0_s11" value="divided" value_original="divided" />
        <character is_modifier="false" name="depth" src="d0_s11" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o11262" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="true" name="depth" src="d0_s11" value="shallow" value_original="shallow" />
        <character is_modifier="true" name="shape" src="d0_s11" value="rounded" value_original="rounded" />
        <character constraint="by sinuses" constraintid="o11263" is_modifier="false" name="arrangement" src="d0_s11" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o11263" name="sinuse" name_original="sinuses" src="d0_s11" type="structure">
        <character is_modifier="true" name="depth" src="d0_s11" value="shallow" value_original="shallow" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s11" to="3.7" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11264" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="crenate-denticulate" value_original="crenate-denticulate" />
      </biological_entity>
      <biological_entity id="o11265" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <relation from="o11264" id="r2435" name="part_of" negation="false" src="d0_s11" to="o11265" />
    </statement>
    <statement id="d0_s12">
      <text>Indusia transversely oblong, 1–3 mm, glabrous.</text>
      <biological_entity id="o11266" name="indusium" name_original="indusia" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s12" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores mostly 34–40 µm diam. 2n = 58.</text>
      <biological_entity id="o11267" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character char_type="range_value" from="34" from_unit="um" name="diameter" src="d0_s13" to="40" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="2n" id="o11268" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="58" value_original="58" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rich, deciduous woodlands, often on humus-covered talus slopes and moist lime soils</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rich" />
        <character name="habitat" value="deciduous woodlands" />
        <character name="habitat" value="humus-covered talus" />
        <character name="habitat" value="slopes" />
        <character name="habitat" value="moist lime soils" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–700 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="700" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., Que.; Ala., Ark., Conn., Del., D.C., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7</number>
  <other_name type="common_name">Northern maidenhair</other_name>
  <other_name type="common_name">adiante du Canada</other_name>
  <discussion>Once considered a single species across its range in North America and eastern Asia, Adiantum pedatum is considered to be a complex of at least three vicariant species (A. pedatum and A. aleuticum occur in North America) and a derivative allopolyploid species (C. A. Paris 1991). Adiantum pedatum in the strict sense is restricted to deciduous woodlands in eastern North America.</discussion>
  
</bio:treatment>