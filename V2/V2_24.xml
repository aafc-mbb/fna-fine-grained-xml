<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">woodsia</taxon_name>
    <taxon_name authority="D. C. Eaton" date="1865" rank="species">oregana</taxon_name>
    <place_of_publication>
      <publication_title>Canad. Naturalist &amp; Quart. J. Sci.</publication_title>
      <place_in_publication>n. s. 2: 90. 1865</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus woodsia;species oregana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">200004233</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems compact, erect to ascending, with few-to-many persistent petiole bases of unequal lengths;</text>
      <biological_entity id="o8455" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="ascending" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o8456" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="few-to-many" value_original="few-to-many" />
        <character is_modifier="true" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o8455" id="r1790" modifier="of unequal lengths" name="with" negation="false" src="d0_s0" to="o8456" />
    </statement>
    <statement id="d0_s1">
      <text>scales often uniformly brown but at least some bicolored with dark central stripe and pale-brown margins, narrowly lanceolate.</text>
      <biological_entity id="o8457" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often uniformly" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character constraint="with margins" constraintid="o8459" is_modifier="false" modifier="at-least" name="coloration" src="d0_s1" value="bicolored" value_original="bicolored" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s1" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="central" id="o8458" name="stripe" name_original="stripe" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o8459" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 4–25 × 1–4 cm.</text>
      <biological_entity id="o8460" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole reddish-brown to dark purple proximally when mature, not articulate above base, somewhat pliable and resistant to shattering.</text>
      <biological_entity id="o8461" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="reddish-brown" modifier="when mature" name="coloration" src="d0_s3" to="dark purple" />
        <character constraint="above base" constraintid="o8462" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="articulate" value_original="articulate" />
        <character is_modifier="false" modifier="somewhat" name="fragility_or_texture" notes="" src="d0_s3" value="pliable" value_original="pliable" />
      </biological_entity>
      <biological_entity id="o8462" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Blade linear-lanceolate to narrowly ovate, pinnate-pinnatifid or 2-pinnate proximally, sparsely to moderately glandular, never viscid;</text>
      <biological_entity id="o8463" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s4" to="narrowly ovate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="pinnate-pinnatifid" value_original="pinnate-pinnatifid" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s4" value="2-pinnate" value_original="2-pinnate" />
        <character is_modifier="false" modifier="sparsely to moderately" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="never" name="coating" src="d0_s4" value="viscid" value_original="viscid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>glandular-hairs with thin stalks and slightly expanded tips;</text>
      <biological_entity id="o8464" name="glandular-hair" name_original="glandular-hairs" src="d0_s5" type="structure" />
      <biological_entity id="o8465" name="stalk" name_original="stalks" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o8466" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="slightly" name="size" src="d0_s5" value="expanded" value_original="expanded" />
      </biological_entity>
      <relation from="o8464" id="r1791" name="with" negation="false" src="d0_s5" to="o8465" />
      <relation from="o8464" id="r1792" name="with" negation="false" src="d0_s5" to="o8466" />
    </statement>
    <statement id="d0_s6">
      <text>rachis with scattered glandular-hairs and occasional hairlike scales.</text>
      <biological_entity id="o8467" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity id="o8468" name="glandular-hair" name_original="glandular-hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o8469" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="occasional" value_original="occasional" />
        <character is_modifier="true" name="shape" src="d0_s6" value="hairlike" value_original="hairlike" />
      </biological_entity>
      <relation from="o8467" id="r1793" name="with" negation="false" src="d0_s6" to="o8468" />
      <relation from="o8467" id="r1794" name="with" negation="false" src="d0_s6" to="o8469" />
    </statement>
    <statement id="d0_s7">
      <text>Pinnae ovate-deltate to elliptic, longer than wide, abruptly tapered to a rounded or broadly acute apex;</text>
      <biological_entity id="o8470" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate-deltate" name="shape" src="d0_s7" to="elliptic" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="longer than wide" value_original="longer than wide" />
        <character constraint="to apex" constraintid="o8471" is_modifier="false" modifier="abruptly" name="shape" src="d0_s7" value="tapered" value_original="tapered" />
      </biological_entity>
      <biological_entity id="o8471" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="true" modifier="broadly" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>largest pinnae with 3–9 pairs of pinnules;</text>
      <biological_entity constraint="largest" id="o8472" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o8473" name="pair" name_original="pairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s8" to="9" />
      </biological_entity>
      <biological_entity id="o8474" name="pinnule" name_original="pinnules" src="d0_s8" type="structure" />
      <relation from="o8472" id="r1795" name="with" negation="false" src="d0_s8" to="o8473" />
      <relation from="o8473" id="r1796" name="part_of" negation="false" src="d0_s8" to="o8474" />
    </statement>
    <statement id="d0_s9">
      <text>abaxial and adaxial surfaces glabrescent to moderately glandular, lacking nonglandular hairs or scales.</text>
      <biological_entity constraint="abaxial and adaxial" id="o8475" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character char_type="range_value" from="glabrescent" name="pubescence" src="d0_s9" to="moderately glandular" />
        <character is_modifier="false" name="quantity" src="d0_s9" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o8476" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <biological_entity id="o8477" name="scale" name_original="scales" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Pinnules dentate, often shallowly lobed;</text>
      <biological_entity id="o8478" name="pinnule" name_original="pinnules" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="often shallowly" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>margins nonlustrous, thin, with occasional glands, lacking cilia, rarely with 1–2-celled translucent projections.</text>
      <biological_entity id="o8479" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s11" value="nonlustrous" value_original="nonlustrous" />
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o8480" name="gland" name_original="glands" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="occasional" value_original="occasional" />
      </biological_entity>
      <biological_entity id="o8481" name="cilium" name_original="cilia" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o8482" name="projection" name_original="projections" src="d0_s11" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s11" value="1-2-celled" value_original="1-2-celled" />
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s11" value="translucent" value_original="translucent" />
      </biological_entity>
      <relation from="o8479" id="r1797" name="with" negation="false" src="d0_s11" to="o8480" />
      <relation from="o8481" id="r1798" modifier="rarely" name="with" negation="false" src="d0_s11" to="o8482" />
    </statement>
    <statement id="d0_s12">
      <text>Vein tips slightly (if at all) enlarged, barely visible adaxially.</text>
      <biological_entity constraint="vein" id="o8483" name="tip" name_original="tips" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="barely; adaxially" name="prominence" src="d0_s12" value="visible" value_original="visible" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Indusia of narrow, usually filamentous segments, these uniseriate for most of length, composed of ± isodiametric cells, concealed by or slightly surpassing mature sporangia.</text>
      <biological_entity id="o8484" name="indusium" name_original="indusia" src="d0_s13" type="structure">
        <character constraint="for" is_modifier="false" name="length" src="d0_s13" value="uniseriate" value_original="uniseriate" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s13" value="concealed" value_original="concealed" />
      </biological_entity>
      <biological_entity id="o8485" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s13" value="narrow" value_original="narrow" />
        <character is_modifier="true" modifier="usually" name="texture" src="d0_s13" value="filamentous" value_original="filamentous" />
      </biological_entity>
      <biological_entity id="o8486" name="cell" name_original="cells" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture_or_shape" src="d0_s13" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity id="o8487" name="sporangium" name_original="sporangia" src="d0_s13" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s13" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o8484" id="r1799" name="part_of" negation="false" src="d0_s13" to="o8485" />
      <relation from="o8484" id="r1800" name="composed of" negation="false" src="d0_s13" to="o8486" />
      <relation from="o8484" id="r1801" modifier="slightly" name="surpassing" negation="false" src="d0_s13" to="o8487" />
    </statement>
    <statement id="d0_s14">
      <text>Spores averaging 39–50 µm.</text>
      <biological_entity id="o8488" name="spore" name_original="spores" src="d0_s14" type="structure" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., Ont., Que., Sask.; Ariz., Calif., Colo., Idaho, Iowa, Kans., Mich., Minn., Mont., N.Dak., N.Mex., N.Y., Nebr., Nev., Okla., Oreg., S.Dak., Utah, Wash., Wis., Wyo.; only in the flora.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="only in the flora" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8</number>
  <other_name type="common_name">Oregon cliff fern</other_name>
  <other_name type="common_name">woodsie de l'oregon</other_name>
  <discussion>The variability and promiscuity of Woodsia oregana have been major sources of taxonomic difficulties in Woodsia, and more work will be necessary before relationships in this complex are fully resolved. As defined here, W. oregana comprises two subspecies that are chromosomally and biochemically distinct. In addition, the two taxa are nearly allopatric, with the diploid (subsp. oregana) confined to the Pacific Northwest and the tetraploid (subsp. cathcartiana) extending from the southwestern United States to eastern Canada. Isozyme studies indicate that subsp. cathcartiana is not an autotetraploid derived from known diploid populations of subsp. oregana, as was hypothesized by D. F. M. Brown (1964), and it may be more appropriate to recognize these taxa as distinct species. The morphologic features that distinguish these subspecies are very subtle, however, and they are associated primarily with differences in chromosome number. Until further systematic analyses are undertaken, these cytotypes should be maintained as subspecies of W. oregana.</discussion>
  <discussion>Subspecies 2.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Spores averaging 39-45 µm; cells on pinnule margins regular in shape, margins appearing entire; adaxial epidermal cells averaging less than 120 µm.</description>
      <determination>8a Woodsia oregana subsp. oregana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Spores averaging 45-50 µm; cells on pinnule margins irregular in shape, margins usually minutely dentate and appearing ragged; adaxial epidermal cells averaging more than 120 µm.</description>
      <determination>8b Woodsia oregana subsp. cathcartiana</determination>
    </key_statement>
  </key>
</bio:treatment>