<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Newman" date="unknown" rank="family">grammitidaceae</taxon_name>
    <taxon_name authority="Swartz" date="1802" rank="genus">grammitis</taxon_name>
    <taxon_name authority="(Jenman) Proctor" date="1953" rank="species">nimbata</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Inst. Jamaica, Sci. Ser.</publication_title>
      <place_in_publication>5: 34. 1953</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grammitidaceae;genus grammitis;species nimbata</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500655</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Jenman" date="unknown" rank="species">nimbatum</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot.</publication_title>
      <place_in_publication>24: 271. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species nimbatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Xiphopteris</taxon_name>
    <taxon_name authority="(Jenman) Copeland" date="unknown" rank="species">nimbata</taxon_name>
    <taxon_hierarchy>genus Xiphopteris;species nimbata;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, apices bearing scales;</text>
      <biological_entity id="o1403" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o1404" name="apex" name_original="apices" src="d0_s0" type="structure" />
      <biological_entity id="o1405" name="scale" name_original="scales" src="d0_s0" type="structure" />
      <relation from="o1404" id="r326" name="bearing" negation="false" src="d0_s0" to="o1405" />
    </statement>
    <statement id="d0_s1">
      <text>scales lustrous yellow to brownish, concolored, 0.5–1 mm, margins stiffly ciliate.</text>
      <biological_entity id="o1406" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="lustrous" value_original="lustrous" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s1" to="brownish" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="concolored" value_original="concolored" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1407" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="stiffly" name="architecture_or_pubescence_or_shape" src="d0_s1" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves densely fasciculate, 3–7 cm.</text>
      <biological_entity id="o1408" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="densely" name="architecture_or_arrangement" src="d0_s2" value="fasciculate" value_original="fasciculate" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s2" to="7" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 2–7 × 0.5 mm, densely hairy;</text>
      <biological_entity id="o1409" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s3" to="7" to_unit="mm" />
        <character name="width" src="d0_s3" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s3" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>hairs dark reddish or brownish, 0.8–1 mm.</text>
      <biological_entity id="o1410" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark reddish" value_original="dark reddish" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s4" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade linear, pinnatifid ca. 2/3 to rachis, narrowed and short-decurrent proximally, 3–5 mm wide;</text>
      <biological_entity id="o1411" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
        <character name="quantity" src="d0_s5" value="2/3" value_original="2/3" />
        <character is_modifier="false" name="shape" notes="" src="d0_s5" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" modifier="proximally" name="architecture" src="d0_s5" value="short-decurrent" value_original="short-decurrent" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1412" name="rachis" name_original="rachis" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>rachises and blade tissue on both sides with crowded, spreading setae like those of petiole or longer.</text>
      <biological_entity id="o1413" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="blade" id="o1414" name="tissue" name_original="tissue" src="d0_s6" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s6" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o1415" name="side" name_original="sides" src="d0_s6" type="structure" />
      <biological_entity id="o1416" name="seta" name_original="setae" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="crowded" value_original="crowded" />
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character constraint="of petiole" constraintid="o1417" is_modifier="false" name="shape" src="d0_s6" value="like" value_original="like" />
      </biological_entity>
      <biological_entity id="o1417" name="petiole" name_original="petiole" src="d0_s6" type="structure" />
      <relation from="o1413" id="r327" name="on" negation="false" src="d0_s6" to="o1415" />
      <relation from="o1414" id="r328" name="on" negation="false" src="d0_s6" to="o1415" />
      <relation from="o1415" id="r329" name="with" negation="false" src="d0_s6" to="o1416" />
    </statement>
    <statement id="d0_s7">
      <text>Segments mostly in 20–30 pairs, oblique-spreading, oblong, 0.8–1.2 mm wide with entire margins.</text>
      <biological_entity id="o1418" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" notes="" src="d0_s7" value="oblique-spreading" value_original="oblique-spreading" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oblong" value_original="oblong" />
        <character char_type="range_value" constraint="with margins" constraintid="o1420" from="0.8" from_unit="mm" name="width" src="d0_s7" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1419" name="pair" name_original="pairs" src="d0_s7" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s7" to="30" />
      </biological_entity>
      <biological_entity id="o1420" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o1418" id="r330" name="in" negation="false" src="d0_s7" to="o1419" />
    </statement>
    <statement id="d0_s8">
      <text>Veins concealed abaxially, unbranched, or fertile segments sometimes with single acroscopic branch, veins ending in hydathodes on adaxial surface.</text>
      <biological_entity id="o1421" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="abaxially" name="prominence" src="d0_s8" value="concealed" value_original="concealed" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o1422" name="segment" name_original="segments" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o1423" name="branch" name_original="branch" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="single" value_original="single" />
        <character is_modifier="true" name="orientation" src="d0_s8" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <biological_entity id="o1424" name="vein" name_original="veins" src="d0_s8" type="structure" />
      <biological_entity id="o1425" name="hydathode" name_original="hydathodes" src="d0_s8" type="structure" />
      <biological_entity constraint="adaxial" id="o1426" name="surface" name_original="surface" src="d0_s8" type="structure" />
      <relation from="o1422" id="r331" name="with" negation="false" src="d0_s8" to="o1423" />
      <relation from="o1424" id="r332" name="ending in" negation="false" src="d0_s8" to="o1425" />
      <relation from="o1424" id="r333" name="ending in" negation="false" src="d0_s8" to="o1426" />
    </statement>
    <statement id="d0_s9">
      <text>Sori round, 1 per segment, arising near base of acroscopic veinlet;</text>
      <biological_entity id="o1427" name="sorus" name_original="sori" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="round" value_original="round" />
        <character constraint="per segment" constraintid="o1428" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character constraint="near base" constraintid="o1429" is_modifier="false" name="orientation" notes="" src="d0_s9" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o1428" name="segment" name_original="segment" src="d0_s9" type="structure" />
      <biological_entity id="o1429" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o1430" name="veinlet" name_original="veinlet" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o1429" id="r334" name="part_of" negation="false" src="d0_s9" to="o1430" />
    </statement>
    <statement id="d0_s10">
      <text>sporangia glabrous.</text>
      <biological_entity id="o1431" name="sporangium" name_original="sporangia" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>On moist cliffs behind falls</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist cliffs" modifier="on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>ca. 500–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.C.; West Indies in Cuba, Jamaica, Hispaniola.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" value="West Indies in Cuba" establishment_means="native" />
        <character name="distribution" value="Jamaica" establishment_means="native" />
        <character name="distribution" value="Hispaniola" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <discussion>Grammitis nimbata was discovered in the United States in Macon County, North Carolina, at a single locality, in 1966 (D. R. Farrar 1967) and has persisted to the present (D. R. Farrar, in litt. 1989). Primarily, the colony is gametophytic and reproduces by microscopic gemmae borne on the elongate-cordate thallus, but sterile sporophytes have been found in favorable years. The species description and illustration are based on mature spore-bearing plants from the Greater Antilles. The largest plants found from North Carolina are less than 3 cm.</discussion>
  
</bio:treatment>