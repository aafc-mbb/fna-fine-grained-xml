<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Newman" date="unknown" rank="family">aspleniaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">asplenium</taxon_name>
    <taxon_name authority="(D. C. Eaton) A. A. Eaton" date="1904" rank="species">×biscayneanum</taxon_name>
    <place_of_publication>
      <publication_title>Fern Bull.</publication_title>
      <place_in_publication>12: 45. 1904</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aspleniaceae;genus asplenium;species ×biscayneanum</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500186</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Asplenium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">rhizophyllum</taxon_name>
    <taxon_name authority="D. C. Eaton" date="unknown" rank="variety">biscayneanum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>14: 97, plate 168. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Asplenium;species rhizophyllum;variety biscayneanum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots proliferous.</text>
      <biological_entity id="o7957" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="proliferous" value_original="proliferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, not branched;</text>
      <biological_entity id="o7958" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>scales blackish throughout, linear-deltate, sparse, 1–1.3 × 0.1–2.4 mm, margins entire.</text>
      <biological_entity id="o7959" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s2" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-deltate" value_original="linear-deltate" />
        <character is_modifier="false" name="count_or_density" src="d0_s2" value="sparse" value_original="sparse" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s2" to="1.3" to_unit="mm" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s2" to="2.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7960" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves nearly monomorphic.</text>
      <biological_entity id="o7961" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s3" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole green, becoming blackish in older leaves, 1–5 (–12) cm, 1/8–1/3 length of blade;</text>
      <biological_entity id="o7962" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character constraint="in leaves" constraintid="o7963" is_modifier="false" modifier="becoming" name="coloration" src="d0_s4" value="blackish" value_original="blackish" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s4" to="12" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" notes="" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1/8 length of blade" name="length" src="d0_s4" to="1/3 length of blade" />
      </biological_entity>
      <biological_entity id="o7963" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="older" value_original="older" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>indument of black, narrowly lanceolate scales.</text>
      <biological_entity id="o7964" name="indument" name_original="indument" src="d0_s5" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity id="o7965" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="black" value_original="black" />
        <character is_modifier="true" modifier="narrowly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <relation from="o7964" id="r1671" name="part_of" negation="false" src="d0_s5" to="o7965" />
    </statement>
    <statement id="d0_s6">
      <text>Blade dull, linear, 2-pinnate, (4–) 12–22 × 1–3.5 cm, papery, glabrous;</text>
      <biological_entity id="o7966" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s6" value="dull" value_original="dull" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="2-pinnate" value_original="2-pinnate" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_length" src="d0_s6" to="12" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="length" src="d0_s6" to="22" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="3.5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s6" value="papery" value_original="papery" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base slightly tapered;</text>
      <biological_entity id="o7967" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s7" value="tapered" value_original="tapered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex narrowing gradually, not rooting.</text>
      <biological_entity id="o7968" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="gradually" name="width" src="d0_s8" value="narrowing" value_original="narrowing" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="rooting" value_original="rooting" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Rachis mostly blackish, green distally and in juvenile leaves, shiny, sparsely scaly.</text>
      <biological_entity id="o7969" name="rachis" name_original="rachis" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s9" value="blackish" value_original="blackish" />
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s9" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" notes="" src="d0_s9" value="shiny" value_original="shiny" />
        <character is_modifier="false" modifier="sparsely" name="architecture_or_pubescence" src="d0_s9" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o7970" name="leaf" name_original="leaves" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="juvenile" value_original="juvenile" />
      </biological_entity>
      <relation from="o7969" id="r1672" name="in" negation="false" src="d0_s9" to="o7970" />
    </statement>
    <statement id="d0_s10">
      <text>Pinnae in 9–20 pairs, oblong, 0.5–2 × 0.4–1.3 cm;</text>
      <biological_entity id="o7971" name="pinna" name_original="pinnae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s10" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s10" to="1.3" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o7972" name="pair" name_original="pairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="9" is_modifier="true" name="quantity" src="d0_s10" to="20" />
      </biological_entity>
      <relation from="o7971" id="r1673" name="in" negation="false" src="d0_s10" to="o7972" />
    </statement>
    <statement id="d0_s11">
      <text>apex blunt to truncate.</text>
      <biological_entity id="o7973" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character char_type="range_value" from="blunt" name="shape" src="d0_s11" to="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Pinnules of 1–2 segments;</text>
      <biological_entity id="o7974" name="pinnule" name_original="pinnules" src="d0_s12" type="structure" />
      <biological_entity id="o7975" name="segment" name_original="segments" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s12" to="2" />
      </biological_entity>
      <relation from="o7974" id="r1674" name="consist_of" negation="false" src="d0_s12" to="o7975" />
    </statement>
    <statement id="d0_s13">
      <text>segments linear-oblong, 3–6 × 1–3 mm;</text>
      <biological_entity id="o7976" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="linear-oblong" value_original="linear-oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s13" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>margins dentate;</text>
      <biological_entity id="o7977" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>apex notched, pointed, rounded, or blunt.</text>
      <biological_entity id="o7978" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="notched" value_original="notched" />
        <character is_modifier="false" name="shape" src="d0_s15" value="pointed" value_original="pointed" />
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s15" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s15" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Veins free, evident.</text>
      <biological_entity id="o7979" name="vein" name_original="veins" src="d0_s16" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s16" value="free" value_original="free" />
        <character is_modifier="false" name="prominence" src="d0_s16" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Sori 1–2 per segment, on both basiscopic and acroscopic sides.</text>
      <biological_entity id="o7980" name="sorus" name_original="sori" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per segment" constraintid="o7981" from="1" name="quantity" src="d0_s17" to="2" />
      </biological_entity>
      <biological_entity id="o7981" name="segment" name_original="segment" src="d0_s17" type="structure" />
      <biological_entity id="o7982" name="side" name_original="sides" src="d0_s17" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s17" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="true" name="orientation" src="d0_s17" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o7980" id="r1675" name="on" negation="false" src="d0_s17" to="o7982" />
    </statement>
    <statement id="d0_s18">
      <text>Spores abortive.</text>
    </statement>
    <statement id="d0_s19">
      <text>2n = ca. 180.</text>
      <biological_entity id="o7983" name="spore" name_original="spores" src="d0_s18" type="structure">
        <character is_modifier="false" name="development" src="d0_s18" value="abortive" value_original="abortive" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7984" name="chromosome" name_original="" src="d0_s19" type="structure">
        <character name="quantity" src="d0_s19" value="180" value_original="180" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hammocks, on limestone rock faces</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hammocks" constraint="on limestone rock" />
        <character name="habitat" value="limestone rock" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–20 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="20" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23</number>
  <other_name type="common_name">Biscayne spleenwort</other_name>
  <discussion>Asplenium × biscayneanum is the hybrid of A. trichomanes-dentatum and A. verecundum, with which it occurs. This peculiar spleenwort may most readily be separated from A. trichomanes-dentatum by its deeply cut pinnae, and from A. verecundum by being only 2-pinnate and having long petioles. Chromosome pairing in A. × biscayneanum is irregular. Judging from herbarium collections, it shows considerable hybrid vigor. All the collections are from Dade County, Florida, where it may now be extirpated.</discussion>
  
</bio:treatment>