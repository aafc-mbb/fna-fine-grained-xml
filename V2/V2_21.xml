<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Willkomm in Willkomm &amp; Lange" date="unknown" rank="family">selaginellaceae</taxon_name>
    <taxon_name authority="Palisot de Beauvois" date="1805" rank="genus">selaginella</taxon_name>
    <taxon_name authority="(Palisot de Beauvois) Baker" date="1883" rank="subgenus">stachygynandrum</taxon_name>
    <taxon_name authority="Hieronymus ex Small" date="1918" rank="species">eatonii</taxon_name>
    <place_of_publication>
      <publication_title>Ferns Trop. Florida</publication_title>
      <place_in_publication>67. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family selaginellaceae;genus selaginella;subgenus stachygynandrum;species eatonii</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">233501229</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Diplostachyum</taxon_name>
    <taxon_name authority="(Hieronymus ex Small) Small" date="unknown" rank="species">eatonii</taxon_name>
    <taxon_hierarchy>genus Diplostachyum;species eatonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial, forming tiny (1–4 cm), dense clumps.</text>
      <biological_entity id="o6569" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6570" name="clump" name_original="clumps" src="d0_s0" type="structure">
        <character is_modifier="true" name="size" src="d0_s0" value="tiny" value_original="tiny" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o6569" id="r1386" name="forming" negation="false" src="d0_s0" to="o6570" />
    </statement>
    <statement id="d0_s1">
      <text>Stems short-creeping, unbranched or few-forked, flat, not articulate, glabrous.</text>
      <biological_entity id="o6571" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="short-creeping" value_original="short-creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="shape" src="d0_s1" value="few-forked" value_original="few-forked" />
        <character is_modifier="false" name="prominence_or_shape" src="d0_s1" value="flat" value_original="flat" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s1" value="articulate" value_original="articulate" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rhizophores axillary, 0.02–0.04 mm diam.</text>
      <biological_entity id="o6572" name="rhizophore" name_original="rhizophores" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="0.02" from_unit="mm" name="diameter" src="d0_s2" to="0.04" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves delicate, papery.</text>
      <biological_entity id="o6573" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="fragility" src="d0_s3" value="delicate" value_original="delicate" />
        <character is_modifier="false" name="texture" src="d0_s3" value="papery" value_original="papery" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Lateral leaves spreading, well spaced or crowded toward stem tip, green, ovate to ovate-oblong, 1–1.5 × 0.5–0.9 mm;</text>
      <biological_entity constraint="lateral" id="o6574" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s4" value="spaced" value_original="spaced" />
        <character constraint="toward stem tip" constraintid="o6575" is_modifier="false" name="arrangement" src="d0_s4" value="crowded" value_original="crowded" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="ovate-oblong" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s4" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s4" to="0.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="stem" id="o6575" name="tip" name_original="tip" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>base rounded;</text>
      <biological_entity id="o6576" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins transparent, serrate;</text>
      <biological_entity id="o6577" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="transparent" value_original="transparent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>apex acute.</text>
      <biological_entity id="o6578" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Median leaves lanceolate, 0.8–1.2 × 0.3–0.35 mm;</text>
      <biological_entity constraint="median" id="o6579" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="length" src="d0_s8" to="1.2" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s8" to="0.35" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>base oblique;</text>
      <biological_entity id="o6580" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s9" value="oblique" value_original="oblique" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>margins transparent, serrate;</text>
      <biological_entity id="o6581" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="transparent" value_original="transparent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>apex bristled;</text>
      <biological_entity id="o6582" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="bristled" value_original="bristled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>bristle to 1/3 length of leaf.</text>
      <biological_entity id="o6583" name="bristle" name_original="bristle" src="d0_s12" type="structure">
        <character char_type="range_value" from="0 length of leaf" name="length" src="d0_s12" to="1/3 length of leaf" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Strobili solitary, 2–3 mm;</text>
      <biological_entity id="o6584" name="strobilus" name_original="strobili" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s13" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s13" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>sporophylls ovatelanceolate, strongly keeled toward tip, keel dentate, base glabrous, margins serrate, apex long-acuminate.</text>
      <biological_entity id="o6585" name="sporophyll" name_original="sporophylls" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character constraint="toward tip" constraintid="o6586" is_modifier="false" modifier="strongly" name="shape" src="d0_s14" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o6586" name="tip" name_original="tip" src="d0_s14" type="structure" />
      <biological_entity id="o6587" name="keel" name_original="keel" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o6588" name="base" name_original="base" src="d0_s14" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s14" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o6589" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s14" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o6590" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="long-acuminate" value_original="long-acuminate" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Hammocks and sink holes in limestone soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="holes" modifier="hammocks and sink" constraint="in limestone soil" />
        <character name="habitat" value="limestone soil" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies in the Bahamas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies in the Bahamas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>36</number>
  <other_name type="common_name">Eaton's spike-moss</other_name>
  <discussion>Selaginella eatonii is a minute species, easy to distinguish by its long-bristled leaf apex, iridescent leaf surface, and somewhat transparent sporophylls on the underside of the stem. It does not have any close relatives among the species in the flora. Selaginella eatonii may best be placed within subg. Heterostachys, with which it shares flattened strobili, keeled sporophylls, and a partial laminar flap on the sporophylls. Selaginella eatonii does not have strongly dimorphic sporophylls or a very well-defined laminar flap, and therefore I prefer to treat it here with the other heterophyllous species of subg. Stachygynandrum until a detailed study of subg. Heterostachys can be made.</discussion>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>