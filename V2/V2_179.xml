<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Roth" date="1799" rank="genus">polystichum</taxon_name>
    <taxon_name authority="(Kaulfuss) C. Presl" date="1836" rank="species">munitum</taxon_name>
    <place_of_publication>
      <publication_title>Tent. Pterid.</publication_title>
      <place_in_publication>83. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus polystichum;species munitum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200004619</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aspidium</taxon_name>
    <taxon_name authority="Kaulfuss" date="unknown" rank="species">munitum</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Filic.,</publication_title>
      <place_in_publication>236. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aspidium;species munitum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect or ascending.</text>
      <biological_entity id="o4167" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves arching, 5–18 dm;</text>
      <biological_entity id="o4168" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character char_type="range_value" from="5" from_unit="dm" name="some_measurement" src="d0_s1" to="18" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bulblets absent.</text>
      <biological_entity id="o4169" name="bulblet" name_original="bulblets" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 1/8–1/4 length of leaf, densely scaly;</text>
      <biological_entity id="o4170" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1/8 length of leaf" name="length" src="d0_s3" to="1/4 length of leaf" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>scales redbrown to dark-brown or nearly black, gradually diminishing in size distally.</text>
      <biological_entity id="o4171" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character char_type="range_value" from="redbrown" name="coloration" src="d0_s4" to="dark-brown or nearly black" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s4" value="diminishing" value_original="diminishing" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade linear-lanceolate, 1-pinnate, base slightly narrowed.</text>
      <biological_entity id="o4172" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="1-pinnate" value_original="1-pinnate" />
      </biological_entity>
      <biological_entity id="o4173" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pinnae narrowly lanceolate, straight to falcate, not overlapping, pinnae of shade-growing plants in 1 plane, those of sun-growing plants twisted or contorted, 1–15 cm;</text>
      <biological_entity id="o4174" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s6" value="falcate" value_original="falcate" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity id="o4175" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s6" value="contorted" value_original="contorted" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s6" to="15" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o4176" name="plant" name_original="plants" src="d0_s6" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s6" value="shade-growing" value_original="shade-growing" />
      </biological_entity>
      <biological_entity id="o4177" name="plant" name_original="plants" src="d0_s6" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s6" value="sun-growing" value_original="sun-growing" />
      </biological_entity>
      <relation from="o4175" id="r898" name="part_of" negation="false" src="d0_s6" to="o4176" />
      <relation from="o4175" id="r899" modifier="in 1 plane" name="part_of" negation="false" src="d0_s6" to="o4177" />
    </statement>
    <statement id="d0_s7">
      <text>base ± cuneate, auricles well developed;</text>
      <biological_entity id="o4178" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s7" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o4179" name="auricle" name_original="auricles" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s7" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins serrulate-spiny with teeth ascending;</text>
      <biological_entity id="o4180" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character constraint="with teeth" constraintid="o4181" is_modifier="false" name="architecture_or_shape" src="d0_s8" value="serrulate-spiny" value_original="serrulate-spiny" />
      </biological_entity>
      <biological_entity id="o4181" name="tooth" name_original="teeth" src="d0_s8" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s8" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>apex acuminate with subapical teeth same size as apical tooth;</text>
      <biological_entity id="o4182" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character constraint="with subapical teeth" constraintid="o4183" is_modifier="false" name="shape" src="d0_s9" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="subapical" id="o4183" name="tooth" name_original="teeth" src="d0_s9" type="structure" />
      <biological_entity constraint="apical" id="o4184" name="tooth" name_original="tooth" src="d0_s9" type="structure" />
      <relation from="o4183" id="r900" name="as" negation="false" src="d0_s9" to="o4184" />
    </statement>
    <statement id="d0_s10">
      <text>microscales ovatelanceolate to linear-lanceolate, with contorted projections, dense, on abaxial surface only.</text>
      <biological_entity id="o4185" name="microscale" name_original="microscales" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovatelanceolate" name="shape" src="d0_s10" to="linear-lanceolate" />
        <character is_modifier="false" name="density" notes="" src="d0_s10" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o4186" name="projection" name_original="projections" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s10" value="contorted" value_original="contorted" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4187" name="surface" name_original="surface" src="d0_s10" type="structure" />
      <relation from="o4185" id="r901" name="with" negation="false" src="d0_s10" to="o4186" />
      <relation from="o4185" id="r902" name="on" negation="false" src="d0_s10" to="o4187" />
    </statement>
    <statement id="d0_s11">
      <text>Indusia ciliate.</text>
      <biological_entity id="o4188" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s11" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Spores light yellow.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 82.</text>
      <biological_entity id="o4189" name="spore" name_original="spores" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="light yellow" value_original="light yellow" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4190" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="82" value_original="82" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial, forest floor, only occasionally on rock, in mesic coniferous to moist, mixed evergreen forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="forest floor" modifier="terrestrial" />
        <character name="habitat" value="rock" constraint="in mesic coniferous to moist , mixed evergreen forests" />
        <character name="habitat" value="mesic coniferous" />
        <character name="habitat" value="moist" />
        <character name="habitat" value="mixed evergreen forests" />
        <character name="habitat" value="terrestrial" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Idaho, Mont., Oreg., S.Dak., Wash.; Mexico on Guadalupe Island; naturalized in Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico on Guadalupe Island" establishment_means="native" />
        <character name="distribution" value="naturalized in Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13</number>
  <other_name type="common_name">Common sword fern</other_name>
  <discussion>One of the most abundant ferns in the western flora (rivaled only by Pteridium), Polystichum munitum also is of significant economic importance. Enormous quantities of leaves are gathered for backgrounds in funeral wreaths and other floral displays; the evergreen leaves keep well in cold storage and are exported to Europe. It is extensively used in landscaping, the trade being mainly in wild-collected plants.</discussion>
  <discussion>Polystichum munitum appears to be most closely related to P. imbricans based on morphologic (D. H. Wagner 1979) and electrophoretic (P. S. Soltis et al. 1990) analyses. The chloroplast DNA of P. imbricans, however, is divergent (G. Yatskievych et al. 1988), suggesting a chloroplast origin independent of the nuclear genome. That Polystichum munitum is related to P. acrostichoides is supported by data from chloroplast DNA analysis (G. Yatskievych et al. 1988) but contradicted by data from electrophoretic studies (P. S. Soltis et al. 1990).</discussion>
  <discussion>Polystichum munitum can be distinguished from P. imbricans by its persistent, wide (the largest wider than 1 mm) distal petiolar scales; such scales of P. imbricans are less than 1 mm wide and fall off early.</discussion>
  <discussion>From an evolutionary standpoint, Polystichum munitum is a diploid progenitor of P. andersonii, P. californicum, P. setigerum, and, perhaps, P. scopulinum. Hybrids with all except P. setigerum have been reported, all triploid, attesting to its parental role in the tetraploids (see discussion under each). Hybrids with P. braunii (A. Sleep and T. Reichstein 1967), P. kruckebergii (P. S. Soltis et al. 1987), P. dudleyi (W. H. Wagner Jr. 1973), and P. lemmonii (P. S. Soltis et al. 1989) also have been reported.</discussion>
  <discussion>The population on Guadalupe Island has been called Polystichum solitarium Maxon.</discussion>
  
</bio:treatment>