<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Agardh" date="unknown" rank="family">ophioglossaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ophioglossum</taxon_name>
    <taxon_name authority="Prantl" date="1883" rank="species">californicum</taxon_name>
    <place_of_publication>
      <publication_title>Ber. Deutsch. Bot. Ges.</publication_title>
      <place_in_publication>1: 355. 1883</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ophioglossaceae;genus ophioglossum;species californicum</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500832</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ophioglossum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">lusitanicum</taxon_name>
    <taxon_name authority="(Prantl) R. T. Clausen" date="unknown" rank="subspecies">californicum</taxon_name>
    <taxon_hierarchy>genus Ophioglossum;species lusitanicum;subspecies californicum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots to 16 per plant, pale-brown, 0.5-1 mm diam., producing proliferations.</text>
      <biological_entity id="o12478" name="root" name_original="roots" src="d0_s0" type="structure">
        <character char_type="range_value" constraint="per plant" constraintid="o12479" from="0" name="quantity" src="d0_s0" to="16" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s0" value="pale-brown" value_original="pale-brown" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s0" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12479" name="plant" name_original="plant" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stem upright, to 1.6 cm, 5mm diam., commonly 2 leaves per stem.</text>
      <biological_entity id="o12480" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="upright" value_original="upright" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="1.6" to_unit="cm" />
        <character name="diameter" src="d0_s1" unit="mm" value="5" value_original="5" />
        <character modifier="commonly" name="quantity" src="d0_s1" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o12481" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
      <biological_entity id="o12482" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <relation from="o12481" id="r2729" name="per" negation="false" src="d0_s1" to="o12482" />
    </statement>
    <statement id="d0_s2">
      <text>Trophophore stalk 0-1.8cm, to 2.5 times length of trophophore blade.</text>
      <biological_entity constraint="trophophore" id="o12483" name="stalk" name_original="stalk" src="d0_s2" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="1.8" to_unit="cm" />
        <character constraint="blade" constraintid="o12484" is_modifier="false" name="length" src="d0_s2" value="0-2.5 times length of trophophore blade" />
      </biological_entity>
      <biological_entity constraint="trophophore" id="o12484" name="blade" name_original="blade" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Trophophore blade erect to spreading, commonly ± folded when alive, green, dull, without pale central band when dried, to 4.3 × 1 cm (rarely 0.4 × 0.3 mm), herbaceous, thick, gradually tapering to base, apex attenuate;</text>
      <biological_entity constraint="trophophore" id="o12485" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="spreading" />
        <character is_modifier="false" modifier="when alive" name="architecture_or_shape" src="d0_s3" value="folded" value_original="folded" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="dull" value_original="dull" />
      </biological_entity>
      <biological_entity constraint="central" id="o12486" name="band" name_original="band" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="pale" value_original="pale" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s3" to="4.3" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s3" to="1" to_unit="cm" />
        <character is_modifier="false" modifier="when dried" name="growth_form_or_texture" src="d0_s3" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="width" src="d0_s3" value="thick" value_original="thick" />
        <character constraint="to base" constraintid="o12487" is_modifier="false" modifier="gradually" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o12487" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o12485" id="r2730" name="without" negation="false" src="d0_s3" to="o12486" />
    </statement>
    <statement id="d0_s4">
      <text>venation complex-reticulate, with numerous parallel narrow areoles, each with 1-several included veinlets.</text>
      <biological_entity id="o12488" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s4" value="complex-reticulate" value_original="complex-reticulate" />
      </biological_entity>
      <biological_entity id="o12489" name="areole" name_original="areoles" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="arrangement" src="d0_s4" value="parallel" value_original="parallel" />
        <character is_modifier="true" name="size_or_width" src="d0_s4" value="narrow" value_original="narrow" />
        <character char_type="range_value" constraint="with" from="1" is_modifier="false" name="quantity" src="d0_s4" to="several" />
      </biological_entity>
      <biological_entity id="o12490" name="veinlet" name_original="veinlets" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Sporophores arising near ground level, 1-2.5 times length of trophophore;</text>
      <biological_entity id="o12492" name="level" name_original="level" src="d0_s5" type="structure" />
      <biological_entity id="o12493" name="trophophore" name_original="trophophore" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>sporangial clusters 8-15 × 1-3 mm, with 8-15 pairs of sporangia, apiculum 0.3-1 mm.</text>
      <biological_entity id="o12491" name="sporophore" name_original="sporophores" src="d0_s5" type="structure">
        <character constraint="near level" constraintid="o12492" is_modifier="false" name="orientation" src="d0_s5" value="arising" value_original="arising" />
        <character constraint="trophophore" constraintid="o12493" is_modifier="false" name="length" notes="" src="d0_s5" value="1-2.5 times length of trophophore" />
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s6" value="sporangial" value_original="sporangial" />
      </biological_entity>
      <biological_entity id="o12494" name="pair" name_original="pairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="8" is_modifier="true" name="quantity" src="d0_s6" to="15" />
      </biological_entity>
      <biological_entity id="o12495" name="sporangium" name_original="sporangia" src="d0_s6" type="structure" />
      <biological_entity id="o12496" name="apiculum" name_original="apiculum" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o12494" id="r2731" name="part_of" negation="false" src="d0_s6" to="o12495" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Leaves appearing in late winter and early spring; apparently absent during dry years.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="leaves appearing time" char_type="range_value" to="early spring" from="late winter" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Open grassy fields and prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy fields" modifier="open" />
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>50-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="50" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">California adder's-tongue</other_name>
  <discussion>Ophioglossum californicum differs from the Old World species O. lusitanicum in that O. lusitanicum has a narrowly linear to linear-oblanceolate trophophore that is 1/4 to 1/2 as wide as long; O. lusitanicum also has a much simpler venation and usually lacks an apiculum.</discussion>
  
</bio:treatment>