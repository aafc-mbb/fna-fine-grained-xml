<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">pinus</taxon_name>
    <taxon_name authority="Miller" date="1768" rank="species">echinata</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict., ed. 8</publication_title>
      <place_in_publication>Pinus no. 12. 1768</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus pinus;species echinata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">200005332</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 40m;</text>
      <biological_entity id="o10415" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="40" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 1.2m diam., straight;</text>
      <biological_entity id="o10416" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="1.2" to_unit="m" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>crown rounded to conic.</text>
      <biological_entity id="o10417" name="crown" name_original="crown" src="d0_s2" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s2" to="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark redbrown, scaly-plated, plates with evident resin pockets.</text>
      <biological_entity id="o10418" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="relief" src="d0_s3" value="scaly-plated" value_original="scaly-plated" />
      </biological_entity>
      <biological_entity id="o10419" name="plate" name_original="plates" src="d0_s3" type="structure" />
      <biological_entity constraint="resin" id="o10420" name="pocket" name_original="pockets" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="evident" value_original="evident" />
      </biological_entity>
      <relation from="o10419" id="r2228" name="with" negation="false" src="d0_s3" to="o10420" />
    </statement>
    <statement id="d0_s4">
      <text>Branches spreading-ascending;</text>
      <biological_entity id="o10421" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading-ascending" value_original="spreading-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>2-year-old branchlets slender (ca. 5mm or less), greenish brown to redbrown, often glaucous, aging redbrown to gray, roughened and cracking below leafy portion.</text>
      <biological_entity id="o10422" name="branchlet" name_original="branchlets" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="2-year-old" value_original="2-year-old" />
        <character is_modifier="false" name="size" src="d0_s5" value="slender" value_original="slender" />
        <character char_type="range_value" from="greenish brown" name="coloration" src="d0_s5" to="redbrown" />
        <character is_modifier="false" modifier="often" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="aging" value_original="aging" />
        <character char_type="range_value" from="redbrown" name="coloration" src="d0_s5" to="gray" />
        <character is_modifier="false" name="relief_or_texture" src="d0_s5" value="roughened" value_original="roughened" />
      </biological_entity>
      <biological_entity id="o10423" name="portion" name_original="portion" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="below" name="architecture" src="d0_s5" value="leafy" value_original="leafy" />
      </biological_entity>
      <relation from="o10422" id="r2229" name="cracking below leafy" negation="false" src="d0_s5" to="o10423" />
    </statement>
    <statement id="d0_s6">
      <text>Buds ovoid to cylindric, redbrown, 0.5–0.7 (–1) cm, resinous.</text>
      <biological_entity id="o10424" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character char_type="range_value" from="ovoid" name="shape" src="d0_s6" to="cylindric" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="0.7" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s6" to="1" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s6" to="0.7" to_unit="cm" />
        <character is_modifier="false" name="coating" src="d0_s6" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves 2 (–3) per fascicle, spreading-ascending, persistent 3–5 years, (5–) 7–11 (–13) cm × ca. 1mm, straight, slightly twisted, gray to yellow-green, all surfaces with fine stomatal lines, margins finely serrulate, apex abruptly acute;</text>
      <biological_entity id="o10425" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="3" />
        <character constraint="per fascicle" constraintid="o10426" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s7" value="spreading-ascending" value_original="spreading-ascending" />
        <character is_modifier="false" name="duration" src="d0_s7" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="5" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_length" src="d0_s7" to="7" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="11" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s7" to="13" to_unit="cm" />
        <character char_type="range_value" from="7" from_unit="cm" name="length" src="d0_s7" to="11" to_unit="cm" />
        <character name="width" src="d0_s7" unit="mm" value="1" value_original="1" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s7" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s7" to="yellow-green" />
      </biological_entity>
      <biological_entity id="o10426" name="fascicle" name_original="fascicle" src="d0_s7" type="structure" />
      <biological_entity id="o10427" name="surface" name_original="surfaces" src="d0_s7" type="structure" />
      <biological_entity id="o10428" name="stomatal" name_original="stomatal" src="d0_s7" type="structure">
        <character is_modifier="true" name="width" src="d0_s7" value="fine" value_original="fine" />
      </biological_entity>
      <biological_entity id="o10429" name="line" name_original="lines" src="d0_s7" type="structure">
        <character is_modifier="true" name="width" src="d0_s7" value="fine" value_original="fine" />
      </biological_entity>
      <biological_entity id="o10430" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o10431" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o10427" id="r2230" name="with" negation="false" src="d0_s7" to="o10428" />
      <relation from="o10427" id="r2231" name="with" negation="false" src="d0_s7" to="o10429" />
    </statement>
    <statement id="d0_s8">
      <text>sheath 0.5–1 (–1.5) cm, base persistent.</text>
      <biological_entity id="o10432" name="sheath" name_original="sheath" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10433" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" name="duration" src="d0_s8" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pollen cones cylindric, 15–20mm, yellow to pale purple-green.</text>
      <biological_entity constraint="pollen" id="o10434" name="cone" name_original="cones" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s9" to="20" to_unit="mm" />
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s9" to="pale purple-green" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seed-cones maturing in 2 years, semipersistent, solitary or clustered, spreading, symmetric, lanceoloid or narrowly ovoid before opening, ovoid-conic when open, 4–6 (–7) cm, redbrown, aging gray, nearly sessile or on stalks to 1cm, scales lacking contrasting dark border on adaxial surfaces distally;</text>
      <biological_entity id="o10435" name="cone-seed" name_original="seed-cones" src="d0_s10" type="structure">
        <character constraint="in years" constraintid="o10436" is_modifier="false" name="life_cycle" src="d0_s10" value="maturing" value_original="maturing" />
        <character is_modifier="false" name="duration" notes="" src="d0_s10" value="semipersistent" value_original="semipersistent" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s10" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="shape" src="d0_s10" value="lanceoloid" value_original="lanceoloid" />
        <character constraint="before opening" constraintid="o10437" is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" modifier="when open" name="shape" notes="" src="d0_s10" value="ovoid-conic" value_original="ovoid-conic" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s10" to="7" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="distance" src="d0_s10" to="6" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" name="life_cycle" src="d0_s10" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="gray" value_original="gray" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="on stalks" value_original="on stalks" />
      </biological_entity>
      <biological_entity id="o10436" name="year" name_original="years" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o10437" name="opening" name_original="opening" src="d0_s10" type="structure" />
      <biological_entity id="o10438" name="stalk" name_original="stalks" src="d0_s10" type="structure" />
      <biological_entity id="o10439" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="to 1 cm" name="quantity" src="d0_s10" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o10440" name="border" name_original="border" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10441" name="surface" name_original="surfaces" src="d0_s10" type="structure" />
      <relation from="o10435" id="r2232" name="on" negation="false" src="d0_s10" to="o10438" />
      <relation from="o10439" id="r2233" name="contrasting" negation="false" src="d0_s10" to="o10440" />
      <relation from="o10439" id="r2234" name="on" negation="false" src="d0_s10" to="o10441" />
    </statement>
    <statement id="d0_s11">
      <text>umbo central, with elongate to short, stout, sharp prickle.</text>
      <biological_entity id="o10442" name="umbo" name_original="umbo" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="central" value_original="central" />
      </biological_entity>
      <biological_entity id="o10443" name="prickle" name_original="prickle" src="d0_s11" type="structure">
        <character is_modifier="true" name="shape" src="d0_s11" value="elongate" value_original="elongate" />
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s11" value="short" value_original="short" />
        <character is_modifier="true" name="fragility_or_size" src="d0_s11" value="stout" value_original="stout" />
        <character is_modifier="true" name="shape" src="d0_s11" value="sharp" value_original="sharp" />
      </biological_entity>
      <relation from="o10442" id="r2235" name="with" negation="false" src="d0_s11" to="o10443" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds ellipsoid;</text>
      <biological_entity id="o10444" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>body ca. 6mm, gray to nearly black;</text>
      <biological_entity id="o10445" name="body" name_original="body" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="6" value_original="6" />
        <character char_type="range_value" from="gray" name="coloration" src="d0_s13" to="nearly black" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>wing 12–16mm.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n =24.</text>
      <biological_entity id="o10446" name="wing" name_original="wing" src="d0_s14" type="structure">
        <character char_type="range_value" from="12" from_unit="mm" name="some_measurement" src="d0_s14" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10447" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Uplands, dry forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="uplands" />
        <character name="habitat" value="dry forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–610m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="610" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Fla., Ga., Ill., Ky., La., Md., Miss., Mo., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Tex., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>19</number>
  <other_name type="common_name">Shortleaf pine</other_name>
  <discussion>Although Pinus echinata is highly valued for timber and pulpwood, it is afflicted by root rot. It hybridizes with P. taeda, the pine most commonly associated with it.</discussion>
  
</bio:treatment>