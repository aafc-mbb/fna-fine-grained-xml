<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Matthew H. Hils</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">423</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">Taxaceae</taxon_name>
    <taxon_hierarchy>family Taxaceae;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10871</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs evergreen, usually neither resinous nor aromatic (sharp or foul-odored in Torreya), dioecious or monoecious.</text>
      <biological_entity id="o11313" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" modifier="usually" name="coating" src="d0_s0" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" modifier="usually" name="coating" src="d0_s0" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark scaly or fissured.</text>
      <biological_entity id="o11315" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="relief" src="d0_s1" value="fissured" value_original="fissured" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Lateral branches well developed, similar to leading shoots;</text>
      <biological_entity constraint="lateral" id="o11316" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s2" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="leading" id="o11317" name="shoot" name_original="shoots" src="d0_s2" type="structure" />
      <relation from="o11316" id="r2445" name="to" negation="false" src="d0_s2" to="o11317" />
    </statement>
    <statement id="d0_s3">
      <text>twigs terete, not densely clothed by leaves but ± ridged by decurrent leaf-bases;</text>
      <biological_entity id="o11318" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="terete" value_original="terete" />
      </biological_entity>
      <biological_entity id="o11319" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o11320" name="leaf-base" name_original="leaf-bases" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="more or less" name="shape" src="d0_s3" value="ridged" value_original="ridged" />
        <character is_modifier="true" name="shape" src="d0_s3" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <relation from="o11318" id="r2446" modifier="not densely" name="clothed by" negation="false" src="d0_s3" to="o11319" />
      <relation from="o11318" id="r2447" modifier="not densely" name="clothed by" negation="false" src="d0_s3" to="o11320" />
    </statement>
    <statement id="d0_s4">
      <text>longest internodes less than 1 cm;</text>
      <biological_entity constraint="longest" id="o11321" name="internode" name_original="internodes" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>buds ± inconspicuous.</text>
      <biological_entity id="o11322" name="bud" name_original="buds" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="more or less" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Roots fibrous to woody.</text>
      <biological_entity id="o11323" name="root" name_original="roots" src="d0_s6" type="structure">
        <character char_type="range_value" from="fibrous" name="texture" src="d0_s6" to="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves (needles) simple, persisting several years, shed singly, alternate [opposite], spirally arranged but often twisted so as to appear 2-ranked, linear to linear-lanceolate, decurrent;</text>
      <biological_entity id="o11324" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="simple" value_original="simple" />
        <character is_modifier="false" name="duration" src="d0_s7" value="persisting" value_original="persisting" />
        <character is_modifier="false" name="quantity" src="d0_s7" value="several" value_original="several" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="singly" value_original="singly" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s7" value="arranged" value_original="arranged" />
        <character constraint="as-to-appear 2-ranked" is_modifier="false" modifier="often" name="architecture" src="d0_s7" value="twisted" value_original="twisted" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>resin canals present or absent.</text>
      <biological_entity constraint="resin" id="o11325" name="canal" name_original="canals" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pollen cones maturing and shed annually, solitary or clustered, axillary on year-old branches, globose to ovoid, sporophylls bearing 2–16 microsporangia (pollen-sacs);</text>
      <biological_entity constraint="pollen" id="o11326" name="cone" name_original="cones" src="d0_s9" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s9" value="maturing" value_original="maturing" />
        <character is_modifier="false" modifier="annually" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s9" value="clustered" value_original="clustered" />
        <character constraint="on branches" constraintid="o11327" is_modifier="false" name="position" src="d0_s9" value="axillary" value_original="axillary" />
        <character char_type="range_value" from="globose" name="shape" notes="" src="d0_s9" to="ovoid" />
      </biological_entity>
      <biological_entity id="o11327" name="branch" name_original="branches" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="year-old" value_original="year-old" />
      </biological_entity>
      <biological_entity id="o11328" name="sporophyll" name_original="sporophylls" src="d0_s9" type="structure" />
      <biological_entity id="o11329" name="microsporangium" name_original="microsporangia" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s9" to="16" />
      </biological_entity>
      <relation from="o11328" id="r2448" name="bearing" negation="false" src="d0_s9" to="o11329" />
    </statement>
    <statement id="d0_s10">
      <text>pollen ± spheric, not winged.</text>
      <biological_entity id="o11330" name="pollen" name_original="pollen" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="spheric" value_original="spheric" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Seed-cones reduced to 1–2 ovules subtended by inconspicuous, decussate bracts, maturing in 1–2 seasons, axillary on year-old branches.</text>
      <biological_entity id="o11331" name="seed-cone" name_original="seed-cones" src="d0_s11" type="structure">
        <character constraint="to ovules" constraintid="o11332" is_modifier="false" name="size" src="d0_s11" value="reduced" value_original="reduced" />
        <character constraint="in seasons" constraintid="o11334" is_modifier="false" name="life_cycle" src="d0_s11" value="maturing" value_original="maturing" />
        <character constraint="on branches" constraintid="o11335" is_modifier="false" name="position" notes="" src="d0_s11" value="axillary" value_original="axillary" />
      </biological_entity>
      <biological_entity id="o11332" name="ovule" name_original="ovules" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="2" />
      </biological_entity>
      <biological_entity id="o11333" name="bract" name_original="bracts" src="d0_s11" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s11" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="true" name="arrangement" src="d0_s11" value="decussate" value_original="decussate" />
      </biological_entity>
      <biological_entity id="o11334" name="season" name_original="seasons" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s11" to="2" />
      </biological_entity>
      <biological_entity id="o11335" name="branch" name_original="branches" src="d0_s11" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s11" value="year-old" value_original="year-old" />
      </biological_entity>
      <relation from="o11332" id="r2449" name="subtended by" negation="false" src="d0_s11" to="o11333" />
    </statement>
    <statement id="d0_s12">
      <text>Seeds 1 per cone, erect, not winged, hard seed-coat partially or wholly surrounded by a juicy, fleshy or leathery aril;</text>
      <biological_entity id="o11336" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character constraint="per cone" constraintid="o11337" name="quantity" src="d0_s12" value="1" value_original="1" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s12" value="erect" value_original="erect" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s12" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o11337" name="cone" name_original="cone" src="d0_s12" type="structure" />
      <biological_entity id="o11338" name="seed-coat" name_original="seed-coat" src="d0_s12" type="structure">
        <character is_modifier="true" name="texture" src="d0_s12" value="hard" value_original="hard" />
      </biological_entity>
      <biological_entity id="o11339" name="aril" name_original="aril" src="d0_s12" type="structure">
        <character is_modifier="true" name="texture" src="d0_s12" value="juicy" value_original="juicy" />
        <character is_modifier="true" name="texture" src="d0_s12" value="fleshy" value_original="fleshy" />
        <character is_modifier="true" name="texture" src="d0_s12" value="leathery" value_original="leathery" />
      </biological_entity>
      <relation from="o11338" id="r2450" modifier="wholly" name="surrounded by a" negation="false" src="d0_s12" to="o11339" />
    </statement>
    <statement id="d0_s13">
      <text>cotyledons 2.</text>
      <biological_entity id="o11340" name="cotyledon" name_original="cotyledons" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="2" value_original="2" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mainly Northern Hemisphere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mainly Northern Hemisphere" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5</number>
  <other_name type="common_name">Yew Family</other_name>
  <discussion>Genera 5, species 17–20 (2 genera, 5 species in the flora).</discussion>
  <references>
    <reference>Burns, R. M. and B. H. Honkala. 1990. Silvics of North America. 1. Conifers. Washington. [Agric. Handb. 654.]</reference>
    <reference>Canadian Forestry Service. 1983. Reproduction of conifers. Forest. Techn. Pub. Canad. Forest. Serv. 31.</reference>
    <reference>Hosie, R. C. 1969. Native Trees of Canada, ed. 7. Ottawa. Pp. 110–111.</reference>
    <reference>Krüssmann, G. 1972. Handbuch der Nadelgehölze. Berlin.</reference>
    <reference>Pilger, R. K. F. 1916. Die Taxales. Mitt. Deutsch. Dendrol. Ges. 25: 1–28.</reference>
    <reference>Pilger, R. K. F. 1926. Coniferae: Taxaceae. In: H. G. A. Engler et al., eds. 1924+. Die natürlichen Pflanzenfamilien..., ed. 2. Leipzig and Berlin. Vol. 13, pp. 199–211.</reference>
    <reference>Price, R. A. 1990. The genera of Taxaceae in the southeastern United States. J. Arnold Arbor. 71: 69–91.</reference>
    <reference>Silba, J. 1986. Encyclopaedia Coniferae. Phytologia Mem. 8: 1–127.</reference>
    <reference>Farjon, A. 1990. A Bibliography of Conifers. Königstein. [Regnum Veg. 122.]</reference>
    <reference>Florin, R. 1948. On the morphology and relationship of the Taxaceae. Bot. Gaz. 110: 31–39.</reference>
    <reference>Little, E. L. Jr. 1979. Checklist of United States Trees (Native and Naturalized). Washington. Pp. 283, 287–288. [Agric. Handb. 541.]</reference>
    <reference>Pilger, R. K. F. 1903. Taxaceae. In: H. G. A. Engler, ed. 1900–1953. Das Pflanzenreich... Berlin. Vol. 18[IV,5], pp. 1–124.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves flexible, without resin canal, apex mucronate, soft-pointed, not sharp to touch; aril scarlet to orange-scarlet, soft, mucilaginous, thick, cup-shaped, open at apex, exposing hard seed coat.</description>
      <determination>1 Taxus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves rigid, stiff, with central resin canal, apex acute, spine-tipped, sharp to touch; aril green or green with purple streaks, leathery, resinous, thin, completely enclosing hard seed coat.</description>
      <determination>2 Torreya</determination>
    </key_statement>
  </key>
</bio:treatment>