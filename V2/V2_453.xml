<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">146</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">notholaena</taxon_name>
    <taxon_name authority="Davenport" date="1880" rank="species">grayi</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>7: 50, plate 4. 1880</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus notholaena;species grayi</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500807</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheilanthes</taxon_name>
    <taxon_name authority="(Davenport) Domin" date="unknown" rank="species">grayi</taxon_name>
    <taxon_hierarchy>genus Cheilanthes;species grayi;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Chrysochosma</taxon_name>
    <taxon_name authority="(Davenport) Pichi-Sermolli" date="unknown" rank="species">grayi</taxon_name>
    <taxon_hierarchy>genus Chrysochosma;species grayi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stem scales concolored to weakly bicolored, margins usually brown, very narrow and poorly defined, thin, ciliate-denticulate.</text>
      <biological_entity constraint="stem" id="o10642" name="scale" name_original="scales" src="d0_s0" type="structure">
        <character char_type="range_value" from="concolored" name="coloration" src="d0_s0" to="weakly bicolored" />
      </biological_entity>
      <biological_entity id="o10643" name="margin" name_original="margins" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s0" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="very" name="size_or_width" src="d0_s0" value="narrow" value_original="narrow" />
        <character is_modifier="false" modifier="poorly" name="prominence" src="d0_s0" value="defined" value_original="defined" />
        <character is_modifier="false" name="width" src="d0_s0" value="thin" value_original="thin" />
        <character is_modifier="false" name="shape" src="d0_s0" value="ciliate-denticulate" value_original="ciliate-denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves 5–20 cm.</text>
      <biological_entity id="o10644" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s1" to="20" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petiole brown, equal to or somewhat shorter than blade, rounded adaxially, glandular-farinose, bearing scattered hairs and scales.</text>
      <biological_entity id="o10645" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="false" name="variability" src="d0_s2" value="equal" value_original="equal" />
        <character constraint="than blade" constraintid="o10646" is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="somewhat shorter" value_original="somewhat shorter" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glandular-farinose" value_original="glandular-farinose" />
      </biological_entity>
      <biological_entity id="o10646" name="blade" name_original="blade" src="d0_s2" type="structure" />
      <biological_entity id="o10647" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o10648" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
      </biological_entity>
      <relation from="o10645" id="r2289" name="bearing" negation="false" src="d0_s2" to="o10647" />
      <relation from="o10645" id="r2290" name="bearing" negation="false" src="d0_s2" to="o10648" />
    </statement>
    <statement id="d0_s3">
      <text>Blade linear-lanceolate, 2-pinnate-pinnatifid, 3–6 times longer than wide, abaxially with conspicuous whitish farina and dull, light-brown, lanceolate, entire scales scattered along rachises and costae, adaxially distinctly glandular;</text>
      <biological_entity id="o10649" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s3" value="2-pinnate-pinnatifid" value_original="2-pinnate-pinnatifid" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s3" value="3-6" value_original="3-6" />
        <character is_modifier="false" modifier="adaxially distinctly" name="architecture_or_function_or_pubescence" notes="" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o10650" name="farina" name_original="farina" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s3" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="whitish" value_original="whitish" />
      </biological_entity>
      <biological_entity id="o10651" name="scale" name_original="scales" src="d0_s3" type="structure">
        <character is_modifier="true" name="reflectance" src="d0_s3" value="dull" value_original="dull" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="light-brown" value_original="light-brown" />
        <character is_modifier="true" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
        <character constraint="along costae" constraintid="o10653" is_modifier="false" name="arrangement" src="d0_s3" value="scattered" value_original="scattered" />
      </biological_entity>
      <biological_entity id="o10652" name="rachis" name_original="rachises" src="d0_s3" type="structure" />
      <biological_entity id="o10653" name="costa" name_original="costae" src="d0_s3" type="structure" />
      <relation from="o10649" id="r2291" modifier="abaxially" name="with" negation="false" src="d0_s3" to="o10650" />
      <relation from="o10649" id="r2292" modifier="abaxially" name="with" negation="false" src="d0_s3" to="o10651" />
    </statement>
    <statement id="d0_s4">
      <text>basal pinnae equal to or slightly larger than adjacent pair, ± equilateral, proximal basiscopic pinnules not greatly enlarged.</text>
      <biological_entity constraint="basal" id="o10654" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character is_modifier="false" name="variability" src="d0_s4" value="equal" value_original="equal" />
        <character constraint="than adjacent pair , more or less equilateral , proximal pinnae" constraintid="o10655" is_modifier="false" name="size" src="d0_s4" value="slightly larger" value_original="slightly larger" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o10655" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s4" value="adjacent" value_original="adjacent" />
        <character is_modifier="true" modifier="more or less" name="architecture_or_shape" src="d0_s4" value="equilateral" value_original="equilateral" />
      </biological_entity>
      <biological_entity id="o10656" name="pinnule" name_original="pinnules" src="d0_s4" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s4" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="not greatly" name="size" src="d0_s4" value="enlarged" value_original="enlarged" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Ultimate segments sessile, broadly adnate to costae;</text>
      <biological_entity constraint="ultimate" id="o10657" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character constraint="to costae" constraintid="o10658" is_modifier="false" modifier="broadly" name="fusion" src="d0_s5" value="adnate" value_original="adnate" />
      </biological_entity>
      <biological_entity id="o10658" name="costa" name_original="costae" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>segment margins slightly recurved, rarely concealing sporangia.</text>
      <biological_entity constraint="segment" id="o10659" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s6" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o10660" name="sporangium" name_original="sporangia" src="d0_s6" type="structure" />
      <relation from="o10659" id="r2293" name="rarely concealing" negation="false" src="d0_s6" to="o10660" />
    </statement>
    <statement id="d0_s7">
      <text>Sporangia containing 16 or 32 spores.</text>
      <biological_entity id="o10661" name="sporangium" name_original="sporangia" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" unit="or spores" value="16" value_original="16" />
        <character name="quantity" src="d0_s7" unit="or spores" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Notholaena grayi comprises two cytotypes here treated as subspecies. Sexually reproducing diploid populations (N. grayi subsp. sonorensis) are concentrated in southern Arizona and western Mexico. Apogamous triploids (N. grayi subsp. grayi) are more widespread, extending from Arizona to central Texas and northeastern Mexico. Isozyme analyses indicate that subsp. grayi is an autotriploid derivative of subsp. sonorensis (G. J. Gastony and M. D. Windham 1989).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Most sporangia containing 16 spores; spores generally more than 55 µm.</description>
      <determination>4a Notholaena grayi subsp. grayi</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Most sporangia containing 32 spores; spores generally less than 55 µm.</description>
      <determination>4b Notholaena grayi subsp. sonorensis</determination>
    </key_statement>
  </key>
</bio:treatment>