<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Agardh" date="unknown" rank="family">ophioglossaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">ophioglossum</taxon_name>
    <taxon_name authority="Rafinesque" date="1814" rank="species">pusillum</taxon_name>
    <place_of_publication>
      <publication_title>Précis Découv. Somiol.</publication_title>
      <place_in_publication>46. 1814</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ophioglossaceae;genus ophioglossum;species pusillum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500834</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ophioglossum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">vulgatum</taxon_name>
    <taxon_name authority="(S.F. Blake) Farwell" date="unknown" rank="variety">pseudopodum</taxon_name>
    <taxon_hierarchy>genus Ophioglossum;species vulgatum;variety pseudopodum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots yellow to tan, to 15 per plant, 0.3-1 mm diam., producing proliferations.</text>
      <biological_entity id="o7717" name="root" name_original="roots" src="d0_s0" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s0" to="tan" />
        <character char_type="range_value" constraint="per plant" constraintid="o7718" from="0" name="quantity" src="d0_s0" to="15" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="diameter" notes="" src="d0_s0" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o7718" name="plant" name_original="plant" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Stem upright, to 2 cm, 3mm diam., 1 leaf per stem.</text>
      <biological_entity id="o7719" name="stem" name_original="stem" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="upright" value_original="upright" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s1" to="2" to_unit="cm" />
        <character name="diameter" src="d0_s1" unit="mm" value="3" value_original="3" />
        <character name="quantity" src="d0_s1" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o7720" name="leaf" name_original="leaf" src="d0_s1" type="structure" />
      <biological_entity id="o7721" name="stem" name_original="stem" src="d0_s1" type="structure" />
      <relation from="o7720" id="r1609" name="per" negation="false" src="d0_s1" to="o7721" />
    </statement>
    <statement id="d0_s2">
      <text>Trophophore stalk expanding gradually into blade.</text>
      <biological_entity constraint="trophophore" id="o7722" name="stalk" name_original="stalk" src="d0_s2" type="structure">
        <character constraint="into blade" constraintid="o7723" is_modifier="false" name="size" src="d0_s2" value="expanding" value_original="expanding" />
      </biological_entity>
      <biological_entity id="o7723" name="blade" name_original="blade" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Trophophore blade erect or spreading, usually plane when alive, pale green, dull, mostly oblanceolate to obovate to ovate, widest point in middle, to 10 × 3.5 cm, soft, herbaceous, base tapering gradually, apex rounded;</text>
      <biological_entity constraint="trophophore" id="o7724" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="spreading" value_original="spreading" />
        <character is_modifier="false" modifier="when alive" name="shape" src="d0_s3" value="plane" value_original="plane" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="pale green" value_original="pale green" />
        <character is_modifier="false" name="reflectance" src="d0_s3" value="dull" value_original="dull" />
        <character char_type="range_value" from="mostly oblanceolate" name="shape" src="d0_s3" to="obovate" />
      </biological_entity>
      <biological_entity constraint="widest" id="o7725" name="point" name_original="point" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="length" notes="" src="d0_s3" to="10" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" notes="" src="d0_s3" to="3.5" to_unit="cm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s3" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o7726" name="middle" name_original="middle" src="d0_s3" type="structure" />
      <biological_entity id="o7727" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s3" value="tapering" value_original="tapering" />
      </biological_entity>
      <relation from="o7725" id="r1610" name="in" negation="false" src="d0_s3" to="o7726" />
    </statement>
    <statement id="d0_s4">
      <text>venation complex-reticulate, with included free veinlets in areoles.</text>
      <biological_entity id="o7728" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s4" value="complex-reticulate" value_original="complex-reticulate" />
      </biological_entity>
      <biological_entity id="o7729" name="veinlet" name_original="veinlets" src="d0_s4" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s4" value="free" value_original="free" />
      </biological_entity>
      <biological_entity id="o7730" name="areole" name_original="areoles" src="d0_s4" type="structure" />
      <relation from="o7728" id="r1611" name="included" negation="false" src="d0_s4" to="o7729" />
    </statement>
    <statement id="d0_s5">
      <text>Sporophores arising at ground level, 2.5-4.5 times length of trophophore;</text>
      <biological_entity id="o7732" name="level" name_original="level" src="d0_s5" type="structure" />
      <biological_entity id="o7733" name="trophophore" name_original="trophophore" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>sporangial clusters 20-45 × 1-4 mm, with 10-40 pairs of sporangia, apiculum 1-2 mm. 2n =960.</text>
      <biological_entity id="o7731" name="sporophore" name_original="sporophores" src="d0_s5" type="structure">
        <character constraint="at level" constraintid="o7732" is_modifier="false" name="orientation" src="d0_s5" value="arising" value_original="arising" />
        <character constraint="trophophore" constraintid="o7733" is_modifier="false" name="length" notes="" src="d0_s5" value="2.5-4.5 times length of trophophore" />
        <character is_modifier="false" name="structure_in_adjective_form" src="d0_s6" value="sporangial" value_original="sporangial" />
      </biological_entity>
      <biological_entity id="o7734" name="pair" name_original="pairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="cluster" value_original="cluster" />
        <character char_type="range_value" from="10" is_modifier="true" name="quantity" src="d0_s6" to="40" />
      </biological_entity>
      <biological_entity id="o7735" name="sporangium" name_original="sporangia" src="d0_s6" type="structure" />
      <biological_entity id="o7736" name="apiculum" name_original="apiculum" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o7737" name="chromosome" name_original="" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="960" value_original="960" />
      </biological_entity>
      <relation from="o7734" id="r1612" name="part_of" negation="false" src="d0_s6" to="o7735" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Leaves appearing midspring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="leaves appearing time" char_type="range_value" to="midspring" from="midspring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Frequent and widespread, open fens, marsh edges, pastures, and grassy shores and roadside ditches, north of the southern boundary of Wisconsin glaciation</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open fens" modifier="frequent and widespread" />
        <character name="habitat" value="marsh edges" />
        <character name="habitat" value="pastures" />
        <character name="habitat" value="grassy shores" />
        <character name="habitat" value="roadside ditches" />
        <character name="habitat" value="the southern boundary" modifier="north" constraint="of wisconsin glaciation" />
        <character name="habitat" value="wisconsin glaciation" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., N.B., N.S., Ont., Que.; Alaska, Calif., Conn., Idaho, Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Mont., Nebr., N.H., N.J., N.Y., N.Dak., Ohio, Oreg., Pa., R.I., S.Dak., Vt., Va., Wash., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6</number>
  <other_name type="common_name">Northern adder's-tongue</other_name>
  <other_name type="common_name">herbe-sans-couture</other_name>
  <discussion>Ophioglossum pusillum is inconspicuous and may be much more common than collections indicate. It differs from O. vulgatum in having an ephemeral, membranous basal sheath.</discussion>
  
</bio:treatment>