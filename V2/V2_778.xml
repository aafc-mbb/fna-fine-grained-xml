<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Agardh" date="unknown" rank="family">ophioglossaceae</taxon_name>
    <taxon_name authority="Swartz" date="1801" rank="genus">botrychium</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="subgenus">Botrychium</taxon_name>
    <taxon_name authority="Farrar &amp; Johnson Groh" date="1991" rank="species">gallicomontanum</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>81: 1, figs. 1, 2, 4. 1991</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ophioglossaceae;genus botrychium;subgenus botrychium;species gallicomontanum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500277</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trophophore stalk 1–8 mm;</text>
      <biological_entity constraint="trophophore" id="o15367" name="stalk" name_original="stalk" src="d0_s0" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s0" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade yellow-green, ovate to oblong-linear, 1-pinnate, to 3 × 0.9 cm, firm, glaucescent.</text>
      <biological_entity id="o15368" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s1" to="oblong-linear" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="1-pinnate" value_original="1-pinnate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="3" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s1" to="0.9" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="firm" value_original="firm" />
        <character is_modifier="false" name="coating" src="d0_s1" value="glaucescent" value_original="glaucescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pinnae to 6 pairs, strongly ascending, well separated, distance between the 1st and 2d pinnae considerably greater than between 2d and 3d pairs, basal pinna pair approximately equal in size and cutting to adjacent pair, fan-shaped to narrowly spatulate, often asymmetric, with distal portion longer than and arching over proximal portion, undivided to tip, rarely 2-cleft, margins entire to irregularly cleft, apex rounded, venation like ribs of fan, midrib absent.</text>
      <biological_entity constraint="between pinnae" constraintid="o15370" id="o15369" name="pinna" name_original="pinnae" src="d0_s2" type="structure" constraint_original="between  pinnae, ">
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="6" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s2" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o15370" name="pinna" name_original="pinnae" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o15371" name="pinna" name_original="pinna" src="d0_s2" type="structure">
        <character constraint="to pair" constraintid="o15372" is_modifier="false" modifier="between 2d and 3d pairs; approximately" name="variability" src="d0_s2" value="equal" value_original="equal" />
        <character char_type="range_value" from="fan-shaped" name="shape" notes="" src="d0_s2" to="narrowly spatulate" />
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s2" value="asymmetric" value_original="asymmetric" />
        <character constraint="over proximal portion" constraintid="o15374" is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character constraint="to tip" constraintid="o15375" is_modifier="false" name="architecture_or_shape" notes="" src="d0_s2" value="undivided" value_original="undivided" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" notes="" src="d0_s2" value="2-cleft" value_original="2-cleft" />
      </biological_entity>
      <biological_entity id="o15372" name="pair" name_original="pair" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o15373" name="portion" name_original="portion" src="d0_s2" type="structure" />
      <biological_entity constraint="proximal" id="o15374" name="portion" name_original="portion" src="d0_s2" type="structure" />
      <biological_entity id="o15375" name="tip" name_original="tip" src="d0_s2" type="structure" />
      <biological_entity id="o15376" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape_or_architecture" src="d0_s2" value="entire to irregularly" value_original="entire to irregularly" />
        <character is_modifier="false" modifier="irregularly" name="architecture_or_shape" src="d0_s2" value="cleft" value_original="cleft" />
      </biological_entity>
      <biological_entity id="o15377" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o15378" name="rib" name_original="ribs" src="d0_s2" type="structure" />
      <biological_entity id="o15379" name="midrib" name_original="midrib" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="of fan" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o15371" id="r3356" name="with" negation="false" src="d0_s2" to="o15373" />
      <relation from="o15377" id="r3357" name="like" negation="false" src="d0_s2" to="o15378" />
    </statement>
    <statement id="d0_s3">
      <text>Sporophores 2–3-pinnate, 1.5–3 times length of trophophore.</text>
      <biological_entity id="o15380" name="sporophore" name_original="sporophores" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="2-3-pinnate" value_original="2-3-pinnate" />
        <character constraint="trophophore" constraintid="o15381" is_modifier="false" name="length" src="d0_s3" value="1.5-3 times length of trophophore" />
      </biological_entity>
      <biological_entity id="o15381" name="trophophore" name_original="trophophore" src="d0_s3" type="structure" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Leaves appearing in midspring, dying in summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="leaves appearing time" char_type="range_value" to="midspring" from="midspring" />
        <character name="leaves dying time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Prairies</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="prairies" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Minn.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>14</number>
  <other_name type="common_name">Frenchman's Bluff moonwort</other_name>
  <discussion>Botrychium gallicomontanum is known only from one locality in western Minnesota, where it grows with both B. campestre and B. simplex. It is intermediate between them in the spacing, shape, and stalk length of the pinnae. This is one of four moonwort species that commonly produce dense clusters of minute, spheric gemmae at the root bases. This moonwort is probably an allopolyploid of the two associated species.</discussion>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>