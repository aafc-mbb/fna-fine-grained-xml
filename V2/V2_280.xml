<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="Swartz" date="1806" rank="genus">cheilanthes</taxon_name>
    <taxon_name authority="Baker in Hooker &amp; Baker" date="1867" rank="species">eatonii</taxon_name>
    <place_of_publication>
      <publication_title>in Hooker &amp; Baker,Syn. Fil.</publication_title>
      <place_in_publication>4: 140. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus cheilanthes;species eatonii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500350</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheilanthes</taxon_name>
    <taxon_name authority="Maxon" date="unknown" rank="species">castanea</taxon_name>
    <taxon_hierarchy>genus Cheilanthes;species castanea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheilanthes</taxon_name>
    <taxon_name authority="forma castanea (Maxon) Correll" date="unknown" rank="species">eatonii</taxon_name>
    <taxon_hierarchy>genus Cheilanthes;species eatonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems compact, 4–8 mm diam.;</text>
      <biological_entity id="o13311" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s0" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales mostly bicolored, with broad, well-defined, dark, central stripe and narrow, light-brown margins, linear-lanceolate, straight to slightly contorted, loosely appressed, persistent.</text>
      <biological_entity id="o13312" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s1" value="bicolored" value_original="bicolored" />
        <character is_modifier="false" name="size_or_width" notes="" src="d0_s1" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity constraint="central" id="o13313" name="stripe" name_original="stripe" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="broad" value_original="broad" />
        <character is_modifier="true" name="prominence" src="d0_s1" value="well-defined" value_original="well-defined" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o13314" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="arrangement_or_shape" src="d0_s1" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o13312" id="r2903" name="with" negation="false" src="d0_s1" to="o13313" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves clustered, 6–35 cm;</text>
    </statement>
    <statement id="d0_s3">
      <text>vernation noncircinate.</text>
      <biological_entity id="o13315" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole dark-brown, rounded adaxially.</text>
      <biological_entity id="o13316" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade oblong-lanceolate, 3–4-pinnate at base, 1.5–5 cm wide;</text>
      <biological_entity id="o13317" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="oblong-lanceolate" value_original="oblong-lanceolate" />
        <character constraint="at base" constraintid="o13318" is_modifier="false" name="architecture_or_shape" src="d0_s5" value="3-4-pinnate" value_original="3-4-pinnate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" notes="" src="d0_s5" to="5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o13318" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>rachis rounded adaxially, with scattered linear-lanceolate scales and monomorphic pubescence.</text>
      <biological_entity id="o13319" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s6" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
      <biological_entity id="o13320" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="shape" src="d0_s6" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <relation from="o13319" id="r2904" name="with" negation="false" src="d0_s6" to="o13320" />
    </statement>
    <statement id="d0_s7">
      <text>Pinnae not articulate, dark color of stalk continuing into pinna base, basal pair not conspicuously larger than adjacent pair, usually equilateral, appearing tomentose to glabrescent adaxially.</text>
      <biological_entity id="o13321" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="articulate" value_original="articulate" />
        <character constraint="of stalk" constraintid="o13322" is_modifier="false" name="coloration" src="d0_s7" value="dark color" value_original="dark color" />
      </biological_entity>
      <biological_entity id="o13322" name="stalk" name_original="stalk" src="d0_s7" type="structure" />
      <biological_entity constraint="pinna" id="o13323" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o13324" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="not conspicuously larger than adjacent pair" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="equilateral" value_original="equilateral" />
        <character char_type="range_value" from="tomentose" modifier="adaxially" name="pubescence" src="d0_s7" to="glabrescent" />
      </biological_entity>
      <relation from="o13322" id="r2905" name="continuing into" negation="false" src="d0_s7" to="o13323" />
    </statement>
    <statement id="d0_s8">
      <text>Costae green adaxially for most of length;</text>
      <biological_entity id="o13325" name="costa" name_original="costae" src="d0_s8" type="structure">
        <character constraint="for" is_modifier="false" name="length" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>abaxial scales multiseriate, lanceolate to linear, truncate or subcordate at base, without overlapping basal lobes, conspicuous, the largest 0.4–0.7 mm wide, loosely imbricate, not concealing ultimate segments, erose-dentate, rarely with 1–2 cilia at base on a few scales.</text>
      <biological_entity constraint="abaxial" id="o13326" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="multiseriate" value_original="multiseriate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="linear truncate or subcordate" />
        <character char_type="range_value" constraint="at base" constraintid="o13327" from="lanceolate" name="shape" src="d0_s9" to="linear truncate or subcordate" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s9" value="conspicuous" value_original="conspicuous" />
        <character is_modifier="false" name="size" src="d0_s9" value="largest" value_original="largest" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s9" to="0.7" to_unit="mm" />
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s9" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o13327" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity constraint="basal" id="o13328" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o13329" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="not" name="position" src="d0_s9" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s9" value="erose-dentate" value_original="erose-dentate" />
      </biological_entity>
      <biological_entity id="o13330" name="cilium" name_original="cilia" src="d0_s9" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s9" to="2" />
      </biological_entity>
      <biological_entity id="o13331" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o13332" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="few" value_original="few" />
      </biological_entity>
      <relation from="o13326" id="r2906" name="without" negation="false" src="d0_s9" to="o13328" />
      <relation from="o13329" id="r2907" modifier="rarely" name="with" negation="false" src="d0_s9" to="o13330" />
      <relation from="o13330" id="r2908" name="at" negation="false" src="d0_s9" to="o13331" />
      <relation from="o13331" id="r2909" name="on" negation="false" src="d0_s9" to="o13332" />
    </statement>
    <statement id="d0_s10">
      <text>Ultimate segments oval to round, beadlike, the largest 1–3 mm, abaxially densely tomentose, adaxially pubescent with fine, unbranched hairs or glabrescent.</text>
      <biological_entity constraint="ultimate" id="o13333" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character char_type="range_value" from="oval" name="shape" src="d0_s10" to="round" />
        <character is_modifier="false" name="shape" src="d0_s10" value="beadlike" value_original="beadlike" />
        <character is_modifier="false" name="size" src="d0_s10" value="largest" value_original="largest" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially densely" name="pubescence" src="d0_s10" value="tomentose" value_original="tomentose" />
        <character constraint="with hairs" constraintid="o13334" is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o13334" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="width" src="d0_s10" value="fine" value_original="fine" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>False indusia marginal to obscurely inframarginal, somewhat differentiated, 0.05–0.25 mm wide.</text>
      <biological_entity constraint="false" id="o13335" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="marginal" value_original="marginal" />
        <character constraint="to obscurely inframarginal , somewhat differentiated , 0.05-0.25 mm" is_modifier="false" name="width" src="d0_s11" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sori ± continuous around segment margins.</text>
      <biological_entity id="o13336" name="sorus" name_original="sori" src="d0_s12" type="structure">
        <character constraint="around segment margins" constraintid="o13337" is_modifier="false" modifier="more or less" name="architecture" src="d0_s12" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity constraint="segment" id="o13337" name="margin" name_original="margins" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Sporangia containing 32 spores.</text>
      <biological_entity id="o13339" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="32" value_original="32" />
      </biological_entity>
      <relation from="o13338" id="r2910" name="containing" negation="false" src="d0_s13" to="o13339" />
    </statement>
    <statement id="d0_s14">
      <text>n = 2n = 90, 120, apogamous.</text>
      <biological_entity id="o13338" name="sporangium" name_original="sporangia" src="d0_s13" type="structure" />
      <biological_entity constraint="n=2n" id="o13340" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" unit=",,apogamous" value="90" value_original="90" />
        <character name="quantity" src="d0_s14" unit=",,apogamous" value="120" value_original="120" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and ledges, found on a variety of substrates including limestone and granite</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ledges" />
        <character name="habitat" value="a variety" constraint="of substrates" />
        <character name="habitat" value="substrates" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="granite" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Ark., Colo., N.Mex., Okla., Tex., Utah, Va., W.Va.; Mexico; Central America in Costa Rica.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America in Costa Rica" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6</number>
  <other_name type="common_name">Eaton's lip fern</other_name>
  <discussion>As here circumscribed, Cheilanthes eatonii is a variable species comprising apogamous triploid and tetraploid cytotypes of unknown parentage. It includes plants previously identified as C. castanea and C. pinkavii (ined.). Type specimens of C. eatonii and C. castanea are quite distinct morphologically, but most plants here included within C. eatonii are intermediate between these two extremes (T. Reeves 1979). Because there is no clear morphologic break, C. castanea is placed here in synonymy under C. eatonii pending further study. Reports of hybridization between C. eatonii and C. villosa (D. B. Lellinger 1985) are based on specimens from western Texas and southern New Mexico that appear to be intermediate between these taxa in several characters. T. Reeves (1979) applied the name C. pinkavii to these specimens; that name has never been validly published. Formal recognition of this taxon is deferred pending completion of a biosystematic study of the C. eatonii complex as a whole.</discussion>
  
</bio:treatment>