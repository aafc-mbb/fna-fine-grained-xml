<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Warren H. Wagner Jr.</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Kaulfuss" date="unknown" rank="family">schizaeaceae</taxon_name>
    <taxon_name authority="Wallich" date="1829" rank="genus">Actinostachys</taxon_name>
    <place_of_publication>
      <publication_title>Numer. List.</publication_title>
      <place_in_publication>1. 1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family schizaeaceae;genus Actinostachys</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek aktis, ray, and stachys, spike, referring to the rays of the fertile leaves</other_info_on_name>
    <other_info_on_name type="fna_id">100468</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial.</text>
      <biological_entity id="o5357" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Roots dark, fibrous, covered with dark, stiff hairs, 2–3 mm.</text>
      <biological_entity id="o5358" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark" value_original="dark" />
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o5359" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="dark" value_original="dark" />
        <character is_modifier="true" name="fragility" src="d0_s1" value="stiff" value_original="stiff" />
      </biological_entity>
      <relation from="o5358" id="r1161" name="covered with" negation="false" src="d0_s1" to="o5359" />
    </statement>
    <statement id="d0_s2">
      <text>Stems upright;</text>
      <biological_entity id="o5360" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="upright" value_original="upright" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>hairs uniseriate.</text>
      <biological_entity id="o5361" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s3" value="uniseriate" value_original="uniseriate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves all fertile (even youngest), unbranched, long-petioled.</text>
      <biological_entity id="o5362" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s4" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="long-petioled" value_original="long-petioled" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blades falsely digitate, reduced to 2–many erect to spreading terminal rays;</text>
      <biological_entity id="o5363" name="blade" name_original="blades" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="falsely" name="architecture" src="d0_s5" value="digitate" value_original="digitate" />
        <character is_modifier="false" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="2-many" value_original="2-many" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s5" to="spreading" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o5364" name="ray" name_original="rays" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>rays appearing to whorled but actually borne on very short rachis.</text>
      <biological_entity id="o5365" name="ray" name_original="rays" src="d0_s6" type="structure" />
      <biological_entity constraint="short" id="o5366" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="whorled" value_original="whorled" />
      </biological_entity>
      <relation from="o5365" id="r1162" name="appearing to" negation="false" src="d0_s6" to="o5366" />
    </statement>
    <statement id="d0_s7">
      <text>Sporangia in 2–4 rows.</text>
      <biological_entity id="o5367" name="sporangium" name_original="sporangia" src="d0_s7" type="structure" />
      <biological_entity id="o5368" name="row" name_original="rows" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s7" to="4" />
      </biological_entity>
      <relation from="o5367" id="r1163" name="in" negation="false" src="d0_s7" to="o5368" />
    </statement>
    <statement id="d0_s8">
      <text>Gametophytes subterranean, not green, tuberlike, brown-hairy.</text>
    </statement>
    <statement id="d0_s9">
      <text>x = 134, 140.</text>
      <biological_entity id="o5369" name="gametophyte" name_original="gametophytes" src="d0_s8" type="structure">
        <character is_modifier="false" name="location" src="d0_s8" value="subterranean" value_original="subterranean" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s8" value="tuberlike" value_original="tuberlike" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="brown-hairy" value_original="brown-hairy" />
      </biological_entity>
      <biological_entity constraint="x" id="o5370" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="134" value_original="134" />
        <character name="quantity" src="d0_s9" value="140" value_original="140" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide in tropical regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide in tropical regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">Ray spiked fern</other_name>
  <discussion>Species 20 (1 in the flora).</discussion>
  <references>
    <reference>Wagner, W. H. Jr. and V. Quevedo. 1985. Polymorphism in Actinostachys pennula  (Swartz) Hooker and the taxonomic status of A. germanii  (Fée) Prantl. [Abstract.] Amer. J. Bot. 72: 927–928.</reference>
  </references>
  
</bio:treatment>