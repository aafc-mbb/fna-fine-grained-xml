<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>David M. Johnson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">marsileaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Marsilea</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1099. 1753; Gen. Pl. ed. 5, 485, 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family marsileaceae;genus Marsilea</taxon_hierarchy>
    <other_info_on_name type="etymology">for Count Luigi Marsigli (1656–1730), Italian mycologist at Bologna</other_info_on_name>
    <other_info_on_name type="fna_id">119753</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants aquatic or amphibious, forming diffuse or dense colonies.</text>
      <biological_entity id="o4399" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="habitat" src="d0_s0" value="aquatic" value_original="aquatic" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="amphibious" value_original="amphibious" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o4400" name="colony" name_original="colonies" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="diffuse" value_original="diffuse" />
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o4399" id="r951" name="forming" negation="false" src="d0_s0" to="o4400" />
    </statement>
    <statement id="d0_s1">
      <text>Roots arising at nodes, sometimes also on internodes.</text>
      <biological_entity id="o4401" name="root" name_original="roots" src="d0_s1" type="structure">
        <character constraint="at nodes" constraintid="o4402" is_modifier="false" name="orientation" src="d0_s1" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o4402" name="node" name_original="nodes" src="d0_s1" type="structure" />
      <biological_entity id="o4403" name="internode" name_original="internodes" src="d0_s1" type="structure" />
      <relation from="o4401" id="r952" modifier="sometimes" name="on" negation="false" src="d0_s1" to="o4403" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves deciduous in temperate regions, heteromorphic, floating leaves averaging larger than land leaves.</text>
      <biological_entity id="o4404" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="in regions" constraintid="o4405" is_modifier="false" name="duration" src="d0_s2" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s2" value="heteromorphic" value_original="heteromorphic" />
      </biological_entity>
      <biological_entity id="o4405" name="region" name_original="regions" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="temperate" value_original="temperate" />
      </biological_entity>
      <biological_entity id="o4406" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="growth_form_or_location" src="d0_s2" value="floating" value_original="floating" />
        <character constraint="than land leaves" constraintid="o4407" is_modifier="false" name="size" src="d0_s2" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o4407" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Petiole filiform, stiffly erect or procumbent in land leaves, lax in floating leaves.</text>
      <biological_entity id="o4408" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="filiform" value_original="filiform" />
        <character is_modifier="false" modifier="stiffly" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character constraint="in land, leaves" constraintid="o4409, o4410" is_modifier="false" name="growth_form" src="d0_s3" value="procumbent" value_original="procumbent" />
        <character constraint="in leaves" constraintid="o4411" is_modifier="false" name="architecture_or_arrangement" notes="" src="d0_s3" value="lax" value_original="lax" />
      </biological_entity>
      <biological_entity id="o4409" name="land" name_original="land" src="d0_s3" type="structure" />
      <biological_entity id="o4410" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o4411" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" name="growth_form_or_location" src="d0_s3" value="floating" value_original="floating" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Blade palmately divided into 4 pinnae.</text>
      <biological_entity id="o4412" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character constraint="into pinnae" constraintid="o4413" is_modifier="false" modifier="palmately" name="shape" src="d0_s4" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o4413" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="4" value_original="4" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pinnae cuneate or obdeltate, pulvinate at base, frequently with numerous red or brown streaks abaxially in floating leaves.</text>
      <biological_entity id="o4414" name="pinna" name_original="pinnae" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="obdeltate" value_original="obdeltate" />
        <character constraint="at base" constraintid="o4415" is_modifier="false" name="shape" src="d0_s5" value="pulvinate" value_original="pulvinate" />
      </biological_entity>
      <biological_entity id="o4415" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o4416" name="streak" name_original="streaks" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="red" value_original="red" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity id="o4417" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" name="growth_form_or_location" src="d0_s5" value="floating" value_original="floating" />
      </biological_entity>
      <relation from="o4414" id="r953" modifier="frequently" name="with" negation="false" src="d0_s5" to="o4416" />
      <relation from="o4416" id="r954" name="in" negation="false" src="d0_s5" to="o4417" />
    </statement>
    <statement id="d0_s6">
      <text>Sporocarps borne on branched or unbranched stalks at or near bases of petioles, aboveground (except in Marsilea ancylopoda), attached laterally to stalk apex (attached portion called raphe), tip of stalk often protruding as bump or tooth (proximal tooth), some species also with tooth distal to stalk apex (distal tooth);</text>
      <biological_entity id="o4418" name="sporocarp" name_original="sporocarps" src="d0_s6" type="structure">
        <character is_modifier="false" name="location" notes="" src="d0_s6" value="aboveground" value_original="aboveground" />
        <character constraint="to stalk apex" constraintid="o4422" is_modifier="false" name="fixation" src="d0_s6" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o4419" name="stalk" name_original="stalks" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="branched" value_original="branched" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o4420" name="base" name_original="bases" src="d0_s6" type="structure" />
      <biological_entity id="o4421" name="petiole" name_original="petioles" src="d0_s6" type="structure" />
      <biological_entity constraint="stalk" id="o4422" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <biological_entity id="o4423" name="tip" name_original="tip" src="d0_s6" type="structure">
        <character constraint="as tooth" constraintid="o4426" is_modifier="false" modifier="often" name="prominence" src="d0_s6" value="protruding" value_original="protruding" />
      </biological_entity>
      <biological_entity id="o4424" name="stalk" name_original="stalk" src="d0_s6" type="structure" />
      <biological_entity id="o4425" name="bump" name_original="bump" src="d0_s6" type="structure" />
      <biological_entity id="o4426" name="tooth" name_original="tooth" src="d0_s6" type="structure" />
      <biological_entity id="o4427" name="species" name_original="species" src="d0_s6" type="taxon_name" />
      <biological_entity id="o4428" name="tooth" name_original="tooth" src="d0_s6" type="structure">
        <character constraint="to stalk apex" constraintid="o4429" is_modifier="false" name="position_or_shape" src="d0_s6" value="distal" value_original="distal" />
      </biological_entity>
      <biological_entity constraint="stalk" id="o4429" name="apex" name_original="apex" src="d0_s6" type="structure" />
      <relation from="o4419" id="r955" name="borne on" negation="false" src="d0_s6" to="o4419" />
      <relation from="o4419" id="r956" name="at" negation="false" src="d0_s6" to="o4420" />
      <relation from="o4420" id="r957" name="part_of" negation="false" src="d0_s6" to="o4421" />
      <relation from="o4423" id="r958" name="part_of" negation="false" src="d0_s6" to="o4424" />
      <relation from="o4427" id="r959" name="with" negation="false" src="d0_s6" to="o4428" />
    </statement>
    <statement id="d0_s7">
      <text>sporocarps densely to sparsely hairy, less so with age, dehiscing into 2 valves.</text>
      <biological_entity id="o4430" name="sporocarp" name_original="sporocarps" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="densely to sparsely" name="pubescence" src="d0_s7" value="hairy" value_original="hairy" />
        <character constraint="into valves" constraintid="o4432" is_modifier="false" name="dehiscence" notes="" src="d0_s7" value="dehiscing" value_original="dehiscing" />
      </biological_entity>
      <biological_entity id="o4431" name="age" name_original="age" src="d0_s7" type="structure" />
      <biological_entity id="o4432" name="valve" name_original="valves" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <relation from="o4430" id="r960" modifier="less" name="with" negation="false" src="d0_s7" to="o4431" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nearly worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Nearly worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Water-clover</other_name>
  <other_name type="common_name">pepperwort</other_name>
  <discussion>Species identification is virtually impossible without fertile material. The common name water-clover refers to the resemblance of the leaves to those of clover (Trifolium spp., Fabaceae); pepperwort refers to the sporocarp, which approximates a peppercorn in size and shape.</discussion>
  <discussion>Ca. 45 species (6 in the flora with 5 native, 1 introduced).</discussion>
  <discussion>Species Ca. 45 (6 in the flora with 5 native, 1 introduced)</discussion>
  <references>
    <reference>Johnson, D. M. 1985. New records for longevity of Marsilea sporocarps. Amer. Fern J. 75: 30–31.</reference>
    <reference>Johnson, D. M. 1988. Proposal to conserve Marsilea L. (Pteridophyta: Marsileaceae) with Marsilea quadrifolia as typ. conserv. Taxon 37: 483–486.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Roots both at nodes and sparsely (1–3) along internodes; stalks of sporocarps frequently branched.</description>
      <determination>1 Marsilea quadrifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Roots only at nodes; stalks of sporocarps branched or unbranched.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distal tooth of sporocarps 0.4–1.2 mm, acute.</description>
      <determination>6 Marsilea vestita</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Distal tooth of sporocarps absent or to 0.4 mm and blunt.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Sporocarps 6–9 mm, strongly ascending; stalks usually branched.</description>
      <determination>5 Marsilea macropoda</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Sporocarps 2.4–6 mm, perpendicular to strongly nodding, i.e., stalk curved or bent; stalks unbranched.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stalks recurved or prostrate, often hooked again at base of raphe; sporocarps borne underground or below stem level.</description>
      <determination>2 Marsilea ancylopoda</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stalks erect, never hooked at base of raphe; sporocarps borne aboveground or above stem level.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sporocarps 2–3 mm wide, proximal tooth of sporocarp 0.2 mm or absent.</description>
      <determination>4 Marsilea mollis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sporocarps 3.6–4 mm wide, proximal tooth of sporocarp 0.2–0.6 mm and curved away from sporocarp.</description>
      <determination>3 Marsilea oligospora</determination>
    </key_statement>
  </key>
</bio:treatment>