<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Agardh" date="unknown" rank="family">ophioglossaceae</taxon_name>
    <taxon_name authority="Swartz" date="1801" rank="genus">botrychium</taxon_name>
    <taxon_name authority="Lyon" date="unknown" rank="subgenus">sceptridium</taxon_name>
    <taxon_name authority="Sect. Sceptridium (Lyon) R. T. Clausen" date="1938" rank="section">Sceptridium</taxon_name>
    <taxon_name authority="W. H. Wagner" date="1982" rank="species">rugulosum</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Univ. Michigan Herb.</publication_title>
      <place_in_publication>15: 315. 1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ophioglossaceae;genus botrychium;subgenus sceptridium;section sceptridium;species rugulosum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500294</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Botrychium</taxon_name>
    <taxon_name authority="(S.G. Gmelin) Ruprecht forma dentatum R.M. Tryon" date="unknown" rank="species">multifidum</taxon_name>
    <taxon_hierarchy>genus Botrychium;species multifidum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trophophore stalk 2 to 15 cm, 1–2.5 times length of trophophore rachis;</text>
      <biological_entity constraint="trophophore" id="o13921" name="stalk" name_original="stalk" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character constraint="rachis" constraintid="o13922" is_modifier="false" name="length" src="d0_s0" value="1-2.5 times length of trophophore rachis" />
      </biological_entity>
      <biological_entity constraint="trophophore" id="o13922" name="rachis" name_original="rachis" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>blade green, finely rugulose and convex distally, 2–4-pinnate, to 15 × 26 cm, somewhat herbaceous.</text>
      <biological_entity id="o13923" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" modifier="finely" name="relief" src="d0_s1" value="rugulose" value_original="rugulose" />
        <character is_modifier="false" modifier="distally" name="shape" src="d0_s1" value="convex" value_original="convex" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="2-4-pinnate" value_original="2-4-pinnate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s1" to="26" to_unit="cm" />
        <character is_modifier="false" modifier="somewhat" name="growth_form_or_texture" src="d0_s1" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pinnae to 9 pairs, usually approximate, horizontal to ascending, distance between 1st and 2d pinnae not or slightly more than between 2d and 3d pairs, divided to tip.</text>
      <biological_entity constraint="between pinnae" constraintid="o13925" id="o13924" name="pinna" name_original="pinnae" src="d0_s2" type="structure" constraint_original="between  pinnae, ">
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="9" />
        <character is_modifier="false" modifier="usually" name="arrangement" src="d0_s2" value="approximate" value_original="approximate" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s2" to="ascending" />
        <character constraint="to tip" constraintid="o13926" is_modifier="false" modifier="slightly; between 2d and 3d pairs" name="shape" src="d0_s2" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity id="o13925" name="pinna" name_original="pinnae" src="d0_s2" type="structure" />
      <biological_entity id="o13926" name="tip" name_original="tip" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Pinnules obliquely and angularly trowel-shaped to spatulate, margins usually denticulate, apex acute, venation pinnate.</text>
      <biological_entity id="o13927" name="pinnule" name_original="pinnules" src="d0_s3" type="structure">
        <character char_type="range_value" from="angularly trowel-shaped" name="shape" src="d0_s3" to="spatulate" />
      </biological_entity>
      <biological_entity id="o13928" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="usually" name="shape" src="d0_s3" value="denticulate" value_original="denticulate" />
      </biological_entity>
      <biological_entity id="o13929" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acute" value_original="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sporophores 2-pinnate, 1–2 times length of trophophore.</text>
      <biological_entity id="o13931" name="trophophore" name_original="trophophore" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>2n =90.</text>
      <biological_entity id="o13930" name="sporophore" name_original="sporophores" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="2-pinnate" value_original="2-pinnate" />
        <character constraint="trophophore" constraintid="o13931" is_modifier="false" name="length" src="d0_s4" value="1-2 times length of trophophore" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13932" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="90" value_original="90" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Leaves green over winter, appearing in midspring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="leaves appearing time" char_type="range_value" to="midspring" from="midspring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>In open fields and secondary forests over wide range in vicinity of St. Lawrence Seaway</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="open fields" modifier="in" />
        <character name="habitat" value="secondary forests" constraint="over wide range in vicinity of st. lawrence seaway" />
        <character name="habitat" value="wide range" constraint="in vicinity of st. lawrence seaway" />
        <character name="habitat" value="vicinity" constraint="of st. lawrence seaway" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1000" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ont., Que.; Mich., Minn., N.Y., Vt., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>8</number>
  <other_name type="common_name">St. Lawrence grapefern</other_name>
  <other_name type="common_name">botryche à limbe rugueux</other_name>
  <discussion>The name "rugulosum" refers to the tendency of the segments to become more or less wrinkled and convex. Botrychium rugulosum occurs with B. dissectum, B. multifidum, and rarely B. oneidense. It is often found in small stands of only 5–10 individuals, but some populations number over 100.</discussion>
  
</bio:treatment>