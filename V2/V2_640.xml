<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan R. Smith</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ching ex Pichi Sermolli" date="unknown" rank="family">thelypteridaceae</taxon_name>
    <taxon_name authority="(C. Presl) Fée" date="1852" rank="genus">Phegopteris</taxon_name>
    <place_of_publication>
      <publication_title>Mém. Foug.</publication_title>
      <place_in_publication>5: 242. 1852</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family thelypteridaceae;genus Phegopteris</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek phegos, beech, and pteris, fern</other_info_on_name>
    <other_info_on_name type="fna_id">124933</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="C. Presl" date="unknown" rank="section">Phegopteris</taxon_name>
    <place_of_publication>
      <publication_title>Tent. Pterid.,</publication_title>
      <place_in_publication>179. 1836</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;section Phegopteris;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Schmidel" date="unknown" rank="genus">Thelypteris</taxon_name>
    <taxon_name authority="(C. Presl) Ching" date="unknown" rank="subgenus">Phegopteris</taxon_name>
    <taxon_hierarchy>genus Thelypteris;subgenus Phegopteris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems long-creeping, 1–4 mm diam.</text>
      <biological_entity id="o10771" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="long-creeping" value_original="long-creeping" />
        <character char_type="range_value" from="1" from_unit="mm" name="diameter" src="d0_s0" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Blades 2–3-pinnatifid in proximal part, broadest at base, apex gradually reduced;</text>
      <biological_entity id="o10772" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character constraint="in proximal part" constraintid="o10773" is_modifier="false" name="shape" src="d0_s1" value="2-3-pinnatifid" value_original="2-3-pinnatifid" />
        <character constraint="at base" constraintid="o10774" is_modifier="false" name="width" notes="" src="d0_s1" value="broadest" value_original="broadest" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o10773" name="part" name_original="part" src="d0_s1" type="structure" />
      <biological_entity id="o10774" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o10775" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s1" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>pinnae deeply lobed, mostly strongly adnate, connected by wing along rachis, wing sometimes forming lobe between pinnae and served by vein arising from rachis;</text>
      <biological_entity id="o10776" name="pinna" name_original="pinnae" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="deeply" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" modifier="mostly strongly" name="fusion" src="d0_s2" value="adnate" value_original="adnate" />
        <character constraint="by wing" constraintid="o10777" is_modifier="false" name="fusion" src="d0_s2" value="connected" value_original="connected" />
      </biological_entity>
      <biological_entity id="o10777" name="wing" name_original="wing" src="d0_s2" type="structure" />
      <biological_entity id="o10778" name="rachis" name_original="rachis" src="d0_s2" type="structure" />
      <biological_entity id="o10779" name="wing" name_original="wing" src="d0_s2" type="structure" />
      <biological_entity id="o10780" name="lobe" name_original="lobe" src="d0_s2" type="structure" />
      <biological_entity id="o10781" name="pinna" name_original="pinnae" src="d0_s2" type="structure" />
      <biological_entity id="o10782" name="vein" name_original="vein" src="d0_s2" type="structure" />
      <biological_entity id="o10783" name="rachis" name_original="rachis" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o10784.o10781-o10782-o10783." name="pinna-vein" name_original="pinnae and vein" src="d0_s2" type="structure" />
      <relation from="o10777" id="r2318" name="along" negation="false" src="d0_s2" to="o10778" />
      <relation from="o10779" id="r2319" name="forming" negation="false" src="d0_s2" to="o10780" />
    </statement>
    <statement id="d0_s3">
      <text>costae not grooved adaxially;</text>
      <biological_entity id="o10785" name="costa" name_original="costae" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not; adaxially" name="architecture" src="d0_s3" value="grooved" value_original="grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>buds absent;</text>
      <biological_entity id="o10786" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>veins free, simple or often forked, distal veins of segment reaching margin or nearly so;</text>
      <biological_entity id="o10787" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s5" value="free" value_original="free" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity constraint="distal" id="o10788" name="vein" name_original="veins" src="d0_s5" type="structure" />
      <biological_entity id="o10789" name="segment" name_original="segment" src="d0_s5" type="structure" />
      <biological_entity id="o10790" name="margin" name_original="margin" src="d0_s5" type="structure" />
      <relation from="o10788" id="r2320" name="part_of" negation="false" src="d0_s5" to="o10789" />
      <relation from="o10788" id="r2321" name="reaching" negation="false" src="d0_s5" to="o10790" />
    </statement>
    <statement id="d0_s6">
      <text>indument abaxially of unbranched, unicellular hairs, rachises and costae also with spreading, ovatelanceolate scales.</text>
      <biological_entity id="o10791" name="indument" name_original="indument" src="d0_s6" type="structure" constraint="hair" constraint_original="hair; hair" />
      <biological_entity id="o10792" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="unbranched" value_original="unbranched" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="unicellular" value_original="unicellular" />
      </biological_entity>
      <biological_entity id="o10793" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="unbranched" value_original="unbranched" />
        <character is_modifier="true" name="architecture" src="d0_s6" value="unicellular" value_original="unicellular" />
      </biological_entity>
      <biological_entity id="o10794" name="costa" name_original="costae" src="d0_s6" type="structure" />
      <biological_entity id="o10795" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="shape" src="d0_s6" value="ovatelanceolate" value_original="ovatelanceolate" />
      </biological_entity>
      <relation from="o10791" id="r2322" name="part_of" negation="false" src="d0_s6" to="o10792" />
      <relation from="o10791" id="r2323" name="part_of" negation="false" src="d0_s6" to="o10793" />
      <relation from="o10791" id="r2324" name="part_of" negation="false" src="d0_s6" to="o10794" />
      <relation from="o10791" id="r2325" name="with" negation="false" src="d0_s6" to="o10795" />
    </statement>
    <statement id="d0_s7">
      <text>Sori round to oblong, supramedial to inframarginal, lacking indusia;</text>
      <biological_entity id="o10796" name="sorus" name_original="sori" src="d0_s7" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s7" to="oblong" />
        <character constraint="to indusia" constraintid="o10797" is_modifier="false" name="position" src="d0_s7" value="supramedial" value_original="supramedial" />
      </biological_entity>
      <biological_entity id="o10797" name="indusium" name_original="indusia" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sporangial capsule often bearing stalked glands or hairs.</text>
      <biological_entity id="o10799" name="gland" name_original="glands" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
      <biological_entity id="o10800" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
      </biological_entity>
      <relation from="o10798" id="r2326" name="bearing" negation="false" src="d0_s8" to="o10799" />
      <relation from="o10798" id="r2327" name="bearing" negation="false" src="d0_s8" to="o10800" />
    </statement>
    <statement id="d0_s9">
      <text>x = 30.</text>
      <biological_entity constraint="sporangial" id="o10798" name="capsule" name_original="capsule" src="d0_s8" type="structure" />
      <biological_entity constraint="x" id="o10801" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North temperate and boreal, 1 in temperate e Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North temperate and boreal" establishment_means="native" />
        <character name="distribution" value="1 in temperate e Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Beech fern</other_name>
  <discussion>Species 3 (2 in the flora).</discussion>
  <references>
    <reference>Holttum, R. E. 1969. Studies in the family Thelypteridaceae. The genera Phegopteris,  Pseudophegopteris, and Macrothelypteris. Blumea 17: 5–32.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Proximal pair of pinnae (7–)10–20 cm, connected to those next above by wing along rachis; scales on costae abaxially whitish to light tan, narrowly lanceolate, mostly 3–5 cells wide at base; hairs of costae abaxially mostly less than 0.25 mm, a few sometimes to 0.5 mm; segments of larger pinnae deeply lobed; veins always forked to pinnate.</description>
      <determination>1 Phegopteris hexagonoptera</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Proximal pair of pinnae 3–10(–15) cm, sessile or slightly adnate to rachis; scales on costae abaxially tan to often shiny brown, ovate-lanceolate, usually 6–12 or more cells wide at base; hairs of costae abaxially mostly more than 0.3 mm with many ca. 0.5 mm; segments of pinna entire or crenate, rarely shallowly lobed; veins in segments of middle pinnae mostly simple.</description>
      <determination>2 Phegopteris connectilis</determination>
    </key_statement>
  </key>
</bio:treatment>