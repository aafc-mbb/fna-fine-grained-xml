<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Newman" date="unknown" rank="family">aspleniaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">asplenium</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">adiantum-nigrum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1081. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aspleniaceae;genus asplenium;species adiantum-nigrum</taxon_hierarchy>
    <other_info_on_name type="fna_id">200004099</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Asplenium</taxon_name>
    <taxon_name authority="A. Nelson" date="unknown" rank="species">andrewsii</taxon_name>
    <taxon_hierarchy>genus Asplenium;species andrewsii;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Asplenium</taxon_name>
    <taxon_name authority="J. G. Baker" date="unknown" rank="species">chihuahuense</taxon_name>
    <taxon_hierarchy>genus Asplenium;species chihuahuense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Asplenium</taxon_name>
    <taxon_name authority="Davenport" date="unknown" rank="species">dubiosum</taxon_name>
    <taxon_hierarchy>genus Asplenium;species dubiosum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots not proliferous.</text>
      <biological_entity id="o15828" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s0" value="proliferous" value_original="proliferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or short-creeping, infrequently branched;</text>
      <biological_entity id="o15829" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="short-creeping" value_original="short-creeping" />
        <character is_modifier="false" modifier="infrequently" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>scales dark-brown to blackish throughout, narrowly deltate, 2–4 (–5) × 0.2–0.5 mm, margins entire or shallowly denticulate to serrulate.</text>
      <biological_entity id="o15830" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character char_type="range_value" from="dark-brown" modifier="throughout" name="coloration" src="d0_s2" to="blackish" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s2" to="4" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s2" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15831" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character char_type="range_value" from="shallowly denticulate" name="shape" src="d0_s2" to="serrulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves monomorphic.</text>
      <biological_entity id="o15832" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole dark reddish-brown proximally, often fading to green distally, lustrous, 2–20 cm, 2/3–2 times length of blade;</text>
      <biological_entity id="o15833" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="proximally" name="coloration" src="d0_s4" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character char_type="range_value" from="fading" modifier="often; distally" name="coloration" src="d0_s4" to="green" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="lustrous" value_original="lustrous" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="20" to_unit="cm" />
        <character constraint="blade" constraintid="o15834" is_modifier="false" name="length" src="d0_s4" value="2/3-2 times length of blade" />
      </biological_entity>
      <biological_entity id="o15834" name="blade" name_original="blade" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>indument of black filiform scales and minute hairs.</text>
      <biological_entity id="o15835" name="indument" name_original="indument" src="d0_s5" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity id="o15836" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="black" value_original="black" />
        <character is_modifier="true" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o15837" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="minute" value_original="minute" />
      </biological_entity>
      <relation from="o15835" id="r3454" name="part_of" negation="false" src="d0_s5" to="o15836" />
      <relation from="o15835" id="r3455" name="part_of" negation="false" src="d0_s5" to="o15837" />
    </statement>
    <statement id="d0_s6">
      <text>Blade deltate, 2–3-pinnate, 2.5–10 × 2–6.5 cm, thick, hairs dark, scattered, minute;</text>
      <biological_entity id="o15838" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="2-3-pinnate" value_original="2-3-pinnate" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="length" src="d0_s6" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s6" to="6.5" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s6" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o15839" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark" value_original="dark" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="size" src="d0_s6" value="minute" value_original="minute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base truncate;</text>
      <biological_entity id="o15840" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="truncate" value_original="truncate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex acute to acuminate, not rooting.</text>
      <biological_entity id="o15841" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s8" to="acuminate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="rooting" value_original="rooting" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Rachis greenish throughout or sometimes reddish-brown proximally, lustrous, sparsely pubescent.</text>
      <biological_entity id="o15842" name="rachis" name_original="rachis" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="throughout; throughout; sometimes proximally; proximally" name="coloration" src="d0_s9" value="greenish throughout or sometimes reddish-brown" />
        <character is_modifier="false" name="reflectance" src="d0_s9" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pinnae in 4–10 pairs, deltate to lanceolate;</text>
      <biological_entity id="o15844" name="pair" name_original="pairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="4" is_modifier="true" name="quantity" src="d0_s10" to="10" />
      </biological_entity>
      <relation from="o15843" id="r3456" name="in" negation="false" src="d0_s10" to="o15844" />
    </statement>
    <statement id="d0_s11">
      <text>most proximal (largest) pinnae 1.5–4 × 1–2.5 cm;</text>
      <biological_entity id="o15843" name="pinna" name_original="pinnae" src="d0_s10" type="structure">
        <character char_type="range_value" from="deltate" name="shape" notes="" src="d0_s10" to="lanceolate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o15845" name="pinna" name_original="pinnae" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s11" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s11" to="2.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>base obliquely obtuse;</text>
      <biological_entity id="o15846" name="base" name_original="base" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s12" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>segment margins coarsely incised;</text>
      <biological_entity constraint="segment" id="o15847" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="coarsely" name="shape" src="d0_s13" value="incised" value_original="incised" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>apex acute.</text>
      <biological_entity id="o15848" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Veins free, evident.</text>
      <biological_entity id="o15849" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
        <character is_modifier="false" name="prominence" src="d0_s15" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Sori 1–numerous pairs per pinna [1–6 pairs per segment], on both basiscopic and acroscopic sides.</text>
      <biological_entity id="o15850" name="sorus" name_original="sori" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per pinna" constraintid="o15851" from="1" is_modifier="false" name="quantity" src="d0_s16" to="numerous" />
      </biological_entity>
      <biological_entity id="o15851" name="pinna" name_original="pinna" src="d0_s16" type="structure" />
      <biological_entity id="o15852" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s16" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="true" name="orientation" src="d0_s16" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o15850" id="r3457" name="on" negation="false" src="d0_s16" to="o15852" />
    </statement>
    <statement id="d0_s17">
      <text>Spores 64 per sporangium.</text>
      <biological_entity id="o15854" name="sporangium" name_original="sporangium" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 144.</text>
      <biological_entity id="o15853" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character constraint="per sporangium" constraintid="o15854" name="quantity" src="d0_s17" value="64" value_original="64" />
      </biological_entity>
      <biological_entity constraint="2n" id="o15855" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="144" value_original="144" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1675–2300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2300" to_unit="m" from="1675" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., Utah; Eurasia; Africa.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Eurasia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22</number>
  <other_name type="common_name">Black spleenwort</other_name>
  <discussion>Asplenium adiantum-nigrum is principally a Eurasian species and occurs extremely rarely in North America (see M. G. Shivas 1969 and M. D. Windham 1983 for a discussion of the conspecificity of Western Hemisphere and Eastern Hemisphere material). It is an allotetraploid derived from hybridization of two European taxa, A. cuneifolium Viviani and A. onopteris Linnaeus (M. G. Shivas 1969). Hybrids involving A. adiantum-nigrum and other Asplenium species occur in Europe but are unknown in North America.</discussion>
  
</bio:treatment>