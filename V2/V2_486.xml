<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">pinus</taxon_name>
    <taxon_name authority="Douglas" date="1827" rank="species">lambertiana</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Linn. Soc. London, Bot.</publication_title>
      <place_in_publication>15: 500. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus pinus;species lambertiana</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500939</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 75m;</text>
      <biological_entity id="o16140" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="75" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 3.3m diam., massive, straight;</text>
      <biological_entity id="o16141" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="3.3" to_unit="m" />
        <character is_modifier="false" name="size" src="d0_s1" value="massive" value_original="massive" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>crown narrowly conic, becoming rounded.</text>
      <biological_entity id="o16142" name="crown" name_original="crown" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark cinnamon to gray-brown, deeply furrowed, plates long, scaly.</text>
      <biological_entity id="o16143" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character char_type="range_value" from="cinnamon" name="coloration" src="d0_s3" to="gray-brown" />
        <character is_modifier="false" modifier="deeply" name="architecture" src="d0_s3" value="furrowed" value_original="furrowed" />
      </biological_entity>
      <biological_entity id="o16144" name="plate" name_original="plates" src="d0_s3" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s3" value="long" value_original="long" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Branches spreading, distal branches ascending;</text>
      <biological_entity id="o16145" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="distal" id="o16146" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>twigs gray-green to red-tan, aging gray, mostly puberulent.</text>
      <biological_entity id="o16147" name="twig" name_original="twigs" src="d0_s5" type="structure">
        <character char_type="range_value" from="gray-green" name="coloration" src="d0_s5" to="red-tan" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="gray" value_original="gray" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Buds cylindro-ovoid, redbrown, to 0.8cm, resinous.</text>
      <biological_entity id="o16148" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindro-ovoid" value_original="cylindro-ovoid" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s6" to="0.8" to_unit="cm" />
        <character is_modifier="false" name="coating" src="d0_s6" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves 5 per fascicle, spreading to ascending, persisting 2–4 years, 5–10cm × (0.9–) 1–1.5 (–2) mm, straight, slightly twisted, pliant, blue-green, abaxial surface with only a few lines evident, adaxial surfaces with evident white stomatal lines, margins finely serrulate, apex acuminate;</text>
      <biological_entity id="o16149" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character constraint="per fascicle" constraintid="o16150" name="quantity" src="d0_s7" value="5" value_original="5" />
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s7" to="ascending" />
        <character is_modifier="false" name="duration" src="d0_s7" value="persisting" value_original="persisting" />
        <character char_type="range_value" from="2" name="quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.9" from_unit="mm" name="atypical_width" src="d0_s7" to="1" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s7" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="fragility_or_texture" src="d0_s7" value="pliant" value_original="pliant" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue-green" value_original="blue-green" />
      </biological_entity>
      <biological_entity id="o16150" name="fascicle" name_original="fascicle" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o16151" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity id="o16152" name="line" name_original="lines" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="only" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16153" name="surface" name_original="surfaces" src="d0_s7" type="structure" />
      <biological_entity id="o16154" name="stomatal" name_original="stomatal" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="evident" value_original="evident" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o16155" name="line" name_original="lines" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="evident" value_original="evident" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="white" value_original="white" />
      </biological_entity>
      <biological_entity id="o16156" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o16157" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <relation from="o16151" id="r3538" name="with" negation="false" src="d0_s7" to="o16152" />
      <relation from="o16153" id="r3539" name="with" negation="false" src="d0_s7" to="o16154" />
      <relation from="o16153" id="r3540" name="with" negation="false" src="d0_s7" to="o16155" />
    </statement>
    <statement id="d0_s8">
      <text>sheath (1–) 1.5–2cm, shed early.</text>
      <biological_entity id="o16158" name="sheath" name_original="sheath" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="atypical_some_measurement" src="d0_s8" to="1.5" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s8" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pollen cones ellipsoid-cylindric, to 15mm, yellow.</text>
      <biological_entity constraint="pollen" id="o16159" name="cone" name_original="cones" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ellipsoid-cylindric" value_original="ellipsoid-cylindric" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seed-cones maturing in 2 years, shedding seeds and falling soon thereafter, often clustered, pendent, symmetric, cylindric before opening, lance-cylindric to ellipsoid-cylindric when open, 25–50cm, yellowbrown, stalks 6–15cm;</text>
      <biological_entity id="o16160" name="cone-seed" name_original="seed-cones" src="d0_s10" type="structure">
        <character constraint="in years" constraintid="o16161" is_modifier="false" name="life_cycle" src="d0_s10" value="maturing" value_original="maturing" />
        <character is_modifier="false" modifier="soon thereafter; thereafter" name="life_cycle" src="d0_s10" value="falling" value_original="falling" />
        <character is_modifier="false" modifier="often" name="arrangement_or_growth_form" src="d0_s10" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character constraint="before opening" constraintid="o16163" is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="lance-cylindric" modifier="when open" name="shape" notes="" src="d0_s10" to="ellipsoid-cylindric" />
        <character char_type="range_value" from="25" from_unit="cm" name="distance" src="d0_s10" to="50" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellowbrown" value_original="yellowbrown" />
      </biological_entity>
      <biological_entity id="o16161" name="year" name_original="years" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o16162" name="seed" name_original="seeds" src="d0_s10" type="structure" />
      <biological_entity id="o16163" name="opening" name_original="opening" src="d0_s10" type="structure" />
      <biological_entity id="o16164" name="stalk" name_original="stalks" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s10" to="15" to_unit="cm" />
      </biological_entity>
      <relation from="o16160" id="r3541" name="shedding" negation="false" src="d0_s10" to="o16162" />
    </statement>
    <statement id="d0_s11">
      <text>apophyses somewhat thickened;</text>
      <biological_entity id="o16165" name="apophysis" name_original="apophyses" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="somewhat" name="size_or_width" src="d0_s11" value="thickened" value_original="thickened" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>umbo terminal, depressed, resinous, slightly excurved.</text>
      <biological_entity id="o16166" name="umbo" name_original="umbo" src="d0_s12" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s12" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s12" value="depressed" value_original="depressed" />
        <character is_modifier="false" name="coating" src="d0_s12" value="resinous" value_original="resinous" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s12" value="excurved" value_original="excurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds obovoid, oblique apically;</text>
      <biological_entity id="o16167" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="obovoid" value_original="obovoid" />
        <character is_modifier="false" modifier="apically" name="orientation_or_shape" src="d0_s13" value="oblique" value_original="oblique" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>body 1–2cm, deep brown;</text>
      <biological_entity id="o16168" name="body" name_original="body" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s14" to="2" to_unit="cm" />
        <character is_modifier="false" name="depth" src="d0_s14" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>wing broad, 2–3cm.</text>
    </statement>
    <statement id="d0_s16">
      <text>2n =24.</text>
      <biological_entity id="o16169" name="wing" name_original="wing" src="d0_s15" type="structure">
        <character is_modifier="false" name="width" src="d0_s15" value="broad" value_original="broad" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s15" to="3" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16170" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane dry to moist forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane dry to moist forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>330–3200m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3200" to_unit="m" from="330" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Oreg.; Mexico in n Baja California.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" value="Mexico in n Baja California" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <other_name type="common_name">Sugar pine</other_name>
  <discussion>The largest species of the genus, Pinus lambertiana also has the longest seed cone in the genus. It is an important timber tree with harvest far exceeding regrowth. It is easily distinguished from P. monticola and P. strobus by its larger cones and thicker cone scales with larger seeds; it is somewhat less reliably distinguished by its leaves, which are slightly wider and more tapering-tipped and have some stomatal lines evident on the abaxial surfaces (the lines not evident in P. monticola and P. strobus). A "sugary" resin high in cyclitols exudes from the sweet-scented fresh-cut wood.</discussion>
  
</bio:treatment>