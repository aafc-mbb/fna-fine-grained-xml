<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">370</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="A. Dietrich" date="1824" rank="genus">picea</taxon_name>
    <taxon_name authority="(Linnaeus) H. Karsten" date="1881" rank="species">abies</taxon_name>
    <place_of_publication>
      <publication_title>Deut. Fl.</publication_title>
      <place_in_publication>2/3: 324. 1881</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus picea;species abies</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">200005295</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pinus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">abies</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1002. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pinus;species abies;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 30m;</text>
      <biological_entity id="o13236" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="30" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 2m diam.;</text>
      <biological_entity id="o13237" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="2" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>crown conic.</text>
      <biological_entity id="o13238" name="crown" name_original="crown" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark gray-brown, scaly.</text>
      <biological_entity id="o13239" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray-brown" value_original="gray-brown" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Branches short and stout, the upper ascending, the lower drooping;</text>
      <biological_entity id="o13240" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s4" value="short" value_original="short" />
        <character is_modifier="false" name="fragility_or_size" src="d0_s4" value="stout" value_original="stout" />
      </biological_entity>
      <biological_entity constraint="upper" id="o13241" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="lower" id="o13242" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="drooping" value_original="drooping" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>twigs stout, reddish-brown, usually glabrous.</text>
      <biological_entity id="o13243" name="twig" name_original="twigs" src="d0_s5" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s5" value="stout" value_original="stout" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Buds reddish-brown, 5–7mm, apex acute.</text>
      <biological_entity id="o13244" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="reddish-brown" value_original="reddish-brown" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13245" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves 1–2.5cm, 4-angled in cross-section, rigid, light to dark green, bearing stomates on all surfaces, apex blunt-tipped.</text>
      <biological_entity id="o13246" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s7" to="2.5" to_unit="cm" />
        <character constraint="in cross-section" constraintid="o13247" is_modifier="false" name="shape" src="d0_s7" value="4-angled" value_original="4-angled" />
        <character is_modifier="false" name="texture" notes="" src="d0_s7" value="rigid" value_original="rigid" />
        <character char_type="range_value" from="light" name="coloration" src="d0_s7" to="dark green" />
      </biological_entity>
      <biological_entity id="o13247" name="cross-section" name_original="cross-section" src="d0_s7" type="structure" />
      <biological_entity id="o13248" name="stomate" name_original="stomates" src="d0_s7" type="structure" />
      <biological_entity id="o13249" name="surface" name_original="surfaces" src="d0_s7" type="structure" />
      <biological_entity id="o13250" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="blunt-tipped" value_original="blunt-tipped" />
      </biological_entity>
      <relation from="o13246" id="r2886" name="bearing" negation="false" src="d0_s7" to="o13248" />
      <relation from="o13246" id="r2887" name="on" negation="false" src="d0_s7" to="o13249" />
    </statement>
    <statement id="d0_s8">
      <text>Seed-cones (10–) 12–16cm;</text>
      <biological_entity id="o13251" name="cone-seed" name_original="seed-cones" src="d0_s8" type="structure">
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_distance" src="d0_s8" to="12" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="12" from_unit="cm" name="distance" src="d0_s8" to="16" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>scales diamond-shaped, widest near middle, 18–30 × 15–20mm, thin and flexuous, margin at apex erose to toothed, apex extending 6–10mm beyond seed-wing impression.</text>
      <biological_entity id="o13252" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="diamond--shaped" value_original="diamond--shaped" />
        <character constraint="near middle scales" constraintid="o13253" is_modifier="false" name="width" src="d0_s9" value="widest" value_original="widest" />
        <character char_type="range_value" from="18" from_unit="mm" name="length" notes="" src="d0_s9" to="30" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s9" value="thin" value_original="thin" />
        <character is_modifier="false" name="course" src="d0_s9" value="flexuous" value_original="flexuous" />
      </biological_entity>
      <biological_entity constraint="middle" id="o13253" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character char_type="range_value" from="15" from_unit="mm" name="width" notes="" src="d0_s9" to="20" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o13254" name="margin" name_original="margin" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="toothed" value_original="toothed" />
      </biological_entity>
      <biological_entity id="o13255" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s9" value="erose" value_original="erose" />
      </biological_entity>
      <relation from="o13254" id="r2888" name="at" negation="false" src="d0_s9" to="o13255" />
    </statement>
    <statement id="d0_s10">
      <text>2n =24.</text>
      <biological_entity id="o13256" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity constraint="2n" id="o13257" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Norway spruce</other_name>
  <other_name type="common_name">épinette de Norvège</other_name>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Woods and persisting after cultivation.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woods" />
        <character name="habitat" value="cultivation" modifier="and persisting after" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Minn., probably elsewhere; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" value="probably elsewhere" establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Norway spruce, native to Europe, has become locally naturalized, at least in north central United States (and adjacent Canada). The species is the most widely cultivated spruce in North America; many cultivars exist, including dwarf shrubs.</discussion>
  
</bio:treatment>