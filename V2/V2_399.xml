<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">20</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">lycopodiaceae</taxon_name>
    <taxon_name authority="Holub" date="1964" rank="genus">phlegmariurus</taxon_name>
    <taxon_name authority="(Jacquin) W. H. Wagner &amp; Beitel" date="1992" rank="species">dichotomus</taxon_name>
    <place_of_publication>
      <publication_title>Novon</publication_title>
      <place_in_publication>3: 305. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lycopodiaceae;genus phlegmariurus;species dichotomus</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500906</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lycopodium</taxon_name>
    <taxon_name authority="Jacquin" date="unknown" rank="species">dichotomum</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Stirp. Vindob.,</publication_title>
      <place_in_publication>314. 1762</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lycopodium;species dichotomum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Huperzia</taxon_name>
    <taxon_name authority="(Jacquin) Trevisan" date="unknown" rank="species">dichotoma</taxon_name>
    <taxon_hierarchy>genus Huperzia;species dichotoma;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Shoots pendent, 1–3-forked, clustered at base, 10–30 cm, long-lived.</text>
      <biological_entity id="o10007" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="shape" src="d0_s0" value="1-3-forked" value_original="1-3-forked" />
        <character constraint="at base" constraintid="o10008" is_modifier="false" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" notes="" src="d0_s0" to="30" to_unit="cm" />
        <character is_modifier="false" name="duration" src="d0_s0" value="long-lived" value_original="long-lived" />
      </biological_entity>
      <biological_entity id="o10008" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Roots densely covered with fine grayish uniseriate hairs, to 1 mm.</text>
      <biological_entity id="o10009" name="root" name_original="roots" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o10010" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="true" name="width" src="d0_s1" value="fine" value_original="fine" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="grayish" value_original="grayish" />
        <character is_modifier="true" name="architecture_or_arrangement" src="d0_s1" value="uniseriate" value_original="uniseriate" />
      </biological_entity>
      <relation from="o10009" id="r2157" modifier="densely" name="covered with" negation="false" src="d0_s1" to="o10010" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves spreading to ascending, 15–20 × 0.5–12 mm, very gradually narrowed from base to long-attenuate apex;</text>
      <biological_entity id="o10011" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="ascending" />
        <character char_type="range_value" from="15" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s2" to="12" to_unit="mm" />
        <character constraint="from base" constraintid="o10012" is_modifier="false" modifier="very gradually" name="shape" src="d0_s2" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o10012" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o10013" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="long-attenuate" value_original="long-attenuate" />
      </biological_entity>
      <relation from="o10012" id="r2158" name="to" negation="false" src="d0_s2" to="o10013" />
    </statement>
    <statement id="d0_s3">
      <text>margins entire.</text>
      <biological_entity id="o10014" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Fertile leaves reduced, 7–12 × 0.2–0.5 mm.</text>
      <biological_entity id="o10015" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s4" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="size" src="d0_s4" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s4" to="12" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s4" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Sporangia 2–3 times width of subtending leaf.</text>
      <biological_entity id="o10016" name="sporangium" name_original="sporangia" src="d0_s5" type="structure">
        <character constraint="leaf" constraintid="o10017" is_modifier="false" name="width" src="d0_s5" value="2-3 times width of subtending leaf" />
      </biological_entity>
      <biological_entity constraint="subtending" id="o10017" name="leaf" name_original="leaf" src="d0_s5" type="structure" />
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Hanging fir-moss</other_name>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Epiphytic on trunks and branches of trees in tropical and subtropical regions.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="epiphytic" constraint="on trunks and branches" />
        <character name="habitat" value="trunks" />
        <character name="habitat" value="branches" />
        <character name="habitat" value="trees" />
        <character name="habitat" value="tropical" />
        <character name="habitat" value="subtropical regions" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies; Central America; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>In the flora, Phlegmariurus dichotomus is known only from Big Cypress Swamp, Florida.</discussion>
  
</bio:treatment>