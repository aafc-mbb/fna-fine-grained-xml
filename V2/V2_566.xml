<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan R. Smith</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="(Nuttall ex Hooker &amp; Baker) Copeland" date="1947" rank="genus">Aspidotis</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Fil.</publication_title>
      <place_in_publication>68. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus Aspidotis</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek aspidotes, shield-bearer, for the shieldlike false indusia</other_info_on_name>
    <other_info_on_name type="fna_id">102870</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Bernhardi" date="unknown" rank="genus">Hypolepis</taxon_name>
    <taxon_name authority="Nuttall ex Hooker &amp; Baker" date="unknown" rank="section">Aspidotis</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Fil.</publication_title>
      <place_in_publication>4: 131. 1867</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Hypolepis;section Aspidotis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial or on rock.</text>
      <biological_entity id="o8121" name="whole_organism" name_original="" src="" type="structure">
        <character constraint="on rock" is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="on rock" value_original="on rock" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ± compact, short-creeping, ascending at tip, branched;</text>
      <biological_entity id="o8122" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="short-creeping" value_original="short-creeping" />
        <character constraint="at tip" constraintid="o8123" is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o8123" name="tip" name_original="tip" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>scales mostly dark-brown, often with very narrow margin of lighter color, lanceolate, margins entire.</text>
      <biological_entity id="o8124" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="mostly" name="coloration" src="d0_s2" value="dark-brown" value_original="dark-brown" />
      </biological_entity>
      <biological_entity id="o8125" name="margin" name_original="margin" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="very" name="size_or_width" src="d0_s2" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o8126" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="lighter color" value_original="lighter color" />
        <character is_modifier="true" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
      <relation from="o8124" id="r1699" modifier="often" name="with" negation="false" src="d0_s2" to="o8125" />
      <relation from="o8125" id="r1700" name="part_of" negation="false" src="d0_s2" to="o8126" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves monomorphic to somewhat dimorphic, crowded, 8–35 cm.</text>
      <biological_entity id="o8127" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="somewhat" name="growth_form" src="d0_s3" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="crowded" value_original="crowded" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s3" to="35" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole usually dark reddish-brown, with single groove adaxially, glabrous, with single vascular-bundle.</text>
      <biological_entity id="o8128" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s4" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o8129" name="groove" name_original="groove" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="single" value_original="single" />
      </biological_entity>
      <biological_entity id="o8130" name="vascular-bundle" name_original="vascular-bundle" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="single" value_original="single" />
      </biological_entity>
      <relation from="o8128" id="r1701" name="with" negation="false" src="d0_s4" to="o8129" />
      <relation from="o8128" id="r1702" name="with" negation="false" src="d0_s4" to="o8130" />
    </statement>
    <statement id="d0_s5">
      <text>Blade ovate-triangular, deltate, or pentagonal, 3–4 (–5) -pinnate, thick to thin, abaxially glabrous, adaxially lustrous, striate, glabrous;</text>
      <biological_entity id="o8131" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate-triangular" value_original="ovate-triangular" />
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pentagonal" value_original="pentagonal" />
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="pentagonal" value_original="pentagonal" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="3-4(-5)-pinnate" value_original="3-4(-5)-pinnate" />
        <character char_type="range_value" from="thick" name="width" src="d0_s5" to="thin" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="reflectance" src="d0_s5" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="coloration_or_pubescence_or_relief" src="d0_s5" value="striate" value_original="striate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachis straight.</text>
      <biological_entity id="o8132" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Ultimate segments of blades short-stalked or with base narrowed and decurrent onto costa or costule-bearing segments, linear to lanceolate, mostly 0.5–1.3 mm wide;</text>
      <biological_entity constraint="ultimate" id="o8133" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s7" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="with base" value_original="with base" />
        <character constraint="onto segments" constraintid="o8137" is_modifier="false" name="shape" src="d0_s7" value="decurrent" value_original="decurrent" />
        <character char_type="range_value" from="linear" name="shape" notes="" src="d0_s7" to="lanceolate" />
        <character char_type="range_value" from="0.5" from_unit="mm" modifier="mostly" name="width" src="d0_s7" to="1.3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o8134" name="blade" name_original="blades" src="d0_s7" type="structure" />
      <biological_entity id="o8135" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o8136" name="costa" name_original="costa" src="d0_s7" type="structure" />
      <biological_entity id="o8137" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="costule-bearing" value_original="costule-bearing" />
      </biological_entity>
      <relation from="o8133" id="r1703" name="part_of" negation="false" src="d0_s7" to="o8134" />
      <relation from="o8133" id="r1704" name="with" negation="false" src="d0_s7" to="o8135" />
    </statement>
    <statement id="d0_s8">
      <text>stalks greenish, not darkened;</text>
      <biological_entity id="o8138" name="stalk" name_original="stalks" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s8" value="darkened" value_original="darkened" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>fertile margins recurved.</text>
      <biological_entity id="o8139" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="orientation" src="d0_s9" value="recurved" value_original="recurved" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Veins of ultimate segments obscure, free, ± pinnate and unbranched.</text>
      <biological_entity id="o8140" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s10" value="obscure" value_original="obscure" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s10" value="pinnate" value_original="pinnate" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o8141" name="segment" name_original="segments" src="d0_s10" type="structure" />
      <relation from="o8140" id="r1705" name="part_of" negation="false" src="d0_s10" to="o8141" />
    </statement>
    <statement id="d0_s11">
      <text>False indusia appearing inframarginal, scarious, whitish, broad, partly concealing sporangia.</text>
      <biological_entity constraint="false" id="o8142" name="indusium" name_original="indusia" src="d0_s11" type="structure" />
      <biological_entity id="o8143" name="sporangium" name_original="sporangia" src="d0_s11" type="structure">
        <character is_modifier="true" name="texture" src="d0_s11" value="scarious" value_original="scarious" />
        <character is_modifier="true" name="coloration" src="d0_s11" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="width" src="d0_s11" value="broad" value_original="broad" />
        <character is_modifier="true" modifier="partly" name="position" src="d0_s11" value="concealing" value_original="concealing" />
      </biological_entity>
      <relation from="o8142" id="r1706" name="appearing inframarginal" negation="false" src="d0_s11" to="o8143" />
    </statement>
    <statement id="d0_s12">
      <text>Sporangia in marginal, discrete or continuous sori on abaxial surface, containing 64 spores, lacking paraphyses and glands.</text>
      <biological_entity id="o8144" name="sporangium" name_original="sporangia" src="d0_s12" type="structure" />
      <biological_entity id="o8145" name="sorus" name_original="sori" src="d0_s12" type="structure">
        <character is_modifier="true" name="position" src="d0_s12" value="marginal" value_original="marginal" />
        <character is_modifier="true" name="fusion" src="d0_s12" value="discrete" value_original="discrete" />
        <character is_modifier="true" name="architecture" src="d0_s12" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o8146" name="surface" name_original="surface" src="d0_s12" type="structure" />
      <biological_entity id="o8147" name="spore" name_original="spores" src="d0_s12" type="structure" />
      <biological_entity id="o8148" name="paraphyse" name_original="paraphyses" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o8149" name="gland" name_original="glands" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="lacking" value_original="lacking" />
      </biological_entity>
      <relation from="o8144" id="r1707" name="in" negation="false" src="d0_s12" to="o8145" />
      <relation from="o8145" id="r1708" name="on" negation="false" src="d0_s12" to="o8146" />
      <relation from="o8144" id="r1709" name="containing" negation="false" src="d0_s12" to="o8147" />
      <relation from="o8144" id="r1710" name="containing" negation="false" src="d0_s12" to="o8148" />
      <relation from="o8144" id="r1711" name="containing" negation="false" src="d0_s12" to="o8149" />
    </statement>
    <statement id="d0_s13">
      <text>Spores dark-brown, tetrahedral-globose, trilete, reticulate, equatorial flange absent.</text>
      <biological_entity id="o8150" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s13" value="tetrahedral-globose" value_original="tetrahedral-globose" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="trilete" value_original="trilete" />
        <character is_modifier="false" name="architecture_or_coloration_or_relief" src="d0_s13" value="reticulate" value_original="reticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>x = 30.</text>
      <biological_entity constraint="equatorial" id="o8151" name="flange" name_original="flange" src="d0_s13" type="structure">
        <character is_modifier="false" name="presence" src="d0_s13" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o8152" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="30" value_original="30" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, 1 in Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="1 in Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11</number>
  <other_name type="common_name">Lace ferns</other_name>
  <other_name type="common_name">aspidote</other_name>
  <discussion>D. B. Lellinger (1968) recognized Aspidotis as separate from Cheilanthes based on its elongate, distantly dentate segments with striate shining surface and on its broad, scarious indusia.</discussion>
  <discussion>Species 4 (3 in the flora).</discussion>
  <references>
    <reference>Smith, A. R. 1975. The California species of Aspidotis. Madroño 23: 15–24.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Mature, fertile blades with continuous sori along length of segments (not at apex); indusia with 10–35 shallow, regular teeth or erose; fertile segments linear, margins ± entire.</description>
      <determination>3 Aspidotis densa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Mature, fertile blades with sori discrete or partially discontinuous; indusia with coarse, irregular teeth or entire; fertile segments lanceolate to deltate, distantly dentate.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sori discrete, 1–3(–5) per blade segment; indusia margins with 2–6 coarse, irregular teeth or ± entire.</description>
      <determination>1 Aspidotis californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sori partially discontinuous, connected by narrow indusial wings, 3–7(–9) per blade segment; indusia margins with 6–10 coarse, irregular teeth or lobes.</description>
      <determination>2 Aspidotis carlotta-halliae</determination>
    </key_statement>
  </key>
</bio:treatment>