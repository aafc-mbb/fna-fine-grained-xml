<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Presl" date="unknown" rank="family">blechnaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">blechnum</taxon_name>
    <taxon_name authority="Linnaeus" date="1763" rank="species">occidentale</taxon_name>
    <taxon_name authority="Hooker" date="1860" rank="variety">minor</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Fil.</publication_title>
      <place_in_publication>3: 51. 1860</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family blechnaceae;genus blechnum;species occidentale;variety minor</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500267</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Blechnum</taxon_name>
    <taxon_name authority="forma pubirachis (Rosenstock) Lellinger" date="unknown" rank="species">glandulosum</taxon_name>
    <place_of_publication>
      <publication_title>in Link</publication_title>
    </place_of_publication>
    <taxon_hierarchy>genus Blechnum;species glandulosum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Blechnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">occidentale</taxon_name>
    <taxon_hierarchy>genus Blechnum;species occidentale;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Blechnum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">occidentale</taxon_name>
    <taxon_name authority="Rosenstock" date="unknown" rank="variety">pubirachis</taxon_name>
    <taxon_hierarchy>genus Blechnum;species occidentale;variety pubirachis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems slender, creeping, elongate, branched, ascending to erect at tip, not climbing.</text>
      <biological_entity id="o253" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="size" src="d0_s0" value="slender" value_original="slender" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="shape" src="d0_s0" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="branched" value_original="branched" />
        <character char_type="range_value" constraint="at tip" constraintid="o254" from="ascending" name="orientation" src="d0_s0" to="erect" />
        <character is_modifier="false" modifier="not" name="growth_form" notes="" src="d0_s0" value="climbing" value_original="climbing" />
      </biological_entity>
      <biological_entity id="o254" name="tip" name_original="tip" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Leaves ± monomorphic, cespitose to widely spaced, erect to arching, fertile leaves only slightly contracted and longer than sterile leaves.</text>
      <biological_entity id="o255" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="cespitose" value_original="cespitose" />
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s1" value="spaced" value_original="spaced" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="arching" />
      </biological_entity>
      <biological_entity id="o256" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="only slightly" name="condition_or_size" src="d0_s1" value="contracted" value_original="contracted" />
        <character constraint="than sterile leaves" constraintid="o257" is_modifier="false" name="length_or_size" src="d0_s1" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o257" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s1" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petiole straw-colored to light-brown, (4-) 8-34 cm, coarsely scaly proximally.</text>
      <biological_entity id="o258" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="straw-colored" name="coloration" src="d0_s2" to="light-brown" />
        <character char_type="range_value" from="4" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="8" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s2" to="34" to_unit="cm" />
        <character is_modifier="false" modifier="coarsely; proximally" name="architecture_or_pubescence" src="d0_s2" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Blade narrowly to broadly lanceolate, 1-pinnate proximally, becoming pinnatifid distally, or pinnatifid throughout, without conform terminal pinna, 10-30 × 3-12 cm, base truncate, pubescent abaxially.</text>
      <biological_entity id="o259" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly to broadly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="proximally" name="architecture_or_shape" src="d0_s3" value="1-pinnate" value_original="1-pinnate" />
        <character is_modifier="false" modifier="becoming; distally" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="throughout" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" notes="" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" notes="" src="d0_s3" to="12" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="terminal" id="o260" name="pinna" name_original="pinna" src="d0_s3" type="structure" />
      <biological_entity id="o261" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <relation from="o259" id="r49" name="without" negation="false" src="d0_s3" to="o260" />
    </statement>
    <statement id="d0_s4">
      <text>Rachis with indument of spreading, ± gland-tipped hairs abaxially.</text>
      <biological_entity id="o262" name="rachis" name_original="rachis" src="d0_s4" type="structure" />
      <biological_entity id="o263" name="indument" name_original="indument" src="d0_s4" type="structure" />
      <biological_entity id="o264" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s4" value="spreading" value_original="spreading" />
        <character is_modifier="true" modifier="more or less" name="architecture" src="d0_s4" value="gland-tipped" value_original="gland-tipped" />
      </biological_entity>
      <relation from="o262" id="r50" name="with" negation="false" src="d0_s4" to="o263" />
      <relation from="o263" id="r51" name="part_of" negation="false" src="d0_s4" to="o264" />
    </statement>
    <statement id="d0_s5">
      <text>Pinnae not articulate to rachis, proximal pinnae sessile to subsessile, distal pinnae adnate;</text>
      <biological_entity id="o265" name="pinna" name_original="pinnae" src="d0_s5" type="structure">
        <character constraint="to rachis" constraintid="o266" is_modifier="false" modifier="not" name="architecture" src="d0_s5" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o266" name="rachis" name_original="rachis" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o267" name="pinna" name_original="pinnae" src="d0_s5" type="structure">
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s5" to="subsessile" />
      </biological_entity>
      <biological_entity constraint="distal" id="o268" name="pinna" name_original="pinnae" src="d0_s5" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s5" value="adnate" value_original="adnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>larger pinnae curved, lanceolate, 2-7 × 0.5-1.5 cm.</text>
      <biological_entity constraint="larger" id="o269" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character is_modifier="false" name="course" src="d0_s6" value="curved" value_original="curved" />
        <character is_modifier="false" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="7" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s6" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Fertile pinnae slightly contracted;</text>
      <biological_entity id="o270" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s7" value="fertile" value_original="fertile" />
        <character is_modifier="false" modifier="slightly" name="condition_or_size" src="d0_s7" value="contracted" value_original="contracted" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>margins minutely serrulate to nearly entire;</text>
      <biological_entity id="o271" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character char_type="range_value" from="minutely serrulate" name="architecture_or_shape" src="d0_s8" to="nearly entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>costae with indument of hairs abaxially.</text>
      <biological_entity id="o273" name="indument" name_original="indument" src="d0_s9" type="structure" constraint="hair" constraint_original="hair; hair" />
      <biological_entity id="o274" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <relation from="o272" id="r52" name="with" negation="false" src="d0_s9" to="o273" />
      <relation from="o273" id="r53" name="part_of" negation="false" src="d0_s9" to="o274" />
    </statement>
    <statement id="d0_s10">
      <text>2n = 124.</text>
      <biological_entity id="o272" name="costa" name_original="costae" src="d0_s9" type="structure" />
      <biological_entity constraint="2n" id="o275" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="124" value_original="124" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky and clayey places near seasonally dry streams, shady hammocks or open woods, over limestone, soil nearly neutral</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky" constraint="near seasonally dry streams , shady hammocks or open woods" />
        <character name="habitat" value="clayey places" constraint="near seasonally dry streams , shady hammocks or open woods" />
        <character name="habitat" value="dry streams" modifier="seasonally" />
        <character name="habitat" value="shady hammocks" />
        <character name="habitat" value="open woods" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="soil" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="0" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla., Ga., La., Tex.; West Indies; Central America; South America to Bolivia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America to Bolivia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2a</number>
  <other_name type="common_name">Hammock fern</other_name>
  <other_name type="common_name">New World midsorus fern</other_name>
  <discussion>Blechnum occidentale var. minor differs from var. occidentale in having rachises slightly pubescent or puberulous abaxially. Both varieties are found throughout the New World tropics; B. occidentale var. minor grows at higher elevations (D. B. Lellinger 1985). The nomenclature of these taxa is complicated, and other names may apply. Systematic problems involving different ploidy levels and apparent geographic clines in Blechnum occidentale sensu lato remain to be solved.</discussion>
  <discussion>Plants of Blechnum occidentale often reproduce extensively by stolons as well as by spores.</discussion>
  
</bio:treatment>