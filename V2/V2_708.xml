<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Newman" date="1851" rank="genus">gymnocarpium</taxon_name>
    <taxon_name authority="Pryer &amp; Haufler" date="1993" rank="species">appalachianum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot.</publication_title>
      <place_in_publication>18: 161. 1993</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus gymnocarpium;species appalachianum</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500656</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0.5–1.5 mm diam.;</text>
      <biological_entity id="o17230" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s0" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales 1.5–3 mm.</text>
      <biological_entity id="o17231" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s1" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Fertile leaves usually 10–32 cm.</text>
      <biological_entity id="o17232" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s2" to="32" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 6–20 cm, with sparse glandular-hairs distally;</text>
      <biological_entity id="o17233" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o17234" name="glandular-hair" name_original="glandular-hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o17233" id="r3767" name="with" negation="false" src="d0_s3" to="o17234" />
    </statement>
    <statement id="d0_s4">
      <text>scales 2–5 mm.</text>
      <biological_entity id="o17235" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade broadly deltate, 2–3-pinnate-pinnatifid, 4–12 cm, lax and delicate, abaxial surface and rachis glabrous or with occasional glandular-hairs, adaxial surface glabrous.</text>
      <biological_entity id="o17236" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="2-3-pinnate-pinnatifid" value_original="2-3-pinnate-pinnatifid" />
        <character char_type="range_value" from="4" from_unit="cm" name="some_measurement" src="d0_s5" to="12" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="delicate" value_original="delicate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17237" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="with occasional glandular-hairs" value_original="with occasional glandular-hairs" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o17238" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o17239" name="glandular-hair" name_original="glandular-hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="occasional" value_original="occasional" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17240" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o17237" id="r3768" name="with" negation="false" src="d0_s5" to="o17239" />
      <relation from="o17238" id="r3769" name="with" negation="false" src="d0_s5" to="o17239" />
    </statement>
    <statement id="d0_s6">
      <text>Pinna apex entire, rounded.</text>
      <biological_entity constraint="pinna" id="o17241" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Proximal pinnae 3–10 cm, ± perpendicular to rachis, with basiscopic pinnules ± perpendicular to costa;</text>
      <biological_entity constraint="proximal" id="o17242" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s7" to="10" to_unit="cm" />
        <character constraint="to rachis" constraintid="o17243" is_modifier="false" modifier="more or less" name="orientation" src="d0_s7" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o17243" name="rachis" name_original="rachis" src="d0_s7" type="structure" />
      <biological_entity id="o17244" name="pinnule" name_original="pinnules" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="basiscopic" value_original="basiscopic" />
        <character constraint="to costa" constraintid="o17245" is_modifier="false" modifier="more or less" name="orientation" src="d0_s7" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o17245" name="costa" name_original="costa" src="d0_s7" type="structure" />
      <relation from="o17242" id="r3770" name="with" negation="false" src="d0_s7" to="o17244" />
    </statement>
    <statement id="d0_s8">
      <text>basal basiscopic pinnules stalked or sessile, pinnate-pinnatifid or pinnatifid, if sessile then with basal basiscopic pinnulet (division of pinnule) always shorter than adjacent pinnulet;</text>
      <biological_entity constraint="basal" id="o17246" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o17247" name="pinnule" name_original="pinnules" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="stalked" value_original="stalked" />
        <character is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s8" value="pinnate-pinnatifid" value_original="pinnate-pinnatifid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="pinnatifid" value_original="pinnatifid" />
        <character constraint="with basal pinnae" constraintid="o17248" is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17248" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o17249" name="pinnulet" name_original="pinnulet" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character constraint="than adjacent pinnulet" constraintid="o17250" is_modifier="false" name="height_or_length_or_size" src="d0_s8" value="always shorter" value_original="always shorter" />
      </biological_entity>
      <biological_entity id="o17250" name="pinnulet" name_original="pinnulet" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2d basal basiscopic pinnule infrequently stalked, if sessile then with basal basiscopic pinnulet shorter than adjacent pinnulet;</text>
      <biological_entity constraint="basal" id="o17251" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o17252" name="pinnule" name_original="pinnule" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="infrequently" name="architecture" src="d0_s9" value="stalked" value_original="stalked" />
        <character constraint="with basal pinnae" constraintid="o17253" is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17253" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o17254" name="pinnulet" name_original="pinnulet" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character constraint="than adjacent pinnulet" constraintid="o17255" is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17255" name="pinnulet" name_original="pinnulet" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>basal acroscopic pinnule occasionally stalked, if sessile then with basal basiscopic pinnulet shorter than adjacent pinnulet.</text>
      <biological_entity constraint="basal" id="o17256" name="pinna" name_original="pinnae" src="d0_s10" type="structure" />
      <biological_entity id="o17257" name="pinnule" name_original="pinnule" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" modifier="occasionally" name="architecture" src="d0_s10" value="stalked" value_original="stalked" />
        <character constraint="with basal pinnae" constraintid="o17258" is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17258" name="pinna" name_original="pinnae" src="d0_s10" type="structure" />
      <biological_entity id="o17259" name="pinnulet" name_original="pinnulet" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="basiscopic" value_original="basiscopic" />
        <character constraint="than adjacent pinnulet" constraintid="o17260" is_modifier="false" name="height_or_length_or_size" src="d0_s10" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17260" name="pinnulet" name_original="pinnulet" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pinnae of 2d pair usually stalked, if sessile then with basal basiscopic pinnule shorter than adjacent pinnule and equaling basal acroscopic pinnule;</text>
      <biological_entity id="o17261" name="pinna" name_original="pinnae" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s11" value="stalked" value_original="stalked" />
        <character constraint="with basal pinnae" constraintid="o17263" is_modifier="false" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o17262" name="pair" name_original="pair" src="d0_s11" type="structure" />
      <biological_entity constraint="basal" id="o17263" name="pinna" name_original="pinnae" src="d0_s11" type="structure" />
      <biological_entity id="o17264" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s11" value="basiscopic" value_original="basiscopic" />
        <character constraint="than adjacent pinnule and equaling basal acroscopic pinnule" constraintid="o17266" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17266" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o17261" id="r3771" name="part_of" negation="false" src="d0_s11" to="o17262" />
    </statement>
    <statement id="d0_s12">
      <text>basal acroscopic pinnule shorter than adjacent pinnule, often with entire, rounded apex.</text>
      <biological_entity constraint="basal" id="o17267" name="pinna" name_original="pinnae" src="d0_s12" type="structure" />
      <biological_entity id="o17268" name="pinnule" name_original="pinnule" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="acroscopic" value_original="acroscopic" />
        <character constraint="than adjacent pinnule" constraintid="o17269" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17269" name="pinnule" name_original="pinnule" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o17270" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="true" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o17268" id="r3772" modifier="often" name="with" negation="false" src="d0_s12" to="o17270" />
    </statement>
    <statement id="d0_s13">
      <text>Pinnae of 3d pair usually sessile with basal basiscopic pinnule shorter than adjacent pinnule and equaling or shorter than basal acroscopic pinnule;</text>
      <biological_entity id="o17271" name="pinna" name_original="pinnae" src="d0_s13" type="structure">
        <character constraint="with basal pinnae" constraintid="o17273" is_modifier="false" modifier="usually" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o17272" name="pair" name_original="pair" src="d0_s13" type="structure" />
      <biological_entity constraint="basal" id="o17273" name="pinna" name_original="pinnae" src="d0_s13" type="structure" />
      <biological_entity id="o17274" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="basiscopic" value_original="basiscopic" />
        <character constraint="than adjacent pinnule" constraintid="o17275" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="variability" notes="" src="d0_s13" value="equaling" value_original="equaling" />
        <character constraint="than basal acroscopic pinnule" constraintid="o17276" is_modifier="false" name="height_or_length_or_size" src="d0_s13" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17275" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o17276" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o17271" id="r3773" name="part_of" negation="false" src="d0_s13" to="o17272" />
    </statement>
    <statement id="d0_s14">
      <text>basal acroscopic pinnule equaling or shorter than adjacent pinnule.</text>
      <biological_entity constraint="basal" id="o17277" name="pinna" name_original="pinnae" src="d0_s14" type="structure" />
      <biological_entity id="o17278" name="pinnule" name_original="pinnule" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
        <character constraint="than adjacent pinnule" constraintid="o17279" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o17279" name="pinnule" name_original="pinnule" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s14" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Ultimate segments of proximal pinnae oblong, entire to crenate, apex entire, rounded.</text>
      <biological_entity constraint="ultimate" id="o17280" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s15" to="crenate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o17281" name="pinna" name_original="pinnae" src="d0_s15" type="structure" />
      <biological_entity id="o17282" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o17280" id="r3774" name="part_of" negation="false" src="d0_s15" to="o17281" />
    </statement>
    <statement id="d0_s16">
      <text>Spores 27–31 µm. 2n = 80.</text>
      <biological_entity id="o17283" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="27" from_unit="um" name="some_measurement" src="d0_s16" to="31" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17284" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="80" value_original="80" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Maple-birch-hemlock (Acer - Betula - Tsuga) woods on mountain slopes and summits, on moist sandstone or talus slopes with cold air seepage (algific)</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acer" modifier="maple-birch-hemlock" />
        <character name="habitat" value="betula" />
        <character name="habitat" value="tsuga" />
        <character name="habitat" value="( acer" constraint="on mountain slopes and summits" />
        <character name="habitat" value="betula" constraint="on mountain slopes and summits" />
        <character name="habitat" value="tsuga ) woods" constraint="on mountain slopes and summits" />
        <character name="habitat" value="mountain slopes" modifier="on" />
        <character name="habitat" value="summits" />
        <character name="habitat" value="moist sandstone" modifier="on" constraint="with cold air seepage" />
        <character name="habitat" value="talus slopes" constraint="with cold air seepage" />
        <character name="habitat" value="cold air seepage" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Md., N.C., Ohio, Pa., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Appalachian oak fern</other_name>
  <discussion>Gymnocarpium appalachianum, restricted to the Appalachian region, is a very local endemic.</discussion>
  <discussion type="conservation">Of conservation concern.</discussion>
  
</bio:treatment>