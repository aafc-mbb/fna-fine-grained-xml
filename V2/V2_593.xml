<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Frank D. Watson</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">399</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Bartlett" date="unknown" rank="family">Cupressaceae</taxon_name>
    <taxon_hierarchy>family Cupressaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">10237</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees or shrubs evergreen (usually deciduous in Taxodium), generally resinous and aromatic, monoecious (usually dioecious in Juniperus).</text>
      <biological_entity id="o14572" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" modifier="generally" name="coating" src="d0_s0" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="tree" />
        <character is_modifier="false" name="duration" src="d0_s0" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" modifier="generally" name="coating" src="d0_s0" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="odor" src="d0_s0" value="aromatic" value_original="aromatic" />
        <character is_modifier="false" name="reproduction" src="d0_s0" value="monoecious" value_original="monoecious" />
        <character name="growth_form" value="shrub" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark fibrous and furrowed (smooth or exfoliating in plates in some Cupressus and Juniperus species).</text>
      <biological_entity id="o14574" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="furrowed" value_original="furrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Lateral branches well developed, similar to leading shoots, twigs terete, angled, or flattened dorsiventrally (with structurally distinct lower and upper surfaces; Thuja, Calocedrus), densely clothed by scalelike leaves or by decurrent leaf-bases;</text>
      <biological_entity constraint="lateral" id="o14575" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s2" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity constraint="leading" id="o14576" name="shoot" name_original="shoots" src="d0_s2" type="structure" />
      <biological_entity id="o14577" name="twig" name_original="twigs" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s2" value="angled" value_original="angled" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="shape" src="d0_s2" value="angled" value_original="angled" />
        <character is_modifier="false" modifier="dorsiventrally" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
      </biological_entity>
      <biological_entity id="o14578" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="scale-like" value_original="scalelike" />
      </biological_entity>
      <biological_entity id="o14579" name="leaf-base" name_original="leaf-bases" src="d0_s2" type="structure">
        <character is_modifier="true" name="shape" src="d0_s2" value="scale-like" value_original="scalelike" />
        <character is_modifier="true" name="shape" src="d0_s2" value="decurrent" value_original="decurrent" />
      </biological_entity>
      <relation from="o14575" id="r3185" name="to" negation="false" src="d0_s2" to="o14576" />
      <relation from="o14577" id="r3186" modifier="densely" name="clothed" negation="false" src="d0_s2" to="o14578" />
      <relation from="o14577" id="r3187" modifier="densely" name="clothed" negation="false" src="d0_s2" to="o14579" />
    </statement>
    <statement id="d0_s3">
      <text>longest internodes to 1 cm;</text>
      <biological_entity constraint="longest" id="o14580" name="internode" name_original="internodes" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>buds undifferentiated and inconspicuous (except in Sequoia).</text>
      <biological_entity id="o14581" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s4" value="undifferentiated" value_original="undifferentiated" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Roots fibrous to woody (bearing aboveground knees in Taxodium).</text>
      <biological_entity id="o14582" name="root" name_original="roots" src="d0_s5" type="structure">
        <character char_type="range_value" from="fibrous" name="texture" src="d0_s5" to="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves simple, usually persisting 3–5 years and shed with lateral shoots (cladoptosic) (shed annually in Taxodium), alternate and spirally arranged but sometimes twisted so as to appear 2-ranked, or opposite in 4 ranks, or whorled, deltate-scalelike to linear, decurrent, sessile or petioled;</text>
      <biological_entity id="o14583" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="simple" value_original="simple" />
        <character is_modifier="false" modifier="usually" name="duration" src="d0_s6" value="persisting" value_original="persisting" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="5" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s6" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="spirally" name="arrangement" src="d0_s6" value="arranged" value_original="arranged" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s6" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="2-ranked" value_original="2-ranked" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" src="d0_s6" value="2-ranked" value_original="2-ranked" />
        <character constraint="in ranks" constraintid="o14585" is_modifier="false" name="arrangement" src="d0_s6" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement" notes="" src="d0_s6" value="whorled" value_original="whorled" />
        <character char_type="range_value" from="deltate-scalelike" name="shape" src="d0_s6" to="linear" />
        <character is_modifier="false" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="petioled" value_original="petioled" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o14584" name="shoot" name_original="shoots" src="d0_s6" type="structure" />
      <biological_entity id="o14585" name="rank" name_original="ranks" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
      <relation from="o14583" id="r3188" name="shed with" negation="false" src="d0_s6" to="o14584" />
    </statement>
    <statement id="d0_s7">
      <text>adult leaves appressed or spreading, often differing between lateral and leading shoots (twigs heterophyllous), sometimes strongly dimorphic on each twig (Thuja, Calocedrus) with lateral scale-leaf pairs conspicuously keeled;</text>
      <biological_entity id="o14586" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s7" value="adult" value_original="adult" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character constraint="on twig" constraintid="o14588" is_modifier="false" modifier="often; sometimes strongly" name="growth_form" notes="" src="d0_s7" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="lateral and leading" id="o14587" name="shoot" name_original="shoots" src="d0_s7" type="structure" />
      <biological_entity id="o14588" name="twig" name_original="twig" src="d0_s7" type="structure" />
      <biological_entity constraint="lateral" id="o14589" name="scale-leaf" name_original="scale-leaf" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="conspicuously" name="shape" src="d0_s7" value="keeled" value_original="keeled" />
      </biological_entity>
      <relation from="o14586" id="r3189" name="differing between" negation="false" src="d0_s7" to="o14587" />
      <relation from="o14588" id="r3190" name="with" negation="false" src="d0_s7" to="o14589" />
    </statement>
    <statement id="d0_s8">
      <text>juvenile leaves linear, flattened, spreading;</text>
    </statement>
    <statement id="d0_s9">
      <text>often with solitary abaxial resin-gland;</text>
      <biological_entity id="o14590" name="leaf" name_original="leaves" src="d0_s8" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s8" value="juvenile" value_original="juvenile" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s8" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s8" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="orientation" src="d0_s8" value="spreading" value_original="spreading" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14591" name="resin-gland" name_original="resin-gland" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture_or_arrangement_or_growth_form" src="d0_s9" value="solitary" value_original="solitary" />
      </biological_entity>
      <relation from="o14590" id="r3191" name="with" negation="false" src="d0_s9" to="o14591" />
    </statement>
    <statement id="d0_s10">
      <text>resin canal present.</text>
      <biological_entity constraint="resin" id="o14592" name="canal" name_original="canal" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pollen cones maturing and shed annually, solitary, terminal (rarely in clusters of 2–5, axillary in Juniperus communis; usually in terminal panicles in Taxodium), simple, spheric to oblong;</text>
      <biological_entity constraint="pollen" id="o14593" name="cone" name_original="cones" src="d0_s11" type="structure">
        <character is_modifier="false" name="life_cycle" src="d0_s11" value="maturing" value_original="maturing" />
        <character is_modifier="false" modifier="annually" name="architecture_or_arrangement_or_growth_form" src="d0_s11" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s11" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="simple" value_original="simple" />
        <character char_type="range_value" from="spheric" name="shape" src="d0_s11" to="oblong" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>sporophylls overlapping, bearing 2–10 abaxial microsporangia (pollen-sacs);</text>
      <biological_entity id="o14594" name="sporophyll" name_original="sporophylls" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="overlapping" value_original="overlapping" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o14595" name="microsporangium" name_original="microsporangia" src="d0_s12" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s12" to="10" />
      </biological_entity>
      <relation from="o14594" id="r3192" name="bearing" negation="false" src="d0_s12" to="o14595" />
    </statement>
    <statement id="d0_s13">
      <text>pollen spheric, not winged.</text>
      <biological_entity id="o14596" name="pollen" name_original="pollen" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="spheric" value_original="spheric" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s13" value="winged" value_original="winged" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seed-cones maturing in 1–2 seasons, shed with short-shoots or persisting indefinitely on long-lived axes (shattering at maturity in Taxodium), compound, solitary, terminal (rarely in clusters of 2–5, axillary in Juniperus communis);</text>
      <biological_entity id="o14597" name="seed-cone" name_original="seed-cones" src="d0_s14" type="structure">
        <character constraint="in seasons" constraintid="o14598" is_modifier="false" name="life_cycle" src="d0_s14" value="maturing" value_original="maturing" />
        <character constraint="on axes" constraintid="o14600" is_modifier="false" name="duration" src="d0_s14" value="persisting" value_original="persisting" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s14" value="compound" value_original="compound" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s14" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s14" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o14598" name="season" name_original="seasons" src="d0_s14" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s14" to="2" />
      </biological_entity>
      <biological_entity id="o14599" name="short-shoot" name_original="short-shoots" src="d0_s14" type="structure" />
      <biological_entity id="o14600" name="axis" name_original="axes" src="d0_s14" type="structure">
        <character is_modifier="true" name="duration" src="d0_s14" value="long-lived" value_original="long-lived" />
      </biological_entity>
      <relation from="o14597" id="r3193" name="shed with" negation="false" src="d0_s14" to="o14599" />
    </statement>
    <statement id="d0_s15">
      <text>scales overlapping or abutting, fused to subtending bracts with only bract apex sometimes free;</text>
      <biological_entity id="o14602" name="bract" name_original="bracts" src="d0_s15" type="structure" />
      <biological_entity constraint="bract" id="o14603" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="sometimes" name="fusion" src="d0_s15" value="free" value_original="free" />
      </biological_entity>
      <relation from="o14601" id="r3194" name="subtending" negation="false" src="d0_s15" to="o14602" />
      <relation from="o14601" id="r3195" name="with" negation="false" src="d0_s15" to="o14603" />
    </statement>
    <statement id="d0_s16">
      <text>each scale-bract complex peltate, oblong or cuneate, at maturity woody or fleshy, with 1–20 erect (inverted with age in Sequoia and Sequoiadendron), adaxial ovules.</text>
      <biological_entity id="o14601" name="scale" name_original="scales" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s15" value="overlapping" value_original="overlapping" />
        <character is_modifier="false" name="position" src="d0_s15" value="abutting" value_original="abutting" />
        <character is_modifier="false" name="fusion" src="d0_s15" value="fused" value_original="fused" />
      </biological_entity>
      <biological_entity id="o14604" name="scale-bract" name_original="scale-bract" src="d0_s16" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s16" value="complex" value_original="complex" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="peltate" value_original="peltate" />
        <character is_modifier="false" name="shape" src="d0_s16" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="shape" src="d0_s16" value="cuneate" value_original="cuneate" />
        <character is_modifier="false" modifier="at maturity" name="texture" src="d0_s16" value="woody" value_original="woody" />
        <character is_modifier="false" name="texture" src="d0_s16" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14605" name="ovule" name_original="ovules" src="d0_s16" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s16" to="20" />
        <character is_modifier="true" name="orientation" src="d0_s16" value="erect" value_original="erect" />
      </biological_entity>
      <relation from="o14604" id="r3196" name="with" negation="false" src="d0_s16" to="o14605" />
    </statement>
    <statement id="d0_s17">
      <text>Seeds 1–20 per scale, not winged or with 2–3 symmetric or asymmetric wings;</text>
      <biological_entity id="o14606" name="seed" name_original="seeds" src="d0_s17" type="structure">
        <character char_type="range_value" constraint="per scale" constraintid="o14607" from="1" name="quantity" src="d0_s17" to="20" />
        <character constraint="with wings" constraintid="o14608" is_modifier="false" modifier="not" name="architecture" notes="" src="d0_s17" value="winged" value_original="winged" />
        <character is_modifier="false" name="architecture" src="d0_s17" value="with 2-3 symmetric or asymmetric wings" />
      </biological_entity>
      <biological_entity id="o14607" name="scale" name_original="scale" src="d0_s17" type="structure" />
      <biological_entity id="o14608" name="wing" name_original="wings" src="d0_s17" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s17" to="3" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s17" value="symmetric" value_original="symmetric" />
        <character is_modifier="true" name="architecture_or_shape" src="d0_s17" value="asymmetric" value_original="asymmetric" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>aril lacking;</text>
      <biological_entity id="o14609" name="aril" name_original="aril" src="d0_s18" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s18" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s19">
      <text>cotyledons 2–9.</text>
      <biological_entity id="o14610" name="cotyledon" name_original="cotyledons" src="d0_s19" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s19" to="9" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Widespread in temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Widespread in temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <other_name type="common_name">Redwood or Cypress Family</other_name>
  <discussion>Pollination usually occurs in late winter or spring but may occur anytime from late summer to early winter for some species of Juniperus. Seed maturation occurs in late summer or autumn. Species of Cupressus have serotinous cones that remain closed for many years, some opening only after exposure to fire.</discussion>
  <discussion>The Cupressaceae, with a known fossil record extending back to the Jurassic (C. N. Miller Jr. 1988), constitute a diverse family often divided between Cupressaceae in the strict sense (for genera with leaves opposite in four ranks or whorled) and Taxodiaceae (leaves mostly alternate), but they are best kept together (J. E. Eckenwalder 1976; R. A. Price 1989). The unity of the family is best shown in the structure of the mature seed cones: the bract-scale complexes are intimately fused for most of their common length, the 1–20 ovules are erect at first but may invert with maturity, and the paired seed wings, if present, are derived from the seed coat. A majority of genera are monotypic and most others display disjunct or relictual distributions, even though individual species may be widely distributed. Only bird-dispersed Juniperus is species rich, with a wide, nearly continuous Northern Hemisphere distribution. Because of their uniformity, seedlings and juvenile specimens may not be determinable to genus. Foliage of cultivars may deviate greatly from forms found in wild plants.</discussion>
  <discussion>Although no members of the family attain dominance over immense geographic spans as do some species of the Pinaceae in the boreal forests, they can achieve considerable local and regional prominence. Examples include redwood (Sequoia sempervirens) along the coast of northern California, several species of Juniperus (together with pinyons) at moderate elevations in the southwestern United States and Mexico, and baldcypress (Taxodium distichum) in deep swamps of the southeastern United States. Their ranges and regions of dominance were considerably greater during the early Tertiary.</discussion>
  <discussion>The heartwood of many species of Cupressaceae is resistant to termite damage and fungal decay, and therefore it is widely used in contact with soil. Most prominent in the flora are redwood and baldcypress; the premier coffin wood of China, Cunninghamia lanceolata, is another member of the family. Other genera, usually called cedars, may have aromatic woods with a variety of specialty uses. Wooden pencils are made from incense-cedar (Calocedrus decurrens) and eastern redcedar (Juniperus virginiana), which is also used for lining cedar chests. Wood from species of Thuja is still used for cedar roofing shingles.</discussion>
  <discussion>In addition to the taxa treated below (including one naturalized species), several additional species and genera are cultivated to a greater or lesser extent and may persist without spreading after abandonment of cultivation. Some of the more important cultivated species of genera not treated in the flora include: Cryptomeria japonica (Linnaeus f.) D. Don (Japanese-cedar), differing from Sequoiadendron in its smaller, globose cones with bract/scale complexes bearing five to eight teeth; Cunninghamia lanceolata (Lambert) Hooker (China-fir), unlike all North American native taxa in its pointed, flat, lanceolate, drooping leaves to 7 cm; Metasequoia glyptostroboides Hu &amp; W. C. Cheng (dawn-redwood), differing from Sequoia in its opposite leaves and deciduous branchlets and from Taxodium in its opposite leaves and persistent seed-cone scales; Microbiota decussata Komarov (microbiota), differing from spreading junipers in its minute, opening, one-seeded cone; Platycladus orientalis (Linnaeus) Franco [Thuja orientalis Linnaeus; Biota orientalis (Linnaeus) Endlicher; oriental arborvitae], which may escape locally from cultivation, differing from Thuja in its vertical sprays of branchlets, thicker, fleshier bract/scale complexes with prominent hornlike umbos, and unwinged seeds; and Thujopsis dolabrata (Linnaeus f.) Siebold &amp; Zuccarini (hiba arborvitae), differing from Thuja in its much broader lateral leaf pairs and its sprays of branchlets with prominent white waxy markings beneath.</discussion>
  <discussion>Genera 25–30, species 110–130 (9 genera, 30 species in the flora).</discussion>
  <references>
    <reference>Burns, R. M. and B. H. Honkala. 1990. Silvics of North America. 1. Conifers. Washington. [Agric. Handb. 654.]</reference>
    <reference>Canadian Forestry Service. 1983. Reproduction of Conifers. Forest. Techn. Pub. Canad. Forest. Serv. 31.</reference>
    <reference>Farjon, A. 1990. A Bibliography of Conifers. Königstein. [Regnum Veg. 122.]</reference>
    <reference>Krajina, V. J., K. Klinka, and J. Worrall. 1982. Distribution and Ecological Characteristics of Trees and Shrubs of British Columbia. Vancouver.</reference>
    <reference>Eckenwalder, J. E. 1976. Re-evaluation of Cupressaceae and Taxodiaceae: A proposed merger. Madroño 23: 237–256.</reference>
    <reference>Hosie, R. C. 1969. Native Trees of Canada, ed. 7. Ottawa. Pp. 83–95.</reference>
    <reference>Little, E. L. Jr. 1979. Checklist of United States Trees (Native and Naturalized). Washington. Pp. 33–36. [Agric. Handb. 541.]</reference>
    <reference>Rehder, A. J. 1949. Bibliography of Cultivated Trees and Shrubs Hardy in the Cooler Temperate Regions of the Northern Hemisphere. Jamaica Plain.</reference>
    <reference>Silba, J. 1986. Encyclopaedia Coniferae. Phytologia Mem. 8: 1–127.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves alternate; leaves of adult usually with expanded needlelike or linear or linear-lanceolate blade.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves opposite in 4 ranks or whorled; leaves of adults usually ± scalelike to subulate.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leafy branchlets falling annually or seasonally; pollen cones mostly in pendent axillary panicles; seed cones nearly globose, shattering at maturity, scales each bearing (1-)2 irregularly 3-angled, wingless seeds; se United States.</description>
      <determination>3 Taxodium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leafy branchlets persisting for several years; pollen cones mostly terminal and solitary; seed cones oblong or globose, opening but scales persistent at maturity, scales each bearing 2-9 lenticular, winged seeds; w United States.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Branchlets with leaves mostly in 2 ranks, with obvious annual growth constrictions; leaves linear or linear-lanceolate to deltate, flattened, free portion to ca. 30 mm; mature seed cones 1.3-3.5 cm.</description>
      <determination>1 Sequoia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Branchlets with radiating leaves, without obvious annual growth constrictions; leaves mostly needlelike, triangular in cross section, free portion to ca. 15 mm; mature seed cones 4-9 cm.</description>
      <determination>2 Sequoiadendron</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Seed cones berrylike, remaining closed, seeds retained; scales generally fleshy or fibrous; monoecious or dioecious.</description>
      <determination>8 Juniperus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Seed cones opening, seeds shed; scales woody; monoecious.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves in whorls of 3.</description>
      <determination>9 Callitris</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves opposite in 4 ranks or seemingly in whorls of 4.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Branchlets in radial arrays (partially comblike in C. macnabiana); seed cones globose or oblong with peltate scales, at least 10 mm diam.</description>
      <determination>4 Cupressus</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Branchlets in flattened sprays; seed cones ellipsoid with oblong, basifixed scales or globose with rounded, peltate or basifixed scales, less than 12 mm diam.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Branchlets teret or rhombic in cross section, facial and lateral leaves similar; seed cones globose, their scales usually peltate (basifixed in C. nootkatensis)</description>
      <determination>5 Chamaecyparis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Branchlets flattened, facial and lateral leaves clearly differentiated; seed cones ellipsoid, their scales basifixed.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Scales of seed cones 4-6 pairs; seed wings equal; leaves clearly opposite in 4 ranks.</description>
      <determination>6 Thuja</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Scales of seed cones 2-3 pairs; seed wings markedly unequal; leaves seemingly in whorls of 4.</description>
      <determination>7 Calocedrus</determination>
    </key_statement>
  </key>
</bio:treatment>