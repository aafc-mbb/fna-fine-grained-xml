<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">pinus</taxon_name>
    <taxon_name authority="Parlatore ex Sudworth" date="1897" rank="species">quadrifolia</taxon_name>
    <place_of_publication>
      <publication_title>U.S.D.A. Div. Forest. Bull.</publication_title>
      <place_in_publication>14: 17. 1897</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus pinus;species quadrifolia</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500950</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pinus</taxon_name>
    <taxon_name authority="Zuccarini" date="unknown" rank="species">cembroides</taxon_name>
    <taxon_name authority="Voss" date="unknown" rank="variety">parryana</taxon_name>
    <taxon_hierarchy>genus Pinus;species cembroides;variety parryana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pinus</taxon_name>
    <taxon_name authority="Lanner" date="unknown" rank="species">juarezensis</taxon_name>
    <taxon_hierarchy>genus Pinus;species juarezensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pinus</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">parryana</taxon_name>
    <place_of_publication>
      <place_in_publication>1862,</place_in_publication>
      <other_info_on_pub>not Gordon 1858</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus Pinus;species parryana;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 10m;</text>
      <biological_entity id="o9297" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="10" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 0.5m diam., straight, much branched;</text>
      <biological_entity id="o9298" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="0.5" to_unit="m" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="much" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>crown dense, becoming rounded.</text>
      <biological_entity id="o9299" name="crown" name_original="crown" src="d0_s2" type="structure">
        <character is_modifier="false" name="density" src="d0_s2" value="dense" value_original="dense" />
        <character is_modifier="false" modifier="becoming" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark redbrown, irregularly furrowed and cross-checked to irregularly rectangular, plates scaly.</text>
      <biological_entity id="o9300" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="irregularly" name="architecture" src="d0_s3" value="furrowed" value_original="furrowed" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s3" value="rectangular" value_original="rectangular" />
      </biological_entity>
      <biological_entity id="o9301" name="plate" name_original="plates" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Branches spreading to ascending, persistent to trunk base;</text>
      <biological_entity id="o9302" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s4" to="ascending" />
        <character constraint="to trunk base" constraintid="o9303" is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="trunk" id="o9303" name="base" name_original="base" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>twigs slender, pale orangebrown, puberulent-glandular, aging brown to gray-brown.</text>
      <biological_entity id="o9304" name="twig" name_original="twigs" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="slender" value_original="slender" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale orangebrown" value_original="pale orangebrown" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s5" value="puberulent-glandular" value_original="puberulent-glandular" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="aging" value_original="aging" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s5" to="gray-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Buds ovoid, light-redbrown, ca. 0.4–0.5cm, slightly resinous.</text>
      <biological_entity id="o9305" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="light-redbrown" value_original="light-redbrown" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s6" to="0.5" to_unit="cm" />
        <character is_modifier="false" modifier="slightly" name="coating" src="d0_s6" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves (3–) 4 (–5) per fascicle, persisting 3–4 years, (2–) 3–6cm × (1–) 1.2–1.7mm, curved, connivent, stiff, green to blue-green, margins entire to minutely scaly-denticulate, finely serrulate, apex subulate, adaxial surfaces mostly strongly whitened with stomatal bands, abaxial surface not so but 2 subepidermal resin bands evident;</text>
      <biological_entity id="o9306" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="3" name="atypical_quantity" src="d0_s7" to="4" to_inclusive="false" />
        <character char_type="range_value" from="4" from_inclusive="false" name="atypical_quantity" src="d0_s7" to="5" />
        <character constraint="per fascicle" constraintid="o9307" name="quantity" src="d0_s7" value="4" value_original="4" />
        <character is_modifier="false" name="duration" notes="" src="d0_s7" value="persisting" value_original="persisting" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="2" from_unit="cm" name="atypical_length" src="d0_s7" to="3" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s7" to="6" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_width" src="d0_s7" to="1.2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s7" to="1.7" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s7" value="curved" value_original="curved" />
        <character is_modifier="false" name="arrangement" src="d0_s7" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="fragility" src="d0_s7" value="stiff" value_original="stiff" />
        <character char_type="range_value" from="green" name="coloration" src="d0_s7" to="blue-green" />
      </biological_entity>
      <biological_entity id="o9307" name="fascicle" name_original="fascicle" src="d0_s7" type="structure" />
      <biological_entity id="o9308" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="entire" name="shape" src="d0_s7" to="minutely scaly-denticulate" />
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o9309" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="subulate" value_original="subulate" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o9310" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character constraint="with stomatal, bands" constraintid="o9311, o9312" is_modifier="false" modifier="mostly strongly" name="coloration" src="d0_s7" value="whitened" value_original="whitened" />
      </biological_entity>
      <biological_entity id="o9311" name="stomatal" name_original="stomatal" src="d0_s7" type="structure" />
      <biological_entity id="o9312" name="band" name_original="bands" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o9313" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="resin" id="o9314" name="band" name_original="bands" src="d0_s7" type="structure" constraint_original="subepidermal resin">
        <character is_modifier="false" name="prominence" src="d0_s7" value="evident" value_original="evident" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>sheath 0.5–0.6cm, scales soon recurved, forming rosette, shed early.</text>
      <biological_entity id="o9315" name="sheath" name_original="sheath" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s8" to="0.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9316" name="scale" name_original="scales" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="soon" name="orientation" src="d0_s8" value="recurved" value_original="recurved" />
      </biological_entity>
      <biological_entity id="o9317" name="rosette" name_original="rosette" src="d0_s8" type="structure" />
      <relation from="o9316" id="r1983" name="forming" negation="false" src="d0_s8" to="o9317" />
    </statement>
    <statement id="d0_s9">
      <text>Pollen cones ovoid, ca. 10mm, yellowish.</text>
      <biological_entity constraint="pollen" id="o9318" name="cone" name_original="cones" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character name="some_measurement" src="d0_s9" unit="mm" value="10" value_original="10" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellowish" value_original="yellowish" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seed-cones maturing in 2 years, shedding seeds and falling soon thereafter, spreading, symmetric, ovoid before opening, broadly ovoid to depressed-globose when open, (3–) 4–8 (–10) cm, pale yellowbrown, sessile to short-stalked, apophyses thickened, strongly raised, diamond-shaped, transversely keeled, umbo subcentral, low-pyramidal or sunken, blunt.</text>
      <biological_entity id="o9319" name="cone-seed" name_original="seed-cones" src="d0_s10" type="structure">
        <character constraint="in years" constraintid="o9320" is_modifier="false" name="life_cycle" src="d0_s10" value="maturing" value_original="maturing" />
        <character is_modifier="false" modifier="soon thereafter; thereafter" name="life_cycle" src="d0_s10" value="falling" value_original="falling" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character constraint="before opening" constraintid="o9322" is_modifier="false" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="broadly ovoid" modifier="when open" name="shape" notes="" src="d0_s10" to="depressed-globose" />
        <character char_type="range_value" from="3" from_unit="cm" name="atypical_distance" src="d0_s10" to="4" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="8" from_inclusive="false" from_unit="cm" name="atypical_distance" src="d0_s10" to="10" to_unit="cm" />
        <character char_type="range_value" from="4" from_unit="cm" name="distance" src="d0_s10" to="8" to_unit="cm" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="pale yellowbrown" value_original="pale yellowbrown" />
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s10" to="short-stalked" />
      </biological_entity>
      <biological_entity id="o9320" name="year" name_original="years" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o9321" name="seed" name_original="seeds" src="d0_s10" type="structure" />
      <biological_entity id="o9322" name="opening" name_original="opening" src="d0_s10" type="structure" />
      <biological_entity id="o9323" name="apophysis" name_original="apophyses" src="d0_s10" type="structure">
        <character is_modifier="false" name="size_or_width" src="d0_s10" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="strongly" name="prominence" src="d0_s10" value="raised" value_original="raised" />
        <character is_modifier="false" name="shape" src="d0_s10" value="diamond--shaped" value_original="diamond--shaped" />
        <character is_modifier="false" modifier="transversely" name="shape" src="d0_s10" value="keeled" value_original="keeled" />
      </biological_entity>
      <biological_entity id="o9324" name="umbo" name_original="umbo" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="subcentral" value_original="subcentral" />
      </biological_entity>
      <biological_entity id="o9325" name="whole-organism" name_original="" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="blunt" value_original="blunt" />
      </biological_entity>
      <relation from="o9319" id="r1984" name="shedding" negation="false" src="d0_s10" to="o9321" />
    </statement>
    <statement id="d0_s11">
      <text>Seeds obovoid, body ca. 15mm, brown, wingless.</text>
      <biological_entity id="o9326" name="seed" name_original="seeds" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obovoid" value_original="obovoid" />
      </biological_entity>
      <biological_entity id="o9327" name="body" name_original="body" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="15" value_original="15" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="brown" value_original="brown" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="wingless" value_original="wingless" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry rocky sites</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky sites" modifier="dry" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1200–1800m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="1200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico in Baja California.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico in Baja California" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>9</number>
  <other_name type="common_name">Parry pinyon</other_name>
  <other_name type="common_name">piñón</other_name>
  <discussion>Pinus quadrifolia is the rarest pinyon in the flora. It hybridizes naturally with P. monophylla.</discussion>
  
</bio:treatment>