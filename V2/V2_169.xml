<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Agardh" date="unknown" rank="family">ophioglossaceae</taxon_name>
    <taxon_name authority="Swartz" date="1801" rank="genus">botrychium</taxon_name>
    <taxon_name authority="(J. Milde) R. T. Clausen" date="1938" rank="subgenus">Osmundopteris</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Torrey Bot. Club</publication_title>
      <place_in_publication>19(2): 93. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ophioglossaceae;genus botrychium;subgenus Osmundopteris</taxon_hierarchy>
    <other_info_on_name type="fna_id">302118</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Botrychium</taxon_name>
    <taxon_name authority="J.Milde" date="unknown" rank="section">Osmundopteris</taxon_name>
    <place_of_publication>
      <publication_title>Verh. Zool.- Bot. Ges. Wien</publication_title>
      <place_in_publication>19(2): 96. 1869</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Botrychium;section Osmundopteris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots 15 or fewer, yellow to brown, 0.5–2 mm diam. 1cm from base.</text>
      <biological_entity id="o8398" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s0" value="count" value_original="count" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="list" value_original="list" />
        <character name="quantity" src="d0_s0" unit="or fewerpunct yellow to brown" value="15" value_original="15" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s0" to="2" to_unit="mm" />
        <character constraint="from base" constraintid="o8399" name="location" src="d0_s0" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o8399" name="base" name_original="base" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Plants over 12 cm.</text>
      <biological_entity id="o8400" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Common stalk lacking idioblasts (elongate, spindle-shaped cells in vascular strand).</text>
      <biological_entity id="o8401" name="stalk" name_original="stalk" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="common" value_original="common" />
      </biological_entity>
      <biological_entity id="o8402" name="idioblast" name_original="idioblasts" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Trophophore sessile and erect, arising from middle to distal portion of common stalk well above ground level;</text>
      <biological_entity id="o8403" name="trophophore" name_original="trophophore" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character constraint="from portion" constraintid="o8404" is_modifier="false" name="orientation" src="d0_s3" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o8404" name="portion" name_original="portion" src="d0_s3" type="structure" />
      <biological_entity id="o8405" name="stalk" name_original="stalk" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="common" value_original="common" />
      </biological_entity>
      <biological_entity id="o8406" name="level" name_original="level" src="d0_s3" type="structure" />
      <relation from="o8404" id="r1774" name="part_of" negation="false" src="d0_s3" to="o8405" />
      <relation from="o8404" id="r1775" name="above" negation="false" src="d0_s3" to="o8406" />
    </statement>
    <statement id="d0_s4">
      <text>blade usually 1 per plant, seasonal, absent during winter, deltate, 3–4-pinnate, 5–25 cm wide when mature, thin, herbaceous.</text>
      <biological_entity id="o8407" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character constraint="per plant" constraintid="o8408" name="quantity" src="d0_s4" value="1" value_original="1" />
        <character is_modifier="false" name="duration" notes="" src="d0_s4" value="seasonal" value_original="seasonal" />
        <character constraint="during winter" is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
        <character is_modifier="false" name="shape" src="d0_s4" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="3-4-pinnate" value_original="3-4-pinnate" />
        <character char_type="range_value" from="5" from_unit="cm" modifier="when mature" name="width" src="d0_s4" to="25" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s4" value="thin" value_original="thin" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s4" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o8408" name="plant" name_original="plant" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Leaf primordia densely hairy, hairs 1.5–2mm.</text>
      <biological_entity constraint="leaf" id="o8409" name="primordium" name_original="primordia" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s5" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity id="o8410" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaf-sheath open.</text>
      <biological_entity id="o8411" name="sheath" name_original="leaf-sheath" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pinna divisions reduced in size to tip.</text>
      <biological_entity id="o8413" name="tip" name_original="tip" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>First pinnule on basal pinnae usually borne acroscopically.</text>
      <biological_entity constraint="pinna" id="o8412" name="division" name_original="divisions" src="d0_s7" type="structure">
        <character constraint="to tip" constraintid="o8413" is_modifier="false" name="size" src="d0_s7" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o8414" name="pinnule" name_original="pinnule" src="d0_s8" type="structure" />
      <biological_entity constraint="basal" id="o8415" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <relation from="o8414" id="r1776" modifier="acroscopically" name="on" negation="false" src="d0_s8" to="o8415" />
    </statement>
    <statement id="d0_s9">
      <text>Sporophores long-stalked, arising from middle to distal portion of common stalk well above ground level, commonly absent due to abortion in early development or seasonal, stalks only slightly flattened, not fleshy, 0.5–0.8mm wide.</text>
      <biological_entity id="o8417" name="portion" name_original="portion" src="d0_s9" type="structure" />
      <biological_entity id="o8418" name="stalk" name_original="stalk" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="common" value_original="common" />
      </biological_entity>
      <biological_entity id="o8419" name="ground" name_original="ground" src="d0_s9" type="structure">
        <character is_modifier="false" name="position_relational" src="d0_s9" value="level" value_original="level" />
      </biological_entity>
      <biological_entity id="o8420" name="stalk" name_original="stalks" src="d0_s9" type="structure">
        <character is_modifier="true" name="character" src="d0_s9" value="development" value_original="development" />
        <character is_modifier="true" name="duration" src="d0_s9" value="seasonal" value_original="seasonal" />
        <character is_modifier="false" modifier="only slightly" name="shape" src="d0_s9" value="flattened" value_original="flattened" />
      </biological_entity>
      <relation from="o8417" id="r1777" name="part_of" negation="false" src="d0_s9" to="o8418" />
      <relation from="o8417" id="r1778" name="above" negation="false" src="d0_s9" to="o8419" />
    </statement>
    <statement id="d0_s10">
      <text>x =92.</text>
      <biological_entity id="o8416" name="sporophore" name_original="sporophores" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="long-stalked" value_original="long-stalked" />
        <character constraint="from portion" constraintid="o8417" is_modifier="false" name="orientation" src="d0_s9" value="arising" value_original="arising" />
        <character constraint="in " constraintid="o8420" is_modifier="false" modifier="commonly" name="presence" notes="" src="d0_s9" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="not" name="texture" notes="" src="d0_s9" value="fleshy" value_original="fleshy" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="0.8" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="x" id="o8421" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="92" value_original="92" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a</number>
  <discussion>Species 2–3 (1 in the flora).</discussion>
  
</bio:treatment>