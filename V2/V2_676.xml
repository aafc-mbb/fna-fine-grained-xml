<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan R. Smith</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ching ex Pichi Sermolli" date="unknown" rank="family">thelypteridaceae</taxon_name>
    <taxon_name authority="(H. Itô) Ching" date="1963" rank="genus">Macrothelypteris</taxon_name>
    <place_of_publication>
      <publication_title>Acta Phytotax. Sin.</publication_title>
      <place_in_publication>8: 308. 1963</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family thelypteridaceae;genus Macrothelypteris</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek makros, large, thelys, female, and pteris, fern</other_info_on_name>
    <other_info_on_name type="fna_id">119411</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Schmidel" date="unknown" rank="genus">Thelypteris</taxon_name>
    <taxon_name authority="H. Itô" date="unknown" rank="section">Macrothelypteris</taxon_name>
    <place_of_publication>
      <publication_title>in Nakai &amp; Honda, Nov. Fl. Jap.</publication_title>
      <place_in_publication>4: 141. 1938</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Thelypteris;section Macrothelypteris;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Schmidel" date="unknown" rank="genus">Thelypteris</taxon_name>
    <taxon_name authority="(H. Itô)." date="unknown" rank="subgenus">Macrothelypteris</taxon_name>
    <taxon_hierarchy>genus Thelypteris;subgenus Macrothelypteris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems short-creeping, thick, 1 cm diam.</text>
      <biological_entity id="o14723" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="short-creeping" value_original="short-creeping" />
        <character is_modifier="false" name="width" src="d0_s0" value="thick" value_original="thick" />
        <character name="diameter" src="d0_s0" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Blades 2-pinnate-pinnatifid nearly throughout, broadest at base, apex gradually reduced;</text>
      <biological_entity id="o14724" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="nearly throughout; throughout" name="shape" src="d0_s1" value="2-pinnate-pinnatifid" value_original="2-pinnate-pinnatifid" />
        <character constraint="at base" constraintid="o14725" is_modifier="false" name="width" src="d0_s1" value="broadest" value_original="broadest" />
      </biological_entity>
      <biological_entity id="o14725" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o14726" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s1" value="reduced" value_original="reduced" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>pinnae pinnate-pinnatifid, sessile or stalked, not connected by wing along rachis;</text>
      <biological_entity id="o14727" name="pinna" name_original="pinnae" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="pinnate-pinnatifid" value_original="pinnate-pinnatifid" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="stalked" value_original="stalked" />
        <character constraint="by wing" constraintid="o14728" is_modifier="false" modifier="not" name="fusion" src="d0_s2" value="connected" value_original="connected" />
      </biological_entity>
      <biological_entity id="o14728" name="wing" name_original="wing" src="d0_s2" type="structure" />
      <biological_entity id="o14729" name="rachis" name_original="rachis" src="d0_s2" type="structure" />
      <relation from="o14728" id="r3229" name="along" negation="false" src="d0_s2" to="o14729" />
    </statement>
    <statement id="d0_s3">
      <text>costae not grooved adaxially;</text>
      <biological_entity id="o14730" name="costa" name_original="costae" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not; adaxially" name="architecture" src="d0_s3" value="grooved" value_original="grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>buds absent;</text>
      <biological_entity id="o14731" name="bud" name_original="buds" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>veins free, often forked, tips not reaching margin;</text>
      <biological_entity id="o14732" name="vein" name_original="veins" src="d0_s5" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s5" value="free" value_original="free" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity id="o14733" name="tip" name_original="tips" src="d0_s5" type="structure" />
      <biological_entity id="o14734" name="margin" name_original="margin" src="d0_s5" type="structure" />
      <relation from="o14733" id="r3230" name="reaching" negation="true" src="d0_s5" to="o14734" />
    </statement>
    <statement id="d0_s6">
      <text>rachises and costae lacking scales;</text>
      <biological_entity id="o14735" name="rachis" name_original="rachises" src="d0_s6" type="structure" />
      <biological_entity id="o14736" name="costa" name_original="costae" src="d0_s6" type="structure" />
      <biological_entity id="o14737" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>indument abaxially of unbranched, septate hairs mostly over 1 mm.</text>
      <biological_entity id="o14738" name="indument" name_original="indument" src="d0_s7" type="structure" constraint="hair" constraint_original="hair; hair" />
      <biological_entity id="o14739" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s7" value="unbranched" value_original="unbranched" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="septate" value_original="septate" />
      </biological_entity>
      <relation from="o14738" id="r3231" name="part_of" negation="false" src="d0_s7" to="o14739" />
    </statement>
    <statement id="d0_s8">
      <text>Sori round, medial to supramedial;</text>
      <biological_entity id="o14740" name="sorus" name_original="sori" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="round" value_original="round" />
        <character char_type="range_value" from="medial" name="position" src="d0_s8" to="supramedial" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>indusia small, less than 0.3 mm diam., often obscured in mature sori;</text>
      <biological_entity id="o14741" name="indusium" name_original="indusia" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="small" value_original="small" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s9" to="0.3" to_unit="mm" />
        <character constraint="in sori" constraintid="o14742" is_modifier="false" modifier="often" name="prominence" src="d0_s9" value="obscured" value_original="obscured" />
      </biological_entity>
      <biological_entity id="o14742" name="sorus" name_original="sori" src="d0_s9" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s9" value="mature" value_original="mature" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>sporangial capsules bearing short-stalked glands.</text>
      <biological_entity id="o14744" name="gland" name_original="glands" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="short-stalked" value_original="short-stalked" />
      </biological_entity>
      <relation from="o14743" id="r3232" name="bearing" negation="false" src="d0_s10" to="o14744" />
    </statement>
    <statement id="d0_s11">
      <text>x = 31.</text>
      <biological_entity constraint="sporangial" id="o14743" name="capsule" name_original="capsules" src="d0_s10" type="structure" />
      <biological_entity constraint="x" id="o14745" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="31" value_original="31" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical and subtropical regions, North America, Asia, Africa, Pacific Islands, Australia in Queensland.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical and subtropical regions" establishment_means="native" />
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
        <character name="distribution" value="Africa" establishment_means="native" />
        <character name="distribution" value="Pacific Islands" establishment_means="native" />
        <character name="distribution" value="Australia in Queensland" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <discussion>Species ca. 10 (1 introduced in the flora).</discussion>
  <references>
    <reference>Holttum, R. E. 1969. Studies in the family Thelypteridaceae. The genera Phegopteris,  Pseudophegopteris, and Macrothelypteris. Blumea 17: 5–32.</reference>
    <reference>Leonard, S. W. 1972. The distribution of Thelypteris torresiana in the southeastern United States. Amer. Fern J. 62: 97–99.</reference>
  </references>
  
</bio:treatment>