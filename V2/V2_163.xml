<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="D. M. Benham &amp; Windham" date="1992" rank="genus">astrolepis</taxon_name>
    <taxon_name authority="(Hooker) D. M. Benham &amp; Windham" date="1992" rank="species">integerrima</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>82: 57. 1992</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus astrolepis;species integerrima</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500211</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Notholaena</taxon_name>
    <taxon_name authority="(Lagasca ex Swartz) Kaulfuss" date="unknown" rank="species">sinuata</taxon_name>
    <taxon_name authority="Hooker" date="unknown" rank="variety">integerrima</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Fil.</publication_title>
      <place_in_publication>5: 108. 1864</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Notholaena;species sinuata;variety integerrima;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cheilanthes</taxon_name>
    <taxon_name authority="(Hooker) Mickel" date="unknown" rank="species">integerrima</taxon_name>
    <taxon_hierarchy>genus Cheilanthes;species integerrima;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Notholaena</taxon_name>
    <taxon_name authority="(Hooker) Hevly" date="unknown" rank="species">integerrima</taxon_name>
    <taxon_hierarchy>genus Notholaena;species integerrima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems compact;</text>
      <biological_entity id="o15292" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>stem scales uniformly tan or somewhat darker near base, to 15 mm, margins ciliate-dentate to entire.</text>
      <biological_entity constraint="stem" id="o15293" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="uniformly" name="coloration" src="d0_s1" value="tan" value_original="tan" />
        <character constraint="near base" constraintid="o15294" is_modifier="false" modifier="somewhat; somewhat" name="coloration" src="d0_s1" value="darker" value_original="darker" />
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" notes="" src="d0_s1" to="15" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o15294" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o15295" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character char_type="range_value" from="ciliate-dentate" name="architecture_or_shape" src="d0_s1" to="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 8–45 cm.</text>
      <biological_entity id="o15296" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s2" to="45" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Blade 1-pinnate to pinnate-pinnatifid, pinna pairs 20–45.</text>
      <biological_entity id="o15297" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="1-pinnate" name="shape" src="d0_s3" to="pinnate-pinnatifid" />
      </biological_entity>
      <biological_entity id="o15298" name="pinna" name_original="pinna" src="d0_s3" type="structure">
        <character char_type="range_value" from="20" name="quantity" src="d0_s3" to="45" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pinnae oblong to ovate, largest usually 7–15 mm, entire or asymmetrically lobed, lobes 2–7, broadly rounded, separated by shallow sinuses;</text>
      <biological_entity id="o15299" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" modifier="usually" name="size" src="d0_s4" value="largest" value_original="largest" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s4" to="15" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s4" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="asymmetrically" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o15300" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s4" to="7" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character constraint="by sinuses" constraintid="o15301" is_modifier="false" name="arrangement" src="d0_s4" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o15301" name="sinuse" name_original="sinuses" src="d0_s4" type="structure">
        <character is_modifier="true" name="depth" src="d0_s4" value="shallow" value_original="shallow" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>abaxial scales concealing surface, lanceolate, usually 1–1.5 mm, ciliate with coarse marginal projections;</text>
      <biological_entity constraint="abaxial" id="o15302" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1" from_unit="mm" modifier="usually" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
        <character constraint="with marginal projections" constraintid="o15304" is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o15303" name="surface" name_original="surface" src="d0_s5" type="structure" />
      <biological_entity constraint="marginal" id="o15304" name="projection" name_original="projections" src="d0_s5" type="structure">
        <character is_modifier="true" name="relief" src="d0_s5" value="coarse" value_original="coarse" />
      </biological_entity>
      <relation from="o15302" id="r3339" name="concealing" negation="false" src="d0_s5" to="o15303" />
    </statement>
    <statement id="d0_s6">
      <text>adaxial scales abundant, mostly persistent, stellate to coarsely ciliate, elongate, attached at base, body mostly 5–7 cells wide.</text>
      <biological_entity constraint="adaxial" id="o15305" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s6" value="abundant" value_original="abundant" />
        <character is_modifier="false" modifier="mostly" name="duration" src="d0_s6" value="persistent" value_original="persistent" />
        <character char_type="range_value" from="stellate" name="shape" src="d0_s6" to="coarsely ciliate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
        <character constraint="at base" constraintid="o15306" is_modifier="false" name="fixation" src="d0_s6" value="attached" value_original="attached" />
      </biological_entity>
      <biological_entity id="o15306" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o15307" name="body" name_original="body" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s6" to="7" />
      </biological_entity>
      <biological_entity id="o15308" name="cell" name_original="cells" src="d0_s6" type="structure">
        <character is_modifier="false" name="width" src="d0_s6" value="wide" value_original="wide" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sporangia containing 32 spores.</text>
      <biological_entity id="o15310" name="spore" name_original="spores" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="32" value_original="32" />
      </biological_entity>
      <relation from="o15309" id="r3340" name="containing" negation="false" src="d0_s7" to="o15310" />
    </statement>
    <statement id="d0_s8">
      <text>n = 2n = 87, apogamous.</text>
      <biological_entity id="o15309" name="sporangium" name_original="sporangia" src="d0_s7" type="structure" />
      <biological_entity constraint="n=2n" id="o15311" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" unit=",apogamous" value="87" value_original="87" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky hillsides and cliffs, usually on limestone or other calcareous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky hillsides" />
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="other calcareous" />
        <character name="habitat" value="substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1800 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Nev., N.Mex., Okla., Tex.; n,c Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="c Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <discussion>R. H. Hevly (1965) hypothesized that Astrolepis integerrima was produced by hybridization between A. cochisensis and A. sinuata. Recent isozyme analyses (D. M. Benham 1989) indicate, however, that Astrolepis integerrima is an apogamous allotriploid hybrid between A. cochisensis and an unnamed Mexican taxon related to A. crassifolia (Houlston &amp; T. Moore) D. M. Benham &amp; Windham. Two morphologic forms exist in this taxon: one with essentially entire pinnae, and one (more common in the United States) with larger, asymmetrically lobed pinnae. The former might be confused with A. cochisensis on occasion, but the abundance of adaxial scales and the larger pinnae of A. integerrima should serve to distinguish these species. The lobed form of A. integerrima is superficially similar to A. windhamii, from which it is distinguished by the abundance and greater width of adaxial scales and the asymmetrical lobing of the pinnae.</discussion>
  
</bio:treatment>