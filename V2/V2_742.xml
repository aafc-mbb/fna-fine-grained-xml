<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Clifton E. Nauman, A. Murray Evans</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ching" date="unknown" rank="family">dennstaedtiaceae</taxon_name>
    <taxon_name authority="Bernhardi" date="1776" rank="genus">Dennstaedtia</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Schrader)</publication_title>
      <place_in_publication>1800(2): 124. 1802 [Named after A. W. Dennstaedt, 1776</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dennstaedtiaceae;genus Dennstaedtia</taxon_hierarchy>
    <other_info_on_name type="etymology">Named after A. W. Dennstaedt, 1826, German botanist</other_info_on_name>
    <other_info_on_name type="fna_id">109626</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial, often forming colonies.</text>
      <biological_entity id="o8016" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o8017" name="colony" name_original="colonies" src="d0_s0" type="structure" />
      <relation from="o8016" id="r1681" modifier="often" name="forming" negation="false" src="d0_s0" to="o8017" />
    </statement>
    <statement id="d0_s1">
      <text>Stems subterranean, long to short-creeping;</text>
      <biological_entity id="o8018" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="location" src="d0_s1" value="subterranean" value_original="subterranean" />
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="short-creeping" value_original="short-creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>hairs dark reddish-brown, jointed.</text>
      <biological_entity id="o8019" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="jointed" value_original="jointed" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves clustered or scattered, erect to arching, ovate to lanceolate to deltate, 0.4–3 m.</text>
      <biological_entity id="o8020" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="arrangement" src="d0_s3" value="scattered" value_original="scattered" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s3" to="arching" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="lanceolate" />
        <character char_type="range_value" from="0.4" from_unit="m" name="some_measurement" src="d0_s3" to="3" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole glabrous to pubescent, usually without prickles, often with stem buds near base;</text>
      <biological_entity id="o8021" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="glabrous" name="pubescence" src="d0_s4" to="pubescent" />
      </biological_entity>
      <biological_entity id="o8022" name="prickle" name_original="prickles" src="d0_s4" type="structure" />
      <biological_entity constraint="stem" id="o8023" name="bud" name_original="buds" src="d0_s4" type="structure" />
      <biological_entity id="o8024" name="base" name_original="base" src="d0_s4" type="structure" />
      <relation from="o8021" id="r1682" modifier="usually" name="without" negation="false" src="d0_s4" to="o8022" />
      <relation from="o8021" id="r1683" modifier="often" name="with" negation="false" src="d0_s4" to="o8023" />
      <relation from="o8023" id="r1684" name="near" negation="false" src="d0_s4" to="o8024" />
    </statement>
    <statement id="d0_s5">
      <text>vascular-bundles 1–2, arranged in U or O-shape in cross-section.</text>
      <biological_entity id="o8025" name="vascular-bundle" name_original="vascular-bundles" src="d0_s5" type="structure">
        <character char_type="range_value" from="1" name="quantity" src="d0_s5" to="2" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="arranged" value_original="arranged" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Blade 2–4-pinnate;</text>
      <biological_entity id="o8026" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="2-4-pinnate" value_original="2-4-pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>rachis without prickles;</text>
      <biological_entity id="o8027" name="rachis" name_original="rachis" src="d0_s7" type="structure" />
      <biological_entity id="o8028" name="prickle" name_original="prickles" src="d0_s7" type="structure" />
      <relation from="o8027" id="r1685" name="without" negation="false" src="d0_s7" to="o8028" />
    </statement>
    <statement id="d0_s8">
      <text>nectaries absent.</text>
      <biological_entity id="o8029" name="nectary" name_original="nectaries" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Segments pinnately divided, ultimate segments ovate to lanceolate, margins dentate or lobed.</text>
      <biological_entity id="o8030" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="pinnately" name="shape" src="d0_s9" value="divided" value_original="divided" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o8031" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o8032" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Veins free, pinnately branched.</text>
      <biological_entity id="o8033" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
        <character is_modifier="false" modifier="pinnately" name="architecture" src="d0_s10" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sori marginal at vein tips, distinct, round or cylindric;</text>
      <biological_entity id="o8034" name="sorus" name_original="sori" src="d0_s11" type="structure">
        <character constraint="at vein tips" constraintid="o8035" is_modifier="false" name="position" src="d0_s11" value="marginal" value_original="marginal" />
        <character is_modifier="false" name="fusion" notes="" src="d0_s11" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="shape" src="d0_s11" value="round" value_original="round" />
        <character is_modifier="false" name="shape" src="d0_s11" value="cylindric" value_original="cylindric" />
      </biological_entity>
      <biological_entity constraint="vein" id="o8035" name="tip" name_original="tips" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>indusia formed by fusion of true indusium and minute blade tooth to form circular or slightly 2-valvate cup.</text>
      <biological_entity id="o8036" name="indusium" name_original="indusia" src="d0_s12" type="structure" />
      <biological_entity id="o8037" name="indusium" name_original="indusium" src="d0_s12" type="structure">
        <character is_modifier="true" name="derivation" src="d0_s12" value="true" value_original="true" />
        <character is_modifier="true" name="size" src="d0_s12" value="minute" value_original="minute" />
        <character is_modifier="false" name="fusion" src="d0_s12" value="circular" value_original="circular" />
        <character is_modifier="false" modifier="slightly" name="fusion" src="d0_s12" value="2-valvate" value_original="2-valvate" />
      </biological_entity>
      <biological_entity id="o8038" name="cup" name_original="cup" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Spores tetrahedral-globose, trilete (rarely monolete), tuberculate or ridged.</text>
    </statement>
    <statement id="d0_s14">
      <text>x = 34, 46, 47.</text>
      <biological_entity id="o8039" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="tetrahedral-globose" value_original="tetrahedral-globose" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="trilete" value_original="trilete" />
        <character is_modifier="false" name="relief" src="d0_s13" value="tuberculate" value_original="tuberculate" />
        <character is_modifier="false" name="shape" src="d0_s13" value="ridged" value_original="ridged" />
      </biological_entity>
      <biological_entity constraint="x" id="o8040" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="34" value_original="34" />
        <character name="quantity" src="d0_s14" value="46" value_original="46" />
        <character name="quantity" src="d0_s14" value="47" value_original="47" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide, mostly tropical.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
        <character name="distribution" value="mostly tropical" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <discussion>Species ca. 70 (3 in the flora).</discussion>
  <references>
    <reference>Tryon, R. M. 1960. A review of the genus Dennstaedtia in America. Contr. Gray Herb. 187: 23–52.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Blades yellow-green or pale green, mostly 2-pinnate-pinnatifid, leaves usually less than 1 m.</description>
      <determination>1 Dennstaedtia punctilobula</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Blades dark green, 3-pinnate, leaves 1–2(–3) m.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal segments of pinnules alternate; blades dull; indusia globose; Texas.</description>
      <determination>2 Dennstaedtia globulifera</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal segments of pinnules opposite; blades lustrous; indusia tubular or cylindric; Florida.</description>
      <determination>3 Dennstaedtia bipinnata</determination>
    </key_statement>
  </key>
</bio:treatment>