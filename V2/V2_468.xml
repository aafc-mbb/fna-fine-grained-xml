<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Newman" date="unknown" rank="family">aspleniaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">asplenium</taxon_name>
    <taxon_name authority="Kunze" date="1844" rank="species">resiliens</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>18: 331. 1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family aspleniaceae;genus asplenium;species resiliens</taxon_hierarchy>
    <other_info_on_name type="fna_id">200004162</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Roots not proliferous.</text>
      <biological_entity id="o226" name="root" name_original="roots" src="d0_s0" type="structure">
        <character is_modifier="false" modifier="not" name="reproduction" src="d0_s0" value="proliferous" value_original="proliferous" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect, unbranched;</text>
      <biological_entity id="o227" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>scales black throughout, linear-lanceolate, 4–5 × 0.2–0.6 mm, margins entire.</text>
      <biological_entity id="o228" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s2" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s2" to="5" to_unit="mm" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="width" src="d0_s2" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o229" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves monomorphic.</text>
      <biological_entity id="o230" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole black throughout, lustrous, 1.5–3 (–5) cm, 1/4–1/10 length of blade;</text>
      <biological_entity id="o231" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s4" value="black" value_original="black" />
        <character is_modifier="false" name="reflectance" src="d0_s4" value="lustrous" value_original="lustrous" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="some_measurement" src="d0_s4" to="3" to_unit="cm" />
        <character char_type="range_value" from="1/4 length of blade" name="length" src="d0_s4" to="1/10 length of blade" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>indument of blackish brown, filiform scales.</text>
      <biological_entity id="o232" name="indument" name_original="indument" src="d0_s5" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity id="o233" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s5" value="blackish brown" value_original="blackish brown" />
        <character is_modifier="true" name="shape" src="d0_s5" value="filiform" value_original="filiform" />
      </biological_entity>
      <relation from="o232" id="r45" name="part_of" negation="false" src="d0_s5" to="o233" />
    </statement>
    <statement id="d0_s6">
      <text>Blade linear to narrowly oblanceolate, 1-pinnate throughout, 9–20 (–30) × 1–2 (–2.5) cm, thick, glabrous;</text>
      <biological_entity id="o234" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s6" to="narrowly oblanceolate" />
        <character is_modifier="false" modifier="throughout" name="architecture_or_shape" src="d0_s6" value="1-pinnate" value_original="1-pinnate" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="30" to_unit="cm" />
        <character char_type="range_value" from="9" from_unit="cm" name="length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s6" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s6" to="2" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s6" value="thick" value_original="thick" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base gradually tapered;</text>
      <biological_entity id="o235" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="gradually" name="shape" src="d0_s7" value="tapered" value_original="tapered" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex acute, not rooting.</text>
      <biological_entity id="o236" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="rooting" value_original="rooting" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Rachis black throughout, lustrous, glabrous.</text>
      <biological_entity id="o237" name="rachis" name_original="rachis" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="throughout" name="coloration" src="d0_s9" value="black" value_original="black" />
        <character is_modifier="false" name="reflectance" src="d0_s9" value="lustrous" value_original="lustrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pinnae in 20–40 pairs, oblong;</text>
      <biological_entity id="o238" name="pinna" name_original="pinnae" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" notes="" src="d0_s10" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o239" name="pair" name_original="pairs" src="d0_s10" type="structure">
        <character char_type="range_value" from="20" is_modifier="true" name="quantity" src="d0_s10" to="40" />
      </biological_entity>
      <relation from="o238" id="r46" name="in" negation="false" src="d0_s10" to="o239" />
    </statement>
    <statement id="d0_s11">
      <text>medial pinnae (5–) 10–20 × (2–) 3–5 mm;</text>
      <biological_entity constraint="medial" id="o240" name="pinna" name_original="pinnae" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_length" src="d0_s11" to="10" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" src="d0_s11" to="20" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s11" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>base usually with an acroscopic auricle;</text>
      <biological_entity id="o241" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o242" name="auricle" name_original="auricle" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o241" id="r47" name="with" negation="false" src="d0_s12" to="o242" />
    </statement>
    <statement id="d0_s13">
      <text>margins ± entire to shallowly crenate;</text>
      <biological_entity id="o243" name="margin" name_original="margins" src="d0_s13" type="structure">
        <character char_type="range_value" from="less entire" name="shape" src="d0_s13" to="shallowly crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>apex obtuse.</text>
      <biological_entity id="o244" name="apex" name_original="apex" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Veins free, obscure.</text>
      <biological_entity id="o245" name="vein" name_original="veins" src="d0_s15" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s15" value="free" value_original="free" />
        <character is_modifier="false" name="prominence" src="d0_s15" value="obscure" value_original="obscure" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Sori 2–5 pairs per pinna, on both basiscopic and acroscopic sides, often confluent with age.</text>
      <biological_entity id="o246" name="sorus" name_original="sori" src="d0_s16" type="structure">
        <character char_type="range_value" constraint="per pinna" constraintid="o247" from="2" name="quantity" src="d0_s16" to="5" />
        <character constraint="with age" constraintid="o249" is_modifier="false" modifier="often" name="arrangement" notes="" src="d0_s16" value="confluent" value_original="confluent" />
      </biological_entity>
      <biological_entity id="o247" name="pinna" name_original="pinna" src="d0_s16" type="structure" />
      <biological_entity id="o248" name="side" name_original="sides" src="d0_s16" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s16" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="true" name="orientation" src="d0_s16" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <biological_entity id="o249" name="age" name_original="age" src="d0_s16" type="structure" />
      <relation from="o246" id="r48" name="on" negation="false" src="d0_s16" to="o248" />
    </statement>
    <statement id="d0_s17">
      <text>Spores 32 per sporangium.</text>
      <biological_entity id="o251" name="sporangium" name_original="sporangium" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>n = 2n = 108 (apogamous).</text>
      <biological_entity id="o250" name="spore" name_original="spores" src="d0_s17" type="structure">
        <character constraint="per sporangium" constraintid="o251" name="quantity" src="d0_s17" value="32" value_original="32" />
      </biological_entity>
      <biological_entity constraint="n=2n" id="o252" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="108" value_original="108" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs, sinkholes, on limestone or other basic rocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="sinkholes" />
        <character name="habitat" value="limestone" />
        <character name="habitat" value="other basic rocks" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>100–1500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1500" to_unit="m" from="100" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ariz., Ark., Del., Fla., Ga., Ill., Kans., Ky., La., Md., Miss., Mo., N.Mex., Nev., N.C., Okla., Pa., Tenn., Tex., Utah, Va., W.Va.; Mexico; West Indies in Hispaniola, Jamaica; Central America in Guatemala; South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies in Hispaniola" establishment_means="native" />
        <character name="distribution" value="Jamaica" establishment_means="native" />
        <character name="distribution" value="Central America in Guatemala" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18</number>
  <other_name type="common_name">Black-stemmed spleenwort</other_name>
  <discussion>Asplenium parvulum M. Martens &amp; Galeotti is an older, but illegitimate, name because it is a later homonym of A. parvulum Hooker.</discussion>
  <discussion>In Florida Asplenium resiliens hybridizes with A. heterochroum Kunze (4 x), producing A. × heteroresiliens (5 x).</discussion>
  
</bio:treatment>