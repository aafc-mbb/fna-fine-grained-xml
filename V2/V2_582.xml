<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Agardh" date="unknown" rank="family">ophioglossaceae</taxon_name>
    <taxon_name authority="Swartz" date="1801" rank="genus">botrychium</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="subgenus">Botrychium</taxon_name>
    <taxon_name authority="H. St. John" date="1929" rank="species">pinnatum</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>19: 11. 1929</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ophioglossaceae;genus botrychium;subgenus botrychium;species pinnatum;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500291</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Botrychium</taxon_name>
    <taxon_name authority="J. Milde" date="unknown" rank="species">boreale</taxon_name>
    <taxon_name authority="(Ruprecht) R.T. Clausen" date="unknown" rank="subspecies">obtusilobum</taxon_name>
    <taxon_hierarchy>genus Botrychium;species boreale;subspecies obtusilobum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trophophore stalk 0–2 mm, 0 to 0.1 times length of trophophore rachis;</text>
      <biological_entity constraint="trophophore" id="o13341" name="stalk" name_original="stalk" src="d0_s0" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s0" to="2" to_unit="mm" />
        <character constraint="rachis" constraintid="o13342" is_modifier="false" name="length" src="d0_s0" value="0-0.1 times length of trophophore rachis" />
      </biological_entity>
      <biological_entity constraint="trophophore" id="o13342" name="rachis" name_original="rachis" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>blade bright shiny green, oblong-deltate, 1–2-pinnate, to 8 × 5 cm, papery.</text>
      <biological_entity id="o13343" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s1" value="bright" value_original="bright" />
        <character is_modifier="false" name="reflectance" src="d0_s1" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="green" value_original="green" />
        <character is_modifier="false" name="shape" src="d0_s1" value="oblong-deltate" value_original="oblong-deltate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="1-2-pinnate" value_original="1-2-pinnate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="8" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s1" to="5" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s1" value="papery" value_original="papery" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pinnae to 7 pairs, only slightly ascending, approximate to overlapping, distance between 1st and 2d pinnae not or slightly more than between 2d and 3d pairs, basal pinna pair approximately equal in size and cutting to adjacent pair, obliquely ovate to lanceolate-oblong, to spatulate, deeply and regularly lobed or pinnulate, lobed to tip, margins entire to very shallowly crenate, apex truncate to somewhat acute, venation pinnate.</text>
      <biological_entity constraint="between pinnae" constraintid="o13345" id="o13344" name="pinna" name_original="pinnae" src="d0_s2" type="structure" constraint_original="between  pinnae, ">
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="7" />
        <character is_modifier="false" modifier="only slightly" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="approximate" name="arrangement" src="d0_s2" to="overlapping" />
      </biological_entity>
      <biological_entity id="o13345" name="pinna" name_original="pinnae" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o13346" name="pinna" name_original="pinna" src="d0_s2" type="structure">
        <character constraint="to pair" constraintid="o13347" is_modifier="false" modifier="slightly; between 2d and 3d pairs; approximately" name="variability" src="d0_s2" value="equal" value_original="equal" />
        <character char_type="range_value" from="obliquely ovate" name="shape" notes="" src="d0_s2" to="lanceolate-oblong" />
        <character char_type="range_value" from="obliquely ovate" name="shape" src="d0_s2" to="lanceolate-oblong" />
        <character is_modifier="false" modifier="deeply; regularly" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character name="shape" src="d0_s2" value="pinnulate" value_original="pinnulate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s2" value="entire to very" value_original="entire to very" />
        <character is_modifier="false" modifier="very; shallowly" name="shape" src="d0_s2" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o13347" name="pair" name_original="pair" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o13348" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="truncate to somewhat" value_original="truncate to somewhat" />
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s2" value="acute" value_original="acute" />
        <character constraint="to tip" constraintid="o13349" is_modifier="false" name="architecture_or_shape" src="d0_s2" value="pinnate" value_original="pinnate" />
      </biological_entity>
      <biological_entity id="o13349" name="tip" name_original="tip" src="d0_s2" type="structure" />
      <biological_entity id="o13350" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire to very" value_original="entire to very" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sporophores 2-pinnate, 1–2 times length of trophophore.</text>
      <biological_entity id="o13352" name="trophophore" name_original="trophophore" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>2n =180.</text>
      <biological_entity id="o13351" name="sporophore" name_original="sporophores" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="2-pinnate" value_original="2-pinnate" />
        <character constraint="trophophore" constraintid="o13352" is_modifier="false" name="length" src="d0_s3" value="1-2 times length of trophophore" />
      </biological_entity>
      <biological_entity constraint="2n" id="o13353" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="180" value_original="180" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Leaves appearing in June to August.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="leaves appearing time" char_type="range_value" to="August" from="June" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Grassy slopes, streambanks, woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy slopes" />
        <character name="habitat" value="streambanks" />
        <character name="habitat" value="woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., N.W.T., Yukon; Alaska, Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25</number>
  <other_name type="common_name">Northwestern moonwort</other_name>
  <discussion>Botrychium pinnatum is most commonly associated with B. lanceolatum and B. lunaria. Specimens of B. pinnatum have been misidentified as Botrychium boreale.</discussion>
  
</bio:treatment>