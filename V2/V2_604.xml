<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">marsileaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">marsilea</taxon_name>
    <taxon_name authority="Goodding" date="1902" rank="species">oligospora</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>33: 66. 1902</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family marsileaceae;genus marsilea;species oligospora</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500774</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming dense clones.</text>
      <biological_entity id="o17917" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o17918" name="clone" name_original="clones" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="dense" value_original="dense" />
      </biological_entity>
      <relation from="o17917" id="r3929" name="forming" negation="false" src="d0_s0" to="o17918" />
    </statement>
    <statement id="d0_s1">
      <text>Roots arising at nodes.</text>
      <biological_entity id="o17919" name="root" name_original="roots" src="d0_s1" type="structure">
        <character constraint="at nodes" constraintid="o17920" is_modifier="false" name="orientation" src="d0_s1" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o17920" name="node" name_original="nodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Petioles 3.1–5.7 cm, sparsely pubescent.</text>
      <biological_entity id="o17921" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="3.1" from_unit="cm" name="some_measurement" src="d0_s2" to="5.7" to_unit="cm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s2" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pinnae 6–15 × 5–14 mm, pilose.</text>
      <biological_entity id="o17922" name="pinna" name_original="pinnae" src="d0_s3" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="length" src="d0_s3" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s3" to="14" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sporocarp stalks erect, unbranched, attached at base of petiole, sometimes bent near apex, 5–10 mm.</text>
      <biological_entity constraint="sporocarp" id="o17923" name="stalk" name_original="stalks" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="unbranched" value_original="unbranched" />
        <character constraint="at base" constraintid="o17924" is_modifier="false" name="fixation" src="d0_s4" value="attached" value_original="attached" />
        <character constraint="near apex" constraintid="o17926" is_modifier="false" modifier="sometimes" name="shape" notes="" src="d0_s4" value="bent" value_original="bent" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s4" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o17924" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o17925" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o17926" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <relation from="o17924" id="r3930" name="part_of" negation="false" src="d0_s4" to="o17925" />
    </statement>
    <statement id="d0_s5">
      <text>Sporocarps nodding, 5–6 × 3.6–4 mm, 1.5 mm thick, ovate in lateral view, covered with long straight hairs when young but eventually glabrate;</text>
      <biological_entity id="o17927" name="sporocarp" name_original="sporocarps" src="d0_s5" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s5" value="nodding" value_original="nodding" />
        <character name="thickness" src="d0_s5" unit="mm" value="5-6×3.6-4" value_original="5-6×3.6-4" />
        <character name="thickness" src="d0_s5" unit="mm" value="1.5" value_original="1.5" />
        <character is_modifier="false" modifier="in lateral view" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o17928" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character is_modifier="true" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="when young; eventually" name="pubescence" src="d0_s5" value="glabrate" value_original="glabrate" />
      </biological_entity>
      <relation from="o17927" id="r3931" name="covered with" negation="false" src="d0_s5" to="o17928" />
    </statement>
    <statement id="d0_s6">
      <text>raphe 0.8–1 mm, proximal tooth 0.2–0.6 mm, curved away from sporocarp, distal tooth up to 0.4 mm, broad, blunt or absent.</text>
      <biological_entity id="o17929" name="raphe" name_original="raphe" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s6" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o17930" name="tooth" name_original="tooth" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s6" to="0.6" to_unit="mm" />
        <character constraint="away-from sporocarp" constraintid="o17931" is_modifier="false" name="course" src="d0_s6" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o17931" name="sporocarp" name_original="sporocarp" src="d0_s6" type="structure" />
      <biological_entity constraint="distal" id="o17932" name="tooth" name_original="tooth" src="d0_s6" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s6" to="0.4" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s6" value="broad" value_original="broad" />
        <character is_modifier="false" name="shape" src="d0_s6" value="blunt" value_original="blunt" />
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Sori 14–20.</text>
      <biological_entity id="o17933" name="sorus" name_original="sori" src="d0_s7" type="structure">
        <character char_type="range_value" from="14" name="quantity" src="d0_s7" to="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporocarps produced summer–fall (Jun–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporocarps appearing time" char_type="range_value" to="fall" from="summer" />
        <character name="sporocarps appearing time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Around ponds and marshes, in wet depressions in sagebrush and less commonly on river margins</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="ponds" modifier="around" constraint="in wet depressions in sagebrush" />
        <character name="habitat" value="marshes" constraint="in wet depressions in sagebrush" />
        <character name="habitat" value="wet depressions" constraint="in sagebrush" />
        <character name="habitat" value="sagebrush" />
        <character name="habitat" value="river margins" modifier="and less commonly on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <discussion>Marsilea oligospora recently has been resegregated from M. vestita (D. M. Johnson 1986), from which it differs consistently in its nodding sporocarps that lack a pronounced distal tooth and its pilose leaves and stems. Where their ranges overlap, M. oligospora also has longer sporocarp stalks than does M. vestita. Plants of this species were recently grown from spores 100 years old (D. M. Johnson 1985).</discussion>
  
</bio:treatment>