<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">lycopodiaceae</taxon_name>
    <taxon_name authority="Holub" date="1975" rank="genus">diphasiastrum</taxon_name>
    <taxon_name authority="(Ruprecht) Holub" date="1975" rank="species">sitchense</taxon_name>
    <place_of_publication>
      <publication_title>Preslia</publication_title>
      <place_in_publication>47: 108. 1975</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lycopodiaceae;genus diphasiastrum;species sitchense</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500583</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lycopodium</taxon_name>
    <taxon_name authority="Ruprecht" date="unknown" rank="species">sitchense</taxon_name>
    <place_of_publication>
      <publication_title>Beitr. Pflanzenk. Russ. Reiches</publication_title>
      <place_in_publication>3: 30. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Lycopodium;species sitchense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Horizontal stems on substrate surface or shallowly buried, 1–2.7 mm wide;</text>
      <biological_entity id="o6182" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" modifier="shallowly" name="location" src="d0_s0" value="buried" value_original="buried" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s0" to="2.7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6183" name="surface" name_original="surface" src="d0_s0" type="structure" />
      <relation from="o6182" id="r1312" name="on" negation="false" src="d0_s0" to="o6183" />
    </statement>
    <statement id="d0_s1">
      <text>leaves appressed, broadly lanceolate, 1.8–3.2 × 0.5–1 mm, apex blunt.</text>
      <biological_entity id="o6184" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s1" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s1" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s1" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6185" name="apex" name_original="apex" src="d0_s1" type="structure">
        <character is_modifier="false" name="shape" src="d0_s1" value="blunt" value_original="blunt" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Upright shoots clustered and branching mostly at base, 5.5–17.5 cm;</text>
      <biological_entity id="o6186" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="upright" value_original="upright" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character constraint="at base" constraintid="o6187" is_modifier="false" name="architecture" src="d0_s2" value="branching" value_original="branching" />
        <character char_type="range_value" from="5.5" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="17.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6187" name="base" name_original="base" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>leaves appressed, broadly lanceolate, 1.8–3.2 × 0.5–1 mm, apex acuminate.</text>
      <biological_entity id="o6188" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s3" to="3.2" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6189" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Branchlets dark green, somewhat shiny, round in cross-section, 1.7–2.5 mm wide, annual bud constrictions inconspicuous.</text>
      <biological_entity id="o6190" name="branchlet" name_original="branchlets" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark green" value_original="dark green" />
        <character is_modifier="false" modifier="somewhat" name="reflectance" src="d0_s4" value="shiny" value_original="shiny" />
        <character constraint="in cross-section" constraintid="o6191" is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" notes="" src="d0_s4" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6191" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
      <biological_entity constraint="bud" id="o6192" name="constriction" name_original="constrictions" src="d0_s4" type="structure">
        <character is_modifier="true" name="duration" src="d0_s4" value="annual" value_original="annual" />
        <character is_modifier="false" name="prominence" src="d0_s4" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves on branchlets monomorphic, 5-ranked, not overlapping, appressed to spreading-ascending, incurved, free portion of blades 3.4–5.6 × 0.4–0.9 mm, widest at middle, apex sharply pointed.</text>
      <biological_entity id="o6193" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s5" value="5-ranked" value_original="5-ranked" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s5" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="appressed" name="orientation" src="d0_s5" to="spreading-ascending" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="incurved" value_original="incurved" />
      </biological_entity>
      <biological_entity id="o6194" name="branchlet" name_original="branchlets" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="monomorphic" value_original="monomorphic" />
      </biological_entity>
      <biological_entity id="o6195" name="portion" name_original="portion" src="d0_s5" type="structure">
        <character is_modifier="true" name="fusion" src="d0_s5" value="free" value_original="free" />
        <character char_type="range_value" from="3.4" from_unit="mm" name="length" src="d0_s5" to="5.6" to_unit="mm" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="width" src="d0_s5" to="0.9" to_unit="mm" />
        <character constraint="at middle portion" constraintid="o6197" is_modifier="false" name="width" src="d0_s5" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o6196" name="blade" name_original="blades" src="d0_s5" type="structure" />
      <biological_entity constraint="middle" id="o6197" name="portion" name_original="portion" src="d0_s5" type="structure" />
      <biological_entity id="o6198" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s5" value="pointed" value_original="pointed" />
      </biological_entity>
      <relation from="o6193" id="r1313" name="on" negation="false" src="d0_s5" to="o6194" />
      <relation from="o6195" id="r1314" name="part_of" negation="false" src="d0_s5" to="o6196" />
    </statement>
    <statement id="d0_s6">
      <text>Peduncles absent or rarely 1 cm.</text>
      <biological_entity id="o6199" name="peduncle" name_original="peduncles" src="d0_s6" type="structure">
        <character is_modifier="false" name="presence" src="d0_s6" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s6" value="rarely" value_original="rarely" />
        <character name="some_measurement" src="d0_s6" unit="cm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Stalks absent.</text>
      <biological_entity id="o6200" name="stalk" name_original="stalks" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Strobili solitary on upright shoots, 4.5–38 × 3–5 mm, gradually narrowing to rounded tip.</text>
      <biological_entity id="o6201" name="strobilus" name_original="strobili" src="d0_s8" type="structure">
        <character constraint="on shoots" constraintid="o6202" is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="length" notes="" src="d0_s8" to="38" to_unit="mm" />
        <character is_modifier="false" modifier="gradually" name="width" src="d0_s8" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity id="o6202" name="shoot" name_original="shoots" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="upright" value_original="upright" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" notes="" src="d0_s8" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6203" name="tip" name_original="tip" src="d0_s8" type="structure">
        <character is_modifier="true" name="shape" src="d0_s8" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Sporophylls deltate, 1.8–3.6 × 1.7–2.8 mm;</text>
      <biological_entity id="o6204" name="sporophyll" name_original="sporophylls" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s9" to="3.6" to_unit="mm" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="width" src="d0_s9" to="2.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>apex rounded.</text>
    </statement>
    <statement id="d0_s11">
      <text>2n = 46.</text>
      <biological_entity id="o6205" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6206" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="46" value_original="46" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine meadows, open rocky barrens, conifer woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine meadows" />
        <character name="habitat" value="open rocky barrens" />
        <character name="habitat" value="conifer woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Idaho, Maine, Mont., N.H., N.Y., Oreg., Vt., Wash.; Asia in Kamchatka, Japan.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Asia in Kamchatka" establishment_means="native" />
        <character name="distribution" value="Japan" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <other_name type="common_name">Sitka club-moss</other_name>
  <other_name type="common_name">lycopode de Sitka</other_name>
  <discussion>The mature shoots in Diphasiastrum sitchense resemble the juvenile phases of the other species. The unique, round, 5-ranked leaves may represent an early developmental state.</discussion>
  <discussion>The hybrid Diphasiastrum alpinum X sitchense is very rare. It is known from Greenland, British Columbia, Newfoundland, Montana, Oregon, and Washington. Specimens of D. sitchense from Greenland, Newfoundland, and Washington cited by J. H. Wilce (1965) are actually this hybrid.</discussion>
  
</bio:treatment>