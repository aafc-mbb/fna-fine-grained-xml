<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Thomas A. Lumpkin</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">338</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Wettstein" date="unknown" rank="family">Azollaceae</taxon_name>
    <taxon_hierarchy>family Azollaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">10085</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants aquatic, floating on placid water, occasionally stranded, subsisting on mud;</text>
      <biological_entity id="o14650" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="aquatic" value_original="aquatic" />
        <character constraint="on placid water" is_modifier="false" name="growth_form_or_location" src="d0_s0" value="floating" value_original="floating" />
        <character is_modifier="false" modifier="occasionally" name="condition" src="d0_s0" value="stranded" value_original="stranded" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o14651" name="mud" name_original="mud" src="d0_s0" type="structure" />
      <relation from="o14650" id="r3205" name="subsisting on" negation="false" src="d0_s0" to="o14651" />
    </statement>
    <statement id="d0_s1">
      <text>plants heterosporous (producing 2 kinds of spores), leptosporangiate, proliferous by axillary fragmentation.</text>
      <biological_entity id="o14652" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s1" value="heterosporous" value_original="heterosporous" />
        <character is_modifier="false" name="reproduction" src="d0_s1" value="leptosporangiate" value_original="leptosporangiate" />
        <character constraint="by axillary fragmentation" constraintid="o14653" is_modifier="false" name="reproduction" src="d0_s1" value="proliferous" value_original="proliferous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o14653" name="fragmentation" name_original="fragmentation" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Roots translucent to brown, lax, singular [in bundles] without branches, emerging at stem branch points;</text>
      <biological_entity id="o14654" name="root" name_original="roots" src="d0_s2" type="structure">
        <character char_type="range_value" from="translucent" name="coloration" src="d0_s2" to="brown" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s2" value="lax" value_original="lax" />
        <character constraint="without branches" constraintid="o14655" is_modifier="false" name="arrangement" src="d0_s2" value="singular" value_original="singular" />
      </biological_entity>
      <biological_entity id="o14655" name="branch" name_original="branches" src="d0_s2" type="structure" />
      <biological_entity constraint="branch" id="o14656" name="point" name_original="points" src="d0_s2" type="structure" constraint_original="stem branch" />
      <relation from="o14654" id="r3206" name="emerging at" negation="false" src="d0_s2" to="o14656" />
    </statement>
    <statement id="d0_s3">
      <text>root hairs to 1 cm, emerging from root cap.</text>
      <biological_entity constraint="root" id="o14657" name="hair" name_original="hairs" src="d0_s3" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s3" to="1" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="root" id="o14658" name="cap" name_original="cap" src="d0_s3" type="structure" />
      <relation from="o14657" id="r3207" name="emerging from" negation="false" src="d0_s3" to="o14658" />
    </statement>
    <statement id="d0_s4">
      <text>Stems usually not green, with extensive subdichotomous branching, prostrate and reniform or polyreniform, or nearly erect.</text>
      <biological_entity id="o14659" name="stem" name_original="stems" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="usually not" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character is_modifier="false" modifier="with extensive subdichotomous branching" name="growth_form_or_orientation" src="d0_s4" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="reniform" value_original="reniform" />
        <character name="shape" src="d0_s4" value="polyreniform" value_original="polyreniform" />
        <character is_modifier="false" modifier="nearly" name="orientation" src="d0_s4" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Leaves sessile, alternate, often imbricate, in 2 ranks along upper side of stem, 0.6–2 mm wide;</text>
      <biological_entity id="o14661" name="rank" name_original="ranks" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
      </biological_entity>
      <biological_entity constraint="upper" id="o14662" name="side" name_original="side" src="d0_s5" type="structure" />
      <biological_entity id="o14663" name="stem" name_original="stem" src="d0_s5" type="structure" />
      <relation from="o14660" id="r3208" name="in" negation="false" src="d0_s5" to="o14661" />
      <relation from="o14661" id="r3209" name="along" negation="false" src="d0_s5" to="o14662" />
      <relation from="o14662" id="r3210" name="part_of" negation="false" src="d0_s5" to="o14663" />
    </statement>
    <statement id="d0_s6">
      <text>each leaf with 2 lobes;</text>
      <biological_entity id="o14660" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="alternate" value_original="alternate" />
        <character is_modifier="false" modifier="often" name="arrangement" src="d0_s5" value="imbricate" value_original="imbricate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" notes="" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14664" name="leaf" name_original="leaf" src="d0_s6" type="structure" />
      <biological_entity id="o14665" name="lobe" name_original="lobes" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="2" value_original="2" />
      </biological_entity>
      <relation from="o14664" id="r3211" name="with" negation="false" src="d0_s6" to="o14665" />
    </statement>
    <statement id="d0_s7">
      <text>upper (emersed) lobe greenish or reddish and photosynthetic, with narrow colorless margin, several cells thick, bearing colony of blue-green algae (Anabaena) in ovoid cavity at base of lower side;</text>
      <biological_entity constraint="upper" id="o14666" name="lobe" name_original="lobe" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="nutrition" src="d0_s7" value="photosynthetic" value_original="photosynthetic" />
      </biological_entity>
      <biological_entity id="o14667" name="margin" name_original="margin" src="d0_s7" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s7" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="coloration" src="d0_s7" value="colorless" value_original="colorless" />
      </biological_entity>
      <biological_entity id="o14668" name="cell" name_original="cells" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="several" value_original="several" />
        <character is_modifier="false" name="width" src="d0_s7" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o14669" name="colony" name_original="colony" src="d0_s7" type="structure" />
      <biological_entity id="o14670" name="algae" name_original="algae" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="blue-green" value_original="blue-green" />
      </biological_entity>
      <biological_entity id="o14671" name="cavity" name_original="cavity" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o14672" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity constraint="lower" id="o14673" name="side" name_original="side" src="d0_s7" type="structure" />
      <relation from="o14666" id="r3212" name="with" negation="false" src="d0_s7" to="o14667" />
      <relation from="o14668" id="r3213" name="bearing" negation="false" src="d0_s7" to="o14669" />
      <relation from="o14668" id="r3214" name="part_of" negation="false" src="d0_s7" to="o14670" />
      <relation from="o14668" id="r3215" name="in" negation="false" src="d0_s7" to="o14671" />
      <relation from="o14671" id="r3216" name="at" negation="false" src="d0_s7" to="o14672" />
      <relation from="o14672" id="r3217" name="part_of" negation="false" src="d0_s7" to="o14673" />
    </statement>
    <statement id="d0_s8">
      <text>lower lobe often floating or immersed, slightly larger than upper lobe, mostly not green (often colorless and translucent), 1 cell thick except at base, ± cupshaped.</text>
      <biological_entity constraint="lower" id="o14674" name="lobe" name_original="lobe" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="often" name="growth_form_or_location" src="d0_s8" value="floating" value_original="floating" />
        <character is_modifier="false" name="prominence" src="d0_s8" value="immersed" value_original="immersed" />
        <character constraint="than upper lobe" constraintid="o14675" is_modifier="false" name="size" src="d0_s8" value="slightly larger" value_original="slightly larger" />
        <character is_modifier="false" modifier="mostly not" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="upper" id="o14675" name="lobe" name_original="lobe" src="d0_s8" type="structure" />
      <biological_entity id="o14676" name="cell" name_original="cell" src="d0_s8" type="structure">
        <character constraint="except " constraintid="o14677" is_modifier="false" name="width" src="d0_s8" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s8" value="cup-shaped" value_original="cup-shaped" />
      </biological_entity>
      <biological_entity id="o14677" name="base" name_original="base" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Sporocarps in pairs [tetrads] at base of lateral branches, members of pair of same sex or of different sexes.</text>
      <biological_entity id="o14678" name="sporocarp" name_original="sporocarps" src="d0_s9" type="structure" />
      <biological_entity id="o14679" name="pair" name_original="pairs" src="d0_s9" type="structure" />
      <relation from="o14678" id="r3218" name="in" negation="false" src="d0_s9" to="o14679" />
    </statement>
    <statement id="d0_s10">
      <text>Megasporocarps containing 1 megasporangium that produces 1 functional megaspore.</text>
      <biological_entity id="o14680" name="megasporocarp" name_original="megasporocarps" src="d0_s10" type="structure" />
      <biological_entity id="o14681" name="megasporangium" name_original="megasporangium" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="functional" id="o14682" name="megaspore" name_original="megaspore" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="1" value_original="1" />
      </biological_entity>
      <relation from="o14680" id="r3219" name="containing" negation="false" src="d0_s10" to="o14681" />
      <relation from="o14681" id="r3220" name="produces" negation="false" src="d0_s10" to="o14682" />
    </statement>
    <statement id="d0_s11">
      <text>Megaspore spheric, 0.2–0.6 mm, topped with dark, conic, slightly narrower structure (indusium) covering 3 [9] floats and a blue-green algal colony.</text>
      <biological_entity id="o14683" name="megaspore" name_original="megaspore" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="spheric" value_original="spheric" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
        <character constraint="with structure" constraintid="o14684" is_modifier="false" name="shape" src="d0_s11" value="topped" value_original="topped" />
      </biological_entity>
      <biological_entity id="o14684" name="structure" name_original="structure" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s11" value="dark" value_original="dark" />
        <character is_modifier="true" name="shape" src="d0_s11" value="conic" value_original="conic" />
        <character is_modifier="true" modifier="slightly" name="width" src="d0_s11" value="narrower" value_original="narrower" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Microsporocarps globose, apically umbonate, 10–27µm diam., containing to 130 microsporangia;</text>
      <biological_entity id="o14685" name="microsporocarp" name_original="microsporocarps" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="globose" value_original="globose" />
        <character is_modifier="false" modifier="apically" name="shape" src="d0_s12" value="umbonate" value_original="umbonate" />
        <character char_type="range_value" from="10" from_unit="µm" name="diameter" src="d0_s12" to="27" to_unit="µm" />
      </biological_entity>
      <biological_entity id="o14686" name="microsporangium" name_original="microsporangia" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="130" value_original="130" />
      </biological_entity>
      <relation from="o14685" id="r3221" name="containing to" negation="false" src="d0_s12" to="o14686" />
    </statement>
    <statement id="d0_s13">
      <text>microsporangia containing 32 or 64 microspores 3 µm diam., aggregated into 3–10 masses covered with arrowlike barbs [glabrous or with needlelike hairs on 1 side].</text>
      <biological_entity id="o14687" name="microsporangium" name_original="microsporangia" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" unit="or microspores" value="32" value_original="32" />
        <character name="quantity" src="d0_s13" unit="or microspores" value="64" value_original="64" />
        <character name="diameter" src="d0_s13" unit="um" value="3" value_original="3" />
        <character constraint="into masses" constraintid="o14688" is_modifier="false" name="arrangement" src="d0_s13" value="aggregated" value_original="aggregated" />
      </biological_entity>
      <biological_entity id="o14688" name="mass" name_original="masses" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" is_modifier="true" name="quantity" src="d0_s13" to="10" />
      </biological_entity>
      <biological_entity id="o14689" name="barb" name_original="barbs" src="d0_s13" type="structure">
        <character is_modifier="true" name="shape" src="d0_s13" value="arrowlike" value_original="arrowlike" />
      </biological_entity>
      <relation from="o14688" id="r3222" name="covered with" negation="false" src="d0_s13" to="o14689" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide, tropical to temperate regions.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
        <character name="distribution" value="tropical to temperate regions" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25</number>
  <other_name type="common_name">Azolla Family</other_name>
  <discussion>Azollaceae has been included in Salviniaceae, but the relationship is not close.</discussion>
  <discussion>In this treatment, "upper lobe" refers to the emersed lobe and "lower lobe" refers to the immersed lobe. Developmentally, the emersed lobe is actually abaxial and the immersed lobe is adaxial. To facilitate identification in the field, however, the terms describe the appearance of the lobes to the viewer, not the development of the lobes.</discussion>
  <discussion>Genus 1, species ca. 7 (3 species in the flora).</discussion>
  <references>
    <reference>Christensen, C. 1938. Azollaceae. In: F. Verdoorn, ed. 1938. Manual of Pteridology. The Hague. P. 550.</reference>
    <reference>Reed, C. F. 1954. Index Marsileata et Salviniata. Bol. Soc. Brot., ser. 2a, 28: 1–61.</reference>
  </references>
  
</bio:treatment>