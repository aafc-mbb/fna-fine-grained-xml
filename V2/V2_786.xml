<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Newman" date="1851" rank="genus">gymnocarpium</taxon_name>
    <taxon_name authority="(Linnaeus) Newman" date="1851" rank="species">dryopteris</taxon_name>
    <place_of_publication>
      <publication_title>Phytologist</publication_title>
      <place_in_publication>4: app. 24. 1851</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus gymnocarpium;species dryopteris</taxon_hierarchy>
    <other_info_on_name type="fna_id">200003903</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">dryopteris</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1093. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species dryopteris;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryopteris</taxon_name>
    <taxon_name authority="C. Christensen" date="unknown" rank="species">linnaeana</taxon_name>
    <taxon_hierarchy>genus Dryopteris;species linnaeana;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lastrea</taxon_name>
    <taxon_name authority="(Linnaeus) Bory" date="unknown" rank="species">dryopteris</taxon_name>
    <taxon_hierarchy>genus Lastrea;species dryopteris;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Phegopteris</taxon_name>
    <taxon_name authority="(Linnaeus) Fée" date="unknown" rank="species">dryopteris</taxon_name>
    <taxon_hierarchy>genus Phegopteris;species dryopteris;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelypteris</taxon_name>
    <taxon_name authority="(Linnaeus) Slosson" date="unknown" rank="species">dryopteris</taxon_name>
    <taxon_hierarchy>genus Thelypteris;species dryopteris;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0.5–1.5 mm diam.;</text>
      <biological_entity id="o10105" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s0" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales 1–4 mm.</text>
      <biological_entity id="o10106" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Fertile leaves usually 12–42 cm.</text>
      <biological_entity id="o10107" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="12" from_unit="cm" name="some_measurement" src="d0_s2" to="42" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 9–28 cm, with sparse glandular-hairs distally;</text>
      <biological_entity id="o10108" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="9" from_unit="cm" name="some_measurement" src="d0_s3" to="28" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o10109" name="glandular-hair" name_original="glandular-hairs" src="d0_s3" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s3" value="sparse" value_original="sparse" />
      </biological_entity>
      <relation from="o10108" id="r2172" name="with" negation="false" src="d0_s3" to="o10109" />
    </statement>
    <statement id="d0_s4">
      <text>scales 2–6 mm.</text>
      <biological_entity id="o10110" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade broadly deltate, 2-pinnate-pinnatifid, 3–14 cm, lax and delicate, abaxial surface and rachis glabrous or with sparse glandular-hairs, adaxial surface glabrous.</text>
      <biological_entity id="o10111" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="2-pinnate-pinnatifid" value_original="2-pinnate-pinnatifid" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="14" to_unit="cm" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="delicate" value_original="delicate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10112" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="with sparse glandular-hairs" value_original="with sparse glandular-hairs" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o10113" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o10114" name="glandular-hair" name_original="glandular-hairs" src="d0_s5" type="structure">
        <character is_modifier="true" name="count_or_density" src="d0_s5" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o10115" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o10112" id="r2173" name="with" negation="false" src="d0_s5" to="o10114" />
      <relation from="o10113" id="r2174" name="with" negation="false" src="d0_s5" to="o10114" />
    </statement>
    <statement id="d0_s6">
      <text>Pinna apex entire, rounded.</text>
      <biological_entity constraint="pinna" id="o10116" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Proximal pinnae 2–12 cm, ± perpendicular to rachis, with basiscopic pinnules ± perpendicular to costa;</text>
      <biological_entity constraint="proximal" id="o10117" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="12" to_unit="cm" />
        <character constraint="to rachis" constraintid="o10118" is_modifier="false" modifier="more or less" name="orientation" src="d0_s7" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o10118" name="rachis" name_original="rachis" src="d0_s7" type="structure" />
      <biological_entity id="o10119" name="pinnule" name_original="pinnules" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="basiscopic" value_original="basiscopic" />
        <character constraint="to costa" constraintid="o10120" is_modifier="false" modifier="more or less" name="orientation" src="d0_s7" value="perpendicular" value_original="perpendicular" />
      </biological_entity>
      <biological_entity id="o10120" name="costa" name_original="costa" src="d0_s7" type="structure" />
      <relation from="o10117" id="r2175" name="with" negation="false" src="d0_s7" to="o10119" />
    </statement>
    <statement id="d0_s8">
      <text>basal basiscopic pinnule usually sessile, pinnatifid or rarely pinnate-pinnatifid, if sessile then with basal basiscopic pinnulet often equaling or longer than adjacent pinnulet;</text>
      <biological_entity constraint="basal" id="o10121" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o10122" name="pinnule" name_original="pinnule" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s8" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="pinnate-pinnatifid" value_original="pinnate-pinnatifid" />
        <character constraint="with basal pinnae" constraintid="o10123" is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10123" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o10124" name="pinnulet" name_original="pinnulet" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="often" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
        <character constraint="than adjacent pinnulet" constraintid="o10125" is_modifier="false" name="length_or_size" src="d0_s8" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o10125" name="pinnulet" name_original="pinnulet" src="d0_s8" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s8" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2d basal basiscopic pinnule sessile, with basal basiscopic pinnulet equaling or longer than adjacent pinnulet;</text>
      <biological_entity constraint="basal" id="o10126" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o10127" name="pinnule" name_original="pinnule" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10128" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o10129" name="pinnulet" name_original="pinnulet" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
        <character constraint="than adjacent pinnulet" constraintid="o10130" is_modifier="false" name="length_or_size" src="d0_s9" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o10130" name="pinnulet" name_original="pinnulet" src="d0_s9" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s9" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o10127" id="r2176" name="with" negation="false" src="d0_s9" to="o10128" />
    </statement>
    <statement id="d0_s10">
      <text>basal acroscopic pinnule sessile, with basal basiscopic pinnulet longer than or equaling adjacent pinnulet.</text>
      <biological_entity constraint="basal" id="o10131" name="pinna" name_original="pinnae" src="d0_s10" type="structure" />
      <biological_entity id="o10132" name="pinnule" name_original="pinnule" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10133" name="pinna" name_original="pinnae" src="d0_s10" type="structure" />
      <biological_entity id="o10134" name="pinnulet" name_original="pinnulet" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="basiscopic" value_original="basiscopic" />
        <character constraint="than or equaling adjacent pinnulet" constraintid="o10135" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o10135" name="pinnulet" name_original="pinnulet" src="d0_s10" type="structure">
        <character is_modifier="true" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s10" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o10132" id="r2177" name="with" negation="false" src="d0_s10" to="o10133" />
    </statement>
    <statement id="d0_s11">
      <text>Pinnae of 2d pair usually sessile with basal basiscopic pinnule longer than or equaling adjacent pinnule and about equal to basal acroscopic pinnule;</text>
      <biological_entity id="o10136" name="pinna" name_original="pinnae" src="d0_s11" type="structure">
        <character constraint="with basal pinnae" constraintid="o10138" is_modifier="false" modifier="usually" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o10137" name="pair" name_original="pair" src="d0_s11" type="structure" />
      <biological_entity constraint="basal" id="o10138" name="pinna" name_original="pinnae" src="d0_s11" type="structure" />
      <biological_entity id="o10139" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s11" value="basiscopic" value_original="basiscopic" />
        <character constraint="than or equaling adjacent pinnule" constraintid="o10140" is_modifier="false" name="length_or_size" src="d0_s11" value="longer" value_original="longer" />
        <character is_modifier="false" name="variability" notes="" src="d0_s11" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity id="o10140" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s11" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10141" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s11" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o10136" id="r2178" name="part_of" negation="false" src="d0_s11" to="o10137" />
    </statement>
    <statement id="d0_s12">
      <text>basal acroscopic pinnule equaling or slightly shorter than adjacent pinnule, often with entire, rounded apex.</text>
      <biological_entity constraint="basal" id="o10142" name="pinna" name_original="pinnae" src="d0_s12" type="structure" />
      <biological_entity id="o10143" name="pinnule" name_original="pinnule" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
        <character constraint="than adjacent pinnule" constraintid="o10144" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o10144" name="pinnule" name_original="pinnule" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o10145" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="true" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o10143" id="r2179" modifier="often" name="with" negation="false" src="d0_s12" to="o10145" />
    </statement>
    <statement id="d0_s13">
      <text>Pinnae of 3d pair sessile with basal basiscopic pinnule equaling adjacent pinnule and equaling basal acroscopic pinnules;</text>
      <biological_entity id="o10146" name="pinna" name_original="pinnae" src="d0_s13" type="structure">
        <character constraint="with basal pinnae" constraintid="o10148" is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o10147" name="pair" name_original="pair" src="d0_s13" type="structure" />
      <biological_entity constraint="basal" id="o10148" name="pinna" name_original="pinnae" src="d0_s13" type="structure" />
      <biological_entity id="o10149" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="basiscopic" value_original="basiscopic" />
      </biological_entity>
      <biological_entity id="o10150" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity constraint="basal" id="o10151" name="pinnule" name_original="pinnules" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="orientation" src="d0_s13" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o10146" id="r2180" name="part_of" negation="false" src="d0_s13" to="o10147" />
    </statement>
    <statement id="d0_s14">
      <text>basal acroscopic pinnule equaling or slightly shorter than adjacent pinnule.</text>
      <biological_entity constraint="basal" id="o10152" name="pinna" name_original="pinnae" src="d0_s14" type="structure" />
      <biological_entity id="o10153" name="pinnule" name_original="pinnule" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
        <character constraint="than adjacent pinnule" constraintid="o10154" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o10154" name="pinnule" name_original="pinnule" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s14" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Ultimate segments of proximal pinnae oblong, entire to crenate, apex entire, rounded.</text>
      <biological_entity constraint="ultimate" id="o10155" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s15" to="crenate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o10156" name="pinna" name_original="pinnae" src="d0_s15" type="structure" />
      <biological_entity id="o10157" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o10155" id="r2181" name="part_of" negation="false" src="d0_s15" to="o10156" />
    </statement>
    <statement id="d0_s16">
      <text>Spores 34–39 µm. 2n = 160.</text>
      <biological_entity id="o10158" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="34" from_unit="um" name="some_measurement" src="d0_s16" to="39" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="2n" id="o10159" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="160" value_original="160" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cool, coniferous and mixed woods and at base of shale talus slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coniferous" modifier="cool" />
        <character name="habitat" value="mixed woods" />
        <character name="habitat" value="base" modifier="and at" constraint="of shale talus slopes" />
        <character name="habitat" value="shale talus slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; St. Pierre and Miquelon; Alta., B.C., Man., N.B., Nfld. and Labr. (Nfld.), N.W.T., N.S., Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Colo., Conn., Idaho, Iowa, Maine, Mass., Mich., Minn., Mont., N.H., N.J., N.Mex., N.Y., Ohio, Oreg., Pa., R.I., S.Dak., Vt., Wash., W.Va., Wis., Wyo.; n,c Europe; n Asia to China, Japan.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="St. Pierre and Miquelon" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="n" establishment_means="native" />
        <character name="distribution" value="c Europe" establishment_means="native" />
        <character name="distribution" value="n Asia to China" establishment_means="native" />
        <character name="distribution" value="Japan" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Common oak fern</other_name>
  <other_name type="common_name">gymnocarpe fougère-du-chêne</other_name>
  <discussion>Gymnocarpium dryopteris is a fertile allotetraploid species that arose following hybridization between G. appalachianum and G. disjunctum (see reticulogram). Its wide distribution over much of the north temperate zone has provided ample opportunity for secondary contact between G. dryopteris and each of its diploid parents, thereby resulting in a wide-ranging composite of abortive-spored triploid crosses (G. disjunctum × G. dryopteris and G. appalachianum × G. dryopteris). These relationships are shown on the diagram. Sterile triploid plants are not restricted only to areas where the range of the tetraploid overlaps with that of either diploid. Their broad distribution could be explained in part by their spores, which are of two types: malformed, black, and with very exaggerated perispores, or round with extensive netted perispores (K. M. Pryer and D. M. Britton 1983). The latter spore type is capable of germination and presumably permits the plants to reproduce apogamously. The name G. × brittonianum (Sarvela) Pryer &amp; Haufler has been applied to the G. disjunctum × G. dryopteris hybrid formula (K. M. Pryer and C. H. Haufler 1993). The type of G. × brittonianum has aborted and round spores, and leaves that strongly resemble those of G. disjunctum. They are large, 3-pinnate-pinnatifid, and the second and third pairs of pinnae are sessile with basal basiscopic pinnules markedly longer than the basal acroscopic pinnules. Sterile triploid plants with a morphology similar to the type of G. × brittonianum are frequent. The biology of both of these cryptic hybrid taxa needs further study, which should lead to detailed morphologic descriptions and distribution maps.</discussion>
  <discussion>Gymnocarpium dryopteris also hybridizes with both G. jessoense subsp. parvulum and G. robertianum.</discussion>
  
</bio:treatment>