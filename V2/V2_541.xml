<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ching ex Pichi Sermolli" date="unknown" rank="family">thelypteridaceae</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">thelypteris</taxon_name>
    <taxon_name authority="(Kunze) A. R. Smith" date="1973" rank="subgenus">Amauropelta</taxon_name>
    <taxon_name authority="(Desvaux) Proctor" date="1953" rank="species">resinifera</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Inst. Jamaica, Sci. Ser.</publication_title>
      <place_in_publication>5: 63. 1953</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family thelypteridaceae;genus thelypteris;subgenus amauropelta;species resinifera;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501300</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Desvaux" date="unknown" rank="species">resiniferum</taxon_name>
    <place_of_publication>
      <publication_title>Ges. Naturf. Freunde Berlin Mag. Neuesten Entdeck. Gesammten Naturk.</publication_title>
      <place_in_publication>5: 317. 1811</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species resiniferum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Amauropelta</taxon_name>
    <taxon_name authority="(Desvaux) Pichi-Sermolli" date="unknown" rank="species">resinifera</taxon_name>
    <taxon_hierarchy>genus Amauropelta;species resinifera;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryopteris</taxon_name>
    <taxon_name authority="(C. Presl) C. Christensen" date="unknown" rank="species">panamensis</taxon_name>
    <taxon_hierarchy>genus Dryopteris;species panamensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Thelypteris</taxon_name>
    <taxon_name authority="(C. Presl) E. P. St. John." date="unknown" rank="species">panamensis</taxon_name>
    <taxon_hierarchy>genus Thelypteris;species panamensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect, (3–) 5–12 mm diam.</text>
      <biological_entity id="o16532" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character char_type="range_value" from="3" from_unit="mm" name="diameter" src="d0_s0" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s0" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves monomorphic, evergreen, clustered, (15–) 25–100 (–135) cm;</text>
      <biological_entity id="o16533" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="duration" src="d0_s1" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="15" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="25" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="100" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="135" to_unit="cm" />
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>croziers often mucilaginous.</text>
      <biological_entity id="o16534" name="crozier" name_original="croziers" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="often" name="coating" src="d0_s2" value="mucilaginous" value_original="mucilaginous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole straw-colored, 2–15 (–25) cm × 1–4 (–6) mm, at base with brownish, ovatelanceolate, ± glabrous scales.</text>
      <biological_entity id="o16535" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="straw-colored" value_original="straw-colored" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="25" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s3" to="15" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s3" to="6" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s3" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16536" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o16537" name="scale" name_original="scales" src="d0_s3" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s3" value="brownish" value_original="brownish" />
        <character is_modifier="true" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="true" modifier="more or less" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o16535" id="r3630" name="at" negation="false" src="d0_s3" to="o16536" />
      <relation from="o16536" id="r3631" name="with" negation="false" src="d0_s3" to="o16537" />
    </statement>
    <statement id="d0_s4">
      <text>Blade to 110 cm, proximally with 6–12 pairs of smaller pinnae, distally tapering gradually to a pinnatifid apex.</text>
      <biological_entity id="o16538" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s4" to="110" to_unit="cm" />
        <character constraint="to apex" constraintid="o16541" is_modifier="false" modifier="distally" name="shape" notes="" src="d0_s4" value="tapering" value_original="tapering" />
      </biological_entity>
      <biological_entity id="o16539" name="pair" name_original="pairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s4" to="12" />
      </biological_entity>
      <biological_entity constraint="smaller" id="o16540" name="pinna" name_original="pinnae" src="d0_s4" type="structure" />
      <biological_entity id="o16541" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="true" name="shape" src="d0_s4" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <relation from="o16538" id="r3632" modifier="proximally" name="with" negation="false" src="d0_s4" to="o16539" />
      <relation from="o16539" id="r3633" name="part_of" negation="false" src="d0_s4" to="o16540" />
    </statement>
    <statement id="d0_s5">
      <text>Proximal pinnae hastate or auriculate;</text>
      <biological_entity constraint="proximal" id="o16542" name="pinna" name_original="pinnae" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="hastate" value_original="hastate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="auriculate" value_original="auriculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>longest (medial) pinnae 2–14 (–20) × 0.4–2.5 cm, incised to within 1 mm of costa;</text>
      <biological_entity constraint="longest" id="o16543" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character char_type="range_value" from="14" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s6" to="20" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s6" to="14" to_unit="cm" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="width" src="d0_s6" to="2.5" to_unit="cm" />
        <character constraint="of costa" constraintid="o16544" is_modifier="false" name="shape" src="d0_s6" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o16544" name="costa" name_original="costa" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>segments 2–3 mm wide, strongly oblique and somewhat curved;</text>
      <biological_entity id="o16545" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="strongly" name="orientation_or_shape" src="d0_s7" value="oblique" value_original="oblique" />
        <character is_modifier="false" modifier="somewhat" name="course" src="d0_s7" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>veins meeting margin above sinus.</text>
      <biological_entity id="o16546" name="vein" name_original="veins" src="d0_s8" type="structure" />
      <biological_entity id="o16547" name="margin" name_original="margin" src="d0_s8" type="structure" />
      <biological_entity id="o16548" name="sinus" name_original="sinus" src="d0_s8" type="structure" />
      <relation from="o16546" id="r3634" name="meeting" negation="false" src="d0_s8" to="o16547" />
      <relation from="o16546" id="r3635" name="above" negation="false" src="d0_s8" to="o16548" />
    </statement>
    <statement id="d0_s9">
      <text>Indument abaxially of hairs 0.2–0.5 mm on rachises, costae, and sometimes veins and blade tissue, also of numerous glands, these yellowish to often reddish, resinous, shiny, sessile, hemispheric;</text>
      <biological_entity id="o16549" name="indument" name_original="indument" src="d0_s9" type="structure">
        <character char_type="range_value" constraint="on rachises" constraintid="o16551" from="0.2" from_unit="mm" name="some_measurement" src="d0_s9" to="0.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16550" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <biological_entity id="o16551" name="rachis" name_original="rachises" src="d0_s9" type="structure" />
      <biological_entity id="o16552" name="costa" name_original="costae" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s9" value="yellowish to often" value_original="yellowish to often" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s9" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="coating" src="d0_s9" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="reflectance" src="d0_s9" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s9" value="hemispheric" value_original="hemispheric" />
      </biological_entity>
      <biological_entity id="o16553" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity constraint="blade" id="o16554" name="tissue" name_original="tissue" src="d0_s9" type="structure" />
      <biological_entity id="o16555" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s9" value="numerous" value_original="numerous" />
      </biological_entity>
      <relation from="o16549" id="r3636" name="part_of" negation="false" src="d0_s9" to="o16550" />
      <relation from="o16552" id="r3637" name="consist_of" negation="false" src="d0_s9" to="o16555" />
    </statement>
    <statement id="d0_s10">
      <text>blade tissue adaxially glabrous or sparsely hairy.</text>
      <biological_entity constraint="blade" id="o16556" name="tissue" name_original="tissue" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="hairy" value_original="hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sori round, medial to submarginal;</text>
      <biological_entity id="o16557" name="sorus" name_original="sori" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="round" value_original="round" />
        <character char_type="range_value" from="medial" name="position" src="d0_s11" to="submarginal" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>indusia tan, glandular, and sparsely hairy.</text>
    </statement>
    <statement id="d0_s13">
      <text>2n = 58.</text>
      <biological_entity id="o16558" name="indusium" name_original="indusia" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="tan" value_original="tan" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s12" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s12" value="hairy" value_original="hairy" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16559" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="58" value_original="58" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Damp woods and swamps in subacid soil</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="woods" modifier="damp" constraint="in subacid soil" />
        <character name="habitat" value="swamps" constraint="in subacid soil" />
        <character name="habitat" value="subacid soil" modifier="in" />
        <character name="habitat" value="damp" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; Mexico; West Indies in the Greater Antilles; Central America; nw South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies in the Greater Antilles" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="nw South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <other_name type="common_name">Glandular maiden fern</other_name>
  <other_name type="common_name">wax-dot maiden fern</other_name>
  
</bio:treatment>