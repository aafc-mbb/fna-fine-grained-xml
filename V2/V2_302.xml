<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ching" date="unknown" rank="family">dennstaedtiaceae</taxon_name>
    <taxon_name authority="Gleditsch ex Scopoli" date="1760" rank="genus">pteridium</taxon_name>
    <taxon_name authority="(Linnaeus) Kuhn in Decken" date="1879" rank="species">aquilinum</taxon_name>
    <place_of_publication>
      <publication_title>in Decken,Reisen Ost-Afrika</publication_title>
      <place_in_publication>3(3): 11. 1879</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dennstaedtiaceae;genus pteridium;species aquilinum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200003325</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pteris</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">aquilina</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1075. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pteris;species aquilina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Petioles scattered along creeping stems, 0.3–3.5 m, shallowly to deeply grooved adaxially, base not strongly distinct from stem.</text>
      <biological_entity id="o18084" name="petiole" name_original="petioles" src="d0_s0" type="structure">
        <character constraint="along stems" constraintid="o18085" is_modifier="false" name="arrangement" src="d0_s0" value="scattered" value_original="scattered" />
        <character char_type="range_value" from="0.3" from_unit="m" name="some_measurement" notes="" src="d0_s0" to="3.5" to_unit="m" />
        <character is_modifier="false" modifier="shallowly to deeply; adaxially" name="architecture" src="d0_s0" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o18085" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
      </biological_entity>
      <biological_entity id="o18086" name="base" name_original="base" src="d0_s0" type="structure">
        <character constraint="from stem" constraintid="o18087" is_modifier="false" modifier="not strongly" name="fusion" src="d0_s0" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o18087" name="stem" name_original="stem" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>Blades broadly deltate, papery to leathery, sparsely to densely hairy abaxially, rarely glabrous.</text>
      <biological_entity id="o18088" name="blade" name_original="blades" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s1" value="deltate" value_original="deltate" />
        <character char_type="range_value" from="papery" name="texture" src="d0_s1" to="leathery" />
        <character is_modifier="false" modifier="sparsely to densely; abaxially" name="pubescence" src="d0_s1" value="hairy" value_original="hairy" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pinnae often opposite to subopposite [alternate];</text>
      <biological_entity id="o18089" name="pinna" name_original="pinnae" src="d0_s2" type="structure">
        <character char_type="range_value" from="opposite" modifier="often" name="arrangement" src="d0_s2" to="subopposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>proximal pinnae often prolonged basiscopically, each proximal pinna nearly equal to distal part of leaf in size and dissection (except in var. caudata).</text>
      <biological_entity constraint="proximal" id="o18090" name="pinna" name_original="pinnae" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often; basiscopically" name="length" src="d0_s3" value="prolonged" value_original="prolonged" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o18091" name="pinna" name_original="pinna" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="nearly" name="variability" src="d0_s3" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="distal" id="o18092" name="part" name_original="part" src="d0_s3" type="structure" />
      <biological_entity id="o18093" name="leaf" name_original="leaf" src="d0_s3" type="structure" />
      <biological_entity id="o18094" name="dissection" name_original="dissection" src="d0_s3" type="structure">
        <character is_modifier="true" name="character" src="d0_s3" value="size" value_original="size" />
      </biological_entity>
      <relation from="o18092" id="r3968" name="part_of" negation="false" src="d0_s3" to="o18093" />
      <relation from="o18092" id="r3969" name="in" negation="false" src="d0_s3" to="o18094" />
    </statement>
    <statement id="d0_s4">
      <text>Segments alternate, numerous.</text>
      <biological_entity id="o18095" name="segment" name_original="segments" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="quantity" src="d0_s4" value="numerous" value_original="numerous" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.S., Nfld. and Labr. (Nfld.), Ont., P.E.I., Que.; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., D.C., Del., Fla., Ga., Idaho, Ill., Ind., Iowa, Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., Mont., N.C., N.Dak., N.H., N.J., N.Mex., N.Y., Nev., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Va., Vt., W.Va., Wash., Wis., Wyo.; Almost worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Almost worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <other_name type="common_name">Bracken</other_name>
  <other_name type="common_name">fougère-aigle commune</other_name>
  <discussion>In accord with the most recent revision (R. M. Tryon 1941) of the genus, Pteridium is treated here as a single widespread species composed of two subspecies with 12 varieties. So treated, it is probably the most widespread species of all vascular plants, with the exception of a few annual weeds (F. H. Perring and B. G. Gardner 1976). The plants are generally aggressive, invading disturbed areas as weeds in pastures, cultivated fields, and roadsides. In Europe, it was harvested and burned to produce potash. Although croziers are eaten in many temperate cultures, bracken has been shown to contain thiaminase (and other compounds with mutagenic and carcinogenic properties).</discussion>
  <discussion>Disagreement exists among taxonomists regarding the rank that should be accorded to the taxa treated herein as varieties. In a survey of the genus, C. N. Page (1976) noted uniform chromosome numbers and flavonoid compositions of the varieties. D. B. Lellinger (1985) separated the genus into at least two species based on morphology, recognizing as species the subspecies of R. M. Tryon (1941). J. T. Mickel and J. M. Beitel (1988) reported sympatric occurrence in Mexico of three taxa that maintained consistent characteristics and only rarely produced plants with combined characteristics. They suggested that these three taxa should be considered as species that occasionally hybridize. P. J. Brownsey (1989) reported that two different brackens in Australia formed sterile hybrids and should be treated as species. Modern systematic studies are needed to evaluate the status and rank of the four North American varieties. As treated below, Pteridium aquilinum var. pubescens, var. latiusculum, and var. pseudocaudatum are in subsp. aquilinum, and var. caudatum is in subsp. caudatum (Linnaeus) Bonaparte.</discussion>
  <discussion>Varieties 12 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fertile ultimate segments only decurrent or more decurrent than surcurrent, mostly 1-2 mm wide; hairs on abaxial surface of blades abundant, straight, stiff, subappressed to spreading.</description>
      <determination>1a Pteridium aquilinum var. caudatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fertile ultimate segments adnate or equally decurrent and surcurrent, mostly 3-6 mm wide; hairs on abaxial surface of blades abundant to sparse, twisted and flexible, if abundant then lax, spreading.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pinnules at nearly 90º angle to costa; outer indusium pilose on margin and often on surface; hairs on abaxial surface of blades abundant, lax, and spreading.</description>
      <determination>1b Pteridium aquilinum var. pubescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pinnules at 45º -60º angle to costa; outer indusium glabrous; hairs on abaxial surface of blades sparse or blades nearly glabrous.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Terminal segments of pinnules 2-4 times longer than wide; segment margins and abaxial surface of blade midrib and costae shaggy.</description>
      <determination>1c Pteridium aquilinum var. latiusculum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Terminal segments of pinnules ca. 6-15 times longer than wide; segment margins and abaxial surface of blade midrib and costae sparsely pilose to glabrous.</description>
      <determination>1d Pteridium aquilinum var. pseudocaudatum</determination>
    </key_statement>
  </key>
</bio:treatment>