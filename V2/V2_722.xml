<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">lycopodiaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">lycopodium</taxon_name>
    <taxon_name authority="W. H. Wagner" date="1989" rank="species">hickeyi</taxon_name>
    <place_of_publication>
      <publication_title>Beitel, &amp; R. C. Moran, Amer. Fern J.</publication_title>
      <place_in_publication>79: 119–121. 1989</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lycopodiaceae;genus lycopodium;species hickeyi</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500761</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Lycopodium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">obscurum</taxon_name>
    <taxon_name authority="Hickey" date="unknown" rank="variety">isophyllum</taxon_name>
    <taxon_hierarchy>genus Lycopodium;species obscurum;variety isophyllum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Horizontal stems subterranean.</text>
      <biological_entity id="o4550" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="horizontal" value_original="horizontal" />
        <character is_modifier="false" name="location" src="d0_s0" value="subterranean" value_original="subterranean" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Upright shoots treelike, many branched, branchlets numerous and strongly differentiated;</text>
      <biological_entity id="o4551" name="shoot" name_original="shoots" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="upright" value_original="upright" />
        <character is_modifier="false" name="growth_form" src="d0_s1" value="arboreous" value_original="treelike" />
        <character is_modifier="false" name="quantity" src="d0_s1" value="many" value_original="many" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o4552" name="branchlet" name_original="branchlets" src="d0_s1" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s1" value="numerous" value_original="numerous" />
        <character is_modifier="false" modifier="strongly" name="variability" src="d0_s1" value="differentiated" value_original="differentiated" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>annual bud constrictions absent;</text>
      <biological_entity constraint="bud" id="o4553" name="constriction" name_original="constrictions" src="d0_s2" type="structure">
        <character is_modifier="true" name="duration" src="d0_s2" value="annual" value_original="annual" />
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>leaves on main axis below lateral branchlets tightly appressed, dark green, needlelike, 3.5–4.5 × 0.5–0.6 mm, soft.</text>
      <biological_entity id="o4554" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" notes="" src="d0_s3" value="dark green" value_original="dark green" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="needlelike" value_original="needlelike" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="length" src="d0_s3" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="0.6" to_unit="mm" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s3" value="soft" value_original="soft" />
      </biological_entity>
      <biological_entity constraint="main" id="o4555" name="axis" name_original="axis" src="d0_s3" type="structure" />
      <biological_entity id="o4556" name="branchlet" name_original="branchlets" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="below" name="position" src="d0_s3" value="lateral" value_original="lateral" />
        <character is_modifier="false" modifier="tightly" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
      </biological_entity>
      <relation from="o4554" id="r988" name="on" negation="false" src="d0_s3" to="o4555" />
      <relation from="o4555" id="r989" name="below lateral" negation="false" src="d0_s3" to="o4556" />
    </statement>
    <statement id="d0_s4">
      <text>Lateral branchlets round in cross-section, 4–7 mm diam.;</text>
      <biological_entity constraint="lateral" id="o4557" name="branchlet" name_original="branchlets" src="d0_s4" type="structure">
        <character constraint="in cross-section" constraintid="o4558" is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" notes="" src="d0_s4" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4558" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>annual bud constrictions inconspicuous;</text>
      <biological_entity constraint="bud" id="o4559" name="constriction" name_original="constrictions" src="d0_s5" type="structure">
        <character is_modifier="true" name="duration" src="d0_s5" value="annual" value_original="annual" />
        <character is_modifier="false" name="prominence" src="d0_s5" value="inconspicuous" value_original="inconspicuous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>leaves ascending, in 6 ranks, 1 on upperside, 4 lateral, and 1 on underside, equal in size, linear, widest in middle;</text>
      <biological_entity id="o4560" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="position" src="d0_s6" value="lateral" value_original="lateral" />
        <character constraint="on underside" constraintid="o4563" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="size" notes="" src="d0_s6" value="equal" value_original="equal" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
        <character constraint="in middle, leaves" constraintid="o4564, o4565" is_modifier="false" name="width" src="d0_s6" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o4561" name="rank" name_original="ranks" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="6" value_original="6" />
        <character constraint="on upperside" constraintid="o4562" name="quantity" src="d0_s6" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4562" name="upperside" name_original="upperside" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o4563" name="underside" name_original="underside" src="d0_s6" type="structure" />
      <biological_entity id="o4564" name="middle" name_original="middle" src="d0_s6" type="structure" />
      <biological_entity id="o4565" name="leaf" name_original="leaves" src="d0_s6" type="structure" />
      <relation from="o4560" id="r990" name="in" negation="false" src="d0_s6" to="o4561" />
    </statement>
    <statement id="d0_s7">
      <text>margins entire;</text>
      <biological_entity id="o4566" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex acuminate, lacking hair tip.</text>
      <biological_entity id="o4567" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acuminate" value_original="acuminate" />
      </biological_entity>
      <biological_entity constraint="hair" id="o4568" name="tip" name_original="tip" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Strobili sessile, 1–7 per upright shoot, 15–65 mm.</text>
      <biological_entity id="o4569" name="strobilus" name_original="strobili" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
        <character char_type="range_value" constraint="per shoot" constraintid="o4570" from="1" name="quantity" src="d0_s9" to="7" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="65" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4570" name="shoot" name_original="shoot" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="upright" value_original="upright" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Sporophylls 3–3.5 × 2–2.5 mm, apex long, gradually narrowing to tip.</text>
      <biological_entity id="o4571" name="sporophyll" name_original="sporophylls" src="d0_s10" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s10" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s10" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o4573" name="tip" name_original="tip" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>2n = 68.</text>
      <biological_entity id="o4572" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="long" value_original="long" />
        <character constraint="to tip" constraintid="o4573" is_modifier="false" modifier="gradually" name="width" src="d0_s10" value="narrowing" value_original="narrowing" />
      </biological_entity>
      <biological_entity constraint="2n" id="o4574" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="68" value_original="68" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mainly in hardwood forests and second-growth, shrubby habitats</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="hardwood forests" modifier="mainly in" />
        <character name="habitat" value="second-growth" />
        <character name="habitat" value="shrubby habitats" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que., Sask.; Conn., Ind., Ky., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5</number>
  <other_name type="common_name">Hickey's tree club-moss</other_name>
  <other_name type="common_name">lycopode de Hickey</other_name>
  <discussion>The range of Lycopodium hickeyi overlaps with that of L. obscurum and extends considerably north and west of that species. Although the arrangement of the leaf ranks is similar to that of L. obscurum, the leaf dimorphy and the ascending orientation and absence of twisting of the leaves are diagnostic. Where ranges of two or three species overlap, indivual species retain their identities, indicating that their critical differences have a genetic basis.</discussion>
  
</bio:treatment>