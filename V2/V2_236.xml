<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Adanson" date="1763" rank="genus">dryopteris</taxon_name>
    <taxon_name authority="(Kaulfuss) Maxon" date="1921" rank="species">arguta</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>11: 3. 1921</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus dryopteris;species arguta</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500589</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aspidium</taxon_name>
    <taxon_name authority="Kaulfuss" date="unknown" rank="species">argutum</taxon_name>
    <place_of_publication>
      <publication_title>Enum. Filic.,</publication_title>
      <place_in_publication>242. 1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aspidium;species argutum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Leaves monomorphic, green through winter, 25–90 × 8–30 cm.</text>
      <biological_entity id="o12768" name="leaf" name_original="leaves" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s0" value="monomorphic" value_original="monomorphic" />
        <character constraint="through winter" is_modifier="false" name="coloration" src="d0_s0" value="green" value_original="green" />
        <character char_type="range_value" from="25" from_unit="cm" name="length" src="d0_s0" to="90" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="cm" name="width" src="d0_s0" to="30" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Petiole 1/4–1/3 length of leaf, scaly at least at base;</text>
      <biological_entity id="o12769" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character char_type="range_value" from="1/4 length of leaf" name="length" src="d0_s1" to="1/3 length of leaf" />
        <character constraint="at base" constraintid="o12770" is_modifier="false" name="architecture_or_pubescence" src="d0_s1" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity id="o12770" name="base" name_original="base" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>scales scattered, light-brown.</text>
      <biological_entity id="o12771" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s2" value="scattered" value_original="scattered" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="light-brown" value_original="light-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Blade green to yellow-green, ovatelanceolate, pinnate-pinnatifid to 2-pinnate at base, herbaceous, glandular.</text>
      <biological_entity id="o12772" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s3" to="yellow-green" />
        <character is_modifier="false" name="shape" src="d0_s3" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character char_type="range_value" constraint="at base" constraintid="o12773" from="pinnate-pinnatifid" name="shape" src="d0_s3" to="2-pinnate" />
        <character is_modifier="false" name="growth_form_or_texture" notes="" src="d0_s3" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s3" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o12773" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Pinnae ± in plane of blade, lance-oblong;</text>
      <biological_entity id="o12774" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="lance-oblong" value_original="lance-oblong" />
      </biological_entity>
      <biological_entity id="o12775" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <relation from="o12774" id="r2779" modifier="in plane" name="part_of" negation="false" src="d0_s4" to="o12775" />
    </statement>
    <statement id="d0_s5">
      <text>basal pinnae deltate, not much reduced, basal pinnules ± same length as adjacent pinnules, basal basiscopic pinnule and basal acroscopic pinnule ± equal, pinnule margins serrate with spreading, spinelike teeth.</text>
      <biological_entity constraint="basal" id="o12776" name="pinna" name_original="pinnae" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="deltate" value_original="deltate" />
        <character is_modifier="false" modifier="not much" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity constraint="basal" id="o12777" name="pinnule" name_original="pinnules" src="d0_s5" type="structure" />
      <biological_entity id="o12778" name="pinnule" name_original="pinnules" src="d0_s5" type="structure">
        <character is_modifier="true" name="length" src="d0_s5" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="basiscopic" value_original="basiscopic" />
      </biological_entity>
      <biological_entity id="o12779" name="pinnule" name_original="pinnule" src="d0_s5" type="structure">
        <character is_modifier="true" name="length" src="d0_s5" value="adjacent" value_original="adjacent" />
        <character is_modifier="false" name="orientation" src="d0_s5" value="basiscopic" value_original="basiscopic" />
      </biological_entity>
      <biological_entity constraint="basal" id="o12780" name="pinnule" name_original="pinnules" src="d0_s5" type="structure" />
      <biological_entity constraint="basal" id="o12781" name="pinnule" name_original="pinnule" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" modifier="more or less" name="variability" src="d0_s5" value="equal" value_original="equal" />
      </biological_entity>
      <biological_entity constraint="pinnule" id="o12782" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character constraint="with teeth" constraintid="o12783" is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
      <biological_entity id="o12783" name="tooth" name_original="teeth" src="d0_s5" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s5" value="spreading" value_original="spreading" />
        <character is_modifier="true" name="shape" src="d0_s5" value="spinelike" value_original="spinelike" />
      </biological_entity>
      <relation from="o12777" id="r2780" name="as" negation="false" src="d0_s5" to="o12778" />
      <relation from="o12777" id="r2781" name="as" negation="false" src="d0_s5" to="o12779" />
      <relation from="o12777" id="r2782" name="as" negation="false" src="d0_s5" to="o12781" />
    </statement>
    <statement id="d0_s6">
      <text>Sori midway between midvein and margin of segments.</text>
      <biological_entity id="o12784" name="sorus" name_original="sori" src="d0_s6" type="structure">
        <character constraint="between midvein, margin" constraintid="o12785, o12786" is_modifier="false" name="position" src="d0_s6" value="midway" value_original="midway" />
      </biological_entity>
      <biological_entity id="o12785" name="midvein" name_original="midvein" src="d0_s6" type="structure" />
      <biological_entity id="o12786" name="margin" name_original="margin" src="d0_s6" type="structure" />
      <biological_entity id="o12787" name="segment" name_original="segments" src="d0_s6" type="structure" />
      <relation from="o12785" id="r2783" name="part_of" negation="false" src="d0_s6" to="o12787" />
      <relation from="o12786" id="r2784" name="part_of" negation="false" src="d0_s6" to="o12787" />
    </statement>
    <statement id="d0_s7">
      <text>Indusia lacking glands.</text>
      <biological_entity id="o12789" name="gland" name_original="glands" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>2n = 82.</text>
      <biological_entity id="o12788" name="indusium" name_original="indusia" src="d0_s7" type="structure" />
      <biological_entity constraint="2n" id="o12790" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="82" value_original="82" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Shaded slopes and open woods</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="shaded slopes" />
        <character name="habitat" value="open woods" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Ariz., Calif., Oreg., Wash.; Mexico in Baja California.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico in Baja California" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Marginal wood fern</other_name>
  <other_name type="common_name">coastal wood fern</other_name>
  <discussion>Dryopteris arguta is somewhat variable. It has been suggested that more than one taxon is involved. No hybrids involving D. arguta are known.</discussion>
  
</bio:treatment>