<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Willkomm in Willkomm &amp; Lange" date="unknown" rank="family">selaginellaceae</taxon_name>
    <taxon_name authority="Palisot de Beauvois" date="1805" rank="genus">selaginella</taxon_name>
    <taxon_name authority="Jermy" date="1986" rank="subgenus">tetragonostachys</taxon_name>
    <taxon_name authority="D. C. Eaton ex L. Underwood" date="1898" rank="species">mutica</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 128. 1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family selaginellaceae;genus selaginella;subgenus tetragonostachys;species mutica</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501234</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants on rock or terrestrial, forming loose mats.</text>
      <biological_entity id="o1500" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1501" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
      </biological_entity>
      <relation from="o1500" id="r347" name="forming" negation="false" src="d0_s0" to="o1501" />
    </statement>
    <statement id="d0_s1">
      <text>Stems radially symmetric, long to short-creeping, not readily fragmenting, ± regularly forked, without budlike arrested branches, tips straight;</text>
      <biological_entity id="o1502" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="radially" name="architecture_or_shape" src="d0_s1" value="symmetric" value_original="symmetric" />
        <character is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="short-creeping" value_original="short-creeping" />
        <character is_modifier="false" modifier="not readily; readily; more or less regularly" name="shape" src="d0_s1" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity id="o1503" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture_or_shape" src="d0_s1" value="budlike" value_original="budlike" />
      </biological_entity>
      <biological_entity id="o1504" name="tip" name_original="tips" src="d0_s1" type="structure">
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o1502" id="r348" name="without" negation="false" src="d0_s1" to="o1503" />
    </statement>
    <statement id="d0_s2">
      <text>main-stem indeterminate, lateral branches determinate, 1–2-forked.</text>
      <biological_entity id="o1505" name="main-stem" name_original="main-stem" src="d0_s2" type="structure">
        <character is_modifier="false" name="development" src="d0_s2" value="indeterminate" value_original="indeterminate" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o1506" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="false" name="development" src="d0_s2" value="determinate" value_original="determinate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="1-2-forked" value_original="1-2-forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Rhizophores borne on upperside of stems, throughout stem length, 0.13–0.23 mm diam.</text>
      <biological_entity id="o1507" name="rhizophore" name_original="rhizophores" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.13" from_unit="mm" name="diameter" notes="" src="d0_s3" to="0.23" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1508" name="upperside" name_original="upperside" src="d0_s3" type="structure" />
      <biological_entity id="o1509" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity id="o1510" name="stem" name_original="stem" src="d0_s3" type="structure" />
      <relation from="o1507" id="r349" name="borne on" negation="false" src="d0_s3" to="o1508" />
      <relation from="o1507" id="r350" name="borne on" negation="false" src="d0_s3" to="o1509" />
      <relation from="o1507" id="r351" name="throughout" negation="false" src="d0_s3" to="o1510" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves monomorphic, in ± alternate pseudowhorls of 3, tightly appressed, ascending, green, lanceolate to linear-lanceolate or lanceolate-elliptic, 1–2 × 0.45–0.6 mm;</text>
      <biological_entity id="o1511" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="tightly" name="fixation_or_orientation" notes="" src="d0_s4" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="linear-lanceolate or lanceolate-elliptic" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s4" to="2" to_unit="mm" />
        <character char_type="range_value" from="0.45" from_unit="mm" name="width" src="d0_s4" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o1512" name="pseudowhorl" name_original="pseudowhorls" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="more or less" name="arrangement" src="d0_s4" value="alternate" value_original="alternate" />
        <character name="quantity" src="d0_s4" value="3" value_original="3" />
      </biological_entity>
      <relation from="o1511" id="r352" name="in" negation="false" src="d0_s4" to="o1512" />
    </statement>
    <statement id="d0_s5">
      <text>abaxial ridges well defined;</text>
      <biological_entity constraint="abaxial" id="o1513" name="ridge" name_original="ridges" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="well" name="prominence" src="d0_s5" value="defined" value_original="defined" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>base rounded and adnate, sometimes slightly decurrent, pubescent or glabrous;</text>
      <biological_entity id="o1514" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="fusion" src="d0_s6" value="adnate" value_original="adnate" />
        <character is_modifier="false" modifier="sometimes slightly" name="shape" src="d0_s6" value="decurrent" value_original="decurrent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>margins ciliate to denticulate, cilia transparent, spreading or ascending, 0.03–0.17 mm;</text>
      <biological_entity id="o1515" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character char_type="range_value" from="ciliate" name="shape" src="d0_s7" to="denticulate" />
      </biological_entity>
      <biological_entity id="o1516" name="cilium" name_original="cilia" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s7" value="transparent" value_original="transparent" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="orientation" src="d0_s7" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0.03" from_unit="mm" name="some_measurement" src="d0_s7" to="0.17" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>apex keeled, obtuse or slightly attenuate, nearly truncate in profile, blunt to short-bristled;</text>
      <biological_entity id="o1517" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="shape" src="d0_s8" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s8" value="attenuate" value_original="attenuate" />
        <character is_modifier="false" modifier="nearly; in profile" name="architecture_or_shape" src="d0_s8" value="truncate" value_original="truncate" />
        <character char_type="range_value" from="blunt" name="shape" src="d0_s8" to="short-bristled" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>bristle transparent to greenish transparent or whitish, smooth, 0.06–0.45 mm.</text>
      <biological_entity id="o1518" name="bristle" name_original="bristle" src="d0_s9" type="structure">
        <character char_type="range_value" from="transparent" name="coloration" src="d0_s9" to="greenish transparent or whitish" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s9" value="smooth" value_original="smooth" />
        <character char_type="range_value" from="0.06" from_unit="mm" name="some_measurement" src="d0_s9" to="0.45" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Strobili solitary, (0.6–) 1–3 cm;</text>
      <biological_entity id="o1519" name="strobilus" name_original="strobili" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s10" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_some_measurement" src="d0_s10" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s10" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sporophylls ovatelanceolate, ovate-elliptic, or deltate-ovate, abaxial ridges well defined, base glabrous, margins ciliate to denticulate, apex strongly to slightly keeled, short-bristled to blunt.</text>
      <biological_entity id="o1520" name="sporophyll" name_original="sporophylls" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="ovatelanceolate" value_original="ovatelanceolate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="deltate-ovate" value_original="deltate-ovate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="ovate-elliptic" value_original="ovate-elliptic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="deltate-ovate" value_original="deltate-ovate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1521" name="ridge" name_original="ridges" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="well" name="prominence" src="d0_s11" value="defined" value_original="defined" />
      </biological_entity>
      <biological_entity id="o1522" name="base" name_original="base" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o1523" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character char_type="range_value" from="ciliate" name="shape" src="d0_s11" to="denticulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>2n = 18.</text>
      <biological_entity id="o1524" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
        <character char_type="range_value" from="short-bristled" name="shape" src="d0_s11" to="blunt" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1525" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="18" value_original="18" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Colo., N.Mex., Tex., Utah, Wyo.; only in the flora.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="only in the flora" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17</number>
  <discussion>Selaginella mutica, S. underwoodii (R. M. Tryon 1955; C. A. Weatherby 1943), and S. wallacei all have similar patterns of variation. Study is needed to assess to what degree such variability is caused by environmental or genetic factors. Within S. mutica, two rather distinct, morphologic extremes are recognized here as varieties. Many specimens having leaves with spreading, long, marginal cilia and a short, broken, apical bristle have been considered intermediate between the two varieties, but they belong in S. mutica var. mutica.</discussion>
  <discussion>Selaginella mutica may be one of the parent species of the putative hybrid species S. × neomexicana (see discussion). Selaginella mutica is often found growing in the same habitat with S. underwoodii, S. × neomexicana, and S. weatherbiana. According to R. M. Tryon (1955), where the two grow together, S. mutica mats gradually entirely replace mats of S. underwoodii over time. Selaginella mutica is sometimes confused with S. viridissima.</discussion>
  <discussion>Varieties 2.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Margins of sporophylls usually long-ciliate, seldom denticulate, cilia spreading; apex of leaves with or without bristle, 0.03–0.06 mm; leaf margins long- ciliate, rarely denticulate, cilia spreading.</description>
      <determination>17a Selaginella mutica var. mutica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Margins of sporophylls mostly very short-ciliate to denticulate, cilia and teeth ascending; apex of leaves with bristle 0.2–0.45 mm; leaf margins short-ciliate to denticulate, cilia and teeth ascending.</description>
      <determination>17b Selaginella mutica var. limitanea</determination>
    </key_statement>
  </key>
</bio:treatment>