<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">pinus</taxon_name>
    <taxon_name authority="D. K. Bailey" date="1970" rank="species">longaeva</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>57: 243. 1970</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus pinus;species longaeva</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500942</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pinus</taxon_name>
    <taxon_name authority="Engelmann" date="unknown" rank="species">aristata</taxon_name>
    <taxon_name authority="(D.K.Bailey) Little" date="unknown" rank="variety">longaeva</taxon_name>
    <taxon_hierarchy>genus Pinus;species aristata;variety longaeva;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 16m;</text>
      <biological_entity id="o4775" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="16" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 2m diam., strongly tapering;</text>
      <biological_entity id="o4776" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="2" to_unit="m" />
        <character is_modifier="false" modifier="strongly" name="shape" src="d0_s1" value="tapering" value_original="tapering" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>crown rounded, flattened (sheared), or irregular.</text>
      <biological_entity id="o4777" name="crown" name_original="crown" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
        <character is_modifier="false" name="architecture_or_course" src="d0_s2" value="irregular" value_original="irregular" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark redbrown, shallowly to deeply fissured with thick, scaly, irregular, blocky ridges.</text>
      <biological_entity id="o4778" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="redbrown" value_original="redbrown" />
        <character constraint="with ridges" constraintid="o4779" is_modifier="false" modifier="shallowly to deeply" name="relief" src="d0_s3" value="fissured" value_original="fissured" />
      </biological_entity>
      <biological_entity id="o4779" name="ridge" name_original="ridges" src="d0_s3" type="structure">
        <character is_modifier="true" name="width" src="d0_s3" value="thick" value_original="thick" />
        <character is_modifier="true" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
        <character is_modifier="true" name="architecture_or_course" src="d0_s3" value="irregular" value_original="irregular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Branches contorted, pendent;</text>
      <biological_entity id="o4780" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s4" value="contorted" value_original="contorted" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="pendent" value_original="pendent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>twigs pale redbrown, aging gray to yellow-gray, puberulent, young branches resembling long bottlebrushes because of persistent leaves.</text>
      <biological_entity id="o4781" name="twig" name_original="twigs" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale redbrown" value_original="pale redbrown" />
        <character is_modifier="false" name="life_cycle" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="gray to yellow-gray" value_original="gray to yellow-gray" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o4782" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s5" value="young" value_original="young" />
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="long" value_original="long" />
        <character constraint="because-of leaves" constraintid="o4783" is_modifier="false" name="shape" src="d0_s5" value="bottlebrushes" value_original="bottlebrushes" />
      </biological_entity>
      <biological_entity id="o4783" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="true" name="duration" src="d0_s5" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Buds ovoid-acuminate, pale redbrown, ca. 1cm, resinous.</text>
      <biological_entity id="o4784" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ovoid-acuminate" value_original="ovoid-acuminate" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="pale redbrown" value_original="pale redbrown" />
        <character name="some_measurement" src="d0_s6" unit="cm" value="1" value_original="1" />
        <character is_modifier="false" name="coating" src="d0_s6" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves mostly 5 per fascicle, upcurved, persisting 10–30 years, 1.5–3.5cm × 0.8–1.2mm, mostly connivent, deep yellow-green, with few resin splotches but often scurfy with pale scales, abaxial surface without median groove but with 2 subepidermal but evident resin bands, adaxial surfaces conspicuously whitened with stomates, margins entire or remotely and finely serrulate distally, apex bluntly acute to short-acuminate;</text>
      <biological_entity id="o4785" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character constraint="per fascicle" constraintid="o4786" name="quantity" src="d0_s7" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" notes="" src="d0_s7" value="upcurved" value_original="upcurved" />
        <character is_modifier="false" name="duration" src="d0_s7" value="persisting" value_original="persisting" />
        <character char_type="range_value" from="10" name="quantity" src="d0_s7" to="30" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s7" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="width" src="d0_s7" to="1.2" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s7" value="connivent" value_original="connivent" />
        <character is_modifier="false" name="depth" src="d0_s7" value="deep" value_original="deep" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="yellow-green" value_original="yellow-green" />
      </biological_entity>
      <biological_entity id="o4786" name="fascicle" name_original="fascicle" src="d0_s7" type="structure" />
      <biological_entity constraint="resin" id="o4787" name="splotch" name_original="splotches" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="few" value_original="few" />
        <character constraint="with scales" constraintid="o4788" is_modifier="false" modifier="often" name="pubescence" src="d0_s7" value="scurfy" value_original="scurfy" />
      </biological_entity>
      <biological_entity id="o4788" name="scale" name_original="scales" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o4789" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity constraint="median" id="o4790" name="groove" name_original="groove" src="d0_s7" type="structure" />
      <biological_entity constraint="resin" id="o4791" name="band" name_original="bands" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="position" src="d0_s7" value="subepidermal" value_original="subepidermal" />
        <character is_modifier="true" name="prominence" src="d0_s7" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o4792" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character constraint="with stomates" constraintid="o4793" is_modifier="false" modifier="conspicuously" name="coloration" src="d0_s7" value="whitened" value_original="whitened" />
      </biological_entity>
      <biological_entity id="o4793" name="stomate" name_original="stomates" src="d0_s7" type="structure" />
      <biological_entity id="o4794" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s7" value="remotely" value_original="remotely" />
        <character is_modifier="false" modifier="finely; distally" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o4795" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="bluntly acute" name="shape" src="d0_s7" to="short-acuminate" />
      </biological_entity>
      <relation from="o4785" id="r1037" name="with" negation="false" src="d0_s7" to="o4787" />
      <relation from="o4789" id="r1038" name="without" negation="false" src="d0_s7" to="o4790" />
      <relation from="o4790" id="r1039" name="with" negation="false" src="d0_s7" to="o4791" />
    </statement>
    <statement id="d0_s8">
      <text>sheath ca. 1cm, soon forming rosette, shed early.</text>
      <biological_entity id="o4796" name="sheath" name_original="sheath" src="d0_s8" type="structure">
        <character name="some_measurement" src="d0_s8" unit="cm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o4797" name="rosette" name_original="rosette" src="d0_s8" type="structure" />
      <relation from="o4796" id="r1040" modifier="soon" name="forming" negation="false" src="d0_s8" to="o4797" />
    </statement>
    <statement id="d0_s9">
      <text>Pollen cones cylindro-ellipsoid, 7–10mm, purple-red.</text>
      <biological_entity constraint="pollen" id="o4798" name="cone" name_original="cones" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="cylindro-ellipsoid" value_original="cylindro-ellipsoid" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purple-red" value_original="purple-red" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seed-cones maturing in 2 years, shedding seeds and falling soon thereafter, spreading, symmetric, lance-cylindric with rounded base before opening, lance-cylindric to narrowly ovoid when open, 6–9.5cm, purple, aging redbrown, nearly sessile;</text>
      <biological_entity id="o4799" name="cone-seed" name_original="seed-cones" src="d0_s10" type="structure">
        <character constraint="in years" constraintid="o4800" is_modifier="false" name="life_cycle" src="d0_s10" value="maturing" value_original="maturing" />
        <character is_modifier="false" modifier="soon thereafter; thereafter" name="life_cycle" src="d0_s10" value="falling" value_original="falling" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character constraint="with base" constraintid="o4802" is_modifier="false" name="shape" src="d0_s10" value="lance-cylindric" value_original="lance-cylindric" />
        <character char_type="range_value" from="lance-cylindric" modifier="when open" name="shape" notes="" src="d0_s10" to="narrowly ovoid" />
        <character char_type="range_value" from="6" from_unit="cm" name="distance" src="d0_s10" to="9.5" to_unit="cm" />
        <character is_modifier="false" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="false" name="life_cycle" src="d0_s10" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="redbrown" value_original="redbrown" />
        <character is_modifier="false" modifier="nearly" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o4800" name="year" name_original="years" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o4801" name="seed" name_original="seeds" src="d0_s10" type="structure" />
      <biological_entity id="o4802" name="base" name_original="base" src="d0_s10" type="structure">
        <character is_modifier="true" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o4803" name="opening" name_original="opening" src="d0_s10" type="structure" />
      <relation from="o4799" id="r1041" name="shedding" negation="false" src="d0_s10" to="o4801" />
      <relation from="o4802" id="r1042" name="before" negation="false" src="d0_s10" to="o4803" />
    </statement>
    <statement id="d0_s11">
      <text>apophyses much thickened, sharply keeled;</text>
      <biological_entity id="o4804" name="apophysis" name_original="apophyses" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="much" name="size_or_width" src="d0_s11" value="thickened" value_original="thickened" />
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s11" value="keeled" value_original="keeled" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>umbo central, raised on low buttress, truncate to umbilicate, abruptly narrowed to slender but stiff, variable prickle 1–6mm, resin exudate pale.</text>
      <biological_entity id="o4805" name="umbo" name_original="umbo" src="d0_s12" type="structure">
        <character is_modifier="false" name="position" src="d0_s12" value="central" value_original="central" />
        <character char_type="range_value" from="truncate" name="shape" src="d0_s12" to="umbilicate" />
        <character is_modifier="false" modifier="abruptly" name="shape" src="d0_s12" value="narrowed" value_original="narrowed" />
        <character is_modifier="false" name="size" src="d0_s12" value="slender" value_original="slender" />
        <character is_modifier="false" name="fragility" src="d0_s12" value="stiff" value_original="stiff" />
      </biological_entity>
      <biological_entity id="o4806" name="buttress" name_original="buttress" src="d0_s12" type="structure">
        <character is_modifier="true" name="position" src="d0_s12" value="low" value_original="low" />
      </biological_entity>
      <biological_entity id="o4807" name="prickle" name_original="prickle" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="variable" value_original="variable" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="resin" id="o4808" name="exudate" name_original="exudate" src="d0_s12" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s12" value="pale" value_original="pale" />
      </biological_entity>
      <relation from="o4805" id="r1043" name="raised on" negation="false" src="d0_s12" to="o4806" />
    </statement>
    <statement id="d0_s13">
      <text>Seeds ellipsoid-obovoid;</text>
      <biological_entity id="o4809" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ellipsoid-obovoid" value_original="ellipsoid-obovoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>body 5–8mm, pale-brown, mottled with dark red;</text>
      <biological_entity id="o4810" name="body" name_original="body" src="d0_s14" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s14" to="8" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="pale-brown" value_original="pale-brown" />
        <character is_modifier="false" name="coloration" src="d0_s14" value="mottled with dark red" value_original="mottled with dark red" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>wing 10–12mm.</text>
      <biological_entity id="o4811" name="wing" name_original="wing" src="d0_s15" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s15" to="12" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Subalpine and alpine</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="subalpine" />
        <character name="habitat" value="alpine" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1700–3400m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3400" to_unit="m" from="1700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Nev., Utah.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>13</number>
  <other_name type="common_name">Intermountain bristlecone pine</other_name>
  <discussion>Pinus longaeva is considered by dendrochronologists to be the longest-lived tree. One tree was estimated to be 5000 years old.</discussion>
  
</bio:treatment>