<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">pinus</taxon_name>
    <taxon_name authority="Douglas ex D. Don in Lambert" date="1832" rank="species">monticola</taxon_name>
    <place_of_publication>
      <publication_title>in Lambert,Descr. Pinus [ed. 3]</publication_title>
      <place_in_publication>2: unnumbered page between 144 and 145. 1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus pinus;species monticola</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500944</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Strobus</taxon_name>
    <taxon_name authority="(Douglas ex D. Don) Rydberg" date="unknown" rank="species">monticola</taxon_name>
    <taxon_hierarchy>genus Strobus;species monticola;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 70m;</text>
      <biological_entity id="o17517" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="70" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 2.5m diam., straight;</text>
      <biological_entity id="o17518" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="2.5" to_unit="m" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>crown narrowly conic, becoming broad and flattened.</text>
      <biological_entity id="o17519" name="crown" name_original="crown" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="conic" value_original="conic" />
        <character is_modifier="false" modifier="becoming" name="width" src="d0_s2" value="broad" value_original="broad" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark gray, distinctly platy, plates scaly.</text>
      <biological_entity id="o17520" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="gray" value_original="gray" />
      </biological_entity>
      <biological_entity id="o17521" name="plate" name_original="plates" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="distinctly" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Branches nearly whorled, spreading-ascending;</text>
      <biological_entity id="o17522" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s4" value="whorled" value_original="whorled" />
        <character is_modifier="false" name="orientation" src="d0_s4" value="spreading-ascending" value_original="spreading-ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>twigs slender, pale redbrown, rusty puberulent and slightly glandular (rarely glabrous), aging purple-brown or gray, smooth.</text>
      <biological_entity id="o17523" name="twig" name_original="twigs" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="slender" value_original="slender" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="pale redbrown" value_original="pale redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="rusty" value_original="rusty" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
        <character is_modifier="false" modifier="slightly" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="aging" value_original="aging" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="purple-brown" value_original="purple-brown" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="gray" value_original="gray" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s5" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Buds ellipsoid or cylindric, rust-colored, 0.4–0.5cm, slightly resinous.</text>
      <biological_entity id="o17524" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="ellipsoid" value_original="ellipsoid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="rust-colored" value_original="rust-colored" />
        <character char_type="range_value" from="0.4" from_unit="cm" name="some_measurement" src="d0_s6" to="0.5" to_unit="cm" />
        <character is_modifier="false" modifier="slightly" name="coating" src="d0_s6" value="resinous" value_original="resinous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves 5 per fascicle, spreading to ascending, persisting 3–4 years, 4–10cm × 0.7–1mm, straight, slightly twisted, pliant, blue-green, abaxial surface without evident stomatal lines, adaxial surfaces with evident stomatal lines, margins finely serrulate, apex broadly to narrowly acute;</text>
      <biological_entity id="o17525" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character constraint="per fascicle" constraintid="o17526" name="quantity" src="d0_s7" value="5" value_original="5" />
        <character char_type="range_value" from="spreading" name="orientation" notes="" src="d0_s7" to="ascending" />
        <character is_modifier="false" name="duration" src="d0_s7" value="persisting" value_original="persisting" />
        <character char_type="range_value" from="3" name="quantity" src="d0_s7" to="4" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s7" to="10" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s7" to="1" to_unit="mm" />
        <character is_modifier="false" name="course" src="d0_s7" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="architecture" src="d0_s7" value="twisted" value_original="twisted" />
        <character is_modifier="false" name="fragility_or_texture" src="d0_s7" value="pliant" value_original="pliant" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="blue-green" value_original="blue-green" />
      </biological_entity>
      <biological_entity id="o17526" name="fascicle" name_original="fascicle" src="d0_s7" type="structure" />
      <biological_entity constraint="abaxial" id="o17527" name="surface" name_original="surface" src="d0_s7" type="structure" />
      <biological_entity constraint="stomatal" id="o17528" name="line" name_original="lines" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o17529" name="surface" name_original="surfaces" src="d0_s7" type="structure" />
      <biological_entity id="o17530" name="stomatal" name_original="stomatal" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o17531" name="line" name_original="lines" src="d0_s7" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s7" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o17532" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="finely" name="architecture_or_shape" src="d0_s7" value="serrulate" value_original="serrulate" />
      </biological_entity>
      <biological_entity id="o17533" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="broadly to narrowly" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o17527" id="r3841" name="without" negation="false" src="d0_s7" to="o17528" />
      <relation from="o17529" id="r3842" name="with" negation="false" src="d0_s7" to="o17530" />
      <relation from="o17529" id="r3843" name="with" negation="false" src="d0_s7" to="o17531" />
    </statement>
    <statement id="d0_s8">
      <text>sheath 1–1.5cm, shed early.</text>
      <biological_entity id="o17534" name="sheath" name_original="sheath" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s8" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pollen cones ellipsoid, 10–15mm, yellow.</text>
      <biological_entity constraint="pollen" id="o17535" name="cone" name_original="cones" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ellipsoid" value_original="ellipsoid" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s9" to="15" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="yellow" value_original="yellow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seed-cones maturing in 2 years, shedding seeds and falling soon thereafter, clustered, pendent, symmetric, lance-cylindric to ellipsoid-cylindric before opening, broadly lanceoloid to ellipsoid-cylindric when open, 10–25cm, creamy brown to yellowish, without purple or gray tints, resinous, stalks to 2cm;</text>
      <biological_entity id="o17536" name="cone-seed" name_original="seed-cones" src="d0_s10" type="structure">
        <character constraint="in years" constraintid="o17537" is_modifier="false" name="life_cycle" src="d0_s10" value="maturing" value_original="maturing" />
        <character is_modifier="false" modifier="soon thereafter; thereafter" name="life_cycle" src="d0_s10" value="falling" value_original="falling" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s10" value="clustered" value_original="clustered" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="shape" src="d0_s10" value="symmetric" value_original="symmetric" />
        <character char_type="range_value" constraint="before opening" constraintid="o17539" from="lance-cylindric" name="shape" src="d0_s10" to="ellipsoid-cylindric" />
        <character char_type="range_value" from="broadly lanceoloid" modifier="when open" name="shape" notes="" src="d0_s10" to="ellipsoid-cylindric" />
        <character char_type="range_value" from="10" from_unit="cm" name="distance" src="d0_s10" to="25" to_unit="cm" />
        <character char_type="range_value" from="creamy brown" name="coloration" src="d0_s10" to="yellowish" />
      </biological_entity>
      <biological_entity id="o17537" name="year" name_original="years" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o17538" name="seed" name_original="seeds" src="d0_s10" type="structure" />
      <biological_entity id="o17539" name="opening" name_original="opening" src="d0_s10" type="structure" />
      <biological_entity id="o17540" name="stalk" name_original="stalks" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s10" value="purple" value_original="purple" />
        <character is_modifier="true" name="coloration" src="d0_s10" value="gray tints" value_original="gray tints" />
        <character is_modifier="true" name="coating" src="d0_s10" value="resinous" value_original="resinous" />
        <character name="some_measurement" src="d0_s10" unit="cm" value="2" value_original="2" />
      </biological_entity>
      <relation from="o17536" id="r3844" name="shedding" negation="false" src="d0_s10" to="o17538" />
      <relation from="o17536" id="r3845" name="without" negation="false" src="d0_s10" to="o17540" />
    </statement>
    <statement id="d0_s11">
      <text>umbo terminal, depressed.</text>
      <biological_entity id="o17541" name="umbo" name_original="umbo" src="d0_s11" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s11" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s11" value="depressed" value_original="depressed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Seeds compressed, broadly obovoid-deltoid;</text>
      <biological_entity id="o17542" name="seed" name_original="seeds" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="compressed" value_original="compressed" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s12" value="obovoid-deltoid" value_original="obovoid-deltoid" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>body 5–7mm, redbrown;</text>
      <biological_entity id="o17543" name="body" name_original="body" src="d0_s13" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s13" to="7" to_unit="mm" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="redbrown" value_original="redbrown" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>wing 2–2.5cm.</text>
    </statement>
    <statement id="d0_s15">
      <text>2n =24.</text>
      <biological_entity id="o17544" name="wing" name_original="wing" src="d0_s14" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s14" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o17545" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Montane moist forests, lowland fog forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="montane moist forests" />
        <character name="habitat" value="lowland fog forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–3000m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Calif., Idaho, Mont., Nev., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Western white pine</other_name>
  <other_name type="common_name">pin argenté</other_name>
  <discussion>Pinus monticola is the most important western source for matchwood. Its wood lacks the sugary exudates seen in P. lambertiana.</discussion>
  <discussion>Western white pine (Pinus monticola) is the state tree of Idaho.</discussion>
  
</bio:treatment>