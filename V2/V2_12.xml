<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Ching ex Pichi Sermolli" date="unknown" rank="family">thelypteridaceae</taxon_name>
    <taxon_name authority="Schmidel" date="1763" rank="genus">thelypteris</taxon_name>
    <taxon_name authority="(C. Presl) Duek" date="1971" rank="subgenus">Goniopteris</taxon_name>
    <taxon_name authority="(Poeppig ex Sprengel) C. V. Morton" date="1951" rank="species">sclerophylla</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>41: 87. 1951</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family thelypteridaceae;genus thelypteris;subgenus goniopteris;species sclerophylla;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233501302</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aspidium</taxon_name>
    <taxon_name authority="Poeppig ex Sprengel" date="unknown" rank="species">sclerophyllum</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Veg.</publication_title>
      <place_in_publication>4(1): 99. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Aspidium;species sclerophyllum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Dryopteris</taxon_name>
    <taxon_name authority="(Poeppig ex Sprengel) C. Christensen" date="unknown" rank="species">sclerophylla</taxon_name>
    <taxon_hierarchy>genus Dryopteris;species sclerophylla;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Goniopteris</taxon_name>
    <taxon_name authority="(Poeppig ex Sprengel) Wherry" date="unknown" rank="species">sclerophylla</taxon_name>
    <taxon_hierarchy>genus Goniopteris;species sclerophylla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems short-creeping to suberect, 5–8 mm diam.</text>
      <biological_entity id="o14766" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="short-creeping" name="orientation" src="d0_s0" to="suberect" />
        <character char_type="range_value" from="5" from_unit="mm" name="diameter" src="d0_s0" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves monomorphic, evergreen, clustered, 20–55 (–80) cm.</text>
      <biological_entity id="o14767" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="duration" src="d0_s1" value="evergreen" value_original="evergreen" />
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s1" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="55" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="80" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" src="d0_s1" to="55" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Petiole straw-colored, 5–18 (–25) cm × 1–3 mm, at base with brown, lanceolate, stellate-hairy scales.</text>
      <biological_entity id="o14768" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="straw-colored" value_original="straw-colored" />
        <character char_type="range_value" from="18" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="18" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14769" name="base" name_original="base" src="d0_s2" type="structure" />
      <biological_entity id="o14770" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s2" value="brown" value_original="brown" />
        <character is_modifier="true" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="true" name="pubescence" src="d0_s2" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
      <relation from="o14768" id="r3235" name="at" negation="false" src="d0_s2" to="o14769" />
      <relation from="o14769" id="r3236" name="with" negation="false" src="d0_s2" to="o14770" />
    </statement>
    <statement id="d0_s3">
      <text>Blade 15–40 (–55) × 5–10 cm, proximal 2–5 (–10) pairs of pinnae gradually reduced, blade gradually narrowed distally to a pinnatifid apex.</text>
      <biological_entity id="o14771" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="55" to_unit="cm" />
        <character char_type="range_value" from="15" from_unit="cm" name="length" src="d0_s3" to="40" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s3" to="10" to_unit="cm" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="5" from_inclusive="false" name="atypical_quantity" src="d0_s3" to="10" />
        <character char_type="range_value" constraint="of pinnae" constraintid="o14772" from="2" name="quantity" src="d0_s3" to="5" />
      </biological_entity>
      <biological_entity id="o14772" name="pinna" name_original="pinnae" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s3" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o14773" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character constraint="to apex" constraintid="o14774" is_modifier="false" modifier="gradually" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o14774" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="true" name="shape" src="d0_s3" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pinnae 18–25 pairs, 2–5 (–8) × 1–1.5 (–2) cm, deeply serrate to incised nearly 3/4 of width, distal pinnae often strongly adnate;</text>
      <biological_entity id="o14775" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character char_type="range_value" from="18" name="quantity" src="d0_s4" to="25" />
        <character char_type="range_value" from="5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="deeply serrate" modifier="nearly" name="shape" src="d0_s4" to="incised" />
        <character constraint="of distal pinnae" constraintid="o14776" name="quantity" src="d0_s4" value="3/4" value_original="3/4" />
      </biological_entity>
      <biological_entity constraint="distal" id="o14776" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character is_modifier="true" name="character" src="d0_s4" value="width" value_original="width" />
        <character is_modifier="false" modifier="often strongly" name="fusion" src="d0_s4" value="adnate" value_original="adnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>segments of rather harsh texture, somewhat oblique, rounded-deltate to often acute;</text>
      <biological_entity id="o14777" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="somewhat" name="orientation_or_shape" src="d0_s5" value="oblique" value_original="oblique" />
        <character is_modifier="false" name="shape" src="d0_s5" value="rounded-deltate to often" value_original="rounded-deltate to often" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal pair of veins from adjacent segments united below sinus with excurrent vein.</text>
      <biological_entity constraint="vein" id="o14778" name="segment" name_original="segments" src="d0_s6" type="structure" constraint_original="vein proximal; vein" />
      <biological_entity id="o14779" name="vein" name_original="veins" src="d0_s6" type="structure" />
      <biological_entity id="o14780" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o14781" name="sinus" name_original="sinus" src="d0_s6" type="structure" />
      <biological_entity id="o14782" name="vein" name_original="vein" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <relation from="o14778" id="r3237" name="part_of" negation="false" src="d0_s6" to="o14779" />
      <relation from="o14778" id="r3238" name="from" negation="false" src="d0_s6" to="o14780" />
      <relation from="o14780" id="r3239" name="united" negation="false" src="d0_s6" to="o14781" />
      <relation from="o14780" id="r3240" name="united" negation="false" src="d0_s6" to="o14782" />
    </statement>
    <statement id="d0_s7">
      <text>Indument on both surfaces of numerous, sessile or short-stalked, stellate hairs 0.1–0.2 mm on costae, veins, and blade tissue;</text>
      <biological_entity id="o14783" name="indument" name_original="indument" src="d0_s7" type="structure" />
      <biological_entity id="o14784" name="surface" name_original="surfaces" src="d0_s7" type="structure">
        <character char_type="range_value" constraint="on blade tissue" constraintid="o14788" from="0.1" from_unit="mm" name="some_measurement" src="d0_s7" to="0.2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14785" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="numerous" value_original="numerous" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="true" name="architecture" src="d0_s7" value="short-stalked" value_original="short-stalked" />
        <character is_modifier="true" name="arrangement_or_shape" src="d0_s7" value="stellate" value_original="stellate" />
      </biological_entity>
      <biological_entity id="o14786" name="costa" name_original="costae" src="d0_s7" type="structure" />
      <biological_entity id="o14787" name="vein" name_original="veins" src="d0_s7" type="structure" />
      <biological_entity constraint="blade" id="o14788" name="tissue" name_original="tissue" src="d0_s7" type="structure" />
      <relation from="o14783" id="r3241" name="on" negation="false" src="d0_s7" to="o14784" />
      <relation from="o14784" id="r3242" name="part_of" negation="false" src="d0_s7" to="o14785" />
    </statement>
    <statement id="d0_s8">
      <text>rachises and costae sometimes with longer simple hairs to 0.8 mm abaxially.</text>
      <biological_entity id="o14789" name="rachis" name_original="rachises" src="d0_s8" type="structure" />
      <biological_entity id="o14790" name="costa" name_original="costae" src="d0_s8" type="structure" />
      <biological_entity constraint="longer" id="o14791" name="hair" name_original="hairs" src="d0_s8" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s8" value="simple" value_original="simple" />
        <character char_type="range_value" from="0" from_unit="mm" modifier="abaxially" name="some_measurement" src="d0_s8" to="0.8" to_unit="mm" />
      </biological_entity>
      <relation from="o14789" id="r3243" name="with" negation="false" src="d0_s8" to="o14791" />
      <relation from="o14790" id="r3244" name="with" negation="false" src="d0_s8" to="o14791" />
    </statement>
    <statement id="d0_s9">
      <text>Sori round, medial to supramedial;</text>
      <biological_entity id="o14792" name="sorus" name_original="sori" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="round" value_original="round" />
        <character char_type="range_value" from="medial" name="position" src="d0_s9" to="supramedial" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>indusia tan, densely stellate-hairy;</text>
      <biological_entity id="o14793" name="indusium" name_original="indusia" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="tan" value_original="tan" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="stellate-hairy" value_original="stellate-hairy" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sporangia glabrous.</text>
    </statement>
    <statement id="d0_s12">
      <text>2n = 144.</text>
      <biological_entity id="o14794" name="sporangium" name_original="sporangia" src="d0_s11" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s11" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14795" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="144" value_original="144" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial or on rock in limestone hammocks</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rock" modifier="terrestrial or on" constraint="in limestone hammocks" />
        <character name="habitat" value="limestone hammocks" />
        <character name="habitat" value="terrestrial" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–50 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="50" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Fla.; West Indies in the Greater Antilles.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" value="West Indies in the Greater Antilles" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18</number>
  <other_name type="common_name">Stiff star-hair fern</other_name>
  <discussion>In the flora Thelypteris sclerophylla is known only from Dade County, Florida, where it is rare.</discussion>
  <discussion>C. Christensen (1913), C. V. Morton (1951), and D. B. Lellinger (1985) have attributed the basionym to Kunze in Sprengel, but Sprengel clearly credited Poeppig, rightly or wrongly. Sprengel's original description also differs in a number of details from that by G. Kunze (Linnaea 9: 92. 1834), so that Kunze's later attribution of the basionym to himself cannot be accepted.</discussion>
  
</bio:treatment>