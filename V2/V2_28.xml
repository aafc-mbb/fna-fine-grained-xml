<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Newman" date="1851" rank="genus">gymnocarpium</taxon_name>
    <taxon_name authority="(Koidzumi) Koidzumi" date="1936" rank="species">jessoense</taxon_name>
    <taxon_name authority="Sarvela" date="1978" rank="subspecies">parvulum</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Bot. Fenn.</publication_title>
      <place_in_publication>15: 103. 1978</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus gymnocarpium;species jessoense;subspecies parvulum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233500658</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Gymnocarpium</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">continentale</taxon_name>
    <taxon_hierarchy>genus Gymnocarpium;species continentale;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems 0.5–1.5 mm diam.;</text>
      <biological_entity id="o1106" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="diameter" src="d0_s0" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales 1–4 mm.</text>
      <biological_entity id="o1107" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s1" to="4" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Fertile leaves usually 8–39 cm.</text>
      <biological_entity id="o1108" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s2" value="fertile" value_original="fertile" />
        <character char_type="range_value" from="8" from_unit="cm" name="some_measurement" src="d0_s2" to="39" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 5–25 cm, with moderately abundant glandular-hairs distally;</text>
      <biological_entity id="o1109" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s3" to="25" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o1110" name="glandular-hair" name_original="glandular-hairs" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="moderately" name="quantity" src="d0_s3" value="abundant" value_original="abundant" />
      </biological_entity>
      <relation from="o1109" id="r264" name="with" negation="false" src="d0_s3" to="o1110" />
    </statement>
    <statement id="d0_s4">
      <text>scales 2–6 mm.</text>
      <biological_entity id="o1111" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s4" to="6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade narrowly deltate to narrowly ovate, 2-pinnate-pinnatifid, 3–14 cm, firm and robust or lax and delicate, abaxial surface moderately glandular, rachis moderately to densely glandular, adaxial surface glabrous.</text>
      <biological_entity id="o1112" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly deltate" name="shape" src="d0_s5" to="narrowly ovate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="2-pinnate-pinnatifid" value_original="2-pinnate-pinnatifid" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s5" to="14" to_unit="cm" />
        <character is_modifier="false" name="texture" src="d0_s5" value="firm" value_original="firm" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="robust" value_original="robust" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s5" value="lax" value_original="lax" />
        <character is_modifier="false" name="fragility" src="d0_s5" value="delicate" value_original="delicate" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o1113" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o1114" name="rachis" name_original="rachis" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="moderately to densely" name="architecture_or_function_or_pubescence" src="d0_s5" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o1115" name="surface" name_original="surface" src="d0_s5" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pinna apex acute.</text>
      <biological_entity constraint="pinna" id="o1116" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Proximal pinnae 2–9 cm, strongly curved toward apex of leaf, basiscopic pinnules strongly curved toward apex of pinna;</text>
      <biological_entity constraint="proximal" id="o1117" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s7" to="9" to_unit="cm" />
        <character constraint="toward apex" constraintid="o1118" is_modifier="false" modifier="strongly" name="course" src="d0_s7" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o1118" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o1119" name="leaf" name_original="leaf" src="d0_s7" type="structure" />
      <biological_entity id="o1120" name="pinnule" name_original="pinnules" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="basiscopic" value_original="basiscopic" />
        <character constraint="toward apex" constraintid="o1121" is_modifier="false" modifier="strongly" name="course" src="d0_s7" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity id="o1121" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o1122" name="pinna" name_original="pinna" src="d0_s7" type="structure" />
      <relation from="o1118" id="r265" name="part_of" negation="false" src="d0_s7" to="o1119" />
      <relation from="o1121" id="r266" name="part_of" negation="false" src="d0_s7" to="o1122" />
    </statement>
    <statement id="d0_s8">
      <text>basal basiscopic pinnule usually sessile, pinnatifid or rarely pinnate-pinnatifid, if sessile then with basal basiscopic pinnulet often equaling adjacent pinnulet;</text>
      <biological_entity constraint="basal" id="o1123" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o1124" name="pinnule" name_original="pinnule" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="usually" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s8" value="pinnatifid" value_original="pinnatifid" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s8" value="pinnate-pinnatifid" value_original="pinnate-pinnatifid" />
        <character constraint="with basal pinnae" constraintid="o1125" is_modifier="false" name="architecture" src="d0_s8" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1125" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o1126" name="pinnulet" name_original="pinnulet" src="d0_s8" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s8" value="basiscopic" value_original="basiscopic" />
      </biological_entity>
      <biological_entity id="o1127" name="pinnulet" name_original="pinnulet" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="often" name="variability" src="d0_s8" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s8" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>2d basal basiscopic pinnule sessile, with basal basiscopic pinnulet equaling adjacent pinnulet;</text>
      <biological_entity constraint="basal" id="o1128" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o1129" name="pinnule" name_original="pinnule" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1130" name="pinna" name_original="pinnae" src="d0_s9" type="structure" />
      <biological_entity id="o1131" name="pinnulet" name_original="pinnulet" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="basiscopic" value_original="basiscopic" />
      </biological_entity>
      <biological_entity id="o1132" name="pinnulet" name_original="pinnulet" src="d0_s9" type="structure">
        <character is_modifier="true" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s9" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <relation from="o1129" id="r267" name="with" negation="false" src="d0_s9" to="o1130" />
    </statement>
    <statement id="d0_s10">
      <text>basal acroscopic pinnule sessile with basal basiscopic pinnulet longer than or equaling adjacent pinnulet.</text>
      <biological_entity constraint="basal" id="o1133" name="pinna" name_original="pinnae" src="d0_s10" type="structure" />
      <biological_entity id="o1134" name="pinnule" name_original="pinnule" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="acroscopic" value_original="acroscopic" />
        <character constraint="with basal pinnae" constraintid="o1135" is_modifier="false" name="architecture" src="d0_s10" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1135" name="pinna" name_original="pinnae" src="d0_s10" type="structure" />
      <biological_entity id="o1136" name="pinnulet" name_original="pinnulet" src="d0_s10" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s10" value="basiscopic" value_original="basiscopic" />
        <character constraint="than or equaling adjacent pinnulet" constraintid="o1137" is_modifier="false" name="length_or_size" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o1137" name="pinnulet" name_original="pinnulet" src="d0_s10" type="structure">
        <character is_modifier="true" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s10" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Pinnae of 2d pair almost always sessile with basal basiscopic pinnule usually equaling or slightly shorter than adjacent pinnule and equaling basal acroscopic pinnule;</text>
      <biological_entity id="o1138" name="pinna" name_original="pinnae" src="d0_s11" type="structure">
        <character constraint="with basal pinnae" constraintid="o1139" is_modifier="false" modifier="almost always" name="architecture" src="d0_s11" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1139" name="pinna" name_original="pinnae" src="d0_s11" type="structure" />
      <biological_entity id="o1140" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s11" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="usually" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
        <character constraint="than adjacent pinnule and equaling basal acroscopic pinnule" constraintid="o1142" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o1142" name="pinnule" name_original="pinnule" src="d0_s11" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s11" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>basal acroscopic pinnule equaling or slightly shorter than adjacent pinnule, apex often entire, rounded.</text>
      <biological_entity constraint="basal" id="o1143" name="pinna" name_original="pinnae" src="d0_s12" type="structure" />
      <biological_entity id="o1144" name="pinnule" name_original="pinnule" src="d0_s12" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s12" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
        <character constraint="than adjacent pinnule" constraintid="o1145" is_modifier="false" name="height_or_length_or_size" src="d0_s12" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o1145" name="pinnule" name_original="pinnule" src="d0_s12" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s12" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o1146" name="apex" name_original="apex" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="often" name="architecture_or_shape" src="d0_s12" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s12" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Pinnae of 3d pair sessile with basal basiscopic pinnule equaling adjacent pinnule and equaling basal acroscopic pinnule;</text>
      <biological_entity id="o1147" name="pinna" name_original="pinnae" src="d0_s13" type="structure">
        <character constraint="with basal pinnae" constraintid="o1149" is_modifier="false" name="architecture" src="d0_s13" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o1148" name="pair" name_original="pair" src="d0_s13" type="structure" />
      <biological_entity constraint="basal" id="o1149" name="pinna" name_original="pinnae" src="d0_s13" type="structure" />
      <biological_entity id="o1150" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s13" value="basiscopic" value_original="basiscopic" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1152" name="pinnule" name_original="pinnule" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="arrangement" src="d0_s13" value="adjacent" value_original="adjacent" />
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
        <character is_modifier="true" name="orientation" src="d0_s13" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o1147" id="r268" name="part_of" negation="false" src="d0_s13" to="o1148" />
    </statement>
    <statement id="d0_s14">
      <text>basal acroscopic pinnule equaling or slightly shorter than adjacent pinnule.</text>
      <biological_entity constraint="basal" id="o1153" name="pinna" name_original="pinnae" src="d0_s14" type="structure" />
      <biological_entity id="o1154" name="pinnule" name_original="pinnule" src="d0_s14" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s14" value="acroscopic" value_original="acroscopic" />
        <character is_modifier="false" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
        <character constraint="than adjacent pinnule" constraintid="o1155" is_modifier="false" name="height_or_length_or_size" src="d0_s14" value="slightly shorter" value_original="slightly shorter" />
      </biological_entity>
      <biological_entity id="o1155" name="pinnule" name_original="pinnule" src="d0_s14" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s14" value="adjacent" value_original="adjacent" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Ultimate segments of proximal pinnae oblong, entire to slightly crenate, apex entire, rounded.</text>
      <biological_entity constraint="ultimate" id="o1156" name="segment" name_original="segments" src="d0_s15" type="structure">
        <character is_modifier="false" name="shape" src="d0_s15" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s15" to="slightly crenate" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o1157" name="pinna" name_original="pinnae" src="d0_s15" type="structure" />
      <biological_entity id="o1158" name="apex" name_original="apex" src="d0_s15" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s15" value="rounded" value_original="rounded" />
      </biological_entity>
      <relation from="o1156" id="r269" name="part_of" negation="false" src="d0_s15" to="o1157" />
    </statement>
    <statement id="d0_s16">
      <text>Spores 32–37 µm. 2n = 160.</text>
      <biological_entity id="o1159" name="spore" name_original="spores" src="d0_s16" type="structure">
        <character char_type="range_value" from="32" from_unit="um" name="some_measurement" src="d0_s16" to="37" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="2n" id="o1160" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="160" value_original="160" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Acid or neutral substrates at summit of cool, shale talus slopes, and on granitic cliffs and outcrops</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="acid" />
        <character name="habitat" value="neutral substrates" constraint="at summit of cool , shale talus" />
        <character name="habitat" value="summit" constraint="of cool , shale talus" />
        <character name="habitat" value="cool" />
        <character name="habitat" value="shale talus" />
        <character name="habitat" value="granitic cliffs" modifier="slopes and on" />
        <character name="habitat" value="outcrops" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.W.T., Ont., Que., Sask., Yukon; Alaska, Conn., Iowa, Maine, Mich., Minn., Vt., Wis.; Europe in Finland; Asia in Siberia, Kazakhstan.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" value="Europe in Finland" establishment_means="native" />
        <character name="distribution" value="Asia in Siberia" establishment_means="native" />
        <character name="distribution" value="Kazakhstan" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4a</number>
  <other_name type="common_name">Nahanni oak fern</other_name>
  <other_name type="common_name">gymnocarpe du japon sous-espèce fréle</other_name>
  <discussion>Hybrids between Gymnocarpium jessoense subsp. parvulum and G. dryopteris (G. × intermedium Sarvela) are usually found wherever these two taxa occur together (Finland; Manitoba, Northwest Territories, Ontario, Quebec, Saskatchewan, Yukon; Alaska, Michigan, Minnesota, Wisconsin), and they are particularly abundant in the Great Lakes region. These hybrids have sometimes been referred to as G. × heterosporum, a name that is, however, correctly restricted to unique hybrids between G. robertianum and G. appalachianum (see discussion under G. robertianum; K. M. Pryer 1992). Gymnocarpium × intermedium is intermediate between the two parental species in its leaf morphology and glandularity, and it can be readily distinguished by its small, blackish, malformed, abortive spores, as well as large, brown, round spores that may allow this taxon to reproduce apogamously. Of the Gymnocarpium sterile hybrids, G. × intermedium is the easiest to distinguish morphologically.</discussion>
  
</bio:treatment>