<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Willkomm in Willkomm &amp; Lange" date="unknown" rank="family">selaginellaceae</taxon_name>
    <taxon_name authority="Palisot de Beauvois" date="1805" rank="genus">selaginella</taxon_name>
    <taxon_name authority="Jermy" date="1986" rank="subgenus">tetragonostachys</taxon_name>
    <taxon_name authority="Hieronymus" date="1900" rank="species">hansenii</taxon_name>
    <place_of_publication>
      <publication_title>Hedwigia</publication_title>
      <place_in_publication>39: 301. 1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family selaginellaceae;genus selaginella;subgenus tetragonostachys;species hansenii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501232</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial, forming loose to clustered mats.</text>
      <biological_entity id="o11845" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o11846" name="mat" name_original="mats" src="d0_s0" type="structure">
        <character is_modifier="true" name="architecture_or_fragility" src="d0_s0" value="loose" value_original="loose" />
        <character is_modifier="true" name="arrangement_or_growth_form" src="d0_s0" value="clustered" value_original="clustered" />
      </biological_entity>
      <relation from="o11845" id="r2553" name="forming" negation="false" src="d0_s0" to="o11846" />
    </statement>
    <statement id="d0_s1">
      <text>Stems not readily fragmenting, prostrate, upperside and underside structurally different, irregularly forked, branches determinate, tips upturned.</text>
      <biological_entity id="o11847" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o11848" name="upperside" name_original="upperside" src="d0_s1" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s1" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity id="o11849" name="underside" name_original="underside" src="d0_s1" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s1" value="prostrate" value_original="prostrate" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s1" value="forked" value_original="forked" />
      </biological_entity>
      <biological_entity id="o11850" name="branch" name_original="branches" src="d0_s1" type="structure">
        <character is_modifier="false" name="development" src="d0_s1" value="determinate" value_original="determinate" />
      </biological_entity>
      <biological_entity id="o11851" name="tip" name_original="tips" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="upturned" value_original="upturned" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Rhizophores borne on upperside of stems, throughout stem length, 0.25–0.45 mm diam.</text>
      <biological_entity id="o11852" name="rhizophore" name_original="rhizophores" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.25" from_unit="mm" name="diameter" notes="" src="d0_s2" to="0.45" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11853" name="upperside" name_original="upperside" src="d0_s2" type="structure" />
      <biological_entity id="o11854" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o11855" name="stem" name_original="stem" src="d0_s2" type="structure" />
      <relation from="o11852" id="r2554" name="borne on" negation="false" src="d0_s2" to="o11853" />
      <relation from="o11852" id="r2555" name="borne on" negation="false" src="d0_s2" to="o11854" />
      <relation from="o11852" id="r2556" name="throughout" negation="false" src="d0_s2" to="o11855" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves with underside leaves slightly longer and narrower than upperside leaves, otherwise monomorphic, not clearly ranked, tightly appressed, ascending, green or green with red spots, or reddish, linear-lanceolate (underside) to linear-triangular (upperside), (2–) 3–4.5 × 0.5–0.6 mm;</text>
      <biological_entity id="o11856" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="than upperside leaves" constraintid="o11858" is_modifier="false" name="width" src="d0_s3" value="slightly longer and narrower" value_original="slightly longer and narrower" />
        <character is_modifier="false" modifier="otherwise" name="architecture" notes="" src="d0_s3" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="not clearly" name="arrangement" src="d0_s3" value="ranked" value_original="ranked" />
        <character is_modifier="false" modifier="tightly" name="fixation_or_orientation" src="d0_s3" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character constraint="with red spots" is_modifier="false" name="coloration" src="d0_s3" value="green" value_original="green" />
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish" value_original="reddish" />
        <character char_type="range_value" from="linear-lanceolate" name="shape" src="d0_s3" to="linear-triangular" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s3" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s3" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s3" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="underside" id="o11857" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="slightly" name="length_or_size" src="d0_s3" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity constraint="upperside" id="o11858" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o11856" id="r2557" name="with" negation="false" src="d0_s3" to="o11857" />
    </statement>
    <statement id="d0_s4">
      <text>abaxial ridges present;</text>
      <biological_entity constraint="abaxial" id="o11859" name="ridge" name_original="ridges" src="d0_s4" type="structure">
        <character is_modifier="false" name="presence" src="d0_s4" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>base abruptly adnate, pubescent (sometimes glabrous);</text>
      <biological_entity id="o11860" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="abruptly" name="fusion" src="d0_s5" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>margins ciliate, cilia white to white opaque, strongly appressed and ascending, 0.03–0.1 mm;</text>
      <biological_entity id="o11861" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s6" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o11862" name="cilium" name_original="cilia" src="d0_s6" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s6" to="white opaque" />
        <character is_modifier="false" modifier="strongly" name="fixation_or_orientation" src="d0_s6" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character char_type="range_value" from="0.03" from_unit="mm" name="some_measurement" src="d0_s6" to="0.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>apex with bristle white to white-opaque, 0.5–1.4 mm (those on underside leaves sometimes 1/4–1/2 longer than those on upperside leaves).</text>
      <biological_entity id="o11863" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" notes="" src="d0_s7" to="1.4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o11864" name="bristle" name_original="bristle" src="d0_s7" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s7" to="white-opaque" />
      </biological_entity>
      <relation from="o11863" id="r2558" name="with" negation="false" src="d0_s7" to="o11864" />
    </statement>
    <statement id="d0_s8">
      <text>Strobili solitary, 5–7 mm;</text>
      <biological_entity id="o11865" name="strobilus" name_original="strobili" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s8" value="solitary" value_original="solitary" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s8" to="7" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>sporophylls ovate-deltate to ovate-triangular, abaxial ridges not prominent, base glabrous, margins short-ciliate, apex bristled.</text>
      <biological_entity id="o11866" name="sporophyll" name_original="sporophylls" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate-deltate" name="shape" src="d0_s9" to="ovate-triangular" />
      </biological_entity>
      <biological_entity constraint="abaxial" id="o11867" name="ridge" name_original="ridges" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="not" name="prominence" src="d0_s9" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o11868" name="base" name_original="base" src="d0_s9" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o11869" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="short-ciliate" value_original="short-ciliate" />
      </biological_entity>
      <biological_entity id="o11870" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="bristled" value_original="bristled" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs and rocky slopes or on igneous rock</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="igneous rock" modifier="or on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>330–1350 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1350" to_unit="m" from="330" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6</number>
  <other_name type="common_name">Hansen's spike-moss</other_name>
  <discussion>Leaf dimorphism in Selaginella hansenii is only slightly and inconsistently expressed; the upperside leaves tend to be more lanceolate, short, and slightly thick, whereas the underside leaves tend to be more linear, longer, and thinner, but in some specimens the leaves are monomorphic. Red leaves are rare within Selaginella subg. Tetragonostachys, otherwise found in the flora only occasionally in S. rupestris. Such leaves are more common in S. steyermarkii Alston from southern Mexico and Guatemala and S. sartorii Hieronymus from Mexico.</discussion>
  
</bio:treatment>