<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Roth" date="1799" rank="genus">polystichum</taxon_name>
    <taxon_name authority="Maxon" date="1918" rank="species">dudleyi</taxon_name>
    <place_of_publication>
      <publication_title>J. Wash. Acad. Sci.</publication_title>
      <place_in_publication>8: 620. 1918</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus polystichum;species dudleyi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500986</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polystichum</taxon_name>
    <taxon_name authority="(Linnaeus) Roth" date="unknown" rank="species">aculeatum</taxon_name>
    <taxon_name authority="(Maxon) Jepson" date="unknown" rank="variety">dudleyi</taxon_name>
    <taxon_hierarchy>genus Polystichum;species aculeatum;variety dudleyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems erect.</text>
      <biological_entity id="o5206" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves monomorphic, arching, 2–10 dm;</text>
      <biological_entity id="o5207" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s1" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="arching" value_original="arching" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s1" to="10" to_unit="dm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>bulblets absent.</text>
      <biological_entity id="o5208" name="bulblet" name_original="bulblets" src="d0_s2" type="structure">
        <character is_modifier="false" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole 1/5–1/3 length of leaf, densely scaly;</text>
      <biological_entity id="o5209" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1/5 length of leaf" name="length" src="d0_s3" to="1/3 length of leaf" />
        <character is_modifier="false" modifier="densely" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>scales light-brown, gradually diminishing in size distally.</text>
      <biological_entity id="o5210" name="scale" name_original="scales" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" modifier="gradually; distally" name="size" src="d0_s4" value="diminishing" value_original="diminishing" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade broadly lanceolate, 2-pinnate, base not narrowed.</text>
      <biological_entity id="o5211" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="2-pinnate" value_original="2-pinnate" />
      </biological_entity>
      <biological_entity id="o5212" name="base" name_original="base" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="narrowed" value_original="narrowed" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pinnae narrowly lanceolate, not overlapping, in 1 plane, 3–13 cm;</text>
      <biological_entity id="o5213" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s6" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="not" name="arrangement" src="d0_s6" value="overlapping" value_original="overlapping" />
        <character char_type="range_value" from="3" from_unit="cm" modifier="in 1 plane" name="some_measurement" src="d0_s6" to="13" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>base oblique, apex acute with subapical and apical teeth same size;</text>
      <biological_entity id="o5214" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation_or_shape" src="d0_s7" value="oblique" value_original="oblique" />
      </biological_entity>
      <biological_entity id="o5215" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character constraint="with subapical apical teeth" constraintid="o5216" is_modifier="false" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
      <biological_entity constraint="subapical and apical" id="o5216" name="tooth" name_original="teeth" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>microscales filiform, lacking projections, sparse abaxially, but longer than in other Polystichum species, forming loosely tangled network over blade and sori (such network only in this species), sparse adaxially.</text>
      <biological_entity id="o5217" name="microscale" name_original="microscales" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="filiform" value_original="filiform" />
      </biological_entity>
      <biological_entity id="o5218" name="projection" name_original="projections" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="lacking" value_original="lacking" />
        <character is_modifier="false" modifier="abaxially" name="count_or_density" src="d0_s8" value="sparse" value_original="sparse" />
        <character is_modifier="false" modifier="in other polystichum species; adaxially" name="count_or_density" src="d0_s8" value="sparse" value_original="sparse" />
      </biological_entity>
      <biological_entity id="o5219" name="blade" name_original="blade" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="loosely" name="arrangement" src="d0_s8" value="tangled" value_original="tangled" />
        <character is_modifier="true" name="arrangement" src="d0_s8" value="network" value_original="network" />
      </biological_entity>
      <biological_entity id="o5220" name="sorus" name_original="sori" src="d0_s8" type="structure">
        <character is_modifier="true" modifier="loosely" name="arrangement" src="d0_s8" value="tangled" value_original="tangled" />
        <character is_modifier="true" name="arrangement" src="d0_s8" value="network" value_original="network" />
      </biological_entity>
      <relation from="o5218" id="r1135" modifier="in other polystichum species" name="forming" negation="false" src="d0_s8" to="o5219" />
      <relation from="o5218" id="r1136" modifier="in other polystichum species" name="forming" negation="false" src="d0_s8" to="o5220" />
    </statement>
    <statement id="d0_s9">
      <text>Pinnules ± stalked, linear-falcate to oblique-rhombic, acroscopic auricle well developed on proximal pinnules;</text>
      <biological_entity id="o5221" name="pinnule" name_original="pinnules" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" src="d0_s9" value="stalked" value_original="stalked" />
        <character char_type="range_value" from="linear-falcate" name="shape" src="d0_s9" to="oblique-rhombic" />
      </biological_entity>
      <biological_entity id="o5222" name="auricle" name_original="auricle" src="d0_s9" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s9" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o5223" name="pinnule" name_original="pinnules" src="d0_s9" type="structure" />
      <relation from="o5222" id="r1137" modifier="well" name="developed on" negation="false" src="d0_s9" to="o5223" />
    </statement>
    <statement id="d0_s10">
      <text>margins spinulose-dentate;</text>
      <biological_entity id="o5224" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="spinulose-dentate" value_original="spinulose-dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>apex acute.</text>
      <biological_entity id="o5225" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Indusia ciliate.</text>
      <biological_entity id="o5226" name="indusium" name_original="indusia" src="d0_s12" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s12" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores brown.</text>
    </statement>
    <statement id="d0_s14">
      <text>2n = 82.</text>
      <biological_entity id="o5227" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brown" value_original="brown" />
      </biological_entity>
      <biological_entity constraint="2n" id="o5228" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="82" value_original="82" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="100" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>6</number>
  <other_name type="common_name">Dudley's sword fern</other_name>
  <discussion>Polystichum dudleyi is confined to coastal central California. Hybrids with P. californicum are relatively frequent where these species occur together. These hybrids would key here but, unlike P. dudleyi, they are less divided and have aborted sporangia. The sterile diploid hybrid with P. munitum is also frequent in areas of sympatry. It is indistinguishable from P. californicum except for malformed sporangia and chromosome number (W. H. Wagner Jr. 1973).</discussion>
  
</bio:treatment>