<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Clifton E. Nauman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="J. Presl &amp; C. Presl" date="unknown" rank="family">polypodiaceae</taxon_name>
    <taxon_name authority="(R. Brown) J. Smith" date="1841" rank="genus">Phlebodium</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. (Hooker)</publication_title>
      <place_in_publication>4: 58. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polypodiaceae;genus Phlebodium</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek phlebos, vein, referring to the prominent venation</other_info_on_name>
    <other_info_on_name type="fna_id">125032</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="R. Brown" date="unknown" rank="section">Phlebodium</taxon_name>
    <place_of_publication>
      <publication_title>in J. J. Bennett et al., Pl. Jav. Rar.</publication_title>
      <place_in_publication>1: 4. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;section Phlebodium;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants epiphytic.</text>
      <biological_entity id="o9737" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems creeping, branched, not whitish pruinose, 8–30 mm diam.;</text>
      <biological_entity id="o9738" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s1" value="creeping" value_original="creeping" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="not" name="coloration" src="d0_s1" value="whitish" value_original="whitish" />
        <character is_modifier="false" name="coating" src="d0_s1" value="pruinose" value_original="pruinose" />
        <character char_type="range_value" from="8" from_unit="mm" name="diameter" src="d0_s1" to="30" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>scales concolored, lanceolate, not clathrate, glabrous, margins dentate.</text>
      <biological_entity id="o9739" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="concolored" value_original="concolored" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s2" value="clathrate" value_original="clathrate" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9740" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s2" value="dentate" value_original="dentate" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves monomorphic, widely spaced, not conspicuously narrowed at tip, to 130 cm.</text>
      <biological_entity id="o9741" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s3" value="spaced" value_original="spaced" />
        <character constraint="at tip" constraintid="o9742" is_modifier="false" modifier="not conspicuously" name="shape" src="d0_s3" value="narrowed" value_original="narrowed" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="130" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o9742" name="tip" name_original="tip" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Petiole articulate to stem, dark-brown, round in cross-section, with 2 adaxial grooves.</text>
      <biological_entity id="o9743" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character constraint="to stem" constraintid="o9744" is_modifier="false" name="architecture" src="d0_s4" value="articulate" value_original="articulate" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s4" value="dark-brown" value_original="dark-brown" />
        <character constraint="in cross-section" constraintid="o9745" is_modifier="false" name="shape" src="d0_s4" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o9744" name="stem" name_original="stem" src="d0_s4" type="structure" />
      <biological_entity id="o9745" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
      <biological_entity constraint="adaxial" id="o9746" name="groove" name_original="grooves" src="d0_s4" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s4" value="2" value_original="2" />
      </biological_entity>
      <relation from="o9743" id="r2089" name="with" negation="false" src="d0_s4" to="o9746" />
    </statement>
    <statement id="d0_s5">
      <text>Blade ovate to elliptic, not pectinate, pinnatisect with broadly winged rachises and rounded sinuses, with fewer than 20 pairs of segments, glaucous, usually glabrous, scales absent;</text>
      <biological_entity id="o9747" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s5" to="elliptic" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s5" value="pectinate" value_original="pectinate" />
        <character constraint="with rachises" constraintid="o9748" is_modifier="false" name="architecture_or_shape" src="d0_s5" value="pinnatisect" value_original="pinnatisect" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o9748" name="rachis" name_original="rachises" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="broadly" name="architecture" src="d0_s5" value="winged" value_original="winged" />
      </biological_entity>
      <biological_entity id="o9749" name="sinuse" name_original="sinuses" src="d0_s5" type="structure">
        <character is_modifier="true" name="shape" src="d0_s5" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o9750" name="pair" name_original="pairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="0" is_modifier="true" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o9751" name="segment" name_original="segments" src="d0_s5" type="structure" />
      <biological_entity id="o9752" name="scale" name_original="scales" src="d0_s5" type="structure">
        <character is_modifier="false" name="presence" src="d0_s5" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o9747" id="r2090" name="with" negation="false" src="d0_s5" to="o9750" />
      <relation from="o9750" id="r2091" name="part_of" negation="false" src="d0_s5" to="o9751" />
    </statement>
    <statement id="d0_s6">
      <text>rachis glabrous.</text>
      <biological_entity id="o9753" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Segments linear to lanceolate, margins entire, apex rounded.</text>
      <biological_entity id="o9754" name="segment" name_original="segments" src="d0_s7" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s7" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o9755" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity id="o9756" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Veins free near margins, well developed near costae, highly reticulate, usually with 1 row of costal areoles extending from vein to vein without included veinlets, and with a series of elongate, polygonal areoles with 1–3 excurrent included veinlets meeting at apices, and similar areoles closer to margin of segment mostly without included veinlets.</text>
      <biological_entity id="o9757" name="vein" name_original="veins" src="d0_s8" type="structure">
        <character constraint="near margins" constraintid="o9758" is_modifier="false" name="fusion" src="d0_s8" value="free" value_original="free" />
        <character is_modifier="false" modifier="highly" name="architecture_or_coloration_or_relief" src="d0_s8" value="reticulate" value_original="reticulate" />
      </biological_entity>
      <biological_entity id="o9758" name="margin" name_original="margins" src="d0_s8" type="structure" />
      <biological_entity id="o9759" name="costa" name_original="costae" src="d0_s8" type="structure" />
      <biological_entity id="o9760" name="row" name_original="row" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="1" value_original="1" />
      </biological_entity>
      <biological_entity constraint="costal" id="o9761" name="areole" name_original="areoles" src="d0_s8" type="structure" />
      <biological_entity id="o9762" name="vein" name_original="vein" src="d0_s8" type="structure" />
      <biological_entity id="o9763" name="vein" name_original="vein" src="d0_s8" type="structure" />
      <biological_entity id="o9764" name="series" name_original="series" src="d0_s8" type="structure" />
      <biological_entity constraint="included" id="o9765" name="veinlet" name_original="veinlets" src="d0_s8" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s8" to="3" />
        <character is_modifier="true" name="architecture" src="d0_s8" value="excurrent" value_original="excurrent" />
      </biological_entity>
      <biological_entity id="o9766" name="apex" name_original="apices" src="d0_s8" type="structure" />
      <biological_entity id="o9767" name="areole" name_original="areoles" src="d0_s8" type="structure">
        <character constraint="to margin" constraintid="o9768" is_modifier="false" name="arrangement" src="d0_s8" value="closer" value_original="closer" />
      </biological_entity>
      <biological_entity id="o9768" name="margin" name_original="margin" src="d0_s8" type="structure" />
      <biological_entity id="o9769" name="segment" name_original="segment" src="d0_s8" type="structure" />
      <biological_entity constraint="included" id="o9770" name="veinlet" name_original="veinlets" src="d0_s8" type="structure" />
      <relation from="o9757" id="r2092" modifier="well" name="developed near" negation="false" src="d0_s8" to="o9759" />
      <relation from="o9757" id="r2093" modifier="usually" name="with" negation="false" src="d0_s8" to="o9760" />
      <relation from="o9760" id="r2094" name="part_of" negation="false" src="d0_s8" to="o9761" />
      <relation from="o9760" id="r2095" name="extending from" negation="false" src="d0_s8" to="o9762" />
      <relation from="o9757" id="r2096" name="to" negation="false" src="d0_s8" to="o9763" />
      <relation from="o9763" id="r2097" name="without included veinlets , and with" negation="false" src="d0_s8" to="o9764" />
      <relation from="o9764" id="r2098" name="with" negation="false" src="d0_s8" to="o9765" />
      <relation from="o9764" id="r2099" name="meeting at" negation="false" src="d0_s8" to="o9766" />
      <relation from="o9768" id="r2100" name="part_of" negation="false" src="d0_s8" to="o9769" />
      <relation from="o9768" id="r2101" name="without" negation="false" src="d0_s8" to="o9770" />
    </statement>
    <statement id="d0_s9">
      <text>Sori on veins, transverse, 2 in each major areole on included veinlets, circular to oblong;</text>
      <biological_entity id="o9771" name="sorus" name_original="sori" src="d0_s9" type="structure">
        <character is_modifier="false" name="dehiscence_or_orientation" notes="" src="d0_s9" value="transverse" value_original="transverse" />
        <character constraint="in areole" constraintid="o9773" name="quantity" src="d0_s9" value="2" value_original="2" />
        <character char_type="range_value" from="circular" name="shape" src="d0_s9" to="oblong" />
      </biological_entity>
      <biological_entity id="o9772" name="vein" name_original="veins" src="d0_s9" type="structure" />
      <biological_entity id="o9773" name="areole" name_original="areole" src="d0_s9" type="structure">
        <character is_modifier="true" name="size" src="d0_s9" value="major" value_original="major" />
      </biological_entity>
      <biological_entity id="o9774" name="veinlet" name_original="veinlets" src="d0_s9" type="structure" />
      <relation from="o9771" id="r2102" name="on" negation="false" src="d0_s9" to="o9772" />
      <relation from="o9773" id="r2103" name="included" negation="false" src="d0_s9" to="o9774" />
    </statement>
    <statement id="d0_s10">
      <text>indument absent.</text>
      <biological_entity id="o9775" name="indument" name_original="indument" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Spores tuberculate.</text>
    </statement>
    <statement id="d0_s12">
      <text>x = 37.</text>
      <biological_entity id="o9776" name="spore" name_original="spores" src="d0_s11" type="structure">
        <character is_modifier="false" name="relief" src="d0_s11" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="x" id="o9777" name="chromosome" name_original="" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="37" value_original="37" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tropical regions, North America, Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Tropical regions" establishment_means="native" />
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Golden polypodies</other_name>
  <discussion>Species 2–4 (1 in the flora).</discussion>
  <references>
    <reference>Duncan, W. H. 1954. Polypodium aureum in Florida and Georgia. Amer. Fern J. 44: 155–158.</reference>
    <reference>Lellinger, D. B. 1987. Nomenclatural notes on some ferns of Costa Rica, Panama, and Colombia. III. Amer. Fern J. 77: 101–102.</reference>
    <reference>Proctor, G. R. 1985. Ferns of Jamaica. London.</reference>
    <reference>Snyder, L. H. Jr. and J. G. Bruce. 1986. Field Guide to the Ferns and Other Pteridophytes of Georgia. Athens, Ga.</reference>
  </references>
  
</bio:treatment>