<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">116</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="C. Presl" date="unknown" rank="family">lygodiaceae</taxon_name>
    <taxon_name authority="Swartz" date="1801" rank="genus">lygodium</taxon_name>
    <taxon_name authority="(Cavanilles) R. Brown" date="1810" rank="species">microphyllum</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>162. 1810</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lygodiaceae;genus lygodium;species microphyllum</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">233500763</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Ugena</taxon_name>
    <taxon_name authority="Cavanilles" date="unknown" rank="species">microphylla</taxon_name>
    <place_of_publication>
      <publication_title>Icon.</publication_title>
      <place_in_publication>6: 76, plate 595. 1801</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Ugena;species microphylla;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems creeping.</text>
      <biological_entity id="o5830" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves to ca. 10 m.</text>
      <biological_entity id="o5831" name="leaf" name_original="leaves" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Petioles borne 2-5 mm apart, 7-25 cm.</text>
      <biological_entity id="o5832" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s2" to="5" to_unit="mm" />
        <character is_modifier="false" name="arrangement" src="d0_s2" value="apart" value_original="apart" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Sterile pinnae on 0.5-1.5 cm stalks, oblong, 1-pinnate, 5-12 × 3-6 cm;</text>
      <biological_entity id="o5833" name="pinna" name_original="pinnae" src="d0_s3" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s3" value="sterile" value_original="sterile" />
        <character is_modifier="false" name="shape" notes="" src="d0_s3" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="1-pinnate" value_original="1-pinnate" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s3" to="12" to_unit="cm" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" src="d0_s3" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5834" name="stalk" name_original="stalks" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s3" to="1.5" to_unit="cm" />
      </biological_entity>
      <relation from="o5833" id="r1250" name="on" negation="false" src="d0_s3" to="o5834" />
    </statement>
    <statement id="d0_s4">
      <text>ultimate segments triangular-lanceolate to oblong-lanceolate, truncate to shallowly cordate or somewhat auriculate proximally, usually not lobed, but if lobed, lobes rounded at apex and not directed toward leaf apex;</text>
      <biological_entity constraint="ultimate" id="o5835" name="segment" name_original="segments" src="d0_s4" type="structure">
        <character char_type="range_value" from="triangular-lanceolate" name="shape" src="d0_s4" to="oblong-lanceolate truncate" />
        <character char_type="range_value" from="triangular-lanceolate" name="shape" src="d0_s4" to="oblong-lanceolate truncate" />
        <character is_modifier="false" modifier="somewhat; somewhat; proximally" name="shape" src="d0_s4" value="auriculate" value_original="auriculate" />
        <character is_modifier="false" modifier="usually not" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lobed" value_original="lobed" />
      </biological_entity>
      <biological_entity id="o5836" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character constraint="at apex" constraintid="o5837" is_modifier="false" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
        <character constraint="toward leaf apex" constraintid="o5838" is_modifier="false" modifier="not" name="orientation" notes="" src="d0_s4" value="directed" value_original="directed" />
      </biological_entity>
      <biological_entity id="o5837" name="apex" name_original="apex" src="d0_s4" type="structure" />
      <biological_entity constraint="leaf" id="o5838" name="apex" name_original="apex" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>segment apex rounded-acute to obtuse;</text>
      <biological_entity constraint="segment" id="o5839" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="rounded-acute" name="shape" src="d0_s5" to="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>segments articulate to petiolules, leaving wiry stalks when detached;</text>
      <biological_entity id="o5840" name="segment" name_original="segments" src="d0_s6" type="structure">
        <character constraint="to petiolules" constraintid="o5841" is_modifier="false" name="architecture" src="d0_s6" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o5841" name="petiolule" name_original="petiolules" src="d0_s6" type="structure" />
      <biological_entity id="o5842" name="stalk" name_original="stalks" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="wiry" value_original="wiry" />
      </biological_entity>
      <relation from="o5840" id="r1251" name="leaving" negation="false" src="d0_s6" to="o5842" />
    </statement>
    <statement id="d0_s7">
      <text>blade tissue glabrous abaxially.</text>
      <biological_entity constraint="blade" id="o5843" name="tissue" name_original="tissue" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="abaxially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Fertile pinnae on 0.5-1 cm stalks, oblong, 1-pinnate, 3-14 × 2.5-6 cm;</text>
      <biological_entity id="o5844" name="pinna" name_original="pinnae" src="d0_s8" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s8" value="fertile" value_original="fertile" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="oblong" value_original="oblong" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="1-pinnate" value_original="1-pinnate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s8" to="14" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_unit="cm" name="width" src="d0_s8" to="6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o5845" name="stalk" name_original="stalks" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" is_modifier="true" name="some_measurement" src="d0_s8" to="1" to_unit="cm" />
      </biological_entity>
      <relation from="o5844" id="r1252" name="on" negation="false" src="d0_s8" to="o5845" />
    </statement>
    <statement id="d0_s9">
      <text>ultimate segments ovate to lanceolate-oblong, fringed with fertile lobes, otherwise similar to sterile segments.</text>
      <biological_entity constraint="ultimate" id="o5846" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s9" to="lanceolate-oblong" />
        <character constraint="with lobes" constraintid="o5847" is_modifier="false" name="shape" src="d0_s9" value="fringed" value_original="fringed" />
      </biological_entity>
      <biological_entity id="o5847" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="fertile" value_original="fertile" />
      </biological_entity>
      <biological_entity id="o5848" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="true" name="reproduction" src="d0_s9" value="sterile" value_original="sterile" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Terrestrial on riverbanks, swamps (especially cypress swamps), cabbage palm hammocks, and other wet, disturbed sites.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="riverbanks" modifier="terrestrial on" />
        <character name="habitat" value="swamps" />
        <character name="habitat" value="cypress swamps" modifier="especially" />
        <character name="habitat" value="palm hammocks" modifier=") cabbage" />
        <character name="habitat" value="other wet" />
        <character name="habitat" value="disturbed sites" />
        <character name="habitat" value="terrestrial" modifier="especially" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Fla.; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <other_name type="common_name">Small-leaved climbing fern</other_name>
  <discussion>Lygodium microphyllum is native to southeastern Asia and recently naturalized. The species may be very abundant locally and may climb to a height of 9 meters in trees. Sometimes it forms thick mats covering considerable areas at ground level (J. Beckner 1968; C. E. Nauman and D. F. Austin 1978).</discussion>
  
</bio:treatment>