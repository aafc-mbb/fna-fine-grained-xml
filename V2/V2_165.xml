<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">332</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Mirbel" date="unknown" rank="family">marsileaceae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">marsilea</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">quadrifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1099. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family marsileaceae;genus marsilea;species quadrifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200005211</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants forming diffuse clones.</text>
      <biological_entity id="o12839" name="whole_organism" name_original="" src="" type="structure">
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o12840" name="clone" name_original="clones" src="d0_s0" type="structure">
        <character is_modifier="true" name="density" src="d0_s0" value="diffuse" value_original="diffuse" />
      </biological_entity>
      <relation from="o12839" id="r2800" name="forming" negation="false" src="d0_s0" to="o12840" />
    </statement>
    <statement id="d0_s1">
      <text>Roots arising at nodes and 1–3 on internodes.</text>
      <biological_entity id="o12841" name="root" name_original="roots" src="d0_s1" type="structure">
        <character constraint="at nodes" constraintid="o12842" is_modifier="false" name="orientation" src="d0_s1" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity id="o12842" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" constraint="on internodes" constraintid="o12843" from="1" name="quantity" src="d0_s1" to="3" />
      </biological_entity>
      <biological_entity id="o12843" name="internode" name_original="internodes" src="d0_s1" type="structure" />
    </statement>
    <statement id="d0_s2">
      <text>Petioles 5.4–16.5 cm, sparsely pubescent to glabrous.</text>
      <biological_entity id="o12844" name="petiole" name_original="petioles" src="d0_s2" type="structure">
        <character char_type="range_value" from="5.4" from_unit="cm" name="some_measurement" src="d0_s2" to="16.5" to_unit="cm" />
        <character char_type="range_value" from="sparsely pubescent" name="pubescence" src="d0_s2" to="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Pinnae 7–21 × 6–19 mm, sparsely pubescent to glabrous.</text>
      <biological_entity id="o12845" name="pinna" name_original="pinnae" src="d0_s3" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s3" to="21" to_unit="mm" />
        <character char_type="range_value" from="6" from_unit="mm" name="width" src="d0_s3" to="19" to_unit="mm" />
        <character char_type="range_value" from="sparsely pubescent" name="pubescence" src="d0_s3" to="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sporocarp stalks ascending, frequently branched, attached 1–12 mm above base of petiole;</text>
      <biological_entity constraint="sporocarp" id="o12846" name="stalk" name_original="stalks" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="frequently" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" name="fixation" src="d0_s4" value="attached" value_original="attached" />
        <character char_type="range_value" constraint="above base" constraintid="o12847" from="1" from_unit="mm" name="some_measurement" src="d0_s4" to="12" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12847" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity id="o12848" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <relation from="o12847" id="r2801" name="part_of" negation="false" src="d0_s4" to="o12848" />
    </statement>
    <statement id="d0_s5">
      <text>unbranched stalks or ultimate branches of stalks 3–16 mm;</text>
      <biological_entity id="o12849" name="stalk" name_original="stalks" src="d0_s5" type="structure" constraint="stalk" constraint_original="stalk; stalk">
        <character is_modifier="true" name="architecture" src="d0_s5" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="ultimate" id="o12850" name="branch" name_original="branches" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="unbranched" value_original="unbranched" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12851" name="stalk" name_original="stalks" src="d0_s5" type="structure" />
      <relation from="o12849" id="r2802" name="part_of" negation="false" src="d0_s5" to="o12851" />
      <relation from="o12850" id="r2803" name="part_of" negation="false" src="d0_s5" to="o12851" />
    </statement>
    <statement id="d0_s6">
      <text>common trunk of branched stalks 1–4 mm (rarely 2–3 unbranched stalks attached separately to same petiole).</text>
      <biological_entity id="o12852" name="trunk" name_original="trunk" src="d0_s6" type="structure" constraint="stalk" constraint_original="stalk; stalk">
        <character is_modifier="true" name="quantity" src="d0_s6" value="common" value_original="common" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12853" name="stalk" name_original="stalks" src="d0_s6" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s6" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o12852" id="r2804" name="part_of" negation="false" src="d0_s6" to="o12853" />
    </statement>
    <statement id="d0_s7">
      <text>Sporocarps perpendicular to ascending, 4–5.6 × 3–4 mm, 2.3–2.8 mm thick, rounded, oval, or elliptic in lateral view, pubescent but soon glabrate;</text>
      <biological_entity id="o12854" name="sporocarp" name_original="sporocarps" src="d0_s7" type="structure">
        <character char_type="range_value" from="perpendicular" name="orientation" src="d0_s7" to="ascending" />
        <character name="thickness" src="d0_s7" unit="mm" value="4-5.6×3-4" value_original="4-5.6×3-4" />
        <character char_type="range_value" from="2.3" from_unit="mm" name="thickness" src="d0_s7" to="2.8" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="shape" src="d0_s7" value="oval" value_original="oval" />
        <character is_modifier="false" modifier="in lateral view" name="shape" src="d0_s7" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="soon" name="pubescence" src="d0_s7" value="glabrate" value_original="glabrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>raphe 1.4–1.9 mm, proximal tooth usually absent, distal tooth absent or 0.1–0.2 mm.</text>
      <biological_entity id="o12855" name="raphe" name_original="raphe" src="d0_s8" type="structure">
        <character char_type="range_value" from="1.4" from_unit="mm" name="some_measurement" src="d0_s8" to="1.9" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o12856" name="tooth" name_original="tooth" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="usually" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="distal" id="o12857" name="tooth" name_original="tooth" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s8" value="0.1-0.2 mm" value_original="0.1-0.2 mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Sori 10–17.</text>
      <biological_entity id="o12858" name="sorus" name_original="sori" src="d0_s9" type="structure">
        <character char_type="range_value" from="10" name="quantity" src="d0_s9" to="17" />
      </biological_entity>
    </statement>
  </description>
  <number>1</number>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporocarps produced summer–fall (Jun–Oct).</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporocarps appearing time" char_type="range_value" to="fall" from="summer" />
        <character name="sporocarps appearing time" char_type="range_value" to="Oct" from="Jun" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>On mud and in shallow water.</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mud" modifier="on" />
        <character name="habitat" value="shallow water" modifier="and in" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Ont., Conn., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Mo., N.J., N.Y., Ohio, Pa.; se Europe; Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" value="se Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <discussion>Marsilea quadrifolia was introduced in Connecticut about 1860. Many of the localities from which it is known at present are artificial bodies of water. This may indicate intentional introduction of the plant as a curiosity.</discussion>
  <discussion>Because its leaves are glabrous to essentially glabrous, Marsilea quadrifolia is unlikely to be confused with any other Marsilea in the flora. Likewise, the petioles of the land leaves in this species tend to be procumbent rather than stiffly erect as in the others. The branched sporocarp stalks found in M. quadrifolia are found elsewhere only in M. macropoda; the latter, however, is a hairy plant and has no distal tooth on the very large sporocarp.</discussion>
  <discussion>Marsilea minuta Linnaeus, a widespread species in the paleotropics, has recently been collected from the Florida Panhandle. It resembles M. quadrifolia in having roots both at the nodes and on the internodes and in having relatively glabrous land leaves, but it has sporocarps that are only 1.3–1.7 mm thick, with a distal tooth 0.3–0.6 mm long. Marsilea minuta also has a tendency for the terminal margins of the land leaves to be crenate rather than entire.</discussion>
  
</bio:treatment>