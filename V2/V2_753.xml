<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Alan R. Smith</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
    <other_info_on_meta type="treatment_page">309</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Newman" date="unknown" rank="family">Grammitidaceae</taxon_name>
    <taxon_hierarchy>family Grammitidaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">10383</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, mostly small, on rock or commonly epiphytic [rarely terrestrial].</text>
      <biological_entity id="o6041" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="mostly" name="size" src="d0_s0" value="small" value_original="small" />
        <character is_modifier="false" modifier="commonly" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6042" name="rock" name_original="rock" src="d0_s0" type="structure" />
      <relation from="o6041" id="r1285" name="on" negation="false" src="d0_s0" to="o6042" />
    </statement>
    <statement id="d0_s1">
      <text>Stems long to short-creeping or suberect, usually unbranched, bearing scales [rarely scales absent], solenostelic (having phloem on both sides of xylem) to dictyostelic (having complex nets of xylem).</text>
      <biological_entity id="o6043" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character constraint="to bearing, scales" constraintid="o6044, o6045" is_modifier="false" name="length_or_size" src="d0_s1" value="long" value_original="long" />
        <character char_type="range_value" from="solenostelic" name="architecture" notes="" src="d0_s1" to="dictyostelic" />
      </biological_entity>
      <biological_entity id="o6044" name="bearing" name_original="bearing" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="short-creeping" value_original="short-creeping" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="suberect" value_original="suberect" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
      <biological_entity id="o6045" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s1" value="short-creeping" value_original="short-creeping" />
        <character is_modifier="true" name="orientation" src="d0_s1" value="suberect" value_original="suberect" />
        <character is_modifier="true" modifier="usually" name="architecture" src="d0_s1" value="unbranched" value_original="unbranched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves erect, arching, or pendent, monomorphic [rarely with specialized fertile areas], less than 50 cm [rarely longer], usually scaleless throughout.</text>
      <biological_entity id="o6046" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="arching" value_original="arching" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="pendent" value_original="pendent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="0" from_unit="cm" name="some_measurement" src="d0_s2" to="50" to_unit="cm" />
        <character is_modifier="false" modifier="usually; throughout" name="pubescence" src="d0_s2" value="scaleless" value_original="scaleless" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petioles often dark-colored and wiry, commonly terete, usually less than 2 mm diam., articulate or not articulate, with 1 or 2 vascular strands.</text>
      <biological_entity id="o6047" name="petiole" name_original="petioles" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s3" value="dark-colored" value_original="dark-colored" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="wiry" value_original="wiry" />
        <character is_modifier="false" modifier="commonly" name="shape" src="d0_s3" value="terete" value_original="terete" />
        <character char_type="range_value" from="0" from_unit="mm" modifier="usually" name="diameter" src="d0_s3" to="2" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="articulate" value_original="articulate" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o6048" name="strand" name_original="strands" src="d0_s3" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s3" value="1" value_original="1" />
      </biological_entity>
      <relation from="o6047" id="r1286" name="with" negation="false" src="d0_s3" to="o6048" />
    </statement>
    <statement id="d0_s4">
      <text>Blades simple and entire to commonly pinnatifid or 1-pinnate, rarely 2-pinnate or more divided, glabrous or commonly bearing hairs, especially on petioles and rachises;</text>
      <biological_entity id="o6049" name="blade" name_original="blades" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="simple" value_original="simple" />
        <character char_type="range_value" from="entire" name="shape" src="d0_s4" to="commonly pinnatifid" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="1-pinnate" value_original="1-pinnate" />
        <character is_modifier="false" modifier="rarely" name="architecture_or_shape" src="d0_s4" value="2-pinnate" value_original="2-pinnate" />
        <character is_modifier="false" name="shape" src="d0_s4" value="divided" value_original="divided" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s4" value="commonly" value_original="commonly" />
      </biological_entity>
      <biological_entity id="o6050" name="hair" name_original="hairs" src="d0_s4" type="structure" />
      <biological_entity id="o6051" name="petiole" name_original="petioles" src="d0_s4" type="structure" />
      <biological_entity id="o6052" name="rachis" name_original="rachises" src="d0_s4" type="structure" />
      <relation from="o6049" id="r1287" name="bearing" negation="false" src="d0_s4" to="o6050" />
      <relation from="o6049" id="r1288" modifier="especially" name="on" negation="false" src="d0_s4" to="o6051" />
      <relation from="o6049" id="r1289" modifier="especially" name="on" negation="false" src="d0_s4" to="o6052" />
    </statement>
    <statement id="d0_s5">
      <text>hairs tan to dark reddish-brown [or transparent], unicellular to multicellular;</text>
      <biological_entity id="o6053" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="tan" name="coloration" src="d0_s5" to="dark reddish-brown" />
        <character char_type="range_value" from="unicellular" name="architecture" src="d0_s5" to="multicellular" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>rachises often dark-colored, not grooved adaxially.</text>
      <biological_entity id="o6054" name="rachis" name_original="rachises" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s6" value="dark-colored" value_original="dark-colored" />
        <character is_modifier="false" modifier="not; adaxially" name="architecture" src="d0_s6" value="grooved" value_original="grooved" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Veins free [to anastomosing in simple patterns];</text>
      <biological_entity id="o6055" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s7" value="free" value_original="free" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>hydathodes present or absent, sometimes obscured by lime dots adaxially.</text>
      <biological_entity id="o6056" name="hydathode" name_original="hydathodes" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="sometimes" name="prominence" src="d0_s8" value="obscured" value_original="obscured" />
        <character constraint="by" is_modifier="false" modifier="adaxially" name="coloration" src="d0_s8" value="lime dots" value_original="lime dots" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Sori abaxial on veins, round to oblong [occasionally elongate];</text>
      <biological_entity id="o6057" name="sorus" name_original="sori" src="d0_s9" type="structure">
        <character constraint="on veins" constraintid="o6058" is_modifier="false" name="position" src="d0_s9" value="abaxial" value_original="abaxial" />
        <character char_type="range_value" from="round" name="shape" notes="" src="d0_s9" to="oblong" />
      </biological_entity>
      <biological_entity id="o6058" name="vein" name_original="veins" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>paraphyses present or absent, these glandular or hairlike;</text>
      <biological_entity id="o6059" name="paraphyse" name_original="paraphyses" src="d0_s10" type="structure">
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="presence" src="d0_s10" value="absent" value_original="absent" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="shape" src="d0_s10" value="hairlike" value_original="hairlike" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>sporangia with stalk of 1 row of cells;</text>
      <biological_entity id="o6060" name="sporangium" name_original="sporangia" src="d0_s11" type="structure" />
      <biological_entity id="o6061" name="stalk" name_original="stalk" src="d0_s11" type="structure" />
      <biological_entity id="o6062" name="row" name_original="row" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6063" name="cell" name_original="cells" src="d0_s11" type="structure" />
      <relation from="o6060" id="r1290" name="with" negation="false" src="d0_s11" to="o6061" />
      <relation from="o6061" id="r1291" name="part_of" negation="false" src="d0_s11" to="o6062" />
      <relation from="o6061" id="r1292" name="part_of" negation="false" src="d0_s11" to="o6063" />
    </statement>
    <statement id="d0_s12">
      <text>indusia absent.</text>
      <biological_entity id="o6064" name="indusium" name_original="indusia" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores greenish, tetrahedral-globose, trilete, surface commonly papillate.</text>
      <biological_entity id="o6065" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s13" value="tetrahedral-globose" value_original="tetrahedral-globose" />
        <character is_modifier="false" name="architecture" src="d0_s13" value="trilete" value_original="trilete" />
      </biological_entity>
      <biological_entity id="o6066" name="surface" name_original="surface" src="d0_s13" type="structure">
        <character is_modifier="false" modifier="commonly" name="relief" src="d0_s13" value="papillate" value_original="papillate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Gametophytes greenish, borne aboveground, ribbon-shaped, sometimes bearing multicellular gemmae.</text>
      <biological_entity id="o6067" name="gametophyte" name_original="gametophytes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o6068" name="gemma" name_original="gemmae" src="d0_s14" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s14" value="multicellular" value_original="multicellular" />
        <character is_modifier="true" name="location" src="d0_s14" value="aboveground" value_original="aboveground" />
        <character is_modifier="true" name="shape" src="d0_s14" value="ribbon--shaped" value_original="ribbon--shaped" />
      </biological_entity>
      <relation from="o6067" id="r1293" name="borne" negation="false" src="d0_s14" to="o6068" />
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Mostly tropical and subtropical, North America, ca. 250 in Mexico, West Indies, Central America, South America.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Mostly tropical and subtropical" establishment_means="native" />
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="ca. 250 in Mexico" establishment_means="native" />
        <character name="distribution" value="West Indies" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>21</number>
  <discussion>Circumscription of genera is unsettled and in some cases arbitrarily based on blade dissection. Generic limits need redefinition and are the subject of current study. The family description above encompasses the worldwide variation. Relationships of the family have generally been thought to be with the Polypodiaceae with which the family is sometimes combined, but that is open to question. Most species are epiphytic, and many occur in cloud forests at middle and high elevations.</discussion>
  <discussion>Genera ca. 10 or more, species ca. 500 or more (1 genus, 1 species in the flora).</discussion>
  
</bio:treatment>