<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Agardh" date="unknown" rank="family">ophioglossaceae</taxon_name>
    <taxon_name authority="Swartz" date="1801" rank="genus">botrychium</taxon_name>
    <taxon_name authority="Lyon" date="unknown" rank="subgenus">sceptridium</taxon_name>
    <taxon_name authority="Sect. Sceptridium (Lyon) R. T. Clausen" date="1938" rank="section">Sceptridium</taxon_name>
    <taxon_name authority="(Gilbert) House" date="1905" rank="species">oneidense</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>7: 126. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ophioglossaceae;genus botrychium;subgenus sceptridium;section sceptridium;species oneidense;</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500287</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Botrychium</taxon_name>
    <taxon_name authority="(Thunberg) Swartz" date="unknown" rank="species">ternatum</taxon_name>
    <taxon_name authority="Gilbert" date="unknown" rank="variety">oneidense</taxon_name>
    <place_of_publication>
      <publication_title>Fern Bull.</publication_title>
      <place_in_publication>9: 27. 1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Botrychium;species ternatum;variety oneidense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Botrychium</taxon_name>
    <taxon_name authority="Sprengel" date="unknown" rank="species">dissectum</taxon_name>
    <taxon_name authority="(Gilbert) Farwell" date="unknown" rank="variety">oneidense</taxon_name>
    <taxon_hierarchy>genus Botrychium;species dissectum;variety oneidense;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Botrychium</taxon_name>
    <taxon_name authority="(S.G. Gmelin) Ruprecht" date="unknown" rank="species">multifidum</taxon_name>
    <taxon_name authority="(Gilbert) Farwell" date="unknown" rank="variety">oneidense</taxon_name>
    <taxon_hierarchy>genus Botrychium;species multifidum;variety oneidense;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trophophore stalk 2–15 cm, 1.5–2.5 times length of blade rachis;</text>
      <biological_entity constraint="trophophore" id="o14037" name="stalk" name_original="stalk" src="d0_s0" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s0" to="15" to_unit="cm" />
        <character constraint="rachis" constraintid="o14038" is_modifier="false" name="length" src="d0_s0" value="1.5-2.5 times length of blade rachis" />
      </biological_entity>
      <biological_entity constraint="blade" id="o14038" name="rachis" name_original="rachis" src="d0_s0" type="structure" />
    </statement>
    <statement id="d0_s1">
      <text>blade dull bluish green, ± plane, 2–3-pinnate, to 15 × 20 cm, ± leathery.</text>
      <biological_entity id="o14039" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s1" value="dull" value_original="dull" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="bluish green" value_original="bluish green" />
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s1" value="plane" value_original="plane" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="2-3-pinnate" value_original="2-3-pinnate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="15" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s1" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s1" value="leathery" value_original="leathery" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pinnae to 5 pairs, usually remote, horizontal to ascending, distance between 1st and 2d pinnae not or slightly more than between 2d and 3d pairs, undivided except in proximal 2/3–3/4.</text>
      <biological_entity constraint="between pinnae" constraintid="o14041" id="o14040" name="pinna" name_original="pinnae" src="d0_s2" type="structure" constraint_original="between  pinnae, ">
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="5" />
        <character is_modifier="false" modifier="usually" name="arrangement_or_density" src="d0_s2" value="remote" value_original="remote" />
        <character char_type="range_value" from="horizontal" name="orientation" src="d0_s2" to="ascending" />
        <character is_modifier="false" modifier="slightly; between 2d and 3d pairs" name="architecture_or_shape" src="d0_s2" value="undivided" value_original="undivided" />
        <character is_modifier="false" name="position" src="d0_s2" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="2/3" name="quantity" src="d0_s2" to="3/4" />
      </biological_entity>
      <biological_entity id="o14041" name="pinna" name_original="pinnae" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Pinnules obliquely ovate, margins finely crenulate to denticulate, apex rounded to acute, venation pinnate.</text>
      <biological_entity id="o14042" name="pinnule" name_original="pinnules" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="obliquely" name="shape" src="d0_s3" value="ovate" value_original="ovate" />
      </biological_entity>
      <biological_entity id="o14043" name="margin" name_original="margins" src="d0_s3" type="structure">
        <character char_type="range_value" from="finely crenulate" name="shape" src="d0_s3" to="denticulate" />
      </biological_entity>
      <biological_entity id="o14044" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character char_type="range_value" from="rounded" name="shape" src="d0_s3" to="acute" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Sporophores 2–3-pinnate, 1.5–2.5 times length of trophophore.</text>
      <biological_entity id="o14046" name="trophophore" name_original="trophophore" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>2n =90.</text>
      <biological_entity id="o14045" name="sporophore" name_original="sporophores" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="2-3-pinnate" value_original="2-3-pinnate" />
        <character constraint="trophophore" constraintid="o14046" is_modifier="false" name="length" src="d0_s4" value="1.5-2.5 times length of trophophore" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14047" name="chromosome" name_original="" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="90" value_original="90" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Leaves green over winter, sporophores seasonal, new leaves appearing in spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="leaves appearing time" char_type="range_value" to="spring" from="spring" />
        <character name="leaves appearing time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>In moist, shady, acidic woods and swamps</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist" modifier="in" />
        <character name="habitat" value="shady" />
        <character name="habitat" value="acidic woods" />
        <character name="habitat" value="swamps" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., Ont., Que.; Conn., Del., D.C., Ind., Ky., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7</number>
  <other_name type="common_name">Blunt-lobed grapefern</other_name>
  <other_name type="common_name">botryche d'oneida</other_name>
  <discussion>Botrychium oneidense commonly occurs with B. dissectum and B. multifidum. Young individuals of both may resemble B. oneidense (W.H. Wagner Jr. 1961b).</discussion>
  
</bio:treatment>