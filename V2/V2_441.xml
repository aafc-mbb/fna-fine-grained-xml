<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="R. Brown" date="1810" rank="genus">woodsia</taxon_name>
    <taxon_name authority="Lemmon" date="1882" rank="species">plummerae</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>7: 6. 1882</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus woodsia;species plummerae</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">233501353</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Woodsia</taxon_name>
    <taxon_name authority="(Sprengel) Torrey" date="unknown" rank="species">obtusa</taxon_name>
    <taxon_name authority="D. C. Eaton &amp; M. Faxon" date="unknown" rank="variety">glandulosa</taxon_name>
    <taxon_hierarchy>genus Woodsia;species obtusa;variety glandulosa;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Woodsia</taxon_name>
    <taxon_name authority="E. Fournier" date="unknown" rank="species">obtusa</taxon_name>
    <taxon_name authority="(Lemmon) Maxon" date="unknown" rank="variety">plummerae</taxon_name>
    <taxon_hierarchy>genus Woodsia;species obtusa;variety plummerae;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Woodsia</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">pusilla</taxon_name>
    <taxon_name authority="(D. C. Eaton &amp; M. Faxon) T. M. C. Taylor" date="unknown" rank="variety">glandulosa</taxon_name>
    <taxon_hierarchy>genus Woodsia;species pusilla;variety glandulosa;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems compact, erect to ascending, with a few persistent petiole bases of unequal lengths;</text>
      <biological_entity id="o12733" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="ascending" />
      </biological_entity>
      <biological_entity constraint="petiole" id="o12734" name="base" name_original="bases" src="d0_s0" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s0" value="few" value_original="few" />
        <character is_modifier="true" name="duration" src="d0_s0" value="persistent" value_original="persistent" />
      </biological_entity>
      <relation from="o12733" id="r2768" modifier="of unequal lengths" name="with" negation="false" src="d0_s0" to="o12734" />
    </statement>
    <statement id="d0_s1">
      <text>scales often uniformly brown but at least some bicolored with dark central stripe and pale-brown margins, narrowly lanceolate.</text>
      <biological_entity id="o12735" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="often uniformly" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character constraint="with margins" constraintid="o12737" is_modifier="false" modifier="at-least" name="coloration" src="d0_s1" value="bicolored" value_original="bicolored" />
        <character is_modifier="false" modifier="narrowly" name="shape" notes="" src="d0_s1" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity constraint="central" id="o12736" name="stripe" name_original="stripe" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="dark" value_original="dark" />
      </biological_entity>
      <biological_entity id="o12737" name="margin" name_original="margins" src="d0_s1" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s1" value="pale-brown" value_original="pale-brown" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves 5–25 × 1.5–6 cm.</text>
      <biological_entity id="o12738" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s2" to="25" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="width" src="d0_s2" to="6" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole reddish-brown to dark purple when mature, not articulate above base, somewhat pliable and resistant to shattering.</text>
      <biological_entity id="o12739" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="reddish-brown" modifier="when mature" name="coloration" src="d0_s3" to="dark purple" />
        <character constraint="above base" constraintid="o12740" is_modifier="false" modifier="not" name="architecture" src="d0_s3" value="articulate" value_original="articulate" />
        <character is_modifier="false" modifier="somewhat" name="fragility_or_texture" notes="" src="d0_s3" value="pliable" value_original="pliable" />
      </biological_entity>
      <biological_entity id="o12740" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Blade lanceolate to ovate, usually 2-pinnate proximally, densely glandular, often somewhat viscid;</text>
    </statement>
    <statement id="d0_s5">
      <text>most glandular-hairs with thick stalks and distinctly bulbous tips;</text>
      <biological_entity id="o12741" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate" />
        <character is_modifier="false" modifier="usually; proximally" name="architecture_or_shape" src="d0_s4" value="2-pinnate" value_original="2-pinnate" />
        <character is_modifier="false" modifier="densely" name="architecture_or_function_or_pubescence" src="d0_s4" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="often somewhat" name="coating" src="d0_s4" value="viscid" value_original="viscid" />
      </biological_entity>
      <biological_entity id="o12742" name="glandular-hair" name_original="glandular-hairs" src="d0_s5" type="structure" />
      <biological_entity id="o12743" name="stalk" name_original="stalks" src="d0_s5" type="structure">
        <character is_modifier="true" name="width" src="d0_s5" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o12744" name="tip" name_original="tips" src="d0_s5" type="structure">
        <character is_modifier="true" modifier="distinctly" name="architecture" src="d0_s5" value="bulbous" value_original="bulbous" />
      </biological_entity>
      <relation from="o12742" id="r2769" name="with" negation="false" src="d0_s5" to="o12743" />
      <relation from="o12742" id="r2770" name="with" negation="false" src="d0_s5" to="o12744" />
    </statement>
    <statement id="d0_s6">
      <text>rachis with abundant glandular-hairs and a few narrow scales.</text>
      <biological_entity id="o12745" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity id="o12746" name="glandular-hair" name_original="glandular-hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="abundant" value_original="abundant" />
      </biological_entity>
      <biological_entity id="o12747" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="few" value_original="few" />
        <character is_modifier="true" name="size_or_width" src="d0_s6" value="narrow" value_original="narrow" />
      </biological_entity>
      <relation from="o12745" id="r2771" name="with" negation="false" src="d0_s6" to="o12746" />
      <relation from="o12745" id="r2772" name="with" negation="false" src="d0_s6" to="o12747" />
    </statement>
    <statement id="d0_s7">
      <text>Pinnae ovate-deltate to elliptic, longer than wide, abruptly tapered to a rounded or broadly acute apex, occasionally attenuate;</text>
      <biological_entity id="o12748" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character char_type="range_value" from="ovate-deltate" name="shape" src="d0_s7" to="elliptic" />
        <character is_modifier="false" name="length_or_size" src="d0_s7" value="longer than wide" value_original="longer than wide" />
        <character constraint="to apex" constraintid="o12749" is_modifier="false" modifier="abruptly" name="shape" src="d0_s7" value="tapered" value_original="tapered" />
        <character is_modifier="false" modifier="occasionally" name="shape" notes="" src="d0_s7" value="attenuate" value_original="attenuate" />
      </biological_entity>
      <biological_entity id="o12749" name="apex" name_original="apex" src="d0_s7" type="structure">
        <character is_modifier="true" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
        <character is_modifier="true" modifier="broadly" name="shape" src="d0_s7" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>largest pinnae with 5–11 pairs of pinnules;</text>
      <biological_entity constraint="largest" id="o12750" name="pinna" name_original="pinnae" src="d0_s8" type="structure" />
      <biological_entity id="o12751" name="pair" name_original="pairs" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="11" />
      </biological_entity>
      <biological_entity id="o12752" name="pinnule" name_original="pinnules" src="d0_s8" type="structure" />
      <relation from="o12750" id="r2773" name="with" negation="false" src="d0_s8" to="o12751" />
      <relation from="o12751" id="r2774" name="part_of" negation="false" src="d0_s8" to="o12752" />
    </statement>
    <statement id="d0_s9">
      <text>abaxial and adaxial surfaces glandular, lacking nonglandular hairs or scales.</text>
      <biological_entity constraint="abaxial and adaxial" id="o12753" name="surface" name_original="surfaces" src="d0_s9" type="structure" />
      <biological_entity constraint="abaxial and adaxial" id="o12754" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity constraint="abaxial and adaxial" id="o12755" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture_or_function_or_pubescence" src="d0_s9" value="glandular" value_original="glandular" />
        <character is_modifier="true" name="quantity" src="d0_s9" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Pinnules dentate, often shallowly lobed;</text>
      <biological_entity id="o12756" name="pinnule" name_original="pinnules" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="dentate" value_original="dentate" />
        <character is_modifier="false" modifier="often shallowly" name="shape" src="d0_s10" value="lobed" value_original="lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>margins nonlustrous, thin, densely glandular, lacking cilia but with occasional 1–2-celled translucent projections.</text>
      <biological_entity id="o12757" name="margin" name_original="margins" src="d0_s11" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s11" value="nonlustrous" value_original="nonlustrous" />
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
        <character is_modifier="false" modifier="densely" name="architecture_or_function_or_pubescence" src="d0_s11" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o12758" name="cilium" name_original="cilia" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="lacking" value_original="lacking" />
      </biological_entity>
      <biological_entity id="o12759" name="projection" name_original="projections" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="occasional" value_original="occasional" />
        <character is_modifier="true" name="architecture" src="d0_s11" value="1-2-celled" value_original="1-2-celled" />
        <character is_modifier="true" name="coloration_or_reflectance" src="d0_s11" value="translucent" value_original="translucent" />
      </biological_entity>
      <relation from="o12758" id="r2775" name="with" negation="false" src="d0_s11" to="o12759" />
    </statement>
    <statement id="d0_s12">
      <text>Vein tips slightly (if at all) enlarged, barely visible adaxially.</text>
      <biological_entity constraint="vein" id="o12760" name="tip" name_original="tips" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="slightly" name="size" src="d0_s12" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="barely; adaxially" name="prominence" src="d0_s12" value="visible" value_original="visible" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Indusia of relatively broad segments;</text>
      <biological_entity id="o12761" name="indusium" name_original="indusia" src="d0_s13" type="structure" />
      <biological_entity id="o12762" name="segment" name_original="segments" src="d0_s13" type="structure">
        <character is_modifier="true" modifier="relatively" name="width" src="d0_s13" value="broad" value_original="broad" />
      </biological_entity>
      <relation from="o12761" id="r2776" name="part_of" negation="false" src="d0_s13" to="o12762" />
    </statement>
    <statement id="d0_s14">
      <text>segments multiseriate for most of length, often divided and uniseriate distally, composed of ± isodiametric cells, often surpassing mature sporangia.</text>
      <biological_entity id="o12763" name="segment" name_original="segments" src="d0_s14" type="structure">
        <character constraint="for" is_modifier="false" name="length" src="d0_s14" value="multiseriate" value_original="multiseriate" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s14" value="divided" value_original="divided" />
        <character is_modifier="false" modifier="distally" name="architecture_or_arrangement" src="d0_s14" value="uniseriate" value_original="uniseriate" />
      </biological_entity>
      <biological_entity id="o12764" name="cell" name_original="cells" src="d0_s14" type="structure">
        <character is_modifier="true" modifier="more or less" name="architecture_or_shape" src="d0_s14" value="isodiametric" value_original="isodiametric" />
      </biological_entity>
      <biological_entity id="o12765" name="sporangium" name_original="sporangia" src="d0_s14" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s14" value="mature" value_original="mature" />
      </biological_entity>
      <relation from="o12763" id="r2777" name="composed of" negation="false" src="d0_s14" to="o12764" />
      <relation from="o12763" id="r2778" modifier="often" name="surpassing" negation="false" src="d0_s14" to="o12765" />
    </statement>
    <statement id="d0_s15">
      <text>Spores averaging 44–50 µm. 2n = 152.</text>
      <biological_entity id="o12766" name="spore" name_original="spores" src="d0_s15" type="structure">
        <character char_type="range_value" from="44" from_unit="um" name="some_measurement" src="d0_s15" to="50" to_unit="um" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12767" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="152" value_original="152" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating late spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs and rocky slopes, usually on granite or volcanic substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="volcanic substrates" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>700–3100 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3100" to_unit="m" from="700" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., N.Mex., Okla., Tex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5</number>
  <other_name type="common_name">Plummer's cliff fern</other_name>
  <discussion>The origin and phylogenetic affinities of the tetraploid Woodsia plummerae have not been established with certainty. The hypothesis that it arose as a hybrid between the Mexican species W. mollis (Kaulfuss) J. Smith and W. mexicana Fée (D. F. M. Brown 1964; D. B. Lellinger 1985) seems untenable in light of recent chromosome studies indicating that the latter species is also tetraploid (M. D. Windham 1993). On the basis of sporophyte morphology and spore ornamentation, W. plummerae appears most closely related to the W. mexicana complex and W. oregana. In fact, W. oregana can be difficult to separate from W. plummerae in western New Mexico and northern Arizona. Intermediate plants occurring in this region may represent stable allotetraploids resulting from hybridization between the diploid progenitors of W. plummerae and W. oregana subsp. cathcartiana. Considering the available evidence, populations of W. plummerae in the United States probably originated through autopolyploidy from a recently discovered, but as yet unnamed, Mexican diploid of similar morphology. Woodsia plummerae occasionally hybridizes with W. phillipsii to produce sterile, morphologically intermediate triploids.</discussion>
  
</bio:treatment>