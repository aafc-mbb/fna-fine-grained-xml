<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Clifton E. Nauman</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Schott" date="1834" rank="genus">Nephrolepis</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Fil.</publication_title>
      <place_in_publication>plate 3. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus Nephrolepis</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek nephros, kidney, and lepis, scale, in reference to shape of the indusia</other_info_on_name>
    <other_info_on_name type="fna_id">122157</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants terrestrial, epiphytic, or on rock.</text>
      <biological_entity id="o1450" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character is_modifier="false" name="habitat" src="d0_s0" value="epiphytic" value_original="epiphytic" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o1451" name="rock" name_original="rock" src="d0_s0" type="structure" />
      <relation from="o1450" id="r338" name="on" negation="false" src="d0_s0" to="o1451" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending to erect, bearing wiry stolons and sometimes underground tubers.</text>
      <biological_entity id="o1452" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="ascending" name="orientation" src="d0_s1" to="erect" />
      </biological_entity>
      <biological_entity id="o1453" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s1" value="wiry" value_original="wiry" />
      </biological_entity>
      <biological_entity id="o1454" name="tuber" name_original="tubers" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="sometimes" name="location" src="d0_s1" value="underground" value_original="underground" />
      </biological_entity>
      <relation from="o1452" id="r339" name="bearing" negation="false" src="d0_s1" to="o1453" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves monomorphic, evergreen.</text>
      <biological_entity id="o1455" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="duration" src="d0_s2" value="evergreen" value_original="evergreen" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole ca. 1/10–1/2 length of blade, base not swollen;</text>
      <biological_entity id="o1456" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1/10 length of blade" name="length" src="d0_s3" to="1/2 length of blade" />
      </biological_entity>
      <biological_entity id="o1457" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s3" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>vascular-bundles more than 3, arranged in an arc, ± round in cross-section.</text>
      <biological_entity id="o1458" name="vascular-bundle" name_original="vascular-bundles" src="d0_s4" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s4" upper_restricted="false" />
        <character constraint="in arc" constraintid="o1459" is_modifier="false" name="arrangement" src="d0_s4" value="arranged" value_original="arranged" />
        <character constraint="in cross-section" constraintid="o1460" is_modifier="false" modifier="more or less" name="shape" notes="" src="d0_s4" value="round" value_original="round" />
      </biological_entity>
      <biological_entity id="o1459" name="arc" name_original="arc" src="d0_s4" type="structure" />
      <biological_entity id="o1460" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Blade narrowly elliptic to linear-lanceolate, 1-pinnate (to 4–5-pinnate in various cultivated forms), very gradually reduced distally to minute pinnatifid apex, often seemingly indeterminate with apex never expanded, herbaceous to papery.</text>
      <biological_entity id="o1461" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="narrowly elliptic" name="shape" src="d0_s5" to="linear-lanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="1-pinnate" value_original="1-pinnate" />
        <character constraint="to apex" constraintid="o1462" is_modifier="false" modifier="very gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character constraint="with apex" constraintid="o1463" is_modifier="false" modifier="often seemingly" name="development" notes="" src="d0_s5" value="indeterminate" value_original="indeterminate" />
        <character char_type="range_value" from="herbaceous" name="texture" notes="" src="d0_s5" to="papery" />
      </biological_entity>
      <biological_entity id="o1462" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" name="size" src="d0_s5" value="minute" value_original="minute" />
        <character is_modifier="true" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o1463" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="never" name="size" src="d0_s5" value="expanded" value_original="expanded" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pinnae articulate to rachis, sometimes deciduous, segment (pinna) margins entire, crenulate, or biserrate;</text>
      <biological_entity id="o1464" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character constraint="to rachis" constraintid="o1465" is_modifier="false" name="architecture" src="d0_s6" value="articulate" value_original="articulate" />
        <character is_modifier="false" modifier="sometimes" name="duration" notes="" src="d0_s6" value="deciduous" value_original="deciduous" />
      </biological_entity>
      <biological_entity id="o1465" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity constraint="segment" id="o1466" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="biserrate" value_original="biserrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal pinnae (usually several pairs) slightly to greatly reduced, sessile, equilateral or inequilateral with basiscopic base excised and often an acroscopic basal auricle;</text>
      <biological_entity constraint="proximal" id="o1467" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="slightly to greatly" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="shape" src="d0_s7" value="equilateral" value_original="equilateral" />
        <character constraint="with base" constraintid="o1468" is_modifier="false" name="shape" src="d0_s7" value="inequilateral" value_original="inequilateral" />
      </biological_entity>
      <biological_entity id="o1468" name="base" name_original="base" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="basiscopic" value_original="basiscopic" />
      </biological_entity>
      <biological_entity constraint="basal" id="o1469" name="auricle" name_original="auricle" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="often" name="orientation" src="d0_s7" value="acroscopic" value_original="acroscopic" />
      </biological_entity>
      <relation from="o1468" id="r340" name="excised" negation="false" src="d0_s7" to="o1469" />
    </statement>
    <statement id="d0_s8">
      <text>costae adaxially grooved, grooves not continuous from rachis to costae;</text>
      <biological_entity id="o1470" name="costa" name_original="costae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s8" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o1471" name="groove" name_original="grooves" src="d0_s8" type="structure">
        <character constraint="from rachis" constraintid="o1472" is_modifier="false" modifier="not" name="architecture" src="d0_s8" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o1472" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
      <biological_entity id="o1473" name="costa" name_original="costae" src="d0_s8" type="structure" />
      <relation from="o1472" id="r341" name="to" negation="false" src="d0_s8" to="o1473" />
    </statement>
    <statement id="d0_s9">
      <text>indument of linear-lanceolate scales and sometimes multicellular hairs on abaxial and sometimes adaxial surfaces.</text>
      <biological_entity id="o1474" name="indument" name_original="indument" src="d0_s9" type="structure" constraint="scale" constraint_original="scale; scale" />
      <biological_entity id="o1475" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="true" name="shape" src="d0_s9" value="linear-lanceolate" value_original="linear-lanceolate" />
      </biological_entity>
      <biological_entity id="o1476" name="hair" name_original="hairs" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s9" value="multicellular" value_original="multicellular" />
      </biological_entity>
      <biological_entity id="o1477" name="surface" name_original="surfaces" src="d0_s9" type="structure">
        <character is_modifier="true" name="position" src="d0_s9" value="abaxial" value_original="abaxial" />
        <character is_modifier="true" modifier="sometimes" name="position" src="d0_s9" value="adaxial" value_original="adaxial" />
      </biological_entity>
      <relation from="o1474" id="r342" name="part_of" negation="false" src="d0_s9" to="o1475" />
      <relation from="o1474" id="r343" name="part_of" negation="false" src="d0_s9" to="o1476" />
      <relation from="o1474" id="r344" name="on" negation="false" src="d0_s9" to="o1477" />
    </statement>
    <statement id="d0_s10">
      <text>Veins free, forked.</text>
      <biological_entity id="o1478" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
        <character is_modifier="false" name="shape" src="d0_s10" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sori ± round;</text>
      <biological_entity id="o1479" name="sorus" name_original="sori" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s11" value="round" value_original="round" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>indusia round-reniform and with deep sinus to semicircular with broad sinus or lunate without sinus and seemingly laterally attached, persistent.</text>
      <biological_entity id="o1480" name="indusium" name_original="indusia" src="d0_s12" type="structure">
        <character is_modifier="false" name="shape" src="d0_s12" value="round-reniform" value_original="round-reniform" />
        <character is_modifier="false" modifier="seemingly laterally" name="fixation" notes="" src="d0_s12" value="attached" value_original="attached" />
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity id="o1481" name="sinus" name_original="sinus" src="d0_s12" type="structure">
        <character is_modifier="true" name="depth" src="d0_s12" value="deep" value_original="deep" />
        <character constraint="with " constraintid="o1483" is_modifier="false" name="arrangement_or_shape" src="d0_s12" value="semicircular" value_original="semicircular" />
      </biological_entity>
      <biological_entity id="o1482" name="sinus" name_original="sinus" src="d0_s12" type="structure">
        <character is_modifier="true" name="width" src="d0_s12" value="broad" value_original="broad" />
      </biological_entity>
      <biological_entity id="o1483" name="sinus" name_original="sinus" src="d0_s12" type="structure">
        <character is_modifier="true" name="shape" src="d0_s12" value="lunate" value_original="lunate" />
      </biological_entity>
      <relation from="o1480" id="r345" name="with" negation="false" src="d0_s12" to="o1481" />
    </statement>
    <statement id="d0_s13">
      <text>Spores brownish, tuberculate to rugose.</text>
    </statement>
    <statement id="d0_s14">
      <text>x = 41.</text>
      <biological_entity id="o1484" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brownish" value_original="brownish" />
        <character char_type="range_value" from="tuberculate" name="relief" src="d0_s13" to="rugose" />
      </biological_entity>
      <biological_entity constraint="x" id="o1485" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="41" value_original="41" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Widespread in tropical areas.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Widespread in tropical areas" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>18</number>
  <other_name type="common_name">Boston fern</other_name>
  <discussion>Nephrolepis often has veins ending in hydathodes and whitish lime-dots adaxially.</discussion>
  <discussion>Cultivars of Nephrolepis occasionally are found in the wild, where they persist for some time. Numerous forms of N. exaltata cv. `Bostoniensis' and its derivatives are widely cultivated, and the following are known from Florida: N. exaltata cv. `Bostoniensis', N. exaltata cv. `Elegantissima' complex, N. exaltata cv. `Florida Ruffles', N. exaltata cv. `M. P. Mills'.</discussion>
  <discussion>Nephrolepis falcata forma furcans (T. Moore in Nicholson) Proctor resembles N. biserrata in size, pinna shape, and sori, but it differs characteristically in having forking pinnae and rachises. It is widely cultivated and persists when escaped; it is not known to spread from spores. It is known in the literature under the following names: Aspidium biserratum Swartz var. furcans (T. Moore in Nicholson) Farwell, Nephrolepis biserrata (Swartz) Schott var. furcans (T. Moore in Nicholson) Hortus ex Bailey, and Nephrolepis davallioides var. furcans T. Moore in Nicholson.</discussion>
  <discussion>Nephrolepis hirsutula (G. Forster) C. Presl cv. `Superba' has irregularly pinnatisect, elliptic pinnae and a dense covering of reddish orange scales over most of the leaf surfaces.</discussion>
  <discussion>The report of Nephrolepis pectinata (Willdenow) Schott for Florida by E. T. Wherry (1964) was based on a misdetermination (T. Darling Jr. 1982).</discussion>
  <discussion>Species 25–30 (4 in the flora).</discussion>
  <references>
    <reference>Darling, T. Jr. 1982. The deletion of Nephrolepis pectinata from the flora of Florida. Amer. Fern J. 72: 63.</reference>
    <reference>Nauman, C. E. 1985. A Systematic Revision of the Neotropical Species of Nephrolepis Schott. Ph.D. dissertation. University of Tennessee.</reference>
    <reference>Nauman, C. E. 1981. The genus Nephrolepis in Florida. Amer. Fern J. 71: 35–40.</reference>
    <reference>Wherry, E. T. 1964. The Southern Fern Guide. Garden City, N.Y.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Adaxial costae of central pinnae sparsely to densely covered with short, erect hairs (often also with scales).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Adaxial costae of central pinnae glabrous, with or without scales.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Mature petioles at base covered moderately to densely with appressed, dark brown scales with pale margins.</description>
      <determination>1 Nephrolepis multiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Mature petioles at base often with a few loose, reddish to light brown, concolored scales.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Adaxial costae sparsely hairy, hairs ca. 0.5 mm; pinnae mostly falcate.</description>
      <determination>2 Nephrolepis ×averyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Adaxial costae densely hairy to tomentose, hairs 0.2-0.4 mm; pinnae not falcate or only slightly so.</description>
      <determination>3 Nephrolepis biserrata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Indusia circular and peltate or horseshoe-shaped and attached at narrow sinus, largest ca. 1 mm wide; pinnae usually more than 5 cm, often with conspicuous hairs 0.3-0.4 mm on blade surface.</description>
      <determination>3 Nephrolepis biserrata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Indusia reniform, horseshoe-shaped, or lunate to deltate-rounded and attached by narrow to broad sinus, 1.1-1.7 mm wide or wider; pinnae usually less than 5 cm, without hairs or hairs less than 0.3 mm and inconspicuous.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants with or without tubers; adaxial rachis scales distinctly bicolored (pale with darker point of attachment), often dense; points of pinna attachment 5-12 mm apart; pinnae glabrous; indusia lunate to deltate-rounded or reniform.</description>
      <determination>4 Nephrolepis cordifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants never bearing tubers; adaxial rachis scales concolored or indistinctly bicolored, dense to sparse; points of pinna attachment 7-21 mm apart; pinnae with a few scales near costae; indusia usually reniform to horseshoe-shaped.</description>
      <determination>5 Nephrolepis exaltata</determination>
    </key_statement>
  </key>
</bio:treatment>