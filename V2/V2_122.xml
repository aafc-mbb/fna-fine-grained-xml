<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">larix</taxon_name>
    <taxon_name authority="Nuttall" date="1849" rank="species">occidentalis</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Sylv.</publication_title>
      <place_in_publication>3: 143, plate 120. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus larix;species occidentalis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500745</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 50m;</text>
      <biological_entity id="o14132" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="50" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 2m diam., usually (when forest grown) branch-free over most of height;</text>
      <biological_entity id="o14133" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="2" to_unit="m" />
        <character is_modifier="false" modifier="usually" name="height" src="d0_s1" value="branch-free" value_original="branch-free" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>crown short, conic.</text>
      <biological_entity id="o14134" name="crown" name_original="crown" src="d0_s2" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
        <character is_modifier="false" name="shape" src="d0_s2" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark reddish-brown, scaly, with deep furrows between flat, flaky, cinnamon-colored plates.</text>
      <biological_entity id="o14135" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="reddish-brown" value_original="reddish-brown" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
      </biological_entity>
      <biological_entity constraint="between plates" constraintid="o14137" id="o14136" name="furrow" name_original="furrows" src="d0_s3" type="structure" constraint_original="between  plates, ">
        <character is_modifier="true" name="depth" src="d0_s3" value="deep" value_original="deep" />
      </biological_entity>
      <biological_entity id="o14137" name="plate" name_original="plates" src="d0_s3" type="structure">
        <character is_modifier="true" name="prominence_or_shape" src="d0_s3" value="flat" value_original="flat" />
        <character is_modifier="true" name="fragility" src="d0_s3" value="flaky" value_original="flaky" />
        <character is_modifier="true" name="coloration" src="d0_s3" value="cinnamon-colored" value_original="cinnamon-colored" />
      </biological_entity>
      <relation from="o14135" id="r3095" name="with" negation="false" src="d0_s3" to="o14136" />
    </statement>
    <statement id="d0_s4">
      <text>Branches horizontal, occasionally drooping in lower crown of open-grown trees;</text>
      <biological_entity id="o14138" name="branch" name_original="branches" src="d0_s4" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s4" value="horizontal" value_original="horizontal" />
        <character constraint="in lower crown" constraintid="o14139" is_modifier="false" modifier="occasionally" name="orientation" src="d0_s4" value="drooping" value_original="drooping" />
      </biological_entity>
      <biological_entity constraint="lower" id="o14139" name="crown" name_original="crown" src="d0_s4" type="structure" />
      <biological_entity id="o14140" name="tree" name_original="trees" src="d0_s4" type="structure">
        <character is_modifier="true" name="habitat" src="d0_s4" value="open-grown" value_original="open-grown" />
      </biological_entity>
      <relation from="o14139" id="r3096" name="part_of" negation="false" src="d0_s4" to="o14140" />
    </statement>
    <statement id="d0_s5">
      <text>twigs orangebrown, initially pubescent, becoming glabrous or very sparsely pubescent during first-year.</text>
      <biological_entity id="o14141" name="twig" name_original="twigs" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="orangebrown" value_original="orangebrown" />
        <character is_modifier="false" modifier="initially" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="becoming" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character constraint="during first-year" constraintid="o14142" is_modifier="false" modifier="very sparsely; very sparsely" name="pubescence" src="d0_s5" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o14142" name="first-year" name_original="first-year" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>Buds dark-brown, generally puberulent, scale margins erose.</text>
      <biological_entity id="o14143" name="bud" name_original="buds" src="d0_s6" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s6" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="generally" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity constraint="scale" id="o14144" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_relief" src="d0_s6" value="erose" value_original="erose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Leaves of short-shoots 2–5cm × 0.65–0.80mm, 0.4–0.6mm thick, keeled abaxially, with shallow convex midrib adaxially, pale green;</text>
      <biological_entity id="o14145" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="thickness" src="d0_s7" to="5" to_unit="cm" />
        <character name="thickness" src="d0_s7" unit="mm" value="×0.65-0.80" value_original="×0.65-0.80" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="thickness" src="d0_s7" to="0.6" to_unit="mm" />
        <character is_modifier="false" modifier="abaxially" name="shape" src="d0_s7" value="keeled" value_original="keeled" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s7" value="pale green" value_original="pale green" />
      </biological_entity>
      <biological_entity id="o14146" name="short-shoot" name_original="short-shoots" src="d0_s7" type="structure" />
      <biological_entity id="o14147" name="midrib" name_original="midrib" src="d0_s7" type="structure">
        <character is_modifier="true" name="depth" src="d0_s7" value="shallow" value_original="shallow" />
        <character is_modifier="true" name="shape" src="d0_s7" value="convex" value_original="convex" />
      </biological_entity>
      <relation from="o14145" id="r3097" name="part_of" negation="false" src="d0_s7" to="o14146" />
      <relation from="o14145" id="r3098" name="with" negation="false" src="d0_s7" to="o14147" />
    </statement>
    <statement id="d0_s8">
      <text>resin canals 20–50 µm from margins, each surrounded by 5–7 epithelial cells.</text>
      <biological_entity constraint="resin" id="o14148" name="canal" name_original="canals" src="d0_s8" type="structure">
        <character char_type="range_value" constraint="from margins" constraintid="o14149" from="20" from_unit="um" name="location" src="d0_s8" to="50" to_unit="um" />
      </biological_entity>
      <biological_entity id="o14149" name="margin" name_original="margins" src="d0_s8" type="structure" />
      <biological_entity id="o14150" name="epithelial" name_original="epithelial" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
      <biological_entity id="o14151" name="cell" name_original="cells" src="d0_s8" type="structure">
        <character char_type="range_value" from="5" is_modifier="true" name="quantity" src="d0_s8" to="7" />
      </biological_entity>
      <relation from="o14148" id="r3099" name="surrounded by" negation="false" src="d0_s8" to="o14150" />
      <relation from="o14148" id="r3100" name="surrounded by" negation="false" src="d0_s8" to="o14151" />
    </statement>
    <statement id="d0_s9">
      <text>Seed-cones 2–3 × 1.3–1.6cm, on curved stalks 2.5–4.5 × 3.5–5mm;</text>
      <biological_entity id="o14152" name="seed-cone" name_original="seed-cones" src="d0_s9" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s9" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.3" from_unit="cm" name="width" src="d0_s9" to="1.6" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14153" name="stalk" name_original="stalks" src="d0_s9" type="structure">
        <character is_modifier="true" name="course" src="d0_s9" value="curved" value_original="curved" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s9" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="width" src="d0_s9" to="5" to_unit="mm" />
      </biological_entity>
      <relation from="o14152" id="r3101" name="on" negation="false" src="d0_s9" to="o14153" />
    </statement>
    <statement id="d0_s10">
      <text>scales 45–55, margins entire, adaxial surface pubescent;</text>
      <biological_entity id="o14154" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="45" name="quantity" src="d0_s10" to="55" />
      </biological_entity>
      <biological_entity id="o14155" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s10" value="entire" value_original="entire" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14156" name="surface" name_original="surface" src="d0_s10" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>bracts tipped by awn to 3mm, exceeding scales by ca. 4mm.</text>
      <biological_entity id="o14157" name="bract" name_original="bracts" src="d0_s11" type="structure">
        <character constraint="by awn" constraintid="o14158" is_modifier="false" name="architecture" src="d0_s11" value="tipped" value_original="tipped" />
      </biological_entity>
      <biological_entity id="o14158" name="awn" name_original="awn" src="d0_s11" type="structure">
        <character char_type="range_value" from="0" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o14159" name="scale" name_original="scales" src="d0_s11" type="structure" />
      <relation from="o14157" id="r3102" name="exceeding" negation="false" src="d0_s11" to="o14159" />
    </statement>
    <statement id="d0_s12">
      <text>Pollen 71–84µm diam.</text>
      <biological_entity id="o14160" name="pollen" name_original="pollen" src="d0_s12" type="structure">
        <character char_type="range_value" from="71" from_unit="µm" name="diameter" src="d0_s12" to="84" to_unit="µm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Seeds reddish-brown, body 3mm, wing 6mm.</text>
      <biological_entity id="o14161" name="seed" name_original="seeds" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="reddish-brown" value_original="reddish-brown" />
      </biological_entity>
      <biological_entity id="o14162" name="body" name_original="body" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>2n =24.</text>
      <biological_entity id="o14163" name="wing" name_original="wing" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="6" value_original="6" />
      </biological_entity>
      <biological_entity constraint="2n" id="o14164" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mountain valleys and lower slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="mountain valleys" />
        <character name="habitat" value="lower slopes" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>500–1600m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="500" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Idaho, Mont., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">Western larch</other_name>
  <other_name type="common_name">mélèze occidental</other_name>
  <discussion>Western larch, when forest grown, is usually branch-free over most of its height. This is one of the most valuable timber-producing species in western North America. Its wood is made into framing, railway ties, pilings, exterior and interior finishing work, and pulp. In some localities it is the preferred firewood.</discussion>
  
</bio:treatment>