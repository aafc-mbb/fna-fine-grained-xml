<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Masahiro Kato</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Roth" date="1799" rank="genus">Athyrium</taxon_name>
    <place_of_publication>
      <publication_title>Tent. Fl. Germ.</publication_title>
      <place_in_publication>3(1,1): 31, 58. 1799</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus Athyrium</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek athyros, doorless; the sporangia only tardily push back the outer edge of the indusium</other_info_on_name>
    <other_info_on_name type="fna_id">103074</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants generally terrestrial.</text>
      <biological_entity id="o11788" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" modifier="generally" name="growth_form_or_habitat" src="d0_s0" value="terrestrial" value_original="terrestrial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems short-creeping or ascending, stolons absent.</text>
      <biological_entity id="o11789" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="short-creeping" value_original="short-creeping" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity id="o11790" name="stolon" name_original="stolons" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves monomorphic, usually dying back in winter.</text>
      <biological_entity id="o11791" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="monomorphic" value_original="monomorphic" />
        <character constraint="in winter" is_modifier="false" modifier="usually" name="condition" src="d0_s2" value="dying" value_original="dying" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Petiole ± 0.5 times length of blade or less, base swollen and dentate, persisting as trophopod over winter or not;</text>
      <biological_entity id="o11792" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character constraint="blade" constraintid="o11793" is_modifier="false" modifier="less" name="length" src="d0_s3" value="0.5 times length of blade" />
        <character constraint="blade" constraintid="o11794" is_modifier="false" name="length" src="d0_s3" value="0.5 times length of blade" />
      </biological_entity>
      <biological_entity id="o11793" name="blade" name_original="blade" src="d0_s3" type="structure" />
      <biological_entity id="o11795" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="swollen" value_original="swollen" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="dentate" value_original="dentate" />
        <character constraint="as trophopod" constraintid="o11796" is_modifier="false" name="duration" src="d0_s3" value="persisting" value_original="persisting" />
      </biological_entity>
      <biological_entity id="o11796" name="trophopod" name_original="trophopod" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>vascular-bundles 2, lateral, lunate in cross-section.</text>
      <biological_entity id="o11797" name="vascular-bundle" name_original="vascular-bundles" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s4" value="lateral" value_original="lateral" />
        <character constraint="in cross-section" constraintid="o11798" is_modifier="false" name="shape" src="d0_s4" value="lunate" value_original="lunate" />
      </biological_entity>
      <biological_entity id="o11798" name="cross-section" name_original="cross-section" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Blade lanceolate to elliptic or oblanceolate, 1–3-pinnate-pinnatifid, gradually reduced distally to confluent, pinnatifid apex, herbaceous.</text>
      <biological_entity id="o11799" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="elliptic or oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="1-3-pinnate-pinnatifid" value_original="1-3-pinnate-pinnatifid" />
        <character constraint="to apex" constraintid="o11800" is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character is_modifier="false" name="growth_form_or_texture" notes="" src="d0_s5" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o11800" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s5" value="confluent" value_original="confluent" />
        <character is_modifier="true" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Pinnae not articulate to rachis, segment margins serrulate or crenate;</text>
      <biological_entity id="o11801" name="pinna" name_original="pinnae" src="d0_s6" type="structure">
        <character constraint="to rachis" constraintid="o11802" is_modifier="false" modifier="not" name="architecture" src="d0_s6" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o11802" name="rachis" name_original="rachis" src="d0_s6" type="structure" />
      <biological_entity constraint="segment" id="o11803" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="serrulate" value_original="serrulate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="crenate" value_original="crenate" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>proximal pinnae often reduced, sessile to short-petiolulate, ± equilateral;</text>
      <biological_entity constraint="proximal" id="o11804" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="size" src="d0_s7" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s7" to="short-petiolulate" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_shape" src="d0_s7" value="equilateral" value_original="equilateral" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>costae adaxially grooved, grooves continuous from rachis to costae to costules;</text>
      <biological_entity id="o11805" name="costa" name_original="costae" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s8" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o11806" name="groove" name_original="grooves" src="d0_s8" type="structure">
        <character constraint="from rachis" constraintid="o11807" is_modifier="false" name="architecture" src="d0_s8" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity id="o11807" name="rachis" name_original="rachis" src="d0_s8" type="structure" />
      <biological_entity id="o11808" name="costa" name_original="costae" src="d0_s8" type="structure" />
      <biological_entity id="o11809" name="costule" name_original="costules" src="d0_s8" type="structure" />
      <relation from="o11807" id="r2545" name="to" negation="false" src="d0_s8" to="o11808" />
      <relation from="o11808" id="r2546" name="to" negation="false" src="d0_s8" to="o11809" />
    </statement>
    <statement id="d0_s9">
      <text>indument absent or of linear to lanceolate scales or 1-celled glands abaxially.</text>
      <biological_entity id="o11810" name="indument" name_original="indument" src="d0_s9" type="structure">
        <character constraint="or of glands" constraintid="o11812" is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o11811" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character char_type="range_value" from="linear" is_modifier="true" name="shape" src="d0_s9" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o11812" name="gland" name_original="glands" src="d0_s9" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s9" value="1-celled" value_original="1-celled" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Veins free, simple or forked.</text>
      <biological_entity id="o11813" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="free" value_original="free" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="simple" value_original="simple" />
        <character is_modifier="false" name="shape" src="d0_s10" value="forked" value_original="forked" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Sori in 1 row between midrib and margin, round to elongate, straight or hooked at distal end, or horseshoe-shaped;</text>
      <biological_entity constraint="between midrib and margin" constraintid="o11816-o11817" id="o11815" name="row" name_original="row" src="d0_s11" type="structure" constraint_original="between  midrib and  margin, ">
        <character is_modifier="true" name="quantity" src="d0_s11" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o11816" name="midrib" name_original="midrib" src="d0_s11" type="structure" />
      <biological_entity id="o11817" name="margin" name_original="margin" src="d0_s11" type="structure" />
      <biological_entity constraint="distal" id="o11818" name="end" name_original="end" src="d0_s11" type="structure" />
      <relation from="o11814" id="r2547" name="in" negation="false" src="d0_s11" to="o11815" />
    </statement>
    <statement id="d0_s12">
      <text>indusia shaped like sori, persistent, attached laterally or with narrow sinus, or indusia absent.</text>
      <biological_entity id="o11814" name="sorus" name_original="sori" src="d0_s11" type="structure">
        <character char_type="range_value" from="round" name="shape" notes="" src="d0_s11" to="elongate" />
        <character is_modifier="false" name="course" src="d0_s11" value="straight" value_original="straight" />
        <character constraint="at distal end" constraintid="o11818" is_modifier="false" name="shape" src="d0_s11" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="shape" notes="" src="d0_s11" value="horseshoe--shaped" value_original="horseshoe--shaped" />
        <character is_modifier="false" name="shape" src="d0_s12" value="indusia--shaped" value_original="indusia--shaped" />
      </biological_entity>
      <biological_entity id="o11819" name="sorus" name_original="sori" src="d0_s12" type="structure">
        <character is_modifier="false" name="duration" src="d0_s12" value="persistent" value_original="persistent" />
        <character is_modifier="false" modifier="laterally" name="fixation" src="d0_s12" value="attached" value_original="attached" />
        <character is_modifier="false" name="fixation" src="d0_s12" value="with narrow sinus" value_original="with narrow sinus" />
      </biological_entity>
      <biological_entity id="o11820" name="sinus" name_original="sinus" src="d0_s12" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s12" value="narrow" value_original="narrow" />
      </biological_entity>
      <biological_entity id="o11821" name="indusium" name_original="indusia" src="d0_s12" type="structure">
        <character is_modifier="false" name="presence" src="d0_s12" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Spores brownish, rugose.</text>
    </statement>
    <statement id="d0_s14">
      <text>x = 40.</text>
      <biological_entity id="o11822" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="relief" src="d0_s13" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="x" id="o11823" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="40" value_original="40" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Worldwide.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Worldwide" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>5</number>
  <other_name type="common_name">Lady fern</other_name>
  <discussion>In species outside the flora stems are sometimes long-creeping to erect, with leaves radially or dorsiventrally arranged.</discussion>
  <discussion>Species about 180 (2 in the flora).</discussion>
  <references>
    <reference>Johnson, D. M. 1986b. Trophopods in North American species of Athyrium  (Aspleniaceae). Syst. Bot. 11: 26–31.</reference>
    <reference>Kato, M. 1977. Classification of Athyrium and allied genera of Japan. Bot. Mag. (Tokyo) 90: 23–40.</reference>
    <reference>Liew, F. S. 1972. Numerical taxonomic studies on North American lady ferns and their allies. Taiwania 17: 190–221.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sori round, submarginal; indusia much reduced or usually absent.</description>
      <determination>1a Athyrium distentifolium var. americanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sori elongate or hooked, medial; indusia well developed.</description>
      <determination>2 Athyrium filix-femina</determination>
    </key_statement>
  </key>
</bio:treatment>