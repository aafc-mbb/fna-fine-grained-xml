<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Agardh" date="unknown" rank="family">ophioglossaceae</taxon_name>
    <taxon_name authority="Swartz" date="1801" rank="genus">botrychium</taxon_name>
    <taxon_name authority="Swartz" date="unknown" rank="subgenus">Botrychium</taxon_name>
    <taxon_name authority="W. H. Wagner" date="1986" rank="species">ascendens</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Fern J.</publication_title>
      <place_in_publication>76: 36, figs. 1, 2. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family ophioglossaceae;genus botrychium;subgenus botrychium;species ascendens;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500271</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trophophore stalk 3–10 mm, 1/6 length of trophophore rachis;</text>
      <biological_entity constraint="trophophore" id="o9243" name="stalk" name_original="stalk" src="d0_s0" type="structure">
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s0" to="10" to_unit="mm" />
        <character name="length" src="d0_s0" value="1/6 length of trophophore rachis" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>blade yellow-green, oblong to oblong-lanceolate, 1-pinnate, to 6 × 1.5 cm, thin but firm.</text>
      <biological_entity id="o9244" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="yellow-green" value_original="yellow-green" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s1" to="oblong-lanceolate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s1" value="1-pinnate" value_original="1-pinnate" />
        <character char_type="range_value" from="0" from_unit="cm" name="length" src="d0_s1" to="6" to_unit="cm" />
        <character char_type="range_value" from="0" from_unit="cm" name="width" src="d0_s1" to="1.5" to_unit="cm" />
        <character is_modifier="false" name="width" src="d0_s1" value="thin" value_original="thin" />
        <character is_modifier="false" name="texture" src="d0_s1" value="firm" value_original="firm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Pinnae to 5 pairs, strongly ascending, well separated, distance between 1st and 2d pinnae not or slightly more than between 2d and 3d pairs, basal pinna pair approximately equal in size and cutting to adjacent pair, obliquely narrowly cuneate, undivided to tip, margins sharply denticulate and often shallowly incised, apex rounded, venation like ribs of fan, midrib absent.</text>
      <biological_entity constraint="between pinnae" constraintid="o9246" id="o9245" name="pinna" name_original="pinnae" src="d0_s2" type="structure" constraint_original="between  pinnae, ">
        <character char_type="range_value" from="0" name="quantity" src="d0_s2" to="5" />
        <character is_modifier="false" modifier="strongly" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" modifier="well" name="arrangement" src="d0_s2" value="separated" value_original="separated" />
      </biological_entity>
      <biological_entity id="o9246" name="pinna" name_original="pinnae" src="d0_s2" type="structure" />
      <biological_entity constraint="basal" id="o9247" name="pinna" name_original="pinna" src="d0_s2" type="structure">
        <character constraint="to pair" constraintid="o9248" is_modifier="false" modifier="slightly; between 2d and 3d pairs; approximately" name="variability" src="d0_s2" value="equal" value_original="equal" />
        <character is_modifier="false" modifier="obliquely narrowly" name="shape" notes="" src="d0_s2" value="cuneate" value_original="cuneate" />
        <character constraint="to tip" constraintid="o9249" is_modifier="false" name="architecture_or_shape" src="d0_s2" value="undivided" value_original="undivided" />
      </biological_entity>
      <biological_entity id="o9248" name="pair" name_original="pair" src="d0_s2" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s2" value="adjacent" value_original="adjacent" />
      </biological_entity>
      <biological_entity id="o9249" name="tip" name_original="tip" src="d0_s2" type="structure" />
      <biological_entity id="o9250" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s2" value="denticulate" value_original="denticulate" />
        <character is_modifier="false" modifier="often shallowly" name="shape" src="d0_s2" value="incised" value_original="incised" />
      </biological_entity>
      <biological_entity id="o9251" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o9252" name="rib" name_original="ribs" src="d0_s2" type="structure" />
      <biological_entity id="o9253" name="midrib" name_original="midrib" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="of fan" name="presence" src="d0_s2" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o9251" id="r1962" name="like" negation="false" src="d0_s2" to="o9252" />
    </statement>
    <statement id="d0_s3">
      <text>Sporophores 2-pinnate at base of sporangial cluster, 1.3–2 times length of trophophore.</text>
      <biological_entity id="o9255" name="base" name_original="base" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s3" value="cluster" value_original="cluster" />
      </biological_entity>
      <biological_entity id="o9256" name="sporangial" name_original="sporangial" src="d0_s3" type="structure" />
      <biological_entity id="o9257" name="trophophore" name_original="trophophore" src="d0_s3" type="structure" />
      <relation from="o9255" id="r1963" name="part_of" negation="false" src="d0_s3" to="o9256" />
    </statement>
    <statement id="d0_s4">
      <text>2n =180.</text>
      <biological_entity id="o9254" name="sporophore" name_original="sporophores" src="d0_s3" type="structure">
        <character constraint="at base" constraintid="o9255" is_modifier="false" name="architecture_or_shape" src="d0_s3" value="2-pinnate" value_original="2-pinnate" />
        <character constraint="trophophore" constraintid="o9257" is_modifier="false" name="length" notes="" src="d0_s3" value="1.3-2 times length of trophophore" />
      </biological_entity>
      <biological_entity constraint="2n" id="o9258" name="chromosome" name_original="" src="d0_s4" type="structure">
        <character name="quantity" src="d0_s4" value="180" value_original="180" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Leaves appearing in late spring to midsummer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="leaves appearing time" char_type="range_value" to="midsummer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>In grassy fields, widely scattered</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="grassy fields" modifier="in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0–2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C., Ont., Yukon; Alaska, Calif., Mont., Nev., Oreg., Wyo.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>10</number>
  <other_name type="common_name">Upswept moonwort</other_name>
  <discussion>Botrychium ascendens is a distinctive little moonwort that grows with B. crenulatum, B. lunaria, and B. minganense. This species and B. pedunculosum are the only grapeferns that often have extra sporangia on the proximal pinnae.</discussion>
  
</bio:treatment>