<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Lindley" date="unknown" rank="family">pinaceae</taxon_name>
    <taxon_name authority="(Endlicher) Carrière" date="unknown" rank="genus">tsuga</taxon_name>
    <taxon_name authority="(Linnaeus) Carrière" date="1855" rank="species">canadensis</taxon_name>
    <place_of_publication>
      <place_in_publication>189. 1855</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pinaceae;genus tsuga;species canadensis</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233501320</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Pinus</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">canadensis</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl. ed.</publication_title>
      <place_in_publication>2, 2: 1471. 1763</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Pinus;species canadensis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees to 30m;</text>
      <biological_entity id="o16316" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="some_measurement" src="d0_s0" to="30" to_unit="m" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>trunk to 1.5m diam.;</text>
      <biological_entity id="o16317" name="trunk" name_original="trunk" src="d0_s1" type="structure">
        <character char_type="range_value" from="0" from_unit="m" name="diameter" src="d0_s1" to="1.5" to_unit="m" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>crown broadly conic.</text>
      <biological_entity id="o16318" name="crown" name_original="crown" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s2" value="conic" value_original="conic" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Bark brownish, scaly and fissured.</text>
      <biological_entity id="o16319" name="bark" name_original="bark" src="d0_s3" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s3" value="brownish" value_original="brownish" />
        <character is_modifier="false" name="architecture_or_pubescence" src="d0_s3" value="scaly" value_original="scaly" />
        <character is_modifier="false" name="relief" src="d0_s3" value="fissured" value_original="fissured" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Twigs yellowbrown, densely pubescent.</text>
      <biological_entity id="o16320" name="twig" name_original="twigs" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="yellowbrown" value_original="yellowbrown" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Buds ovoid, 1.5–2.5mm.</text>
      <biological_entity id="o16321" name="bud" name_original="buds" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s5" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Leaves (5–) 15–20 (–25) mm, mostly appearing 2-ranked, flattened;</text>
      <biological_entity id="o16322" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="15" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="20" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="25" to_unit="mm" />
        <character char_type="range_value" from="15" from_unit="mm" name="some_measurement" src="d0_s6" to="20" to_unit="mm" />
        <character is_modifier="false" modifier="mostly" name="arrangement" src="d0_s6" value="2-ranked" value_original="2-ranked" />
        <character is_modifier="false" name="shape" src="d0_s6" value="flattened" value_original="flattened" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>abaxial surface glaucous, with 2 broad, conspicuous stomatal bands, adaxial surface shiny green (yellow-green);</text>
      <biological_entity constraint="abaxial" id="o16323" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
      </biological_entity>
      <biological_entity id="o16324" name="stomatal" name_original="stomatal" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="width" src="d0_s7" value="broad" value_original="broad" />
        <character is_modifier="true" name="prominence" src="d0_s7" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o16325" name="band" name_original="bands" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="2" value_original="2" />
        <character is_modifier="true" name="width" src="d0_s7" value="broad" value_original="broad" />
        <character is_modifier="true" name="prominence" src="d0_s7" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o16326" name="surface" name_original="surface" src="d0_s7" type="structure">
        <character is_modifier="false" name="reflectance" src="d0_s7" value="shiny" value_original="shiny" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="green" value_original="green" />
      </biological_entity>
      <relation from="o16323" id="r3574" name="with" negation="false" src="d0_s7" to="o16324" />
      <relation from="o16323" id="r3575" name="with" negation="false" src="d0_s7" to="o16325" />
    </statement>
    <statement id="d0_s8">
      <text>margins minutely dentate, especially toward apex.</text>
      <biological_entity id="o16327" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="minutely" name="architecture_or_shape" src="d0_s8" value="dentate" value_original="dentate" />
      </biological_entity>
      <biological_entity id="o16328" name="apex" name_original="apex" src="d0_s8" type="structure" />
      <relation from="o16327" id="r3576" modifier="especially" name="toward" negation="false" src="d0_s8" to="o16328" />
    </statement>
    <statement id="d0_s9">
      <text>Seed-cones ovoid, 1.5–2.5 × 1–1.5cm;</text>
      <biological_entity id="o16329" name="seed-cone" name_original="seed-cones" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="length" src="d0_s9" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s9" to="1.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>scales ovate to cuneate, 8–12 × 7–10mm, apex ± round, often projected outward.</text>
      <biological_entity id="o16330" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s10" to="cuneate" />
        <character char_type="range_value" from="8" from_unit="mm" name="length" src="d0_s10" to="12" to_unit="mm" />
        <character char_type="range_value" from="7" from_unit="mm" name="width" src="d0_s10" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>2n =24.</text>
      <biological_entity id="o16331" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="round" value_original="round" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16332" name="chromosome" name_original="" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Moist rocky ridges, ravines, and hillsides</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="moist rocky ridges" />
        <character name="habitat" value="ravines" />
        <character name="habitat" value="hillsides" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600–1800m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.B., N.S., Ont., P.E.I., Que.; Ala., Conn., Del., Ga., Ind., Ky., Maine, Md., Mass., Mich., Minn., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va., Wis.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>3</number>
  <other_name type="common_name">Eastern hemlock</other_name>
  <other_name type="common_name">pruche du Canada</other_name>
  <discussion>Numerous cultivars of Tsuga canadensis have been developed, including compact shrubs, dwarfs, and graceful trees. Wood of the species tends to be brittle and inferior to that of the other North American hemlocks.</discussion>
  <discussion>Eastern hemlock (Tsuga canadensis) is the state tree of Pennsylvania.</discussion>
  
</bio:treatment>