<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="Swartz" date="1806" rank="genus">cheilanthes</taxon_name>
    <taxon_name authority="Link" date="1841" rank="species">leucopoda</taxon_name>
    <place_of_publication>
      <publication_title>Fil. Spec.</publication_title>
      <place_in_publication>66. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus cheilanthes;species leucopoda</taxon_hierarchy>
    <other_info_on_name type="fna_id">233500359</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems compact, usually 4–10 mm diam.;</text>
      <biological_entity id="o14890" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character char_type="range_value" from="4" from_unit="mm" modifier="usually" name="diameter" src="d0_s0" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales uniformly brown, linear-subulate, straight to slightly contorted, loosely appressed, persistent.</text>
      <biological_entity id="o14891" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="uniformly" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s1" value="linear-subulate" value_original="linear-subulate" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="arrangement_or_shape" src="d0_s1" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves clustered, 7–30 cm;</text>
    </statement>
    <statement id="d0_s3">
      <text>vernation circinate.</text>
      <biological_entity id="o14892" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s2" to="30" to_unit="cm" />
        <character is_modifier="false" name="vernation" src="d0_s3" value="circinate" value_original="circinate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole straw-colored, shallowly grooved distally on adaxial surface.</text>
      <biological_entity id="o14893" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="straw-colored" value_original="straw-colored" />
        <character constraint="on adaxial surface" constraintid="o14894" is_modifier="false" modifier="shallowly" name="architecture" src="d0_s4" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity constraint="adaxial" id="o14894" name="surface" name_original="surface" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>Blade elongate-pentagonal, 4-pinnate at base, 3–10 cm wide;</text>
      <biological_entity id="o14895" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="elongate-pentagonal" value_original="elongate-pentagonal" />
        <character constraint="at base" constraintid="o14896" is_modifier="false" name="architecture_or_shape" src="d0_s5" value="4-pinnate" value_original="4-pinnate" />
        <character char_type="range_value" from="3" from_unit="cm" name="width" notes="" src="d0_s5" to="10" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o14896" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>rachis grooved adaxially, lacking scales, with monomorphic pubescence.</text>
      <biological_entity id="o14897" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="adaxially" name="architecture" src="d0_s6" value="grooved" value_original="grooved" />
      </biological_entity>
      <biological_entity id="o14898" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s6" value="lacking" value_original="lacking" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pinnae not articulate, color of stalk continuing into pinna base, basal pair larger than adjacent pair, strongly inequilateral, proximal basiscopic pinnules greatly enlarged, appearing hirsute adaxially.</text>
      <biological_entity id="o14899" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="articulate" value_original="articulate" />
      </biological_entity>
      <biological_entity id="o14900" name="stalk" name_original="stalk" src="d0_s7" type="structure" />
      <biological_entity constraint="pinna" id="o14901" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o14902" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character constraint="than adjacent pair , strongly inequilateral , proximal pinnae" constraintid="o14903" is_modifier="false" name="size" src="d0_s7" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o14903" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s7" value="adjacent" value_original="adjacent" />
        <character is_modifier="true" modifier="strongly" name="shape" src="d0_s7" value="inequilateral" value_original="inequilateral" />
      </biological_entity>
      <biological_entity id="o14904" name="pinnule" name_original="pinnules" src="d0_s7" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s7" value="basiscopic" value_original="basiscopic" />
        <character is_modifier="false" modifier="greatly" name="size" src="d0_s7" value="enlarged" value_original="enlarged" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s7" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <relation from="o14900" id="r3262" name="continuing into" negation="false" src="d0_s7" to="o14901" />
    </statement>
    <statement id="d0_s8">
      <text>Costae green or straw-colored adaxially for entire length;</text>
      <biological_entity id="o14905" name="costa" name_original="costae" src="d0_s8" type="structure">
        <character is_modifier="false" name="length" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="length" src="d0_s8" value="straw-colored" value_original="straw-colored" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>abaxial scales absent.</text>
      <biological_entity constraint="abaxial" id="o14906" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Ultimate segments oblong to lanceolate, not especially beadlike, the largest 3–5 mm, abaxially and adaxially hirsute with long, noncapitate hairs.</text>
      <biological_entity constraint="ultimate" id="o14907" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s10" to="lanceolate" />
        <character is_modifier="false" modifier="not especially" name="shape" src="d0_s10" value="beadlike" value_original="beadlike" />
        <character is_modifier="false" name="size" src="d0_s10" value="largest" value_original="largest" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character constraint="with hairs" constraintid="o14908" is_modifier="false" modifier="abaxially; adaxially" name="pubescence" src="d0_s10" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o14908" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="length_or_size" src="d0_s10" value="long" value_original="long" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="noncapitate" value_original="noncapitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>False indusia marginal, weakly differentiated, 0.05–0.25 mm wide.</text>
      <biological_entity constraint="false" id="o14909" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="marginal" value_original="marginal" />
        <character is_modifier="false" modifier="weakly" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s11" to="0.25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sori usually discontinuous, concentrated on apical and lateral lobes.</text>
      <biological_entity id="o14910" name="sorus" name_original="sori" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement" src="d0_s12" value="discontinuous" value_original="discontinuous" />
        <character constraint="on apical lateral lobes" constraintid="o14911" is_modifier="false" name="arrangement_or_density" src="d0_s12" value="concentrated" value_original="concentrated" />
      </biological_entity>
      <biological_entity constraint="apical and lateral" id="o14911" name="lobe" name_original="lobes" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Sporangia containing 32 spores.</text>
      <biological_entity id="o14913" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="32" value_original="32" />
      </biological_entity>
      <relation from="o14912" id="r3263" name="containing" negation="false" src="d0_s13" to="o14913" />
    </statement>
    <statement id="d0_s14">
      <text>2n = 60.</text>
      <biological_entity id="o14912" name="sporangium" name_original="sporangia" src="d0_s13" type="structure" />
      <biological_entity constraint="2n" id="o14914" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="60" value_original="60" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Rocky slopes and ledges, apparently confined to limestone</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="ledges" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300–500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="500" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; n Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="n Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>22</number>
  <other_name type="common_name">White-footed lip fern</other_name>
  <discussion>In North America, Cheilanthes leucopoda is known only from the Edwards Plateau in west central Texas. It is unique among local Cheilanthes species in being a sexual diploid that consistently produces 32 spores per sporangium.</discussion>
  
</bio:treatment>