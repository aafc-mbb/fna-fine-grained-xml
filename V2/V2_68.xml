<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Gray" date="unknown" rank="family">taxaceae</taxon_name>
    <taxon_name authority="Arnott" date="1838" rank="genus">Torreya</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Nat. Hist.</publication_title>
      <place_in_publication>1: 130. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family taxaceae;genus Torreya</taxon_hierarchy>
    <other_info_on_name type="etymology">After John Torrey (1796–1873), distinguished U.S. botanist</other_info_on_name>
    <other_info_on_name type="fna_id">133154</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="Rafinesque" date="unknown" rank="genus">Tumion</taxon_name>
    <taxon_hierarchy>genus Tumion;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Trees dioecious.</text>
      <biological_entity id="o3496" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s0" value="dioecious" value_original="dioecious" />
        <character name="growth_form" value="tree" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Bark brown to grayish brown, tinged with orange, fissured.</text>
      <biological_entity id="o3497" name="bark" name_original="bark" src="d0_s1" type="structure">
        <character char_type="range_value" from="brown" name="coloration" src="d0_s1" to="grayish brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="tinged with orange" value_original="tinged with orange" />
        <character is_modifier="false" name="relief" src="d0_s1" value="fissured" value_original="fissured" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Branches spreading to drooping;</text>
      <biological_entity id="o3498" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s2" to="drooping" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>twigs nearly opposite.</text>
      <biological_entity id="o3499" name="twig" name_original="twigs" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="nearly" name="arrangement" src="d0_s3" value="opposite" value_original="opposite" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly appearing 2-ranked, rigid;</text>
      <biological_entity id="o3500" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s4" value="2-ranked" value_original="2-ranked" />
        <character is_modifier="false" name="texture" src="d0_s4" value="rigid" value_original="rigid" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>stomates abaxial, in 2 narrow, glaucous, whitish or brownish bands;</text>
      <biological_entity id="o3501" name="stomate" name_original="stomates" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="abaxial" value_original="abaxial" />
      </biological_entity>
      <biological_entity id="o3502" name="band" name_original="bands" src="d0_s5" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="true" name="size_or_width" src="d0_s5" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="pubescence" src="d0_s5" value="glaucous" value_original="glaucous" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="whitish" value_original="whitish" />
        <character is_modifier="true" name="coloration" src="d0_s5" value="brownish" value_original="brownish" />
      </biological_entity>
      <relation from="o3501" id="r751" name="in" negation="false" src="d0_s5" to="o3502" />
    </statement>
    <statement id="d0_s6">
      <text>apex sharp-pointed, spine-tipped, sharp to touch;</text>
      <biological_entity id="o3503" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="sharp-pointed" value_original="sharp-pointed" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="spine-tipped" value_original="spine-tipped" />
        <character constraint="to touch" constraintid="o3504" is_modifier="false" name="shape" src="d0_s6" value="sharp" value_original="sharp" />
      </biological_entity>
      <biological_entity id="o3504" name="touch" name_original="touch" src="d0_s6" type="structure" />
    </statement>
    <statement id="d0_s7">
      <text>resin canal central.</text>
      <biological_entity constraint="resin" id="o3505" name="canal" name_original="canal" src="d0_s7" type="structure">
        <character is_modifier="false" name="position" src="d0_s7" value="central" value_original="central" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pollen cones ovoid or oblong, with 6–8 whorls of 4 sporophylls, each bearing 4 sporangia.</text>
      <biological_entity constraint="pollen" id="o3506" name="cone" name_original="cones" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="ovoid" value_original="ovoid" />
        <character is_modifier="false" name="shape" src="d0_s8" value="oblong" value_original="oblong" />
      </biological_entity>
      <biological_entity id="o3507" name="whorl" name_original="whorls" src="d0_s8" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s8" to="8" />
      </biological_entity>
      <biological_entity id="o3508" name="sporophyll" name_original="sporophylls" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="4" value_original="4" />
      </biological_entity>
      <biological_entity id="o3509" name="sporangium" name_original="sporangia" src="d0_s8" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s8" value="4" value_original="4" />
      </biological_entity>
      <relation from="o3506" id="r752" name="with" negation="false" src="d0_s8" to="o3507" />
      <relation from="o3507" id="r753" name="part_of" negation="false" src="d0_s8" to="o3508" />
      <relation from="o3506" id="r754" name="bearing" negation="false" src="d0_s8" to="o3509" />
    </statement>
    <statement id="d0_s9">
      <text>Ovules 2, only 1 of each pair maturing.</text>
      <biological_entity id="o3510" name="ovule" name_original="ovules" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="2" value_original="2" />
        <character modifier="only" name="quantity" src="d0_s9" value="1" value_original="1" />
        <character constraint="of each pair" is_modifier="false" name="life_cycle" src="d0_s9" value="maturing" value_original="maturing" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Seed maturing in 2 years;</text>
      <biological_entity id="o3511" name="seed" name_original="seed" src="d0_s10" type="structure">
        <character constraint="in years" constraintid="o3512" is_modifier="false" name="life_cycle" src="d0_s10" value="maturing" value_original="maturing" />
      </biological_entity>
      <biological_entity id="o3512" name="year" name_original="years" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>aril green or green with purple streaks, resinous, leathery, thin, completely enclosing woody seed-coat, splitting into 2 parts at maturity;</text>
      <biological_entity id="o3513" name="aril" name_original="aril" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character constraint="with streaks" constraintid="o3514" is_modifier="false" name="coloration" src="d0_s11" value="green" value_original="green" />
        <character is_modifier="false" name="coating" notes="" src="d0_s11" value="resinous" value_original="resinous" />
        <character is_modifier="false" name="texture" src="d0_s11" value="leathery" value_original="leathery" />
        <character is_modifier="false" name="width" src="d0_s11" value="thin" value_original="thin" />
        <character constraint="into parts" constraintid="o3516" is_modifier="false" name="architecture_or_dehiscence" src="d0_s11" value="splitting" value_original="splitting" />
      </biological_entity>
      <biological_entity id="o3514" name="streak" name_original="streaks" src="d0_s11" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s11" value="purple" value_original="purple" />
      </biological_entity>
      <biological_entity id="o3515" name="seed-coat" name_original="seed-coat" src="d0_s11" type="structure">
        <character is_modifier="true" name="texture" src="d0_s11" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o3516" name="part" name_original="parts" src="d0_s11" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s11" value="2" value_original="2" />
      </biological_entity>
      <relation from="o3513" id="r755" modifier="completely" name="enclosing" negation="false" src="d0_s11" to="o3515" />
    </statement>
    <statement id="d0_s12">
      <text>albumen ruminate.</text>
    </statement>
    <statement id="d0_s13">
      <text>x = 11.</text>
      <biological_entity id="o3517" name="albumen" name_original="albumen" src="d0_s12" type="structure">
        <character is_modifier="false" name="relief" src="d0_s12" value="ruminate" value_original="ruminate" />
      </biological_entity>
      <biological_entity constraint="x" id="o3518" name="chromosome" name_original="" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="11" value_original="11" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>North America, Asia in China and Japan.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="North America" establishment_means="native" />
        <character name="distribution" value="Asia in China and Japan" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">Torreya</other_name>
  <other_name type="common_name">stinking-cedar</other_name>
  <discussion>Two Asian species are planted as ornamentals in North America: Torreya nucifera Siebold &amp; Zuccarini (kaya-nut, Japanese torreya), which yields edible seeds and cooking oil, and T. grandis Fortune (Chinese torreya).</discussion>
  <discussion>Species 4(–6) (2 in the flora).</discussion>
  <references>
    <reference>Buchholz, J. T. 1940. The embryogeny of Torreya, with a note on Austrotaxus. Bull. Torrey Bot. Club 67: 731–754.</reference>
    <reference>Burke, J. G. 1975. Human use of the California nutmeg tree,  Torreya californica, and of other members of the genus. Econ. Bot. 29: 127–139.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Two-year-old branches reddish brown; leaves 3-8 cm, flattened on adaxial side, with 2 deeply impressed, glaucous bands of stomates abaxially, emitting pungent odor when crushed; aril light green streaked with purple; California.</description>
      <determination>1 Torreya californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Two-year-old branches yellowish green, yellowish brown, or gray; leaves 1.5-3.8 cm, rounded on adaxial side, with 2 scarcely impressed, grayish bands of stomates abaxially, emitting fetid odor when crushed; aril dark green streaked with purple; Florida, Georgia.</description>
      <determination>2 Torreya taxifolia</determination>
    </key_statement>
  </key>
</bio:treatment>