<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Herter" date="unknown" rank="family">dryopteridaceae</taxon_name>
    <taxon_name authority="Roth" date="1799" rank="genus">athyrium</taxon_name>
    <taxon_name authority="(Linnaeus) Roth ex Mertens" date="1799" rank="species">filix-femina</taxon_name>
    <place_of_publication>
      <publication_title>Arch. Bot. (Leipzig)</publication_title>
      <place_in_publication>2(1): 106. 1799</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family dryopteridaceae;genus athyrium;species filix-femina</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">200003781</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polypodium</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="species">filix-femina</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 1090. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polypodium;species filix-femina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems short-creeping or ascending.</text>
      <biological_entity id="o3752" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="short-creeping" value_original="short-creeping" />
        <character is_modifier="false" name="orientation" src="d0_s0" value="ascending" value_original="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Petiole straw-colored distally, 7–60 cm, base dark redbrown or black, swollen, with 2 rows of teeth;</text>
      <biological_entity id="o3753" name="petiole" name_original="petiole" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="distally" name="coloration" src="d0_s1" value="straw-colored" value_original="straw-colored" />
        <character char_type="range_value" from="7" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o3754" name="base" name_original="base" src="d0_s1" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s1" value="dark redbrown" value_original="dark redbrown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="black" value_original="black" />
        <character is_modifier="false" name="shape" src="d0_s1" value="swollen" value_original="swollen" />
      </biological_entity>
      <biological_entity id="o3755" name="row" name_original="rows" src="d0_s1" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s1" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o3756" name="tooth" name_original="teeth" src="d0_s1" type="structure" />
      <relation from="o3754" id="r807" name="with" negation="false" src="d0_s1" to="o3755" />
      <relation from="o3755" id="r808" name="part_of" negation="false" src="d0_s1" to="o3756" />
    </statement>
    <statement id="d0_s2">
      <text>scales light to dark-brown, linear to ovatelanceolate, 7–20 × 1–5 mm.</text>
      <biological_entity id="o3757" name="scale" name_original="scales" src="d0_s2" type="structure">
        <character char_type="range_value" from="light" name="coloration" src="d0_s2" to="dark-brown" />
        <character char_type="range_value" from="linear" name="shape" src="d0_s2" to="ovatelanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="length" src="d0_s2" to="20" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s2" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Blade elliptic, lanceolate to oblanceolate, 2-pinnate to 2-pinnate-pinnatifid, 18–30 × 5–50 cm, herbaceous but with cartilaginous margin, narrowed to base, apex acuminate.</text>
      <biological_entity id="o3758" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="elliptic" value_original="elliptic" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="oblanceolate 2-pinnate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s3" to="oblanceolate 2-pinnate" />
        <character char_type="range_value" from="18" from_unit="cm" name="length" src="d0_s3" to="30" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s3" to="50" to_unit="cm" />
        <character constraint="with margin" constraintid="o3759" is_modifier="false" name="growth_form_or_texture" src="d0_s3" value="herbaceous" value_original="herbaceous" />
        <character constraint="to base" constraintid="o3760" is_modifier="false" name="shape" notes="" src="d0_s3" value="narrowed" value_original="narrowed" />
      </biological_entity>
      <biological_entity id="o3759" name="margin" name_original="margin" src="d0_s3" type="structure">
        <character is_modifier="true" name="pubescence_or_texture" src="d0_s3" value="cartilaginous" value_original="cartilaginous" />
      </biological_entity>
      <biological_entity id="o3760" name="base" name_original="base" src="d0_s3" type="structure" />
      <biological_entity id="o3761" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Pinnae sessile to short-stalked, linear-oblong to lanceolate, apex acuminate.</text>
      <biological_entity id="o3762" name="pinna" name_original="pinnae" src="d0_s4" type="structure">
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s4" to="short-stalked" />
        <character char_type="range_value" from="linear-oblong" name="shape" src="d0_s4" to="lanceolate" />
      </biological_entity>
      <biological_entity id="o3763" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acuminate" value_original="acuminate" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Pinnules pinnatifid, segments oblong-linear to narrowly deltate, margins serrate.</text>
      <biological_entity id="o3764" name="pinnule" name_original="pinnules" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="pinnatifid" value_original="pinnatifid" />
      </biological_entity>
      <biological_entity id="o3765" name="segment" name_original="segments" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong-linear" name="shape" src="d0_s5" to="narrowly deltate" />
      </biological_entity>
      <biological_entity id="o3766" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="serrate" value_original="serrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Rachis, costae, and costules glabrous or with glands or hairs.</text>
      <biological_entity id="o3767" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="with glands or hairs" value_original="with glands or hairs" />
      </biological_entity>
      <biological_entity id="o3768" name="costa" name_original="costae" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3769" name="costule" name_original="costules" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o3770" name="gland" name_original="glands" src="d0_s6" type="structure" />
      <biological_entity id="o3771" name="hair" name_original="hairs" src="d0_s6" type="structure" />
      <relation from="o3767" id="r809" name="with" negation="false" src="d0_s6" to="o3770" />
      <relation from="o3768" id="r810" name="with" negation="false" src="d0_s6" to="o3770" />
      <relation from="o3769" id="r811" name="with" negation="false" src="d0_s6" to="o3770" />
      <relation from="o3767" id="r812" name="with" negation="false" src="d0_s6" to="o3771" />
      <relation from="o3768" id="r813" name="with" negation="false" src="d0_s6" to="o3771" />
      <relation from="o3769" id="r814" name="with" negation="false" src="d0_s6" to="o3771" />
    </statement>
    <statement id="d0_s7">
      <text>Veins pinnate.</text>
      <biological_entity id="o3772" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="pinnate" value_original="pinnate" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Sori straight, hooked at distal end, or horseshoe-shaped;</text>
      <biological_entity id="o3773" name="sorus" name_original="sori" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character constraint="at distal end" constraintid="o3774" is_modifier="false" name="shape" src="d0_s8" value="hooked" value_original="hooked" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="horseshoe--shaped" value_original="horseshoe--shaped" />
      </biological_entity>
      <biological_entity constraint="distal" id="o3774" name="end" name_original="end" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>indusia dentate or ciliate.</text>
      <biological_entity id="o3775" name="indusium" name_original="indusia" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s9" value="dentate" value_original="dentate" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="ciliate" value_original="ciliate" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.S., N.W.T., Nfld. and Labr. (Nfld.), Ont., P.E.I., Que., Sask., Yukon; Ala., Alaska, Ariz., Ark., Calif., Colo., Conn., Del., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Mass., Md., Mich., Minn., Miss., Mo., Mont., N.C., N.Dak., N.H., N.J., N.Mex., N.Y., Nebr., Nev., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Utah, Va., Vt., W.Va., Wash., Wis., Wyo.; Mexico, Central America, South America, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
        <character name="distribution" value="Central America" establishment_means="native" />
        <character name="distribution" value="South America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>2</number>
  <other_name type="common_name">Lady fern</other_name>
  <other_name type="common_name">athyrie fougère-femelle</other_name>
  <discussion>Athyrium filix-femina is circumboreal, and this or closely related species extend into Mexico, Central America, and South America. The delimitation and infraspecific classification of A. filix-femina need detailed study.</discussion>
  <discussion>Varieties ca. 5 (4 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petiole scales more than 1 cm; blades ca. 2 times length of petioles, elliptic to oblanceolate.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petiole scales 1 cm or less; blades 1-1.5 times length of petioles, elliptic to lanceolate.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pinnules narrowly deltate or oblong-lanceolate, nearly equilateral at base; indusia long-ciliate; spores yellow.</description>
      <determination>2a Athyrium filix-femina var. cyclosorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Pinnules linear-oblong or linear-lanceolate, inequilateral at base; indusia dentate or long-ciliate; spores brown.</description>
      <determination>2b Athyrium filix-femina var. californicum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petiole scales brown to dark brown; blades elliptic, narrowed to base, broadest near or just below middle; pinnae sessile or short-stalked; pinnules linear to oblong; indusia not glandular; spores yellow.</description>
      <determination>2c Athyrium filix-femina var. angustum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petiole scales light brown to brown; blades ovate-lanceolate to lanceolate, slightly narrowed to base, broadest just above base; pinnae usually stalked; pinnules oblong-lanceolate to narrowly deltate; indusia glandular or not; spores dark brown.</description>
      <determination>2d Athyrium filix-femina var. asplenioides</determination>
    </key_statement>
  </key>
</bio:treatment>