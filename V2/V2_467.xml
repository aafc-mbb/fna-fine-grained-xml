<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/01 21:10:54</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="volume">2</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="E. D. M. Kirchner" date="unknown" rank="family">pteridaceae</taxon_name>
    <taxon_name authority="Swartz" date="1806" rank="genus">cheilanthes</taxon_name>
    <taxon_name authority="D. C. Eaton in Emory" date="1859" rank="species">gracillima</taxon_name>
    <place_of_publication>
      <publication_title>in Emory,Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2: 234. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family pteridaceae;genus cheilanthes;species gracillima</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">233500353</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Myriopteris</taxon_name>
    <taxon_name authority="(D. C. Eaton) J. Smith" date="unknown" rank="species">gracillima</taxon_name>
    <taxon_hierarchy>genus Myriopteris;species gracillima;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Stems short-creeping, 4–8 mm diam.;</text>
      <biological_entity id="o12944" name="stem" name_original="stems" src="d0_s0" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="short-creeping" value_original="short-creeping" />
        <character char_type="range_value" from="4" from_unit="mm" name="diameter" src="d0_s0" to="8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>scales uniformly brown or with poorly defined, dark, central stripe, linear-lanceolate, straight to slightly contorted, loosely appressed, persistent.</text>
      <biological_entity id="o12945" name="scale" name_original="scales" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="uniformly" name="coloration" src="d0_s1" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="with poorly defined , dark , central stripe" />
        <character is_modifier="false" name="shape" notes="" src="d0_s1" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="course" src="d0_s1" value="straight" value_original="straight" />
        <character is_modifier="false" modifier="slightly" name="arrangement_or_shape" src="d0_s1" value="contorted" value_original="contorted" />
        <character is_modifier="false" modifier="loosely" name="fixation_or_orientation" src="d0_s1" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="duration" src="d0_s1" value="persistent" value_original="persistent" />
      </biological_entity>
      <biological_entity constraint="central" id="o12946" name="stripe" name_original="stripe" src="d0_s1" type="structure">
        <character is_modifier="true" modifier="poorly" name="prominence" src="d0_s1" value="defined" value_original="defined" />
        <character is_modifier="true" name="coloration" src="d0_s1" value="dark" value_original="dark" />
      </biological_entity>
      <relation from="o12945" id="r2829" name="with" negation="false" src="d0_s1" to="o12946" />
    </statement>
    <statement id="d0_s2">
      <text>Leaves clustered, 5–25 cm;</text>
    </statement>
    <statement id="d0_s3">
      <text>vernation noncircinate.</text>
      <biological_entity id="o12947" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="arrangement_or_growth_form" src="d0_s2" value="clustered" value_original="clustered" />
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s2" to="25" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Petiole dark-brown, rounded adaxially.</text>
      <biological_entity id="o12948" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s4" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s4" value="rounded" value_original="rounded" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Blade linear-oblong, 2–3-pinnate at base, 1–2.5 cm wide;</text>
      <biological_entity id="o12949" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="linear-oblong" value_original="linear-oblong" />
        <character constraint="at base" constraintid="o12950" is_modifier="false" name="architecture_or_shape" src="d0_s5" value="2-3-pinnate" value_original="2-3-pinnate" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" notes="" src="d0_s5" to="2.5" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o12950" name="base" name_original="base" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>rachis rounded adaxially, with scattered linear scales, not pubescent.</text>
      <biological_entity id="o12951" name="rachis" name_original="rachis" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="adaxially" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="not" name="pubescence" notes="" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o12952" name="scale" name_original="scales" src="d0_s6" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s6" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="arrangement_or_course_or_shape" src="d0_s6" value="linear" value_original="linear" />
      </biological_entity>
      <relation from="o12951" id="r2830" name="with" negation="false" src="d0_s6" to="o12952" />
    </statement>
    <statement id="d0_s7">
      <text>Pinnae not articulate, dark color of stalk continuing into pinna base, basal pair not conspicuously larger than adjacent pair, usually equilateral, appearing sparsely pubescent or glabrous adaxially.</text>
      <biological_entity id="o12953" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s7" value="articulate" value_original="articulate" />
        <character constraint="of stalk" constraintid="o12954" is_modifier="false" name="coloration" src="d0_s7" value="dark color" value_original="dark color" />
      </biological_entity>
      <biological_entity id="o12954" name="stalk" name_original="stalk" src="d0_s7" type="structure" />
      <biological_entity constraint="pinna" id="o12955" name="base" name_original="base" src="d0_s7" type="structure" />
      <biological_entity constraint="basal" id="o12956" name="pinna" name_original="pinnae" src="d0_s7" type="structure">
        <character is_modifier="false" name="size" src="d0_s7" value="not conspicuously larger than adjacent pair" />
        <character is_modifier="false" modifier="usually" name="architecture_or_shape" src="d0_s7" value="equilateral" value_original="equilateral" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s7" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="adaxially" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o12954" id="r2831" name="continuing into" negation="false" src="d0_s7" to="o12955" />
    </statement>
    <statement id="d0_s8">
      <text>Costae green adaxially for most of length;</text>
      <biological_entity id="o12957" name="costa" name_original="costae" src="d0_s8" type="structure">
        <character constraint="for" is_modifier="false" name="length" src="d0_s8" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>abaxial scales multiseriate, linear, truncate at base, inconspicuous, the largest 0.1–0.4 mm wide, loosely imbricate, not concealing ultimate segments, long-ciliate, cilia usually confined to base.</text>
      <biological_entity constraint="abaxial" id="o12958" name="scale" name_original="scales" src="d0_s9" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s9" value="multiseriate" value_original="multiseriate" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s9" value="linear" value_original="linear" />
        <character constraint="at base" constraintid="o12959" is_modifier="false" name="architecture_or_shape" src="d0_s9" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="prominence" notes="" src="d0_s9" value="inconspicuous" value_original="inconspicuous" />
        <character is_modifier="false" name="size" src="d0_s9" value="largest" value_original="largest" />
        <character char_type="range_value" from="0.1" from_unit="mm" name="width" src="d0_s9" to="0.4" to_unit="mm" />
        <character is_modifier="false" modifier="loosely" name="arrangement" src="d0_s9" value="imbricate" value_original="imbricate" />
      </biological_entity>
      <biological_entity id="o12959" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity constraint="ultimate" id="o12960" name="segment" name_original="segments" src="d0_s9" type="structure">
        <character is_modifier="true" modifier="not" name="position" src="d0_s9" value="concealing" value_original="concealing" />
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s9" value="long-ciliate" value_original="long-ciliate" />
      </biological_entity>
      <biological_entity id="o12961" name="cilium" name_original="cilia" src="d0_s9" type="structure" />
      <biological_entity id="o12962" name="base" name_original="base" src="d0_s9" type="structure" />
      <relation from="o12961" id="r2832" name="confined to" negation="false" src="d0_s9" to="o12962" />
    </statement>
    <statement id="d0_s10">
      <text>Ultimate segments oblong or rarely oval, beadlike, the largest 1.5–3 mm, abaxially densely covered with branched hairs and small, ciliate scales, adaxially with scattered, branched hairs or glabrescent.</text>
      <biological_entity constraint="ultimate" id="o12963" name="segment" name_original="segments" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="oblong" value_original="oblong" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s10" value="oval" value_original="oval" />
        <character is_modifier="false" name="shape" src="d0_s10" value="beadlike" value_original="beadlike" />
        <character is_modifier="false" name="size" src="d0_s10" value="largest" value_original="largest" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="3" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrescent" value_original="glabrescent" />
      </biological_entity>
      <biological_entity id="o12964" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o12965" name="scale" name_original="scales" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="branched" value_original="branched" />
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
        <character is_modifier="true" name="architecture_or_pubescence_or_shape" src="d0_s10" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o12966" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="arrangement" src="d0_s10" value="scattered" value_original="scattered" />
        <character is_modifier="true" name="architecture" src="d0_s10" value="branched" value_original="branched" />
      </biological_entity>
      <relation from="o12963" id="r2833" modifier="abaxially densely" name="covered with" negation="false" src="d0_s10" to="o12964" />
      <relation from="o12963" id="r2834" modifier="abaxially densely" name="covered with" negation="false" src="d0_s10" to="o12965" />
      <relation from="o12963" id="r2835" modifier="adaxially" name="with" negation="false" src="d0_s10" to="o12966" />
    </statement>
    <statement id="d0_s11">
      <text>False indusia marginal, slightly differentiated, 0.05–0.25 mm wide.</text>
      <biological_entity constraint="false" id="o12967" name="indusium" name_original="indusia" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="marginal" value_original="marginal" />
        <character is_modifier="false" modifier="slightly" name="variability" src="d0_s11" value="differentiated" value_original="differentiated" />
        <character char_type="range_value" from="0.05" from_unit="mm" name="width" src="d0_s11" to="0.25" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Sori ± continuous around segment margins.</text>
      <biological_entity id="o12968" name="sorus" name_original="sori" src="d0_s12" type="structure">
        <character constraint="around segment margins" constraintid="o12969" is_modifier="false" modifier="more or less" name="architecture" src="d0_s12" value="continuous" value_original="continuous" />
      </biological_entity>
      <biological_entity constraint="segment" id="o12969" name="margin" name_original="margins" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>Sporangia containing 64 spores.</text>
      <biological_entity id="o12970" name="sporangium" name_original="sporangia" src="d0_s13" type="structure" />
      <biological_entity id="o12971" name="spore" name_original="spores" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="64" value_original="64" />
      </biological_entity>
      <relation from="o12970" id="r2836" name="containing" negation="false" src="d0_s13" to="o12971" />
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Sporulating summer–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="sporulating time" char_type="range_value" to="fall" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Cliffs and rocky slopes, usually on igneous substrates</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="cliffs" />
        <character name="habitat" value="rocky slopes" />
        <character name="habitat" value="igneous substrates" modifier="usually on" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>800–3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="800" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C.; Calif., Idaho, Mont., Nev., Oreg., Utah, Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4</number>
  <other_name type="common_name">Lace fern</other_name>
  <discussion>Cheilanthes gracillima is a well-marked species, but it apparently hybridizes with C. intertexta (see reticulogram) to produce plants of intermediate morphology with malformed spores that have been called C. gracillima var. aberrans M. E. Jones (A. R. Smith 1974).</discussion>
  
</bio:treatment>