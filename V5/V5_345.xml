<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">174</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">175</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">armeria</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 420. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species armeria;</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="fna_id">242000720</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Atocion</taxon_name>
    <taxon_name authority="(Linnaeus) Rafinesque" date="unknown" rank="species">armeria</taxon_name>
    <taxon_hierarchy>genus Atocion;species armeria;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants annual, glabrous throughout, ± glaucous, sometimes glutnous in distal parts;</text>
      <biological_entity id="o6558" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character is_modifier="false" modifier="throughout" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="more or less" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity constraint="distal" id="o6559" name="part" name_original="parts" src="d0_s0" type="structure" />
      <relation from="o6558" id="r710" modifier="sometimes" name="in" negation="false" src="d0_s0" to="o6559" />
    </statement>
    <statement id="d0_s1">
      <text>taproot slender.</text>
      <biological_entity id="o6560" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems simple, branched in inflorescence, (10–) 20–40 (–70) cm.</text>
      <biological_entity id="o6561" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character constraint="in inflorescence" constraintid="o6562" is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character char_type="range_value" from="10" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s2" to="20" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" notes="" src="d0_s2" to="70" to_unit="cm" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" notes="" src="d0_s2" to="40" to_unit="cm" />
      </biological_entity>
      <biological_entity id="o6562" name="inflorescence" name_original="inflorescence" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>Leaves: basal withering before flowering, blade lanceolate-spatulate, 2–5 cm;</text>
      <biological_entity id="o6563" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity constraint="basal" id="o6564" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character constraint="before flowering" is_modifier="false" name="life_cycle" src="d0_s3" value="withering" value_original="withering" />
      </biological_entity>
      <biological_entity id="o6565" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate-spatulate" value_original="lanceolate-spatulate" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s3" to="5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline sessile to amplexicaulous, blade lanceolate to ovate or elliptic, 1–6 cm × 5–25 mm, apex acute.</text>
      <biological_entity id="o6566" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="cauline" id="o6567" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character char_type="range_value" from="sessile" name="architecture" src="d0_s4" to="amplexicaulous" />
      </biological_entity>
      <biological_entity id="o6568" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s4" to="ovate or elliptic" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s4" to="6" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s4" to="25" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6569" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences cymose, bracteate;</text>
      <biological_entity id="o6570" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="bracteate" value_original="bracteate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cyme capitate or with flowers clustered at end of slender branches;</text>
      <biological_entity id="o6571" name="cyme" name_original="cyme" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="capitate" value_original="capitate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="with flowers" value_original="with flowers" />
      </biological_entity>
      <biological_entity id="o6572" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character constraint="at end" constraintid="o6573" is_modifier="false" name="arrangement_or_growth_form" src="d0_s6" value="clustered" value_original="clustered" />
      </biological_entity>
      <biological_entity id="o6573" name="end" name_original="end" src="d0_s6" type="structure" />
      <biological_entity constraint="slender" id="o6574" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <relation from="o6571" id="r711" name="with" negation="false" src="d0_s6" to="o6572" />
      <relation from="o6573" id="r712" name="part_of" negation="false" src="d0_s6" to="o6574" />
    </statement>
    <statement id="d0_s7">
      <text>bracts lanceolate-acicular, 2–10 mm.</text>
      <biological_entity id="o6575" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate-acicular" value_original="lanceolate-acicular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s7" to="10" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels 0.1–0.5 cm.</text>
      <biological_entity id="o6576" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character char_type="range_value" from="0.1" from_unit="cm" name="some_measurement" src="d0_s8" to="0.5" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx usually purple-tinged, 10-veined, elongate, clavate, lobed, constricted proximally into narrow tube, 13–17 × 2.5–4 mm, rather membranous;</text>
      <biological_entity id="o6577" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o6578" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s9" value="purple-tinged" value_original="purple-tinged" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="10-veined" value_original="10-veined" />
        <character is_modifier="false" name="shape" src="d0_s9" value="elongate" value_original="elongate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="clavate" value_original="clavate" />
        <character is_modifier="false" name="shape" src="d0_s9" value="lobed" value_original="lobed" />
        <character constraint="into tube" constraintid="o6579" is_modifier="false" name="size" src="d0_s9" value="constricted" value_original="constricted" />
        <character char_type="range_value" from="13" from_unit="mm" name="length" notes="" src="d0_s9" to="17" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" notes="" src="d0_s9" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="rather" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o6579" name="tube" name_original="tube" src="d0_s9" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>lobes ovate-triangular, ca. 1 mm, apex obtuse;</text>
      <biological_entity id="o6580" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o6581" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate-triangular" value_original="ovate-triangular" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="1" value_original="1" />
      </biological_entity>
      <biological_entity id="o6582" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals pink (rarely white), unlobed, limb obovate, ca. 5 mm, base cuneate into claw 6–8 mm, auricles absent, appendages linear to lanceolate, 2–3 mm, apex acute;</text>
      <biological_entity id="o6583" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o6584" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" name="shape" src="d0_s11" value="unlobed" value_original="unlobed" />
      </biological_entity>
      <biological_entity id="o6585" name="limb" name_original="limb" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character name="some_measurement" src="d0_s11" unit="mm" value="5" value_original="5" />
      </biological_entity>
      <biological_entity id="o6586" name="base" name_original="base" src="d0_s11" type="structure">
        <character constraint="into claw" constraintid="o6587" is_modifier="false" name="shape" src="d0_s11" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o6587" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character char_type="range_value" from="6" from_unit="mm" name="some_measurement" src="d0_s11" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6588" name="auricle" name_original="auricles" src="d0_s11" type="structure">
        <character is_modifier="false" name="presence" src="d0_s11" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o6589" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s11" to="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s11" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6590" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens slightly longer than petal claws;</text>
      <biological_entity id="o6591" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o6592" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character constraint="than petal claws" constraintid="o6593" is_modifier="false" name="length_or_size" src="d0_s12" value="slightly longer" value_original="slightly longer" />
      </biological_entity>
      <biological_entity constraint="petal" id="o6593" name="claw" name_original="claws" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>styles 3 (–4), exserted.</text>
      <biological_entity id="o6594" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o6595" name="style" name_original="styles" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="4" />
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s13" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules oblong, 7–10 mm, opening by 6 (or 8) spreading teeth;</text>
      <biological_entity id="o6596" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s14" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6597" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="6" value_original="6" />
        <character is_modifier="true" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
      </biological_entity>
      <relation from="o6596" id="r713" name="opening by" negation="false" src="d0_s14" to="o6597" />
    </statement>
    <statement id="d0_s15">
      <text>carpophore 7–8 mm, glabrous.</text>
      <biological_entity id="o6598" name="carpophore" name_original="carpophore" src="d0_s15" type="structure">
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s15" to="8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds dark-brown, reniform-rotund, less than 1 mm diam., rugose.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 24 (Europe).</text>
      <biological_entity id="o6599" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="reniform-rotund" value_original="reniform-rotund" />
        <character char_type="range_value" from="0" from_unit="mm" name="diameter" src="d0_s16" to="1" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s16" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6600" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="24" value_original="24" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Waste places, disturbed ground</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="waste places" />
        <character name="habitat" value="disturbed ground" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; B.C., N.B., N.S., Ont., Que.; Calif., Conn., Del., D.C., Fla., Ill., Ind., Ky., Maine, Md., Mass., Mich., Minn., Mo., N.H., N.J., N.Y., N.C., Ohio, Oreg., Pa., S.C., Utah, Vt., Va., Wash., W.Va., Wis.; Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="D.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Fla." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Maine" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="introduced" />
        <character name="distribution" value="Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4.</number>
  <other_name type="common_name">Sweet-William catchfly</other_name>
  <other_name type="common_name">silène arméria</other_name>
  <discussion>The long-tubular, clavate calyx enclosing the unusually long carpophore helps to distinguish Silene armeria. It is an occasional and adventive garden escape.</discussion>
  
</bio:treatment>