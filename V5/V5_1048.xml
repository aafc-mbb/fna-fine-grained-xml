<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">511</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="mention_page">512</other_info_on_meta>
    <other_info_on_meta type="mention_page">513</other_info_on_meta>
    <other_info_on_meta type="illustration_page">509</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1937" rank="section">Axillares</taxon_name>
    <taxon_name authority="(Danser) Rechinger f." date="1936" rank="species">triangulivalvis</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>40: 297. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section axillares;species triangulivalvis;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417186</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Weinmann" date="unknown" rank="species">salicifolius</taxon_name>
    <taxon_name authority="Danser" date="unknown" rank="subspecies">triangulivalvis</taxon_name>
    <place_of_publication>
      <publication_title>Ned. Kruidk. Arch.</publication_title>
      <place_in_publication>1925: 415, plate 1, figs. 1, 2. 1926</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Rumex;species salicifolius;subspecies triangulivalvis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Weinmann" date="unknown" rank="species">salicifolius</taxon_name>
    <taxon_name authority="(Danser) J. C. Hickman" date="unknown" rank="variety">triangulivalvis</taxon_name>
    <taxon_hierarchy>genus Rumex;species salicifolius;variety triangulivalvis;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous, with vertical rootstock, occasionally with short-creeping rhizomes.</text>
      <biological_entity id="o27190" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o27191" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <biological_entity id="o27192" name="rhizome" name_original="rhizomes" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="short-creeping" value_original="short-creeping" />
      </biological_entity>
      <relation from="o27190" id="r3024" name="with" negation="false" src="d0_s0" to="o27191" />
      <relation from="o27190" id="r3025" modifier="occasionally" name="with" negation="false" src="d0_s0" to="o27192" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending or erect, usually producing axillary shoots below 1st-order inflorescence or at proximal nodes, (30–) 40–100 cm.</text>
      <biological_entity id="o27193" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o27194" name="shoot" name_original="shoots" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o27195" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="atypical_some_measurement" src="d0_s1" to="40" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="40" from_unit="cm" name="some_measurement" src="d0_s1" to="100" to_unit="cm" />
      </biological_entity>
      <relation from="o27193" id="r3026" modifier="usually" name="producing" negation="false" src="d0_s1" to="o27194" />
      <relation from="o27193" id="r3027" modifier="usually" name="below 1st-order inflorescence or at" negation="false" src="d0_s1" to="o27195" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades light or yellowish green, veins scarcely prominent abaxially, linear-lanceolate, 6–17 × 1–4 (–5) cm, usually ca. 5–6 times as long as wide, widest near middle, thin, not coriaceous, base cuneate, margins entire, flat or undulate, apex acute.</text>
      <biological_entity id="o27196" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s2" value="light" value_original="light" />
        <character is_modifier="false" name="coloration" src="d0_s2" value="yellowish green" value_original="yellowish green" />
      </biological_entity>
      <biological_entity id="o27197" name="vein" name_original="veins" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="scarcely; abaxially" name="prominence" src="d0_s2" value="prominent" value_original="prominent" />
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="17" to_unit="cm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s2" to="5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="4" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="5-6" value_original="5-6" />
        <character constraint="near middle veins" constraintid="o27198" is_modifier="false" name="width" src="d0_s2" value="widest" value_original="widest" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="coriaceous" value_original="coriaceous" />
      </biological_entity>
      <biological_entity constraint="middle" id="o27198" name="vein" name_original="veins" src="d0_s2" type="structure">
        <character is_modifier="false" name="width" notes="" src="d0_s2" value="thin" value_original="thin" />
      </biological_entity>
      <biological_entity id="o27199" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o27200" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="flat" value_original="flat" />
        <character is_modifier="false" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
      </biological_entity>
      <biological_entity id="o27201" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal and axillary, terminal usually occupying distal 1/5–1/3 of stem, rather dense or interrupted in proximal 1/2, broadly to narrowly paniculate (branches usually with 2d-order branches, rarely simple).</text>
      <biological_entity id="o27202" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="usually" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" modifier="rather" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character constraint="in proximal 1/2" constraintid="o27204" is_modifier="false" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="broadly to narrowly" name="arrangement" notes="" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o27203" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/5" is_modifier="true" name="quantity" src="d0_s3" to="1/3" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o27204" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <relation from="o27202" id="r3028" name="occupying" negation="false" src="d0_s3" to="o27203" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels articulated in proximal 1/3 or almost near base, filiform (but slightly thickened distally), 4–8 mm, usually ca. 1.5 times as long as inner tepals, articulation indistinctly swollen.</text>
      <biological_entity id="o27205" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character constraint="in " constraintid="o27207" is_modifier="false" name="architecture" src="d0_s4" value="articulated" value_original="articulated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
        <character constraint="tepal" constraintid="o27210" is_modifier="false" name="length" src="d0_s4" value="1.5 times as long as inner tepals" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o27206" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o27207" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="proximal" id="o27208" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity id="o27209" name="base" name_original="base" src="d0_s4" type="structure" />
      <biological_entity constraint="inner" id="o27210" name="tepal" name_original="tepals" src="d0_s4" type="structure" />
      <biological_entity id="o27211" name="articulation" name_original="articulation" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
      <relation from="o27207" id="r3029" name="in" negation="false" src="d0_s4" to="o27208" />
      <relation from="o27207" id="r3030" name="in" negation="false" src="d0_s4" to="o27209" />
    </statement>
    <statement id="d0_s5">
      <text>Flowers 10–25 in whorls;</text>
      <biological_entity id="o27212" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o27213" from="10" name="quantity" src="d0_s5" to="25" />
      </biological_entity>
      <biological_entity id="o27213" name="whorl" name_original="whorls" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>inner tepals broadly triangular, (2–) 2.5–3.5 (–3.8) × (2–) 2.5–3 (–3.5) mm, base truncate or rounded, margins entire or indistinctly erose only near base, apex acute, occasionally subobtuse-triangular;</text>
      <biological_entity constraint="inner" id="o27214" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_length" src="d0_s6" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s6" to="3.8" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="length" src="d0_s6" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="atypical_width" src="d0_s6" to="2.5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o27215" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o27216" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="entire" value_original="entire" />
        <character constraint="near base" constraintid="o27217" is_modifier="false" modifier="indistinctly" name="architecture" src="d0_s6" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o27217" name="base" name_original="base" src="d0_s6" type="structure" />
      <biological_entity id="o27218" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s6" value="subobtuse-triangular" value_original="subobtuse-triangular" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tubercles usually 3, (1 in some forms, then large, occupying at least 0.5 width of inner tepal), equal or subequal, much narrower than inner tepals glabrous or minutely verrucose.</text>
      <biological_entity id="o27219" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character is_modifier="false" name="size" src="d0_s7" value="subequal" value_original="subequal" />
        <character constraint="than inner tepals" constraintid="o27220" is_modifier="false" name="width" src="d0_s7" value="much narrower" value_original="much narrower" />
        <character is_modifier="false" modifier="minutely" name="relief" src="d0_s7" value="verrucose" value_original="verrucose" />
      </biological_entity>
      <biological_entity constraint="inner" id="o27220" name="tepal" name_original="tepals" src="d0_s7" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes brown or dark reddish-brown, 1.7–2.2 × 1–1.5 mm. 2n = 20.</text>
      <biological_entity id="o27221" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character char_type="range_value" from="1.7" from_unit="mm" name="length" src="d0_s8" to="2.2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o27222" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Many types of ruderal and alluvial habitats: waste places, roadsides, railroad embarkments, cultivated fields, meadows, sandy and gravelly shores, ditches</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="many types" constraint="of ruderal and alluvial habitats : waste places , roadsides , railroad embarkments , cultivated fields , meadows , sandy and gravelly shores , ditches" />
        <character name="habitat" value="ruderal" />
        <character name="habitat" value="alluvial habitats" />
        <character name="habitat" value="waste places" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="railroad embarkments" />
        <character name="habitat" value="cultivated fields" />
        <character name="habitat" value="meadows" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="gravelly shores" />
        <character name="habitat" value="ditches" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2500 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2500" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Alta., B.C., Man., N.B., N.S., N.W.T., Ont., P.E.I., Que., Sask., Yukon; Ariz., Calif., Colo., Conn., Idaho, Ill., Ind., Iowa, Kans., Ky., ?La., Maine., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.Dak., Ohio, Oreg., Pa., R.I., S.Dak., Tex., Utah, Vt., Wash., W.Va., Wis., Wyo.; Europe; introduced in Czech Republic, Denmark, Finland, Germany, Latvia, Norway, Russia, Switzerland, the Netherlands, Ukraine, United Kingdom, and elsewhere.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Alta." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Man." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.B." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.S." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="P.E.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Sask." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Yukon" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" value="?La" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Maine." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Minn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mont." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nebr." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.H." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="R.I." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.Dak." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Vt." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wis." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wyo." establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="in Czech Republic" establishment_means="introduced" />
        <character name="distribution" value="Denmark" establishment_means="introduced" />
        <character name="distribution" value="Finland" establishment_means="introduced" />
        <character name="distribution" value="Germany" establishment_means="introduced" />
        <character name="distribution" value="Latvia" establishment_means="introduced" />
        <character name="distribution" value="Norway" establishment_means="introduced" />
        <character name="distribution" value="Russia" establishment_means="introduced" />
        <character name="distribution" value="Switzerland" establishment_means="introduced" />
        <character name="distribution" value="the Netherlands" establishment_means="introduced" />
        <character name="distribution" value="Ukraine" establishment_means="introduced" />
        <character name="distribution" value="United Kingdom" establishment_means="introduced" />
        <character name="distribution" value="and elsewhere" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>24.</number>
  <other_name type="common_name">White</other_name>
  <other_name type="common_name">white willow</other_name>
  <other_name type="common_name">or triangular-valved dock</other_name>
  <discussion>Rumex triangulivalvis is the most common and widespread species of the R. salicifolius group. It often occurs in ruderal habitats and may be expected outside its present range.</discussion>
  <discussion>The names Rumex salicifolius and R. mexicanus (in the broad sense) were commonly applied to this species by many North American and European authors.</discussion>
  
</bio:treatment>