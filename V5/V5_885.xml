<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">425</other_info_on_meta>
    <other_info_on_meta type="mention_page">416</other_info_on_meta>
    <other_info_on_meta type="illustration_page">426</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Oregonium</taxon_name>
    <taxon_name authority="S. Watson" date="1875" rank="species">baileyi</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>10: 348. 1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oregonium;species baileyi;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060189</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="unknown" rank="species">vimineum</taxon_name>
    <taxon_name authority="(S. Watson) S. Stokes" date="unknown" rank="subspecies">baileyi</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species vimineum;subspecies baileyi;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect to spreading, 1–4 (–5) dm, glabrous or tomentose, grayish.</text>
      <biological_entity id="o28094" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="spreading" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="tomentose" value_original="tomentose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: aerial flowering-stems erect, 0.5–1 dm, glabrous or tomentose.</text>
      <biological_entity id="o28095" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o28096" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s1" to="1" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal;</text>
      <biological_entity id="o28097" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>petiole 0.5–3 cm, tomentose;</text>
      <biological_entity id="o28098" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade suborbiculate, 0.5–2 × 0.5–2 cm, densely white-tomentose abaxially, mostly tomentose and grayish to greenish adaxially.</text>
      <biological_entity id="o28099" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="suborbiculate" value_original="suborbiculate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s4" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s4" to="2" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s4" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" modifier="mostly" name="pubescence" src="d0_s4" value="tomentose" value_original="tomentose" />
        <character char_type="range_value" from="grayish" modifier="adaxially" name="coloration" src="d0_s4" to="greenish" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences cymose, infrequently distally uniparous due to suppression of secondary branches, open to diffuse, 5–35 (–45) × 5–35 (–4) cm;</text>
      <biological_entity id="o28100" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="cymose" value_original="cymose" />
        <character constraint="of secondary branches" constraintid="o28101" is_modifier="false" modifier="infrequently distally" name="architecture" src="d0_s5" value="uniparous" value_original="uniparous" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="open" value_original="open" />
        <character is_modifier="false" name="density" src="d0_s5" value="diffuse" value_original="diffuse" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="45" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s5" to="35" to_unit="cm" />
        <character char_type="range_value" from="35" from_inclusive="false" from_unit="cm" name="average_width" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s5" to="35" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o28101" name="branch" name_original="branches" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>branches glabrous or tomentose;</text>
      <biological_entity id="o28102" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 0.5–3 × 1–3 mm.</text>
      <biological_entity id="o28103" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s7" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles absent.</text>
      <biological_entity id="o28104" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres somewhat appressed to branches, turbinate, 1–1.5 × 0.5–1 mm, glabrous or tomentose;</text>
      <biological_entity id="o28105" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character constraint="to branches" constraintid="o28106" is_modifier="false" modifier="somewhat" name="fixation_or_orientation" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s9" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s9" to="1" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o28106" name="branch" name_original="branches" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>teeth 5, erect, 0.2–0.3 mm.</text>
      <biological_entity id="o28107" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s10" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 1.5–2 mm;</text>
      <biological_entity id="o28108" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth white to rose, minutely glandular, rarely glabrous;</text>
      <biological_entity id="o28109" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="rose" />
        <character is_modifier="false" modifier="minutely" name="architecture_or_function_or_pubescence" src="d0_s12" value="glandular" value_original="glandular" />
        <character is_modifier="false" modifier="rarely" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals monomorphic, oblong to oblong-obovate, somewhat constricted near middle and flaring adaxially;</text>
      <biological_entity id="o28110" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s13" to="oblong-obovate" />
        <character constraint="near middle" constraintid="o28111" is_modifier="false" modifier="somewhat" name="size" src="d0_s13" value="constricted" value_original="constricted" />
        <character is_modifier="false" modifier="adaxially" name="shape" notes="" src="d0_s13" value="flaring" value_original="flaring" />
      </biological_entity>
      <biological_entity id="o28111" name="middle" name_original="middle" src="d0_s13" type="structure" />
    </statement>
    <statement id="d0_s14">
      <text>stamens included, 1–1.5 mm;</text>
      <biological_entity id="o28112" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments pilose proximally.</text>
      <biological_entity id="o28113" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s15" value="pilose" value_original="pilose" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes brown, 3-gonous, 1–1.5 mm.</text>
      <biological_entity id="o28114" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Idaho, Nev., Oreg., Utah, Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Idaho" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>215.</number>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Eriogonum baileyi is closely related to and sometimes difficult to distinguish from E. elegans of the Inner Coast Ranges of California, and E. brachyanthum along the eastern edge of the Sierra Nevada, especially in Inyo and Mono counties. However, these species do not seem to intergrade. Eriogonum baileyi often grows with other annual wild buckwheats (especially E. ampullaceum and E. brachyanthum), so care must be taken not to make mixed collections.</discussion>
  <discussion>The Kawaiisu people of southern California pounded the seeds into a meal, which they ate dry, and also mixed with water to serve as a beverage (M. L. Zigmond 1981).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowering stems and inflorescence branches glabrous; e and s California, sc Idaho, Nevada, e Oregon, sw Utah, e Washington</description>
      <determination>215a Eriogonum baileyi var. baileyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowering stems and inflorescence branches tomentose; ne California, w and nc Nevada, c Idaho</description>
      <determination>215b Eriogonum baileyi var. praebens</determination>
    </key_statement>
  </key>
</bio:treatment>