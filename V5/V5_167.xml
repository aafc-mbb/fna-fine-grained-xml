<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">82</other_info_on_meta>
    <other_info_on_meta type="mention_page">74</other_info_on_meta>
    <other_info_on_meta type="mention_page">76</other_info_on_meta>
    <other_info_on_meta type="mention_page">77</other_info_on_meta>
    <other_info_on_meta type="mention_page">79</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cerastium</taxon_name>
    <taxon_name authority="Tolmatchew" date="1927" rank="species">bialynickii</taxon_name>
    <place_of_publication>
      <publication_title>Trudy Bot. Muz.</publication_title>
      <place_in_publication>21: 81, fig. 1. 1927</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus cerastium;species bialynickii;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060042</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Cerastium</taxon_name>
    <taxon_name authority="Lange" date="unknown" rank="species">arcticum</taxon_name>
    <taxon_name authority="Hultén" date="unknown" rank="variety">sordidum</taxon_name>
    <taxon_hierarchy>genus Cerastium;species arcticum;variety sordidum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, compact, pulvinate, taprooted.</text>
      <biological_entity id="o21762" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s0" value="compact" value_original="compact" />
        <character is_modifier="false" name="shape" src="d0_s0" value="pulvinate" value_original="pulvinate" />
        <character is_modifier="false" name="architecture" src="d0_s0" value="taprooted" value_original="taprooted" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending, much-branched at base, 2–10 cm, densely hispid-pubescent, hairs patent, fuscous, multicellular;</text>
      <biological_entity id="o21763" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character constraint="at base" constraintid="o21764" is_modifier="false" name="architecture" src="d0_s1" value="much-branched" value_original="much-branched" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" notes="" src="d0_s1" to="10" to_unit="cm" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s1" value="hispid-pubescent" value_original="hispid-pubescent" />
      </biological_entity>
      <biological_entity id="o21764" name="base" name_original="base" src="d0_s1" type="structure" />
      <biological_entity id="o21765" name="hair" name_original="hairs" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="patent" value_original="patent" />
        <character is_modifier="false" name="coloration" src="d0_s1" value="fuscous" value_original="fuscous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="multicellular" value_original="multicellular" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>internodes short;</text>
      <biological_entity id="o21766" name="internode" name_original="internodes" src="d0_s2" type="structure">
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s2" value="short" value_original="short" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>small axillary tufts of leaves absent.</text>
      <biological_entity constraint="axillary" id="o21767" name="tuft" name_original="tufts" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o21768" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="presence" src="d0_s3" value="absent" value_original="absent" />
      </biological_entity>
      <relation from="o21767" id="r2434" name="consist_of" negation="false" src="d0_s3" to="o21768" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves dense below the inflorescence;</text>
      <biological_entity id="o21769" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="below inflorescence" constraintid="o21770" is_modifier="false" name="density" src="d0_s4" value="dense" value_original="dense" />
      </biological_entity>
      <biological_entity id="o21770" name="inflorescence" name_original="inflorescence" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade broadly lanceolate, elliptic-lanceolate to ovate or obovate, 5–10 × 1.5–4 mm, thick and ± fleshy, apex broadly acute to obtuse, hispid, hairs fuscous, multicellular, long;</text>
      <biological_entity id="o21771" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="elliptic-lanceolate" name="shape" src="d0_s5" to="ovate or obovate" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s5" to="10" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" name="width" src="d0_s5" value="thick" value_original="thick" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o21772" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly acute" name="shape" src="d0_s5" to="obtuse" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="hispid" value_original="hispid" />
      </biological_entity>
      <biological_entity id="o21773" name="hair" name_original="hairs" src="d0_s5" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s5" value="fuscous" value_original="fuscous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="multicellular" value_original="multicellular" />
        <character is_modifier="false" name="length_or_size" src="d0_s5" value="long" value_original="long" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal leaves marcescent, sometimes subglabrous.</text>
      <biological_entity constraint="proximal" id="o21774" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" name="condition" src="d0_s6" value="marcescent" value_original="marcescent" />
        <character is_modifier="false" modifier="sometimes" name="pubescence" src="d0_s6" value="subglabrous" value_original="subglabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences often 1-flowered and compact, sometimes 2–3-flowered cymes, hairs patent, dense, fuscous, eglandular and glandular;</text>
      <biological_entity id="o21775" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="often" name="architecture" src="d0_s7" value="1-flowered" value_original="1-flowered" />
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s7" value="compact" value_original="compact" />
      </biological_entity>
      <biological_entity id="o21776" name="cyme" name_original="cymes" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="sometimes" name="architecture" src="d0_s7" value="2-3-flowered" value_original="2-3-flowered" />
      </biological_entity>
      <biological_entity id="o21777" name="hair" name_original="hairs" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="patent" value_original="patent" />
        <character is_modifier="false" name="density" src="d0_s7" value="dense" value_original="dense" />
        <character is_modifier="false" name="coloration" src="d0_s7" value="fuscous" value_original="fuscous" />
        <character is_modifier="false" name="architecture" src="d0_s7" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s7" value="glandular" value_original="glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts lanceolate, with or without narrow, scarious margins, densely pubescent, often glandular.</text>
      <biological_entity id="o21778" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s8" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="often" name="architecture_or_function_or_pubescence" notes="" src="d0_s8" value="glandular" value_original="glandular" />
      </biological_entity>
      <biological_entity id="o21779" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="true" name="size_or_width" src="d0_s8" value="narrow" value_original="narrow" />
        <character is_modifier="true" name="texture" src="d0_s8" value="scarious" value_original="scarious" />
      </biological_entity>
      <relation from="o21778" id="r2435" name="with or without" negation="false" src="d0_s8" to="o21779" />
    </statement>
    <statement id="d0_s9">
      <text>Pedicels straight or ± angled at base and/or apex, 5–12 mm, 1–2 times as long as sepals, densely pubescent with long, eglandular and short, glandular-hairs.</text>
      <biological_entity id="o21780" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character constraint="at base, apex" constraintid="o21781, o21782" is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="angled" value_original="angled" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" notes="" src="d0_s9" to="12" to_unit="mm" />
        <character constraint="sepal" constraintid="o21783" is_modifier="false" name="length" src="d0_s9" value="1-2 times as long as sepals" />
        <character constraint="with long" is_modifier="false" modifier="densely" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="eglandular" value_original="eglandular" />
        <character is_modifier="false" name="height_or_length_or_size" src="d0_s9" value="short" value_original="short" />
      </biological_entity>
      <biological_entity id="o21781" name="base" name_original="base" src="d0_s9" type="structure" />
      <biological_entity id="o21782" name="apex" name_original="apex" src="d0_s9" type="structure" />
      <biological_entity id="o21783" name="sepal" name_original="sepals" src="d0_s9" type="structure" />
      <biological_entity id="o21784" name="glandular-hair" name_original="glandular-hairs" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: sepals usually purplish, 5–6 (–7) mm, herbaceous center narrowly lanceolate, margins broad, making apex obtuse, densely pubescent, hairs long, stiff, glandular and eglandular;</text>
      <biological_entity id="o21785" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o21786" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s10" value="purplish" value_original="purplish" />
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s10" to="7" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s10" to="6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o21787" name="center" name_original="center" src="d0_s10" type="structure">
        <character is_modifier="true" name="growth_form_or_texture" src="d0_s10" value="herbaceous" value_original="herbaceous" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="lanceolate" value_original="lanceolate" />
      </biological_entity>
      <biological_entity id="o21788" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21789" name="apex" name_original="apex" src="d0_s10" type="structure" />
      <biological_entity id="o21790" name="hair" name_original="hairs" src="d0_s10" type="structure">
        <character is_modifier="false" name="length_or_size" src="d0_s10" value="long" value_original="long" />
        <character is_modifier="false" name="fragility" src="d0_s10" value="stiff" value_original="stiff" />
        <character is_modifier="false" name="architecture_or_function_or_pubescence" src="d0_s10" value="glandular" value_original="glandular" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="eglandular" value_original="eglandular" />
      </biological_entity>
      <relation from="o21788" id="r2436" name="making" negation="false" src="d0_s10" to="o21789" />
    </statement>
    <statement id="d0_s11">
      <text>petals oblanceolate, 7–9 mm, 1–1.5 times as long as sepals, apex 2-fid;</text>
      <biological_entity id="o21791" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o21792" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s11" to="9" to_unit="mm" />
        <character constraint="sepal" constraintid="o21793" is_modifier="false" name="length" src="d0_s11" value="1-1.5 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o21793" name="sepal" name_original="sepals" src="d0_s11" type="structure" />
      <biological_entity id="o21794" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="2-fid" value_original="2-fid" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 10;</text>
      <biological_entity id="o21795" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o21796" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="10" value_original="10" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 5.</text>
      <biological_entity id="o21797" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o21798" name="style" name_original="styles" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules cylindric, slightly curved, 9–12 mm, 1.5–2 times as long as sepals;</text>
      <biological_entity id="o21799" name="capsule" name_original="capsules" src="d0_s14" type="structure">
        <character is_modifier="false" name="shape" src="d0_s14" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="slightly" name="course" src="d0_s14" value="curved" value_original="curved" />
        <character char_type="range_value" from="9" from_unit="mm" name="some_measurement" src="d0_s14" to="12" to_unit="mm" />
        <character constraint="sepal" constraintid="o21800" is_modifier="false" name="length" src="d0_s14" value="1.5-2 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o21800" name="sepal" name_original="sepals" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>teeth 10, erect, margins convolute.</text>
      <biological_entity id="o21801" name="tooth" name_original="teeth" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="10" value_original="10" />
        <character is_modifier="false" name="orientation" src="d0_s15" value="erect" value_original="erect" />
      </biological_entity>
      <biological_entity id="o21802" name="margin" name_original="margins" src="d0_s15" type="structure">
        <character is_modifier="false" name="arrangement_or_shape" src="d0_s15" value="convolute" value_original="convolute" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds brown, 0.8–1 mm, tuberculate;</text>
      <biological_entity id="o21803" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s16" to="1" to_unit="mm" />
        <character is_modifier="false" name="relief" src="d0_s16" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>testa not inflated, tightly enclosing seed.</text>
      <biological_entity id="o21805" name="seed" name_original="seed" src="d0_s17" type="structure" />
      <relation from="o21804" id="r2437" modifier="tightly" name="enclosing" negation="false" src="d0_s17" to="o21805" />
    </statement>
    <statement id="d0_s18">
      <text>2n = 108.</text>
      <biological_entity id="o21804" name="testa" name_original="testa" src="d0_s17" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s17" value="inflated" value_original="inflated" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21806" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="108" value_original="108" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Tundra, rocky exposures, screes, nunataks in the high arctic</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="tundra" />
        <character name="habitat" value="rocky exposures" />
        <character name="habitat" value="screes" />
        <character name="habitat" value="the high arctic" modifier="nunataks in" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Greenland; N.W.T., Nunavut; Alaska; Eurasia (Russian Far East, arctic Siberia and associated islands, Spitzbergen).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Greenland" establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="N.W.T." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Nunavut" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" value="Eurasia (Russian Far East)" establishment_means="native" />
        <character name="distribution" value="Eurasia (arctic Siberia and associated islands)" establishment_means="native" />
        <character name="distribution" value="Eurasia (Spitzbergen)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Bialynick’s mouse-ear chickweed</other_name>
  <discussion>Cerastium bialynickii previously was included in C. arcticum but is very different from that species, being a small, compact plant with a dense, hispid, fuscous pubescence, a small calyx with the broad, scarious margins making it obtuse, and small flowers, capsules, and seeds. Cerastium bialynickii resembles small compact plants of C. beeringianum but it differs in calyx shape and chromosome number.</discussion>
  
</bio:treatment>