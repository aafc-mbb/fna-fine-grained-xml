<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">69</other_info_on_meta>
    <other_info_on_meta type="mention_page">68</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Fenzl" date="1833" rank="genus">eremogone</taxon_name>
    <taxon_name authority="(S. Watson) Ikonnikov" date="1973" rank="species">macradenia</taxon_name>
    <taxon_name authority="(Maguire) R. L. Hartman &amp; Rabeler" date="2004" rank="variety">arcuifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 240. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus eremogone;species macradenia;variety arcuifolia;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060155</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">macradenia</taxon_name>
    <taxon_name authority="Maguire" date="unknown" rank="variety">arcuifolia</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>74: 51. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Arenaria;species macradenia;variety arcuifolia;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">kuschei</taxon_name>
    <taxon_hierarchy>genus Arenaria;species kuschei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Arenaria</taxon_name>
    <taxon_name authority="S. Watson" date="unknown" rank="species">macradenia</taxon_name>
    <taxon_name authority="(Eastwood) Maguire" date="unknown" rank="variety">kuschei</taxon_name>
    <taxon_hierarchy>genus Arenaria;species macradenia;variety kuschei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eremogone</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">macradenia</taxon_name>
    <taxon_name authority="(Eastwood) R. L. Hartman &amp; Rabeler" date="unknown" rank="variety">kuschei</taxon_name>
    <taxon_hierarchy>genus Eremogone;species macradenia;variety kuschei;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Cauline leaves in 6–12+ pairs;</text>
      <biological_entity constraint="cauline" id="o19455" name="leaf" name_original="leaves" src="d0_s0" type="structure" />
      <biological_entity id="o19456" name="pair" name_original="pairs" src="d0_s0" type="structure">
        <character char_type="range_value" from="6" is_modifier="true" name="quantity" src="d0_s0" to="12" upper_restricted="false" />
      </biological_entity>
      <relation from="o19455" id="r2170" name="in" negation="false" src="d0_s0" to="o19456" />
    </statement>
    <statement id="d0_s1">
      <text>blade curved downward, especially proximal ones, 1.2–2 mm wide.</text>
      <biological_entity id="o19457" name="blade" name_original="blade" src="d0_s1" type="structure">
        <character is_modifier="false" name="course" src="d0_s1" value="curved" value_original="curved" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="downward" value_original="downward" />
        <character is_modifier="false" modifier="especially" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="1.2" from_unit="mm" name="width" src="d0_s1" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Inflorescences: branches erect to ascending.</text>
      <biological_entity id="o19458" name="inflorescence" name_original="inflorescences" src="d0_s2" type="structure" />
      <biological_entity id="o19459" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s2" to="ascending" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Flowers: sepals 4.5–7 mm, to 7 mm in fruit, ± glabrous or sparsely to rarely densely stipitate-glandular;</text>
      <biological_entity id="o19460" name="flower" name_original="flowers" src="d0_s3" type="structure" />
      <biological_entity id="o19461" name="sepal" name_original="sepals" src="d0_s3" type="structure">
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o19462" from="0" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
        <character char_type="range_value" from="less glabrous or" name="pubescence" notes="" src="d0_s3" to="sparsely rarely densely stipitate-glandular" />
      </biological_entity>
      <biological_entity id="o19462" name="fruit" name_original="fruit" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>nectaries narrowly longitudinally rectangular, truncate, 0.7–0.8 mm, densely minutely pubescent with erect to spreading hairs.</text>
      <biological_entity id="o19463" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity id="o19464" name="nectary" name_original="nectaries" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="narrowly longitudinally" name="shape" src="d0_s4" value="rectangular" value_original="rectangular" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="truncate" value_original="truncate" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s4" to="0.8" to_unit="mm" />
        <character constraint="with hairs" constraintid="o19465" is_modifier="false" modifier="densely minutely" name="pubescence" src="d0_s4" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o19465" name="hair" name_original="hairs" src="d0_s4" type="structure">
        <character char_type="range_value" from="erect" is_modifier="true" name="orientation" src="d0_s4" to="spreading" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Dry slopes and foothills, dry, yellow pine and oak forests</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="dry slopes" />
        <character name="habitat" value="foothills" />
        <character name="habitat" value="dry" />
        <character name="habitat" value="yellow pine" />
        <character name="habitat" value="oak forests" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>600-2400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2400" to_unit="m" from="600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>11b.</number>
  <discussion>While B. Maguire (1947, 1951) regarded var. arcuifolia as “a strongly marked variant,” R. L. Hartman (1993) noted that it “intergrades with var. macradenia and might be considered the same as the latter.”</discussion>
  <discussion>The collection from “Forest Camp” described as Arenaria macradenia var. kuschei differs from var. arcuifolia in having densely stipitate-glandular pedicels and sepals; it does not deserve taxonomic recognition. Recently, populations of plants matching var. kuschei were discovered in the Liebre Mountains, northwestern Los Angeles County (T. S. Ross and S. Boyd 1996b). Four of the seven populations were mixed, some individuals having the stipitate-glandular pubescence of var. kuschei, and others, the glabrous inflorescences of var. arcuifolia. Furthermore, there was an east-to-west trend along the summit of Liebre Mountain from populations with a low frequency of glandular individuals to populations that were strictly glandular (Boyd, pers. comm.).</discussion>
  
</bio:treatment>