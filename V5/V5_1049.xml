<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">512</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1937" rank="section">Axillares</taxon_name>
    <taxon_name authority="Rechinger f." date="1936" rank="species">transitorius</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>40: 296. 1936</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section axillares;species transitorius;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060804</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Weinmann" date="unknown" rank="species">salicifolius</taxon_name>
    <taxon_name authority="(Rechinger f.) J. C. Hickman" date="unknown" rank="variety">transitorius</taxon_name>
    <taxon_hierarchy>genus Rumex;species salicifolius;variety transitorius;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous, with vertical rootstock.</text>
      <biological_entity id="o16420" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o16421" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o16420" id="r1831" name="with" negation="false" src="d0_s0" to="o16421" />
    </statement>
    <statement id="d0_s1">
      <text>Stems ascending, ascending-decumbent, or erect, simple or producing axillary shoots below 1st-order inflorescence or at proximal nodes, 25–70 cm.</text>
      <biological_entity id="o16422" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending-decumbent" value_original="ascending-decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending-decumbent" value_original="ascending-decumbent" />
        <character is_modifier="false" name="orientation" src="d0_s1" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="simple" value_original="simple" />
        <character name="architecture" src="d0_s1" value="producing axillary shoots" value_original="producing axillary shoots" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o16423" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="25" from_unit="cm" name="some_measurement" src="d0_s1" to="70" to_unit="cm" />
      </biological_entity>
      <relation from="o16422" id="r1832" name="below 1st-order inflorescence or at" negation="false" src="d0_s1" to="o16423" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades linear-lanceolate or lanceolate, 6–15 (–17) × 2–4 cm, usually ca. 3.5–6 times as long as wide, widest near middle or nearly so, thin or rarely subcoriaceous, base cuneate, margins entire, undulate or slightly crisped, apex acute.</text>
      <biological_entity id="o16424" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="15" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s2" to="17" to_unit="cm" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="15" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="width" src="d0_s2" to="4" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="3.5-6" value_original="3.5-6" />
        <character constraint="near " constraintid="o16428" is_modifier="false" name="width" src="d0_s2" value="widest" value_original="widest" />
      </biological_entity>
      <biological_entity id="o16425" name="middle" name_original="middle" src="d0_s2" type="structure" />
      <biological_entity id="o16426" name="middle" name_original="middle" src="d0_s2" type="structure" />
      <biological_entity id="o16427" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" name="width" src="d0_s2" value="thin" value_original="thin" />
        <character is_modifier="true" modifier="rarely" name="texture" src="d0_s2" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o16428" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="rarely" name="texture" src="d0_s2" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o16429" name="middle" name_original="middle" src="d0_s2" type="structure" />
      <biological_entity id="o16430" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="true" modifier="rarely" name="texture" src="d0_s2" value="subcoriaceous" value_original="subcoriaceous" />
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o16431" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s2" value="crisped" value_original="crisped" />
      </biological_entity>
      <biological_entity id="o16432" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
      <relation from="o16428" id="r1833" name="near" negation="false" src="d0_s2" to="o16429" />
      <relation from="o16428" id="r1834" modifier="near middle or so , thin or rarely subcoriaceous , base" name="near" negation="false" src="d0_s2" to="o16430" />
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal and axillary, terminal usually occupying distal 1/5–1/3 of stem, dense or occasionally interrupted near base, usually broadly paniculate (branches simple or with few 2d-order branches).</text>
      <biological_entity id="o16433" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="usually" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character constraint="near base" constraintid="o16435" is_modifier="false" modifier="occasionally" name="architecture" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="usually broadly" name="arrangement" notes="" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o16434" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/5" is_modifier="true" name="quantity" src="d0_s3" to="1/3" />
      </biological_entity>
      <biological_entity id="o16435" name="base" name_original="base" src="d0_s3" type="structure" />
      <relation from="o16433" id="r1835" name="occupying" negation="false" src="d0_s3" to="o16434" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels articulated in proximal 1/3, filiform, 3–7 mm, equaling or 1.5–2 times as long as inner tepals, articulation indistinctly swollen.</text>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 10–20 (–25) in whorls;</text>
      <biological_entity id="o16438" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" from="20" from_inclusive="false" name="atypical_quantity" src="d0_s5" to="25" />
        <character char_type="range_value" constraint="in whorls" constraintid="o16439" from="10" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o16439" name="whorl" name_original="whorls" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>inner tepals, broadly ovate to ovatelanceolate, occasionally almost triangular, (2.5–) 3–3.5 × 2–2.5 mm, base truncate or rounded, margins entire or indistinctly erose, apex obtuse or subacute;</text>
      <biological_entity constraint="inner" id="o16440" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="broadly ovate" name="shape" src="d0_s6" to="ovatelanceolate" />
        <character is_modifier="false" modifier="occasionally almost" name="shape" src="d0_s6" value="triangular" value_original="triangular" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="atypical_length" src="d0_s6" to="3" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="3.5" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s6" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o16441" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="truncate" value_original="truncate" />
        <character is_modifier="false" name="shape" src="d0_s6" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o16442" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="indistinctly" name="architecture" src="d0_s6" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity id="o16443" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tubercles 3 (occasionally 1 in var. monotylos Rechinger f., then very large, subequal or only slightly narrower than inner tepal), distinctly unequal (1 larger tubercle subequal or slightly narrower than inner tepal), usually smooth.</text>
      <biological_entity id="o16444" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" modifier="distinctly" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character is_modifier="false" modifier="usually" name="architecture_or_pubescence_or_relief" src="d0_s7" value="smooth" value_original="smooth" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes dark reddish-brown, 1.8–2.4 × 1–1.5 mm. 2n = 20.</text>
      <biological_entity id="o16445" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="length" src="d0_s8" to="2.4" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="1.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o16446" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering late spring–early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="late spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Coastal dunes and marshes, shores of rivers and streams, wet meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal dunes" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="shores" constraint="of rivers and streams" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-2000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2000" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Alaska, Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Alaska" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>25.</number>
  <other_name type="common_name">Pacific willow dock</other_name>
  <discussion>J. T. Kartesz (1987, vol. 1) reported Rumex transitorius from Washoe County, Nevada; the morphological characters mentioned in his description suggest another taxon of the R. salicifolius aggregate. Records from Idaho also need confirmation.</discussion>
  
</bio:treatment>