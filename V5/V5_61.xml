<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">36</other_info_on_meta>
    <other_info_on_meta type="mention_page">32</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Meisner" date="unknown" rank="subfamily">Paronychioideae</taxon_name>
    <taxon_name authority="Miller" date="1754" rank="genus">paronychia</taxon_name>
    <taxon_name authority="Correll" date="1967" rank="species">congesta</taxon_name>
    <place_of_publication>
      <publication_title>Brittonia</publication_title>
      <place_in_publication>18: 307. 1967</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily paronychioideae;genus paronychia;species congesta;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060674</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
      <biological_entity id="o9081" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>caudex woody, branched.</text>
      <biological_entity id="o9082" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="woody" value_original="woody" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="branched" value_original="branched" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, 6–10 cm, evenly puberulent with spreading hairs.</text>
      <biological_entity id="o9083" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character char_type="range_value" from="6" from_unit="cm" name="some_measurement" src="d0_s2" to="10" to_unit="cm" />
        <character constraint="with hairs" constraintid="o9084" is_modifier="false" modifier="evenly" name="pubescence" src="d0_s2" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o9084" name="hair" name_original="hairs" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves: stipules lanceolate, 4–7 mm, apex acuminate, entire;</text>
      <biological_entity id="o9085" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o9086" name="stipule" name_original="stipules" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s3" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o9087" name="apex" name_original="apex" src="d0_s3" type="structure">
        <character is_modifier="false" name="shape" src="d0_s3" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s3" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade linear or needlelike, 4–7 × ca. 0.5 mm, leathery, apex spinulose, puberulous.</text>
      <biological_entity id="o9088" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o9089" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s4" value="needlelike" value_original="needlelike" />
        <character char_type="range_value" from="4" from_unit="mm" name="length" src="d0_s4" to="7" to_unit="mm" />
        <character name="width" src="d0_s4" unit="mm" value="0.5" value_original="0.5" />
        <character is_modifier="false" name="texture" src="d0_s4" value="leathery" value_original="leathery" />
      </biological_entity>
      <biological_entity id="o9090" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="spinulose" value_original="spinulose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="puberulous" value_original="puberulous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Cymes terminal, 7–28+-flowered, branched, mostly congested in clusters 5–15 mm wide.</text>
      <biological_entity id="o9091" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="7-28+-flowered" value_original="7-28+-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="branched" value_original="branched" />
        <character constraint="in clusters" is_modifier="false" modifier="mostly" name="architecture_or_arrangement" src="d0_s5" value="congested" value_original="congested" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" src="d0_s5" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Flowers 5-merous, extended-urecolate, with prominent hypanthial bulge and calyx tapering somewhat distally, 2.8–3.1 mm, moderately pubescent with straight, spreading to appressed hairs generally throughout;</text>
      <biological_entity id="o9092" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="5-merous" value_original="5-merous" />
      </biological_entity>
      <biological_entity id="o9093" name="hypanthial" name_original="hypanthial" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o9094" name="bulge" name_original="bulge" src="d0_s6" type="structure">
        <character is_modifier="true" name="prominence" src="d0_s6" value="prominent" value_original="prominent" />
      </biological_entity>
      <biological_entity id="o9095" name="calyx" name_original="calyx" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="somewhat distally; distally" name="shape" src="d0_s6" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="2.8" from_unit="mm" name="some_measurement" src="d0_s6" to="3.1" to_unit="mm" />
        <character constraint="with hairs" constraintid="o9096" is_modifier="false" modifier="moderately" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o9096" name="hair" name_original="hairs" src="d0_s6" type="structure">
        <character is_modifier="true" name="course" src="d0_s6" value="straight" value_original="straight" />
        <character char_type="range_value" from="spreading" is_modifier="true" name="orientation" src="d0_s6" to="appressed" />
      </biological_entity>
      <relation from="o9092" id="r1011" name="with" negation="false" src="d0_s6" to="o9093" />
      <relation from="o9092" id="r1012" name="with" negation="false" src="d0_s6" to="o9094" />
    </statement>
    <statement id="d0_s7">
      <text>sepals green to redbrown, veins absent or midrib and lateral pair of veins evident in fruit, lanceolate to oblong, 1.3–1.5 mm, leathery to rigid, margins translucent, less than 0.05 mm wide, scarious, apex terminated by awn, hood prominent, narrowly rounded, awn widely divergent, 0.5–0.7 mm, broadly conic in proximal 1/2–2/3 with yellowish, ± glabrous spine;</text>
      <biological_entity id="o9097" name="sepal" name_original="sepals" src="d0_s7" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s7" to="redbrown" />
      </biological_entity>
      <biological_entity id="o9098" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity id="o9099" name="midrib" name_original="midrib" src="d0_s7" type="structure">
        <character constraint="of veins" constraintid="o9100" is_modifier="false" name="position" src="d0_s7" value="lateral" value_original="lateral" />
        <character char_type="range_value" from="lanceolate" name="shape" notes="" src="d0_s7" to="oblong" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="leathery" name="texture" src="d0_s7" to="rigid" />
      </biological_entity>
      <biological_entity id="o9100" name="vein" name_original="veins" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o9101" is_modifier="false" name="prominence" src="d0_s7" value="evident" value_original="evident" />
      </biological_entity>
      <biological_entity id="o9101" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
      <biological_entity id="o9102" name="margin" name_original="margins" src="d0_s7" type="structure">
        <character is_modifier="false" name="coloration_or_reflectance" src="d0_s7" value="translucent" value_original="translucent" />
        <character char_type="range_value" from="0" from_unit="mm" name="width" src="d0_s7" to="0.05" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s7" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o9103" name="apex" name_original="apex" src="d0_s7" type="structure" />
      <biological_entity id="o9104" name="awn" name_original="awn" src="d0_s7" type="structure" />
      <biological_entity id="o9105" name="hood" name_original="hood" src="d0_s7" type="structure">
        <character is_modifier="false" name="prominence" src="d0_s7" value="prominent" value_original="prominent" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s7" value="rounded" value_original="rounded" />
      </biological_entity>
      <biological_entity id="o9106" name="awn" name_original="awn" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="widely" name="arrangement" src="d0_s7" value="divergent" value_original="divergent" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="0.7" to_unit="mm" />
        <character constraint="in yellowish" constraintid="o9107" is_modifier="false" modifier="broadly" name="shape" src="d0_s7" value="conic" value_original="conic" />
      </biological_entity>
      <biological_entity id="o9107" name="yellowish" name_original="yellowish" src="d0_s7" type="structure">
        <character is_modifier="true" name="position" src="d0_s7" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="1/2" is_modifier="true" name="quantity" src="d0_s7" to="2/3" />
      </biological_entity>
      <biological_entity id="o9108" name="spine" name_original="spine" src="d0_s7" type="structure">
        <character is_modifier="true" modifier="more or less" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o9103" id="r1013" name="terminated by" negation="false" src="d0_s7" to="o9104" />
    </statement>
    <statement id="d0_s8">
      <text>staminodes subulate, ca. 0.5 mm;</text>
      <biological_entity id="o9109" name="staminode" name_original="staminodes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="subulate" value_original="subulate" />
        <character name="some_measurement" src="d0_s8" unit="mm" value="0.5" value_original="0.5" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>style 1, 2-lobed in distal 1/3–3/5, 0.5–0.7 mm.</text>
      <biological_entity id="o9110" name="style" name_original="style" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character constraint="in distal 1/3-3/5 , 0.5-0.7 mm" is_modifier="false" name="shape" src="d0_s9" value="2-lobed" value_original="2-lobed" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Utricles ± ovoid, 0.8–0.9 mm, ± smooth, glabrous.</text>
      <biological_entity id="o9111" name="utricle" name_original="utricles" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s10" value="ovoid" value_original="ovoid" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s10" to="0.9" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="architecture_or_pubescence_or_relief" src="d0_s10" value="smooth" value_original="smooth" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Thorn scrubland openings</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="scrubland openings" modifier="thorn" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>7.</number>
  <other_name type="common_name">Rio Grande nailwort</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Paronychia congesta is known only from two collections in Jim Hogg County.</discussion>
  
</bio:treatment>