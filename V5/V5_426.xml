<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">209</other_info_on_meta>
    <other_info_on_meta type="mention_page">172</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">202</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="B. L. Robinson" date="1891" rank="species">suksdorfii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>16: 44, plate 6, figs. 9–11. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species suksdorfii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060896</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, cespitose, with decumbent subterranean shoots;</text>
      <biological_entity id="o12933" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o12934" name="shoot" name_original="shoots" src="d0_s0" type="structure">
        <character is_modifier="true" name="growth_form_or_orientation" src="d0_s0" value="decumbent" value_original="decumbent" />
        <character is_modifier="true" name="location" src="d0_s0" value="subterranean" value_original="subterranean" />
      </biological_entity>
      <relation from="o12933" id="r1409" name="with" negation="false" src="d0_s0" to="o12934" />
    </statement>
    <statement id="d0_s1">
      <text>taproot stout;</text>
      <biological_entity id="o12935" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex branched, woody.</text>
      <biological_entity id="o12936" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="texture" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Stems numerous, erect, simple, 3–15 cm, pubescent, viscid-glandular distally.</text>
      <biological_entity id="o12937" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s3" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="orientation" src="d0_s3" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character char_type="range_value" from="3" from_unit="cm" name="some_measurement" src="d0_s3" to="15" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="distally" name="architecture_or_function_or_pubescence" src="d0_s3" value="viscid-glandular" value_original="viscid-glandular" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves mostly basal, densely tufted;</text>
      <biological_entity id="o12938" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="densely" name="arrangement_or_pubescence" notes="" src="d0_s4" value="tufted" value_original="tufted" />
      </biological_entity>
      <biological_entity id="o12939" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s4" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>basal numerous, pseudopetiolate, blade narrowly oblanceolate, tapering into base, 0.5–3 cm × 1.5–4 mm, ± fleshy, apex acute, puberulent;</text>
      <biological_entity constraint="basal" id="o12940" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s5" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="pseudopetiolate" value_original="pseudopetiolate" />
      </biological_entity>
      <biological_entity id="o12941" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character constraint="into base" constraintid="o12942" is_modifier="false" name="shape" src="d0_s5" value="tapering" value_original="tapering" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" notes="" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" notes="" src="d0_s5" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="more or less" name="texture" src="d0_s5" value="fleshy" value_original="fleshy" />
      </biological_entity>
      <biological_entity id="o12942" name="base" name_original="base" src="d0_s5" type="structure" />
      <biological_entity id="o12943" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="puberulent" value_original="puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>cauline in 1–3 pairs, ± sessile, reduced, blade narrowly oblanceolate to linear-lanceolate, 0.7–2 cm × 1–3 mm, apex acute, puberulent.</text>
      <biological_entity constraint="cauline" id="o12944" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="more or less" name="architecture" notes="" src="d0_s6" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="size" src="d0_s6" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o12945" name="pair" name_original="pairs" src="d0_s6" type="structure">
        <character char_type="range_value" from="1" is_modifier="true" name="quantity" src="d0_s6" to="3" />
      </biological_entity>
      <biological_entity id="o12946" name="blade" name_original="blade" src="d0_s6" type="structure">
        <character char_type="range_value" from="narrowly oblanceolate" name="shape" src="d0_s6" to="linear-lanceolate" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s6" to="2" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s6" to="3" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12947" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="acute" value_original="acute" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <relation from="o12944" id="r1410" name="in" negation="false" src="d0_s6" to="o12945" />
    </statement>
    <statement id="d0_s7">
      <text>Inflorescences: flowers terminal, solitary, or in single dichotomy, bracteate;</text>
      <biological_entity id="o12948" name="inflorescence" name_original="inflorescences" src="d0_s7" type="structure" />
      <biological_entity id="o12949" name="flower" name_original="flowers" src="d0_s7" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s7" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="architecture_or_arrangement_or_growth_form" src="d0_s7" value="solitary" value_original="solitary" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s7" value="bracteate" value_original="bracteate" />
      </biological_entity>
      <biological_entity id="o12950" name="dichotomy" name_original="dichotomy" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="single" value_original="single" />
      </biological_entity>
      <relation from="o12949" id="r1411" name="in" negation="false" src="d0_s7" to="o12950" />
    </statement>
    <statement id="d0_s8">
      <text>bracts leaflike, 3–15 mm.</text>
      <biological_entity id="o12951" name="inflorescence" name_original="inflorescences" src="d0_s8" type="structure" />
      <biological_entity id="o12952" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s8" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s8" to="15" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Pedicels erect, ca. equaling calyx, viscid glandular-pubescent, hairs with purple septa.</text>
      <biological_entity id="o12953" name="pedicel" name_original="pedicels" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="coating" notes="" src="d0_s9" value="viscid" value_original="viscid" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o12954" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="true" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o12955" name="hair" name_original="hairs" src="d0_s9" type="structure" />
      <biological_entity id="o12956" name="septum" name_original="septa" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration_or_density" src="d0_s9" value="purple" value_original="purple" />
      </biological_entity>
      <relation from="o12955" id="r1412" name="with" negation="false" src="d0_s9" to="o12956" />
    </statement>
    <statement id="d0_s10">
      <text>Flowers: calyx prominently 10-veined, campanulate, not contracted proximally around carpophore, 10–15 × 5–7 mm, papery, veins parallel, purplish, with pale commissures, with purple-septate glandular-hairs (rarely septa not purple), lobes ovate, ca. 2 mm, margins broad, membranous, apex obtuse;</text>
      <biological_entity id="o12957" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o12958" name="calyx" name_original="calyx" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="prominently" name="architecture" src="d0_s10" value="10-veined" value_original="10-veined" />
        <character is_modifier="false" name="shape" src="d0_s10" value="campanulate" value_original="campanulate" />
        <character constraint="proximally around carpophore" constraintid="o12959" is_modifier="false" modifier="not" name="condition_or_size" src="d0_s10" value="contracted" value_original="contracted" />
        <character char_type="range_value" from="10" from_unit="mm" name="length" notes="" src="d0_s10" to="15" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="" src="d0_s10" to="7" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s10" value="papery" value_original="papery" />
      </biological_entity>
      <biological_entity id="o12959" name="carpophore" name_original="carpophore" src="d0_s10" type="structure" />
      <biological_entity id="o12960" name="vein" name_original="veins" src="d0_s10" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s10" value="parallel" value_original="parallel" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="purplish" value_original="purplish" />
      </biological_entity>
      <biological_entity id="o12961" name="commissure" name_original="commissures" src="d0_s10" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s10" value="pale" value_original="pale" />
      </biological_entity>
      <biological_entity id="o12962" name="glandular-hair" name_original="glandular-hairs" src="d0_s10" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s10" value="purple-septate" value_original="purple-septate" />
      </biological_entity>
      <biological_entity id="o12963" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="ovate" value_original="ovate" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="2" value_original="2" />
      </biological_entity>
      <biological_entity id="o12964" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="width" src="d0_s10" value="broad" value_original="broad" />
        <character is_modifier="false" name="texture" src="d0_s10" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o12965" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="obtuse" value_original="obtuse" />
      </biological_entity>
      <relation from="o12960" id="r1413" name="with" negation="false" src="d0_s10" to="o12961" />
      <relation from="o12960" id="r1414" name="with" negation="false" src="d0_s10" to="o12962" />
    </statement>
    <statement id="d0_s11">
      <text>corolla off-white or tinged with dusky purple, clawed, claw equaling calyx, broadened distally, limb 2-lobed, 3–5 mm, appendages ca. 1 mm;</text>
      <biological_entity id="o12966" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o12967" name="corolla" name_original="corolla" src="d0_s11" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s11" value="off-white" value_original="off-white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="tinged with dusky purple" value_original="tinged with dusky purple" />
        <character is_modifier="false" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o12968" name="claw" name_original="claw" src="d0_s11" type="structure">
        <character is_modifier="false" modifier="distally" name="width" notes="" src="d0_s11" value="broadened" value_original="broadened" />
      </biological_entity>
      <biological_entity id="o12969" name="calyx" name_original="calyx" src="d0_s11" type="structure">
        <character is_modifier="true" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o12970" name="limb" name_original="limb" src="d0_s11" type="structure">
        <character is_modifier="false" name="shape" src="d0_s11" value="2-lobed" value_original="2-lobed" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s11" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o12971" name="appendage" name_original="appendages" src="d0_s11" type="structure">
        <character name="some_measurement" src="d0_s11" unit="mm" value="1" value_original="1" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens equaling calyx;</text>
      <biological_entity id="o12972" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o12973" name="stamen" name_original="stamens" src="d0_s12" type="structure" />
      <biological_entity id="o12974" name="calyx" name_original="calyx" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>styles 3 (–4), equaling calyx.</text>
      <biological_entity id="o12975" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o12976" name="style" name_original="styles" src="d0_s13" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s13" to="4" />
        <character name="quantity" src="d0_s13" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o12977" name="calyx" name_original="calyx" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Capsules equaling calyx, opening by 6 (or 8) teeth;</text>
      <biological_entity id="o12978" name="capsule" name_original="capsules" src="d0_s14" type="structure" />
      <biological_entity id="o12979" name="calyx" name_original="calyx" src="d0_s14" type="structure">
        <character is_modifier="true" name="variability" src="d0_s14" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o12980" name="tooth" name_original="teeth" src="d0_s14" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s14" value="6" value_original="6" />
      </biological_entity>
      <relation from="o12978" id="r1415" name="opening by" negation="false" src="d0_s14" to="o12980" />
    </statement>
    <statement id="d0_s15">
      <text>carpophore 2.5–3.5 mm.</text>
      <biological_entity id="o12981" name="carpophore" name_original="carpophore" src="d0_s15" type="structure">
        <character char_type="range_value" from="2.5" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Seeds brown, broadly winged, reniform, 1–2 mm, rugose-tessellate.</text>
    </statement>
    <statement id="d0_s17">
      <text>2n = 48.</text>
      <biological_entity id="o12982" name="seed" name_original="seeds" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="brown" value_original="brown" />
        <character is_modifier="false" modifier="broadly" name="architecture" src="d0_s16" value="winged" value_original="winged" />
        <character is_modifier="false" name="shape" src="d0_s16" value="reniform" value_original="reniform" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="2" to_unit="mm" />
        <character is_modifier="false" name="coloration_or_relief" src="d0_s16" value="rugose-tessellate" value_original="rugose-tessellate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12983" name="chromosome" name_original="" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="summer" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Alpine ridges, gravel slopes, talus</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="alpine ridges" />
        <character name="habitat" value="gravel slopes" />
        <character name="habitat" value="talus" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1600-3000 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="3000" to_unit="m" from="1600" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif., Oreg., Wash.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>62.</number>
  <other_name type="common_name">Suksdorf’s catchfly</other_name>
  <discussion>Silene suksdorfii appears to be closely related to S. parryi but differs in its broadly winged seeds, smaller size, cespitose habit, and the prominent purple-septate hairs of the calyx, although the latter occasionally are present in S. parryi. It is very similar to, and in Idaho appears to intergrade with, another alpine species, S. sargentii, which has linear leaves and lacks the purple septa in the hairs and the broad wing on the seeds. It is similar also to S. hitchguirei; see discussion under that species.</discussion>
  
</bio:treatment>