<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">18</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Tanfani in F. Parlatore" date="unknown" rank="subfamily">Polycarpoideae</taxon_name>
    <taxon_name authority="(Persoon) J. Presl &amp; C. Presl" date="unknown" rank="genus">spergularia</taxon_name>
    <taxon_name authority="(Hornemann ex Chamisso &amp; Schlechtendal) Heynhold" date="1846" rank="species">macrotheca</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">macrotheca</taxon_name>
    <taxon_hierarchy>family caryophyllaceae;subfamily polycarpoideae;genus spergularia;species macrotheca;variety macrotheca;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060918</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 5–35 cm.</text>
      <biological_entity id="o29701" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="5" from_unit="cm" name="some_measurement" src="d0_s0" to="35" to_unit="cm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems erect to prostrate.</text>
      <biological_entity id="o29702" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s1" to="prostrate" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves: stipules 5–11 mm;</text>
      <biological_entity id="o29703" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o29704" name="stipule" name_original="stipules" src="d0_s2" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s2" to="11" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade (0.6–) 1–4 cm.</text>
      <biological_entity id="o29705" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o29706" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="0.6" from_unit="cm" name="atypical_some_measurement" src="d0_s3" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="4" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Flowers: sepal lobes ovate to lanceolate, (4.5–) 5–7 mm, to 8 mm in fruit;</text>
      <biological_entity id="o29707" name="flower" name_original="flowers" src="d0_s4" type="structure" />
      <biological_entity constraint="sepal" id="o29708" name="lobe" name_original="lobes" src="d0_s4" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s4" to="lanceolate" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="atypical_some_measurement" src="d0_s4" to="5" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character char_type="range_value" constraint="in fruit" constraintid="o29709" from="0" from_unit="mm" name="some_measurement" src="d0_s4" to="8" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29709" name="fruit" name_original="fruit" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>petals pink to rosy, 0.9–1.1 times as long as sepals;</text>
      <biological_entity id="o29710" name="flower" name_original="flowers" src="d0_s5" type="structure" />
      <biological_entity id="o29711" name="petal" name_original="petals" src="d0_s5" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s5" to="rosy" />
        <character constraint="sepal" constraintid="o29712" is_modifier="false" name="length" src="d0_s5" value="0.9-1.1 times as long as sepals" />
      </biological_entity>
      <biological_entity id="o29712" name="sepal" name_original="sepals" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>styles 0.6–1.2 mm.</text>
      <biological_entity id="o29713" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o29714" name="style" name_original="styles" src="d0_s6" type="structure">
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s6" to="1.2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Capsules 0.8–1.2 times as long as calyx.</text>
      <biological_entity id="o29715" name="capsule" name_original="capsules" src="d0_s7" type="structure">
        <character constraint="calyx" constraintid="o29716" is_modifier="false" name="length" src="d0_s7" value="0.8-1.2 times as long as calyx" />
      </biological_entity>
      <biological_entity id="o29716" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Seeds redbrown, often with submarginal groove or depression, suborbiculate to ovate, (0.6–) 0.7–0.8 mm, smooth (40×);</text>
      <biological_entity id="o29717" name="seed" name_original="seeds" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="redbrown" value_original="redbrown" />
        <character char_type="range_value" from="suborbiculate" name="shape" notes="" src="d0_s8" to="ovate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="atypical_some_measurement" src="d0_s8" to="0.7" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s8" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="architecture_or_pubescence_or_relief" src="d0_s8" value="smooth" value_original="smooth" />
        <character name="quantity" src="d0_s8" value="[40" value_original="[40" />
      </biological_entity>
      <biological_entity constraint="submarginal" id="o29718" name="groove" name_original="groove" src="d0_s8" type="structure" />
      <biological_entity id="o29719" name="depression" name_original="depression" src="d0_s8" type="structure" />
      <relation from="o29717" id="r3340" modifier="often" name="with" negation="false" src="d0_s8" to="o29718" />
      <relation from="o29717" id="r3341" modifier="often" name="with" negation="false" src="d0_s8" to="o29719" />
    </statement>
    <statement id="d0_s9">
      <text>wing absent or 0.1–0.2 mm wide.</text>
    </statement>
    <statement id="d0_s10">
      <text>2n = 36, 72.</text>
      <biological_entity id="o29720" name="wing" name_original="wing" src="d0_s9" type="structure">
        <character is_modifier="false" name="presence" src="d0_s9" value="absent" value_original="absent" />
        <character name="quantity" src="d0_s9" value="0.1-0.2 mm" value_original="0.1-0.2 mm" />
        <character is_modifier="false" name="width" src="d0_s9" value="wide" value_original="wide" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29721" name="chromosome" name_original="" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="36" value_original="36" />
        <character name="quantity" src="d0_s10" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring–fall.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="fall" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Salt flats and marshes, dunes, rocky outcrops, sandy or rocky coastal bluffs, gravelly ridges, alkaline fields</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="salt flats" />
        <character name="habitat" value="marshes" />
        <character name="habitat" value="dunes" />
        <character name="habitat" value="rocky outcrops" />
        <character name="habitat" value="sandy" />
        <character name="habitat" value="rocky coastal bluffs" />
        <character name="habitat" value="gravelly ridges" />
        <character name="habitat" value="alkaline fields" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>less than 300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="300" to_unit="m" from="300" from_unit="m" constraint="less than " />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>B.C.; Calif., Oreg., Wash.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="B.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Oreg." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Wash." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>1a.</number>
  
</bio:treatment>