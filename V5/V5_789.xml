<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">386</other_info_on_meta>
    <other_info_on_meta type="mention_page">223</other_info_on_meta>
    <other_info_on_meta type="mention_page">381</other_info_on_meta>
    <other_info_on_meta type="mention_page">383</other_info_on_meta>
    <other_info_on_meta type="mention_page">387</other_info_on_meta>
    <other_info_on_meta type="mention_page">433</other_info_on_meta>
    <other_info_on_meta type="illustration_page">379</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="Torrey &amp; Frémont in J. C. Frémont" date="1845" rank="species">inflatum</taxon_name>
    <place_of_publication>
      <publication_title>in J. C. Frémont, Rep. Exped. Rocky Mts.,</publication_title>
      <place_in_publication>317. 1845</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species inflatum;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250060340</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Small" date="unknown" rank="species">glaucum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species glaucum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Torrey" date="unknown" rank="species">inflatum</taxon_name>
    <taxon_name authority="I. M. Johnston" date="unknown" rank="variety">deflatum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species inflatum;variety deflatum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">trichopes</taxon_name>
    <taxon_name authority="(Small) S. Stokes" date="unknown" rank="subspecies">glaucum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species trichopes;subspecies glaucum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect, perennial, occasionally flowering first-year, 1–10 (–15) dm, glabrous, usually glaucous, grayish.</text>
      <biological_entity id="o12180" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" modifier="occasionally" name="life_cycle" src="d0_s0" value="flowering" value_original="flowering" />
        <character is_modifier="false" name="structure_subtype" src="d0_s0" value="first-year" value_original="first-year" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="15" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="10" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s0" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="grayish" value_original="grayish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex compact;</text>
      <biological_entity id="o12181" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o12182" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, solid or hollow and fistulose, (0.2–) 2–5 dm, glabrous, usually glaucous, occasionally hirsute proximally.</text>
      <biological_entity id="o12183" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o12184" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="hollow" value_original="hollow" />
        <character is_modifier="false" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.2" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="2" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="2" from_unit="dm" name="some_measurement" src="d0_s2" to="5" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s2" value="glaucous" value_original="glaucous" />
        <character is_modifier="false" modifier="occasionally; proximally" name="pubescence" src="d0_s2" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal;</text>
      <biological_entity id="o12185" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>petiole 2–6 cm, hirsute;</text>
      <biological_entity id="o12186" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="hirsute" value_original="hirsute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade oblong-ovate to oblong or rounded to reniform, (0.5–) 1–2.5 (–3) × (0.5–) 1–2 (–2.5) cm, short-hirsute and grayish or greenish on both surfaces, sometimes less so or glabrous and green adaxially, margins occasionally undulate.</text>
      <biological_entity id="o12187" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblong-ovate" name="shape" src="d0_s5" to="oblong or rounded" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_length" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="atypical_width" src="d0_s5" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s5" to="2.5" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="short-hirsute" value_original="short-hirsute" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="grayish" value_original="grayish" />
        <character constraint="on surfaces" constraintid="o12188" is_modifier="false" name="coloration" src="d0_s5" value="greenish" value_original="greenish" />
        <character is_modifier="false" modifier="sometimes less; less" name="pubescence" notes="" src="d0_s5" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s5" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o12188" name="surface" name_original="surfaces" src="d0_s5" type="structure" />
      <biological_entity id="o12189" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s5" value="undulate" value_original="undulate" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences cymose, open, spreading to erect, 5–70 × 5–50 cm;</text>
      <biological_entity id="o12190" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="cymose" value_original="cymose" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s6" to="erect" />
        <character char_type="range_value" from="5" from_unit="cm" name="length" src="d0_s6" to="70" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches occasionally fistulose, glabrous, usually glaucous;</text>
      <biological_entity id="o12191" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="occasionally" name="shape" src="d0_s7" value="fistulose" value_original="fistulose" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="pubescence" src="d0_s7" value="glaucous" value_original="glaucous" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, scalelike, 1–2.5 (–5) × 1–2.5 mm.</text>
      <biological_entity id="o12192" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s8" to="2.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles erect, straight, filiform to capillary, 0.5–2 (–3.5) cm, glabrous.</text>
      <biological_entity id="o12193" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character char_type="range_value" from="filiform" name="shape" src="d0_s9" to="capillary" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s9" to="3.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s9" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres turbinate, 1–1.5 × 1–1.8 mm, glabrous;</text>
      <biological_entity id="o12194" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s10" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s10" to="1.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 5, erect, 0.4–0.6 mm.</text>
      <biological_entity id="o12195" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s11" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers (1–) 2–3 (–4) mm;</text>
      <biological_entity id="o12196" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="2" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s12" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth yellow with greenish or reddish midribs, densely hirsute with coarse curved hairs;</text>
      <biological_entity id="o12197" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character constraint="with midribs" constraintid="o12198" is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character constraint="with hairs" constraintid="o12199" is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s13" value="hirsute" value_original="hirsute" />
      </biological_entity>
      <biological_entity id="o12198" name="midrib" name_original="midribs" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o12199" name="hair" name_original="hairs" src="d0_s13" type="structure">
        <character is_modifier="true" name="relief" src="d0_s13" value="coarse" value_original="coarse" />
        <character is_modifier="true" name="course" src="d0_s13" value="curved" value_original="curved" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>tepals monomorphic, narrowly ovoid to ovate;</text>
      <biological_entity id="o12200" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="narrowly ovoid" name="shape" src="d0_s14" to="ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens exserted, 1.3–2.5 mm;</text>
      <biological_entity id="o12201" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="1.3" from_unit="mm" name="some_measurement" src="d0_s15" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments glabrous or sparsely pubescent proximally.</text>
      <biological_entity id="o12202" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely; proximally" name="pubescence" src="d0_s16" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes light-brown to brown, lenticular to 3-gonous, 2–2.5 mm, glabrous.</text>
    </statement>
    <statement id="d0_s18">
      <text>2n = 32.</text>
      <biological_entity id="o12203" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character char_type="range_value" from="light-brown" name="coloration" src="d0_s17" to="brown" />
        <character char_type="range_value" from="lenticular" name="shape" src="d0_s17" to="3-gonous" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s17" to="2.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity constraint="2n" id="o12204" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="32" value_original="32" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering year-round.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="" from="" constraint=" year round" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly washes, flats, and slopes, mixed grassland, saltbush, creosote bush, mesquite, and sagebrush communities, pinyon and/or juniper woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly washes" />
        <character name="habitat" value="flats" />
        <character name="habitat" value="slopes" modifier="and" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="creosote bush" />
        <character name="habitat" value="mesquite" />
        <character name="habitat" value="sagebrush communities" modifier="and" />
        <character name="habitat" value="pinyon and/or juniper woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>-30-1800(-2000) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1800" to_unit="m" from="30" from_unit="m" constraint="- " />
        <character name="elevation" char_type="atypical_range" to="2000" to_unit="m" from="30" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ariz., Calif., Colo., Nev., N.Mex., Utah; Mexico (Baja California, Sonora).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ariz." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Colo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Nev." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Utah" establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
        <character name="distribution" value="Mexico (Sonora)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>147.</number>
  <other_name type="common_name">Desert trumpet</other_name>
  <other_name type="common_name">Indian pipeweed</other_name>
  <other_name type="common_name">bottle stopper</other_name>
  <discussion>The cause of the fistulose stem and inflorescence branches in Eriogonum inflatum was imaginatively attributed by A. M. Stone and C. T. Mason (1979) to the larvae of gall insects. This fallacy continues to appear in the literature. Greenhouse studies have shown that stems of this and some other species of the genus inflate without the presence of any insects. Other researchers have shown that the inflation involves a build-up of CO2 within the stems, which take over as the primary photosynthetic body as leaves wilt or eventually dry up and fall away from the plant (C. D. Osmond et al. 1987). Not all individuals of E. inflatum will have fistulose stems and branches, as this feature is partly a function of available moisture: the drier the conditions, the less pronounced the inflation. Stems produced in the summer tend to be inflated less frequently than those produced in the spring.</discussion>
  <discussion>The “annual” phase of Eriogonum inflatum is distinct from its truly annual relatives. Its flowering stems and inflorescence branches are distinctly grayish, whereas those of the true annuals are green or yellowish green.</discussion>
  <discussion>As circumscribed here, Eriogonum inflatum occurs in Arizona, southern and east-central California, western Colorado, northwestern New Mexico, central and southern Nevada, and southern and eastern Utah.</discussion>
  <discussion>Some Native Americans occasionally ate newly emerged stems of Eriogonum inflatum (S. A. Weber and P. D. Seaman 1985; M. L. Zigmond 1981). The hollow stems were used as drinking tubes (Weber and Seaman) and pipes (E. W. Gifford 1936). This wild buckwheat is a food plant for the desert metalmark butterfly (Apodemia mormo deserti).</discussion>
  
</bio:treatment>