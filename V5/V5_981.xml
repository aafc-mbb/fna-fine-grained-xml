<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">467</other_info_on_meta>
    <other_info_on_meta type="mention_page">447</other_info_on_meta>
    <other_info_on_meta type="mention_page">466</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="Reveal &amp; Hardham" date="1989" rank="subgenus">Amphietes</taxon_name>
    <taxon_name authority="(Torrey) Torrey &amp; A. Gray" date="1870" rank="section">Acanthogonum</taxon_name>
    <taxon_name authority="Parry" date="1884" rank="species">orcuttiana</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Davenport Acad. Nat. Sci.</publication_title>
      <place_in_publication>4: 54. 1884</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus amphietes;section acanthogonum;species orcuttiana;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060080</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants prostrate, 0.1–0.5 × 0.3–2 (–2.5) dm, villous.</text>
      <biological_entity id="o18310" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="prostrate" value_original="prostrate" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="length" src="d0_s0" to="0.5" to_unit="dm" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="2.5" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="width" src="d0_s0" to="2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="villous" value_original="villous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o18311" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1–2 cm;</text>
      <biological_entity id="o18312" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade narrowly oblanceolate, 0.5–1.5 × 0.2–0.35 (–0.5) cm, thinly pubescent.</text>
      <biological_entity id="o18313" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s3" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.35" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="0.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="0.35" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences with involucres in small clusters 0.5–1 cm diam., greenish;</text>
      <biological_entity id="o18314" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character constraint="in small clusters 0.5-1 cm" is_modifier="false" name="diam" notes="" src="d0_s4" value="greenish" value_original="greenish" />
      </biological_entity>
      <biological_entity id="o18315" name="involucre" name_original="involucres" src="d0_s4" type="structure" />
      <relation from="o18314" id="r2054" name="with" negation="false" src="d0_s4" to="o18315" />
    </statement>
    <statement id="d0_s5">
      <text>bracts 2, sessile, unequal, 1 laminar and oblanceolate, 0.3–1 cm × 1–3 mm, awnless, this opposite linear, acicular, greatly reduced, 0.1–0.2 cm × 0.3–0.6 mm bract terminated by short, straight awn 0.6–1 mm.</text>
      <biological_entity id="o18316" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="size" src="d0_s5" value="unequal" value_original="unequal" />
        <character name="quantity" src="d0_s5" value="1" value_original="1" />
        <character is_modifier="false" name="position" src="d0_s5" value="laminar" value_original="laminar" />
        <character is_modifier="false" name="shape" src="d0_s5" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s5" to="1" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" name="architecture" src="d0_s5" value="awnless" value_original="awnless" />
        <character is_modifier="false" name="arrangement" src="d0_s5" value="opposite" value_original="opposite" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acicular" value_original="acicular" />
        <character is_modifier="false" modifier="greatly" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="length" src="d0_s5" to="0.2" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s5" to="0.6" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o18317" name="bract" name_original="bract" src="d0_s5" type="structure" />
      <biological_entity id="o18318" name="awn" name_original="awn" src="d0_s5" type="structure">
        <character is_modifier="true" name="height_or_length_or_size" src="d0_s5" value="short" value_original="short" />
        <character is_modifier="true" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o18317" id="r2055" name="terminated by" negation="false" src="d0_s5" to="o18318" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres 1, greenish, campanulate, 3-ribbed, 0.8–2 mm, faintly corrugate, pubescent;</text>
      <biological_entity id="o18319" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" src="d0_s6" value="greenish" value_original="greenish" />
        <character is_modifier="false" name="shape" src="d0_s6" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="3-ribbed" value_original="3-ribbed" />
        <character char_type="range_value" from="0.8" from_unit="mm" name="some_measurement" src="d0_s6" to="2" to_unit="mm" />
        <character is_modifier="false" modifier="faintly" name="arrangement_or_relief" src="d0_s6" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>teeth 3, equal, 1.8–2 mm;</text>
      <biological_entity id="o18320" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character name="quantity" src="d0_s7" value="3" value_original="3" />
        <character is_modifier="false" name="variability" src="d0_s7" value="equal" value_original="equal" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>awns uncinate, 0.6–1 mm.</text>
      <biological_entity id="o18321" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="uncinate" value_original="uncinate" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="some_measurement" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Flowers 1, included to slightly exserted;</text>
      <biological_entity id="o18322" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="1" value_original="1" />
        <character char_type="range_value" from="included" name="position" src="d0_s9" to="slightly exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth yellow, cylindric, 1.5–1.8 mm, densely pubescent abaxially;</text>
      <biological_entity id="o18323" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="some_measurement" src="d0_s10" to="1.8" to_unit="mm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals connate ca. 1/2 their length, monomorphic, narrowly oblanceolate, obtuse to truncate, entire apically, slightly spreading;</text>
      <biological_entity id="o18324" name="tepal" name_original="tepals" src="d0_s11" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character name="length" src="d0_s11" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="architecture" src="d0_s11" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s11" value="oblanceolate" value_original="oblanceolate" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obtuse to truncate" value_original="obtuse to truncate" />
        <character is_modifier="false" modifier="apically" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s11" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>stamens 9, slightly exserted;</text>
      <biological_entity id="o18325" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="9" value_original="9" />
        <character is_modifier="false" modifier="slightly" name="position" src="d0_s12" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, 0.5–0.8 mm, glabrous;</text>
      <biological_entity id="o18326" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.8" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers reddish, ovate, 0.2–0.3 mm.</text>
      <biological_entity id="o18327" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="shape" src="d0_s14" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s14" to="0.3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes dark-brown, lenticular, 2–2.2 mm. 2n = (76, 78), 80, (84).</text>
      <biological_entity id="o18328" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s15" to="2.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o18329" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="[76" value_original="[76" />
        <character name="quantity" src="d0_s15" value="78]" value_original="78]" />
        <character name="quantity" src="d0_s15" value="80" value_original="80" />
        <character name="atypical_quantity" src="d0_s15" value="84" value_original="84" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–May.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="May" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy soil, mesas and hills near coast, coastal scrub communities</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy soil" />
        <character name="habitat" value="mesas" constraint="near coast" />
        <character name="habitat" value="hills" constraint="near coast" />
        <character name="habitat" value="coast" />
        <character name="habitat" value="coastal scrub" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>60-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="60" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>30.</number>
  <other_name type="common_name">Orcutt spineflower</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Chorizanthe orcuttiana is known from a few populations on coastal mesas and hills near San Diego, San Diego County. It is federally listed as endangered. The species is an octoploid that may well have resulted from an ancient hybridization and doubling of chromosomes involving C. procumbens and C. polygonoides var. longispina. The Orcutt spineflower grows in soft, white sand; C. procumbens and C. polygonoides var. longispina are restricted to gravelly sites.</discussion>
  
</bio:treatment>