<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Craig C. Freeman,Harold R. Hinds†</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">594</other_info_on_meta>
    <other_info_on_meta type="mention_page">479</other_info_on_meta>
    <other_info_on_meta type="mention_page">480</other_info_on_meta>
    <other_info_on_meta type="mention_page">574</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="(Linnaeus) Scopoli" date="1754" rank="genus">BISTORTA</taxon_name>
    <place_of_publication>
      <publication_title>Meth. Pl.,</publication_title>
      <place_in_publication>24. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus bistorta;</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin, bi -, twice, and tortus, twisted, alluding to the rhizomes of some species</other_info_on_name>
    <other_info_on_name type="fna_id">104006</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Polygonum</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="unranked">Bistorta</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 360. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Polygonum;unranked Bistorta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, perennial;</text>
      <biological_entity id="o29722" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>roots fibrous, rhizomatous.</text>
      <biological_entity id="o29723" name="root" name_original="roots" src="d0_s1" type="structure">
        <character is_modifier="false" name="texture" src="d0_s1" value="fibrous" value_original="fibrous" />
        <character is_modifier="false" name="architecture" src="d0_s1" value="rhizomatous" value_original="rhizomatous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems erect, simple, glabrous.</text>
      <biological_entity id="o29724" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves mostly basal, some cauline, alternate, petiolate or sessile;</text>
      <biological_entity id="o29725" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="arrangement" notes="" src="d0_s3" value="alternate" value_original="alternate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="petiolate" value_original="petiolate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o29726" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="true" modifier="mostly" name="position" src="d0_s3" value="basal" value_original="basal" />
      </biological_entity>
      <biological_entity constraint="cauline" id="o29727" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>ocrea persistent or disintegrating with age and deciduous entirely or distally, chartaceous;</text>
      <biological_entity id="o29728" name="ocreum" name_original="ocrea" src="d0_s4" type="structure">
        <character is_modifier="false" name="duration" src="d0_s4" value="persistent" value_original="persistent" />
        <character constraint="with age" is_modifier="false" name="dehiscence" src="d0_s4" value="disintegrating" value_original="disintegrating" />
        <character is_modifier="false" modifier="entirely; entirely; distally" name="duration" src="d0_s4" value="deciduous" value_original="deciduous" />
        <character is_modifier="false" name="pubescence_or_texture" src="d0_s4" value="chartaceous" value_original="chartaceous" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>blade linear or lanceolate to elliptic, oblong-ovate, or ovate, margins entire or obscurely and irregularly repand.</text>
      <biological_entity id="o29729" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="elliptic oblong-ovate or ovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="elliptic oblong-ovate or ovate" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s5" to="elliptic oblong-ovate or ovate" />
      </biological_entity>
      <biological_entity id="o29730" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s5" value="entire" value_original="entire" />
        <character name="architecture_or_shape" src="d0_s5" value="obscurely" value_original="obscurely" />
        <character is_modifier="false" modifier="irregularly" name="shape" src="d0_s5" value="repand" value_original="repand" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences terminal, spikelike.</text>
      <biological_entity id="o29731" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s6" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="shape" src="d0_s6" value="spikelike" value_original="spikelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels present.</text>
      <biological_entity id="o29732" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers bisexual, 1–2 per ocreate fascicle, base not stipelike;</text>
      <biological_entity id="o29733" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="reproduction" src="d0_s8" value="bisexual" value_original="bisexual" />
        <character char_type="range_value" constraint="per fascicle" constraintid="o29734" from="1" name="quantity" src="d0_s8" to="2" />
      </biological_entity>
      <biological_entity id="o29734" name="fascicle" name_original="fascicle" src="d0_s8" type="structure" />
      <biological_entity id="o29735" name="base" name_original="base" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s8" value="stipelike" value_original="stipelike" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth nonaccrescent, white, greenish white, pink, or purplish-pink, rarely red, campanulate, glabrous;</text>
      <biological_entity id="o29736" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character is_modifier="false" name="size" src="d0_s9" value="nonaccrescent" value_original="nonaccrescent" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="greenish white" value_original="greenish white" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish-pink" value_original="purplish-pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s9" value="purplish-pink" value_original="purplish-pink" />
        <character is_modifier="false" modifier="rarely" name="coloration" src="d0_s9" value="red" value_original="red" />
        <character is_modifier="false" name="shape" src="d0_s9" value="campanulate" value_original="campanulate" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>tepals 5, connate proximally ca. 1/5 their length, petaloid, monomorphic or slightly dimorphic, outer larger than inner;</text>
      <biological_entity id="o29737" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character name="length" src="d0_s10" value="1/5" value_original="1/5" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="petaloid" value_original="petaloid" />
        <character is_modifier="false" name="architecture" src="d0_s10" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="slightly" name="growth_form" src="d0_s10" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <biological_entity constraint="outer" id="o29738" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character constraint="than inner tepals" constraintid="o29739" is_modifier="false" name="size" src="d0_s10" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity constraint="inner" id="o29739" name="tepal" name_original="tepals" src="d0_s10" type="structure" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 5–8, sometimes poorly developed;</text>
      <biological_entity id="o29740" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character char_type="range_value" from="5" name="quantity" src="d0_s11" to="8" />
        <character is_modifier="false" modifier="sometimes poorly" name="development" src="d0_s11" value="developed" value_original="developed" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments distinct or connate basally, outer ones sometimes adnate to perianth-tube, glabrous;</text>
      <biological_entity id="o29741" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="basally" name="fusion" src="d0_s12" value="connate" value_original="connate" />
        <character is_modifier="false" name="position" src="d0_s12" value="outer" value_original="outer" />
        <character constraint="to perianth-tube" constraintid="o29742" is_modifier="false" modifier="sometimes" name="fusion" src="d0_s12" value="adnate" value_original="adnate" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o29742" name="perianth-tube" name_original="perianth-tube" src="d0_s12" type="structure" />
    </statement>
    <statement id="d0_s13">
      <text>anthers yellow, pink, red, purple, or blackish, ovate to elliptic;</text>
      <biological_entity id="o29743" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s13" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="pink" value_original="pink" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="red" value_original="red" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="blackish" value_original="blackish" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="purple" value_original="purple" />
        <character is_modifier="false" name="coloration" src="d0_s13" value="blackish" value_original="blackish" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s13" to="elliptic" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>styles 3, erect or spreading, distinct or connate proximally;</text>
      <biological_entity id="o29744" name="style" name_original="styles" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="3" value_original="3" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="erect" value_original="erect" />
        <character is_modifier="false" name="orientation" src="d0_s14" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s14" value="connate" value_original="connate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stigmas 2–3, capitate.</text>
      <biological_entity id="o29745" name="stigma" name_original="stigmas" src="d0_s15" type="structure">
        <character char_type="range_value" from="2" name="quantity" src="d0_s15" to="3" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s15" value="capitate" value_original="capitate" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes included or exserted, brown to dark-brown, unwinged, 3-gonous, glabrous.</text>
      <biological_entity id="o29746" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="position" src="d0_s16" value="included" value_original="included" />
        <character is_modifier="false" name="position" src="d0_s16" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="brown" name="coloration" src="d0_s16" to="dark-brown" />
        <character is_modifier="false" name="architecture" src="d0_s16" value="unwinged" value_original="unwinged" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-gonous" value_original="3-gonous" />
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Seeds: embryo curved.</text>
      <biological_entity id="o29747" name="seed" name_original="seeds" src="d0_s17" type="structure" />
    </statement>
    <statement id="d0_s18">
      <text>x = 11, 12.</text>
      <biological_entity id="o29748" name="embryo" name_original="embryo" src="d0_s17" type="structure">
        <character is_modifier="false" name="course" src="d0_s17" value="curved" value_original="curved" />
      </biological_entity>
      <biological_entity constraint="x" id="o29749" name="chromosome" name_original="" src="d0_s18" type="structure">
        <character name="quantity" src="d0_s18" value="11" value_original="11" />
        <character name="quantity" src="d0_s18" value="12" value_original="12" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Arctic and temperate North America, Europe, Asia.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="Arctic and temperate North America" establishment_means="native" />
        <character name="distribution" value="Europe" establishment_means="native" />
        <character name="distribution" value="Asia" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>33.</number>
  <other_name type="common_name">Bistort</other_name>
  <discussion>Species ca. 50 (4 in the flora).</discussion>
  <discussion>Bistorta often is included in Polygonum in the broad sense or in Persicaria. It is accepted here as a distinct genus based on habit, morphology, and anatomy (K. Haraldson 1978; L.-P. Ronse Decraene and J. R. Akeroyd 1988). In the species of the flora area, the base of the petiole forms a long, tubular sheath distal to the node from which the leaf arises and proximal to the point of divergence of the petiole. Distal to the sheath is the ocrea, which usually is darker and thinner.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences narrowly elongate-cylindric, (15-)20-90 × 4-10 mm, usually bearing pyriform, pink to brown or purple bulblets proximally</description>
      <determination>1 Bistorta vivipara</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences short-cylindric to ovoid, 10-70 × 8-25 mm, bulblets absent</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade bases abruptly contracted, truncate to cuneate; petioles prominently winged distally; perianths pink</description>
      <determination>4 Bistorta officinalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade bases usually tapered to rounded, rarely abruptly truncate or cuneate; petioles wingless or rarely winged distally; perianths white to pale pink, bright pink, or purplish pink</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perianths bright pink or purplish pink; basal leaf blades with apices rounded to acute; plants (8-)10-40(-50) cm; n Canada, Alaska</description>
      <determination>3 Bistorta plumosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perianths white or pale pink; basal leaf blades with apices usually acute to acuminate, rarely obtuse; plants (10-)20-70(-75) cm; sw Canada, w United States</description>
      <determination>2 Bistorta bistortoides</determination>
    </key_statement>
  </key>
</bio:treatment>