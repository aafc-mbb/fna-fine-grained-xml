<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">79</other_info_on_meta>
    <other_info_on_meta type="mention_page">80</other_info_on_meta>
    <other_info_on_meta type="mention_page">91</other_info_on_meta>
    <other_info_on_meta type="mention_page">92</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">cerastium</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">arvense</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">arvense</taxon_name>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus cerastium;species arvense;subspecies arvense;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060039</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants straggling and creeping, strongly rhizomatous, not forming clumps, without taproot.</text>
      <biological_entity id="o6638" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="straggling" value_original="straggling" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s0" value="creeping" value_original="creeping" />
        <character is_modifier="false" modifier="strongly" name="architecture" src="d0_s0" value="rhizomatous" value_original="rhizomatous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o6639" name="clump" name_original="clumps" src="d0_s0" type="structure" />
      <biological_entity id="o6640" name="taproot" name_original="taproot" src="d0_s0" type="structure" />
      <relation from="o6638" id="r721" name="forming" negation="true" src="d0_s0" to="o6639" />
      <relation from="o6638" id="r722" name="without" negation="true" src="d0_s0" to="o6640" />
    </statement>
    <statement id="d0_s1">
      <text>Stems: flowering-stems ascending, proximal 1/2 often purple-tinged, usually 25–30 cm, softly pubescent to subglabrous, glandular-hairs confined to inflorescences;</text>
      <biological_entity id="o6641" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o6642" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="position" src="d0_s1" value="proximal" value_original="proximal" />
        <character name="quantity" src="d0_s1" value="1/2" value_original="1/2" />
        <character is_modifier="false" modifier="often" name="coloration" src="d0_s1" value="purple-tinged" value_original="purple-tinged" />
        <character char_type="range_value" from="25" from_unit="cm" modifier="usually" name="some_measurement" src="d0_s1" to="30" to_unit="cm" />
        <character char_type="range_value" from="softly pubescent" name="pubescence" src="d0_s1" to="subglabrous" />
      </biological_entity>
      <biological_entity id="o6643" name="glandular-hair" name_original="glandular-hairs" src="d0_s1" type="structure" />
      <biological_entity id="o6644" name="inflorescence" name_original="inflorescences" src="d0_s1" type="structure" />
      <relation from="o6643" id="r723" name="confined to" negation="false" src="d0_s1" to="o6644" />
    </statement>
    <statement id="d0_s2">
      <text>nonflowering shoots well developed, producing obovate to oblanceolate, spatulate, overwintering leaves;</text>
      <biological_entity id="o6645" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity constraint="nonflowering" id="o6646" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="well" name="development" src="d0_s2" value="developed" value_original="developed" />
      </biological_entity>
      <biological_entity id="o6647" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character char_type="range_value" from="obovate" is_modifier="true" name="shape" src="d0_s2" to="oblanceolate" />
        <character is_modifier="true" name="shape" src="d0_s2" value="spatulate" value_original="spatulate" />
        <character is_modifier="true" name="duration" src="d0_s2" value="overwintering" value_original="overwintering" />
      </biological_entity>
      <relation from="o6646" id="r724" name="producing" negation="false" src="d0_s2" to="o6647" />
    </statement>
    <statement id="d0_s3">
      <text>small axillary tufts of leaves well developed, conspicuous.</text>
      <biological_entity id="o6648" name="stem" name_original="stems" src="d0_s3" type="structure" />
      <biological_entity constraint="axillary" id="o6649" name="tuft" name_original="tufts" src="d0_s3" type="structure">
        <character is_modifier="true" name="size" src="d0_s3" value="small" value_original="small" />
        <character is_modifier="false" modifier="well" name="development" src="d0_s3" value="developed" value_original="developed" />
        <character is_modifier="false" name="prominence" src="d0_s3" value="conspicuous" value_original="conspicuous" />
      </biological_entity>
      <biological_entity id="o6650" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <relation from="o6649" id="r725" name="part_of" negation="false" src="d0_s3" to="o6650" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves on flowering shoots often dimorphic;</text>
      <biological_entity id="o6651" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o6652" name="shoot" name_original="shoots" src="d0_s4" type="structure">
        <character is_modifier="true" name="life_cycle" src="d0_s4" value="flowering" value_original="flowering" />
        <character is_modifier="false" modifier="often" name="growth_form" src="d0_s4" value="dimorphic" value_original="dimorphic" />
      </biological_entity>
      <relation from="o6651" id="r726" name="on" negation="false" src="d0_s4" to="o6652" />
    </statement>
    <statement id="d0_s5">
      <text>midstem leaves larger, blade lanceolate, 12–22 × 2.5–5 mm;</text>
      <biological_entity constraint="midstem" id="o6653" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="larger" value_original="larger" />
      </biological_entity>
      <biological_entity id="o6654" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="12" from_unit="mm" name="length" src="d0_s5" to="22" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>proximal leaves oblong to linear, 5–15 × 0.5–2 mm. flowers: sepals 5–7 mm, with midrib, petals 10–12.5 mm, often turning brown when dried;</text>
      <biological_entity constraint="proximal" id="o6655" name="leaf" name_original="leaves" src="d0_s6" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s6" to="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s6" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s6" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6656" name="flower" name_original="flowers" src="d0_s6" type="structure" />
      <biological_entity id="o6657" name="sepal" name_original="sepals" src="d0_s6" type="structure">
        <character char_type="range_value" from="5" from_unit="mm" name="some_measurement" src="d0_s6" to="7" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6658" name="midrib" name_original="midrib" src="d0_s6" type="structure" />
      <biological_entity id="o6659" name="petal" name_original="petals" src="d0_s6" type="structure">
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" src="d0_s6" to="12.5" to_unit="mm" />
        <character is_modifier="false" modifier="when dried" name="coloration" src="d0_s6" value="brown" value_original="brown" />
      </biological_entity>
      <relation from="o6657" id="r727" name="with" negation="false" src="d0_s6" to="o6658" />
    </statement>
    <statement id="d0_s7">
      <text>anthers 1–1.1 mm.</text>
      <biological_entity constraint="proximal" id="o6660" name="leaf" name_original="leaves" src="d0_s7" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s7" to="linear" />
        <character char_type="range_value" from="5" from_unit="mm" name="length" src="d0_s7" to="15" to_unit="mm" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o6661" name="flower" name_original="flowers" src="d0_s7" type="structure" />
      <biological_entity id="o6662" name="anther" name_original="anthers" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="1.1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Capsules (8.5–) 9.8 (–11.5) × 3–4 mm, ca. 1.5 (–2) times as long as sepals.</text>
      <biological_entity id="o6663" name="capsule" name_original="capsules" src="d0_s8" type="structure">
        <character char_type="range_value" from="8.5" from_unit="mm" name="atypical_length" src="d0_s8" to="9.8" to_inclusive="false" to_unit="mm" />
        <character char_type="range_value" from="9.8" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s8" to="11.5" to_unit="mm" />
        <character name="length" src="d0_s8" unit="mm" value="9.8" value_original="9.8" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="4" to_unit="mm" />
        <character constraint="sepal" constraintid="o6664" is_modifier="false" name="length" src="d0_s8" value="1.5(-2) times as long as sepals" />
      </biological_entity>
      <biological_entity id="o6664" name="sepal" name_original="sepals" src="d0_s8" type="structure" />
    </statement>
    <statement id="d0_s9">
      <text>Seeds 0.7–1.2 mm. 2n = 72.</text>
      <biological_entity id="o6665" name="seed" name_original="seeds" src="d0_s9" type="structure">
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s9" to="1.2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o6666" name="chromosome" name_original="" src="d0_s9" type="structure">
        <character name="quantity" src="d0_s9" value="72" value_original="72" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Lawns, cemeteries, roadsides, riverbanks, old pastures</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="lawns" />
        <character name="habitat" value="cemeteries" />
        <character name="habitat" value="roadsides" />
        <character name="habitat" value="riverbanks" />
        <character name="habitat" value="old pastures" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; Nfld. and Labr. (Nfld.), Ont., Que.; Conn., Md., Mass., N.J., N.Y.; w Europe.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nfld. and Labr. (Nfld.)" establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Ont." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Conn." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Md." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="Mass." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.J." establishment_means="introduced" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="introduced" />
        <character name="distribution" value="w Europe" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>4a.</number>
  <discussion>Subspecies arvense is probably more widespread in North America than present information suggests, but identification of herbarium specimens can be difficult and uncertain. However, in the field the two subspecies are readily distinguishable because of the larger size of subsp. arvense and its strongly rhizomatous habit. Hybrids with C. tomentosum (which have been called C. ×maureri Schulze, an invalid name) are readily formed when the two taxa grow together (J. K. Morton 1973).</discussion>
  
</bio:treatment>