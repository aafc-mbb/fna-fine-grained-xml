<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">401</other_info_on_meta>
    <other_info_on_meta type="mention_page">400</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="Torrey" date="1827" rank="species">tenellum</taxon_name>
    <taxon_name authority="(Torrey ex Bentham) Torrey in W. H. Emory" date="1859" rank="variety">platyphyllum</taxon_name>
    <place_of_publication>
      <publication_title>in W. H. Emory, Rep. U.S. Mex. Bound.</publication_title>
      <place_in_publication>2(1): 176. 1859</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species tenellum;variety platyphyllum;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060517</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Torrey ex Bentham" date="unknown" rank="species">platyphyllum</taxon_name>
    <place_of_publication>
      <publication_title>in A. P. de Candolle and A. L. P. P. de Candolle, Prodr.</publication_title>
      <place_in_publication>14: 20. 1856</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Eriogonum;species platyphyllum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">tenellum</taxon_name>
    <taxon_name authority="(Torrey ex Bentham) S. Stokes" date="unknown" rank="subspecies">platyphyllum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species tenellum;subspecies platyphyllum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants 3–6 dm.</text>
      <biological_entity id="o3722" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="3" from_unit="dm" name="some_measurement" src="d0_s0" to="6" to_unit="dm" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Caudex stems compact to slightly spreading.</text>
      <biological_entity constraint="caudex" id="o3723" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" name="architecture_or_arrangement" src="d0_s1" value="compact" value_original="compact" />
        <character is_modifier="false" modifier="slightly" name="orientation" src="d0_s1" value="spreading" value_original="spreading" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves sheathing up stems;</text>
      <biological_entity id="o3724" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character constraint="up stems" constraintid="o3725" is_modifier="false" name="architecture_or_shape" src="d0_s2" value="sheathing" value_original="sheathing" />
      </biological_entity>
      <biological_entity id="o3725" name="stem" name_original="stems" src="d0_s2" type="structure" />
    </statement>
    <statement id="d0_s3">
      <text>blade ovate to suborbiculate or orbiculate, 1–3 × 1–3 cm.</text>
      <biological_entity id="o3726" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="ovate" name="shape" src="d0_s3" to="suborbiculate or orbiculate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s3" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Oct.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Oct" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly to rocky flats and slopes, saltbush, mesquite, and creosote bush communities, oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly to rocky flats" />
        <character name="habitat" value="gravelly to slopes" />
        <character name="habitat" value="saltbush" />
        <character name="habitat" value="mesquite" />
        <character name="habitat" value="creosote bush communities" />
        <character name="habitat" value="oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-900 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="900" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Tex.; Mexico (Coahuila, Nuevo León).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico (Coahuila)" establishment_means="native" />
        <character name="distribution" value="Mexico (Nuevo León)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>176c.</number>
  <other_name type="common_name">Broad-leaf wild buckwheat</other_name>
  <discussion>Variety platyphyllum is a Mexican taxon that just enters the flora area in Brewster, Presidio, Terrell, and Uvalde counties of western Texas. The type was gathered along the Nueces River in Uvalde County, but the variety has not been found in that area since 1849. Although this differs from the other varieties in terms of its leaf shap, a sharp distinction between this taxon and the others is not always possible in Texas.</discussion>
  
</bio:treatment>