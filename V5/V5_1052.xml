<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">513</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="mention_page">512</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Eaton" date="unknown" rank="subfamily">Polygonoideae</taxon_name>
    <taxon_name authority="Linnaeus" date="1754" rank="genus">rumex</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subgenus">Rumex</taxon_name>
    <taxon_name authority="Rechinger f." date="1937" rank="section">Axillares</taxon_name>
    <taxon_name authority="Lepage" date="1955" rank="species">subarcticus</taxon_name>
    <place_of_publication>
      <publication_title>Naturaliste Canad.</publication_title>
      <place_in_publication>82: 191. 1955</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily polygonoideae;genus rumex;subgenus rumex;section axillares;species subarcticus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060802</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Rumex</taxon_name>
    <taxon_name authority="Bigelow" date="unknown" rank="species">pallidus</taxon_name>
    <taxon_name authority="(Lepage) Á. Löve" date="unknown" rank="subspecies">subarcticus</taxon_name>
    <taxon_hierarchy>genus Rumex;species pallidus;subspecies subarcticus;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, glabrous, with vertical rootstock.</text>
      <biological_entity id="o29419" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character name="growth_form" value="plant" />
      </biological_entity>
      <biological_entity id="o29420" name="rootstock" name_original="rootstock" src="d0_s0" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s0" value="vertical" value_original="vertical" />
      </biological_entity>
      <relation from="o29419" id="r3310" name="with" negation="false" src="d0_s0" to="o29420" />
    </statement>
    <statement id="d0_s1">
      <text>Stems usually procumbent, rarely ascending, usually producing axillary shoots below 1st-order inflorescence or at proximal nodes, 30–60 cm.</text>
      <biological_entity id="o29421" name="stem" name_original="stems" src="d0_s1" type="structure">
        <character is_modifier="false" modifier="usually" name="growth_form" src="d0_s1" value="procumbent" value_original="procumbent" />
        <character is_modifier="false" modifier="rarely" name="orientation" src="d0_s1" value="ascending" value_original="ascending" />
      </biological_entity>
      <biological_entity constraint="axillary" id="o29422" name="shoot" name_original="shoots" src="d0_s1" type="structure" />
      <biological_entity constraint="proximal" id="o29423" name="node" name_original="nodes" src="d0_s1" type="structure">
        <character char_type="range_value" from="30" from_unit="cm" name="some_measurement" src="d0_s1" to="60" to_unit="cm" />
      </biological_entity>
      <relation from="o29421" id="r3311" modifier="usually" name="producing" negation="false" src="d0_s1" to="o29422" />
      <relation from="o29421" id="r3312" modifier="usually" name="below 1st-order inflorescence or at" negation="false" src="d0_s1" to="o29423" />
    </statement>
    <statement id="d0_s2">
      <text>Leaf-blades narrowly linear-lanceolate, 6–17 × 1–3 cm, usually ca. 7–10 times as long as wide, widest near middle, usually thick, not coriaceous or subcoriaceous, base cuneate, margins entire, usually strongly undulate and/or crenulate, apex acute.</text>
      <biological_entity id="o29424" name="leaf-blade" name_original="leaf-blades" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s2" value="linear-lanceolate" value_original="linear-lanceolate" />
        <character char_type="range_value" from="6" from_unit="cm" name="length" src="d0_s2" to="17" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s2" to="3" to_unit="cm" />
        <character is_modifier="false" name="l_w_ratio" src="d0_s2" value="7-10" value_original="7-10" />
        <character constraint="near middle" constraintid="o29425" is_modifier="false" name="width" src="d0_s2" value="widest" value_original="widest" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s2" value="coriaceous" value_original="coriaceous" />
        <character is_modifier="false" name="texture" src="d0_s2" value="subcoriaceous" value_original="subcoriaceous" />
      </biological_entity>
      <biological_entity id="o29425" name="middle" name_original="middle" src="d0_s2" type="structure">
        <character is_modifier="false" modifier="usually" name="width" notes="" src="d0_s2" value="thick" value_original="thick" />
      </biological_entity>
      <biological_entity id="o29426" name="base" name_original="base" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="cuneate" value_original="cuneate" />
      </biological_entity>
      <biological_entity id="o29427" name="margin" name_original="margins" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="usually strongly" name="shape" src="d0_s2" value="undulate" value_original="undulate" />
        <character is_modifier="false" name="shape" src="d0_s2" value="crenulate" value_original="crenulate" />
      </biological_entity>
      <biological_entity id="o29428" name="apex" name_original="apex" src="d0_s2" type="structure">
        <character is_modifier="false" name="shape" src="d0_s2" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Inflorescences terminal and axillary, terminal usually occupying distal 1/5–1/3 of stem, rather dense towards apex, distinctly interrupted in proximal 1/2, usually broadly paniculate (branches almost at right angles to main axis, simple or with few 2d-order branches).</text>
      <biological_entity id="o29429" name="inflorescence" name_original="inflorescences" src="d0_s3" type="structure">
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character is_modifier="false" name="position" src="d0_s3" value="axillary" value_original="axillary" />
        <character is_modifier="false" modifier="usually" name="position_or_structure_subtype" src="d0_s3" value="terminal" value_original="terminal" />
        <character constraint="towards apex" constraintid="o29431" is_modifier="false" modifier="rather" name="density" src="d0_s3" value="dense" value_original="dense" />
        <character constraint="in proximal 1/2" constraintid="o29432" is_modifier="false" modifier="distinctly" name="architecture" notes="" src="d0_s3" value="interrupted" value_original="interrupted" />
        <character is_modifier="false" modifier="usually broadly" name="arrangement" notes="" src="d0_s3" value="paniculate" value_original="paniculate" />
      </biological_entity>
      <biological_entity id="o29430" name="stem" name_original="stem" src="d0_s3" type="structure">
        <character is_modifier="true" name="position_or_shape" src="d0_s3" value="distal" value_original="distal" />
        <character char_type="range_value" from="1/5" is_modifier="true" name="quantity" src="d0_s3" to="1/3" />
      </biological_entity>
      <biological_entity id="o29431" name="apex" name_original="apex" src="d0_s3" type="structure" />
      <biological_entity constraint="proximal" id="o29432" name="1/2" name_original="1/2" src="d0_s3" type="structure" />
      <relation from="o29429" id="r3313" name="occupying" negation="false" src="d0_s3" to="o29430" />
    </statement>
    <statement id="d0_s4">
      <text>Pedicels articulated in proximal 1/3, filiform, 4–7 mm, not more than 2–2.5 times as long as inner tepals, articulation slightly swollen.</text>
      <biological_entity id="o29433" name="pedicel" name_original="pedicels" src="d0_s4" type="structure">
        <character constraint="in proximal 1/3" constraintid="o29434" is_modifier="false" name="architecture" src="d0_s4" value="articulated" value_original="articulated" />
        <character is_modifier="false" name="shape" notes="" src="d0_s4" value="filiform" value_original="filiform" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s4" to="7" to_unit="mm" />
        <character constraint="tepal" constraintid="o29435" is_modifier="false" name="length" src="d0_s4" value="2+-2.5 times as long as inner tepals" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o29434" name="1/3" name_original="1/3" src="d0_s4" type="structure" />
      <biological_entity constraint="inner" id="o29435" name="tepal" name_original="tepals" src="d0_s4" type="structure" />
      <biological_entity id="o29436" name="articulation" name_original="articulation" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="slightly" name="shape" src="d0_s4" value="swollen" value_original="swollen" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Flowers 10–20 in whorls;</text>
      <biological_entity id="o29437" name="flower" name_original="flowers" src="d0_s5" type="structure">
        <character char_type="range_value" constraint="in whorls" constraintid="o29438" from="10" name="quantity" src="d0_s5" to="20" />
      </biological_entity>
      <biological_entity id="o29438" name="whorl" name_original="whorls" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>inner tepals, broadly deltoid or deltoid-ovate, 3–4 × 3.2–4 (–4.5) mm, base truncate, margins entire or indistinctly crenulate, apex obtuse or subacute;</text>
      <biological_entity constraint="inner" id="o29439" name="tepal" name_original="tepals" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s6" value="deltoid" value_original="deltoid" />
        <character is_modifier="false" name="shape" src="d0_s6" value="deltoid-ovate" value_original="deltoid-ovate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s6" to="4" to_unit="mm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s6" to="4.5" to_unit="mm" />
        <character char_type="range_value" from="3.2" from_unit="mm" name="width" src="d0_s6" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o29440" name="base" name_original="base" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s6" value="truncate" value_original="truncate" />
      </biological_entity>
      <biological_entity id="o29441" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="entire" value_original="entire" />
        <character is_modifier="false" modifier="indistinctly" name="shape" src="d0_s6" value="crenulate" value_original="crenulate" />
      </biological_entity>
      <biological_entity id="o29442" name="apex" name_original="apex" src="d0_s6" type="structure">
        <character is_modifier="false" name="shape" src="d0_s6" value="obtuse" value_original="obtuse" />
        <character is_modifier="false" name="shape" src="d0_s6" value="subacute" value_original="subacute" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>tubercles absent, rarely small and indistinct.</text>
      <biological_entity id="o29443" name="tubercle" name_original="tubercles" src="d0_s7" type="structure">
        <character is_modifier="false" name="presence" src="d0_s7" value="absent" value_original="absent" />
        <character is_modifier="false" modifier="rarely" name="size" src="d0_s7" value="small" value_original="small" />
        <character is_modifier="false" name="prominence" src="d0_s7" value="indistinct" value_original="indistinct" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Achenes brown or dark reddish-brown, 2–3 × 1.5–2 mm. 2n = 20.</text>
      <biological_entity id="o29444" name="achene" name_original="achenes" src="d0_s8" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s8" value="brown" value_original="brown" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="dark reddish-brown" value_original="dark reddish-brown" />
        <character char_type="range_value" from="2" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s8" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o29445" name="chromosome" name_original="" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="20" value_original="20" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering early summer.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early summer" from="early summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Mostly coastal and alluvial habitats: sea beaches, shores of rivers and streams, wet meadows</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="coastal" modifier="mostly" />
        <character name="habitat" value="alluvial habitats" />
        <character name="habitat" value="sea beaches" />
        <character name="habitat" value="shores" constraint="of rivers and streams" />
        <character name="habitat" value="rivers" />
        <character name="habitat" value="streams" />
        <character name="habitat" value="wet meadows" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>0-200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="200" to_unit="m" from="0" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Nunavut., Que.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="Canada" value="Nunavut." establishment_means="native" />
        <character name="distribution" geographical_constraint="Canada" value="Que." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>28.</number>
  <other_name type="common_name">Subarctic or subarctic willow dock</other_name>
  <discussion>Some specimens of Rumex subarcticus have well-developed tubercles similar to those of R. pallidus (N. M. Sarkar 1958), to which it is closely related and of which it may be regarded as a northwestern subspecies or variety (see Á. Löve 1986).</discussion>
  
</bio:treatment>