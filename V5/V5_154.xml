<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>Richard K. Rabeler</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">72</other_info_on_meta>
    <other_info_on_meta type="mention_page">4</other_info_on_meta>
    <other_info_on_meta type="mention_page">7</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
    <other_info_on_meta type="illustrator">Yevonn Wilson-Ramsey</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="unknown" rank="subfamily">Alsinoideae</taxon_name>
    <taxon_name authority="Fenzl in S. L. Endlicher" date="1840" rank="genus">LEPYRODICLIS</taxon_name>
    <place_of_publication>
      <publication_title>in S. L. Endlicher, Gen. Pl.</publication_title>
      <place_in_publication>13: 966. 1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily alsinoideae;genus lepyrodiclis;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek lepyron, rind or husk, and diklis, double-folding, alluding to two-valved capsule</other_info_on_name>
    <other_info_on_name type="fna_id">118298</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, annual.</text>
      <biological_entity id="o3103" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Taproots slender.</text>
      <biological_entity id="o3104" name="taproot" name_original="taproots" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Stems ascending to decumbent or sprawling, simple or branched, terete or angled.</text>
      <biological_entity id="o3105" name="stem" name_original="stems" src="d0_s2" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s2" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="sprawling" value_original="sprawling" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="simple" value_original="simple" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
        <character is_modifier="false" name="shape" src="d0_s2" value="terete" value_original="terete" />
        <character is_modifier="false" name="shape" src="d0_s2" value="angled" value_original="angled" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves connate proximally, sessile;</text>
      <biological_entity id="o3106" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" modifier="proximally" name="fusion" src="d0_s3" value="connate" value_original="connate" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="sessile" value_original="sessile" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>blade 1-veined, lanceolate or rarely elliptic, not succulent, apex acute.</text>
      <biological_entity id="o3107" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s4" value="1-veined" value_original="1-veined" />
        <character is_modifier="false" name="shape" src="d0_s4" value="lanceolate" value_original="lanceolate" />
        <character is_modifier="false" modifier="rarely" name="shape" src="d0_s4" value="elliptic" value_original="elliptic" />
        <character is_modifier="false" modifier="not" name="texture" src="d0_s4" value="succulent" value_original="succulent" />
      </biological_entity>
      <biological_entity id="o3108" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences axillary and terminal, compound cymes;</text>
      <biological_entity id="o3109" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="position" src="d0_s5" value="axillary" value_original="axillary" />
        <character is_modifier="false" name="position_or_structure_subtype" src="d0_s5" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o3110" name="cyme" name_original="cymes" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="compound" value_original="compound" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>bracts paired, foliaceous, smaller.</text>
      <biological_entity id="o3111" name="bract" name_original="bracts" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="paired" value_original="paired" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="foliaceous" value_original="foliaceous" />
        <character is_modifier="false" name="size" src="d0_s6" value="smaller" value_original="smaller" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>Pedicels reflexed in fruit.</text>
      <biological_entity id="o3112" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character constraint="in fruit" constraintid="o3113" is_modifier="false" name="orientation" src="d0_s7" value="reflexed" value_original="reflexed" />
      </biological_entity>
      <biological_entity id="o3113" name="fruit" name_original="fruit" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: perianth and androecium weakly perigynous;</text>
      <biological_entity id="o3114" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3115" name="perianth" name_original="perianth" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="weakly" name="position" src="d0_s8" value="perigynous" value_original="perigynous" />
      </biological_entity>
      <biological_entity id="o3116" name="androecium" name_original="androecium" src="d0_s8" type="structure">
        <character is_modifier="false" modifier="weakly" name="position" src="d0_s8" value="perigynous" value_original="perigynous" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>hypanthium minimal;</text>
      <biological_entity id="o3117" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3118" name="hypanthium" name_original="hypanthium" src="d0_s9" type="structure" />
    </statement>
    <statement id="d0_s10">
      <text>sepals 5, distinct, green, lanceolate to elliptic or ovate, 4–5 mm, herbaceous, margins white, scarious, apex somewhat acute, not hooded;</text>
      <biological_entity id="o3119" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3120" name="sepal" name_original="sepals" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="5" value_original="5" />
        <character is_modifier="false" name="fusion" src="d0_s10" value="distinct" value_original="distinct" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="green" value_original="green" />
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s10" to="elliptic or ovate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s10" to="5" to_unit="mm" />
        <character is_modifier="false" name="growth_form_or_texture" src="d0_s10" value="herbaceous" value_original="herbaceous" />
      </biological_entity>
      <biological_entity id="o3121" name="margin" name_original="margins" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="texture" src="d0_s10" value="scarious" value_original="scarious" />
      </biological_entity>
      <biological_entity id="o3122" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" modifier="somewhat" name="shape" src="d0_s10" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="not" name="architecture" src="d0_s10" value="hooded" value_original="hooded" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>petals 5, white or pink, not clawed, blade apex entire or emarginate;</text>
      <biological_entity id="o3123" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3124" name="petal" name_original="petals" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="5" value_original="5" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="pink" value_original="pink" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s11" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity constraint="blade" id="o3125" name="apex" name_original="apex" src="d0_s11" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="entire" value_original="entire" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="emarginate" value_original="emarginate" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>nectaries at base of filaments opposite sepals;</text>
      <biological_entity id="o3126" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o3127" name="nectary" name_original="nectaries" src="d0_s12" type="structure" />
      <biological_entity id="o3128" name="base" name_original="base" src="d0_s12" type="structure" />
      <biological_entity id="o3129" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="opposite" value_original="opposite" />
      </biological_entity>
      <biological_entity id="o3130" name="sepal" name_original="sepals" src="d0_s12" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s12" value="opposite" value_original="opposite" />
      </biological_entity>
      <relation from="o3127" id="r346" name="at" negation="false" src="d0_s12" to="o3128" />
      <relation from="o3128" id="r347" name="part_of" negation="false" src="d0_s12" to="o3129" />
      <relation from="o3128" id="r348" name="part_of" negation="false" src="d0_s12" to="o3130" />
    </statement>
    <statement id="d0_s13">
      <text>stamens 10, arising from nectariferous disc at ovary base;</text>
      <biological_entity id="o3131" name="flower" name_original="flowers" src="d0_s13" type="structure" />
      <biological_entity id="o3132" name="stamen" name_original="stamens" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="10" value_original="10" />
        <character constraint="from nectariferous disc" constraintid="o3133" is_modifier="false" name="orientation" src="d0_s13" value="arising" value_original="arising" />
      </biological_entity>
      <biological_entity constraint="nectariferous" id="o3133" name="disc" name_original="disc" src="d0_s13" type="structure" />
      <biological_entity constraint="ovary" id="o3134" name="base" name_original="base" src="d0_s13" type="structure" />
      <relation from="o3133" id="r349" name="at" negation="false" src="d0_s13" to="o3134" />
    </statement>
    <statement id="d0_s14">
      <text>filaments distinct nearly to base;</text>
      <biological_entity id="o3135" name="flower" name_original="flowers" src="d0_s14" type="structure" />
      <biological_entity id="o3136" name="filament" name_original="filaments" src="d0_s14" type="structure">
        <character constraint="to base" constraintid="o3137" is_modifier="false" name="fusion" src="d0_s14" value="distinct" value_original="distinct" />
      </biological_entity>
      <biological_entity id="o3137" name="base" name_original="base" src="d0_s14" type="structure" />
    </statement>
    <statement id="d0_s15">
      <text>staminodes absent;</text>
      <biological_entity id="o3138" name="flower" name_original="flowers" src="d0_s15" type="structure" />
      <biological_entity id="o3139" name="staminode" name_original="staminodes" src="d0_s15" type="structure">
        <character is_modifier="false" name="presence" src="d0_s15" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>styles 2, clavate, 3.5–4 mm, glabrous proximally;</text>
      <biological_entity id="o3140" name="flower" name_original="flowers" src="d0_s16" type="structure" />
      <biological_entity id="o3141" name="style" name_original="styles" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s16" value="clavate" value_original="clavate" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s16" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>stigmas 2, subterminal, minutely roughened to papillate (50×).</text>
      <biological_entity id="o3142" name="flower" name_original="flowers" src="d0_s17" type="structure" />
      <biological_entity id="o3143" name="stigma" name_original="stigmas" src="d0_s17" type="structure">
        <character name="quantity" src="d0_s17" value="2" value_original="2" />
        <character is_modifier="false" name="position" src="d0_s17" value="subterminal" value_original="subterminal" />
        <character char_type="range_value" from="minutely roughened" name="relief" src="d0_s17" to="papillate" />
        <character name="quantity" src="d0_s17" value="[50" value_original="[50" />
      </biological_entity>
    </statement>
    <statement id="d0_s18">
      <text>Capsules globose-ovoid, opening by 2 straight valves;</text>
      <biological_entity id="o3144" name="capsule" name_original="capsules" src="d0_s18" type="structure">
        <character is_modifier="false" name="shape" src="d0_s18" value="globose-ovoid" value_original="globose-ovoid" />
      </biological_entity>
      <biological_entity id="o3145" name="valve" name_original="valves" src="d0_s18" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s18" value="2" value_original="2" />
        <character is_modifier="true" name="course" src="d0_s18" value="straight" value_original="straight" />
      </biological_entity>
      <relation from="o3144" id="r350" name="opening by" negation="false" src="d0_s18" to="o3145" />
    </statement>
    <statement id="d0_s19">
      <text>carpophore absent.</text>
      <biological_entity id="o3146" name="carpophore" name_original="carpophore" src="d0_s19" type="structure">
        <character is_modifier="false" name="presence" src="d0_s19" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s20">
      <text>Seeds 1–2 (–4), dark-brown, reniform, laterally compressed, tuberculate, marginal wing absent, appendage absent.</text>
      <biological_entity id="o3147" name="seed" name_original="seeds" src="d0_s20" type="structure">
        <character char_type="range_value" from="2" from_inclusive="false" name="atypical_quantity" src="d0_s20" to="4" />
        <character char_type="range_value" from="1" name="quantity" src="d0_s20" to="2" />
        <character is_modifier="false" name="coloration" src="d0_s20" value="dark-brown" value_original="dark-brown" />
        <character is_modifier="false" name="shape" src="d0_s20" value="reniform" value_original="reniform" />
        <character is_modifier="false" modifier="laterally" name="shape" src="d0_s20" value="compressed" value_original="compressed" />
        <character is_modifier="false" name="relief" src="d0_s20" value="tuberculate" value_original="tuberculate" />
      </biological_entity>
      <biological_entity constraint="marginal" id="o3148" name="wing" name_original="wing" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s21">
      <text>x = 17.</text>
      <biological_entity id="o3149" name="appendage" name_original="appendage" src="d0_s20" type="structure">
        <character is_modifier="false" name="presence" src="d0_s20" value="absent" value_original="absent" />
      </biological_entity>
      <biological_entity constraint="x" id="o3150" name="chromosome" name_original="" src="d0_s21" type="structure">
        <character name="quantity" src="d0_s21" value="17" value_original="17" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Introduced; c, sw Asia; introduced in Europe (Germany), Asia (Japan).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" value="c" establishment_means="introduced" />
        <character name="distribution" value="sw Asia" establishment_means="introduced" />
        <character name="distribution" value="in Europe (Germany)" establishment_means="introduced" />
        <character name="distribution" value="Asia (Japan)" establishment_means="introduced" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <discussion>Species 3–4 (1 in the flora).</discussion>
  <references>
    <reference>Rabeler, R. K. and R. R. Old. 1992. Lepyrodiclis holosteoides (Caryophyllaceae), “new” to North America. Madroño 39: 240–242.</reference>
  </references>
  
</bio:treatment>