<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">460</other_info_on_meta>
    <other_info_on_meta type="mention_page">448</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="Reveal &amp; Hardham" date="1989" rank="subgenus">Amphietes</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="section">Ptelosepala</taxon_name>
    <taxon_name authority="Goodman" date="1934" rank="species">rectispina</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>21: 72. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus amphietes;section ptelosepala;species rectispina;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060092</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants spreading to decumbent, 0.3–0.8 (–1) × 0.5–4 (–5) dm, appressed-pubescent.</text>
      <biological_entity id="o24820" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="spreading" name="orientation" src="d0_s0" to="decumbent" />
        <character char_type="range_value" from="0.8" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="1" to_unit="dm" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="length" src="d0_s0" to="0.8" to_unit="dm" />
        <character char_type="range_value" from="4" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="4" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="appressed-pubescent" value_original="appressed-pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o24821" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 0.5–2 cm;</text>
      <biological_entity id="o24822" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s2" to="2" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblanceolate to spatulate, 0.5–1.5 (–2) × 0.2–0.6 cm, thinly pubescent.</text>
      <biological_entity id="o24823" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s3" to="spatulate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s3" to="0.6" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences with involucres in small, open clusters 0.5–1.5 cm diam., greenish to grayish;</text>
      <biological_entity id="o24824" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character char_type="range_value" from="greenish" name="diam" notes="" src="d0_s4" to="grayish" />
      </biological_entity>
      <biological_entity id="o24825" name="involucre" name_original="involucres" src="d0_s4" type="structure" />
      <relation from="o24824" id="r2758" name="with" negation="false" src="d0_s4" to="o24825" />
    </statement>
    <statement id="d0_s5">
      <text>bracts 2, without whorl of sessile bracts about midstem, usually leaflike, oblanceolate to elliptic, 0.5–1.5 cm × 1.5–5 mm, gradually reduced and becoming scalelike at distal nodes, linear, aciculate, acerose, 0.3–0.8 cm × 1–2 mm, awns straight, 0.5–1.5 mm.</text>
      <biological_entity id="o24826" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character is_modifier="false" modifier="usually" name="shape" notes="" src="d0_s5" value="leaflike" value_original="leaflike" />
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="elliptic" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s5" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="1.5" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
        <character is_modifier="false" modifier="gradually" name="size" src="d0_s5" value="reduced" value_original="reduced" />
        <character constraint="at distal nodes" constraintid="o24830" is_modifier="false" modifier="becoming" name="shape" src="d0_s5" value="scale-like" value_original="scalelike" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" notes="" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="coloration" src="d0_s5" value="aciculate" value_original="aciculate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acerose" value_original="acerose" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s5" to="0.8" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="2" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o24827" name="whorl" name_original="whorl" src="d0_s5" type="structure" />
      <biological_entity id="o24828" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="true" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
      </biological_entity>
      <biological_entity id="o24829" name="midstem" name_original="midstem" src="d0_s5" type="structure" />
      <biological_entity constraint="distal" id="o24830" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity id="o24831" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1.5" to_unit="mm" />
      </biological_entity>
      <relation from="o24826" id="r2759" name="without" negation="false" src="d0_s5" to="o24827" />
      <relation from="o24827" id="r2760" name="part_of" negation="false" src="d0_s5" to="o24828" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres 3–10+, grayish to reddish, urceolate, slightly ventricose basally, 2–2.5 (–3) mm, slightly corrugate, without scarious or membranous margins, densely pubescent;</text>
      <biological_entity id="o24832" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character char_type="range_value" from="3" name="quantity" src="d0_s6" to="10" upper_restricted="false" />
        <character char_type="range_value" from="grayish" name="coloration" src="d0_s6" to="reddish" />
        <character is_modifier="false" name="shape" src="d0_s6" value="urceolate" value_original="urceolate" />
        <character is_modifier="false" modifier="slightly; basally" name="shape" src="d0_s6" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s6" to="3" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s6" to="2.5" to_unit="mm" />
        <character is_modifier="false" modifier="slightly" name="arrangement_or_relief" src="d0_s6" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" modifier="densely" name="pubescence" notes="" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o24833" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="true" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o24832" id="r2761" name="without" negation="false" src="d0_s6" to="o24833" />
    </statement>
    <statement id="d0_s7">
      <text>teeth spreading, unequal, 1–2 mm;</text>
      <biological_entity id="o24834" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>awns straight or uncinate, unequal, with longer anterior one straight, mostly 1.5–2.5 mm, others uncinate, 0.3–0.6 mm.</text>
      <biological_entity id="o24835" name="awn" name_original="awns" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s8" value="uncinate" value_original="uncinate" />
        <character is_modifier="false" name="size" src="d0_s8" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="1.5" from_unit="mm" modifier="mostly" name="some_measurement" notes="" src="d0_s8" to="2.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="longer anterior" id="o24836" name="one" name_original="one" src="d0_s8" type="structure">
        <character is_modifier="false" name="course" src="d0_s8" value="straight" value_original="straight" />
      </biological_entity>
      <biological_entity id="o24837" name="other" name_original="others" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="uncinate" value_original="uncinate" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="some_measurement" src="d0_s8" to="0.6" to_unit="mm" />
      </biological_entity>
      <relation from="o24835" id="r2762" name="with" negation="false" src="d0_s8" to="o24836" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers exserted;</text>
      <biological_entity id="o24838" name="flower" name_original="flowers" src="d0_s9" type="structure">
        <character is_modifier="false" name="position" src="d0_s9" value="exserted" value_original="exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>perianth bicolored with floral-tube yellow and tepals yellow or white, cylindric, 3.5–4 mm, sparsely pubescent;</text>
      <biological_entity id="o24839" name="perianth" name_original="perianth" src="d0_s10" type="structure">
        <character constraint="with floral-tube" constraintid="o24840" is_modifier="false" name="coloration" src="d0_s10" value="bicolored" value_original="bicolored" />
      </biological_entity>
      <biological_entity id="o24840" name="floral-tube" name_original="floral-tube" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
      </biological_entity>
      <biological_entity id="o24841" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="yellow" value_original="yellow" />
        <character is_modifier="false" name="coloration" src="d0_s10" value="white" value_original="white" />
        <character is_modifier="false" name="shape" src="d0_s10" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="3.5" from_unit="mm" name="some_measurement" src="d0_s10" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s10" value="pubescent" value_original="pubescent" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>tepals connate 1/2 their length, dimorphic, obovate, those of outer whorl white, obovate to nearly orbiculate, 3–4 times longer than those of inner whorl,, truncate to slightly 2-lobed apically, those of inner lobes erect, yellow, broadly obovate, truncate and erose apically;</text>
      <biological_entity id="o24842" name="tepal" name_original="tepals" src="d0_s11" type="structure" constraint="whorl" constraint_original="whorl; whorl">
        <character is_modifier="false" name="fusion" src="d0_s11" value="connate" value_original="connate" />
        <character name="length" src="d0_s11" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="growth_form" src="d0_s11" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="white" value_original="white" />
        <character char_type="range_value" from="obovate" name="shape" src="d0_s11" to="nearly orbiculate" />
        <character constraint="of inner whorl" constraintid="o24844" is_modifier="false" name="length_or_size" src="d0_s11" value="3-4 times longer than" value_original="3-4 times longer than" />
        <character char_type="range_value" from="truncate" modifier="apically" name="shape" notes="" src="d0_s11" to="slightly 2-lobed" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character is_modifier="false" name="coloration" src="d0_s11" value="yellow" value_original="yellow" />
        <character is_modifier="false" modifier="broadly" name="shape" src="d0_s11" value="obovate" value_original="obovate" />
        <character is_modifier="false" name="architecture_or_shape" src="d0_s11" value="truncate" value_original="truncate" />
        <character is_modifier="false" modifier="apically" name="architecture_or_relief" src="d0_s11" value="erose" value_original="erose" />
      </biological_entity>
      <biological_entity constraint="outer" id="o24843" name="whorl" name_original="whorl" src="d0_s11" type="structure" />
      <biological_entity constraint="inner" id="o24844" name="whorl" name_original="whorl" src="d0_s11" type="structure" />
      <biological_entity constraint="inner" id="o24845" name="lobe" name_original="lobes" src="d0_s11" type="structure" />
      <relation from="o24842" id="r2763" name="part_of" negation="false" src="d0_s11" to="o24843" />
      <relation from="o24842" id="r2764" name="part_of" negation="false" src="d0_s11" to="o24845" />
    </statement>
    <statement id="d0_s12">
      <text>stamens 9, included;</text>
      <biological_entity id="o24846" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character name="quantity" src="d0_s12" value="9" value_original="9" />
        <character is_modifier="false" name="position" src="d0_s12" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>filaments distinct, 1–1.5 mm, glabrous;</text>
      <biological_entity id="o24847" name="filament" name_original="filaments" src="d0_s13" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s13" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s13" to="1.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s13" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>anthers yellow to golden, oblong, 0.5–0.6 mm.</text>
      <biological_entity id="o24848" name="anther" name_original="anthers" src="d0_s14" type="structure">
        <character char_type="range_value" from="yellow" name="coloration" src="d0_s14" to="golden" />
        <character is_modifier="false" name="shape" src="d0_s14" value="oblong" value_original="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s14" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Achenes brown, globose-lenticular, 3–3.5 mm. 2n = (36), 40, (44).</text>
      <biological_entity id="o24849" name="achene" name_original="achenes" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s15" value="globose-lenticular" value_original="globose-lenticular" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s15" to="3.5" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o24850" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="atypical_quantity" src="d0_s15" value="36" value_original="36" />
        <character name="quantity" src="d0_s15" value="40" value_original="40" />
        <character name="atypical_quantity" src="d0_s15" value="44" value_original="44" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, mixed grassland communities, pine-oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="mixed grassland communities" />
        <character name="habitat" value="pine-oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="600" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>17.</number>
  <other_name type="common_name">Prickly spineflower</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Chorizanthe rectispina is infrequent and localized in the Coast Ranges of west-central California.</discussion>
  
</bio:treatment>