<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">463</other_info_on_meta>
    <other_info_on_meta type="mention_page">449</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="R. Brown ex Bentham" date="1836" rank="genus">chorizanthe</taxon_name>
    <taxon_name authority="Reveal &amp; Hardham" date="1989" rank="subgenus">Amphietes</taxon_name>
    <taxon_name authority="Nuttall" date="1848" rank="section">Ptelosepala</taxon_name>
    <taxon_name authority="Goodman" date="1934" rank="species">leptotheca</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Missouri Bot. Gard.</publication_title>
      <place_in_publication>21: 61. 1934</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus chorizanthe;subgenus amphietes;section ptelosepala;species leptotheca;</taxon_hierarchy>
    <other_info_on_name type="fna_id">250060077</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants erect to spreading, 0.5–3 (–3.5) × 0.5–3 (–5) dm, thinly pubescent.</text>
      <biological_entity id="o21735" name="whole_organism" name_original="" src="" type="structure">
        <character char_type="range_value" from="erect" name="orientation" src="d0_s0" to="spreading" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_length" src="d0_s0" to="3.5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="length" src="d0_s0" to="3" to_unit="dm" />
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="dm" name="atypical_width" src="d0_s0" to="5" to_unit="dm" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="width" src="d0_s0" to="3" to_unit="dm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s0" value="pubescent" value_original="pubescent" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Leaves basal;</text>
      <biological_entity id="o21736" name="leaf" name_original="leaves" src="d0_s1" type="structure">
        <character is_modifier="false" name="position" src="d0_s1" value="basal" value_original="basal" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>petiole 1–3 (–4) cm;</text>
      <biological_entity id="o21737" name="petiole" name_original="petiole" src="d0_s2" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s2" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s2" to="3" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>blade oblong to oblong-ovate, 0.5–2 (–3) × 0.3–0.5 (–0.7) cm, thinly pubescent adaxially, usually densely tomentose adaxially.</text>
      <biological_entity id="o21738" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="oblong" name="shape" src="d0_s3" to="oblong-ovate" />
        <character char_type="range_value" from="2" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s3" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="2" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_inclusive="false" from_unit="cm" name="atypical_width" src="d0_s3" to="0.7" to_unit="cm" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="width" src="d0_s3" to="0.5" to_unit="cm" />
        <character is_modifier="false" modifier="thinly; adaxially" name="pubescence" src="d0_s3" value="pubescent" value_original="pubescent" />
        <character is_modifier="false" modifier="usually densely; adaxially" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Inflorescences mostly flat-topped and openly branched, usually reddish;</text>
      <biological_entity id="o21739" name="inflorescence" name_original="inflorescences" src="d0_s4" type="structure">
        <character is_modifier="false" modifier="mostly" name="shape" src="d0_s4" value="flat-topped" value_original="flat-topped" />
        <character is_modifier="false" modifier="openly" name="architecture" src="d0_s4" value="branched" value_original="branched" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s4" value="reddish" value_original="reddish" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>bracts soon deciduous, 2, occasionally leaflike at proximal nodes and similar to proximal leaf-blades only more reduced, short-petiolate, ovate, 0.3–0.4 cm × 2–3 mm, otherwise sessile, linear and acicular, often acerose, 0.1–0.3 cm × 0.7–1 mm, awns straight, 0.5–1 mm.</text>
      <biological_entity id="o21740" name="bract" name_original="bracts" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="soon" name="duration" src="d0_s5" value="deciduous" value_original="deciduous" />
        <character name="quantity" src="d0_s5" value="2" value_original="2" />
        <character constraint="at proximal nodes" constraintid="o21741" is_modifier="false" modifier="occasionally" name="architecture_or_shape" src="d0_s5" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="short-petiolate" value_original="short-petiolate" />
        <character is_modifier="false" name="shape" src="d0_s5" value="ovate" value_original="ovate" />
        <character char_type="range_value" from="0.3" from_unit="cm" name="length" src="d0_s5" to="0.4" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="mm" name="width" src="d0_s5" to="3" to_unit="mm" />
        <character is_modifier="false" modifier="otherwise" name="architecture" src="d0_s5" value="sessile" value_original="sessile" />
        <character is_modifier="false" name="arrangement_or_course_or_shape" src="d0_s5" value="linear" value_original="linear" />
        <character is_modifier="false" name="shape" src="d0_s5" value="acicular" value_original="acicular" />
        <character is_modifier="false" modifier="often" name="shape" src="d0_s5" value="acerose" value_original="acerose" />
        <character char_type="range_value" from="0.1" from_unit="cm" name="length" src="d0_s5" to="0.3" to_unit="cm" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="width" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o21741" name="node" name_original="nodes" src="d0_s5" type="structure" />
      <biological_entity constraint="proximal" id="o21742" name="leaf-blade" name_original="leaf-blades" src="d0_s5" type="structure">
        <character is_modifier="false" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o21743" name="awn" name_original="awns" src="d0_s5" type="structure">
        <character is_modifier="false" name="course" src="d0_s5" value="straight" value_original="straight" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s5" to="1" to_unit="mm" />
      </biological_entity>
      <relation from="o21740" id="r2429" name="to" negation="false" src="d0_s5" to="o21742" />
    </statement>
    <statement id="d0_s6">
      <text>Involucres in congested clusters with 1 at node of dichotomies, reddish, cylindric, not ventricose, 3–4 mm, not corrugate, without scarious or membranous margins, thinly pubescent;</text>
      <biological_entity id="o21744" name="involucre" name_original="involucres" src="d0_s6" type="structure">
        <character constraint="at node" constraintid="o21745" modifier="with" name="quantity" src="d0_s6" value="1" value_original="1" />
        <character is_modifier="false" name="coloration" notes="" src="d0_s6" value="reddish" value_original="reddish" />
        <character is_modifier="false" name="shape" src="d0_s6" value="cylindric" value_original="cylindric" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s6" value="ventricose" value_original="ventricose" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s6" to="4" to_unit="mm" />
        <character is_modifier="false" modifier="not" name="arrangement_or_relief" src="d0_s6" value="corrugate" value_original="corrugate" />
        <character is_modifier="false" modifier="thinly" name="pubescence" notes="" src="d0_s6" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21745" name="node" name_original="node" src="d0_s6" type="structure" />
      <biological_entity id="o21746" name="dichotomy" name_original="dichotomies" src="d0_s6" type="structure" />
      <biological_entity id="o21747" name="margin" name_original="margins" src="d0_s6" type="structure">
        <character is_modifier="true" name="texture" src="d0_s6" value="scarious" value_original="scarious" />
        <character is_modifier="true" name="texture" src="d0_s6" value="membranous" value_original="membranous" />
      </biological_entity>
      <relation from="o21745" id="r2430" name="part_of" negation="false" src="d0_s6" to="o21746" />
      <relation from="o21744" id="r2431" name="without" negation="false" src="d0_s6" to="o21747" />
    </statement>
    <statement id="d0_s7">
      <text>teeth spreading, unequal, 0.7–1.5 mm with longer of 3 longest ones more erect than 3 other shorter and less-prominent ones, awns uncinate, 0.5–1 mm.</text>
      <biological_entity id="o21748" name="tooth" name_original="teeth" src="d0_s7" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s7" value="spreading" value_original="spreading" />
        <character is_modifier="false" name="size" src="d0_s7" value="unequal" value_original="unequal" />
        <character char_type="range_value" from="0.7" from_unit="mm" name="some_measurement" src="d0_s7" to="1.5" to_unit="mm" />
        <character constraint="of longest ones" constraintid="o21749" is_modifier="false" name="length_or_size" src="d0_s7" value="longer" value_original="longer" />
        <character is_modifier="false" modifier="more erect than" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
        <character is_modifier="false" modifier="more erect than" name="prominence" src="d0_s7" value="less-prominent" value_original="less-prominent" />
      </biological_entity>
      <biological_entity constraint="longest" id="o21749" name="one" name_original="ones" src="d0_s7" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s7" value="3" value_original="3" />
        <character modifier="more erect than" name="quantity" src="d0_s7" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o21750" name="awn" name_original="awns" src="d0_s7" type="structure">
        <character is_modifier="false" name="shape" src="d0_s7" value="uncinate" value_original="uncinate" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s7" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Flowers long-exserted;</text>
      <biological_entity id="o21751" name="flower" name_original="flowers" src="d0_s8" type="structure">
        <character is_modifier="false" name="position" src="d0_s8" value="long-exserted" value_original="long-exserted" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>perianth rose to red, infrequently with white lobes, cylindric, 4.5–6 mm, pubescent;</text>
      <biological_entity id="o21752" name="perianth" name_original="perianth" src="d0_s9" type="structure">
        <character char_type="range_value" from="rose" name="coloration" src="d0_s9" to="red" />
        <character is_modifier="false" name="shape" notes="" src="d0_s9" value="cylindric" value_original="cylindric" />
        <character char_type="range_value" from="4.5" from_unit="mm" name="some_measurement" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="pubescent" value_original="pubescent" />
      </biological_entity>
      <biological_entity id="o21753" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s9" value="white" value_original="white" />
      </biological_entity>
      <relation from="o21752" id="r2432" modifier="infrequently" name="with" negation="false" src="d0_s9" to="o21753" />
    </statement>
    <statement id="d0_s10">
      <text>tepals connate ca. 1/2 their length, dimorphic or sometimes monomorphic, narrowly oblanceolate, apex rounded, those of outer whorl slightly broader and occasionally longer than those of inner whorl;</text>
      <biological_entity id="o21754" name="tepal" name_original="tepals" src="d0_s10" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s10" value="connate" value_original="connate" />
        <character name="length" src="d0_s10" value="1/2" value_original="1/2" />
        <character is_modifier="false" name="growth_form" src="d0_s10" value="dimorphic" value_original="dimorphic" />
        <character is_modifier="false" modifier="sometimes" name="architecture" src="d0_s10" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s10" value="oblanceolate" value_original="oblanceolate" />
      </biological_entity>
      <biological_entity id="o21755" name="apex" name_original="apex" src="d0_s10" type="structure">
        <character is_modifier="false" name="shape" src="d0_s10" value="rounded" value_original="rounded" />
        <character is_modifier="false" modifier="slightly" name="width" src="d0_s10" value="broader" value_original="broader" />
      </biological_entity>
      <biological_entity constraint="outer" id="o21756" name="whorl" name_original="whorl" src="d0_s10" type="structure" />
      <relation from="o21755" id="r2433" name="part_of" negation="false" src="d0_s10" to="o21756" />
    </statement>
    <statement id="d0_s11">
      <text>stamens 9, mostly included;</text>
      <biological_entity id="o21757" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="9" value_original="9" />
        <character is_modifier="false" modifier="mostly" name="position" src="d0_s11" value="included" value_original="included" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>filaments distinct, 4–6 mm, glabrous;</text>
      <biological_entity id="o21758" name="filament" name_original="filaments" src="d0_s12" type="structure">
        <character is_modifier="false" name="fusion" src="d0_s12" value="distinct" value_original="distinct" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s12" to="6" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>anthers pink to red, ovate to oblong, 0.5–0.6 mm.</text>
      <biological_entity id="o21759" name="anther" name_original="anthers" src="d0_s13" type="structure">
        <character char_type="range_value" from="pink" name="coloration" src="d0_s13" to="red" />
        <character char_type="range_value" from="ovate" name="shape" src="d0_s13" to="oblong" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="some_measurement" src="d0_s13" to="0.6" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Achenes brown, lenticular, 3–4 mm. 2n = 38.</text>
      <biological_entity id="o21760" name="achene" name_original="achenes" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="lenticular" value_original="lenticular" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s14" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity constraint="2n" id="o21761" name="chromosome" name_original="" src="d0_s14" type="structure">
        <character name="quantity" src="d0_s14" value="38" value_original="38" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly flats and slopes, grassland and chaparral communities, pine-oak woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly flats" />
        <character name="habitat" value="sandy to slopes" />
        <character name="habitat" value="grassland" />
        <character name="habitat" value="chaparral communities" />
        <character name="habitat" value="pine-oak woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>(300-)600-1600(-1900) m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1600" to_unit="m" from="600" from_unit="m" />
        <character name="elevation" char_type="atypical_range" to="1900" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.; Mexico (Baja California).</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
        <character name="distribution" value="Mexico (Baja California)" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>23.</number>
  <other_name type="common_name">Ramona spineflower</other_name>
  <discussion>Chorizanthe leptotheca is found in the foothills of the San Bernardino Mountains of San Bernardino County southward along the eastern edge of the Santa Ana Mountains, and through the San Jacinto and Santa Rosa mountains of Riverside County into the mountains of central San Diego County. The species is also found in north-central Baja California.</discussion>
  <discussion>Ramona spineflower is clearly related to Chorizanthe staticoides, but that species occurs to the west of the range of C. leptotheca and the two are not known to be sympatric.</discussion>
  
</bio:treatment>