<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">199</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="C. L. Hitchcock &amp; Maguire" date="1947" rank="species">plankii</taxon_name>
    <place_of_publication>
      <publication_title>Revis. N. Amer. Silene,</publication_title>
      <place_in_publication>56, plate 7, fig. 55. 1947</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species plankii;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060878</other_info_on_name>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial, cespitose;</text>
      <biological_entity id="o3534" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character is_modifier="false" name="growth_form" src="d0_s0" value="cespitose" value_original="cespitose" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot stout;</text>
      <biological_entity id="o3535" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="fragility_or_size" src="d0_s1" value="stout" value_original="stout" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex with many often subterranean branches, woody.</text>
      <biological_entity id="o3536" name="caudex" name_original="caudex" src="d0_s2" type="structure">
        <character is_modifier="false" name="texture" notes="" src="d0_s2" value="woody" value_original="woody" />
      </biological_entity>
      <biological_entity id="o3537" name="branch" name_original="branches" src="d0_s2" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s2" value="many" value_original="many" />
        <character is_modifier="true" modifier="often" name="location" src="d0_s2" value="subterranean" value_original="subterranean" />
      </biological_entity>
      <relation from="o3536" id="r387" name="with" negation="false" src="d0_s2" to="o3537" />
    </statement>
    <statement id="d0_s3">
      <text>Stems ascending, branched, wiry, leafy, slender, 10–20 cm, finely retrorse gray-puberulent.</text>
      <biological_entity id="o3538" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s3" value="ascending" value_original="ascending" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="branched" value_original="branched" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="wiry" value_original="wiry" />
        <character is_modifier="false" name="architecture" src="d0_s3" value="leafy" value_original="leafy" />
        <character is_modifier="false" name="size" src="d0_s3" value="slender" value_original="slender" />
        <character char_type="range_value" from="10" from_unit="cm" name="some_measurement" src="d0_s3" to="20" to_unit="cm" />
        <character is_modifier="false" modifier="finely" name="orientation" src="d0_s3" value="retrorse" value_original="retrorse" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="gray-puberulent" value_original="gray-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>Leaves largest in midstem region;</text>
      <biological_entity id="o3539" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character constraint="in midstem region" constraintid="o3540" is_modifier="false" name="size" src="d0_s4" value="largest" value_original="largest" />
      </biological_entity>
      <biological_entity constraint="midstem" id="o3540" name="region" name_original="region" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>blade linear to narrowly lanceolate or oblanceolate, 1–4 cm × 1–5 mm, apex sharply acuminate, glandular-puberulent.</text>
      <biological_entity id="o3541" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="linear" name="shape" src="d0_s5" to="narrowly lanceolate or oblanceolate" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="4" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s5" to="5" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3542" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" modifier="sharply" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences with flowers usually solitary, terminal on branches.</text>
      <biological_entity id="o3543" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character constraint="on branches" constraintid="o3545" is_modifier="false" name="position_or_structure_subtype" notes="" src="d0_s6" value="terminal" value_original="terminal" />
      </biological_entity>
      <biological_entity id="o3544" name="flower" name_original="flowers" src="d0_s6" type="structure">
        <character is_modifier="false" modifier="usually" name="architecture_or_arrangement_or_growth_form" src="d0_s6" value="solitary" value_original="solitary" />
      </biological_entity>
      <biological_entity id="o3545" name="branch" name_original="branches" src="d0_s6" type="structure" />
      <relation from="o3543" id="r388" name="with" negation="false" src="d0_s6" to="o3544" />
    </statement>
    <statement id="d0_s7">
      <text>Pedicels shorter than calyx, glandular-puberulent.</text>
      <biological_entity id="o3546" name="pedicel" name_original="pedicels" src="d0_s7" type="structure">
        <character constraint="than calyx" constraintid="o3547" is_modifier="false" name="height_or_length_or_size" src="d0_s7" value="shorter" value_original="shorter" />
        <character is_modifier="false" name="pubescence" src="d0_s7" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o3547" name="calyx" name_original="calyx" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>Flowers: calyx 10-veined, tubular, constricted around carpophore, umbilicate, 20–30 × 3–6 mm, papery, green, glandular-puberulent, lobes lanceolate, 2–4 mm, margins membranous, apex acute;</text>
      <biological_entity id="o3548" name="flower" name_original="flowers" src="d0_s8" type="structure" />
      <biological_entity id="o3549" name="calyx" name_original="calyx" src="d0_s8" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s8" value="10-veined" value_original="10-veined" />
        <character is_modifier="false" name="shape" src="d0_s8" value="tubular" value_original="tubular" />
        <character constraint="around carpophore" constraintid="o3550" is_modifier="false" name="size" src="d0_s8" value="constricted" value_original="constricted" />
        <character is_modifier="false" name="shape" notes="" src="d0_s8" value="umbilicate" value_original="umbilicate" />
        <character char_type="range_value" from="20" from_unit="mm" name="length" src="d0_s8" to="30" to_unit="mm" />
        <character char_type="range_value" from="3" from_unit="mm" name="width" src="d0_s8" to="6" to_unit="mm" />
        <character is_modifier="false" name="texture" src="d0_s8" value="papery" value_original="papery" />
        <character is_modifier="false" name="coloration" src="d0_s8" value="green" value_original="green" />
        <character is_modifier="false" name="pubescence" src="d0_s8" value="glandular-puberulent" value_original="glandular-puberulent" />
      </biological_entity>
      <biological_entity id="o3550" name="carpophore" name_original="carpophore" src="d0_s8" type="structure" />
      <biological_entity id="o3551" name="lobe" name_original="lobes" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s8" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3552" name="margin" name_original="margins" src="d0_s8" type="structure">
        <character is_modifier="false" name="texture" src="d0_s8" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o3553" name="apex" name_original="apex" src="d0_s8" type="structure">
        <character is_modifier="false" name="shape" src="d0_s8" value="acute" value_original="acute" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>corolla scarlet, clawed, claw equaling calyx, limb obconic, 2-lobed, 7–10 mm, margins entire or crenate, appendages ± lacerate, 1–1.5 mm;</text>
      <biological_entity id="o3554" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o3555" name="corolla" name_original="corolla" src="d0_s9" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s9" value="scarlet" value_original="scarlet" />
        <character is_modifier="false" name="shape" src="d0_s9" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o3556" name="claw" name_original="claw" src="d0_s9" type="structure" />
      <biological_entity id="o3557" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character is_modifier="true" name="variability" src="d0_s9" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o3558" name="limb" name_original="limb" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="obconic" value_original="obconic" />
        <character is_modifier="false" name="shape" src="d0_s9" value="2-lobed" value_original="2-lobed" />
        <character char_type="range_value" from="7" from_unit="mm" name="some_measurement" src="d0_s9" to="10" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o3559" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="entire" value_original="entire" />
        <character is_modifier="false" name="shape" src="d0_s9" value="crenate" value_original="crenate" />
      </biological_entity>
      <biological_entity id="o3560" name="appendage" name_original="appendages" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="more or less" name="shape" src="d0_s9" value="lacerate" value_original="lacerate" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s9" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>stamens exserted, ± equaling corolla lobes;</text>
      <biological_entity id="o3561" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o3562" name="stamen" name_original="stamens" src="d0_s10" type="structure">
        <character is_modifier="false" name="position" src="d0_s10" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o3563" name="lobe" name_original="lobes" src="d0_s10" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s10" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>styles 3, exserted, ± equaling corolla lobes.</text>
      <biological_entity id="o3564" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o3565" name="style" name_original="styles" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="3" value_original="3" />
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
      </biological_entity>
      <biological_entity constraint="corolla" id="o3566" name="lobe" name_original="lobes" src="d0_s11" type="structure">
        <character is_modifier="true" modifier="more or less" name="variability" src="d0_s11" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Capsules narrowly ellipsoid, equaling calyx, opening by 6 recurved, brittle teeth;</text>
      <biological_entity id="o3567" name="capsule" name_original="capsules" src="d0_s12" type="structure">
        <character is_modifier="false" modifier="narrowly" name="shape" src="d0_s12" value="ellipsoid" value_original="ellipsoid" />
      </biological_entity>
      <biological_entity id="o3568" name="calyx" name_original="calyx" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o3569" name="tooth" name_original="teeth" src="d0_s12" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s12" value="6" value_original="6" />
        <character is_modifier="true" name="orientation" src="d0_s12" value="recurved" value_original="recurved" />
        <character is_modifier="true" name="fragility" src="d0_s12" value="brittle" value_original="brittle" />
      </biological_entity>
      <relation from="o3568" id="r389" name="opening by" negation="false" src="d0_s12" to="o3569" />
    </statement>
    <statement id="d0_s13">
      <text>carpophore ca. 5 mm.</text>
      <biological_entity id="o3570" name="carpophore" name_original="carpophore" src="d0_s13" type="structure">
        <character name="some_measurement" src="d0_s13" unit="mm" value="5" value_original="5" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>Seeds brown, reniform, 1.5 mm, rugose in concentric rings on sides, margins papillate.</text>
      <biological_entity id="o3571" name="seed" name_original="seeds" src="d0_s14" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s14" value="brown" value_original="brown" />
        <character is_modifier="false" name="shape" src="d0_s14" value="reniform" value_original="reniform" />
        <character name="some_measurement" src="d0_s14" unit="mm" value="1.5" value_original="1.5" />
        <character constraint="in concentric rings" constraintid="o3572" is_modifier="false" name="relief" src="d0_s14" value="rugose" value_original="rugose" />
      </biological_entity>
      <biological_entity constraint="concentric" id="o3572" name="ring" name_original="rings" src="d0_s14" type="structure" />
      <biological_entity id="o3573" name="side" name_original="sides" src="d0_s14" type="structure" />
      <relation from="o3572" id="r390" name="on" negation="false" src="d0_s14" to="o3573" />
    </statement>
    <statement id="d0_s15">
      <text>2n = 48.</text>
      <biological_entity id="o3574" name="margin" name_original="margins" src="d0_s14" type="structure">
        <character is_modifier="false" name="relief" src="d0_s14" value="papillate" value_original="papillate" />
      </biological_entity>
      <biological_entity constraint="2n" id="o3575" name="chromosome" name_original="" src="d0_s15" type="structure">
        <character name="quantity" src="d0_s15" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering summer–early autumn.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="early autumn" from="summer" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Crevices in granite and quartzite cliffs</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="crevices" constraint="in granite and quartzite cliffs" />
        <character name="habitat" value="granite" />
        <character name="habitat" value="quartzite cliffs" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>1300-2600 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="2600" to_unit="m" from="1300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>N.Mex., Tex.; Mexico.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="N.Mex." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tex." establishment_means="native" />
        <character name="distribution" value="Mexico" establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>44.</number>
  <other_name type="common_name">Rio Grande fire pink</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Silene plankii is a close relative of S. laciniata, differing in its compact tufted growth, small and narrow leaves, and shallowly two-lobed petals. It is endemic to the Del Carmen Mountains on either side of the Rio Grande valley. Plants of S. laciniata with a habit and leaves similar to S. plankii but the deeply laciniate petals of S. laciniata occur on the cliffs of Santa Cruz Island off the coast of California.</discussion>
  
</bio:treatment>