<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">212</other_info_on_meta>
    <other_info_on_meta type="mention_page">169</other_info_on_meta>
    <other_info_on_meta type="mention_page">178</other_info_on_meta>
    <other_info_on_meta type="mention_page">208</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">caryophyllaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="unknown" rank="subfamily">Caryophylloideae</taxon_name>
    <taxon_name authority="Linnaeus" date="unknown" rank="genus">silene</taxon_name>
    <taxon_name authority="Linnaeus" date="1753" rank="species">virginica</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 419. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family caryophyllaceae;subfamily caryophylloideae;genus silene;species virginica;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242417264</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Melandrium</taxon_name>
    <taxon_name authority="(Linnaeus) A. Braun" date="unknown" rank="species">virginicum</taxon_name>
    <taxon_hierarchy>genus Melandrium;species virginicum;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silene</taxon_name>
    <taxon_name authority="Walter" date="unknown" rank="species">catesbaei</taxon_name>
    <taxon_hierarchy>genus Silene;species catesbaei;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silene</taxon_name>
    <taxon_name authority="Moench" date="unknown" rank="species">coccinea</taxon_name>
    <taxon_hierarchy>genus Silene;species coccinea;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silene</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virginica</taxon_name>
    <taxon_name authority="Pickens &amp; M. C. W. Pickens" date="unknown" rank="variety">hallensis</taxon_name>
    <taxon_hierarchy>genus Silene;species virginica;variety hallensis;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Silene</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virginica</taxon_name>
    <taxon_name authority="Strausbaugh &amp; Core" date="unknown" rank="variety">robusta</taxon_name>
    <taxon_hierarchy>genus Silene;species virginica;variety robusta;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Plants perennial;</text>
      <biological_entity id="o2455" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="duration" src="d0_s0" value="perennial" value_original="perennial" />
        <character name="growth_form" value="plant" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>taproot slender;</text>
      <biological_entity id="o2456" name="taproot" name_original="taproot" src="d0_s1" type="structure">
        <character is_modifier="false" name="size" src="d0_s1" value="slender" value_original="slender" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>caudex decumbent, branched, producing tufts of leaves and erect flowering shoots.</text>
      <biological_entity id="o2457" name="caudex" name_original="caudex" src="d0_s2" type="structure" constraint="leaf" constraint_original="leaf; leaf">
        <character is_modifier="false" name="growth_form_or_orientation" src="d0_s2" value="decumbent" value_original="decumbent" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="branched" value_original="branched" />
      </biological_entity>
      <biological_entity id="o2458" name="tuft" name_original="tufts" src="d0_s2" type="structure" />
      <biological_entity id="o2459" name="leaf" name_original="leaves" src="d0_s2" type="structure" />
      <biological_entity id="o2460" name="shoot" name_original="shoots" src="d0_s2" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="true" name="life_cycle" src="d0_s2" value="flowering" value_original="flowering" />
      </biological_entity>
      <relation from="o2457" id="r272" name="producing" negation="false" src="d0_s2" to="o2458" />
      <relation from="o2457" id="r273" name="part_of" negation="false" src="d0_s2" to="o2459" />
      <relation from="o2457" id="r274" name="part_of" negation="false" src="d0_s2" to="o2460" />
    </statement>
    <statement id="d0_s3">
      <text>Stems simple proximal to inflorescence, 20–80 cm, glandular-pubescent, often subglabrous near base.</text>
      <biological_entity id="o2461" name="stem" name_original="stems" src="d0_s3" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s3" value="simple" value_original="simple" />
        <character is_modifier="false" name="position" src="d0_s3" value="proximal" value_original="proximal" />
        <character char_type="range_value" from="20" from_unit="cm" name="some_measurement" notes="" src="d0_s3" to="80" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character constraint="near base" constraintid="o2463" is_modifier="false" modifier="often" name="pubescence" src="d0_s3" value="subglabrous" value_original="subglabrous" />
      </biological_entity>
      <biological_entity id="o2462" name="inflorescence" name_original="inflorescence" src="d0_s3" type="structure" />
      <biological_entity id="o2463" name="base" name_original="base" src="d0_s3" type="structure" />
    </statement>
    <statement id="d0_s4">
      <text>Leaves: basal numerous, tufted, petiolate, petiole ciliate, blade oblanceolate, 3–10 cm × 8–18 mm, base spatulate, apex acute to obtuse, glabrous on both surfaces, rarely puberulent;</text>
      <biological_entity id="o2464" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o2465" name="leaf" name_original="leaves" src="d0_s4" type="structure">
        <character is_modifier="false" name="quantity" src="d0_s4" value="numerous" value_original="numerous" />
        <character is_modifier="false" name="arrangement_or_pubescence" src="d0_s4" value="tufted" value_original="tufted" />
        <character is_modifier="false" name="architecture" src="d0_s4" value="petiolate" value_original="petiolate" />
      </biological_entity>
      <biological_entity id="o2466" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s4" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o2467" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="oblanceolate" value_original="oblanceolate" />
        <character char_type="range_value" from="3" from_unit="cm" name="length" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="8" from_unit="mm" name="width" src="d0_s4" to="18" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2468" name="base" name_original="base" src="d0_s4" type="structure">
        <character is_modifier="false" name="shape" src="d0_s4" value="spatulate" value_original="spatulate" />
      </biological_entity>
      <biological_entity id="o2469" name="apex" name_original="apex" src="d0_s4" type="structure">
        <character char_type="range_value" from="acute" name="shape" src="d0_s4" to="obtuse" />
        <character constraint="on surfaces" constraintid="o2470" is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="rarely" name="pubescence" notes="" src="d0_s4" value="puberulent" value_original="puberulent" />
      </biological_entity>
      <biological_entity id="o2470" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
    </statement>
    <statement id="d0_s5">
      <text>cauline in 2–4 pairs, broadly petiolate to sessile, reduced distally, blade oblanceolate to narrowly elliptic or lanceolate, 1–10 (–30) cm × 4–16 (–30) mm, margins ciliate, apex acute, shortly acuminate, glabrous.</text>
      <biological_entity id="o2471" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity constraint="cauline" id="o2472" name="leaf" name_original="leaves" src="d0_s5" type="structure">
        <character char_type="range_value" from="broadly petiolate" name="architecture" notes="" src="d0_s5" to="sessile" />
        <character is_modifier="false" modifier="distally" name="size" src="d0_s5" value="reduced" value_original="reduced" />
      </biological_entity>
      <biological_entity id="o2473" name="pair" name_original="pairs" src="d0_s5" type="structure">
        <character char_type="range_value" from="2" is_modifier="true" name="quantity" src="d0_s5" to="4" />
      </biological_entity>
      <biological_entity id="o2474" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="oblanceolate" name="shape" src="d0_s5" to="narrowly elliptic or lanceolate" />
        <character char_type="range_value" from="10" from_inclusive="false" from_unit="cm" name="atypical_length" src="d0_s5" to="30" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="length" src="d0_s5" to="10" to_unit="cm" />
        <character char_type="range_value" from="16" from_inclusive="false" from_unit="mm" name="atypical_width" src="d0_s5" to="30" to_unit="mm" />
        <character char_type="range_value" from="4" from_unit="mm" name="width" src="d0_s5" to="16" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2475" name="margin" name_original="margins" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s5" value="ciliate" value_original="ciliate" />
      </biological_entity>
      <biological_entity id="o2476" name="apex" name_original="apex" src="d0_s5" type="structure">
        <character is_modifier="false" name="shape" src="d0_s5" value="acute" value_original="acute" />
        <character is_modifier="false" modifier="shortly" name="shape" src="d0_s5" value="acuminate" value_original="acuminate" />
        <character is_modifier="false" name="pubescence" src="d0_s5" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <relation from="o2472" id="r275" name="in" negation="false" src="d0_s5" to="o2473" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences open, with ascending, often elongate branches, (3–) 7–11 (–20) -flowered, bracteate, glandular-pubescent, often densely so, viscid;</text>
      <biological_entity id="o2477" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s6" value="(3-)7-11(-20)-flowered" value_original="(3-)7-11(-20)-flowered" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="bracteate" value_original="bracteate" />
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glandular-pubescent" value_original="glandular-pubescent" />
        <character is_modifier="false" modifier="often densely; densely" name="coating" src="d0_s6" value="viscid" value_original="viscid" />
      </biological_entity>
      <biological_entity id="o2478" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="true" name="orientation" src="d0_s6" value="ascending" value_original="ascending" />
        <character is_modifier="true" modifier="often" name="shape" src="d0_s6" value="elongate" value_original="elongate" />
      </biological_entity>
      <relation from="o2477" id="r276" name="with" negation="false" src="d0_s6" to="o2478" />
    </statement>
    <statement id="d0_s7">
      <text>bracts leaflike, lanceolate, 4–40 mm.</text>
      <biological_entity id="o2479" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s7" value="leaflike" value_original="leaflike" />
        <character is_modifier="false" name="shape" src="d0_s7" value="lanceolate" value_original="lanceolate" />
        <character char_type="range_value" from="4" from_unit="mm" name="some_measurement" src="d0_s7" to="40" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Pedicels erect in flower, sharply deflexed at base in fruit, 1/2–1 times length of calyx.</text>
      <biological_entity id="o2480" name="pedicel" name_original="pedicels" src="d0_s8" type="structure">
        <character constraint="in flower" constraintid="o2481" is_modifier="false" name="orientation" src="d0_s8" value="erect" value_original="erect" />
        <character constraint="at base" constraintid="o2482" is_modifier="false" modifier="sharply" name="orientation" notes="" src="d0_s8" value="deflexed" value_original="deflexed" />
        <character constraint="calyx" constraintid="o2484" is_modifier="false" name="length" notes="" src="d0_s8" value="1/2-1 times length of calyx" />
      </biological_entity>
      <biological_entity id="o2481" name="flower" name_original="flower" src="d0_s8" type="structure" />
      <biological_entity id="o2482" name="base" name_original="base" src="d0_s8" type="structure" />
      <biological_entity id="o2483" name="fruit" name_original="fruit" src="d0_s8" type="structure" />
      <biological_entity id="o2484" name="calyx" name_original="calyx" src="d0_s8" type="structure" />
      <relation from="o2482" id="r277" name="in" negation="false" src="d0_s8" to="o2483" />
    </statement>
    <statement id="d0_s9">
      <text>Flowers: calyx green to purple, 10-veined, tubular to narrowly obconic in flower, 16–22 × 5–6 mm, clavate and swelling to 7–12 mm in fruit, glandular-pubescent, lobes lanceolate to oblong, 3–4 mm, margins usually narrow, membranous, apex acute or obtuse;</text>
      <biological_entity id="o2485" name="flower" name_original="flowers" src="d0_s9" type="structure" />
      <biological_entity id="o2486" name="calyx" name_original="calyx" src="d0_s9" type="structure">
        <character char_type="range_value" from="green" name="coloration" src="d0_s9" to="purple" />
        <character is_modifier="false" name="architecture" src="d0_s9" value="10-veined" value_original="10-veined" />
        <character char_type="range_value" constraint="in flower" constraintid="o2487" from="tubular" name="shape" src="d0_s9" to="narrowly obconic" />
        <character char_type="range_value" from="16" from_unit="mm" name="length" notes="" src="d0_s9" to="22" to_unit="mm" />
        <character char_type="range_value" from="5" from_unit="mm" name="width" notes="" src="d0_s9" to="6" to_unit="mm" />
        <character is_modifier="false" name="shape" src="d0_s9" value="clavate" value_original="clavate" />
        <character constraint="in fruit" constraintid="o2488" is_modifier="false" name="size" src="d0_s9" value="swelling" value_original="swelling" />
        <character is_modifier="false" name="pubescence" notes="" src="d0_s9" value="glandular-pubescent" value_original="glandular-pubescent" />
      </biological_entity>
      <biological_entity id="o2487" name="flower" name_original="flower" src="d0_s9" type="structure" />
      <biological_entity id="o2488" name="fruit" name_original="fruit" src="d0_s9" type="structure" />
      <biological_entity id="o2489" name="lobe" name_original="lobes" src="d0_s9" type="structure">
        <character char_type="range_value" from="lanceolate" name="shape" src="d0_s9" to="oblong" />
        <character char_type="range_value" from="3" from_unit="mm" name="some_measurement" src="d0_s9" to="4" to_unit="mm" />
      </biological_entity>
      <biological_entity id="o2490" name="margin" name_original="margins" src="d0_s9" type="structure">
        <character is_modifier="false" modifier="usually" name="size_or_width" src="d0_s9" value="narrow" value_original="narrow" />
        <character is_modifier="false" name="texture" src="d0_s9" value="membranous" value_original="membranous" />
      </biological_entity>
      <biological_entity id="o2491" name="apex" name_original="apex" src="d0_s9" type="structure">
        <character is_modifier="false" name="shape" src="d0_s9" value="acute" value_original="acute" />
        <character is_modifier="false" name="shape" src="d0_s9" value="obtuse" value_original="obtuse" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>corolla scarlet, 2 times longer than calyx, clawed, claw ciliate, gradually widening into limb, longer than calyx, limb obtriangular to oblong, deeply 2-lobed with 2 small lateral teeth, 10–14 mm, glabrous or nearly so, appendages 2, tubular, 3 mm;</text>
      <biological_entity id="o2492" name="flower" name_original="flowers" src="d0_s10" type="structure" />
      <biological_entity id="o2493" name="corolla" name_original="corolla" src="d0_s10" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s10" value="scarlet" value_original="scarlet" />
        <character constraint="calyx" constraintid="o2494" is_modifier="false" name="length_or_size" src="d0_s10" value="2 times longer than calyx" />
        <character is_modifier="false" name="shape" src="d0_s10" value="clawed" value_original="clawed" />
      </biological_entity>
      <biological_entity id="o2494" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity id="o2495" name="claw" name_original="claw" src="d0_s10" type="structure">
        <character is_modifier="false" name="architecture_or_pubescence_or_shape" src="d0_s10" value="ciliate" value_original="ciliate" />
        <character constraint="into limb" constraintid="o2496" is_modifier="false" modifier="gradually" name="width" src="d0_s10" value="widening" value_original="widening" />
        <character constraint="than calyx" constraintid="o2497" is_modifier="false" name="length_or_size" notes="" src="d0_s10" value="longer" value_original="longer" />
      </biological_entity>
      <biological_entity id="o2496" name="limb" name_original="limb" src="d0_s10" type="structure" />
      <biological_entity id="o2497" name="calyx" name_original="calyx" src="d0_s10" type="structure" />
      <biological_entity id="o2498" name="limb" name_original="limb" src="d0_s10" type="structure">
        <character char_type="range_value" from="obtriangular" name="shape" src="d0_s10" to="oblong" />
        <character constraint="with lateral teeth" constraintid="o2499" is_modifier="false" modifier="deeply" name="shape" src="d0_s10" value="2-lobed" value_original="2-lobed" />
        <character char_type="range_value" from="10" from_unit="mm" name="some_measurement" notes="" src="d0_s10" to="14" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
        <character name="pubescence" src="d0_s10" value="nearly" value_original="nearly" />
      </biological_entity>
      <biological_entity constraint="lateral" id="o2499" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="true" name="size" src="d0_s10" value="small" value_original="small" />
      </biological_entity>
      <biological_entity id="o2500" name="appendage" name_original="appendages" src="d0_s10" type="structure">
        <character name="quantity" src="d0_s10" value="2" value_original="2" />
        <character is_modifier="false" name="shape" src="d0_s10" value="tubular" value_original="tubular" />
        <character name="some_measurement" src="d0_s10" unit="mm" value="3" value_original="3" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>stamens exserted, shorter than petals;</text>
      <biological_entity id="o2501" name="flower" name_original="flowers" src="d0_s11" type="structure" />
      <biological_entity id="o2502" name="stamen" name_original="stamens" src="d0_s11" type="structure">
        <character is_modifier="false" name="position" src="d0_s11" value="exserted" value_original="exserted" />
        <character constraint="than petals" constraintid="o2503" is_modifier="false" name="height_or_length_or_size" src="d0_s11" value="shorter" value_original="shorter" />
      </biological_entity>
      <biological_entity id="o2503" name="petal" name_original="petals" src="d0_s11" type="structure" />
    </statement>
    <statement id="d0_s12">
      <text>styles 3 (–4), equaling stamens.</text>
      <biological_entity id="o2504" name="flower" name_original="flowers" src="d0_s12" type="structure" />
      <biological_entity id="o2505" name="style" name_original="styles" src="d0_s12" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" name="atypical_quantity" src="d0_s12" to="4" />
        <character name="quantity" src="d0_s12" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o2506" name="stamen" name_original="stamens" src="d0_s12" type="structure">
        <character is_modifier="true" name="variability" src="d0_s12" value="equaling" value_original="equaling" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>Capsules ovoid, equaling calyx, opening by 3 (or 4) teeth that sometimes split into 6 (or 8);</text>
      <biological_entity id="o2507" name="capsule" name_original="capsules" src="d0_s13" type="structure">
        <character is_modifier="false" name="shape" src="d0_s13" value="ovoid" value_original="ovoid" />
      </biological_entity>
      <biological_entity id="o2508" name="calyx" name_original="calyx" src="d0_s13" type="structure">
        <character is_modifier="true" name="variability" src="d0_s13" value="equaling" value_original="equaling" />
      </biological_entity>
      <biological_entity id="o2509" name="tooth" name_original="teeth" src="d0_s13" type="structure">
        <character is_modifier="true" name="quantity" src="d0_s13" value="3" value_original="3" />
      </biological_entity>
      <biological_entity id="o2510" name="split" name_original="split" src="d0_s13" type="structure">
        <character name="quantity" src="d0_s13" value="6" value_original="6" />
      </biological_entity>
      <relation from="o2508" id="r278" name="opening by" negation="false" src="d0_s13" to="o2509" />
    </statement>
    <statement id="d0_s14">
      <text>carpophore 2–3 (–4) mm.</text>
      <biological_entity id="o2511" name="carpophore" name_original="carpophore" src="d0_s14" type="structure">
        <character char_type="range_value" from="3" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s14" to="4" to_unit="mm" />
        <character char_type="range_value" from="2" from_unit="mm" name="some_measurement" src="d0_s14" to="3" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>Seeds ash gray, reniform, 1.5 mm, with large inflated papillae.</text>
      <biological_entity id="o2513" name="papilla" name_original="papillae" src="d0_s15" type="structure">
        <character is_modifier="true" name="size" src="d0_s15" value="large" value_original="large" />
        <character is_modifier="true" name="shape" src="d0_s15" value="inflated" value_original="inflated" />
      </biological_entity>
      <relation from="o2512" id="r279" name="with" negation="false" src="d0_s15" to="o2513" />
    </statement>
    <statement id="d0_s16">
      <text>2n = 48.</text>
      <biological_entity id="o2512" name="seed" name_original="seeds" src="d0_s15" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s15" value="ash gray" value_original="ash gray" />
        <character is_modifier="false" name="shape" src="d0_s15" value="reniform" value_original="reniform" />
        <character name="some_measurement" src="d0_s15" unit="mm" value="1.5" value_original="1.5" />
      </biological_entity>
      <biological_entity constraint="2n" id="o2514" name="chromosome" name_original="" src="d0_s16" type="structure">
        <character name="quantity" src="d0_s16" value="48" value_original="48" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering spring.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="spring" from="spring" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Deciduous woodlands, bluffs, moist wooded slopes</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="deciduous woodlands" />
        <character name="habitat" value="bluffs" />
        <character name="habitat" value="wooded slopes" modifier="moist" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-1300 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1300" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Ala., Ark., Del., Ga., Ill., Ind., Iowa, Kans., Ky., La., Mich., Miss., Mo., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Va., W.Va.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Ala." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ark." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Del." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ga." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ill." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ind." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Iowa" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Kans." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ky." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="La." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mich." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Miss." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Mo." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.Y." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="N.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Ohio" establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Okla." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Pa." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="S.C." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Tenn." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="Va." establishment_means="native" />
        <character name="distribution" geographical_constraint="United States" value="W.Va." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>66.</number>
  <other_name type="common_name">Fire pink</other_name>
  <discussion>Silene virginica is related to the scarlet-flowered species from the southwest, S. laciniata and S. subciliata. It makes a beautiful garden plant in semishaded locations. J. A. Steyermark (1963) recorded the occurrence of a hybrid between S. virginica and S. caroliniana subsp. wherryi in Shannon County, Missouri. Reports of the occurrence of S. virginica in Ontario are based on a collection (CAN, K) made in 1873 from “islands in the Detroit River” in “Canada West.”</discussion>
  
</bio:treatment>