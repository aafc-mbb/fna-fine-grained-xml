<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">420</other_info_on_meta>
    <other_info_on_meta type="mention_page">414</other_info_on_meta>
    <other_info_on_meta type="mention_page">421</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Oregonium</taxon_name>
    <taxon_name authority="Greene" date="unknown" rank="species">nortonii</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 165. 1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus oregonium;species nortonii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">250060412</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Douglas ex Bentham" date="unknown" rank="species">vimineum</taxon_name>
    <taxon_name authority="(Greene) S. Stokes" date="unknown" rank="subspecies">nortonii</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species vimineum;subspecies nortonii;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, spreading, 0.5–2 dm, glabrous, usually reddish.</text>
      <biological_entity id="o30467" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="spreading" value_original="spreading" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="some_measurement" src="d0_s0" to="2" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="usually" name="coloration" src="d0_s0" value="reddish" value_original="reddish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: aerial flowering-stems prostrate to ascending or weakly erect, 0.1–0.3 dm, glabrous.</text>
      <biological_entity id="o30468" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o30469" name="flowering-stem" name_original="flowering-stems" src="d0_s1" type="structure">
        <character is_modifier="true" name="location" src="d0_s1" value="aerial" value_original="aerial" />
        <character char_type="range_value" from="prostrate" name="orientation" src="d0_s1" to="ascending or weakly erect" />
        <character char_type="range_value" from="0.1" from_unit="dm" name="some_measurement" src="d0_s1" to="0.3" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s1" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>Leaves basal and cauline;</text>
      <biological_entity id="o30470" name="leaf" name_original="leaves" src="d0_s2" type="structure">
        <character is_modifier="false" name="position" src="d0_s2" value="basal" value_original="basal" />
        <character is_modifier="false" name="position" src="d0_s2" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>basal: petiole 1–3 cm, tomentose, blade round to reniform, 0.5–1.5 × 0.5–1.5 cm, densely white-tomentose abaxially, subglabrous or glabrous and green adaxially;</text>
      <biological_entity constraint="basal" id="o30471" name="leaf" name_original="leaves" src="d0_s3" type="structure" />
      <biological_entity id="o30472" name="petiole" name_original="petiole" src="d0_s3" type="structure">
        <character char_type="range_value" from="1" from_unit="cm" name="some_measurement" src="d0_s3" to="3" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="tomentose" value_original="tomentose" />
      </biological_entity>
      <biological_entity id="o30473" name="blade" name_original="blade" src="d0_s3" type="structure">
        <character char_type="range_value" from="round" name="shape" src="d0_s3" to="reniform" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="length" src="d0_s3" to="1.5" to_unit="cm" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="width" src="d0_s3" to="1.5" to_unit="cm" />
        <character is_modifier="false" modifier="densely; abaxially" name="pubescence" src="d0_s3" value="white-tomentose" value_original="white-tomentose" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="subglabrous" value_original="subglabrous" />
        <character is_modifier="false" name="pubescence" src="d0_s3" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="adaxially" name="coloration" src="d0_s3" value="green" value_original="green" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>cauline: petiole and blade similar to basal leaves.</text>
      <biological_entity constraint="cauline" id="o30474" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o30475" name="petiole" name_original="petiole" src="d0_s4" type="structure" />
      <biological_entity id="o30476" name="blade" name_original="blade" src="d0_s4" type="structure" />
      <biological_entity constraint="basal" id="o30477" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <relation from="o30475" id="r3439" name="to" negation="false" src="d0_s4" to="o30477" />
      <relation from="o30476" id="r3440" name="to" negation="false" src="d0_s4" to="o30477" />
    </statement>
    <statement id="d0_s5">
      <text>Inflorescences cymose, not distally uniparous due to suppression of secondary branches, open, 4–18 × 10–30 cm;</text>
      <biological_entity id="o30478" name="inflorescence" name_original="inflorescences" src="d0_s5" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s5" value="cymose" value_original="cymose" />
        <character constraint="of secondary branches" constraintid="o30479" is_modifier="false" modifier="not distally" name="architecture" src="d0_s5" value="uniparous" value_original="uniparous" />
        <character is_modifier="false" name="architecture" notes="" src="d0_s5" value="open" value_original="open" />
        <character char_type="range_value" from="4" from_unit="cm" name="length" src="d0_s5" to="18" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="width" src="d0_s5" to="30" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="secondary" id="o30479" name="branch" name_original="branches" src="d0_s5" type="structure" />
    </statement>
    <statement id="d0_s6">
      <text>branches glabrous;</text>
      <biological_entity id="o30480" name="branch" name_original="branches" src="d0_s6" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s6" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>bracts 1–2 × 1–2 mm.</text>
      <biological_entity id="o30481" name="bract" name_original="bracts" src="d0_s7" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s7" to="2" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="width" src="d0_s7" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s8">
      <text>Peduncles absent.</text>
      <biological_entity id="o30482" name="peduncle" name_original="peduncles" src="d0_s8" type="structure">
        <character is_modifier="false" name="presence" src="d0_s8" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Involucres terminal at tips of slender branchlets proximally, not appressed to branches, broadly turbinate, 3–4 × 2.5–3.5 mm, glabrous;</text>
      <biological_entity id="o30483" name="involucre" name_original="involucres" src="d0_s9" type="structure">
        <character constraint="at tips" constraintid="o30484" is_modifier="false" name="position_or_structure_subtype" src="d0_s9" value="terminal" value_original="terminal" />
        <character constraint="to branches" constraintid="o30486" is_modifier="false" modifier="not" name="fixation_or_orientation" notes="" src="d0_s9" value="appressed" value_original="appressed" />
        <character is_modifier="false" modifier="broadly" name="shape" notes="" src="d0_s9" value="turbinate" value_original="turbinate" />
        <character char_type="range_value" from="3" from_unit="mm" name="length" src="d0_s9" to="4" to_unit="mm" />
        <character char_type="range_value" from="2.5" from_unit="mm" name="width" src="d0_s9" to="3.5" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o30484" name="tip" name_original="tips" src="d0_s9" type="structure" />
      <biological_entity constraint="slender" id="o30485" name="branchlet" name_original="branchlets" src="d0_s9" type="structure" />
      <biological_entity id="o30486" name="branch" name_original="branches" src="d0_s9" type="structure" />
      <relation from="o30484" id="r3441" name="part_of" negation="false" src="d0_s9" to="o30485" />
    </statement>
    <statement id="d0_s10">
      <text>teeth 6–8, erect, 0.4–0.8 mm.</text>
      <biological_entity id="o30487" name="tooth" name_original="teeth" src="d0_s10" type="structure">
        <character char_type="range_value" from="6" name="quantity" src="d0_s10" to="8" />
        <character is_modifier="false" name="orientation" src="d0_s10" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.4" from_unit="mm" name="some_measurement" src="d0_s10" to="0.8" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>Flowers 1–2 mm;</text>
      <biological_entity id="o30488" name="flower" name_original="flowers" src="d0_s11" type="structure">
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s11" to="2" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>perianth white to rose, glabrous;</text>
      <biological_entity id="o30489" name="perianth" name_original="perianth" src="d0_s12" type="structure">
        <character char_type="range_value" from="white" name="coloration" src="d0_s12" to="rose" />
        <character is_modifier="false" name="pubescence" src="d0_s12" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>tepals monomorphic, obovate;</text>
      <biological_entity id="o30490" name="tepal" name_original="tepals" src="d0_s13" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s13" value="monomorphic" value_original="monomorphic" />
        <character is_modifier="false" name="shape" src="d0_s13" value="obovate" value_original="obovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s14">
      <text>stamens included, 1–1.5 mm;</text>
      <biological_entity id="o30491" name="stamen" name_original="stamens" src="d0_s14" type="structure">
        <character is_modifier="false" name="position" src="d0_s14" value="included" value_original="included" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s14" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>filaments glabrous.</text>
      <biological_entity id="o30492" name="filament" name_original="filaments" src="d0_s15" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s15" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>Achenes light-brown, 3-gonous, 1–1.3 mm.</text>
      <biological_entity id="o30493" name="achene" name_original="achenes" src="d0_s16" type="structure">
        <character is_modifier="false" name="coloration" src="d0_s16" value="light-brown" value_original="light-brown" />
        <character is_modifier="false" name="shape" src="d0_s16" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s16" to="1.3" to_unit="mm" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering May–Aug.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Aug" from="May" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Sandy to gravelly slopes, mixed grassland and chaparral communities, oak and pine woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="sandy to gravelly slopes" />
        <character name="habitat" value="mixed grassland" />
        <character name="habitat" value="chaparral communities" />
        <character name="habitat" value="pine woodlands" modifier="oak and" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>300-1200 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1200" to_unit="m" from="300" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>207.</number>
  <other_name type="past_name">nortoni</other_name>
  <other_name type="common_name">Pinnacles wild buckwheat</other_name>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Eriogonum nortonii is a rare and localized species restricted to the Gabilan Range of Monterey and San Benito counties, from Fremont Peak south to The Pinnacles. It is confined largely to two protected areas, the Hastings Natural History State Reservation and the Pinnacles National Monument. A disjunct population is found southeast of Carmel Highlands in the Santa Lucia Mountains of Monterey County.</discussion>
  
</bio:treatment>