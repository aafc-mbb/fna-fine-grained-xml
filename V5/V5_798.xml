<?xml version="1.0" encoding="UTF-8"?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics https://raw.githubusercontent.com/biosemantics/schemas/master/semanticMarkupOutput.xsd">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <processed_by>
      <processor>
        <date>2020/06/09 03:04:15</date>
        <software type="Semantic Markup" version="0.1.196-SNAPSHOT">CharaParser</software>
        <operator />
        <resource type="OTO Glossary" version="0.19">Plant</resource>
      </processor>
    </processed_by>
    <other_info_on_meta type="treatment_page">390</other_info_on_meta>
    <other_info_on_meta type="mention_page">383</other_info_on_meta>
    <other_info_on_meta type="volume">5</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="Jussieu" date="unknown" rank="family">polygonaceae</taxon_name>
    <taxon_name authority="Arnott in M. Napier" date="1832" rank="subfamily">Eriogonoideae</taxon_name>
    <taxon_name authority="Michaux" date="1803" rank="genus">eriogonum</taxon_name>
    <taxon_name authority="(S. Watson) Greene" date="1891" rank="subgenus">Ganysma</taxon_name>
    <taxon_name authority="S. Watson" date="1886" rank="species">ordii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>21: 468. 1886</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygonaceae;subfamily eriogonoideae;genus eriogonum;subgenus ganysma;species ordii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250060435</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Eriogonum</taxon_name>
    <taxon_name authority="Eastwood" date="unknown" rank="species">tenuissimum</taxon_name>
    <taxon_hierarchy>genus Eriogonum;species tenuissimum;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">
    <statement id="d0_s0">
      <text>Herbs, erect, annual, (0.5–) 1–7 dm, glabrous and sparsely floccose, greenish.</text>
      <biological_entity id="o24090" name="whole_organism" name_original="" src="" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s0" value="erect" value_original="erect" />
        <character is_modifier="false" name="duration" src="d0_s0" value="annual" value_original="annual" />
        <character char_type="range_value" from="0.5" from_unit="dm" name="atypical_some_measurement" src="d0_s0" to="1" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="1" from_unit="dm" name="some_measurement" src="d0_s0" to="7" to_unit="dm" />
        <character is_modifier="false" name="pubescence" src="d0_s0" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="sparsely" name="pubescence" src="d0_s0" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="coloration" src="d0_s0" value="greenish" value_original="greenish" />
        <character name="growth_form" value="herb" />
      </biological_entity>
    </statement>
    <statement id="d0_s1">
      <text>Stems: caudex absent;</text>
      <biological_entity id="o24091" name="stem" name_original="stems" src="d0_s1" type="structure" />
      <biological_entity id="o24092" name="caudex" name_original="caudex" src="d0_s1" type="structure">
        <character is_modifier="false" name="presence" src="d0_s1" value="absent" value_original="absent" />
      </biological_entity>
    </statement>
    <statement id="d0_s2">
      <text>aerial flowering-stems erect, solid, not fistulose, (0.3–) 0.7–3 dm, thinly floccose or glabrous, floccose proximally.</text>
      <biological_entity id="o24093" name="stem" name_original="stems" src="d0_s2" type="structure" />
      <biological_entity id="o24094" name="flowering-stem" name_original="flowering-stems" src="d0_s2" type="structure">
        <character is_modifier="true" name="location" src="d0_s2" value="aerial" value_original="aerial" />
        <character is_modifier="false" name="orientation" src="d0_s2" value="erect" value_original="erect" />
        <character is_modifier="false" name="architecture" src="d0_s2" value="solid" value_original="solid" />
        <character is_modifier="false" modifier="not" name="shape" src="d0_s2" value="fistulose" value_original="fistulose" />
        <character char_type="range_value" from="0.3" from_unit="dm" name="atypical_some_measurement" src="d0_s2" to="0.7" to_inclusive="false" to_unit="dm" />
        <character char_type="range_value" from="0.7" from_unit="dm" name="some_measurement" src="d0_s2" to="3" to_unit="dm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s2" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s2" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="proximally" name="pubescence" src="d0_s2" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s3">
      <text>Leaves basal, occasionally cauline;</text>
      <biological_entity id="o24095" name="leaf" name_original="leaves" src="d0_s3" type="structure">
        <character is_modifier="false" name="position" src="d0_s3" value="basal" value_original="basal" />
        <character is_modifier="false" modifier="occasionally" name="position" src="d0_s3" value="cauline" value_original="cauline" />
      </biological_entity>
    </statement>
    <statement id="d0_s4">
      <text>basal: petiole 2–6 (–10) cm, floccose, blade oblong-oblanceolate to obovate, (1.5–) 2–8 × (0.8–) 1–3 cm, thinly floccose or glabrous and green on both surfaces, margins entire;</text>
      <biological_entity constraint="basal" id="o24096" name="leaf" name_original="leaves" src="d0_s4" type="structure" />
      <biological_entity id="o24097" name="petiole" name_original="petiole" src="d0_s4" type="structure">
        <character char_type="range_value" from="6" from_inclusive="false" from_unit="cm" name="atypical_some_measurement" src="d0_s4" to="10" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="some_measurement" src="d0_s4" to="6" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o24098" name="blade" name_original="blade" src="d0_s4" type="structure">
        <character char_type="range_value" from="oblong-oblanceolate" name="shape" src="d0_s4" to="obovate" />
        <character char_type="range_value" from="1.5" from_unit="cm" name="atypical_length" src="d0_s4" to="2" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="2" from_unit="cm" name="length" src="d0_s4" to="8" to_unit="cm" />
        <character char_type="range_value" from="0.8" from_unit="cm" name="atypical_width" src="d0_s4" to="1" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="1" from_unit="cm" name="width" src="d0_s4" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s4" value="floccose" value_original="floccose" />
        <character is_modifier="false" name="pubescence" src="d0_s4" value="glabrous" value_original="glabrous" />
        <character constraint="on surfaces" constraintid="o24099" is_modifier="false" name="coloration" src="d0_s4" value="green" value_original="green" />
      </biological_entity>
      <biological_entity id="o24099" name="surface" name_original="surfaces" src="d0_s4" type="structure" />
      <biological_entity id="o24100" name="margin" name_original="margins" src="d0_s4" type="structure">
        <character is_modifier="false" name="architecture_or_shape" src="d0_s4" value="entire" value_original="entire" />
      </biological_entity>
    </statement>
    <statement id="d0_s5">
      <text>cauline: petiole 0.5–3 cm, thinly floccose, blade elliptic to obovate, 0.7–3 × 0.2–2 cm, similar to basal blade.</text>
      <biological_entity constraint="cauline" id="o24101" name="leaf" name_original="leaves" src="d0_s5" type="structure" />
      <biological_entity id="o24102" name="petiole" name_original="petiole" src="d0_s5" type="structure">
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s5" to="3" to_unit="cm" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s5" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity id="o24103" name="blade" name_original="blade" src="d0_s5" type="structure">
        <character char_type="range_value" from="elliptic" name="shape" src="d0_s5" to="obovate" />
        <character char_type="range_value" from="0.7" from_unit="cm" name="length" src="d0_s5" to="3" to_unit="cm" />
        <character char_type="range_value" from="0.2" from_unit="cm" name="width" src="d0_s5" to="2" to_unit="cm" />
      </biological_entity>
      <biological_entity constraint="basal" id="o24104" name="blade" name_original="blade" src="d0_s5" type="structure" />
      <relation from="o24103" id="r2671" name="to" negation="false" src="d0_s5" to="o24104" />
    </statement>
    <statement id="d0_s6">
      <text>Inflorescences paniculate, open to diffuse, (5–) 10–50 × 5–50 cm;</text>
      <biological_entity id="o24105" name="inflorescence" name_original="inflorescences" src="d0_s6" type="structure">
        <character is_modifier="false" name="arrangement" src="d0_s6" value="paniculate" value_original="paniculate" />
        <character is_modifier="false" name="architecture" src="d0_s6" value="open" value_original="open" />
        <character is_modifier="false" name="density" src="d0_s6" value="diffuse" value_original="diffuse" />
        <character char_type="range_value" from="5" from_unit="cm" name="atypical_length" src="d0_s6" to="10" to_inclusive="false" to_unit="cm" />
        <character char_type="range_value" from="10" from_unit="cm" name="length" src="d0_s6" to="50" to_unit="cm" />
        <character char_type="range_value" from="5" from_unit="cm" name="width" src="d0_s6" to="50" to_unit="cm" />
      </biological_entity>
    </statement>
    <statement id="d0_s7">
      <text>branches not fistulose, glabrous except for floccose nodes and proximal branches;</text>
      <biological_entity id="o24106" name="branch" name_original="branches" src="d0_s7" type="structure">
        <character is_modifier="false" modifier="not" name="shape" src="d0_s7" value="fistulose" value_original="fistulose" />
        <character constraint="except-for proximal branches" constraintid="o24108" is_modifier="false" name="pubescence" src="d0_s7" value="glabrous" value_original="glabrous" />
      </biological_entity>
      <biological_entity id="o24107" name="node" name_original="nodes" src="d0_s7" type="structure">
        <character is_modifier="true" name="pubescence" src="d0_s7" value="floccose" value_original="floccose" />
      </biological_entity>
      <biological_entity constraint="proximal" id="o24108" name="branch" name_original="branches" src="d0_s7" type="structure" />
    </statement>
    <statement id="d0_s8">
      <text>bracts 3, scalelike, 0.5–3 × 0.3–1 mm.</text>
      <biological_entity id="o24109" name="bract" name_original="bracts" src="d0_s8" type="structure">
        <character name="quantity" src="d0_s8" value="3" value_original="3" />
        <character is_modifier="false" name="shape" src="d0_s8" value="scale-like" value_original="scalelike" />
        <character char_type="range_value" from="0.5" from_unit="mm" name="length" src="d0_s8" to="3" to_unit="mm" />
        <character char_type="range_value" from="0.3" from_unit="mm" name="width" src="d0_s8" to="1" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s9">
      <text>Peduncles erect, straight, capillary, 0.5–2 cm, glabrous or thinly floccose.</text>
      <biological_entity id="o24110" name="peduncle" name_original="peduncles" src="d0_s9" type="structure">
        <character is_modifier="false" name="orientation" src="d0_s9" value="erect" value_original="erect" />
        <character is_modifier="false" name="course" src="d0_s9" value="straight" value_original="straight" />
        <character is_modifier="false" name="shape" src="d0_s9" value="capillary" value_original="capillary" />
        <character char_type="range_value" from="0.5" from_unit="cm" name="some_measurement" src="d0_s9" to="2" to_unit="cm" />
        <character is_modifier="false" name="pubescence" src="d0_s9" value="glabrous" value_original="glabrous" />
        <character is_modifier="false" modifier="thinly" name="pubescence" src="d0_s9" value="floccose" value_original="floccose" />
      </biological_entity>
    </statement>
    <statement id="d0_s10">
      <text>Involucres narrowly turbinate to turbinate, 1–1.5 (–1.8) × 0.6–1.2 mm, glabrous;</text>
      <biological_entity id="o24111" name="involucre" name_original="involucres" src="d0_s10" type="structure">
        <character char_type="range_value" from="narrowly turbinate" name="shape" src="d0_s10" to="turbinate" />
        <character char_type="range_value" from="1.5" from_inclusive="false" from_unit="mm" name="atypical_length" src="d0_s10" to="1.8" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="length" src="d0_s10" to="1.5" to_unit="mm" />
        <character char_type="range_value" from="0.6" from_unit="mm" name="width" src="d0_s10" to="1.2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s10" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s11">
      <text>teeth 4, erect, 0.2–0.5 mm.</text>
      <biological_entity id="o24112" name="tooth" name_original="teeth" src="d0_s11" type="structure">
        <character name="quantity" src="d0_s11" value="4" value_original="4" />
        <character is_modifier="false" name="orientation" src="d0_s11" value="erect" value_original="erect" />
        <character char_type="range_value" from="0.2" from_unit="mm" name="some_measurement" src="d0_s11" to="0.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s12">
      <text>Flowers 1–2.5 (–3) mm;</text>
      <biological_entity id="o24113" name="flower" name_original="flowers" src="d0_s12" type="structure">
        <character char_type="range_value" from="2.5" from_inclusive="false" from_unit="mm" name="atypical_some_measurement" src="d0_s12" to="3" to_unit="mm" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s12" to="2.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s13">
      <text>perianth white with greenish or reddish midribs to pale-yellow with greenish midribs, becoming pink to reddish, densely short-villous;</text>
      <biological_entity id="o24114" name="perianth" name_original="perianth" src="d0_s13" type="structure">
        <character constraint="with midribs" constraintid="o24115" is_modifier="false" name="coloration" src="d0_s13" value="white" value_original="white" />
        <character char_type="range_value" from="pink" modifier="becoming" name="coloration" notes="" src="d0_s13" to="reddish" />
        <character is_modifier="false" modifier="densely" name="pubescence" src="d0_s13" value="short-villous" value_original="short-villous" />
      </biological_entity>
      <biological_entity id="o24115" name="midrib" name_original="midribs" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="reddish" value_original="reddish" />
      </biological_entity>
      <biological_entity id="o24116" name="midrib" name_original="midribs" src="d0_s13" type="structure">
        <character is_modifier="true" name="coloration" src="d0_s13" value="pale-yellow" value_original="pale-yellow" />
        <character is_modifier="true" name="coloration" src="d0_s13" value="greenish" value_original="greenish" />
      </biological_entity>
      <relation from="o24115" id="r2672" name="to" negation="false" src="d0_s13" to="o24116" />
    </statement>
    <statement id="d0_s14">
      <text>tepals monomorphic, oblong to narrowly ovate;</text>
      <biological_entity id="o24117" name="tepal" name_original="tepals" src="d0_s14" type="structure">
        <character is_modifier="false" name="architecture" src="d0_s14" value="monomorphic" value_original="monomorphic" />
        <character char_type="range_value" from="oblong" name="shape" src="d0_s14" to="narrowly ovate" />
      </biological_entity>
    </statement>
    <statement id="d0_s15">
      <text>stamens exserted, 1–1.5 mm;</text>
      <biological_entity id="o24118" name="stamen" name_original="stamens" src="d0_s15" type="structure">
        <character is_modifier="false" name="position" src="d0_s15" value="exserted" value_original="exserted" />
        <character char_type="range_value" from="1" from_unit="mm" name="some_measurement" src="d0_s15" to="1.5" to_unit="mm" />
      </biological_entity>
    </statement>
    <statement id="d0_s16">
      <text>filaments glabrous.</text>
      <biological_entity id="o24119" name="filament" name_original="filaments" src="d0_s16" type="structure">
        <character is_modifier="false" name="pubescence" src="d0_s16" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
    <statement id="d0_s17">
      <text>Achenes dark-brown to black, 3-gonous, 1.8–2 mm, glabrous.</text>
      <biological_entity id="o24120" name="achene" name_original="achenes" src="d0_s17" type="structure">
        <character char_type="range_value" from="dark-brown" name="coloration" src="d0_s17" to="black" />
        <character is_modifier="false" name="shape" src="d0_s17" value="3-gonous" value_original="3-gonous" />
        <character char_type="range_value" from="1.8" from_unit="mm" name="some_measurement" src="d0_s17" to="2" to_unit="mm" />
        <character is_modifier="false" name="pubescence" src="d0_s17" value="glabrous" value_original="glabrous" />
      </biological_entity>
    </statement>
  </description>
  <description type="phenology">
    <statement id="phenology_0">
      <text>Flowering Mar–Jul.</text>
      <biological_entity id="phen_o0" name="whole_organism" name_original="" type="structure">
        <character name="flowering time" char_type="range_value" to="Jul" from="Mar" />
      </biological_entity>
    </statement>
  </description>
  <description type="habitat">
    <statement id="habitat_0">
      <text>Gravelly to clayey flats and slopes, mixed grassland communities, oak and conifer woodlands</text>
      <biological_entity id="hab_o0" name="whole_organism" name_original="" type="structure">
        <character name="habitat" value="gravelly to clayey flats" />
        <character name="habitat" value="gravelly to slopes" />
        <character name="habitat" value="mixed grassland communities" />
        <character name="habitat" value="oak" />
        <character name="habitat" value="conifer woodlands" />
      </biological_entity>
    </statement>
  </description>
  <description type="elevation">
    <statement id="elevation_0">
      <text>200-1400 m</text>
      <biological_entity id="elev_o0" name="whole_organism" name_original="" type="structure">
        <character name="elevation" char_type="range_value" to="1400" to_unit="m" from="200" from_unit="m" />
      </biological_entity>
    </statement>
  </description>
  <description type="distribution">
    <statement id="distribution_0">
      <text>Calif.</text>
      <biological_entity id="dis_o0" name="whole_organism" name_original="" type="structure">
        <character name="distribution" geographical_constraint="United States" value="Calif." establishment_means="native" />
      </biological_entity>
    </statement>
  </description>
  <number>156.</number>
  <other_name type="common_name">Fort Mohave wild buckwheat</other_name>
  <discussion>Eriogonum ordii is infrequently encountered (rarely locally common) along the inner Coast Ranges from Monterey and San Benito counties south through Fresno, Merced, and San Luis Obispo counties to Ventura County, then eastward across northern Los Angeles County to the hills east of Bakersfield in Kern County. This distribution is based on confirmed modern collections. The type (J. G. Lemmon 4189, ASU, BM, DS, G, GH, ISC, K, P, UC, US) supposedly was collected near Fort Mohave, Mohave County, Arizona, in 1884. Two T. Brandegee specimens reportedly were gathered on the boundary of San Diego and Imperial counties, one at Split Mountain (Apr 1905, UC) and the second along San Felipe Creek (4 Apr 1901, UC). Brandegee’s label data often are dubious, and these disjunct sites are discounted.</discussion>
  
</bio:treatment>